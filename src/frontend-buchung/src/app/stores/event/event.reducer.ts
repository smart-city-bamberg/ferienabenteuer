import { createFeature, createReducer, on } from '@ngrx/store';
import { EventActions } from './event.actions';
import { DiscountType, Event } from './event.types';

export const eventFeatureKey = 'event';

export interface State {
  event: Event | undefined;
  discountTypes: DiscountType[];
  tickets: {
    [index: string]: number;
  };
}

export const initialState: State = {
  event: undefined,
  discountTypes: [],
  tickets: {},
};

export const reducer = createReducer(
  initialState,
  on(EventActions.loadEvent, (state) => state),
  on(EventActions.loadEventSuccess, (state, action) => ({
    event: action.event ? { ...action.event } : undefined,
    discountTypes: [...action.discountTypes],
    tickets: {},
  })),
  on(EventActions.loadEventFailure, (state, action) => state),
  on(EventActions.addTicket, (state, action) => ({
    ...state,
    tickets: {
      ...state.tickets,
      [action.discountType]: (state.tickets[action.discountType] ?? 0) + 1,
    },
  })),
  on(EventActions.removeTicket, (state, action) => ({
    ...state,
    tickets: {
      ...state.tickets,
      [action.discountType]: Math.max(
        (state.tickets[action.discountType] ?? 0) - 1,
        0
      ),
    },
  }))
);

export const eventFeature = createFeature({
  name: eventFeatureKey,
  reducer,
});
