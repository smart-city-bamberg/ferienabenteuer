import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import * as fromEvent from './event.reducer';
import { EffectsModule } from '@ngrx/effects';
import { EventEffects } from './event.effects';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature(fromEvent.eventFeatureKey, fromEvent.reducer),
    EffectsModule.forFeature([EventEffects]),
  ],
  declarations: [],
})
export class EventModule {}
