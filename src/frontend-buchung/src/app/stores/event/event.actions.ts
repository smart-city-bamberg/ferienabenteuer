import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { DiscountType, Event } from './event.types';

export const EventActions = createActionGroup({
  source: 'Event',
  events: {
    'Load Event': props<{
      id: string;
    }>(),
    'Load Event Success': props<{
      event: Event;
      discountTypes: DiscountType[];
    }>(),
    'Load Event Failure': props<{ error: unknown }>(),

    'Add Ticket': props<{
      discountType: string;
    }>(),
    'Remove Ticket': props<{
      discountType: string;
    }>(),

    'Add Tickets To Cart': emptyProps(),
    'Add Tickets To Cart Success': emptyProps(),
    'Add Tickets To Cart No Ticket Available': emptyProps(),
    'Add Tickets To Cart Not Logged In': emptyProps(),
    'Add Tickets To Cart Maximum Children Count': emptyProps(),
    'Add Tickets To Cart Booking Not Started': emptyProps(),
    'Add Tickets To Cart Failure': props<{ error: unknown }>(),
  },
});
