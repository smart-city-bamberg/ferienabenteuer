import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  catchError,
  map,
  concatMap,
  filter,
  mergeMap,
  withLatestFrom,
  tap,
  delay,
} from 'rxjs/operators';
import { of } from 'rxjs';
import { EventActions } from './event.actions';
import { AddTicketsToCartGQL, EventGQL } from 'src/app/graphql/generated';
import { DiscountType, Event } from './event.types';
import { routerNavigatedAction } from '@ngrx/router-store';
import { Store } from '@ngrx/store';
import { selectEvent, selectTicketCounts } from './event.selectors';
import { Router } from '@angular/router';
import { selectUser } from '../auth/auth.selectors';

@Injectable()
export class EventEffects {
  loadEvent$ = createEffect(() =>
    this.actions$.pipe(
      ofType(EventActions.loadEvent),
      concatMap((action) =>
        this.eventQuery
          .fetch({
            id: action.id,
          })
          .pipe(
            map((response) =>
              EventActions.loadEventSuccess({
                event: response.data.event as Event,
                discountTypes: response.data.discountTypes as DiscountType[],
              })
            ),
            catchError((error) => of(EventActions.loadEventFailure({ error })))
          )
      )
    )
  );

  addTicketsToCart$ = createEffect(() =>
    this.actions$.pipe(
      ofType(EventActions.addTicketsToCart),
      withLatestFrom(
        this.store.select(selectTicketCounts),
        this.store.select(selectEvent),
        this.store.select(selectUser)
      ),
      concatMap(([action, ticketCounts, event, user]) =>
        !user
          ? of(EventActions.addTicketsToCartNotLoggedIn())
          : this.addTicketsToCartMutation
              .mutate({
                eventId: event?.id as string,
                amounts: Object.entries(ticketCounts).map(([key, value]) => ({
                  amount: value,
                  discountType: key ? key : null,
                })),
              })
              .pipe(
                map((response) => {
                  const addTicketsToCart = response.data?.addTicketsToCart;

                  if (
                    addTicketsToCart?.__typename == 'AddTicketsToCartSuccess' &&
                    addTicketsToCart.cartTickets
                  ) {
                    return EventActions.addTicketsToCartSuccess();
                  }

                  if (
                    addTicketsToCart?.__typename == 'AddTicketsToCartFailed' &&
                    addTicketsToCart.reason == 'NotEnoughTicketsAvailable'
                  ) {
                    return EventActions.addTicketsToCartNoTicketAvailable();
                  }

                  if (
                    addTicketsToCart?.__typename == 'AddTicketsToCartFailed' &&
                    addTicketsToCart.reason == 'MaximumAllowedTicketsPerEvent'
                  ) {
                    return EventActions.addTicketsToCartMaximumChildrenCount();
                  }

                  if (
                    addTicketsToCart?.__typename == 'AddTicketsToCartFailed' &&
                    addTicketsToCart.reason == 'BookingNotStarted'
                  ) {
                    return EventActions.addTicketsToCartBookingNotStarted();
                  }

                  throw Error('no result');
                }),
                catchError((error) =>
                  of(EventActions.addTicketsToCartFailure({ error }))
                )
              )
      )
    )
  );

  loadEventAfterNavigate$ = createEffect(() =>
    this.actions$.pipe(
      ofType(routerNavigatedAction),
      filter((action) =>
        action.payload.routerState.url.startsWith('/veranstaltungen/')
      ),
      map((action) =>
        EventActions.loadEvent({
          id: action.payload.routerState.url.replace('/veranstaltungen/', ''),
        })
      )
    )
  );

  redirectToCartWhenAddToCartSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(EventActions.addTicketsToCartSuccess),
        tap(() => this.router.navigate(['/checkout/warenkorb']))
      ),
    {
      dispatch: false,
    }
  );

  constructor(
    private actions$: Actions,
    private store: Store,
    private router: Router,
    private eventQuery: EventGQL,
    private addTicketsToCartMutation: AddTicketsToCartGQL
  ) {}
}
