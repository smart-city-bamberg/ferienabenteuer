export interface Organizer {
  name: string;
  phone: string;
  email: string;
  addressStreet: string;
  addressZip: string;
  addressCity: string;
  contactPersonFirstname: string;
  contactPersonSalutation: string;
  contactPersonSurname: string;
}

export interface Season {
  id: string;
  year: number;
  bookingStart: string;
  bookingStartPartners: string;
}

export interface Holiday {
  id: string;
  name: string;
  periods: Period[];
}

export interface Category {
  name: string;
}

export interface Period {
  id: string;
  startDate: string;
  endDate: string;
  holiday: Holiday;
  season: Season;
}

export interface CarePeriod {
  period: Period;
  careDays: CareDay[];
}

export interface CareDay {
  day: string;
}

export interface Event {
  id: string;
  name: string;
  careTimeStart: string;
  careTimeEnd: string;
  ageMinimum: number;
  ageMaximum: number;
  costs: number;
  registrationDeadline: string;
  picture:
    | {
        url: string;
      }
    | undefined;
  organizer: Organizer;
  locationCity: string;
  locationStreet: string;
  locationZipcode: string;
  categories: Category[];
  carePeriodCommitted: CarePeriod;
  waitingListCount: number;
  waitingListLimit: number;
  participantsCount: number;
  participantLimit: number;
  description: string;
  contactPersonSalutation: string;
  contactPersonFirstname: string;
  contactPersonSurname: string;
  hints: string;
  meals: string;

  availableTicketCountParticipant: number;
  availableTicketCountWaitinglist: number;
}

export interface DiscountType {
  id: string;
  name: string;
  discountPercent: number;
  description: string;
}
