import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromEvent from './event.reducer';

export const selectEventState = createFeatureSelector<fromEvent.State>(
  fromEvent.eventFeatureKey
);

export const selectEvent = createSelector(
  selectEventState,
  (state) => state.event
);

export const selectDiscountTypes = createSelector(
  selectEventState,
  (state) => state.discountTypes
);

export const selectPrice = createSelector(selectEvent, (event) => event?.costs);

export const selectTicketCounts = createSelector(
  selectEventState,
  (state) => state.tickets
);

export const selectTickets = createSelector(
  selectPrice,
  selectDiscountTypes,
  selectTicketCounts,
  (costs, discountTypes, ticketCounts) =>
    [undefined, ...discountTypes].map((type) => {
      const originalCosts = costs ?? 0.0;
      const discountPercent = (type?.discountPercent ?? 0) / 100;
      const discountedCosts = originalCosts - originalCosts * discountPercent;
      const ticketCount = ticketCounts[type?.id ?? ''] ?? 0;

      return {
        discount: type,
        costs: discountedCosts,
        originalCosts: originalCosts,
        amount: ticketCount,
        sum: ticketCount * discountedCosts,
      };
    })
);

export const selectTotalTickets = createSelector(selectTickets, (tickets) =>
  tickets.reduce(
    (result, ticket) => ({
      amount: result.amount + ticket.amount,
      sum: result.sum + ticket.sum,
    }),
    { sum: 0, amount: 0 }
  )
);

export const selectCanAddToCart = createSelector(
  selectTotalTickets,
  (total) => total.amount > 0
);

function getDaysBetweenDates(
  startDate: Date,
  endDate: Date
): { date: Date; available: boolean }[][] {
  const oneDayInMilliseconds = 24 * 60 * 60 * 1000;
  const daysDiff = Math.floor(
    (endDate.getTime() - startDate.getTime()) / oneDayInMilliseconds
  );

  const weeksArray = Array.from(
    { length: Math.ceil((daysDiff + 1) / 7) },
    (_, index) => {
      const startOfWeek = new Date(startDate);
      const startOfWeekTime =
        startDate.getTime() +
        (index * 7 - ((startDate.getDay() + 6) % 7)) * oneDayInMilliseconds;
      startOfWeek.setTime(startOfWeekTime);

      const endOfWeek = new Date(startOfWeek);
      endOfWeek.setDate(startOfWeek.getDate() + 6);
      const weekDays = Array.from({ length: 7 }, (_, dayIndex) => {
        const currentDate = new Date(startOfWeek);
        currentDate.setDate(startOfWeek.getDate() + dayIndex);
        return {
          date: currentDate,
          available: currentDate >= startDate && currentDate <= endDate,
        };
      });

      return weekDays;
    }
  );

  return weeksArray;
}

export const selectEventPrimaryCareDays = createSelector(
  selectEvent,
  (event) => {
    if (!event) {
      return [];
    }

    const weeks = getDaysBetweenDates(
      new Date(event.carePeriodCommitted.period.startDate),
      new Date(event.carePeriodCommitted.period.endDate)
    );

    return weeks.map((week) =>
      week.map((day) => {
        const careDay = event.carePeriodCommitted?.careDays.find(
          (x) => new Date(x.day).getTime() == day.date.getTime()
        );

        return {
          ...day,
          enabled: careDay !== undefined,
        };
      })
    );
  }
);
