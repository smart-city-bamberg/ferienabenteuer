import { createActionGroup, emptyProps, props } from '@ngrx/store';

export const RegistrationActions = createActionGroup({
  source: 'Registration',
  events: {
    'Register Custodian': emptyProps(),
    'Register Custodian Success': emptyProps(),
    'Register Custodian Rejected': emptyProps(),
    'Register Custodian User Email Exists': emptyProps(),
    'Register Custodian Must Agree DataProtection': emptyProps(),
    'Register Custodian Failure': props<{ error: unknown }>(),
    'Reset Form': emptyProps(),
  },
});
