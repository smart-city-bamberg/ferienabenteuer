import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  catchError,
  map,
  concatMap,
  withLatestFrom,
  tap,
  filter,
} from 'rxjs/operators';
import { Observable, EMPTY, of } from 'rxjs';
import { RegistrationActions } from './registration.actions';
import { Store } from '@ngrx/store';
import { RegisterGQL } from 'src/app/graphql/generated';
import { selectRegistrationForm } from './registration.selectors';
import { Router } from '@angular/router';
import { routerNavigatedAction } from '@ngrx/router-store';

@Injectable()
export class RegistrationEffects {
  loadRegistrations$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(RegistrationActions.registerCustodian),
      withLatestFrom(this.store.select(selectRegistrationForm)),
      concatMap(([action, form]) =>
        !form.isValid
          ? of(RegistrationActions.registerCustodianRejected())
          : this.registerMutation
              .mutate({
                data: {
                  ...form.value,
                },
              })
              .pipe(
                map((response) => {
                  const register = response.data?.register;

                  if (register?.__typename == 'RegisterSuccess') {
                    return RegistrationActions.registerCustodianSuccess();
                  }

                  if (register?.__typename == 'RegisterFailed') {
                    if (register.reason == 'UserEmailExists') {
                      return RegistrationActions.registerCustodianUserEmailExists();
                    }

                    if (register.reason == 'MustAgreeDataProtection') {
                      return RegistrationActions.registerCustodianMustAgreeDataProtection();
                    }
                  }

                  throw new Error('no result');
                }),
                catchError((error) =>
                  of(RegistrationActions.registerCustodianFailure({ error }))
                )
              )
      )
    );
  });

  redirectToLoginAfterRegisterSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(RegistrationActions.registerCustodianSuccess),
        tap(() => this.router.navigate(['/registriert']))
      ),
    {
      dispatch: false,
    }
  );

  loadEventsAfterNavigate$ = createEffect(() =>
    this.actions$.pipe(
      ofType(routerNavigatedAction),
      filter((action) => action.payload.routerState.url === '/registrieren'),
      map((action) => RegistrationActions.resetForm())
    )
  );

  constructor(
    private actions$: Actions,
    private store: Store,
    private router: Router,
    private registerMutation: RegisterGQL
  ) {}
}
