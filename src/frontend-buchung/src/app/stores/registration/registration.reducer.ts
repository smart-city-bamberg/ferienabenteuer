import { createFeature, createReducer, on } from '@ngrx/store';
import { RegistrationActions } from './registration.actions';
import {
  FormGroupState,
  createFormGroupState,
  onNgrxForms,
  updateGroup,
  validate,
  wrapReducerWithFormStateUpdate,
} from 'ngrx-forms';
import {
  email,
  minLength,
  required,
  pattern,
  requiredTrue,
} from 'ngrx-forms/validation';

export const registrationFeatureKey = 'registration';
const formularId = 'registration_form';

interface Registration {
  email: string;
  password: string;
  salutation: string;
  firstname: string;
  surname: string;
  addressCity: string;
  addressZip: string;
  addressStreet: string;
  addressHouseNumber: string;
  addressAdditional: string;
  phone: string;
  emergencyPhone: string;
  agreeDataProtection: boolean;
}

export interface State {
  registrationForm: FormGroupState<Registration>;
}

export const initialState: State = {
  registrationForm: createFormGroupState(formularId, {
    email: '',
    password: '',
    salutation: '',
    firstname: '',
    surname: '',
    addressCity: '',
    addressZip: '',
    addressStreet: '',
    addressHouseNumber: '',
    addressAdditional: '',
    phone: '',
    emergencyPhone: '',
    agreeDataProtection: false,
  }),
};

export const reducer = wrapReducerWithFormStateUpdate(
  createReducer(
    initialState,
    on(RegistrationActions.registerCustodian, (state) => state),
    on(RegistrationActions.registerCustodianSuccess, (state, action) => state),
    on(RegistrationActions.registerCustodianFailure, (state, action) => state),
    on(RegistrationActions.resetForm, (state) => ({
      ...initialState,
    })),
    onNgrxForms()
  ),
  (state) => state.registrationForm,
  (state) =>
    updateGroup<Registration>({
      email: validate(required, email),
      password: validate(
        required,
        pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{12,}$/)
      ),
      salutation: validate(required),
      firstname: validate(required, minLength(1)),
      surname: validate(required, minLength(1)),
      addressCity: validate(required, minLength(2)),
      addressZip: validate(required, pattern(/^[0-9]{5}$/)),
      addressStreet: validate(required, minLength(3)),
      addressHouseNumber: validate(required, minLength(1)),
      phone: validate(required, pattern(/^[0-9,+,(), ,]{1,}(,[0-9]+){0,}$/)),
      emergencyPhone: validate(
        required,
        pattern(/^[0-9,+,(), ,]{1,}(,[0-9]+){0,}$/)
      ),
      agreeDataProtection: validate<boolean>(requiredTrue),
    })(state)
);

export const registrationFeature = createFeature({
  name: registrationFeatureKey,
  reducer,
});
