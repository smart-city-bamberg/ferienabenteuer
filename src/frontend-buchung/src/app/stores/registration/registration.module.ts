import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import * as fromRegistration from './registration.reducer';
import { EffectsModule } from '@ngrx/effects';
import { RegistrationEffects } from './registration.effects';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StoreModule.forFeature(fromRegistration.registrationFeatureKey, fromRegistration.reducer),
    EffectsModule.forFeature([RegistrationEffects])
  ]
})
export class RegistrationModule { }
