import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromDiscounts from './discounts.reducer';

export const selectDiscountsState = createFeatureSelector<fromDiscounts.State>(
  fromDiscounts.discountsFeatureKey
);

export const selectSelectedSeason = createSelector(
  selectDiscountsState,
  (state) => state.year
);

export const selectDiscountTypes = createSelector(
  selectDiscountsState,
  (state) => state.discountTypes
);

export const selectSeasons = createSelector(
  selectDiscountsState,
  (state) => state.seasons
);

export const selectDiscounts = createSelector(
  selectDiscountsState,
  (state) => state.discounts
);

export const selectDiscountsForSeason = createSelector(
  selectDiscounts,
  selectSelectedSeason,
  (discounts, year) =>
    discounts
      .filter((discount) => discount.season.year === year)
      .map((discount) => ({
        ...discount,
        redeemCount: discount.vouchers.filter(
          (voucher) => voucher.ticket !== null
        ).length,
      }))
);

export const selectHasDiscounts = createSelector(
  selectDiscounts,
  (discounts) => discounts.length > 0
);
