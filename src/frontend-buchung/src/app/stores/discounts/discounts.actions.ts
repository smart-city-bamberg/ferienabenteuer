import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { Discount, DiscountType, Season } from './discounts.types';

export const DiscountsActions = createActionGroup({
  source: 'Discounts',
  events: {
    'Load Discounts': emptyProps(),
    'Load Discounts Success': props<{
      discounts: Discount[];
      discountTypes: DiscountType[];
      seasons: Season[];
    }>(),
    'Load Discounts Failure': props<{ error: unknown }>(),

    'Apply For Discount': props<{ discountType: string; child: string }>(),
    'Apply For Discount Success': props<{
      discount: Discount;
    }>(),
    'Apply For Discount Already Applied For Maximum Children': emptyProps(),
    'Apply For Discount Already Applied For Child': emptyProps(),
    'Apply For Discount Already Applied Maximum For Child': emptyProps(),
    'Apply For Discount Failure': props<{ error: unknown }>(),

    'Cancel Discount': props<{
      discountId: string;
    }>(),
    'Cancel Discount Success': props<{
      discountId: string;
    }>(),
    'Cancel Discount Rejected': emptyProps(),
    'Cancel Discount Cannot Cancel Approved': emptyProps(),
    'Cancel Discount Failure': props<{ error: unknown }>(),

    'Select Year': props<{ year: number }>(),
  },
});
