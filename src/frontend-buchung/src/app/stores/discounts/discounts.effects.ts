import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  catchError,
  map,
  concatMap,
  filter,
  mergeMap,
  withLatestFrom,
} from 'rxjs/operators';
import { of } from 'rxjs';
import { DiscountsActions } from './discounts.actions';
import {
  ApplyForDiscountGQL,
  CancelDiscountGQL,
  DiscountsGQL,
} from 'src/app/graphql/generated';
import { Discount, DiscountType, Season } from './discounts.types';
import { routerNavigatedAction } from '@ngrx/router-store';
import { Store } from '@ngrx/store';
import { ChildrenActions } from '../children/children.actions';
import { selectSelectedSeason } from './discounts.selectors';

@Injectable()
export class DiscountsEffects {
  loadDiscounts$ = createEffect(() =>
    this.actions$.pipe(
      ofType(DiscountsActions.loadDiscounts),
      concatMap(() =>
        this.discountsQuery.fetch().pipe(
          map((response) =>
            DiscountsActions.loadDiscountsSuccess({
              discounts: response.data.discounts as Discount[],
              discountTypes: response.data.discountTypes as DiscountType[],
              seasons: response.data.seasons as Season[],
            })
          ),
          catchError((error) =>
            of(DiscountsActions.loadDiscountsFailure({ error }))
          )
        )
      )
    )
  );

  applyForDiscount$ = createEffect(() =>
    this.actions$.pipe(
      ofType(DiscountsActions.applyForDiscount),
      withLatestFrom(this.store.select(selectSelectedSeason)),
      concatMap(([action, year]) =>
        this.applyForDiscountMutation
          .mutate({
            childId: action.child,
            discountTypeId: action.discountType,
            year,
          })
          .pipe(
            map((response) => {
              if (
                response.data?.applyForDiscount.__typename ===
                'ApplyForDiscountSuccess'
              ) {
                return DiscountsActions.applyForDiscountSuccess({
                  discount: response.data.applyForDiscount.discount as Discount,
                });
              }

              if (
                response.data?.applyForDiscount.__typename ===
                'ApplyForDiscountFailed'
              ) {
                if (
                  response.data.applyForDiscount.reason ===
                  'AlreadyAppliedForChild'
                ) {
                  return DiscountsActions.applyForDiscountAlreadyAppliedForChild();
                }

                if (
                  response.data.applyForDiscount.reason ===
                  'AlreadyAppliedMaximumDiscountsForChild'
                ) {
                  return DiscountsActions.applyForDiscountAlreadyAppliedMaximumForChild();
                }

                if (
                  response.data.applyForDiscount.reason ===
                  'AlreadyAppliedForMaximumChildren'
                ) {
                  return DiscountsActions.applyForDiscountAlreadyAppliedForMaximumChildren();
                }
              }

              throw new Error('no result');
            }),
            catchError((error) =>
              of(DiscountsActions.applyForDiscountFailure({ error }))
            )
          )
      )
    )
  );

  cancelDiscount$ = createEffect(() =>
    this.actions$.pipe(
      ofType(DiscountsActions.cancelDiscount),
      concatMap((action) =>
        !confirm('Möchten Sie diese Ermäßigung wirklich stornieren?')
          ? of(DiscountsActions.cancelDiscountRejected())
          : this.cancelDiscountMutation
              .mutate({
                discountId: action.discountId,
              })
              .pipe(
                map((response) => {
                  if (
                    response.data?.cancelDiscount.__typename ===
                    'CancelDiscountSuccess'
                  ) {
                    return DiscountsActions.cancelDiscountSuccess({
                      discountId: response.data.cancelDiscount.discount
                        .id as string,
                    });
                  }

                  if (
                    response.data?.cancelDiscount.__typename ===
                    'CancelDiscountFailed'
                  ) {
                    if (
                      response.data.cancelDiscount.reason ===
                      'CannotCancelApprovedDiscount'
                    ) {
                      return DiscountsActions.cancelDiscountCannotCancelApproved();
                    }
                  }

                  throw new Error('no result');
                }),
                catchError((error) =>
                  of(DiscountsActions.cancelDiscountFailure({ error }))
                )
              )
      )
    )
  );

  loadDiscountsAfterNavigate$ = createEffect(() =>
    this.actions$.pipe(
      ofType(routerNavigatedAction),
      filter((action) =>
        ['/ermaessigungen', '/checkout/ermaessigungen'].some(
          (url) => action.payload.routerState.url === url
        )
      ),
      mergeMap((action) => [DiscountsActions.loadDiscounts()])
    )
  );

  loadChildrenWhenLoadingDiscounts$ = createEffect(() =>
    this.actions$.pipe(
      ofType(DiscountsActions.loadDiscounts),
      map((action) => ChildrenActions.loadChildren())
    )
  );

  loadDiscountsWhenAppliedForDiscount$ = createEffect(() =>
    this.actions$.pipe(
      ofType(DiscountsActions.applyForDiscountSuccess),
      map((action) => DiscountsActions.loadDiscounts())
    )
  );

  constructor(
    private actions$: Actions,
    private store: Store,
    private discountsQuery: DiscountsGQL,
    private applyForDiscountMutation: ApplyForDiscountGQL,
    private cancelDiscountMutation: CancelDiscountGQL
  ) {}
}
