export interface Discount {
  id: string;
  status: string;
  submissionDate: string;
  rejectionReason: string;
  validUntil: string;
  redeemLimit: number;
  type: {
    id: string;
    name: string;
  };
  child: {
    id: string;
    firstname: string;
    surname: string;
  };
  vouchers: {
    id: string;
    redemptionDate: Date | null;
    ticket: {
      id: string;
    } | null;
  }[];
  season: Season;
}

export interface Season {
  id: string;
  year: number;
}

export interface DiscountType {
  id: string;
  name: string;
  discountPercent: number;
  description: string;
  amountEvents: number;
  amountChildren: number;
  amountEventsDescription: string;
  amountChildrenDescription: string;
}
