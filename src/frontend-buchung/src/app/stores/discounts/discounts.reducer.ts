import { createFeature, createReducer, on } from '@ngrx/store';
import { DiscountsActions } from './discounts.actions';
import { Discount, DiscountType, Season } from './discounts.types';
import { number } from 'ngrx-forms/validation';

export const discountsFeatureKey = 'discounts';

export interface State {
  discounts: Discount[];
  discountTypes: DiscountType[];
  seasons: Season[];
  year: number;
}

export const initialState: State = {
  discounts: [],
  discountTypes: [],
  seasons: [],
  year: 2024,
};

export const reducer = createReducer(
  initialState,
  on(DiscountsActions.loadDiscounts, (state) => state),
  on(DiscountsActions.loadDiscountsSuccess, (state, action) => ({
    ...state,
    discounts: [...action.discounts],
    discountTypes: [...action.discountTypes],
    seasons: [...action.seasons],
  })),
  on(DiscountsActions.applyForDiscountSuccess, (state, action) => ({
    ...state,
    discounts: [...state.discounts, action.discount],
  })),
  on(DiscountsActions.cancelDiscountSuccess, (state, action) => ({
    ...state,
    discounts: state.discounts.filter(
      (discount) => discount.id !== action.discountId
    ),
  })),
  on(DiscountsActions.loadDiscountsFailure, (state, action) => state),
  on(DiscountsActions.selectYear, (state, action) => ({
    ...state,
    year: action.year,
  }))
);

export const discountsFeature = createFeature({
  name: discountsFeatureKey,
  reducer,
});
