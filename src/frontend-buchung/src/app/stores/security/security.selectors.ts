import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromSecurity from './security.reducer';

export const selectSecurityState = createFeatureSelector<fromSecurity.State>(
  fromSecurity.securityFeatureKey
);

export const selectChangePasswordForm = createSelector(
  selectSecurityState,
  (state) => state.changePasswordForm
);

export const selectShowDeleteAccountDialog = createSelector(
  selectSecurityState,
  (state) => state.showDeleteAccountDialog
);
