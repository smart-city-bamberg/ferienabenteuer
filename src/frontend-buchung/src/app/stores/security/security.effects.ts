import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  catchError,
  map,
  concatMap,
  withLatestFrom,
  tap,
} from 'rxjs/operators';
import { Observable, EMPTY, of } from 'rxjs';
import { SecurityActions } from './security.actions';
import { Store } from '@ngrx/store';
import { ChangePasswordGQL, DeleteAccountGQL } from 'src/app/graphql/generated';
import { selectChangePasswordForm } from './security.selectors';
import { Router } from '@angular/router';
import { AuthActions } from '../auth/auth.actions';

@Injectable()
export class SecurityEffects {
  changePassword = createEffect(() =>
    this.actions$.pipe(
      ofType(SecurityActions.changePassword),
      withLatestFrom(this.store.select(selectChangePasswordForm)),
      concatMap(([action, form]) =>
        form.value.newPassword !== form.value.newPasswordRepeat
          ? of(SecurityActions.changePasswordRepeatMustEqual())
          : !form.isValid
          ? of(SecurityActions.changePasswordRejected())
          : this.changePasswordMutation
              .mutate({
                currentPassword: form.value.currentPassword,
                newPassword: form.value.newPassword,
              })
              .pipe(
                map((response) => {
                  if (
                    response.data?.changePassword.__typename ==
                    'ChangePasswordSuccess'
                  ) {
                    return SecurityActions.changePasswordSuccess();
                  }

                  if (
                    response.data?.changePassword.__typename ==
                    'ChangePasswordFailed'
                  ) {
                    if (
                      response.data?.changePassword.reason == 'InvalidPassword'
                    ) {
                      return SecurityActions.changePasswordInvalidPassword();
                    }

                    if (
                      response.data?.changePassword.reason ==
                      'PasswordMustNotEqualCurrentPassword'
                    ) {
                      return SecurityActions.changePasswordMustNotEqualOldPassword();
                    }

                    if (
                      response.data?.changePassword.reason ==
                      'PasswordPolicyNotMatched'
                    ) {
                      return SecurityActions.changePasswordPolicyNotMatched();
                    }
                  }

                  throw new Error('no result');
                }),
                catchError((error) =>
                  of(SecurityActions.changePasswordFailure({ error }))
                )
              )
      )
    )
  );

  deleteAccount$ = createEffect(() =>
    this.actions$.pipe(
      ofType(SecurityActions.deleteAccount),
      concatMap((action) =>
        this.deleteAccountMutation.mutate().pipe(
          map((response) => SecurityActions.deleteAccountSuccess()),
          catchError((error) =>
            of(SecurityActions.deleteAccountFailure({ error }))
          )
        )
      )
    )
  );

  redirectToDeleteAccountSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(SecurityActions.deleteAccountSuccess),
        tap(() => this.router.navigate(['/konto-geloescht']))
      ),
    {
      dispatch: false,
    }
  );

  logoutAfterDeleteAccountSuccess$ = createEffect(() =>
    this.actions$.pipe(
      ofType(SecurityActions.deleteAccountSuccess),
      map((action) => AuthActions.logout())
    )
  );

  constructor(
    private actions$: Actions,
    private store: Store,
    private router: Router,
    private changePasswordMutation: ChangePasswordGQL,
    private deleteAccountMutation: DeleteAccountGQL
  ) {}
}
