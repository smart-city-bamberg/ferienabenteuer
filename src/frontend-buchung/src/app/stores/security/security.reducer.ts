import { createFeature, createReducer, on } from '@ngrx/store';
import { SecurityActions } from './security.actions';
import {
  FormGroupState,
  createFormGroupState,
  onNgrxForms,
  updateGroup,
  validate,
  wrapReducerWithFormStateUpdate,
} from 'ngrx-forms';
import { equalTo, minLength, pattern, required } from 'ngrx-forms/validation';

export const securityFeatureKey = 'security';

export interface ChangePassword {
  currentPassword: string;
  newPasswordRepeat: string;
  newPassword: string;
}

export interface State {
  changePasswordForm: FormGroupState<ChangePassword>;
  showDeleteAccountDialog: boolean;
}

const changePasswordFormId = 'change_password';

export const initialState: State = {
  changePasswordForm: createFormGroupState<ChangePassword>(
    changePasswordFormId,
    {
      currentPassword: '',
      newPasswordRepeat: '',
      newPassword: '',
    }
  ),
  showDeleteAccountDialog: false,
};

export const reducer = wrapReducerWithFormStateUpdate(
  createReducer(
    initialState,
    on(SecurityActions.changePassword, (state) => state),
    on(SecurityActions.changePasswordSuccess, (state, action) => state),
    on(SecurityActions.changePasswordFailure, (state, action) => state),
    on(SecurityActions.showDeleteAccountDialog, (state, action) => ({
      ...state,
      showDeleteAccountDialog: true,
    })),
    on(
      SecurityActions.deleteAccount,
      SecurityActions.deleteAccountRejected,
      (state, action) => ({
        ...state,
        showDeleteAccountDialog: false,
      })
    ),
    onNgrxForms()
  ),
  (state) => state.changePasswordForm,
  (state) =>
    updateGroup<ChangePassword>({
      currentPassword: validate(required, minLength(1)),
      newPassword: validate(
        required,
        pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{12,}$/)
      ),
      newPasswordRepeat: (x, y) =>
        validate(equalTo(y.controls.newPassword.value), required)(x),
    })(state)
);

export const securityFeature = createFeature({
  name: securityFeatureKey,
  reducer,
});
