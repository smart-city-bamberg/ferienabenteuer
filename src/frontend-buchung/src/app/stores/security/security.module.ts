import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import * as fromSecurity from './security.reducer';
import { EffectsModule } from '@ngrx/effects';
import { SecurityEffects } from './security.effects';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StoreModule.forFeature(fromSecurity.securityFeatureKey, fromSecurity.reducer),
    EffectsModule.forFeature([SecurityEffects])
  ]
})
export class SecurityModule { }
