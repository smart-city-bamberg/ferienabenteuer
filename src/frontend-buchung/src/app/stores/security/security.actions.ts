import { createActionGroup, emptyProps, props } from '@ngrx/store';

export const SecurityActions = createActionGroup({
  source: 'Security',
  events: {
    'Change Password': emptyProps(),
    'Change Password Rejected': emptyProps(),
    'Change Password Repeat Must Equal': emptyProps(),
    'Change Password Success': emptyProps(),
    'Change Password Invalid Password': emptyProps(),
    'Change Password Policy Not Matched': emptyProps(),
    'Change Password Must Not Equal Old Password': emptyProps(),
    'Change Password Failure': props<{ error: unknown }>(),

    'Delete Account': emptyProps(),
    'Delete Account Success': emptyProps(),
    'Delete Account Failure': props<{ error: unknown }>(),
    'Delete Account Rejected': emptyProps(),
    'Show Delete Account Dialog': emptyProps(),
  },
});
