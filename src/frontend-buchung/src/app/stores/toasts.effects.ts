import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { tap, OperatorFunction } from 'rxjs';
import { Action } from '@ngrx/store';
import { ToastrService } from 'ngx-toastr';
import { CheckoutActions } from './checkout/checkout.actions';
import { EventActions } from './event/event.actions';
import { DiscountsActions } from './discounts/discounts.actions';
import { ChildrenActions } from './children/children.actions';
import { CustodianActions } from './custodian/custodian.actions';
import { RegistrationActions } from './registration/registration.actions';
import { AuthActions } from './auth/auth.actions';
import { VerificationActions } from './verification/verification.actions';
import { SecurityActions } from './security/security.actions';
import { PasswordResetActions } from './password-reset/password-reset.actions';

@Injectable()
export class ToastEffects {
  buildToast = <A>(
    actionFilter: OperatorFunction<Action, A>,
    toastClass: string,
    message: (action: A) => string
  ) =>
    createEffect(
      () =>
        this.actions$.pipe(
          actionFilter,
          tap((action) =>
            this.toaster.show(message(action), undefined, {
              positionClass: 'responsive-toast-position',
              toastClass: 'ngx-toastr ' + toastClass,
              timeOut: 3000,
            })
          )
        ),
      {
        dispatch: false,
      }
    );

  showToastWhenTicketAddedToCartFailure$ = this.buildToast(
    ofType(CheckoutActions.addTicketFailure),
    'toast-error',
    (action) => 'Ticket konnte nicht in den Warenkorb gelegt werden'
  );

  showToastWhenTicketAddedToCartNotEnoughTicketsAvailable$ = this.buildToast(
    ofType(CheckoutActions.addTicketNoTicketAvailable),
    'toast-warning',
    (action) => 'Es steht leider kein weiteres Ticket zur Verfügung'
  );

  showToastWhenTicketAddedToCartMaximumChildrenCount$ = this.buildToast(
    ofType(CheckoutActions.addTicketMaximumChildrenCount),
    'toast-warning',
    (action) =>
      'Es können je Veranstaltung nur so viele Tickets gebucht werden wie Kinder eingetragen wurden'
  );

  showToastWhenTicketAddedToCartBookingNotStarted$ = this.buildToast(
    ofType(CheckoutActions.addTicketBookingNotStarted),
    'toast-warning',
    (action) => 'Erst nach dem Buchungsstart können Tickets gebucht werden'
  );

  showToastWhenCartExpiring$ = this.buildToast(
    ofType(CheckoutActions.cartExpiring),
    'toast-warning',
    (action) => `Der Warenkorb läuft in Kürze ab`
  );

  showToastWhenExtendTicketTermSuccess$ = this.buildToast(
    ofType(CheckoutActions.extendCartTermSuccess),
    'toast-success',
    (action) =>
      `Laufzeit des Warenkorbs wurde verlängert. ${
        10 - action.cart.termExtensionCount
      } weite Verlängerungen möglich`
  );

  showToastWhenExtendTicketTermNotEnoughTicketsAvailable$ = this.buildToast(
    ofType(CheckoutActions.extendCartTermMaximumExtensionsReached),
    'toast-warning',
    (action) => 'Laufzeit des Warenkorbs kann nicht erneut verlängert werden'
  );

  showToastWhenExtendTicketTermMaximumChildrenCount$ = this.buildToast(
    ofType(CheckoutActions.extendCartTermFailure),
    'toast-error',
    (action) => 'Laufzeit des Warenkorbs konnte nicht verlängert werden'
  );

  showToastWhenTicketsAddedToCartNotEnoughTicketsAvailable$ = this.buildToast(
    ofType(EventActions.addTicketsToCartNoTicketAvailable),
    'toast-warning',
    (action) => 'Es stehen nich genügend Tickets zur Verfügung'
  );

  showToastWhenTicketsAddedToCartMaximumChildrenCount$ = this.buildToast(
    ofType(EventActions.addTicketsToCartMaximumChildrenCount),
    'toast-warning',
    (action) =>
      'Es können je Veranstaltung nur so viele Tickets gebucht werden wie Kinder eingetragen wurden'
  );

  showToastWhenTicketsAddedToCartNotLoggedIn$ = this.buildToast(
    ofType(EventActions.addTicketsToCartNotLoggedIn),
    'toast-info',
    (action) =>
      'Um Tickets in den Warenkorb legen zu können, müssen Sie sich zuerst anmelden'
  );

  showToastWhenTicketsAddedToCartBookingNotStarted$ = this.buildToast(
    ofType(EventActions.addTicketsToCartBookingNotStarted),
    'toast-info',
    (action) => 'Erst nach dem Buchungsstart können Tickets gebucht werden'
  );

  showToastWhenTicketsAddedToCartSuccess$ = this.buildToast(
    ofType(EventActions.addTicketsToCartSuccess),
    'toast-success',
    (action) => 'Tickets in den Warenkorb gelegt'
  );

  showToastWhenTicketsAddedToCartFailure$ = this.buildToast(
    ofType(EventActions.addTicketsToCartFailure),
    'toast-error',
    (action) => 'Tickets konnten nicht in den Warenkorb gelegt werden'
  );

  showToastWhenTicketsRemovedFromCartSuccess$ = this.buildToast(
    ofType(CheckoutActions.removeAllEventTicketsFromCartSuccess),
    'toast-success',
    (action) => 'Tickets aus dem Warenkorb entfernt'
  );

  showToastWhenTicketsRemovedFromCartFailure$ = this.buildToast(
    ofType(CheckoutActions.removeAllEventTicketsFromCartFailure),
    'toast-error',
    (action) => 'Tickets konnten nicht aus dem Warenkorb entfernt werden'
  );

  showToastWhenCartValidityExpired$ = this.buildToast(
    ofType(CheckoutActions.cartValidityExpired),
    'toast-warning',
    (action) => 'Der Warenkorb ist abgelaufen und wurde zurückgesetzt'
  );

  showToastWhenApplyForDiscountSuccess$ = this.buildToast(
    ofType(DiscountsActions.applyForDiscountSuccess),
    'toast-success',
    (action) => 'Ermäßigung wurde beantragt'
  );

  showToastWhenApplyForDiscountAlreadyAppliedForChild$ = this.buildToast(
    ofType(DiscountsActions.applyForDiscountAlreadyAppliedForChild),
    'toast-warning',
    (action) => 'Diese Art Ermäßigung wurde bereits für dieses Kind beantragt'
  );

  showToastWhenApplyForDiscountAlreadyAppliedMaximumForChild$ = this.buildToast(
    ofType(DiscountsActions.applyForDiscountAlreadyAppliedMaximumForChild),
    'toast-warning',
    (action) =>
      'Es wurde bereits die maximale Anzahl an Ermäßigungen für das Kinder beantragt'
  );

  showToastWhenApplyForDiscountAlreadyAppliedForMaximumChildren$ =
    this.buildToast(
      ofType(DiscountsActions.applyForDiscountAlreadyAppliedForMaximumChildren),
      'toast-warning',
      (action) =>
        'Diese Art Ermäßigung wurde bereits für die maximale Anzahl Kinder beantragt'
    );

  showToastWhenApplyForDiscountFailure$ = this.buildToast(
    ofType(DiscountsActions.applyForDiscountFailure),
    'toast-error',
    (action) => 'Ermäßigung konnte nicht beantragt werden'
  );

  showToastWhenAssignChildToTicketAlreadyAssignedForEvent$ = this.buildToast(
    ofType(CheckoutActions.assignChildToTicketChildAlreadyAssignedForEvent),
    'toast-warning',
    (action) => 'Kind ist bereits dieser Veranstaltung zugewiesen'
  );

  showToastWhenAssignChildToTicketAlreadyBookedForEvent$ = this.buildToast(
    ofType(CheckoutActions.assignChildToTicketChildAlreadyBookedForEvent),
    'toast-warning',
    (action) =>
      'Für das Kind ist für diese Veranstaltung bereits ein Ticket gebucht oder reserviert'
  );

  showToastWhenAssignChildToTicketFailure$ = this.buildToast(
    ofType(CheckoutActions.assignChildToTicketFailure),
    'toast-error',
    (action) => 'Kind konnte dem Ticket nicht zugewiesen werden'
  );

  showToastWhenBookCartTicketsFailure$ = this.buildToast(
    ofType(CheckoutActions.bookCartTicketsFailure),
    'toast-error',
    (action) => 'Tickets konnten nicht gebucht werden'
  );

  showToastWhenBookCartTicketsRejected$ = this.buildToast(
    ofType(CheckoutActions.bookCartTicketsRejected),
    'toast-info',
    (action) =>
      'Bitte bestätigen Sie, dass Sie die AGB und die Datenschutz- sowie Widerrufsbelehrung akzeptieren'
  );

  showToastWhenBookCartTicketsNotAllTicketsAssignedChild$ = this.buildToast(
    ofType(CheckoutActions.bookCartTicketsNotAllTicketsAssignedChild),
    'toast-warning',
    (action) => 'Jedem Ticket muss ein Kind zugewiesen werden'
  );

  showToastWhenBookCartTicketsNoDiscountVoucherAvailable$ = this.buildToast(
    ofType(CheckoutActions.bookCartTicketsNoDiscountVoucherAvailable),
    'toast-warn',
    (action) => 'Es sind nicht ausreichend Ermäßigungsscheine vorhanden'
  );

  showToastWhenSaveChildSuccess$ = this.buildToast(
    ofType(ChildrenActions.saveChildSuccess),
    'toast-success',
    (action) => 'Kind wurde gespeichert'
  );

  showToastWhenSaveChildRejected$ = this.buildToast(
    ofType(ChildrenActions.saveChildRejected),
    'toast-warning',
    (action) => 'Bitte alle Pflichtfelder ausfüllen, um das Kind zu speichern'
  );

  showToastWhenSaveChildFailure$ = this.buildToast(
    ofType(ChildrenActions.saveChildFailure),
    'toast-error',
    (action) => 'Kind konnte nicht gespeichert werden'
  );

  showToastWhenUpdateCustodianSuccess$ = this.buildToast(
    ofType(CustodianActions.updateCustodianSuccess),
    'toast-success',
    (action) => 'Persönliche Informationen gespeichert'
  );

  showToastWhenUpdateCustodianRejected$ = this.buildToast(
    ofType(CustodianActions.updateCustodianRejected),
    'toast-info',
    (action) => 'Bitte alle Pflichtfelder ausfüllen'
  );

  showToastWhenUpdateFailure$ = this.buildToast(
    ofType(CustodianActions.updateCustodianFailure),
    'toast-warning',
    (action) => 'Persönliche Informationen konnten nicht gespeichert werden'
  );

  showToastWhenRegisterSuccess$ = this.buildToast(
    ofType(RegistrationActions.registerCustodianSuccess),
    'toast-success',
    (action) => 'Erfolgreich registriert'
  );

  showToastWhenRegisterUserEmailExists$ = this.buildToast(
    ofType(RegistrationActions.registerCustodianUserEmailExists),
    'toast-warning',
    (action) => 'Die E-Mail-Adresse ist bereits registriert'
  );

  showToastWhenRegisterMustAgreeDataProtection$ = this.buildToast(
    ofType(RegistrationActions.registerCustodianMustAgreeDataProtection),
    'toast-warning',
    (action) =>
      'Sie müssen der Widerrufsbelehrung und der Datenschutzerklärung zustimmen'
  );

  showToastWhenRegisterRejected$ = this.buildToast(
    ofType(RegistrationActions.registerCustodianRejected),
    'toast-info',
    (action) => 'Bitte alle Pflichtfelder ausfüllen'
  );

  showToastWhenRegisterFailure$ = this.buildToast(
    ofType(RegistrationActions.registerCustodianFailure),
    'toast-error',
    (action) =>
      'Fehler beim Registrieren, bitte versuchen Sie es später noch einmal'
  );

  showToastWhenRedeemEarlyAccessCodeSuccess$ = this.buildToast(
    ofType(CustodianActions.redeemEarlyAccessCodeSuccess),
    'toast-success',
    (action) => 'Frühbuchercode eingelöst'
  );

  showToastWhenRedeemEarlyAccessCodeNotRedeemed$ = this.buildToast(
    ofType(CustodianActions.redeemEarlyAccessCodeNotRedeemed),
    'toast-warning',
    (action) =>
      'Der Frühbuchercode ist ungültig und konnte nicht eingelöst werden'
  );

  showToastWhenRedeemEarlyAccessCodeFailure$ = this.buildToast(
    ofType(CustodianActions.redeemEarlyAccessCodeFailure),
    'toast-error',
    (action) =>
      'Fehler beim Einlösen, bitte versuchen Sie es später noch einmal'
  );

  showToastWhenLogoutSuccess$ = this.buildToast(
    ofType(AuthActions.logoutSuccess),
    'toast-success',
    (action) => 'Sie wurden abgemeldet'
  );

  showToastWhenVerifyAccountSuccess$ = this.buildToast(
    ofType(VerificationActions.verifyAccountSuccess),
    'toast-success',
    (action) => 'Konto aktiviert'
  );

  showToastWhenVerifyAccountInvalidToken$ = this.buildToast(
    ofType(VerificationActions.verifyAccountInvalidToken),
    'toast-warning',
    (action) => 'Konto konnte nicht aktiviert werden, der Code ist ungültig'
  );

  showToastWhenVerifyAccountFailure$ = this.buildToast(
    ofType(VerificationActions.verifyAccountFailure),
    'toast-error',
    (action) =>
      'Fehler beim Aktivieren des Kontos, bitte versuchen Sie es später noch einmal'
  );

  showToastWhenResendValidationCodeSuccess$ = this.buildToast(
    ofType(VerificationActions.sendVerifyAccountSuccess),
    'toast-success',
    (action) =>
      'Aktivierunsgcode wurde an die angegebene E-Mail-Adresse verschickt'
  );

  showToastWhenResendValidationCodeInvalidToken$ = this.buildToast(
    ofType(VerificationActions.sendVerifyAccountUserDoesNotExists),
    'toast-warning',
    (action) =>
      'Aktivierunsgcode konnte nicht verschickt werden. Die E-Mail-Adresse ist nicht registriert'
  );

  showToastWhenResendValidationCodeFailure$ = this.buildToast(
    ofType(VerificationActions.sendVerifyAccountFailure),
    'toast-error',
    (action) =>
      'Fehler beim Aktivieren des Kontos, bitte versuchen Sie es später noch einmal'
  );

  showToastWhenRequestPasswordResetTokenSuccess$ = this.buildToast(
    ofType(PasswordResetActions.requestPasswordResetTokenSuccess),
    'toast-success',
    (action) =>
      'Link zum Passwort-Reset wurde an die angegebene E-Mail-Adresse verschickt'
  );

  showToastWhenRequestPasswordResetTokenInvalidToken$ = this.buildToast(
    ofType(PasswordResetActions.requestPasswordResetTokenUserDoesNotExists),
    'toast-warning',
    (action) =>
      'Link zum Passwort-Reset konnte nicht verschickt werden. Die E-Mail-Adresse ist nicht registriert'
  );

  showToastWhenRequestPasswordResetTokenFailure$ = this.buildToast(
    ofType(PasswordResetActions.requestPasswordResetTokenFailure),
    'toast-error',
    (action) =>
      'Fehler beim Senden des Links zum Passwort-Reset, bitte versuchen Sie es später noch einmal'
  );

  showToastWhenResetPasswordSuccess$ = this.buildToast(
    ofType(PasswordResetActions.resetPasswordSuccess),
    'toast-success',
    (action) => 'Passwort aktualisiert'
  );

  showToastWhenResetPasswordRejected$ = this.buildToast(
    ofType(PasswordResetActions.resetPasswordRejected),
    'toast-info',
    (action) => 'Bitte alle Pflichtfelder ausfüllen'
  );

  showToastWhenResetPasswordRepeatMustEqual$ = this.buildToast(
    ofType(PasswordResetActions.resetPasswordRepeatMustEqual),
    'toast-warning',
    (action) =>
      'Die Wiederholung des neuen Passworts stimmt nicht mit dem neuen Passwort überein'
  );

  showToastWhenResetPasswordPolicyNotMatched$ = this.buildToast(
    ofType(PasswordResetActions.resetPasswordPolicyNotMatched),
    'toast-warning',
    (action) => 'Das Passwort erfüllt nicht die Anforderungen für Komplexität'
  );

  showToastWhenResetPasswordInvalidPassword$ = this.buildToast(
    ofType(PasswordResetActions.resetPasswordInvalidToken),
    'toast-warning',
    (action) => 'Der eingegebene Code ist nicht korrekt'
  );

  showToastWhenResetPasswordMustNotEqualOldPassword$ = this.buildToast(
    ofType(PasswordResetActions.resetPasswordMustNotEqualOldPassword),
    'toast-warning',
    (action) => 'Das neue Passwort darf nicht dem alten Passwort gleichen'
  );

  showToastWhenResetPasswordFailure$ = this.buildToast(
    ofType(PasswordResetActions.resetPasswordFailure),
    'toast-error',
    (action) =>
      'Fehler beim ändern des Passworts, bitte versuchen Sie es später noch einmal'
  );

  showToastWhenChangePasswordSuccess$ = this.buildToast(
    ofType(SecurityActions.changePasswordSuccess),
    'toast-success',
    (action) => 'Passwort aktualisiert'
  );

  showToastWhenChangePasswordRejected$ = this.buildToast(
    ofType(SecurityActions.changePasswordRejected),
    'toast-info',
    (action) => 'Bitte alle Pflichtfelder ausfüllen'
  );

  showToastWhenChangePasswordRepeatMustEqual$ = this.buildToast(
    ofType(SecurityActions.changePasswordRepeatMustEqual),
    'toast-warning',
    (action) =>
      'Die Wiederholung des neuen Passworts stimmt nicht mit dem neuen Passwort überein'
  );

  showToastWhenChangePasswordPolicyNotMatched$ = this.buildToast(
    ofType(SecurityActions.changePasswordPolicyNotMatched),
    'toast-warning',
    (action) => 'Das Passwort erfüllt nicht die Anforderungen für Komplexität'
  );

  showToastWhenChangePasswordInvalidPassword$ = this.buildToast(
    ofType(SecurityActions.changePasswordInvalidPassword),
    'toast-warning',
    (action) => 'Das eingegebene Passwort ist nicht korrekt'
  );

  showToastWhenChangePasswordMustNotEqualOldPassword$ = this.buildToast(
    ofType(SecurityActions.changePasswordMustNotEqualOldPassword),
    'toast-warning',
    (action) => 'Das neue Passwort darf nicht dem alten Passwort gleichen'
  );

  showToastWhenChangePasswordFailure$ = this.buildToast(
    ofType(SecurityActions.changePasswordFailure),
    'toast-error',
    (action) =>
      'Fehler beim Ändern des Passworts, bitte versuchen Sie es später noch einmal'
  );

  showToastWhenDeleteAccountSuccess$ = this.buildToast(
    ofType(SecurityActions.deleteAccountSuccess),
    'toast-success',
    (action) => 'Konto wurde gelöscht'
  );

  showToastWhenDeleteAccountFailure$ = this.buildToast(
    ofType(SecurityActions.deleteAccountFailure),
    'toast-error',
    (action) =>
      'Fehler beim Löschen des Kontos, bitte versuchen Sie es später noch einmal'
  );

  showToastDeleteChildSuccess$ = this.buildToast(
    ofType(ChildrenActions.deleteChildSuccess),
    'toast-success',
    (action) => 'Kind gelöscht'
  );

  showToastDeleteChildFailure$ = this.buildToast(
    ofType(ChildrenActions.deleteChildFailure),
    'toast-error',
    (action) =>
      'Fehler beim Löschen des Kindes, bitte versuchen Sie es später noch einmal'
  );

  showToastCancelDiscountSuccess$ = this.buildToast(
    ofType(DiscountsActions.cancelDiscountSuccess),
    'toast-success',
    (action) => 'Ermäßigung wurde storniert'
  );

  showToastCancelDiscountCannotCancelApproved$ = this.buildToast(
    ofType(DiscountsActions.cancelDiscountCannotCancelApproved),
    'toast-warning',
    (action) => 'Bereits bestätigte Ermäßigung können nicht storniert werden'
  );

  showToastCancelDiscountFailure$ = this.buildToast(
    ofType(DiscountsActions.cancelDiscountFailure),
    'toast-error',
    (action) =>
      'Fehler beim Stornieren der Ermäßigung, bitte versuchen Sie es später noch einmal'
  );

  constructor(private actions$: Actions, private toaster: ToastrService) {}
}
