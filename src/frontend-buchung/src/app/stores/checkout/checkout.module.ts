import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import * as fromCheckout from './checkout.reducer';
import { EffectsModule } from '@ngrx/effects';
import { CheckoutEffects } from './checkout.effects';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature(
      fromCheckout.checkoutFeatureKey,
      fromCheckout.reducer
    ),
    EffectsModule.forFeature([CheckoutEffects]),
  ],
  declarations: [],
})
export class CheckoutModule {}
