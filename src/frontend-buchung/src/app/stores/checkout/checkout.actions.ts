import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { Cart, CartTicket, Child, DiscountType } from './checkout.types';

export const CheckoutActions = createActionGroup({
  source: 'Checkout',
  events: {
    'Load CartTickets': emptyProps(),
    'Load CartTickets Success': props<{
      cart: Cart | undefined;
      children: Child[];
      discountTypes: DiscountType[];
    }>(),
    'Load CartTickets Failure': props<{ error: unknown }>(),

    'Add Ticket': props<{
      eventId: string;
      discountTypeId?: string;
    }>(),
    'Add Ticket Success': props<{
      cartTicket: CartTicket;
    }>(),
    'Add Ticket No Ticket Available': emptyProps(),
    'Add Ticket Maximum Children Count': emptyProps(),
    'Add Ticket Booking Not Started': emptyProps(),
    'Add Ticket Failure': props<{ error: unknown }>(),

    'Remove Ticket': props<{
      eventId: string;
      discountTypeId?: string;
    }>(),
    'Remove Ticket Success': props<{
      cartTicketId: string;
    }>(),
    'Remove Ticket Failure': props<{ error: unknown }>(),

    'Remove All Event Tickets From Cart': props<{
      eventId: string;
    }>(),
    'Remove All Event Tickets From Cart Success': emptyProps(),
    'Remove All Event Tickets From Cart Failure': props<{ error: unknown }>(),

    'Assign Child To Ticket': props<{
      cartTicketId: string;
      childId?: string;
    }>(),
    'Assign Child To Ticket Success': props<{
      cartTicket: CartTicket;
    }>(),
    'Assign Child To Ticket Child Already Assigned For Event': props<{
      cartTicketId: string;
      childId?: string;
    }>(),
    'Assign Child To Ticket Child Already Booked For Event': props<{
      cartTicketId: string;
      childId?: string;
    }>(),
    'Assign Child To Ticket Failure': props<{
      cartTicketId: string;
      childId?: string;
      error: unknown;
    }>(),

    'Assign Discount Type To Ticket': props<{
      cartTicketId: string;
      discountTypeId?: string;
    }>(),
    'Assign Discount Type To Ticket Success': props<{
      cartTicketId: string;
      discountType?: DiscountType;
    }>(),
    'Assign Discount Type To Ticket Failure': props<{
      cartTicketId: string;
      discountTypeId?: string;
      error: unknown;
    }>(),

    'Book CartTickets': emptyProps(),
    'Book CartTickets Success': emptyProps(),
    'Book CartTickets Rejected': emptyProps(),
    'Book CartTickets No Discount Voucher Available': emptyProps(),
    'Book CartTickets Not All Tickets Assigned Child': emptyProps(),
    'Book CartTickets Must Agree Data Protection': emptyProps(),
    'Book CartTickets Must Agree Terms And Conditions': emptyProps(),
    'Book CartTickets Failure': props<{ error: unknown }>(),

    'Cart Validity Expired': emptyProps(),
    'Clear Cart': emptyProps(),
    'Cart Expiring': props<{ remainingTime: number }>(),

    'Extend Cart Term': emptyProps(),
    'Extend Cart Term Success': props<{ cart: Cart }>(),
    'Extend Cart Term Maximum Extensions Reached': emptyProps(),
    'Extend Cart Term Failure': props<{ error: unknown }>(),
  },
});
