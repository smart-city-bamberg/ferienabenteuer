import { number } from 'ngrx-forms/validation';
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { getRouterSelectors } from '@ngrx/router-store';

import * as fromCheckout from './checkout.reducer';
import { CartTicket, Child, Event } from './checkout.types';

const { selectUrl } = getRouterSelectors();

export const selectCheckoutPage = createSelector(selectUrl, (url) =>
  url.replace('/checkout/', '')
);

export const selectCheckoutIsCart = createSelector(
  selectCheckoutPage,
  (page) => page === 'warenkorb'
);
export const selectCheckoutIsDiscounts = createSelector(
  selectCheckoutPage,
  (page) => page === 'ermaessigungen'
);
export const selectCheckoutIsParticipants = createSelector(
  selectCheckoutPage,
  (page) => page === 'teilnehmer'
);
export const selectCheckoutIsPayment = createSelector(
  selectCheckoutPage,
  (page) => page === 'kasse'
);

export const selectCheckoutState = createFeatureSelector<fromCheckout.State>(
  fromCheckout.checkoutFeatureKey
);

export const selectDiscountTypes = createSelector(
  selectCheckoutState,
  (state) => state.discountTypes
);

export const selectCart = createSelector(
  selectCheckoutState,
  (state) => state.cart
);

export const selectCartTickets = createSelector(
  selectCart,
  (cart) => cart?.cartTickets ?? []
);

export const selectChildren = createSelector(
  selectCheckoutState,
  (state) => state.children
);

export const selectCartEventsWithTickets = createSelector(
  selectCartTickets,
  selectDiscountTypes,
  (cartTickets, discountTypes) =>
    cartTickets
      .reduce(
        (result, ticket) =>
          result.some((event) => event.id === ticket.eventTicket.event.id)
            ? result
            : [...result, ticket.eventTicket.event],
        new Array<Event>()
      )
      .sort((a, b) =>
        a.carePeriodCommitted.careDays[0].day.localeCompare(
          b.carePeriodCommitted.careDays[0].day
        )
      )
      .map((event) => {
        const eventTickets = ['participant', 'waitinglist'].map(
          (ticketType) => ({
            ticketType,
            ticketGroups: [undefined, ...discountTypes]
              .map((discountType) => {
                const discountTypeTickets = cartTickets
                  .filter(
                    (ticket) =>
                      ticket.discountType?.id === discountType?.id &&
                      ticket.eventTicket.event.id === event.id &&
                      ticket.eventTicket.type === ticketType
                  )
                  .sort((a, b) => a.eventTicket.number - b.eventTicket.number);

                const originalCosts = event.costs ?? 0.0;
                const discountPercent =
                  (discountType?.discountPercent ?? 0) / 100;
                const discountedCosts =
                  originalCosts - originalCosts * discountPercent;

                return {
                  discount: discountType,
                  costs: discountedCosts,
                  originalCosts: originalCosts,
                  amount: discountTypeTickets.length,
                  sum: discountTypeTickets.length * discountedCosts,
                  ticketType,
                };
              })
              .filter((ticket) => ticket.amount > 0),
          })
        );

        return {
          id: event.id,
          event,
          ticketTypes: eventTickets,
          sum: eventTickets.reduce(
            (sum, tickets) =>
              sum +
              tickets.ticketGroups.reduce((sum, ticket) => sum + ticket.sum, 0),
            0
          ),
        };
      }) ?? []
);

export const selectCartEventsWithChildren = createSelector(
  selectCartTickets,
  selectDiscountTypes,
  selectChildren,
  (cartTickets, discountTypes, children) =>
    cartTickets
      .reduce(
        (result, ticket) =>
          result.some((event) => event.id === ticket.eventTicket.event.id)
            ? result
            : [...result, ticket.eventTicket.event],
        new Array<Event>()
      )
      .sort((a, b) =>
        a.carePeriodCommitted.careDays[0].day.localeCompare(
          b.carePeriodCommitted.careDays[0].day
        )
      )
      .map((event) => {
        const ticketGroups = ['participant', 'waitinglist']
          .map((ticketType) => ({
            ticketType,
            ticketGroups: [undefined, ...discountTypes]
              .map((discountType) => {
                const discountTypeTickets = cartTickets.filter(
                  (ticket) =>
                    ticket.discountType?.id === discountType?.id &&
                    ticket.eventTicket.event.id === event.id &&
                    ticket.eventTicket.type === ticketType
                );
                const eventCartTickets = cartTickets.filter(
                  (ticket) => ticket.eventTicket.event.id === event.id
                );
                const discountTypeTicketsWithChildren = discountTypeTickets
                  .map((ticket) => {
                    return {
                      ...ticket,
                      children: children.filter((child) => {
                        const isSelectedChild = child.id === ticket.child?.id;
                        const childDoesntAppearInAnyOtherTicket =
                          !eventCartTickets.some(
                            (ticket) => ticket.child?.id === child.id
                          );
                        const isNoDiscount = discountType === undefined;
                        const childHasDiscountAvailable = child.discounts.some(
                          (discount) =>
                            discount.type.id === discountType?.id &&
                            discount.vouchersCount > 0
                        );
                        const childHasVouchersAvailable =
                          cartTickets.filter(
                            (ticket) =>
                              ticket.child?.id === child.id &&
                              ticket.discountType?.id === discountType?.id
                          ).length <
                          (child.discounts.find(
                            (discount) => discount.type.id === discountType?.id
                          )?.vouchersCount ?? 0);
                        return (
                          (isSelectedChild ||
                            childDoesntAppearInAnyOtherTicket) &&
                          (isNoDiscount || childHasDiscountAvailable) &&
                          (isSelectedChild ||
                            isNoDiscount ||
                            childHasVouchersAvailable)
                        );
                      }),
                    };
                  })
                  .sort((a, b) => a.eventTicket.number - b.eventTicket.number);

                const originalCosts = event.costs ?? 0.0;
                const discountPercent =
                  (discountType?.discountPercent ?? 0) / 100;
                const discountedCosts =
                  originalCosts - originalCosts * discountPercent;

                return {
                  discount: discountType,
                  ticketType,
                  tickets: discountTypeTicketsWithChildren,
                  costs: discountedCosts,
                  originalCosts: originalCosts,
                  amount: discountTypeTickets.length,
                  sum: discountTypeTickets.length * discountedCosts,
                };
              })
              .filter((group) => group.amount > 0),
          }))
          .filter((group) => group.ticketGroups.length > 0);

        return {
          id: event.id,
          event,
          ticketTypes: ticketGroups,
        };
      }) ?? []
);

const childHasDiscountAvailable = (
  child: Child,
  ticket: CartTicket,
  cartTickets: CartTicket[]
) => {
  const childHasDiscountAvailable = child.discounts.some(
    (discount) =>
      discount.type.id === ticket.discountType?.id && discount.vouchersCount > 0
  );

  if (!childHasDiscountAvailable) {
    return false;
  }

  const cartTicketsWithDiscountType = cartTickets.filter(
    (cartTicket) =>
      cartTicket.child?.id === child.id &&
      cartTicket.discountType?.id === ticket.discountType?.id
  );

  const discount = child.discounts.find(
    (discount) => discount.type.id === ticket.discountType?.id
  );

  const voucherCount = discount?.vouchersCount ?? 0;

  const childHasVouchersAvailable =
    cartTicketsWithDiscountType.length < voucherCount;

  if (childHasVouchersAvailable) {
    return true;
  }

  return false;
};

export const selectCartEventsWithChildrenWithoutGroups = createSelector(
  selectCartTickets,
  selectChildren,
  (cartTickets, children) =>
    cartTickets
      .reduce(
        (result, ticket) =>
          result.some((event) => event.id === ticket.eventTicket.event.id)
            ? result
            : [...result, ticket.eventTicket.event],
        new Array<Event>()
      )
      .sort((a, b) =>
        a.carePeriodCommitted.careDays[0].day.localeCompare(
          b.carePeriodCommitted.careDays[0].day
        )
      )
      .map((event) => {
        const ticketGroups = ['participant', 'waitinglist']
          .map((ticketType) => {
            const ticketTypeTickets = cartTickets.filter(
              (ticket) =>
                ticket.eventTicket.event.id === event.id &&
                ticket.eventTicket.type === ticketType
            );
            const eventCartTickets = cartTickets.filter(
              (ticket) => ticket.eventTicket.event.id === event.id
            );
            const discountTypeTicketsWithChildren = ticketTypeTickets
              .map((ticket) => {
                return {
                  ...ticket,
                  children: children
                    .map((child) => {
                      const isDiscount = ticket.discountType !== null;

                      return {
                        ...child,
                        isBooked: child.tickets.some(
                          (bookedTicket) =>
                            bookedTicket.eventTicket.event.id ===
                            ticket.eventTicket.event.id
                        ),
                        noDiscount:
                          isDiscount &&
                          !childHasDiscountAvailable(
                            child,
                            ticket,
                            cartTickets
                          ),
                      };
                    })
                    .filter((child) => {
                      const isSelectedChild = child.id === ticket.child?.id;

                      if (isSelectedChild) {
                        return true;
                      }

                      const childAppearsInAnyOtherTicket =
                        eventCartTickets.some(
                          (cartTicket) =>
                            cartTicket.child?.id === child.id &&
                            ticket.id !== cartTicket.id
                        );

                      if (childAppearsInAnyOtherTicket) {
                        return false;
                      }

                      return true;
                    })
                    .sort((a, b) => a.firstname.localeCompare(b.firstname)),
                };
              })
              .sort((a, b) => a.eventTicket.number - b.eventTicket.number);

            return {
              ticketType,
              tickets: discountTypeTicketsWithChildren,
              amount: ticketTypeTickets.length,
            };
          })
          .filter((group) => group.tickets.length > 0);

        return {
          id: event.id,
          event,
          ticketTypes: ticketGroups,
        };
      }) ?? []
);

export const selectCartEventsWithChildrenAndSum = createSelector(
  selectCartEventsWithChildren,
  (state) => ({
    events: state,
    sum: state.reduce(
      (sum, next) =>
        sum +
        next.ticketTypes.reduce(
          (sum, n) => sum + n.ticketGroups.reduce((sum, x) => sum + x.sum, 0),
          0
        ),
      0
    ),
  })
);

export const selectCartValidUntil = createSelector(selectCart, (cart) =>
  cart?.validUntil ? new Date(cart?.validUntil) : undefined
);

export const selectCartTicketsForEvent = (eventId: string) =>
  createSelector(selectCartTickets, (cartTickets) =>
    cartTickets.filter((ticket) => ticket.eventTicket.event.id === eventId)
  );

export const selectCartEventsWithTicketsCount = createSelector(
  selectCartTickets,
  (tickets) => tickets.length
);

export const selectPaymentForm = createSelector(
  selectCheckoutState,
  (state) => state.paymentForm
);
