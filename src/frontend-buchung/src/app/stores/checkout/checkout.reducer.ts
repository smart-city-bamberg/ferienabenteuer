import { createFeature, createReducer, on } from '@ngrx/store';
import { CheckoutActions } from './checkout.actions';
import { Cart, CartTicket, Child, DiscountType } from './checkout.types';
import {
  FormGroupState,
  createFormGroupState,
  onNgrxForms,
  setValue,
  updateGroup,
  validate,
  wrapReducerWithFormStateUpdate,
} from 'ngrx-forms';
import { requiredTrue } from 'ngrx-forms/validation';

export const checkoutFeatureKey = 'checkout';

export interface Payment {
  agreeTermsAndConditions: boolean;
  agreeDataProtection: boolean;
}

export interface State {
  cart: Cart | undefined;
  children: Child[];
  discountTypes: DiscountType[];
  paymentForm: FormGroupState<Payment>;
}

export const initialState: State = {
  cart: undefined,
  children: [],
  discountTypes: [],
  paymentForm: createFormGroupState<Payment>('payment_form', {
    agreeDataProtection: false,
    agreeTermsAndConditions: false,
  }),
};

export const reducer = wrapReducerWithFormStateUpdate(
  createReducer(
    initialState,
    on(CheckoutActions.loadCartTicketsSuccess, (state, action) => ({
      ...state,
      cart: action.cart
        ? {
            ...action.cart,
          }
        : undefined,
      children: [...action.children],
      discountTypes: [...action.discountTypes],
      paymentForm: setValue(initialState.paymentForm.value)(state.paymentForm),
    })),
    on(CheckoutActions.addTicketSuccess, (state, action) => ({
      ...state,
      cart: state.cart
        ? {
            ...(state.cart ?? undefined),
            cartTickets: [
              ...(state.cart?.cartTickets ?? []),
              action.cartTicket,
            ],
          }
        : undefined,
      paymentForm: setValue(initialState.paymentForm.value)(state.paymentForm),
    })),
    on(CheckoutActions.removeTicketSuccess, (state, action) => ({
      ...state,
      cart:
        state.cart && state.cart.cartTickets.length > 1
          ? {
              ...(state.cart ?? undefined),
              cartTickets: (state.cart?.cartTickets ?? []).filter(
                (ticket) => ticket.id !== action.cartTicketId
              ),
            }
          : undefined,
      paymentForm: setValue(initialState.paymentForm.value)(state.paymentForm),
    })),
    on(CheckoutActions.assignChildToTicketSuccess, (state, action) => ({
      ...state,
      cart: state.cart
        ? {
            ...(state.cart ?? undefined),
            cartTickets: (state.cart?.cartTickets ?? []).map((cartTicket) =>
              cartTicket.id !== action.cartTicket.id
                ? cartTicket
                : {
                    ...action.cartTicket,
                  }
            ),
          }
        : undefined,
      paymentForm: setValue(initialState.paymentForm.value)(state.paymentForm),
    })),
    on(CheckoutActions.assignDiscountTypeToTicketSuccess, (state, action) => ({
      ...state,
      cart: state.cart
        ? {
            ...(state.cart ?? undefined),
            cartTickets: (state.cart?.cartTickets ?? []).map((cartTicket) =>
              cartTicket.id !== action.cartTicketId
                ? cartTicket
                : {
                    ...cartTicket,
                    child: null,
                    discountType: action.discountType
                      ? { ...action.discountType }
                      : null,
                  }
            ),
          }
        : undefined,
      paymentForm: setValue(initialState.paymentForm.value)(state.paymentForm),
    })),
    on(CheckoutActions.clearCart, (state) => ({
      ...state,
      cart: undefined,
    })),
    on(CheckoutActions.extendCartTermSuccess, (state, action) => ({
      ...state,
      cart: { ...action.cart },
    })),
    onNgrxForms()
  ),
  (state) => state.paymentForm,
  (form) =>
    updateGroup<Payment>({
      agreeDataProtection: validate(requiredTrue),
      agreeTermsAndConditions: validate(requiredTrue),
    })(form)
);

export const checkoutFeature = createFeature({
  name: checkoutFeatureKey,
  reducer,
});
