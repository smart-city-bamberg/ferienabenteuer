export interface Organizer {
  name: string;
  phone: string;
}

export interface Season {
  id: string;
  year: number;
}

export interface Holiday {
  id: string;
  name: string;
  periods: Period[];
}

export interface Category {
  name: string;
}

export interface Period {
  id: string;
  startDate: string;
  endDate: string;
}

export interface CarePeriod {
  period: {
    id: string;
  };
  careDays: CareDay[];
}

export interface CareDay {
  day: string;
}

export interface Event {
  id: string;
  name: string;
  description: string;
  careTimeStart: string;
  careTimeEnd: string;
  ageMinimum: number;
  ageMaximum: number;
  costs: number;
  picture:
    | {
        url: string;
      }
    | undefined;
  organizer: Organizer;
  locationCity: string;
  locationStreet: string;
  locationZipcode: string;
  carePeriodCommitted: CarePeriod;
}

export interface DiscountType {
  id: string;
  name: string;
  discountPercent: number;
  description: string;
}

export interface Child {
  id: string;
  firstname: string;
  surname: string;
  dateOfBirth: string;
  discounts: Discount[];
  tickets: Ticket[];
}

export interface Ticket {
  id: string;
  eventTicket: EventTicket;
}

export interface Discount {
  id: string;
  type: DiscountType;
  vouchersCount: number;
}

export interface CartTicket {
  id: string;
  discountType: DiscountType | null;
  child: Child | null;
  eventTicket: EventTicket;
  addedToCartAt: string;
}

export interface Cart {
  id: string;
  validUntil: string;
  cartTickets: CartTicket[];
  termExtensionCount: number;
}

export interface EventTicket {
  id: string;
  event: Event;
  type: string;
  number: number;
}
