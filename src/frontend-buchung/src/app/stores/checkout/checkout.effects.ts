import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  concatMap,
  map,
  catchError,
  of,
  withLatestFrom,
  filter,
  delay,
  tap,
  switchMap,
  mergeMap,
  timer,
  interval,
  takeWhile,
  takeUntil,
  skipWhile,
} from 'rxjs';
import {
  AddTicketsToCartGQL,
  AssignChildToCartTicketGQL,
  AssignDiscountTypeToCartTicketGQL,
  BookCartTicketsGQL,
  CartGQL,
  ExtendCartTermGQL,
  RemoveTicketsFromCartGQL,
} from 'src/app/graphql/generated';
import { Store } from '@ngrx/store';
import { selectUser } from '../auth/auth.selectors';
import { CheckoutActions } from './checkout.actions';
import { Cart, CartTicket, Child, DiscountType } from './checkout.types';
import { routerNavigatedAction } from '@ngrx/router-store';
import {
  selectCartTickets,
  selectCartTicketsForEvent,
  selectPaymentForm,
} from './checkout.selectors';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthActions } from '../auth/auth.actions';

@Injectable()
export class CheckoutEffects {
  loadCartTickets$ = createEffect(() =>
    this.actions$.pipe(
      ofType(CheckoutActions.loadCartTickets),
      withLatestFrom(this.store.select(selectUser)),
      concatMap(([action, user]) =>
        this.cartQuery
          .fetch({
            userId: user?.id as string,
          })
          .pipe(
            map((response) =>
              CheckoutActions.loadCartTicketsSuccess({
                cart: response.data.carts
                  ? (response.data.carts[0] as Cart)
                  : undefined,
                children: response.data.children as Child[],
                discountTypes: response.data.discountTypes as DiscountType[],
              })
            ),
            catchError((error) =>
              of(CheckoutActions.loadCartTicketsFailure({ error }))
            )
          )
      )
    )
  );

  removeAllEventTicketsFromCart$ = createEffect(() =>
    this.actions$.pipe(
      ofType(CheckoutActions.removeAllEventTicketsFromCart),
      concatMap((action) =>
        of(action).pipe(
          withLatestFrom(
            this.store.select(selectCartTicketsForEvent(action.eventId))
          )
        )
      ),
      concatMap(([action, tickets]) =>
        this.removeTicketsFromCartMutation
          .mutate({
            ticketIds: tickets.map((ticket) => ticket.id),
          })
          .pipe(
            map((response) =>
              CheckoutActions.removeAllEventTicketsFromCartSuccess()
            ),
            catchError((error) =>
              of(
                CheckoutActions.removeAllEventTicketsFromCartFailure({ error })
              )
            )
          )
      )
    )
  );

  removeTicketFromCart$ = createEffect(() =>
    this.actions$.pipe(
      ofType(CheckoutActions.removeTicket),
      withLatestFrom(this.store.select(selectCartTickets)),
      concatMap(([action, tickets]) =>
        this.removeTicketsFromCartMutation
          .mutate({
            ticketIds: [
              tickets.find(
                (ticket) =>
                  ticket.eventTicket.event.id == action.eventId &&
                  ticket.discountType?.id == action.discountTypeId
              )?.id as string,
            ],
          })
          .pipe(
            map((response) =>
              CheckoutActions.removeTicketSuccess({
                cartTicketId: response.data?.removeTicketsFromCart[0] as string,
              })
            ),
            catchError((error) =>
              of(CheckoutActions.removeTicketFailure({ error }))
            )
          )
      )
    )
  );

  addTicketToCart$ = createEffect(() =>
    this.actions$.pipe(
      ofType(CheckoutActions.addTicket),
      concatMap((action) =>
        this.addTicketToCartMutation
          .mutate({
            eventId: action.eventId,
            amounts: [
              {
                amount: 1,
                discountType: action.discountTypeId,
              },
            ],
          })
          .pipe(
            map((response) => {
              const addTicketsToCart = response.data?.addTicketsToCart;

              if (
                addTicketsToCart?.__typename == 'AddTicketsToCartSuccess' &&
                addTicketsToCart.cartTickets
              ) {
                return CheckoutActions.addTicketSuccess({
                  cartTicket: addTicketsToCart.cartTickets[0] as CartTicket,
                });
              }

              if (
                addTicketsToCart?.__typename == 'AddTicketsToCartFailed' &&
                addTicketsToCart.reason == 'NotEnoughTicketsAvailable'
              ) {
                return CheckoutActions.addTicketNoTicketAvailable();
              }

              if (
                addTicketsToCart?.__typename == 'AddTicketsToCartFailed' &&
                addTicketsToCart.reason == 'MaximumAllowedTicketsPerEvent'
              ) {
                return CheckoutActions.addTicketMaximumChildrenCount();
              }

              if (
                addTicketsToCart?.__typename == 'AddTicketsToCartFailed' &&
                addTicketsToCart.reason == 'BookingNotStarted'
              ) {
                return CheckoutActions.addTicketBookingNotStarted();
              }

              throw Error('no result');
            }),
            catchError((error) =>
              of(CheckoutActions.addTicketFailure({ error }))
            )
          )
      )
    )
  );

  assignChildToTicket$ = createEffect(() =>
    this.actions$.pipe(
      ofType(CheckoutActions.assignChildToTicket),
      concatMap((action) =>
        this.assignChildToTicketMutation
          .mutate({
            cartTicketId: action.cartTicketId,
            childId: action.childId ?? null,
          })
          .pipe(
            map((response) => {
              if (
                response.data?.assignChildToCartTicket.__typename ===
                'AssignChildToCartTicketSuccess'
              ) {
                return CheckoutActions.assignChildToTicketSuccess({
                  cartTicket: response.data?.assignChildToCartTicket
                    .cartTicket as CartTicket,
                });
              }

              if (
                response.data?.assignChildToCartTicket.__typename ===
                'AssignChildToCartTicketFailed'
              ) {
                if (
                  response.data.assignChildToCartTicket.reason ===
                  'ChildAlreadyAssignedForEvent'
                ) {
                  return CheckoutActions.assignChildToTicketChildAlreadyAssignedForEvent(
                    {
                      cartTicketId: action.cartTicketId,
                      childId: action.childId,
                    }
                  );
                }

                if (
                  response.data.assignChildToCartTicket.reason ===
                  'ChildAlreadyBookedForEvent'
                ) {
                  return CheckoutActions.assignChildToTicketChildAlreadyBookedForEvent(
                    {
                      cartTicketId: action.cartTicketId,
                      childId: action.childId,
                    }
                  );
                }
              }

              throw new Error('no result');
            }),
            catchError((error) =>
              of(
                CheckoutActions.assignChildToTicketFailure({
                  cartTicketId: action.cartTicketId,
                  childId: action.childId,
                  error,
                })
              )
            )
          )
      )
    )
  );

  assignDiscountTypeToTicket$ = createEffect(() =>
    this.actions$.pipe(
      ofType(CheckoutActions.assignDiscountTypeToTicket),
      concatMap((action) =>
        this.assignDiscountTypeToTicketMutation
          .mutate({
            cartTicketId: action.cartTicketId,
            discountTypeId: action.discountTypeId,
          })
          .pipe(
            map((response) =>
              CheckoutActions.assignDiscountTypeToTicketSuccess({
                cartTicketId: action.cartTicketId,
                discountType: response.data?.assignDiscountTypeToCartTicket
                  .discountType as DiscountType,
              })
            ),
            catchError((error) =>
              of(
                CheckoutActions.assignDiscountTypeToTicketFailure({
                  cartTicketId: action.cartTicketId,
                  discountTypeId: action.discountTypeId,
                  error,
                })
              )
            )
          )
      )
    )
  );

  bookCartTickets$ = createEffect(() =>
    this.actions$.pipe(
      ofType(CheckoutActions.bookCartTickets),
      withLatestFrom(this.store.select(selectPaymentForm)),
      concatMap(([action, form]) =>
        !form.isValid
          ? of(CheckoutActions.bookCartTicketsRejected())
          : this.bookCartTicketsMutation
              .mutate({
                agreeDataProtection: form.value.agreeDataProtection,
                agreeTermsAndConditions: form.value.agreeTermsAndConditions,
              })
              .pipe(
                map((response) => {
                  if (
                    response.data?.bookCartTickets.__typename ===
                    'BookCartTicketsSuccess'
                  ) {
                    return CheckoutActions.bookCartTicketsSuccess();
                  }

                  if (
                    response.data?.bookCartTickets.__typename ===
                    'BookCartTicketsFailed'
                  ) {
                    if (
                      response.data?.bookCartTickets.reason ===
                      'AllTicketsMustHaveChildAssigned'
                    ) {
                      return CheckoutActions.bookCartTicketsNotAllTicketsAssignedChild();
                    }

                    if (
                      response.data?.bookCartTickets.reason ===
                      'NoDiscountVoucherAvailable'
                    ) {
                      return CheckoutActions.bookCartTicketsNoDiscountVoucherAvailable();
                    }

                    if (
                      response.data?.bookCartTickets.reason ===
                      'MustAgreeDataProtection'
                    ) {
                      return CheckoutActions.bookCartTicketsMustAgreeDataProtection();
                    }

                    if (
                      response.data?.bookCartTickets.reason ===
                      'MustAgreeTermsAndConditions'
                    ) {
                      return CheckoutActions.bookCartTicketsMustAgreeTermsAndConditions();
                    }
                  }

                  throw new Error('no result');
                }),
                catchError((error) =>
                  of(
                    CheckoutActions.bookCartTicketsFailure({
                      error,
                    })
                  )
                )
              )
      )
    )
  );

  extendCartTerm$ = createEffect(() =>
    this.actions$.pipe(
      ofType(CheckoutActions.extendCartTerm),
      concatMap((action) =>
        this.extendCartTermMutation.mutate().pipe(
          map((response) => {
            if (
              response.data?.extendCartLifetime.__typename ===
              'ExtendCartTermSuccess'
            ) {
              return CheckoutActions.extendCartTermSuccess({
                cart: response.data.extendCartLifetime.cart as Cart,
              });
            }

            if (
              response.data?.extendCartLifetime.__typename ===
              'ExtendCartTermFailed'
            ) {
              if (
                response.data.extendCartLifetime.reason ===
                'MaximumTermExtensionsReached'
              ) {
                return CheckoutActions.extendCartTermMaximumExtensionsReached();
              }
            }

            throw new Error('no result');
          }),
          catchError((error) =>
            of(
              CheckoutActions.extendCartTermFailure({
                error,
              })
            )
          )
        )
      )
    )
  );

  redirectToSumaryWhenBookCartTicketsSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(CheckoutActions.bookCartTicketsSuccess),
        tap(() => this.router.navigate(['/checkout/erfolgreich']))
      ),
    {
      dispatch: false,
    }
  );

  loadCartAfterNavigate$ = createEffect(() =>
    this.actions$.pipe(
      ofType(routerNavigatedAction),
      filter((action) =>
        ['/checkout', '/veranstaltungen'].some((url) =>
          action.payload.routerState.url.startsWith(url)
        )
      ),
      switchMap((action) =>
        of(action).pipe(
          withLatestFrom(this.store.select(selectUser)),
          filter(([action, user]) => user !== undefined),
          map((action) => CheckoutActions.loadCartTickets())
        )
      )
    )
  );

  scrollToTopAfterNavigate$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(routerNavigatedAction),
        filter((action) => action.payload.event.url.startsWith('/checkout')),
        tap((action) => window.scrollTo({ top: 0 }))
      ),
    {
      dispatch: false,
    }
  );

  loadCartAfterRemoveAllTicketsFromCart$ = createEffect(() =>
    this.actions$.pipe(
      ofType(CheckoutActions.removeAllEventTicketsFromCartSuccess),
      map((action) => CheckoutActions.loadCartTickets())
    )
  );

  checkCartValidity$ = createEffect(() =>
    this.actions$.pipe(
      ofType(CheckoutActions.loadCartTicketsSuccess, AuthActions.logoutSuccess),
      switchMap((action) =>
        of(action).pipe(
          ofType(CheckoutActions.loadCartTicketsSuccess),
          switchMap((action) => of(action.cart?.validUntil)),
          filter((validUntil) => validUntil !== undefined),
          switchMap((validUntil) =>
            of(validUntil).pipe(delay(new Date(validUntil!)))
          ),
          mergeMap(() => [
            CheckoutActions.cartValidityExpired(),
            CheckoutActions.clearCart(),
          ])
        )
      )
    )
  );

  checkCartTimeout$ = createEffect(() =>
    this.actions$.pipe(
      ofType(CheckoutActions.loadCartTicketsSuccess),
      filter((action) => action.cart !== undefined),
      switchMap((action) =>
        interval(1000).pipe(
          map(
            () =>
              new Date(action.cart!.validUntil).getTime() - new Date().getTime()
          ),
          skipWhile((remaining: number) => remaining - 5 * 60 * 1000 > 0),
          takeUntil(this.actions$.pipe(ofType(CheckoutActions.cartExpiring))),
          map((remaining) =>
            CheckoutActions.cartExpiring({
              remainingTime: remaining,
            })
          )
        )
      )
    )
  );

  clearCartOnLogout$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.logoutSuccess),
      map(() => CheckoutActions.clearCart())
    )
  );

  clearCartOnSuccessfulBooking$ = createEffect(() =>
    this.actions$.pipe(
      ofType(CheckoutActions.bookCartTicketsSuccess),
      map(() => CheckoutActions.clearCart())
    )
  );

  constructor(
    private actions$: Actions,
    private store: Store,
    private router: Router,
    private toaster: ToastrService,
    private cartQuery: CartGQL,
    private addTicketToCartMutation: AddTicketsToCartGQL,
    private removeTicketsFromCartMutation: RemoveTicketsFromCartGQL,
    private assignChildToTicketMutation: AssignChildToCartTicketGQL,
    private assignDiscountTypeToTicketMutation: AssignDiscountTypeToCartTicketGQL,
    private bookCartTicketsMutation: BookCartTicketsGQL,
    private extendCartTermMutation: ExtendCartTermGQL
  ) {}
}
