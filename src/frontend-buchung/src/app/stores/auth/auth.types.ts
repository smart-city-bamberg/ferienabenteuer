export interface User {
  id: string;
  email: string;
  name: string;
  role: string;
  isEnabled: boolean;
}
