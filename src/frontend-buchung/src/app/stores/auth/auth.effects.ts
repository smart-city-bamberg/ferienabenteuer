import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  catchError,
  concatMap,
  delay,
  filter,
  map,
  tap,
  withLatestFrom,
} from 'rxjs/operators';
import { of } from 'rxjs';
import { AuthActions } from './auth.actions';
import {
  AuthenticateUserWithPasswordGQL,
  AuthenticatedUserGQL,
  LogoutGQL,
} from 'src/app/graphql/generated';
import { Router } from '@angular/router';
import { Store, select } from '@ngrx/store';
import * as authSelectors from './auth.selectors';
import { User } from './auth.types';

@Injectable()
export class AuthEffects {
  login$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.login),
      concatMap((action) =>
        this.authenticateMutation
          .mutate({ email: action.user, password: action.password })
          .pipe(
            map((response) => {
              if (
                response.data?.authenticateUserWithPassword?.__typename ==
                'UserAuthenticationWithPasswordFailure'
              ) {
                return AuthActions.loginInvalid({
                  message: response.data?.authenticateUserWithPassword.message,
                });
              }

              if (
                response.data?.authenticateUserWithPassword?.__typename ==
                'UserAuthenticationWithPasswordSuccess'
              ) {
                return AuthActions.loginSuccess();
              }

              throw new Error('no response');
            }),
            catchError((error) => of(AuthActions.loginFailure({ error })))
          )
      )
    )
  );

  loadAuthenticatedUser$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.loadUser),
      concatMap(() =>
        this.authenticatedUserQuery.fetch().pipe(
          map((response) => {
            if (response.data?.authenticatedItem) {
              // if (!response.data?.authenticatedItem.isEnabled) {
              //   return AuthActions.loadUserNotEnabled();
              // }

              return AuthActions.loadUserSuccess({
                user: response.data.authenticatedItem as User,
              });
            }

            return AuthActions.loadUserNotAuthenticated();
          }),
          catchError((error) => of(AuthActions.loadUserFailure({ error })))
        )
      )
    )
  );

  loadAuthenticatedUserWhenLoginSuccess$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.loginSuccess),
      map(() => AuthActions.loadUser())
    )
  );

  redirectToEventsWhenLoadAuthenticatedUserSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(AuthActions.loadUserSuccess),
        withLatestFrom(
          this.store.select(authSelectors.selectUrl),
          this.store.select(authSelectors.selectRoleAllowed),
          this.store.select(authSelectors.selectUserEnabled)
        ),
        filter(
          ([action, route, roleAllowed, isEnabled]) =>
            roleAllowed && isEnabled && route === '/login'
        ),
        tap(() => this.router.navigate(['/veranstaltungen']))
      ),
    {
      dispatch: false,
    }
  );

  logout$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.logout),
      concatMap((action) =>
        this.logoutMutation.mutate().pipe(
          map((response) => AuthActions.logoutSuccess()),
          catchError((error) => of(AuthActions.logoutFailure({ error })))
        )
      )
    )
  );

  redirectToEventsWhenLogout$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(AuthActions.logoutSuccess),
        withLatestFrom(this.store.select(authSelectors.selectUrl)),
        filter(([action, url]) => url !== '/konto-geloescht'),
        tap(() => this.router.navigate(['/veranstaltungen']))
      ),
    {
      dispatch: false,
    }
  );

  constructor(
    private actions$: Actions,
    private authenticateMutation: AuthenticateUserWithPasswordGQL,
    private authenticatedUserQuery: AuthenticatedUserGQL,
    private logoutMutation: LogoutGQL,
    private router: Router,
    private store: Store
  ) {}
}
