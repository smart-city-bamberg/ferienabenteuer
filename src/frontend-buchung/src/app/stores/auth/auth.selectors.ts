import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromAuth from './auth.reducer';
import * as routerStore from '@ngrx/router-store';

const allowedRoles = ['custodian'];

export const { selectUrl, selectCurrentRoute } =
  routerStore.getRouterSelectors();

export const selectAuthState = createFeatureSelector<fromAuth.State>(
  fromAuth.authFeatureKey
);

export const selectIsActive = createSelector(
  selectAuthState,
  (state) => state.isActive
);
export const selectInvalidCredentials = createSelector(
  selectAuthState,
  (state) => state.invalidCredentials
);

export const selectRoleAllowed = createSelector(selectAuthState, (state) =>
  allowedRoles.includes(state.user?.role ?? '')
);

export const selectUserEnabled = createSelector(
  selectAuthState,
  (state) => state.user?.isEnabled ?? false
);

export const selectUserNotEnabled = createSelector(
  selectUserEnabled,
  (enabled) => !enabled
);

export const selectHasError = createSelector(
  selectAuthState,
  (state) => state.hasError
);

export const selectUser = createSelector(
  selectAuthState,
  (state) => state.user
);

export const selectUserRole = createSelector(selectUser, (user) => user?.role);
