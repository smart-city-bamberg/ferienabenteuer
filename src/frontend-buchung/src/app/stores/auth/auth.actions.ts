import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { User } from './auth.types';

export const AuthActions = createActionGroup({
  source: 'Auth',
  events: {
    Login: props<{ user: string; password: string }>(),
    'Login Success': emptyProps(),
    'Login Invalid': props<{ message: string }>(),
    'Login Failure': props<{ error: any }>(),

    'Load User': emptyProps(),
    'Load User Success': props<{
      user: User;
    }>(),
    'Load User Not Authenticated': emptyProps(),
    'Load User Not Enabled': emptyProps(),
    'Load User Failure': props<{ error: any }>(),
    Logout: emptyProps(),
    'Logout Success': emptyProps(),
    'Logout Failure': props<{ error: any }>(),
  },
});
