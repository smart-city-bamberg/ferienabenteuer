import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { routerNavigatedAction } from '@ngrx/router-store';
import { filter, tap } from 'rxjs';

@Injectable()
export class LandingPageEffects {
  private targetUrls = ['/impressum', '/datenschutz', '/agb', '/widerruf'];

  scrollToTopAfterNavigate$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(routerNavigatedAction),
        filter((action) =>
          this.targetUrls.some((url) => action.payload.event.url === url)
        ),
        tap((action) => window.scrollTo({ top: 0 }))
      ),
    {
      dispatch: false,
    }
  );

  constructor(private actions$: Actions) {}
}
