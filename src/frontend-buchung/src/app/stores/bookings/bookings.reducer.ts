import { createFeature, createReducer, on } from '@ngrx/store';
import { BookingsActions } from './bookings.actions';
import { Booking, DiscountType, Reservation } from './bookings.types';

export const bookingsFeatureKey = 'bookings';

export interface State {
  bookings: Booking[];
  reservations: Reservation[];
  discountTypes: DiscountType[];
}

export const initialState: State = {
  bookings: [],
  discountTypes: [],
  reservations: [],
};

export const reducer = createReducer(
  initialState,
  on(BookingsActions.loadBookings, (state) => state),
  on(BookingsActions.loadBookingsSuccess, (state, action) => ({
    ...state,
    bookings: [...action.bookings],
    reservations: [...action.reservations],
    discountTypes: [...action.discountTypes],
  })),
  on(BookingsActions.loadBookingsFailure, (state, action) => state)
);

export const bookingsFeature = createFeature({
  name: bookingsFeatureKey,
  reducer,
});
