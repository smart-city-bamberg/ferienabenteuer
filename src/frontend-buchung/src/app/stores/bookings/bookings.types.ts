export interface Organizer {
  id: string;
  email: string;
  name: string;
  bankBic: string;
  bankIban: string;
  bankName: string;
  bankOwner: string;
  phone: string;
}

export interface Season {
  id: string;
  year: number;
}

export interface Holiday {
  id: string;
  name: string;
  periods: Period[];
}

export interface Category {
  id: string;
  name: string;
}

export interface Period {
  id: string;
  startDate: string;
  endDate: string;
}

export interface CarePeriod {
  period: {
    id: string;
  };
  careDays: CareDay[];
}

export interface CareDay {
  day: string;
}

export interface Event {
  id: string;
  name: string;
  description: string;
  careTimeStart: string;
  careTimeEnd: string;
  ageMinimum: number;
  ageMaximum: number;
  costs: any;
  picture:
    | {
        url: string;
      }
    | undefined;
  emergencyPhone: string;
  organizer: Organizer;
  locationCity: string;
  locationStreet: string;
  locationZipcode: string;
  categories: Category[];
  carePeriodCommitted: CarePeriod;
  waitingListCount: number;
  waitingListLimit: number;
  participantsCount: number;
  participantLimit: number;
}

export interface DiscountType {
  id: string;
  name: string;
  discountPercent: number;
  description: string;
}

export interface Discount {
  id: string;
  type: DiscountType;
}

export interface DiscountVoucher {
  id: string;
  discount: Discount;
}

export interface Child {
  id: string;
  firstname: string;
  surname: string;
  dateOfBirth: Date;
}

export interface Ticket {
  id: string;
  discountVoucher: DiscountVoucher | null;
  child: Child | null;
  eventTicket: EventTicket;
  addedToCartAt: Date;
  price: number;
}

export interface EventTicket {
  id: string;
  event: Event;
  type: string;
  number: number;
}

export interface Booking {
  id: string;
  code: string;
  date: string;
  status: string;
  event: Event;
  tickets: Ticket[];
  price: number;
}

export interface Reservation {
  id: string;
  code: string;
  date: string;
  event: Event;
  tickets: Ticket[];
  price: number;
}
