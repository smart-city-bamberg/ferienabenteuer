import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  catchError,
  map,
  concatMap,
  filter,
  switchMap,
  withLatestFrom,
} from 'rxjs/operators';
import { of } from 'rxjs';
import { BookingsActions } from './bookings.actions';
import { BookingsGQL } from 'src/app/graphql/generated';
import { Booking, DiscountType, Reservation } from './bookings.types';
import { routerNavigatedAction } from '@ngrx/router-store';
import { selectUser } from '../auth/auth.selectors';
import { Store } from '@ngrx/store';

@Injectable()
export class BookingsEffects {
  loadBookings$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(BookingsActions.loadBookings),
      concatMap(() =>
        this.bookingsQuery.fetch().pipe(
          map((response) =>
            BookingsActions.loadBookingsSuccess({
              bookings: response.data.bookings as Booking[],
              reservations: response.data.reservations as Reservation[],
              discountTypes: response.data.discountTypes as DiscountType[],
            })
          ),
          catchError((error) =>
            of(BookingsActions.loadBookingsFailure({ error }))
          )
        )
      )
    );
  });

  loadBookingsAfterNavigate$ = createEffect(() =>
    this.actions$.pipe(
      ofType(routerNavigatedAction),
      filter((action) =>
        ['/buchungen'].some((url) => action.payload.routerState.url === url)
      ),
      switchMap((action) =>
        of(action).pipe(
          withLatestFrom(this.store.select(selectUser)),
          filter(([action, user]) => user !== undefined),
          map((action) => BookingsActions.loadBookings())
        )
      )
    )
  );

  constructor(
    private actions$: Actions,
    private store: Store,
    private bookingsQuery: BookingsGQL
  ) {}
}
