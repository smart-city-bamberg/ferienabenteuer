import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { Booking, DiscountType, Reservation } from './bookings.types';

export const BookingsActions = createActionGroup({
  source: 'Bookings',
  events: {
    'Load Bookings': emptyProps(),
    'Load Bookings Success': props<{
      bookings: Booking[];
      discountTypes: DiscountType[];
      reservations: Reservation[];
    }>(),
    'Load Bookings Failure': props<{ error: unknown }>(),
  },
});
