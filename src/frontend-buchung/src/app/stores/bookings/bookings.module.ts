import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import * as fromBookings from './bookings.reducer';
import { EffectsModule } from '@ngrx/effects';
import { BookingsEffects } from './bookings.effects';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StoreModule.forFeature(fromBookings.bookingsFeatureKey, fromBookings.reducer),
    EffectsModule.forFeature([BookingsEffects])
  ]
})
export class BookingsModule { }
