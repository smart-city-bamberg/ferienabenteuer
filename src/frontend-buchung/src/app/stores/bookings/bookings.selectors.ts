import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromBookings from './bookings.reducer';
import { Booking, Event } from './bookings.types';

export const selectBookingsState = createFeatureSelector<fromBookings.State>(
  fromBookings.bookingsFeatureKey
);

export const selectBookings = createSelector(
  selectBookingsState,
  (state) => state.bookings
);

export const selectReservations = createSelector(
  selectBookingsState,
  (state) => state.reservations
);

export const selectDiscountTypes = createSelector(
  selectBookingsState,
  (state) => state.discountTypes
);

export const selectBookingWithTickets = createSelector(
  selectBookings,
  selectDiscountTypes,
  (bookings, discountTypes) =>
    bookings.map((booking) => ({
      ...booking,
      discountGroups: [undefined, ...discountTypes]
        .map((type) => {
          const tickets = booking.tickets.filter(
            (ticket) => ticket.discountVoucher?.discount?.type?.id === type?.id
          );

          return {
            discountType: type,
            tickets: tickets,
            sum: tickets.reduce((sum, ticket) => sum + ticket.price, 0),
          };
        })
        .filter((group) => group.tickets.length > 0),
    }))
);

export const selectReservationsWithTickets = createSelector(
  selectReservations,
  selectDiscountTypes,
  (reservations, discountTypes) =>
    reservations.map((reservation) => ({
      ...reservation,
      discountGroups: [undefined, ...discountTypes]
        .map((type) => {
          const tickets = reservation.tickets.filter(
            (ticket) => ticket.discountVoucher?.discount?.type?.id === type?.id
          );

          return {
            discountType: type,
            tickets: tickets,
            sum: tickets.reduce((sum, ticket) => sum + ticket.price, 0),
          };
        })
        .filter((group) => group.tickets.length > 0),
    }))
);

export const selectBookingEvents = createSelector(
  selectBookings,
  selectReservations,
  (bookings, reservations) =>
    [...bookings, ...reservations].reduce((acc, booking) => {
      if (!acc.some((e) => e.id === booking.event.id)) {
        acc.push(booking.event);
      }
      return acc;
    }, new Array<Event>())
);

export const selectEventsWithBookingsAndTickets = createSelector(
  selectBookingEvents,
  selectBookingWithTickets,
  selectReservationsWithTickets,
  (events, bookings, reservations) =>
    events.map((event) => ({
      ...event,
      bookingsAndReservations: [
        ...bookings
          .filter((booking) => booking.event.id === event.id)
          .map((booking) => ({
            booking,
            date: booking.date,
            reservation: undefined,
          })),
        ...reservations
          .filter((reservation) => reservation.event.id === event.id)
          .map((reservation) => ({
            reservation: reservation,
            booking: undefined,
            date: reservation.date,
          })),
      ].sort((a, b) => a.date.localeCompare(b.date)),
    }))
);

export const selectNoBookingEvents = createSelector(
  selectEventsWithBookingsAndTickets,
  (state) => state.length === 0
);
