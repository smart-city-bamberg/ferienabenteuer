import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromCustodian from './custodian.reducer';

export const selectCustodianState = createFeatureSelector<fromCustodian.State>(
  fromCustodian.custodianFeatureKey
);

export const selectCustodian = createSelector(
  selectCustodianState,
  (state) => state.custodian
);

export const selectCustodianId = createSelector(
  selectCustodianState,
  (state) => state.custodian?.id
);

export const selectCustodianForm = createSelector(
  selectCustodianState,
  (state) => state.custodianForm
);

export const selectEarlyAccessCodeVouchers = createSelector(
  selectCustodianState,
  (state) => state.earlyAccessCodeVouchers
);

export const selectSeasons = createSelector(
  selectCustodianState,
  (state) => state.seasons
);

export const selectSelectedSeason = createSelector(
  selectCustodianState,
  (state) => state.year
);

export const selectCode = createSelector(
  selectCustodianState,
  (state) => state.code
);

export const selectSelectedEarlyAccessCodeVoucher = createSelector(
  selectSelectedSeason,
  selectEarlyAccessCodeVouchers,
  (season, vouchers) =>
    vouchers.find((voucher) => voucher.season.year === season)
);

export const selectSelectedEarlyAccessCodeVoucherCode = createSelector(
  selectSelectedEarlyAccessCodeVoucher,
  selectCode,
  (voucher, code) => voucher?.code ?? code
);

export const selectCodeIsReadOnly = createSelector(
  selectSelectedEarlyAccessCodeVoucher,
  (voucher) => voucher !== undefined
);
