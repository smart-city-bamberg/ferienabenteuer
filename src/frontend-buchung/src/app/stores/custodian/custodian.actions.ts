import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { Custodian, EarlyAccessCodeVoucher, Season } from './custodian.types';

export const CustodianActions = createActionGroup({
  source: 'Custodian',
  events: {
    'Load Custodian': emptyProps(),
    'Load Custodian Success': props<{
      custodian: Custodian;
      earlyAccessCodeVouchers: EarlyAccessCodeVoucher[];
      seasons: Season[];
    }>(),
    'Load Custodian Failure': props<{ error: unknown }>(),

    'Update Custodian': emptyProps(),
    'Update Custodian Success': emptyProps(),
    'Update Custodian Failure': props<{ error: unknown }>(),
    'Update Custodian Rejected': emptyProps(),

    'Select Early Access Code Year': props<{ year: number }>(),
    'Update Early Access Code': props<{ code: string }>(),

    'Redeem Early Access Code': emptyProps(),
    'Redeem Early Access Code Success': props<{
      earlyAccessCodeVoucher: EarlyAccessCodeVoucher;
    }>(),
    'Redeem Early Access Code Not Redeemed': emptyProps(),
    'Redeem Early Access Code Failure': props<{ error: unknown }>(),

    'Generate Gdpr Information': props<{
      custodianId: string;
    }>(),
    'Generate Gdpr Information Success': emptyProps(),
    'Generate Gdpr Information Failure': props<{ error: unknown }>(),
  },
});
