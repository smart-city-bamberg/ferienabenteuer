import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import * as fromCustodian from './custodian.reducer';
import { EffectsModule } from '@ngrx/effects';
import { CustodianEffects } from './custodian.effects';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StoreModule.forFeature(fromCustodian.custodianFeatureKey, fromCustodian.reducer),
    EffectsModule.forFeature([CustodianEffects])
  ]
})
export class CustodianModule { }
