export interface Custodian {
  id: string;
  salutation: string;
  firstname: string;
  surname: string;
  phone: string;
  emergencyPhone: string;
  addressCity: string;
  addressStreet: string;
  addressHouseNumber: string;
  addressZip: string;
  addressAdditional: string;
}

export interface Season {
  id: string;
  year: number;
}

export interface EarlyAccessCodeVoucher {
  id: string;
  code: string;
  season: {
    id: string;
    year: number;
  };
}
