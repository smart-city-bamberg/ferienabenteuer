import { createFeature, createReducer, on } from '@ngrx/store';
import { CustodianActions } from './custodian.actions';
import { Custodian, EarlyAccessCodeVoucher, Season } from './custodian.types';
import {
  FormGroupState,
  createFormGroupState,
  onNgrxForms,
  setValue,
  updateGroup,
  validate,
  wrapReducerWithFormStateUpdate,
} from 'ngrx-forms';

import { minLength, pattern, required } from 'ngrx-forms/validation';

export const custodianFeatureKey = 'custodian';

export interface State {
  custodian: Custodian | undefined;
  custodianForm: FormGroupState<Custodian>;
  earlyAccessCodeVouchers: EarlyAccessCodeVoucher[];
  year: number;
  seasons: Season[];
  code: string;
}

const formularId = 'custodian_form';

export const initialState: State = {
  custodian: undefined,
  custodianForm: createFormGroupState(formularId, {
    addressAdditional: '',
    addressCity: '',
    addressHouseNumber: '',
    addressStreet: '',
    addressZip: '',
    emergencyPhone: '',
    firstname: '',
    surname: '',
    phone: '',
    salutation: '',
    id: '',
  }),
  earlyAccessCodeVouchers: [],
  year: 2024,
  seasons: [],
  code: '',
};

export const reducer = wrapReducerWithFormStateUpdate(
  createReducer(
    initialState,
    on(CustodianActions.loadCustodian, (state) => initialState),
    on(CustodianActions.loadCustodianSuccess, (state, action) => ({
      ...state,
      custodian: action.custodian,
      custodianForm: setValue(action.custodian)(state.custodianForm),
      earlyAccessCodeVouchers: [...action.earlyAccessCodeVouchers],
      seasons: [...action.seasons],
    })),
    on(CustodianActions.loadCustodianFailure, (state, action) => state),
    on(CustodianActions.selectEarlyAccessCodeYear, (state, action) => ({
      ...state,
      year: action.year,
      code: '',
    })),
    on(CustodianActions.updateEarlyAccessCode, (state, action) => ({
      ...state,
      code: action.code,
    })),
    on(CustodianActions.redeemEarlyAccessCodeSuccess, (state, action) => ({
      ...state,
      code: '',
      earlyAccessCodeVouchers: [
        ...state.earlyAccessCodeVouchers,
        action.earlyAccessCodeVoucher,
      ],
    })),

    onNgrxForms()
  ),
  (state) => state.custodianForm,
  (state) =>
    updateGroup<Custodian>({
      salutation: validate(required),
      firstname: validate(required, minLength(1)),
      surname: validate(required, minLength(1)),
      addressCity: validate(required, minLength(2)),
      addressZip: validate(required, pattern(/^[0-9]{5}$/)),
      addressStreet: validate(required, minLength(3)),
      addressHouseNumber: validate(required, minLength(1)),
      phone: validate(required, pattern(/^\+(?:[0-9]⋅?){6,14}[0-9]$/)),
      emergencyPhone: validate(required, pattern(/^\+(?:[0-9]⋅?){6,14}[0-9]$/)),
    })(state)
);

export const custodianFeature = createFeature({
  name: custodianFeatureKey,
  reducer,
});
