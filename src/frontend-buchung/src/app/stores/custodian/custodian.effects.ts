import {
  selectCode,
  selectCustodian,
  selectCustodianForm,
  selectSelectedSeason,
} from './custodian.selectors';
import { Store } from '@ngrx/store';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  catchError,
  map,
  concatMap,
  withLatestFrom,
  filter,
  first,
  switchMap,
} from 'rxjs/operators';
import { of } from 'rxjs';
import { CustodianActions } from './custodian.actions';
import {
  CustodianGQL,
  GdprInformationGQL,
  RedeemEarlyAccessCodeGQL,
  UpdateCustodianGQL,
} from 'src/app/graphql/generated';
import { Custodian, EarlyAccessCodeVoucher, Season } from './custodian.types';
import { routerNavigatedAction } from '@ngrx/router-store';
import * as FileSaver from 'file-saver';

@Injectable()
export class CustodianEffects {
  loadCustodian$ = createEffect(() =>
    this.actions$.pipe(
      ofType(CustodianActions.loadCustodian),
      concatMap(() =>
        this.custodianQuery.fetch().pipe(
          map((response) =>
            CustodianActions.loadCustodianSuccess({
              custodian: (response.data.custodians as Custodian[])[0],
              earlyAccessCodeVouchers: response.data
                .earlyAccessCodeVouchers as EarlyAccessCodeVoucher[],
              seasons: response.data.seasons as Season[],
            })
          ),
          catchError((error) =>
            of(CustodianActions.loadCustodianFailure({ error }))
          )
        )
      )
    )
  );

  updateCustodian$ = createEffect(() =>
    this.actions$.pipe(
      ofType(CustodianActions.updateCustodian),
      withLatestFrom(
        this.store.select(selectCustodian),
        this.store.select(selectCustodianForm)
      ),
      concatMap(([action, custodian, form]) =>
        !form.isValid
          ? of(CustodianActions.updateCustodianRejected())
          : this.updateCustodianMutation
              .mutate({
                id: custodian?.id as string,
                data: {
                  addressAdditional: form.value.addressAdditional,
                  addressCity: form.value.addressCity,
                  addressHouseNumber: form.value.addressHouseNumber,
                  addressStreet: form.value.addressStreet,
                  addressZip: form.value.addressZip,
                  emergencyPhone: form.value.emergencyPhone,
                  firstname: form.value.firstname,
                  surname: form.value.surname,
                  phone: form.value.phone,
                  salutation: form.value.salutation,
                },
              })
              .pipe(
                map((response) => CustodianActions.updateCustodianSuccess()),
                catchError((error) =>
                  of(CustodianActions.updateCustodianFailure({ error }))
                )
              )
      )
    )
  );

  redeemEarlyAccessCode$ = createEffect(() =>
    this.actions$.pipe(
      ofType(CustodianActions.redeemEarlyAccessCode),
      withLatestFrom(
        this.store.select(selectSelectedSeason),
        this.store.select(selectCode)
      ),
      concatMap(([action, season, code]) =>
        this.redeemEarlyAccessCodeMutation
          .mutate({
            year: season,
            code: code,
          })
          .pipe(
            map((response) => {
              const redeemEarlyAccessCode =
                response.data?.redeemEarlyAccessCode;

              if (
                redeemEarlyAccessCode?.__typename ==
                'RedeemEarlyAccessCodeSuccess'
              ) {
                return CustodianActions.redeemEarlyAccessCodeSuccess({
                  earlyAccessCodeVoucher:
                    redeemEarlyAccessCode.earlyAccessCodeVoucher as EarlyAccessCodeVoucher,
                });
              }

              if (
                redeemEarlyAccessCode?.__typename ==
                'RedeemEarlyAccessCodeFailed'
              ) {
                return CustodianActions.redeemEarlyAccessCodeNotRedeemed();
              }

              throw new Error('no result');
            }),
            catchError((error) =>
              of(CustodianActions.redeemEarlyAccessCodeFailure({ error }))
            )
          )
      )
    )
  );

  loadCustodianAfterNavigate$ = createEffect(() =>
    this.actions$.pipe(
      ofType(routerNavigatedAction),
      filter((action) => action.payload.routerState.url === '/daten'),
      map((action) => CustodianActions.loadCustodian())
    )
  );

  generateGdprInformation$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(CustodianActions.generateGdprInformation),
        switchMap((action) =>
          this.gdprInformationQuery
            .fetch({
              custodianId: action.custodianId,
            })
            .pipe(
              map((response) => {
                const json = JSON.stringify(response.data.custodian, null, 2);
                const blob = new Blob([json], {
                  type: 'text/plain;charset=utf-8',
                });

                const fileName = `DSGVO Auskunft (${response.data.custodian?.surname}, ${response.data.custodian?.firstname}).json`;
                FileSaver.saveAs(blob, fileName);

                return CustodianActions.generateGdprInformationSuccess();
              }),
              catchError((error) => {
                console.error(error);
                return of(
                  CustodianActions.generateGdprInformationFailure({ error })
                );
              })
            )
        )
      ),
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private store: Store,
    private custodianQuery: CustodianGQL,
    private updateCustodianMutation: UpdateCustodianGQL,
    private redeemEarlyAccessCodeMutation: RedeemEarlyAccessCodeGQL,
    private gdprInformationQuery: GdprInformationGQL
  ) {}
}
