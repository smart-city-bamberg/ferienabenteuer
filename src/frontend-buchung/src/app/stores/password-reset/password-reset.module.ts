import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import * as fromPasswordReset from './password-reset.reducer';
import { EffectsModule } from '@ngrx/effects';
import { PasswordResetEffects } from './password-reset.effects';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StoreModule.forFeature(fromPasswordReset.passwordResetFeatureKey, fromPasswordReset.reducer),
    EffectsModule.forFeature([PasswordResetEffects])
  ]
})
export class PasswordResetModule { }
