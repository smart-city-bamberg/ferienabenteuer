import { createActionGroup, emptyProps, props } from '@ngrx/store';

export const PasswordResetActions = createActionGroup({
  source: 'PasswordReset',
  events: {
    'Request Password Reset Token': emptyProps(),
    'Request Password Reset Token Success': emptyProps(),
    'Request Password Reset Token User Does Not Exists': emptyProps(),
    'Request Password Reset Token Failure': props<{ error: unknown }>(),

    'Update Token': props<{ token: string }>(),

    'Reset Password': emptyProps(),
    'Reset Password Rejected': emptyProps(),
    'Reset Password Repeat Must Equal': emptyProps(),
    'Reset Password Success': emptyProps(),
    'Reset Password Invalid Token': emptyProps(),
    'Reset Password Policy Not Matched': emptyProps(),
    'Reset Password Must Not Equal Old Password': emptyProps(),
    'Reset Password Failure': props<{ error: unknown }>(),
  },
});
