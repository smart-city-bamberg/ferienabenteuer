import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  catchError,
  map,
  concatMap,
  withLatestFrom,
  filter,
  tap,
} from 'rxjs/operators';
import { Observable, EMPTY, of } from 'rxjs';
import { PasswordResetActions } from './password-reset.actions';
import { Store } from '@ngrx/store';
import {
  RequestPasswordResetTokenGQL,
  ResetPasswordGQL,
} from 'src/app/graphql/generated';
import {
  selectResendEmail,
  selectResetPasswordForm,
} from './password-reset.selectors';
import { routerNavigatedAction } from '@ngrx/router-store';
import { Router } from '@angular/router';

@Injectable()
export class PasswordResetEffects {
  requestPasswordResetToken = createEffect(() =>
    this.actions$.pipe(
      ofType(PasswordResetActions.requestPasswordResetToken),
      withLatestFrom(this.store.select(selectResendEmail)),
      concatMap(([action, email]) =>
        this.requestPasswordResetTokenMutation
          .mutate({
            email,
          })
          .pipe(
            map((response) => {
              if (
                response.data?.requestPasswordResetToken.__typename ==
                'RequestPasswordResetTokenSuccess'
              ) {
                return PasswordResetActions.requestPasswordResetTokenSuccess();
              }

              if (
                response.data?.requestPasswordResetToken.__typename ==
                  'RequestPasswordResetTokenFailed' &&
                response.data.requestPasswordResetToken.reason ===
                  'UserDoesNotExists'
              ) {
                return PasswordResetActions.requestPasswordResetTokenUserDoesNotExists();
              }

              throw new Error('no response');
            }),
            catchError((error) =>
              of(
                PasswordResetActions.requestPasswordResetTokenFailure({ error })
              )
            )
          )
      )
    )
  );

  updateTokenAfterNavigate$ = createEffect(() =>
    this.actions$.pipe(
      ofType(routerNavigatedAction),
      filter((action) =>
        action.payload.routerState.url.startsWith('/neues-passwort')
      ),
      map((action) =>
        PasswordResetActions.updateToken({
          token: action.payload.routerState.root?.queryParams['token'] ?? '',
        })
      )
    )
  );

  resetPassword$ = createEffect(() =>
    this.actions$.pipe(
      ofType(PasswordResetActions.resetPassword),
      withLatestFrom(this.store.select(selectResetPasswordForm)),
      concatMap(([action, form]) =>
        form.value.newPassword !== form.value.newPasswordRepeat
          ? of(PasswordResetActions.resetPasswordRepeatMustEqual())
          : !form.isValid
          ? of(PasswordResetActions.resetPasswordRejected())
          : this.resetPasswordMutation
              .mutate({
                token: form.value.token,
                newPassword: form.value.newPassword,
              })
              .pipe(
                map((response) => {
                  if (
                    response.data?.resetPassword.__typename ==
                    'ResetPasswordSuccess'
                  ) {
                    return PasswordResetActions.resetPasswordSuccess();
                  }

                  if (
                    response.data?.resetPassword.__typename ==
                    'ResetPasswordFailed'
                  ) {
                    if (response.data?.resetPassword.reason == 'InvalidToken') {
                      return PasswordResetActions.resetPasswordInvalidToken();
                    }

                    if (
                      response.data?.resetPassword.reason ==
                      'PasswordMustNotEqualCurrentPassword'
                    ) {
                      return PasswordResetActions.resetPasswordMustNotEqualOldPassword();
                    }

                    if (
                      response.data?.resetPassword.reason ==
                      'PasswordPolicyNotMatched'
                    ) {
                      return PasswordResetActions.resetPasswordPolicyNotMatched();
                    }
                  }

                  throw new Error('no result');
                }),
                catchError((error) =>
                  of(PasswordResetActions.resetPasswordFailure({ error }))
                )
              )
      )
    )
  );

  redirectToNewPasswordAfterRequestTokenSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(PasswordResetActions.requestPasswordResetTokenSuccess),
        tap(() => this.router.navigate(['/neues-passwort']))
      ),
    {
      dispatch: false,
    }
  );

  redirectToLoginAfterVerificationSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(PasswordResetActions.resetPasswordSuccess),
        tap(() => this.router.navigate(['/login']))
      ),
    {
      dispatch: false,
    }
  );

  constructor(
    private actions$: Actions,
    private store: Store,
    private router: Router,
    private requestPasswordResetTokenMutation: RequestPasswordResetTokenGQL,
    private resetPasswordMutation: ResetPasswordGQL
  ) {}
}
