import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromPasswordReset from './password-reset.reducer';

export const selectPasswordResetState =
  createFeatureSelector<fromPasswordReset.State>(
    fromPasswordReset.passwordResetFeatureKey
  );

export const selectRequestPasswordResetTokenForm = createSelector(
  selectPasswordResetState,
  (state) => state.requestPasswordResetToken
);

export const selectResendEmail = createSelector(
  selectRequestPasswordResetTokenForm,
  (form) => form.value.email
);

export const selectResetPasswordForm = createSelector(
  selectPasswordResetState,
  (state) => state.resetPassword
);

export const selectResetPassword = createSelector(
  selectResetPasswordForm,
  (form) => form.value.newPassword
);
