import { createFeature, createReducer, on } from '@ngrx/store';
import { PasswordResetActions } from './password-reset.actions';
import {
  FormGroupState,
  createFormGroupState,
  onNgrxForms,
  setValue,
  updateGroup,
  validate,
  wrapReducerWithFormStateUpdate,
} from 'ngrx-forms';
import { equalTo, pattern, required } from 'ngrx-forms/validation';

export const passwordResetFeatureKey = 'passwordReset';

const requestPasswordResetToken = 'request_password_reset_form';
const resetPassword = 'reset_password_form';

interface RequestPasswordResetToken {
  email: string;
}

interface ResetPassword {
  token: string;
  newPassword: string;
  newPasswordRepeat: string;
}

export interface State {
  requestPasswordResetToken: FormGroupState<RequestPasswordResetToken>;
  resetPassword: FormGroupState<ResetPassword>;
}

export const initialState: State = {
  requestPasswordResetToken: createFormGroupState(requestPasswordResetToken, {
    email: '',
  }),
  resetPassword: createFormGroupState(resetPassword, {
    token: '',
    newPassword: '',
    newPasswordRepeat: '',
  }),
};

export const reducer = wrapReducerWithFormStateUpdate(
  wrapReducerWithFormStateUpdate(
    createReducer(
      initialState,
      on(PasswordResetActions.resetPasswordSuccess, (state, action) => ({
        ...initialState,
      })),
      on(PasswordResetActions.updateToken, (state, action) => ({
        ...state,
        resetPassword: setValue({
          ...state.resetPassword.value,
          token: action.token,
        })(state.resetPassword),
      })),
      onNgrxForms()
    ),
    (state) => state.requestPasswordResetToken,
    (state) =>
      updateGroup<RequestPasswordResetToken>({
        email: validate(required),
      })(state)
  ),
  (state) => state.resetPassword,
  (state) =>
    updateGroup<ResetPassword>({
      token: validate(required),
      newPassword: validate(
        required,
        pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{12,}$/)
      ),
      newPasswordRepeat: (x, y) =>
        validate(equalTo(y.controls.newPassword.value), required)(x),
    })(state)
);

export const passwordResetFeature = createFeature({
  name: passwordResetFeatureKey,
  reducer,
});
