import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { AvailableTicketCount, Event, Holiday, Season } from './events.types';

export const EventsActions = createActionGroup({
  source: 'Events',
  events: {
    'Load Events': emptyProps(),
    'Load Events Success': props<{
      events: Event[];
      holidays: Holiday[];
      seasons: Season[];
    }>(),
    'Load Events Failure': props<{ error: unknown }>(),
    'Select Season': props<{ year: number }>(),
    'Change Search Text': props<{ searchText: string }>(),
    'Toggle Holiday Filter': props<{ holidayId: string }>(),
    'Reset Holiday Filter': emptyProps(),
    'Toggle Category Filter': props<{ categoryId: string }>(),
    'Reset Category Filter': emptyProps(),

    'Select Free Slots Filter': props<{
      amount: number;
      slotType: 'participant' | 'waitinglist';
    }>(),

    'Load Available Ticket Count': emptyProps(),
    'Load Available Ticket Count Success': props<{
      availableTicketCount: AvailableTicketCount[];
    }>(),
    'Load Available Ticket Count Failure': props<{ error: unknown }>(),
    'Toggle Show Filter': emptyProps(),
  },
});
