import { createFeature, createReducer, on } from '@ngrx/store';
import { EventsActions } from './events.actions';
import { AvailableTicketCount, Event, Holiday, Season } from './events.types';

export const eventsFeatureKey = 'events';

export interface State {
  selectedSeason: number;
  events: Event[];
  holidays: Holiday[];
  seasons: Season[];
  searchText: string;
  holidayFilter: string[];
  categoryFilter: string[];
  freeSlotsFilter: { participant: number; waitinglist: number };
  availableTicketCounts: AvailableTicketCount[];
  showFilterMobile: boolean;
}

export const initialState: State = {
  selectedSeason: 2024,
  events: [],
  holidays: [],
  seasons: [],
  searchText: '',
  holidayFilter: [],
  categoryFilter: [],
  availableTicketCounts: [],
  freeSlotsFilter: {
    participant: 0,
    waitinglist: 0,
  },
  showFilterMobile: window.innerWidth >= 768,
};

export const reducer = createReducer(
  initialState,
  on(EventsActions.loadEvents, (state) => state),
  on(EventsActions.loadEventsSuccess, (state, action) => ({
    ...state,
    ...action,
  })),
  on(EventsActions.loadAvailableTicketCountSuccess, (state, action) => ({
    ...state,
    availableTicketCounts: [...action.availableTicketCount],
  })),
  on(EventsActions.loadEventsFailure, (state, action) => state),
  on(EventsActions.selectSeason, (state, action) => ({
    ...state,
    selectedSeason: action.year,
  })),
  on(EventsActions.changeSearchText, (state, action) => ({
    ...state,
    searchText: action.searchText,
  })),
  on(EventsActions.toggleHolidayFilter, (state, action) => ({
    ...state,
    holidayFilter: state.holidayFilter.includes(action.holidayId)
      ? state.holidayFilter.filter((holiday) => holiday !== action.holidayId)
      : [...state.holidayFilter, action.holidayId],
  })),
  on(EventsActions.resetHolidayFilter, (state) => ({
    ...state,
    holidayFilter: [],
  })),
  on(EventsActions.toggleCategoryFilter, (state, action) => ({
    ...state,
    categoryFilter: state.categoryFilter.includes(action.categoryId)
      ? state.categoryFilter.filter(
          (category) => category !== action.categoryId
        )
      : [...state.categoryFilter, action.categoryId],
  })),
  on(EventsActions.resetCategoryFilter, (state) => ({
    ...state,
    categoryFilter: [],
  })),
  on(EventsActions.selectFreeSlotsFilter, (state, action) => ({
    ...state,
    freeSlotsFilter: {
      ...state.freeSlotsFilter,
      [action.slotType]: action.amount,
    },
  })),
  on(EventsActions.toggleShowFilter, (state) => ({
    ...state,
    showFilterMobile: !state.showFilterMobile,
  }))
);

export const eventsFeature = createFeature({
  name: eventsFeatureKey,
  reducer,
});
