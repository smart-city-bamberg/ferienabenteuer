export interface Organizer {
  name: string;
}

export interface Season {
  id: string;
  year: number;
}

export interface Holiday {
  id: string;
  name: string;
  periods: Period[];
}

export interface Category {
  id: string;
  name: string;
}

export interface Period {
  id: string;
  startDate: string;
  endDate: string;
}

export interface CarePeriod {
  period: {
    id: string;
  };
  careDays: CareDay[];
}

export interface CareDay {
  day: string;
}

export interface Event {
  id: string;
  name: string;
  ageMinimum: number;
  ageMaximum: number;
  costs: number;
  picture:
    | {
        url: string;
      }
    | undefined;
  organizer: Organizer;
  locationCity: string;
  locationStreet: string;
  locationZipcode: string;
  categories: Category[];
  carePeriodCommitted: CarePeriod;
  waitingListLimit: number;
  participantLimit: number;
}

export interface AvailableTicketCount {
  eventId: string;
  participant: number;
  waitinglist: number;
}
