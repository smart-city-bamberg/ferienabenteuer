import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  catchError,
  map,
  concatMap,
  withLatestFrom,
  switchMap,
  takeUntil,
} from 'rxjs/operators';
import { of, filter, timer } from 'rxjs';
import { EventsActions } from './events.actions';
import { AvailableTicketCountGQL, EventsGQL } from 'src/app/graphql/generated';
import { AvailableTicketCount, Event, Holiday, Season } from './events.types';
import { Store } from '@ngrx/store';
import { selectSelectedSeason } from './events.selectors';
import { routerNavigatedAction } from '@ngrx/router-store';

@Injectable()
export class EventsEffects {
  loadEvents$ = createEffect(() =>
    this.actions$.pipe(
      ofType(EventsActions.loadEvents),
      withLatestFrom(this.store.select(selectSelectedSeason)),
      concatMap(([action, season]) =>
        this.eventsQuery
          .fetch({ year: season }, { fetchPolicy: 'cache-first' })
          .pipe(
            map((response) =>
              EventsActions.loadEventsSuccess({
                holidays: (response.data?.holidays as Holiday[]) ?? [],
                events: (response.data.events as Event[]) ?? [],
                seasons: (response.data.seasons as Season[]) ?? [],
              })
            ),
            catchError((error) =>
              of(EventsActions.loadEventsFailure({ error }))
            )
          )
      )
    )
  );

  loadAvailableTicketCount$ = createEffect(() =>
    this.actions$.pipe(
      ofType(EventsActions.loadAvailableTicketCount),
      withLatestFrom(this.store.select(selectSelectedSeason)),
      concatMap(([action, season]) =>
        this.availableTicketCountQuery.fetch({ year: season }).pipe(
          map((response) =>
            EventsActions.loadAvailableTicketCountSuccess({
              availableTicketCount: response.data
                .availableTicketCount as AvailableTicketCount[],
            })
          ),
          catchError((error) =>
            of(EventsActions.loadAvailableTicketCountFailure({ error }))
          )
        )
      )
    )
  );

  loadEventsWhenSaisonSelected$ = createEffect(() =>
    this.actions$.pipe(
      ofType(EventsActions.selectSeason),
      map(() => EventsActions.loadEvents())
    )
  );

  loadAvailableTicketCountsWhenLoadEvents$ = createEffect(() =>
    this.actions$.pipe(
      ofType(EventsActions.loadEvents),
      switchMap((action) =>
        timer(0, 10000).pipe(
          takeUntil(this.actions$.pipe(ofType(routerNavigatedAction))),
          map(() => EventsActions.loadAvailableTicketCount())
        )
      )
    )
  );

  loadEventsAfterNavigate$ = createEffect(() =>
    this.actions$.pipe(
      ofType(routerNavigatedAction),
      filter((action) => action.payload.routerState.url === '/veranstaltungen'),
      map((action) => EventsActions.loadEvents())
    )
  );

  constructor(
    private actions$: Actions,
    private store: Store,
    private eventsQuery: EventsGQL,
    private availableTicketCountQuery: AvailableTicketCountGQL
  ) {}
}
