import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromEvents from './events.reducer';
import * as fromRouter from '@ngrx/router-store';
import { AvailableTicketCount, Category, Period } from './events.types';

const now = new Date();
const offset = now.getTimezoneOffset();
const nowWithTimezone = new Date(now.getTime() - offset * 60 * 1000);
const day = nowWithTimezone.toISOString().split('T')[0];

export const selectEventsState = createFeatureSelector<fromEvents.State>(
  fromEvents.eventsFeatureKey
);

const { selectRouteParams } = fromRouter.getRouterSelectors();

export const selectEventId = createSelector(
  selectRouteParams,
  (params) => params?.['eventId']
);

export const selectSelectedSeason = createSelector(
  selectEventsState,
  (state) => state.selectedSeason
);

export const selectSeasons = createSelector(selectEventsState, (state) =>
  [...state.seasons].sort((a, b) => a.year - b.year)
);

export const selectHolidays = createSelector(
  selectEventsState,
  (state) => state?.holidays ?? []
);

export const selectHolidayFilter = createSelector(
  selectEventsState,
  (state) => state.holidayFilter
);

export const selectHolidayFilterEmpty = createSelector(
  selectHolidayFilter,
  (holidayFilter) => holidayFilter.length === 0
);

export const selectEvents = createSelector(
  selectEventsState,
  (state) => state.events
);

export const selectEventsWithSearchIndex = createSelector(
  selectEvents,
  (events) =>
    events.map((event) => ({
      ...event,
      isExpired:
        day.localeCompare(event.carePeriodCommitted.careDays[0].day) > 0,
      searchIndex: [
        event.id,
        event.name,
        event.organizer.name,
        event.costs.toString(),
      ].map((value) => value.toLowerCase()),
    }))
);

export const selectAvailableTicketCounts = createSelector(
  selectEventsState,
  (state) =>
    state.availableTicketCounts.reduce(
      (sum, next) => ({
        ...sum,
        [next.eventId]: next,
      }),
      {} as { [index: string]: AvailableTicketCount }
    )
);

export const selectEventsWithTicketCounts = createSelector(
  selectEventsWithSearchIndex,
  selectAvailableTicketCounts,
  (events, counts) =>
    events.map((event) => ({
      ...event,
      availableTicketCountParticipant: counts[event.id]?.participant ?? 0,
      availableTicketCountWaitinglist: counts[event.id]?.waitinglist ?? 0,
    }))
);

export const selectSearchText = createSelector(
  selectEventsState,
  (state) => state.searchText
);

export const selectCategoryFilter = createSelector(
  selectEventsState,
  (state) => state.categoryFilter
);

export const selectCategoryFilterEmpty = createSelector(
  selectCategoryFilter,
  (categoryFilter) => categoryFilter.length === 0
);

export const selectFreeSlotsFilters = createSelector(
  selectEventsState,
  (state) => state.freeSlotsFilter
);

export const selectFreeSlotsFilter = (
  slotType: 'participant' | 'waitinglist'
) =>
  createSelector(selectEventsState, (state) => state.freeSlotsFilter[slotType]);

export const selectFilteredEvents = createSelector(
  selectEventsWithTicketCounts,
  selectSearchText,
  selectCategoryFilter,
  selectCategoryFilterEmpty,
  (events, searchText, categoryFilter, categoryFilterEmpty) =>
    events.filter(
      (event) =>
        (searchText.length === 0 ||
          event.searchIndex.some((index) =>
            index.includes(searchText.toLowerCase())
          )) &&
        (categoryFilterEmpty ||
          event.categories.some((category) =>
            categoryFilter.includes(category.id)
          ))
    )
);

export const selectFilteredEventsWithFreeSlots = createSelector(
  selectFilteredEvents,
  selectFreeSlotsFilters,
  (events, freeSlotsFilters) =>
    events.filter(
      (event) =>
        event.availableTicketCountParticipant >= freeSlotsFilters.participant &&
        event.availableTicketCountWaitinglist >= freeSlotsFilters.waitinglist
    )
);

export const selectSortedHolidays = createSelector(selectHolidays, (holidays) =>
  [...(holidays ?? [])]
    .filter((holiday) => holiday.periods.length > 0)
    .sort(
      (a, b) =>
        new Date(a.periods[0].startDate).getTime() -
        new Date(b.periods[0].startDate).getTime()
    )
);

export const selectHolidayFiltersWithSelection = createSelector(
  selectSortedHolidays,
  selectHolidayFilter,
  (holidays, holidayFilter) =>
    holidays.map((holiday) => ({
      id: holiday.id,
      name: holiday.name,
      selected: holidayFilter.includes(holiday.id),
    }))
);

export const selectFilteredSortedHolidays = createSelector(
  selectSortedHolidays,
  selectHolidayFilter,
  (holidays, holidayFilter) =>
    holidayFilter.length === 0
      ? holidays
      : holidays.filter((holiday) => holidayFilter.includes(holiday.id))
);

export const selectHolidaysWithEvents = createSelector(
  selectFilteredSortedHolidays,
  selectFilteredEventsWithFreeSlots,
  (holidays, events) =>
    holidays.map((holiday) => {
      const periods = holiday.periods
        .map((period) => ({
          ...period,
          events: events
            .filter(
              (event) => event.carePeriodCommitted.period.id === period.id
            )
            .sort(
              (a, b) =>
                a.carePeriodCommitted.careDays[0].day.localeCompare(
                  b.carePeriodCommitted.careDays[0].day
                ) || a.name.localeCompare(b.name)
            ),
        }))
        .filter((period) => period.events.length > 0)
        .sort((a, b) => a.startDate.localeCompare(b.startDate));

      return {
        ...holiday,
        periods,
        isEmpty: periods.every((period) => period.events.length === 0),
      };
    })
);

export const selectEventCount = createSelector(
  selectHolidaysWithEvents,
  (holidays) =>
    holidays.reduce(
      (count, holiday) =>
        count +
        holiday.periods.reduce((sum, period) => sum + period.events.length, 0),
      0
    )
);

export const selectEvent = createSelector(
  selectEvents,
  selectEventId,
  (events, eventId) => ({ ...events.find((event) => event.id == eventId) })
);

export const selectPeriods = createSelector(selectHolidays, (holidays) =>
  holidays.reduce((all, next) => [...all, ...next.periods], new Array<Period>())
);

export const selectHoliday = createSelector(
  selectHolidays,
  selectEvent,
  (holidays, event) =>
    holidays.find((holiday) =>
      holiday.periods.find(
        (period) => period.id === event.carePeriodCommitted?.period.id
      )
    )
);

export const selectPeriod = createSelector(
  selectPeriods,
  selectEvent,
  (periods, event) =>
    periods.find((period) => period.id === event.carePeriodCommitted?.period.id)
);

export const selectAllCategories = createSelector(selectEvents, (events) => {
  const allCategories: Category[] = [];
  events.forEach((event) => {
    event.categories.forEach((category) => {
      if (!allCategories.some((c) => c.id === category.id)) {
        allCategories.push(category);
      }
    });
  });
  return allCategories;
});

export const selectCategoryFiltersWithSelection = createSelector(
  selectAllCategories,
  selectCategoryFilter,
  (categories, categoryFilter) =>
    categories.map((category) => ({
      id: category.id,
      name: category.name,
      selected: categoryFilter.includes(category.id),
    }))
);

export const selectShowFilterMobile = createSelector(
  selectEventsState,
  (state) => state.showFilterMobile
);

export const selectBothFreeSlotsFiltersZero = createSelector(
  selectFreeSlotsFilters,
  (freeSlotFilters) =>
    freeSlotFilters.participant === 0 && freeSlotFilters.waitinglist === 0
);

export const selectActiveFilter = createSelector(
  selectHolidayFilterEmpty,
  selectCategoryFilterEmpty,
  selectSearchText,
  selectBothFreeSlotsFiltersZero,
  (
    holidayFilterEmpty,
    categoryFilterEmpty,
    searchText,
    bothFreeSlotsFiltersZero
  ) =>
    !holidayFilterEmpty ||
    !categoryFilterEmpty ||
    searchText.trim() !== '' ||
    !bothFreeSlotsFiltersZero
);

export const selectFiltersCount = createSelector(
  selectSearchText,
  selectCategoryFilter,
  selectFreeSlotsFilters,
  selectHolidayFilter,
  (searchText, categoryFilter, freeSlotsFilter, holidayFilter) => {
    const freeSlotsCount =
      freeSlotsFilter.participant >= 1 && freeSlotsFilter.waitinglist >= 1
        ? 2
        : freeSlotsFilter.participant >= 1 || freeSlotsFilter.waitinglist >= 1
        ? 1
        : 0;

    return (
      (searchText.length !== 0 ? 1 : 0) +
      categoryFilter.length +
      freeSlotsCount +
      holidayFilter.length
    );
  }
);
