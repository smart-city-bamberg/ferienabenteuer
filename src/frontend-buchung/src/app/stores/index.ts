import { isDevMode } from '@angular/core';
import { ActionReducerMap, MetaReducer } from '@ngrx/store';
import * as fromAuth from './auth/auth.reducer';
import * as fromEvents from './events/events.reducer';
import * as routerStore from '@ngrx/router-store';
import * as fromCustodian from './custodian/custodian.reducer';
import * as fromCheckout from './checkout/checkout.reducer';
import * as fromChildren from './children/children.reducer';
import * as fromDiscounts from './discounts/discounts.reducer';
import * as fromEvent from './event/event.reducer';
import * as fromContacts from './contacts/contacts.reducer';
import * as fromRegistration from './registration/registration.reducer';
import * as fromBookings from './bookings/bookings.reducer';
import * as fromVerification from './verification/verification.reducer';
import * as fromSecurity from './security/security.reducer';
import * as fromLanding from './landing/landing.reducer';
import * as fromPasswordReset from './password-reset/password-reset.reducer';

export interface State {
  router: routerStore.RouterReducerState<any>;
  [fromAuth.authFeatureKey]: fromAuth.State;
  [fromEvents.eventsFeatureKey]: fromEvents.State;
  [fromCustodian.custodianFeatureKey]: fromCustodian.State;
  [fromCheckout.checkoutFeatureKey]: fromCheckout.State;  [fromChildren.childrenFeatureKey]: fromChildren.State;

[fromDiscounts.discountsFeatureKey]: fromDiscounts.State;
[fromEvent.eventFeatureKey]: fromEvent.State;
[fromContacts.contactsFeatureKey]: fromContacts.State;
[fromRegistration.registrationFeatureKey]: fromRegistration.State;
[fromBookings.bookingsFeatureKey]: fromBookings.State;
[fromVerification.verificationFeatureKey]: fromVerification.State;
[fromSecurity.securityFeatureKey]: fromSecurity.State;
[fromLanding.landingFeatureKey]: fromLanding.State;
[fromPasswordReset.passwordResetFeatureKey]: fromPasswordReset.State;
}

export const reducers: ActionReducerMap<State> = {
  router: routerStore.routerReducer,
  [fromAuth.authFeatureKey]: fromAuth.reducer,
  [fromEvents.eventsFeatureKey]: fromEvents.reducer,
  [fromCustodian.custodianFeatureKey]: fromCustodian.reducer,
  [fromCheckout.checkoutFeatureKey]: fromCheckout.reducer,
  [fromChildren.childrenFeatureKey]: fromChildren.reducer,
  [fromDiscounts.discountsFeatureKey]: fromDiscounts.reducer,
  [fromEvent.eventFeatureKey]: fromEvent.reducer,
  [fromContacts.contactsFeatureKey]: fromContacts.reducer,
  [fromRegistration.registrationFeatureKey]: fromRegistration.reducer,
  [fromBookings.bookingsFeatureKey]: fromBookings.reducer,
  [fromVerification.verificationFeatureKey]: fromVerification.reducer,
  [fromSecurity.securityFeatureKey]: fromSecurity.reducer,
  [fromLanding.landingFeatureKey]: fromLanding.reducer,
  [fromPasswordReset.passwordResetFeatureKey]: fromPasswordReset.reducer,
};

export const metaReducers: MetaReducer<State>[] = isDevMode() ? [] : [];
