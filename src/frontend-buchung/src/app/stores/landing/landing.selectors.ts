import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromLanding from './landing.reducer';

export const selectLandingState = createFeatureSelector<fromLanding.State>(
  fromLanding.landingFeatureKey
);

export const selectSeason = createSelector(
  selectLandingState,
  (state) => state.season
);

export const selectBookingStart = createSelector(
  selectSeason,
  (season) => season?.bookingStart ?? ''
);
