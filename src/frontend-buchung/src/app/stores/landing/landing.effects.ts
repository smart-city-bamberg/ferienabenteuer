import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, map, concatMap, filter } from 'rxjs/operators';
import { of } from 'rxjs';
import { LandingGQL } from 'src/app/graphql/generated';
import { Season } from './landing.types';
import { LandingActions } from './landing.actions';
import { routerNavigatedAction } from '@ngrx/router-store';

const now = new Date();

@Injectable()
export class LandingEffects {
  loadLandings$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(LandingActions.loadLanding),
      concatMap(() =>
        this.landingQuery
          .fetch(
            {
              timestamp: now,
            },
            { fetchPolicy: 'cache-first' }
          )
          .pipe(
            map((response) =>
              LandingActions.loadLandingSuccess({
                seasons: response.data.seasons as Season[],
              })
            ),
            catchError((error) =>
              of(LandingActions.loadLandingFailure({ error }))
            )
          )
      )
    );
  });

  loadLandingAfterNavigate$ = createEffect(() =>
    this.actions$.pipe(
      ofType(routerNavigatedAction),
      filter((action) => action.payload.routerState.url === '/'),
      map((action) => LandingActions.loadLanding())
    )
  );

  constructor(private actions$: Actions, private landingQuery: LandingGQL) {}
}
