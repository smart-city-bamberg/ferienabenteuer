export interface Season {
  id: string;
  year: number;
  bookingStart: string;
  bookingStartPartners: string;
  releaseDate: string;
}
