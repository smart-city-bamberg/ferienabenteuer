import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { Season } from './landing.types';

export const LandingActions = createActionGroup({
  source: 'Landing',
  events: {
    'Load Landing': emptyProps(),
    'Load Landing Success': props<{ seasons: Season[] }>(),
    'Load Landing Failure': props<{ error: unknown }>(),
  },
});
