import { createFeature, createReducer, on } from '@ngrx/store';
import { LandingActions } from './landing.actions';
import { Season } from './landing.types';

export const landingFeatureKey = 'landing';

export interface State {
  season: Season | undefined;
}

export const initialState: State = {
  season: undefined,
};

export const reducer = createReducer(
  initialState,
  on(LandingActions.loadLanding, (state) => state),
  on(LandingActions.loadLandingSuccess, (state, action) => ({
    ...state,
    season: action.seasons.length > 0 ? { ...action.seasons[0] } : undefined,
  })),
  on(LandingActions.loadLandingFailure, (state, action) => state)
);

export const landingFeature = createFeature({
  name: landingFeatureKey,
  reducer,
});
