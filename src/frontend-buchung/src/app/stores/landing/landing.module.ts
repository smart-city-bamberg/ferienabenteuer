import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import * as fromLanding from './landing.reducer';
import { EffectsModule } from '@ngrx/effects';
import { LandingEffects } from './landing.effects';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StoreModule.forFeature(fromLanding.landingFeatureKey, fromLanding.reducer),
    EffectsModule.forFeature([LandingEffects])
  ]
})
export class LandingModule { }
