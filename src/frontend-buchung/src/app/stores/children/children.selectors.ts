import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromChildren from './children.reducer';

export const selectChildrenState = createFeatureSelector<fromChildren.State>(
  fromChildren.childrenFeatureKey
);

export const selectChildren = createSelector(
  selectChildrenState,
  (state) => state.children
);

export const selectChildForm = createSelector(
  selectChildrenState,
  (state) => state.form
);

export const selectChildFormExists = createSelector(
  selectChildrenState,
  (state) => state.form.value.id !== ''
);

export const selectNoChildren = createSelector(
  selectChildren,
  (children) => children.length === 0
);
