import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { Child } from './children.types';

export const ChildrenActions = createActionGroup({
  source: 'Children',
  events: {
    'Load Children': emptyProps(),
    'Load Children Success': props<{ children: Child[] }>(),
    'Load Children Failure': props<{ error: unknown }>(),

    'Load Child': props<{ id: string }>(),
    'Load Child Success': props<{ child: Child }>(),
    'Load Child Failure': props<{ error: unknown }>(),

    'Save Child': emptyProps(),
    'Save Child Success': emptyProps(),
    'Save Child Failure': props<{ error: unknown }>(),
    'Save Child Rejected': emptyProps(),

    'Reset Form': emptyProps(),

    'Delete Child': props<{ childId: string }>(),
    'Delete Child Success': props<{ childId: string }>(),
    'Delete Child Rejected': emptyProps(),
    'Delete Child Failure': props<{ error: unknown }>(),
  },
});
