export interface Child {
  id: string;
  firstname: string;
  surname: string;
  dateOfBirth: string;
  gender: string;
  healthInsurer: string;
  hasLiabilityInsurance: boolean;
  assistanceRequired: boolean;
  physicalImpairment: string;
  medication: string;
  foodIntolerances: string;
  dietaryRegulations: string;
  miscellaneous: string;
}
