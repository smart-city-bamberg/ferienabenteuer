import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  catchError,
  map,
  concatMap,
  filter,
  withLatestFrom,
  tap,
} from 'rxjs/operators';
import { of } from 'rxjs';
import { ChildrenActions } from './children.actions';
import { Store } from '@ngrx/store';
import {
  ChildGQL,
  ChildrenGQL,
  CreateChildGQL,
  MarkChildDeletedGQL,
  UpdateChildGQL,
} from 'src/app/graphql/generated';
import { routerNavigatedAction } from '@ngrx/router-store';
import { Child } from './children.types';
import { selectChildForm } from './children.selectors';
import { Router } from '@angular/router';

@Injectable()
export class ChildrenEffects {
  loadChildren$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ChildrenActions.loadChildren),
      concatMap(() =>
        this.childrenQuery.fetch().pipe(
          map((response) =>
            ChildrenActions.loadChildrenSuccess({
              children: response.data.children as Child[],
            })
          ),
          catchError((error) =>
            of(ChildrenActions.loadChildrenFailure({ error }))
          )
        )
      )
    )
  );

  loadChild$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ChildrenActions.loadChild),
      concatMap((action) =>
        this.childQuery
          .fetch({
            id: action.id,
          })
          .pipe(
            map((response) =>
              ChildrenActions.loadChildSuccess({
                child: response.data.child as Child,
              })
            ),
            catchError((error) =>
              of(ChildrenActions.loadChildFailure({ error }))
            )
          )
      )
    )
  );

  saveChild$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ChildrenActions.saveChild),
      withLatestFrom(this.store.select(selectChildForm)),
      concatMap(([action, child]) =>
        !child.isValid
          ? of(ChildrenActions.saveChildRejected())
          : child.value.id
          ? this.updateChildMutation
              .mutate({
                id: child.value.id,
                data: {
                  firstname: child.value.firstname,
                  surname: child.value.surname,
                  gender: child.value.gender,
                  healthInsurer: child.value.healthInsurer,
                  hasLiabilityInsurance: child.value.hasLiabilityInsurance,
                  assistanceRequired: child.value.assistanceRequired,
                  physicalImpairment: child.value.physicalImpairment,
                  medication: child.value.medication,
                  foodIntolerances: child.value.foodIntolerances,
                  dietaryRegulations: child.value.dietaryRegulations,
                  miscellaneous: child.value.miscellaneous,
                  dateOfBirth: child.value.dateOfBirth,
                },
              })
              .pipe(
                map((response) => ChildrenActions.saveChildSuccess()),
                catchError((error) =>
                  of(ChildrenActions.saveChildFailure({ error }))
                )
              )
          : this.createChildMutation
              .mutate({
                data: {
                  firstname: child.value.firstname,
                  surname: child.value.surname,
                  gender: child.value.gender,
                  healthInsurer: child.value.healthInsurer,
                  hasLiabilityInsurance: child.value.hasLiabilityInsurance,
                  assistanceRequired: child.value.assistanceRequired,
                  physicalImpairment: child.value.physicalImpairment,
                  medication: child.value.medication,
                  foodIntolerances: child.value.foodIntolerances,
                  dietaryRegulations: child.value.dietaryRegulations,
                  miscellaneous: child.value.miscellaneous,
                  dateOfBirth: child.value.dateOfBirth,
                },
              })
              .pipe(
                map((response) => ChildrenActions.saveChildSuccess()),
                catchError((error) =>
                  of(ChildrenActions.saveChildFailure({ error }))
                )
              )
      )
    )
  );

  deleteChild$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ChildrenActions.deleteChild),
      concatMap((action) =>
        confirm('Wollen Sie das Kind wirklich löschen?')
          ? this.deleteChild
              .mutate({
                childId: action.childId,
              })
              .pipe(
                map((response) =>
                  ChildrenActions.deleteChildSuccess({
                    childId: response.data?.markChildDeleted?.id as string,
                  })
                ),
                catchError((error) =>
                  of(ChildrenActions.deleteChildFailure({ error }))
                )
              )
          : of(ChildrenActions.deleteChildRejected())
      )
    )
  );

  loadChildrenAfterNavigate$ = createEffect(() =>
    this.actions$.pipe(
      ofType(routerNavigatedAction),
      filter((action) => action.payload.routerState.url === '/kinder'),
      map((action) => ChildrenActions.loadChildren())
    )
  );

  resetFormAfterNavigate$ = createEffect(() =>
    this.actions$.pipe(
      ofType(routerNavigatedAction),
      filter((action) => action.payload.routerState.url === '/kind-eintragen'),
      map((action) => ChildrenActions.resetForm())
    )
  );

  redirectToChildrenWhenSaveSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(ChildrenActions.saveChildSuccess),
        tap(() => this.router.navigate(['/kinder']))
      ),
    {
      dispatch: false,
    }
  );

  loadChildAfterNavigate$ = createEffect(() =>
    this.actions$.pipe(
      ofType(routerNavigatedAction),
      filter((action) => action.payload.routerState.url.startsWith('/kinder/')),
      map((action) =>
        ChildrenActions.loadChild({
          id: action.payload.routerState.url.replace('/kinder/', ''),
        })
      )
    )
  );

  constructor(
    private actions$: Actions,
    private store: Store,
    private router: Router,
    private childrenQuery: ChildrenGQL,
    private createChildMutation: CreateChildGQL,
    private updateChildMutation: UpdateChildGQL,
    private childQuery: ChildGQL,
    private deleteChild: MarkChildDeletedGQL
  ) {}
}
