import { createFeature, createReducer, on } from '@ngrx/store';
import { ChildrenActions } from './children.actions';
import { Child } from './children.types';
import {
  FormGroupState,
  createFormGroupState,
  onNgrxForms,
  setValue,
  updateGroup,
  validate,
  wrapReducerWithFormStateUpdate,
} from 'ngrx-forms';
import { required } from 'ngrx-forms/validation';

export const childrenFeatureKey = 'children';

export interface ChildForm {
  id: string | undefined;
  firstname: string | undefined;
  surname: string | undefined;
  dateOfBirth: string | undefined;
  gender: string | undefined;
  healthInsurer: string | undefined;
  hasLiabilityInsurance: boolean | undefined;
  assistanceRequired: boolean | undefined;
  physicalImpairment: string | undefined;
  medication: string | undefined;
  foodIntolerances: string | undefined;
  dietaryRegulations: string | undefined;
  miscellaneous: string | undefined;
}

export interface State {
  children: Child[];
  form: FormGroupState<ChildForm>;
}

export const initialState: State = {
  children: [],
  form: createFormGroupState('child-form', {
    id: undefined,
    firstname: undefined,
    surname: undefined,
    dateOfBirth: undefined,
    gender: undefined,
    healthInsurer: undefined,
    hasLiabilityInsurance: undefined,
    assistanceRequired: undefined,
    physicalImpairment: undefined,
    medication: undefined,
    foodIntolerances: undefined,
    dietaryRegulations: undefined,
    miscellaneous: undefined,
  }),
};

export const reducer = wrapReducerWithFormStateUpdate(
  createReducer(
    initialState,
    on(ChildrenActions.loadChildrenSuccess, (state, action) => ({
      ...state,
      children: action.children,
    })),
    on(ChildrenActions.deleteChildSuccess, (state, action) => ({
      ...state,
      children: state.children.filter((child) => child.id !== action.childId),
    })),
    on(ChildrenActions.resetForm, (state, action) => ({
      ...state,
      form: setValue({
        ...initialState.form.value,
      })(state.form),
    })),
    on(ChildrenActions.loadChildSuccess, (state, action) => ({
      ...state,
      form: setValue({
        ...(action.child as ChildForm),
      })(state.form),
    })),
    onNgrxForms()
  ),
  (state) => state.form,
  (state) =>
    updateGroup<ChildForm>({
      firstname: validate(required),
      surname: validate(required),
      gender: validate(required),
      dateOfBirth: validate((value: string | undefined) => {
        if (!value) {
          return 'Geburtsdatum ist erforderlich.';
        }

        const selectedDate = new Date(value);
        const currentDate = new Date();

        if (selectedDate > currentDate) {
          return 'Das Geburtsdatum kann nicht in der Zukunft liegen.';
        }

        return {};
      }),
      healthInsurer: validate(required),
      hasLiabilityInsurance: validate(required),
      assistanceRequired: validate(required),
    })(state)
);

export const childrenFeature = createFeature({
  name: childrenFeatureKey,
  reducer,
});
