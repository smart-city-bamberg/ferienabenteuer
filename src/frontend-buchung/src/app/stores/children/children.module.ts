import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import * as fromChildren from './children.reducer';
import { EffectsModule } from '@ngrx/effects';
import { ChildrenEffects } from './children.effects';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StoreModule.forFeature(fromChildren.childrenFeatureKey, fromChildren.reducer),
    EffectsModule.forFeature([ChildrenEffects])
  ]
})
export class ChildrenModule { }
