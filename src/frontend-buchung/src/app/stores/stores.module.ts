import { NgModule, isDevMode } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GraphQLModule } from '../graphql/graphql.module';
import { StoreModule } from '@ngrx/store';
import { metaReducers, reducers } from '.';
import { EffectsModule } from '@ngrx/effects';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { EventsModule } from './events/events.module';
import { CustodianModule } from './custodian/custodian.module';
import { AuthModule } from './auth/auth.module';
import { CheckoutModule } from './checkout/checkout.module';
import { NgrxFormsModule } from 'ngrx-forms';
import { ChildrenModule } from './children/children.module';
import { DiscountsModule } from './discounts/discounts.module';
import { EventModule } from './event/event.module';
import { ContactsModule } from './contacts/contacts.module';
import { ToastEffects } from './toasts.effects';
import { RegistrationModule } from './registration/registration.module';
import { LandingPageEffects } from './landing-page.effects';
import { BookingsModule } from './bookings/bookings.module';
import { VerificationModule } from './verification/verification.module';
import { SecurityModule } from './security/security.module';
import { LandingModule } from './landing/landing.module';
import { PasswordResetModule } from './password-reset/password-reset.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    GraphQLModule,
    StoreModule.forRoot(reducers, {
      metaReducers,
    }),
    EffectsModule.forRoot([]),
    StoreRouterConnectingModule.forRoot(),
    StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: !isDevMode() }),
    EventsModule,
    EventModule,
    CustodianModule,
    AuthModule,
    ChildrenModule,
    CheckoutModule,
    DiscountsModule,
    ContactsModule,
    RegistrationModule,
    BookingsModule,
    VerificationModule,
    SecurityModule,
    PasswordResetModule,
    LandingModule,
    NgrxFormsModule,
    EffectsModule.forFeature([ToastEffects]),
    EffectsModule.forFeature([LandingPageEffects]),
  ],
})
export class StoresModule {}
