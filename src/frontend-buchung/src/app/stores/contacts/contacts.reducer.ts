import { createFeature, createReducer, on } from '@ngrx/store';
import { ContactsActions } from './contacts.actions';
import { Contact, Organizer } from './contacts.types';

export const contactsFeatureKey = 'contacts';

export interface State {
  contacts: Contact[];
  organizers: Organizer[];
}

export const initialState: State = {
  contacts: [],
  organizers: [],
};

export const reducer = createReducer(
  initialState,
  on(ContactsActions.loadContacts, (state) => state),
  on(ContactsActions.loadContactsSuccess, (state, action) => ({
    ...state,
    contacts: [...action.contacts],
    organizers: [...action.organizers],
  })),
  on(ContactsActions.loadContactsFailure, (state, action) => state)
);

export const contactsFeature = createFeature({
  name: contactsFeatureKey,
  reducer,
});
