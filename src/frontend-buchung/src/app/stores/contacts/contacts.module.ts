import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import * as fromContacts from './contacts.reducer';
import { EffectsModule } from '@ngrx/effects';
import { ContactsEffects } from './contacts.effects';

@NgModule({
  imports: [CommonModule, StoreModule.forFeature(fromContacts.contactsFeatureKey, fromContacts.reducer), EffectsModule.forFeature([ContactsEffects])],
  declarations: [],
})
export class ContactsModule {}
