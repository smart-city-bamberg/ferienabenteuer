import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  catchError,
  map,
  concatMap,
  filter,
  withLatestFrom,
} from 'rxjs/operators';
import { of } from 'rxjs';
import { ContactsActions } from './contacts.actions';
import { ContactsGQL } from 'src/app/graphql/generated';
import { Contact, Organizer } from './contacts.types';
import { routerNavigatedAction } from '@ngrx/router-store';
import { Store } from '@ngrx/store';
import { selectSelectedSeason } from '../events/events.selectors';

@Injectable()
export class ContactsEffects {
  loadContacts$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ContactsActions.loadContacts),
      withLatestFrom(this.store.select(selectSelectedSeason)),
      concatMap(([action, season]) =>
        this.contactsQuery
          .fetch(
            {
              year: season,
            },
            { fetchPolicy: 'cache-first' }
          )
          .pipe(
            map((response) =>
              ContactsActions.loadContactsSuccess({
                contacts: response.data.contacts as Contact[],
                organizers: response.data.organizers as Organizer[],
              })
            ),
            catchError((error) =>
              of(ContactsActions.loadContactsFailure({ error }))
            )
          )
      )
    );
  });

  loadContactsAfterNavigate$ = createEffect(() =>
    this.actions$.pipe(
      ofType(routerNavigatedAction),
      filter((action) =>
        ['/kontakt'].some((url) => action.payload.routerState.url === url)
      ),
      map((action) => ContactsActions.loadContacts())
    )
  );

  constructor(
    private actions$: Actions,
    private store: Store,
    private contactsQuery: ContactsGQL
  ) {}
}
