import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { Contact, Organizer } from './contacts.types';

export const ContactsActions = createActionGroup({
  source: 'Contacts',
  events: {
    'Load Contacts': emptyProps(),
    'Load Contacts Success': props<{
      contacts: Contact[];
      organizers: Organizer[];
    }>(),
    'Load Contacts Failure': props<{ error: unknown }>(),
  },
});
