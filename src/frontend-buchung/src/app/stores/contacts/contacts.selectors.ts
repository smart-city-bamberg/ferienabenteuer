import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromContacts from './contacts.reducer';

export const selectContactsState = createFeatureSelector<fromContacts.State>(
  fromContacts.contactsFeatureKey
);

export const selectContacts = createSelector(
  selectContactsState,
  (state) => state.contacts
);

export const selectOrganizers = createSelector(
  selectContactsState,
  (state) => state.organizers
);
