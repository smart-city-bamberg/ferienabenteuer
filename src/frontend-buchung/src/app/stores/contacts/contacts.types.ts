export interface Contact {
  id: string;
  name: string;
  description: string;
  role: string;
  contactPersonFirstname: string;
  contactPersonSurname: string;
  addressStreet: string;
  addressZip: string;
  addressCity: string;
  email: string;
  phone: string;
}

export interface Organizer {
  id: string;
  name: string;
  phone: string;
  email: string;
  addressStreet: string;
  addressZip: string;
  addressCity: string;
  contactPersonFirstname: string;
  contactPersonSalutation: string;
  contactPersonSurname: string;
}
