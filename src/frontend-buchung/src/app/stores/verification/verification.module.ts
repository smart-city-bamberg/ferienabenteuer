import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import * as fromVerification from './verification.reducer';
import { EffectsModule } from '@ngrx/effects';
import { VerificationEffects } from './verification.effects';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StoreModule.forFeature(fromVerification.verificationFeatureKey, fromVerification.reducer),
    EffectsModule.forFeature([VerificationEffects])
  ]
})
export class VerificationModule { }
