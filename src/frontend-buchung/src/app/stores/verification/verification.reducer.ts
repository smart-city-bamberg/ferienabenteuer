import { createFeature, createReducer, on } from '@ngrx/store';
import { VerificationActions } from './verification.actions';
import {
  FormGroupState,
  createFormGroupState,
  onNgrxForms,
  setValue,
  updateGroup,
  validate,
  wrapReducerWithFormStateUpdate,
} from 'ngrx-forms';
import { required } from 'ngrx-forms/validation';

export const verificationFeatureKey = 'verification';

const verificationId = 'verification_form';
const resendCodeId = 'resend_code_form';

interface Verification {
  token: string;
}

interface ResendCode {
  email: string;
}

export interface State {
  verificationForm: FormGroupState<Verification>;
  resendCodeForm: FormGroupState<ResendCode>;
}

export const initialState: State = {
  verificationForm: createFormGroupState(verificationId, {
    token: '',
  }),
  resendCodeForm: createFormGroupState(resendCodeId, {
    email: '',
  }),
};

export const reducer = wrapReducerWithFormStateUpdate(
  wrapReducerWithFormStateUpdate(
    createReducer(
      initialState,
      on(VerificationActions.verifyAccountSuccess, (state, action) => ({
        ...initialState,
      })),
      on(VerificationActions.updateToken, (state, action) => ({
        ...state,
        verificationForm: setValue({ token: action.token })(
          state.verificationForm
        ),
      })),
      onNgrxForms()
    ),
    (state) => state.verificationForm,
    (state) =>
      updateGroup<Verification>({
        token: validate(required),
      })(state)
  ),
  (state) => state.resendCodeForm,
  (state) =>
    updateGroup<ResendCode>({
      email: validate(required),
    })(state)
);

export const verificationFeature = createFeature({
  name: verificationFeatureKey,
  reducer,
});
