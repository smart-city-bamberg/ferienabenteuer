import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  catchError,
  map,
  concatMap,
  filter,
  withLatestFrom,
  tap,
} from 'rxjs/operators';
import { of } from 'rxjs';
import { VerificationActions } from './verification.actions';
import { routerNavigatedAction } from '@ngrx/router-store';
import { Store } from '@ngrx/store';
import {
  ResendVerificationCodeGQL,
  VerifyAccountGQL,
} from 'src/app/graphql/generated';
import { selectResendEmail, selectToken } from './verification.selectors';
import { Router } from '@angular/router';

@Injectable()
export class VerificationEffects {
  loadVerifications$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(VerificationActions.verifyAccount),
      withLatestFrom(this.store.select(selectToken)),
      concatMap(([action, token]) =>
        this.verifyAccountMutation.mutate({ token }).pipe(
          map((response) => {
            if (
              response.data?.verifyAccount.__typename == 'VerifyAccountSuccess'
            ) {
              return VerificationActions.verifyAccountSuccess({
                data: response,
              });
            }

            if (
              response.data?.verifyAccount.__typename == 'VerifyAccountFailed'
            ) {
              return VerificationActions.verifyAccountInvalidToken();
            }

            throw new Error('no response');
          }),
          catchError((error) =>
            of(VerificationActions.verifyAccountFailure({ error }))
          )
        )
      )
    );
  });

  resendVerifications$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(VerificationActions.sendVerifyAccount),
      withLatestFrom(this.store.select(selectResendEmail)),
      concatMap(([action, email]) =>
        this.resendVerificationCodeMutation.mutate({ email }).pipe(
          map((response) => {
            if (
              response.data?.resendVerificationCode.__typename ===
              'ResendVerificationCodeSuccess'
            ) {
              return VerificationActions.sendVerifyAccountSuccess();
            }

            if (
              response.data?.resendVerificationCode.__typename ==
                'ResendVerificationCodeFailed' &&
              response.data.resendVerificationCode.reason ===
                'UserDoesNotExists'
            ) {
              return VerificationActions.sendVerifyAccountUserDoesNotExists();
            }

            throw new Error('no response');
          }),
          catchError((error) =>
            of(VerificationActions.sendVerifyAccountFailure({ error }))
          )
        )
      )
    );
  });

  updateTokenAfterNavigate$ = createEffect(() =>
    this.actions$.pipe(
      ofType(routerNavigatedAction),
      filter((action) =>
        action.payload.routerState.url.startsWith('/aktivieren')
      ),
      map((action) =>
        VerificationActions.updateToken({
          token: action.payload.routerState.root?.queryParams['token'] ?? '',
        })
      )
    )
  );

  redirectToLoginAfterVerificationSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(VerificationActions.verifyAccountSuccess),
        tap(() => this.router.navigate(['/login']))
      ),
    {
      dispatch: false,
    }
  );

  constructor(
    private actions$: Actions,
    private store: Store,
    private router: Router,
    private verifyAccountMutation: VerifyAccountGQL,
    private resendVerificationCodeMutation: ResendVerificationCodeGQL
  ) {}
}
