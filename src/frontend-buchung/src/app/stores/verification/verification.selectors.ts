import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromVerification from './verification.reducer';

export const selectVerificationState =
  createFeatureSelector<fromVerification.State>(
    fromVerification.verificationFeatureKey
  );

export const selectVerificationForm = createSelector(
  selectVerificationState,
  (state) => state.verificationForm
);

export const selectToken = createSelector(
  selectVerificationForm,
  (form) => form.value.token
);

export const selectResendCodeForm = createSelector(
  selectVerificationState,
  (state) => state.resendCodeForm
);

export const selectResendEmail = createSelector(
  selectResendCodeForm,
  (form) => form.value.email
);
