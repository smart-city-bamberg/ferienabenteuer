import { createActionGroup, emptyProps, props } from '@ngrx/store';

export const VerificationActions = createActionGroup({
  source: 'Verification',
  events: {
    'Verify Account': emptyProps(),
    'Verify Account Success': props<{ data: unknown }>(),
    'Verify Account Invalid Token': emptyProps(),
    'Verify Account Failure': props<{ error: unknown }>(),

    'Update Token': props<{ token: string }>(),

    'Send Verify Account': emptyProps(),
    'Send Verify Account Success': emptyProps(),
    'Send Verify Account User Does Not Exists': emptyProps(),
    'Send Verify Account Failure': props<{ error: unknown }>(),
  },
});
