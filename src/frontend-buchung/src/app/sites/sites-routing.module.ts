import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BookingsComponent } from './account/bookings.component';
import { ChildComponent } from './account/child.component';
import { ChildrenComponent } from './account/children.component';
import { PersonalDataComponent } from './account/personal-data.component';
import { RegistrationCompleteComponent } from './account/registration-complete';
import { RegistrationComponent } from './account/registration.component';
import { SecurityComponent } from './account/security.component';
import { VerificationComponent } from './account/verification.component';
import { AccessDeniedComponent } from './auth/access-denied.component';
import { LoginComponent } from './auth/login.component';
import { CartComponent } from './checkout/cart.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { ParticipantsComponent } from './checkout/participants.component';
import { PaymentComponent } from './checkout/payment.component';
import { SummaryComponent } from './checkout/sumary.component';
import { ContactsComponent } from './contacts.component';
import { DetailViewComponent } from './events/detail-view.component';
import { EventListComponent } from './events/event-list.component';
import { SiteContainerComponent } from './site-container.component';
import { custodianCanActivate } from '../roleGuard';
import { DiscountsComponent as AccountDiscountsComponent } from './account/discounts.component';
import { DiscountsComponent as CheckoutDiscountsComponent } from './checkout/discounts.component';
import { ResetPasswordComponent } from './account/reset-password.component';
import { NewPasswordComponent } from './account/new-password.component';
import { DeleteAccountSuccessComponent } from './account/delete-account-success.component';

const routes: Routes = [
  {
    path: '',
    component: SiteContainerComponent,
    children: [
      {
        path: 'login',
        component: LoginComponent,
      },
      {
        path: 'keinzugang',
        component: AccessDeniedComponent,
      },
      {
        path: 'veranstaltungen',
        component: EventListComponent,
      },
      {
        path: 'veranstaltungen/:eventId',
        component: DetailViewComponent,
      },
      {
        path: 'kontakt',
        component: ContactsComponent,
      },

      {
        path: 'buchungen',
        component: BookingsComponent,
        canActivate: [custodianCanActivate],
      },
      {
        path: 'ermaessigungen',
        component: AccountDiscountsComponent,
        canActivate: [custodianCanActivate],
      },
      {
        path: 'daten',
        component: PersonalDataComponent,
        canActivate: [custodianCanActivate],
      },
      {
        path: 'kinder',
        component: ChildrenComponent,
        canActivate: [custodianCanActivate],
      },
      {
        path: 'kinder/:id',
        component: ChildComponent,
        canActivate: [custodianCanActivate],
      },
      {
        path: 'kind-eintragen',
        component: ChildComponent,
        canActivate: [custodianCanActivate],
      },
      {
        path: 'sicherheit',
        component: SecurityComponent,
        canActivate: [custodianCanActivate],
      },
      {
        path: 'konto-geloescht',
        component: DeleteAccountSuccessComponent,
        canActivate: [custodianCanActivate],
      },
      {
        path: 'checkout',
        component: CheckoutComponent,
        canActivate: [custodianCanActivate],
        children: [
          {
            path: 'warenkorb',
            component: CartComponent,
            canActivate: [custodianCanActivate],
          },
          {
            path: 'ermaessigungen',
            component: CheckoutDiscountsComponent,
            canActivate: [custodianCanActivate],
          },
          {
            path: 'teilnehmer',
            component: ParticipantsComponent,
            canActivate: [custodianCanActivate],
          },
          {
            path: 'kasse',
            component: PaymentComponent,
            canActivate: [custodianCanActivate],
          },
          {
            path: 'erfolgreich',
            component: SummaryComponent,
            canActivate: [custodianCanActivate],
          },
        ],
      },
      {
        path: 'registrieren',
        component: RegistrationComponent,
      },
      {
        path: 'registriert',
        component: RegistrationCompleteComponent,
      },
      {
        path: 'aktivieren',
        component: VerificationComponent,
      },
      {
        path: 'passwort-zuruecksetzen',
        component: ResetPasswordComponent,
      },
      {
        path: 'neues-passwort',
        component: NewPasswordComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SitesRoutingModule {}
