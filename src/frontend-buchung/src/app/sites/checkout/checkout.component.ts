import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { map, of } from 'rxjs';
import { CheckoutActions } from 'src/app/stores/checkout/checkout.actions';
import {
  selectCheckoutPage,
  selectPaymentForm,
} from 'src/app/stores/checkout/checkout.selectors';

@Component({
  selector: 'app-checkout',
  template: `
    <div *ngIf="formState$ | async; let formState">
      <form [ngrxFormState]="formState" class="flex flex-col gap-4">
        <div
          class="white-card flex flex-col"
          *ngIf="formState$ | async; let formState"
        >
          <ol class="ml-2 flex flex-grow items-center md:ml-4">
            <li
              [routerLink]="['/checkout', step.name]"
              *ngFor="let step of steps$ | async"
              [ngClass]="{
              'after:border-gray-100': !step.isDone,
              'after:border-primaryLight': step.isDone,
              'step-not-last':
                !step.isLast,
            }"
              class="flex items-center hover:cursor-pointer"
            >
              <span
                [attr.aria-label]="step.name"
                [ngClass]="{
                  ' border-4 border-primaryLight': step.isActive,
                  'bg-primaryLight/80': step.isDone
                }"
                class="flex h-10 w-10 shrink-0 items-center justify-center rounded-full bg-gray-100 hover:bg-primaryLight/60 lg:h-12 lg:w-12"
              >
                <i
                  *ngIf="step.isDone"
                  class="mi notranslate text-2xl text-white"
                  >check</i
                >
              </span>
            </li>
          </ol>

          <div class="mt-2 flex flex-row justify-between text-xs md:text-base">
            <span
              *ngFor="let step of steps$ | async"
              [ngClass]="{
              'text-primaryLight': step.isDone,
            }"
              [ngClass]="step.class"
              class="text-gray-500"
              >{{ step.label }}</span
            >
          </div>
        </div>

        <div
          class="flex h-8 flex-row px-4 md:h-12 md:px-8"
          *ngIf="activeStep$ | async; let activeStep"
        >
          <div
            class="flex flex-row items-center gap-2 text-2xl font-bold md:gap-4 md:text-3xl"
          >
            <i aria-hidden="true" class="mi notranslate text-textDark">{{
              activeStep.icon
            }}</i>
            <h2>{{ activeStep.label }}</h2>
          </div>
        </div>

        <div>
          <router-outlet />
        </div>

        <div class="mb-8 mt-4 flex h-8 flex-row md:mt-0 md:h-12">
          <button
            *ngIf="!(isFirstStep$ | async) && !(isSummaryStep$ | async)"
            [routerLink]="['/checkout', (previousStep$ | async)?.name]"
            aria-label="Zurück"
            class="flex items-center justify-center gap-2 md:w-44 md:p-2 md:text-xl"
          >
            <i class="mi notranslate">arrow_back</i>
            Zurück
          </button>
          <div class="flex flex-grow justify-end">
            <button
              aria-label="Weiter"
              *ngIf="!(isSummaryStep$ | async) && !(isBookingStep$ | async)"
              [routerLink]="['/checkout', (nextStep$ | async)?.name]"
              class="flex items-center justify-center gap-2 p-2 md:w-44 md:text-xl"
            >
              Weiter <i class="mi notranslate">arrow_forward</i>
            </button>
            <button
              *ngIf="isBookingStep$ | async"
              aria-label="Kostenpflichtig buchen"
              class="flex items-center justify-center gap-2 p-2 md:w-64 md:text-xl"
              (click)="bookCartTickets()"
            >
              Kostenpflichtig Buchen
              <i class="mi notranslate">arrow_forward</i>
            </button>
          </div>
        </div>
      </form>
    </div>
  `,
  styles: [
    `
      :host {
        @apply flex flex-col gap-4;
      }

      .step-not-last {
        @apply flex-grow after:content-[''] after:w-full after:h-1 after:border-b after:border-4 after:inline-block;
      }
    `,
  ],
})
export class CheckoutComponent {
  steps = [
    {
      label: 'Warenkorb',
      name: 'warenkorb',
      icon: 'shopping_cart',
      class: 'ml-[.1rem] md:ml-[.4rem]',
    },
    {
      label: 'Ermäßigungen',
      name: 'ermaessigungen',
      icon: 'sell',
      class: 'ml-[.2rem] md:ml-[.2rem]',
    },
    {
      label: 'Teilnehmer',
      name: 'teilnehmer',
      icon: 'person',
      class: 'mr-[1.2rem] md:mr-[1.7rem]',
    },
    {
      label: 'Kasse',
      name: 'kasse',
      icon: 'shopping_bag',
      class: 'mr-[.4rem] md:mr-[.4rem]',
    },
  ];

  formState$ = this.store.select(selectPaymentForm);

  checkoutPage$ = this.store.select(selectCheckoutPage);

  activeStep$ = this.checkoutPage$.pipe(
    map((page) => this.steps.find((p) => p.name === page))
  );

  nextStep$ = this.checkoutPage$.pipe(
    map(
      (page) =>
        this.steps[
          Math.min(
            this.steps.findIndex((s) => s.name === page) + 1,
            this.steps.length - 1
          )
        ]
    )
  );

  previousStep$ = this.checkoutPage$.pipe(
    map(
      (page) =>
        this.steps[
          Math.max(this.steps.findIndex((s) => s.name === page) - 1, 0)
        ]
    )
  );

  steps$ = this.checkoutPage$.pipe(
    map((page) =>
      this.steps.map((step, index) => ({
        ...step,
        isFirst: index === 0,
        isLast: index === this.steps.length - 1,
        isActive: step.name === page,
        isDone: this.steps.findIndex((s) => s.name == page) > index,
      }))
    )
  );

  checkoutProgress$ = of(10); //this.store.select(selectCheckoutProgress);

  isBookingStep$ = this.checkoutPage$.pipe(
    map((page) => page === this.steps[this.steps.length - 1].name)
  );

  isSummaryStep$ = this.checkoutPage$.pipe(
    map((page) => page === 'erfolgreich')
  );

  isFirstStep$ = this.checkoutPage$.pipe(
    map((page) => page === this.steps[0].name)
  );

  constructor(private store: Store) {}

  bookCartTickets() {
    this.store.dispatch(CheckoutActions.bookCartTickets());
  }
}
