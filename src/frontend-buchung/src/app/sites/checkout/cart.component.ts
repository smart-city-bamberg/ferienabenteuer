import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { CheckoutActions } from 'src/app/stores/checkout/checkout.actions';
import { selectCartEventsWithTickets } from 'src/app/stores/checkout/checkout.selectors';

@Component({
  selector: 'app-checkout-cart',
  template: `
    <div class="flex flex-grow flex-col space-y-8">
      <div
        class="white-card flex flex-col"
        *ngFor="let booking of cart$ | async; trackBy: identifyById"
      >
        <div class="relative">
          <div class="absolute -right-0 mt-2 cursor-pointer md:mt-0">
            <button
              aria-label="Entfernen"
              (click)="removeAllEventTicketsFromCart(booking.event.id)"
            >
              <i class="mi notranslate hidden text-sm md:flex">delete</i>
              <span class="text-xs sm:text-sm md:ml-2">Entfernen</span>
            </button>
          </div>
        </div>

        <app-event-details [event]="booking.event" />

        <div class="mt-4">
          <h3 class="font-extrabold md:text-lg">Tickets</h3>
        </div>
        <div class="divider mb-2"></div>
        <div class="md:mt-2">
          <div
            class="flex flex-col gap-6"
            *ngFor="
              let ticketType of booking.ticketTypes;
              trackBy: identifyByTicketType
            "
          >
            <div
              class="flex flex-col justify-between text-textDark md:flex-row md:items-center md:px-4"
              *ngFor="
                let ticket of ticketType.ticketGroups;
                trackBy: identifyByTicketType
              "
            >
              <div
                class="flex flex-grow flex-col gap-1 md:flex-row md:gap-6 md:text-lg"
              >
                <div
                  class="flex flex-col-reverse text-lg font-semibold leading-tight md:w-48 xl:w-80"
                >
                  <p class="relative flex items-start">
                    {{
                      ticket.discount
                        ? 'Ermäßigung | ' + ticket.discount.name
                        : 'Normal'
                    }}

                    <button
                      *ngIf="ticket.discount"
                      class="peer border-none bg-transparent p-1 text-primaryLight"
                    >
                      <i title="asdfgh" class="mi notranslate outlined">info</i>
                    </button>
                    <span
                      *ngIf="ticket.discount"
                      class="absolute bottom-7 right-0 hidden w-auto items-start gap-2 rounded-md border border-gray-500 bg-white p-2 text-primaryLight shadow-md peer-focus:flex"
                      ><i
                        *ngIf="ticket.discount"
                        title="asdfgh"
                        class="mi notranslate outlined mt-1"
                        >info</i
                      >
                      {{ ticket.discount.description }}</span
                    >
                  </p>
                  <p [ngClass]="ticketTypeClass[ticket.ticketType]">
                    1 {{ ticketTypeLabels[ticket.ticketType] }}
                  </p>
                </div>
                <div
                  class="flex flex-grow flex-row items-center justify-between gap-4 md:gap-8"
                >
                  <div class="flex flex-col">
                    <div
                      *ngIf="ticket.discount"
                      class="flex w-20 flex-row text-xs"
                    >
                      <span class="bg-primaryLight/10 px-1 text-primaryLight"
                        >{{ ticket.discount.discountPercent }}&nbsp;%</span
                      >
                      <p
                        [attr.aria-label]="
                          ticket.originalCosts + ':euro durchgestrichen'
                        "
                        class="ml-1 line-through"
                      >
                        {{ ticket.originalCosts | currency : 'EUR' }}
                      </p>
                    </div>
                    <p class="text-lg">
                      {{ ticket.costs | currency : 'EUR' }}
                    </p>
                  </div>

                  <div class="flex flex-grow flex-row justify-end gap-2">
                    <button
                      aria-label="Ticket entfernen"
                      (click)="
                        removeTicket(booking.event.id, ticket.discount?.id)
                      "
                      class="h-8 w-8 md:h-10 md:w-10"
                    >
                      <i class="mi notranslate text-lg md:text-xl">remove</i>
                    </button>
                    <p
                      class="w-6 items-center justify-center font-semibold md:text-lg"
                    >
                      {{ ticket.amount }}
                    </p>
                    <button
                      aria-label="Ticket hinzufügen"
                      (click)="addTicket(booking.event.id, ticket.discount?.id)"
                      class="h-8 w-8 md:h-10 md:w-10"
                    >
                      <i class="mi notranslate text-lg md:text-xl">add</i>
                    </button>
                  </div>

                  <span class="flex w-20 justify-end text-lg font-semibold">
                    {{ ticket.sum | currency : 'EUR' }}
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="divider my-2"></div>
        <div
          class="flex flex-row justify-between text-lg font-bold text-textDark md:justify-end md:gap-8 md:pr-4"
        >
          <span>Zwischensumme:</span>
          <span>{{ booking.sum | number : '1.2-2' }} €</span>
        </div>
      </div>
    </div>
  `,
  styles: [
    `
      :host {
        @apply flex flex-col flex-grow;
      }
    `,
  ],
})
export class CartComponent {
  constructor(private store: Store) {}

  badgeColors: { [index: string]: string } = {
    bezahlt: 'green',
    austehend: 'orange',
  };

  ticketTypeLabels: { [index: string]: string } = {
    waitinglist: 'Wartelistenplatz',
    participant: 'Teilnehmerplatz',
  };

  ticketTypeClass: { [index: string]: string } = {
    waitinglist: 'text-[#E90C0C]',
    participant: 'text-[#617794]',
  };

  cart$ = this.store.select(selectCartEventsWithTickets);

  addTicket(eventId: string, discountTypeId?: string) {
    this.store.dispatch(
      CheckoutActions.addTicket({
        eventId,
        discountTypeId,
      })
    );
  }

  removeTicket(eventId: string, discountTypeId?: string) {
    this.store.dispatch(
      CheckoutActions.removeTicket({
        eventId,
        discountTypeId,
      })
    );
  }

  removeAllEventTicketsFromCart(eventId: string) {
    this.store.dispatch(
      CheckoutActions.removeAllEventTicketsFromCart({ eventId })
    );
  }

  identifyById(index: number, entity: { id: string }) {
    return entity.id;
  }

  identifyByTicketType(index: number, entity: { ticketType: string }) {
    return entity.ticketType;
  }
}
