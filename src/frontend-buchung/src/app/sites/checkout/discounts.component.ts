import { Component } from '@angular/core';

@Component({
  selector: 'app-checkout-discounts',
  template: ` <app-discounts /> `,
  styles: [
    `
      :host {
        @apply flex flex-col;
      }
    `,
  ],
})
export class DiscountsComponent {}
