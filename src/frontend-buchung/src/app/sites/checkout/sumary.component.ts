import { Component } from '@angular/core';

@Component({
  selector: 'app-checkout-summary',
  template: `
    <div class="white-card flex flex-col gap-3 px-2">
      <div class="mt-8 flex justify-center">
        <img
          class="w-12 md:w-20"
          aria-label="Check Bild"
          src="../assets/images/checkout/finish.svg"
          alt="Lock"
        />
      </div>
      <div class="mb-8 mt-8 flex flex-col items-center gap-4">
        <h2 class="text-center font-bold md:text-xl">
          Ihre Buchung war erfolgreich!
        </h2>
        <p class="text-center text-sm leading-tight text-[#757575]">
          Buchungsbestätigung/en erhalten Sie per E-Mail. Dies kann wenige
          Minuten dauern.
        </p>
      </div>
      <div
        class="mt-3 flex flex-grow flex-row flex-wrap justify-center gap-4 sm:gap-16"
      >
        <button routerLink="/buchungen" class="flex justify-center md:p-2">
          <span class="md:text-xl">Buchungsübersicht</span>
        </button>
        <button
          routerLink="/veranstaltungen"
          class="flex justify-center md:p-2"
        >
          <span class="md:text-xl">Veranstaltungen</span>
        </button>
      </div>
    </div>
  `,
  styles: [
    `
      :host {
        @apply flex flex-col flex-grow;
      }
    `,
  ],
})
export class SummaryComponent {}
