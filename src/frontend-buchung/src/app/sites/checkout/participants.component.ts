import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import {
  selectCartEventsWithChildrenWithoutGroups,
  selectDiscountTypes,
} from 'src/app/stores/checkout/checkout.selectors';
import { CheckoutActions } from 'src/app/stores/checkout/checkout.actions';

@Component({
  selector: 'app-checkout-participants',
  template: `
    <div class="flex flex-col space-y-8">
      <div
        class="white-card flex flex-col"
        *ngFor="let booking of cart$ | async; trackBy: identifyById"
      >
        <app-event-details [event]="booking.event" />

        <div
          *ngFor="
            let ticketType of booking.ticketTypes;
            trackBy: identifyByTicketType
          "
        >
          <div class="mb-1 mt-3">
            <p
              class="md:text-lg"
              [ngClass]="ticketTypeClass[ticketType.ticketType]"
            >
              {{ ticketTypeNames[ticketType.ticketType] }}
            </p>
          </div>

          <div
            *ngFor="let ticket of ticketType.tickets; trackBy: identifyById"
            class="flex flex-col items-center justify-evenly gap-2 md:flex-row md:gap-4"
          >
            <select
              id="child"
              aria-label="Ticket Typ auswählen"
              class="block w-full rounded-lg border border-gray-300 p-2.5 text-sm font-bold text-textDark focus:border-primaryLight focus:ring-primaryLight"
              [ngModel]="ticket.discountType?.id"
              [ngClass]="{
                'bg-primaryLight/10': ticket.child?.id
              }"
              #childSelect
              (ngModelChange)="assignDiscountTypeToTicket(ticket.id, $event)"
            >
              <option [ngValue]="null" selected>Normal</option>
              <option
                *ngFor="
                  let discountType of discountTypes$ | async;
                  trackBy: identifyById
                "
                [value]="discountType.id"
              >
                Ermäßigung | {{ discountType.name }}
              </option>
            </select>
            <select
              id="child"
              aria-label="Kind auswählen"
              class="block w-full rounded-lg border border-gray-300 p-2.5 text-sm font-bold text-textDark focus:border-primaryLight focus:ring-primaryLight"
              [ngModel]="ticket.child?.id"
              [ngClass]="{
                'bg-primaryLight/10': ticket.child?.id
              }"
              #childSelect
              (ngModelChange)="assignChildToTicket(ticket.id, $event)"
            >
              <option [ngValue]="null" selected>(kein Kind ausgwählt)</option>
              <option
                *ngFor="let child of ticket.children; trackBy: identifyById"
                [value]="child.id"
                [disabled]="child.isBooked || child.noDiscount"
              >
                <p class="notranslate">
                  {{ child.firstname }} {{ child.surname }}
                </p>
                {{
                  child.isBooked
                    ? ' (bereits gebucht)'
                    : child.noDiscount
                    ? ' (keine Ermäßigung)'
                    : ''
                }}
              </option>
            </select>
            <div class="my-3 md:invisible"></div>
          </div>
        </div>
      </div>
    </div>
  `,
  styles: [
    `
      :host {
        @apply flex flex-col gap-5;
      }
    `,
  ],
})
export class ParticipantsComponent {
  constructor(private store: Store) {}

  cart$ = this.store.select(selectCartEventsWithChildrenWithoutGroups);
  discountTypes$ = this.store.select(selectDiscountTypes);

  ticketTypeNames: { [index: string]: string } = {
    participant: 'Teilnehmerplatz',
    waitinglist: 'Wartelistenplatz',
  };

  ticketTypeClass: { [index: string]: string } = {
    waitinglist: 'text-[#E90C0C]',
    participant: 'text-[#617794]',
  };

  assignChildToTicket(cartTicketId: string, childId?: string) {
    this.store.dispatch(
      CheckoutActions.assignChildToTicket({
        cartTicketId,
        childId,
      })
    );
  }

  assignDiscountTypeToTicket(cartTicketId: string, discountTypeId?: string) {
    this.store.dispatch(
      CheckoutActions.assignDiscountTypeToTicket({
        cartTicketId,
        discountTypeId,
      })
    );
  }

  identifyById(index: number, entity: { id: string }) {
    return entity.id;
  }

  identifyByTicketType(index: number, entity: { ticketType: string }) {
    return entity.ticketType;
  }
}
