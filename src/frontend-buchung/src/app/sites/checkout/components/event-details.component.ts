import { Component, Input } from '@angular/core';

export interface Organizer {
  name: string;
  phone: string;
}

export interface CarePeriod {
  period: {
    id: string;
  };
  careDays: CareDay[];
}

export interface CareDay {
  day: string;
}

export interface Event {
  id: string;
  name: string;
  description: string;
  careTimeStart: string;
  careTimeEnd: string;
  ageMinimum: number;
  ageMaximum: number;
  costs: number;
  picture:
    | {
        url: string;
      }
    | undefined;
  emergencyPhone?: string;
  organizer: Organizer;
  locationCity: string;
  locationStreet: string;
  locationZipcode: string;
  carePeriodCommitted: CarePeriod;
}

@Component({
  selector: 'app-event-details',
  template: `
    <div class="mt-2 flex flex-col gap-2 md:flex-row md:gap-4">
      <div
        [routerLink]="['/veranstaltungen', event?.id]"
        class="h-[7rem] w-[14rem] cursor-pointer flex-col justify-between rounded-xl border border-gray-300 bg-cover bg-center hover:border-gray-400 md:h-auto lg:w-[18rem] xl:w-[18rem]"
        [style.background-image]="
          event?.picture
            ? 'url(' +
              (event?.picture?.url | imageProxy : '350x,jpeg,q75') +
              ')'
            : 'none'
        "
      ></div>
      <div class="flex flex-grow flex-col">
        <div class="flex flex-row justify-between">
          <div class="flex flex-col">
            <span
              class="notranslate text-sm font-bold text-[#757575] md:text-lg"
              >{{ event?.organizer?.name }}</span
            >
            <span
              class="notranslate mb-3 cursor-pointer text-lg font-black leading-none hover:text-primaryDark md:text-2xl"
              [routerLink]="['/veranstaltungen', event?.id]"
              >{{ event?.name }}</span
            >
          </div>
        </div>

        <div
          class="flex flex-col gap-3 text-xs md:flex-grow md:text-sm xl:flex-row xl:gap-12"
        >
          <div
            class="flex flex-col space-y-2 md:flex-row md:flex-wrap md:justify-start"
          >
            <div class="info-box">
              <div class="icon-box">
                <i aria-label="Adresse," class="mi notranslate">location_on</i>
              </div>
              <div class="flex flex-col leading-tight">
                <span>{{ event?.locationStreet }}</span>
                <span>{{ event?.locationCity }}</span>
              </div>
            </div>
            <div class="info-box">
              <div class="icon-box">
                <i aria-label="Zeitraum," class="mi notranslate"
                  >calendar_month</i
                >
              </div>
              <div class="flex flex-col leading-tight">
                <app-care-days
                  *ngIf="event?.carePeriodCommitted?.careDays"
                  [careDays]="event?.carePeriodCommitted?.careDays ?? []"
                />
                <span
                  >{{ event?.carePeriodCommitted?.careDays?.length }}
                  {{
                    event?.carePeriodCommitted?.careDays?.length ?? 0 > 1
                      ? 'Tage'
                      : 'Tag'
                  }}
                </span>
              </div>
            </div>

            <div class="notranslate info-box">
              <div class="icon-box">
                <i aria-label="Uhrzeit," class="mi">schedule</i>
              </div>
              <div class="flex flex-col">
                <span>
                  <!-- {{ event.carePeriodCommitted.careDays.join(' ') }}: -->
                  {{ event?.careTimeStart }} - {{ event?.careTimeEnd }}</span
                >
                <!-- <span
              >Keine Betreuung am
              {{ event.careDays.noCare }}
            </span> -->
              </div>
            </div>
            <div class="info-box">
              <div class="icon-box">
                <i aria-label="Alter," class="mi notranslate">person</i>
              </div>
              <div class="flex flex-col">
                <span
                  >{{ event?.ageMinimum }} - {{ event?.ageMaximum }} Jahre</span
                >
              </div>
            </div>
            <div class="info-box">
              <div class="icon-box">
                <i aria-label="Telefonnummer," class="mi notranslate">phone</i>
              </div>
              <div class="flex flex-col">
                <span>{{ event?.organizer?.phone }}</span>
              </div>
            </div>

            <div class="info-box" *ngIf="event?.emergencyPhone">
              <div class="icon-box">
                <i aria-label="Notfallnummer," class="mi notranslate outlined"
                  >person_alert</i
                >
              </div>
              <div class="flex flex-col">
                <span>{{ event?.emergencyPhone }}</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  `,
  styles: [
    `
      .info-box {
        @apply flex flex-row gap-3 md:w-1/2 xl:w-1/3 items-center;
      }
    `,
  ],
})
export class EventDetailsComponent {
  @Input()
  event?: Event;
}
