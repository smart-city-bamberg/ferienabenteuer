import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import {
  selectCartEventsWithChildren,
  selectCartEventsWithChildrenAndSum,
  selectPaymentForm,
} from 'src/app/stores/checkout/checkout.selectors';

@Component({
  selector: 'app-checkout-payment',
  template: `
    <div *ngIf="cart$ | async; let cart" class="flex flex-col gap-5">
      <div
        class="white-card flex flex-grow flex-col gap-5"
        *ngFor="let booking of cart.events; let i = index"
      >
        <div class="flex flex-col">
          <div class="flex flex-col">
            <div
              class="flex flex-row gap-2 text-lg font-bold md:text-xl xl:text-2xl"
            >
              <h2 class="notranslate">{{ booking.event.name }}</h2>
            </div>

            <div
              class="my-2 flex flex-col gap-1 text-sm leading-tight md:gap-0 xl:text-lg"
            >
              <div class="flex flex-row items-start gap-2">
                <i aria-label="Zeitraum," class="mi notranslate mt-1">today</i>
                <p class="flex flex-wrap">
                  <app-care-days
                    [careDays]="booking.event.carePeriodCommitted.careDays"
                    [startFormat]="'dd. MMM yyyy'"
                    [endFormat]="'dd. MMM yyyy'"
                  />
                  <span class="sm:ml-2"
                    >({{ booking.event.carePeriodCommitted.careDays.length }}
                    {{
                      booking.event.carePeriodCommitted.careDays.length > 1
                        ? 'Betreuungstage'
                        : 'Betreuungstag'
                    }})
                  </span>
                </p>
              </div>
              <div class="notranslate flex flex-row items-start gap-2">
                <i aria-label="Adresse," class="mi mt-1">location_on</i>
                <p>
                  {{ booking.event.locationStreet }},
                  {{ booking.event.locationCity }}
                </p>
              </div>
              <div class="flex flex-row items-start gap-2">
                <i aria-hidden="true" class="mi notranslate mt-1">apartment</i>
                <p>Veranstalter:</p>
                <p class="notranslate">{{ booking.event.organizer.name }}</p>
              </div>
            </div>
          </div>

          <div class="divider my-2"></div>

          <div *ngFor="let ticketType of booking.ticketTypes">
            <div class="mb-3">
              <h3 class="font-extrabold md:text-xl">
                {{ ticketTypeNames[ticketType.ticketType] }}
              </h3>
            </div>
            <div
              *ngFor="let ticketGroup of ticketType.ticketGroups"
              class="mb-4 flex flex-row gap-4 leading-none"
            >
              <div class="flex flex-grow flex-col">
                <div class="flex flex-row justify-between">
                  <h3 class="text-sm font-bold xl:text-lg">
                    {{ ticketGroup.tickets.length }}x Ticket
                    {{
                      ticketGroup.discount
                        ? 'Ermäßigung | ' + ticketGroup.discount.name
                        : 'Normal'
                    }}
                  </h3>
                  <span class="justify-end whitespace-nowrap font-bold">
                    {{ ticketGroup.sum | number : '1.2-2' }} €
                  </span>
                </div>
                <div class="flex flex-col">
                  <div
                    class="flex flex-row items-center gap-1"
                    *ngFor="let ticket of ticketGroup.tickets"
                  >
                    <i aria-label="Kind" class="mi notranslate">person</i>
                    <p
                      class="notranslate text-sm xl:text-xl"
                      *ngIf="ticket.child; else noChild"
                    >
                      {{ ticket.child.firstname }} {{ ticket.child.surname }},
                      {{ calculateAge(ticket.child.dateOfBirth) }}
                      <span class="translate">&nbsp;Jahre</span>
                    </p>
                    <ng-template #noChild>
                      <span class="text-sm text-[#E90C0C] xl:text-xl"
                        >kein Kind ausgewählt</span
                      >
                    </ng-template>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div
        class="white-card flex flex-grow flex-col gap-5"
        *ngIf="formState$ | async; let formState"
      >
        <p class="text-sm leading-tight text-[#757575]">
          Sie erhalten im Anschluss eine Buchungsbestätigung für jede Buchung
          per E-Mail.
        </p>
        <!-- <div class="divider -my-2"></div>
        <div class="divider -my-2 md:mb-0"></div> -->
        <div
          class="flex flex-col-reverse md:flex-row md:items-center md:justify-between"
        >
          <div class="flex flex-col gap-2 leading-tight">
            <div class="flex flex-row items-start">
              <div class="flex">
                <input
                  type="checkbox"
                  aria-label="Checkbox, AGB"
                  class="checkbox mr-2 mt-1 md:mt-0"
                  [ngrxFormControlState]="
                    formState.controls.agreeDataProtection
                  "
                />
              </div>
              <span class="inline text-xs">
                Ich habe die
                <a routerLink="/agb" aria-label="AGBseite" class="link">AGB</a>
                gelesen und bin einverstanden
              </span>
            </div>
            <div class="flex flex-row items-start">
              <div class="flex">
                <input
                  type="checkbox"
                  aria-label="Checkbox, Wiederruf- und Datenschutzerklärung"
                  class="checkbox mr-2 mt-1 md:mt-0"
                  [ngrxFormControlState]="
                    formState.controls.agreeTermsAndConditions
                  "
                />
              </div>
              <span class="inline text-xs">
                Ich habe die
                <a
                  routerLink="/widerruf"
                  aria-label="Widerrufsbelehrungsseite"
                  class="link"
                  >Widerrufsbelehrung</a
                >
                und die
                <a
                  routerLink="/datenschutz"
                  aria-label="Datenschutzerklärungsseite"
                  class="link"
                  >Datenschutzerklärung</a
                >
                gelesen und bin einverstanden
              </span>
            </div>
          </div>
          <h3 class="mb-2 justify-end text-lg font-bold md:mb-0">
            Gesamtsumme: {{ cart.sum | number : '1.2-2' }} €
          </h3>
        </div>
      </div>
    </div>
  `,
  styles: [
    `
      :host {
        @apply flex flex-col gap-5;
      }

      .mi {
        @apply text-textDark;
      }
    `,
  ],
})
export class PaymentComponent {
  formState$ = this.store.select(selectPaymentForm);
  ticketTypeNames: { [index: string]: string } = {
    participant: 'Teilnehmerplatz',
    waitinglist: 'Wartelistenplatz',
  };

  constructor(private store: Store) {}

  cart$ = this.store.select(selectCartEventsWithChildrenAndSum);

  calculateAge(dateOfBirth: string): number {
    const today = new Date();
    const birthDate = new Date(dateOfBirth);

    let age = today.getFullYear() - birthDate.getFullYear();
    const monthDiff = today.getMonth() - birthDate.getMonth();

    if (
      monthDiff < 0 ||
      (monthDiff === 0 && today.getDate() < birthDate.getDate())
    ) {
      age--;
    }

    return age;
  }
}
