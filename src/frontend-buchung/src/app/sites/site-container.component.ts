import { Component } from '@angular/core';

@Component({
  selector: 'app-site-container',
  template: `
    <app-top-bar class="sticky left-0 right-0 top-0"></app-top-bar>
    <div class="flex flex-grow flex-col bg-[#e9f2f9] pb-4 xl:items-center">
      <div class="mt-8 flex flex-grow flex-col px-2 md:px-8 xl:w-[83rem]">
        <router-outlet />
      </div>
    </div>
  `,
  styles: `
    :host {
      @apply flex flex-grow flex-col;
    }
  `,
})
export class SiteContainerComponent {}
