import { NgrxFormsModule } from 'ngrx-forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EventListComponent } from './events/event-list.component';
import { RouterModule } from '@angular/router';
import { LegalNoticeComponent } from './legal/legal-notice.component';
import { DataProtectionComponent } from './legal/data-protection.component';
import { ConditionsComponent } from './legal/conditions.component';
import { CancellationPolicyComponent } from './legal/cancellation-policy.component';
import { ContactsComponent } from './contacts.component';
import { DetailViewComponent } from './events/detail-view.component';
import { BookingsComponent } from './account/bookings.component';
import { DiscountsComponent as AccountDiscountsComponent } from './account/discounts.component';
import { ChildrenComponent } from './account/children.component';
import { SecurityComponent } from './account/security.component';
import { PersonalDataComponent } from './account/personal-data.component';
import { RegistrationComponent } from './account/registration.component';
import { ComponentsModule } from '../components/components.module';
import { FormsModule } from '@angular/forms';
import { ChildComponent } from './account/child.component';
import { AccessDeniedComponent } from './auth/access-denied.component';
import { LoginComponent } from './auth/login.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { CartComponent } from './checkout/cart.component';
import { ParticipantsComponent } from './checkout/participants.component';
import { PaymentComponent } from './checkout/payment.component';
import { DiscountsComponent as CheckoutDiscountsComponent } from './checkout/discounts.component';
import { SummaryComponent } from './checkout/sumary.component';
import { SiteContainerComponent } from './site-container.component';
import { DirectivesModule } from '../directives/directives.module';
import { PipesModule } from '../pipes/pipes.module';
import { EventDetailsComponent } from './checkout/components/event-details.component';
import { VerificationComponent } from './account/verification.component';
import { RegistrationCompleteComponent } from './account/registration-complete';
import { SitesRoutingModule } from './sites-routing.module';
import { ResetPasswordComponent } from './account/reset-password.component';
import { NewPasswordComponent } from './account/new-password.component';
import { DeleteAccountSuccessComponent } from './account/delete-account-success.component';

@NgModule({
  declarations: [
    SiteContainerComponent,
    EventListComponent,
    LegalNoticeComponent,
    DataProtectionComponent,
    ConditionsComponent,
    CancellationPolicyComponent,
    ContactsComponent,
    DetailViewComponent,
    BookingsComponent,
    AccountDiscountsComponent,
    ChildrenComponent,
    SecurityComponent,
    PersonalDataComponent,
    RegistrationComponent,
    ChildComponent,
    AccessDeniedComponent,
    LoginComponent,
    CheckoutComponent,
    CartComponent,
    ParticipantsComponent,
    PaymentComponent,
    CheckoutDiscountsComponent,
    SummaryComponent,
    EventDetailsComponent,
    VerificationComponent,
    RegistrationCompleteComponent,
    ResetPasswordComponent,
    NewPasswordComponent,
    DeleteAccountSuccessComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    ComponentsModule,
    DirectivesModule,
    PipesModule,
    FormsModule,
    NgrxFormsModule,
    SitesRoutingModule,
  ],
  exports: [
    SiteContainerComponent,
    EventListComponent,
    LegalNoticeComponent,
    DataProtectionComponent,
    ConditionsComponent,
    CancellationPolicyComponent,
    ContactsComponent,
    DetailViewComponent,
    BookingsComponent,
    AccountDiscountsComponent,
    ChildrenComponent,
    SecurityComponent,
    PersonalDataComponent,
    RegistrationComponent,
    ChildComponent,
    AccessDeniedComponent,
    LoginComponent,
    CheckoutComponent,
    CartComponent,
    CheckoutDiscountsComponent,
    ParticipantsComponent,
    PaymentComponent,
    EventDetailsComponent,
    VerificationComponent,
    RegistrationCompleteComponent,
    ResetPasswordComponent,
    NewPasswordComponent,
    DeleteAccountSuccessComponent,
  ],
})
export class SitesModule {}
