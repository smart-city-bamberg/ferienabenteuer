import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import * as contactsSelectors from '../stores/contacts/contacts.selectors';
import { selectUser } from '../stores/auth/auth.selectors';
import { User } from '../stores/auth/auth.types';
import { map } from 'rxjs';

@Component({
  selector: 'app-contacts',
  template: `
    <div *ngIf="user$ | async" class="mb-8">
      <h2 class="site-title">Hilfe</h2>
      <div class="mx-6">
        <span>
          Bei Problemen mit dem Buchungsportal oder sonstigen Anregungen, bitte
          wenden Sie sich per E-Mail an&nbsp;<a
            class="link"
            href="mailto:hilfe@ferienabenteuer-bamberg.de?subject={{
              emailSubject
            }}&body={{ emailBody$ | async }}"
            >hilfe&commat;ferienabenteuer-bamberg.de</a
          >
        </span>
      </div>
    </div>
    <h2 class="site-title">Kontakt</h2>
    <div class="flex flex-col space-y-4">
      <div
        *ngFor="let contact of administratorContacts$ | async"
        class="notranslate white-card"
      >
        <div class="flex flex-col leading-tight">
          <span class="text-xl font-bold">{{ contact.name }}</span>
          <span class="translate">{{ contact.description }}</span>
        </div>
        <div class="divider my-2"></div>
        <div class="flex flex-row items-center gap-2">
          <i aria-label="Name, " class="mi">person</i>
          <div class="flex flex-col">
            <span
              >{{ contact.contactPersonFirstname }}
              {{ contact.contactPersonSurname }}</span
            >
          </div>
        </div>
        <a
          href="tel:{{ contact.phone }}"
          class="flex flex-row items-center gap-2"
          *ngIf="contact.phone"
        >
          <i aria-label="Telefonnummer, " class="mi">phone</i>
          <div class="flex flex-col">
            <a class="link">{{ contact.phone }}</a>
          </div>
        </a>
        <a
          href="mailto:{{ contact.email }}"
          class="flex flex-row items-center gap-2"
          *ngIf="contact.email"
        >
          <i aria-label="E-Mail, " class="mi mt-1">mail</i>
          <div class="flex flex-col">
            <a class="link">{{ contact.email }}</a>
          </div>
        </a>
      </div>
    </div>

    <div class="mx-6 my-8">
      <span
        >Bei
        <span class="font-bold">Fragen zu den einzelnen Veranstaltungen</span>
        und zum Buchungsstand wenden Sie sich bitte
        <span class="font-bold">direkt an den jeweiligen Veranstalter</span>
      </span>
    </div>

    <div class="grid gap-4 md:grid-cols-2 xl:grid-cols-3">
      <div
        *ngFor="let contact of organizerContacts$ | async"
        class="notranslate white-card"
      >
        <div class="flex flex-row items-center gap-3">
          <div class="icon-box">
            <i aria-hidden="true" class="mi">person</i>
          </div>
          <div class="flex flex-col leading-tight">
            <span class="translate text-[#757575]">Veranstalter</span>
            <span class="font-bold">{{ contact.name }}</span>
          </div>
        </div>
        <div class="divider my-2"></div>
        <div
          class="mb-1 flex flex-row items-start gap-2"
          *ngIf="
            contact.addressStreet || contact.addressZip || contact.addressCity
          "
        >
          <i aria-label="Adresse, " class="mi">location_on</i>
          <span class="whitespace-nowrap leading-none">
            {{ contact.addressStreet }}
            <ng-container *ngIf="contact.addressStreet">,<wbr /></ng-container>
            {{ contact.addressZip }} {{ contact.addressCity }}
          </span>
        </div>
        <div class="flex flex-row items-center gap-2">
          <i aria-label="Name, " class="mi">person</i>
          <div class="flex flex-col">
            <span
              >{{ contact.contactPersonFirstname }}
              {{ contact.contactPersonSurname }}</span
            >
          </div>
        </div>
        <a
          href="tel:{{ contact.phone }}"
          class="flex flex-row items-center gap-2"
          *ngIf="contact.phone"
        >
          <i aria-label="Telefonnummer, " class="mi">phone</i>
          <div class="flex flex-col">
            <a class="link">{{ contact.phone }}</a>
          </div>
        </a>
        <a
          href="mailto:{{ contact.email }}"
          class="flex flex-row items-start gap-2"
          *ngIf="contact.email"
        >
          <i aria-label="E-Mail, " class="mi mt-[0.4rem]">mail</i>
          <div class="flex flex-col">
            <a class="link">{{ contact.email }}</a>
          </div>
        </a>
      </div>
    </div>
  `,
  styles: [
    `
      :host {
      }

      span {
        @apply text-textDark;
      }

      .mi {
        @apply text-textDark;
      }
    `,
  ],
})
export class ContactsComponent {
  user$ = this.store.select(selectUser);
  administratorContacts$ = this.store.select(contactsSelectors.selectContacts);
  organizerContacts$ = this.store.select(contactsSelectors.selectOrganizers);

  readonly emailSubject = 'Hilfe Buchungsportal';

  emailBody$ = this.user$.pipe(
    map(
      (user: User | undefined) =>
        // %0D%0 = newline within the email body
        'Hallo Ferienabenteuer-Team,' +
        '%0D%0A%0D%0A' +
        `Account: ${user?.email}` +
        '%0D%0A' +
        `Verwendeter Browser: ${this.userAgent}` +
        '%0D%0A%0D%0A' +
        'Fehlerbeschreibung (bitte so detailliert wie möglich):' +
        '%0D%0A' +
        '- Welche Schritte haben Sie unmittelbar vor dem Auftreten des Fehlers unternommen?' +
        '%0D%0A' +
        '- Gibt es spezifische Aktionen, die den Fehler reproduzierbar machen?' +
        '%0D%0A' +
        '- Tritt der Fehler nur unter bestimmten Bedingungen auf (z. B. nach dem Login, bei der Verwendung bestimmter Funktionen)?' +
        '%0D%0A' +
        `- Erhalten Sie eine Fehlermeldung, und wenn ja, welche?` +
        '%0D%0A%0D%0A' +
        'Viele Grüße,' +
        '%0D%0A' +
        `${user?.name}`
    )
  );
  readonly userAgent = window.navigator.userAgent;

  constructor(private store: Store) {}
}
