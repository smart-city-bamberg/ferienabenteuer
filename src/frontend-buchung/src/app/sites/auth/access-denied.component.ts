import { Component } from '@angular/core';

@Component({
  selector: 'app-access-denied',
  template: `
    <p class="text-center leading-tight">
      Sie haben keine Berechtigung diese Seite aufzurufen.
    </p>
    <a class="button" aria-label="Login Seite" routerLink="/login">Login</a>
  `,
  styles: [
    `
      :host {
        @apply flex flex-col flex-grow min-h-[70vh] items-center justify-center gap-4;
      }
    `,
  ],
})
export class AccessDeniedComponent {}
