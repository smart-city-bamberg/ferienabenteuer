import { Component } from '@angular/core';

@Component({
  selector: 'app-agb',
  template: `
    <div class="landing-page-padding mb-12">
      <h1 class="mb-2 pt-10 text-xl font-extrabold md:text-2xl">
        Allgemeine Geschäftsbedingungen
      </h1>
      <div class="flex flex-col">
        <span
          >Nachfolgende Geschäftsbedingungen gelten für alle Veranstaltungen des
          Bamberger Ferienabenteuers, die online buchbar sind.<br /><br />Vertragspartner
          sind die rechtliche Vertretung (im Folgenden „Familie“) des
          angemeldeten Kindes und der jeweilige Veranstalter. Wer Veranstalter
          ist, ergibt sich aus dem jeweiligen Jahresprogramm. Die aktuellen
          Veranstalter sind auch auf der Homepage des Ferienabenteuers
          veröffentlicht.<br /><br />Die Familienregion bzw. Stadt und Landkreis
          Bamberg als Betreiber der Homepage des Bamberger Ferienabenteuers sind
          keine Veranstalter. Sie bieten mit der Internetseite lediglich eine
          Plattform an, über die Informationen zum Ferienabenteuer eingeholt
          werden und Veranstaltungen gebucht werden können.</span
        >
        <h2>1. Berechtigter Teilnehmerkreis</h2>
        <span
          >Das Ferienabenteuer richtet sich an Kinder im Alter von 6 bis 14
          Jahren, deren Eltern bzw. Sorgeberechtigte in Stadt oder Landkreis
          Bamberg wohnen. Beschäftigte von Partnerunternehmen können das Angebot
          ausnahmsweise auch dann nutzen, wenn sie nicht in der Region
          wohnen.<br /><br />
          Das Ferienabenteuer soll besonders Berufstätige entlasten. Darüber
          hinaus sollen die Angebote im Sinne der Inklusion allen Kindern (mit
          und ohne Behinderungen, mit und ohne Migrationshintergrund und aus
          unterschiedlichen Einkommensverhältnissen) zugänglich sein.</span
        >

        <h2>2. Anmeldefrist</h2>
        <span
          >Die Anmeldefrist ist bei jeder Veranstaltung einzeln angegeben und
          beträgt max. 4 Wochen vor Veranstaltungsbeginn. Die Frist dient der
          Planungssicherheit der Veranstalter. Nach Ablauf der Frist können nach
          Rücksprache mit dem Veranstalter weitere Anmeldungen entgegengenommen
          werden, sofern noch Plätze zur Verfügung stehen.
        </span>
        <h2>3. Zustandekommen des Vertrags</h2>
        <span
          >Der Vertrag kommt mit Zugang der Buchungsbestätigung zustande.<br /><br />
          Werden mehrere Veranstaltungen bei verschiedenen Veranstaltern des
          Ferienabenteuers gebucht, handelt es sich um jeweils separate Verträge
          zwischen der Familie und dem jeweiligen Veranstalter.<br /><br />Die
          Buchungen werden in zeitlicher Reihenfolge berücksichtigt.<br /><br />Bei
          Beantragung einer Ermäßigung gilt Ziff. 6.<br /><br />Wird bei der
          Buchung nur ein Platz auf der
          <span class="font-bold">Warteliste</span> erworben, handelt es sich
          zunächst um eine Buchungsanfrage. Sobald ein regulärer Platz frei
          wird, erhält die Familie eine schriftliche Mitteilung, dass Platz zum
          Nachrücken frei geworden ist. Nimmt die Familie dieses befristete
          Angebot innerhalb von 48 Stunden an, erhält sie eine verbindliche
          Buchungsbestätigung, ansonsten wird der reservierte Platz im
          Buchungssystem wieder verfügbar. Ein keinen Anspruch auf das
          Nachrücken.<br /><br />Die Zahlungsabwicklung richtet sich nach
          Ziff.5. Für eine Rückerstattung vorzeitig überwiesener Beträge kann
          der Veranstalter eine Bearbeitungsgebühr von bis zu 10 % der
          Veranstaltungsentgelte einbehalten.<br /><br />Die Teilnahme an einer
          Veranstaltung gilt als Anmeldung. Sie verpflichtet zur Zahlung der
          Veranstaltungsentgelte.<br /><br />Ein Anspruch auf Vertragsschluss
          besteht nicht.
        </span>
        <h2>4. Leistungsumfang</h2>
        <span
          >Der Umfang der Leistungen, Beginn und Dauer der Veranstaltungen sind
          jeweils in der Veranstaltungsbeschreibung angegeben.<br /><br />Von
          der angegebenen Leistung kann abgewichen werden, soweit dies unter
          Berücksichtigung der Interessen des Veranstalters für die Familie
          zumutbar ist. Das ist der Fall, wenn für die Abweichung ein triftiger
          Grund vorliegt (d.h. die Leistung in dieser Form tatsächlich oder
          rechtlich unmöglich ist, z.B. wenn der Veranstaltungsort nicht nutzbar
          ist oder Drittanbieter (wie Ausflugsziele) ihr Angebot ändern), die
          Leistung aber in anderer, gleichwertiger Form erbracht werden kann.<br />
          Der Veranstalter teilt die Änderung in Textform den angemeldeten
          Familien mit. Wesentliche Änderungen (insbesondere ein Wechsel des
          Veranstaltungsortes) begründen ein Rücktrittsrecht der Familie vom
          Vertrag, siehe Punkt 8. a.<br /><br />Der Veranstalter darf die
          angegebene Leistung hinsichtlich ihrer Dauer oder ihres
          Veranstaltungsinhalts reduzieren, soweit infolge von Ereignissen, die
          bei Vertragsschluss nicht vorhersehbar waren, die Leistung aus
          tatsächlichen oder rechtlichen Gründen einen Aufwand erfordert, der in
          einem groben Missverhältnis zum Interesse der angemeldeten Familien an
          der Veranstaltung steht. Der Veranstalter teilt die Änderung
          unverzüglich in Textform den angemeldeten Familien mit. Das
          Veranstaltungsentgelt wird im Verhältnis „Wert der reduzierten
          Veranstaltung bei Vertragsschluss ./. Wert der vollständigen
          Veranstaltung bei Vertragsschluss“ herabgesetzt. Die Minderung ist,
          soweit erforderlich, durch Schätzung zu ermitteln. Die Änderungen
          begründen ein Rücktrittsrecht der Familie vom Vertrag, siehe Punkt 8.
          a.
        </span>

        <h2>5. Zahlungsabwicklung</h2>
        <span
          >Die Veranstaltungsentgelte werden nach Übersendung einer Rechnung
          über die gebuchte Veranstaltung zum darin angeführten Datum zur
          Zahlung fällig.<br /><br />Die Buchungsbestätigung enthält die
          Rechnung über die Veranstaltungsentgelte, die Bankverbindung des
          Veranstalters, die Widerrufsbelehrung sowie das Datum, zu dem die
          Zahlung spätestens zu leisten ist. Werden von einer Familie mehrere
          Veranstaltungen gebucht, werden die Angaben entsprechend nach
          Veranstaltern getrennt ausgewiesen.<br /><br />Aufgrund der hohen
          Nachfrage nach dem Bamberger Ferienabenteuer ist der rechtzeitige
          Zahlungseingang für den Veranstalter wesentlich. Wird die Zahlung
          innerhalb der Zahlungsfrist nicht oder nicht vollständig geleistet,
          kann der Veranstalter vom Vertrag ohne weitere Fristsetzung
          zurücktreten (§ 323 Abs. 2 Nr. 2 BGB), siehe Punkt 8. b. Die Buchung
          verfällt ersatzlos, der gebuchte Platz wird im Buchungssystem wieder
          verfügbar.<br /><br />Bei Anmeldungen/Nachrücken von der Warteliste
          nach Ablauf der regulären Anmeldefrist sind die Veranstaltungsentgelte
          umgehend nach Erhalt der Rechnung zu überweisen.
        </span>
        <h2>6. Ermäßigung</h2>
        <span
          >Familien können Ermäßigung der Veranstaltungsentgelte in 2 Fällen
          beantragen:</span
        >
        <div class="ml-5">
          <ul class="list-disc">
            <li>
              <span class="font-bold">Bei Kinderreichtum</span> (Bezug von
              Kindergeld für 3 oder mehr Kinder) 50% Ermäßigung für ein Kind
              (nicht übertragbar, auch nicht innerhalb der Familie).<br />Nachweis:
              aktueller Kindergeldbescheid oder personalisierter Kontoauszug mit
              Eingang der Kindergeldzahlung (beides nicht älter als ein
              Jahr).<br />Es können bis zu 4 Veranstaltungen ermäßigt werden.
            </li>
            <li>
              <span class="font-bold">Bei geringem Einkommen</span> erhalten
              alle Kinder der Familie 50 % Ermäßigung. Nachweis: SozCard oder
              aktueller Bescheid über ALG II, Sozialhilfe oder Wohngeld (jeweils
              nicht älter als ein Jahr). Es können pro Kind bis zu 4
              Veranstaltungen ermäßigt werden.
            </li>
          </ul>
        </div>
        <span class="mt-3"
          >Die Ermäßigung ist entweder bei einer konkreten Buchung oder
          allgemein im Nutzerprofil unabhängig einer Buchung zu beantragen. Der
          entsprechende Nachweis (in Kopie) ist in jedem Fall innerhalb von 14
          Tagen vorzulegen bei:
        </span>
        <div class="my-3 flex flex-col">
          <span class="notranslate"
            ><span class="font-bold">Landratsamt Bamberg</span>
            <br />
            Maarit Stierle<br />
            Ludwigstraße 23<br />
            96052 Bamberg<br />
          </span>
          <span>
            Telefon:
            <a
              aria-label="+4995185510 anrufen"
              class="link"
              href="tel:+4995185510"
            >
              +49 (0) 951 85-510
            </a>
          </span>
          Fax: 0951 85-5810
          <span>
            E-Mail:
            <a
              aria-label="E-Mail an maarit.stierle@lra-ba.bayern.de"
              class="link"
              href="mailto:maarit.stierle@lra-ba.bayern.de"
              >maarit.stierle&#64;lra-ba.bayern.de</a
            >
          </span>
          <span class="mt-3"
            >In der Buchungsbestätigung wird aus der beigefügten Rechnung
            ersichtlich, ob eine Ermäßigung gewährt wurde. Sofern die beantragte
            Ermäßigung nicht gewährt wird, kann die Familie die Buchung
            ungeachtet des bestehenden Widerrufsrechts innerhalb von 14 Tagen
            nach Rechnungseingang stornieren.<br /><br />Ein Anspruch auf
            Ermäßigung besteht nicht. Eine Buchung wird immer für das volle
            Veranstaltungsentgelt abgegeben.</span
          >
          <h2>7. Widerrufsrecht</h2>
          <span
            >Das Bamberger Ferienabenteuer ist gemäß § 312g Nr. 9 BGB (Verträge
            zur Erbringung von Dienstleistungen im Zusammenhang mit
            Freizeitbetätigungen, wenn der Vertrag für die Erbringung einen
            spezifischen Termin oder Zeitraum vorsieht), vom Widerrufsrecht
            ausgenommen.<br /><br />Die Veranstalter des Bamberger
            Ferienabenteuers gewähren ein freiwilliges Widerrufsrecht.<br /><br />Der
            Widerruf ist zu richten an den jeweiligen Veranstalter. Die
            Widerrufsfrist beträgt 14 Tage ab dem Tag des Vertragsabschlusses.
            Über das Widerrufsrecht wird vor Vertragsabschluss und in der
            Buchungsbestätigung für alle Veranstalter des Ferienabenteuers
            einheitlich informiert. Ein Widerrufsformular, das verwendet werden
            kann, jedoch nicht verwendet werden muss, steht auf der Homepage zur
            Verfügung.</span
          >
          <h2>8. Rücktritt vom Vertrag</h2>
          <span>
            <span class="font-bold"
              >a) Rücktrittsrecht der Familie (Abmeldung):</span
            ><br /><br />Eine Familie kann unter folgenden Bedingungen vom
            Vertrag zurücktreten:<br /><br /><span class="font-bold"
              >Bei Rücktritt wegen Änderung des Angebots</span
            >
            (vgl. Punkt 3) durch den Veranstalter werden die
            Veranstaltungsentgelte in voller Höhe zurückerstattet. Der Rücktritt
            ist unverzüglich zu erklären.<br /><br /><span class="font-bold"
              >Bei Rücktritt bis 6 Wochen vor Veranstaltungsbeginn</span
            >
            werden die Veranstaltungskosten abzüglich einer
            Bearbeitungspauschale in Höhe von 20,00 Euro zurückerstattet. Wurden
            die Veranstaltungskosten zum Zeitpunkt des Rücktritts noch nicht
            bezahlt, hat die Familie die Bearbeitungspauschale an den
            Veranstalter zu entrichten.<br /><br /><span class="font-bold"
              >Bei Rücktritt ab 6 Wochen vor Veranstaltungsbeginn</span
            >
            sind Rückerstattungen grundsätzlich nicht mehr möglich. Die Familie
            ist zur vollen Zahlung der Veranstaltungskosten verpflichtet, es sei
            denn:<br /><br
          /></span>
          <div class="ml-5">
            <ul class="list-disc">
              <li>der Platz kann über eine Warteliste wiederbesetzt werden</li>
              <li>die Familie stellt eine geeignete Ersatzperson</li>
              <li>die Familie legt ein ärztliches Attest vor</li>
              <li>
                der Veranstalter erkennt sonstige triftige Rücktrittsgründe der
                Familie im eigenen Ermessen an.
              </li>
            </ul>
          </div>
          <span
            ><br /><br />In diesen Ausnahmefällen werden die
            Veranstaltungskosten abzüglich einer Bearbeitungspauschale in Höhe
            von 20,00 Euro zurückerstattet. Wurden die Veranstaltungskosten zum
            Zeitpunkt des Rücktritts noch nicht bezahlt, hat die Familie die
            Bearbeitungspauschale an den Veranstalter zu entrichten.<br /><br />Der
            Rücktritt erfolgt durch eindeutige Erklärung der Familie gegenüber
            dem jeweiligen Veranstalter. Die Erklärung kann persönlich,
            telefonisch oder in Textform erfolgen. Im Fall der Textform reichte
            es zur Wahrung der Frist aus, dass die Erklärung vor Ablauf der
            Rücktrittsfrist abgesendet wird (Poststempel). Die Kontaktdaten des
            Veranstalters sind dem Angebot sowie der Buchungsbestätigung zu
            entnehmen.<br /><br />Der Rücktritt wird vom Veranstalter in
            Textform bestätigt<br /><br />
            <span class="font-bold"
              >Für den Buß- und Bettag wird kein Rücktrittsrecht gewährt.</span
            ></span
          >
          <span
            ><br /><span class="font-bold"
              >b) Rücktrittsrecht des Veranstalters</span
            ><br />
            <br /><span class="font-bold"
              >Rücktritt wegen nicht vertragsgemäß erbrachter Leistung<br
            /></span>
            Werden die Veranstaltungsentgelte innerhalb der Zahlungsfrist nicht
            oder nicht vollständig bezahlt, kann der Veranstalter vom Vertrag
            zurücktreten. Eine Mahnung oder weitere Fristsetzung ist hierzu
            nicht erforderlich. Die Familie wird in Textform über den Rücktritt
            informiert. Die Buchung verfällt ersatzlos, der gebuchte Platz wird
            im Buchungssystem wieder verfügbar.<br /><br /><span
              class="font-bold"
              >Ausfall einer Abenteuerwoche</span
            ><br />Der Veranstalter kann vom Vertrag zurücktreten, wenn 2 Wochen
            vor Veranstaltungsbeginn die Mindestteilnehmerzahl nicht erreicht
            ist, oder wenn die Durchführung der Veranstaltung aus tatsächlichen
            oder rechtlichen Gründen unmöglich ist oder wird.<br />Das gleiche
            gilt, wenn infolge von Ereignissen, die bei Vertragsschluss nicht
            vorhersehbar waren, die Leistung aus tatsächlichen oder rechtlichen
            Gründen einen Aufwand erfordert, der in einem groben Missverhältnis
            zum Interesse der angemeldeten Familien an der Veranstaltung steht.
            Nach Rücksprache mit der Familienregion informiert der Veranstalter
            umgehend alle angemeldeten Familien. Der Veranstalter weist dabei
            auf ggf. noch verfügbare Veranstaltungen des Ferienabenteuers hin.
            Ein Anspruch auf ein Ersatzangebot besteht nicht.</span
          >
          <h2>9. Haftung</h2>
          <span>
            Bei der Verletzung des Lebens, des Körpers oder der Gesundheit
            haftet der Veranstalter für alle durch ihn, einen gesetzlichen
            Vertreter oder einen Erfüllungsgehilfen verursachten Schäden nach
            den gesetzlichen Vorschriften. Für sonstige durch ihn, eine
            gesetzliche Vertretung oder einen Erfüllungsgehilfen verursachte
            Schäden haftet der Veranstalter nur bei Vorsatz und grober
            Fahrlässigkeit. Bei durch den Veranstalter, einer gesetzlichen
            Vertretung oder einen Erfüllungsgehilfen fahrlässig verursachten
            Sach- und Vermögensschäden haftet der Veranstalter nur bei der
            Verletzung einer wesentlichen Vertragspflicht, jedoch der Höhe nach
            beschränkt auf die bei Vertragsschluss vorhersehbaren und
            vertragstypischen Schäden. Wesentliche Vertragspflichten sind
            solche, deren Erfüllung die ordnungsgemäße Durchführung des
            Vertrages überhaupt erst ermöglicht, deren Verletzung die Erreichung
            des Vertragszwecks gefährdet und auf deren Einhaltung der
            Vertragspartner regelmäßig vertrauen darf.<br /><br />Die
            Familienregion bzw. Stadt und Landkreis Bamberg haften im Rahmen der
            gesetzlichen Bestimmungen lediglich für die ordnungsgemäße
            Buchungsabwicklung.
          </span>
          <span class="mt-10"> Stand: Dezember 2023 </span>
        </div>
      </div>
    </div>
  `,
  styles: [
    `
      :host {
        @apply text-neutralGrayDark;
      }

      h2 {
        @apply font-bold mt-4 mb-2;
      }
    `,
  ],
})
export class ConditionsComponent {}
