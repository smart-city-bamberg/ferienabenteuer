import { Component } from '@angular/core';

@Component({
  selector: 'app-datenschutzerklaerung',
  template: `
    <div class="landing-page-padding mb-12">
      <h1 class="mb-2 pt-10 text-xl font-extrabold md:text-2xl">
        Datenschutzhinweise nach Art. 13 der europäischen
        Datenschutzgrundverordnung <br />
        (DSGVO)
      </h1>
      <div class="flex flex-col">
        <h2>Allgemeiner Hinweis</h2>
        <span
          >Der Stadt Bamberg und dem Landratsamt Bamberg ist Datenschutz ein
          wichtiges Anliegen. Wir legen deshalb auch bei der mit unserer
          Aufgabenerfüllung verbundenen Verarbeitung personenbezogener Daten
          Wert auf eine datensparsame und bürgerfreundliche
          Datenverarbeitung.<br />
          Diese Datenschutzerklärung bezieht sich auch auf die Verarbeitung
          personenbezogener Daten und Informationen im Sinne des § 25 TTDSG im
          Rahmen dieses Internetauftritts, einschließlich der dort angebotenen
          Dienste.<br />Die folgenden Hinweise geben einen Überblick darüber,
          was mit Ihren personenbezogenen Daten passiert, wenn Sie unsere
          Website besuchen. Personenbezogene Daten sind alle Daten, mit denen
          Sie persönlich identifiziert werden können.
        </span>
        <h2>
          1. Wer ist verantwortlich für die Datenerfassung auf dieser Website?
        </h2>
        <span>Verantwortlich für die Verarbeitung Ihrer Daten:</span>
        <div class="mt-3 flex flex-col">
          <span class="notranslate"
            ><span class="font-bold">Stadt Bamberg</span><br />
            Maximiliansplatz 3<br />
            96047 Bamberg<br />
            <div class="flex flex-wrap">
              <p class="translate">Vertreten durch:&nbsp;</p>
              <p>Oberbürgermeister Andreas Starke<br /></p>
            </div>
          </span>
          <span class="mt-3">
            Telefon:
            <a aria-label="+49951870 anrufen" class="link" href="tel:+49951870">
              +49 (0) 951 87 - 0
            </a>
          </span>
          Fax: +49 (0) 951 87 - 1964
          <span
            >E-Mail:
            <a
              aria-label="E-Mail an stadtverwaltung@stadt.bamberg.de"
              class="link"
              href="mailto:stadtverwaltung@stadt.bamberg.de"
              >stadtverwaltung&#64;stadt.bamberg.de</a
            >
          </span>
        </div>
        <div class="mt-3 flex flex-col">
          <span class="notranslate"
            ><span class="font-bold">Landkreis Bamberg</span><br />
            Ludwigstraße 23<br />
            96052 Bamberg<br />
            <div class="flex flex-row flex-wrap">
              <p class="translate">Vertreten durch:&nbsp;</p>
              Herrn Landrat Johann Kalb<br />
            </div>
          </span>
          <span class="mt-3">
            Telefon:
            <a aria-label="+49951850 anrufen" class="link" href="tel:+49951850">
              +49 (0) 951 85 - 0
            </a>
          </span>
          Fax: +49 (0) 951 85 - 125
          <span
            >E-Mail:
            <a
              aria-label="E-Mail an poststelle@lra-ba.bayern.de"
              class="link"
              href="mailto:poststelle@lra-ba.bayern.de"
              >poststelle&#64;lra-ba.bayern.de</a
            >
          </span>
        </div>
        <div class="mt-3 flex flex-col">
          <span
            ><span class="font-bold"
              >Kontaktdaten des/der Datenschutzbeauftragten:</span
            ><br />
            Behörderliche/r Datenschutzbeauftragte/r der Stadt Bamberg:<br />
            <span class="notranslate">
              Bauer-Banzhaf<br />
              Fachbereich 6A<br />
              Untere Sandstraße 34<br />
              96049 Bamberg<br
            /></span>
          </span>
          <span class="mt-3">
            Telefon:
            <a
              aria-label="+49951871130 anrufen"
              class="link"
              href="tel:+49951871130"
            >
              0951 87 - 1130
            </a>
          </span>
          Fax: 0951 87 - 1139
          <span
            >E-Mail:
            <a
              aria-label="E-Mail an datenschutz@stadt.bamberg.de"
              class="link"
              href="mailto:datenschutz@stadt.bamberg.de"
              >datenschutz&#64;stadt.bamberg.de</a
            >
          </span>
          <span>
            <br />Behörderliche/r Datenschutzbeauftragte/r des Landratsamts
            Bamberg:<br />
            <span class="notranslate"
              >Landratsamt Bamberg<br />
              Stabstelle Datenschutz<br />
              Ludwigstraße 23<br />
              96052 Bamberg<br
            /></span>
          </span>
          <span class="mt-3">
            Telefon:
            <a
              aria-label="+4995185198 anrufen"
              class="link"
              href="tel:+4995185198"
            >
              0951 85 - 198
            </a>
            /
            <a
              aria-label="+4995185199 anrufen"
              class="link"
              href="tel:+4995185199"
            >
              0951 85 - 199
            </a>
          </span>
          <span
            >E-Mail:
            <a
              aria-label="E-Mail an dsb@lra-ba.bayern.de"
              class="link"
              href="mailto:dsb@lra-ba.bayern.de"
              >dsb&#64;lra-ba.bayern.de</a
            >
          </span>
        </div>
        <h2>
          2. Zwecke und Rechtsgrundlagen für die Verarbeitung personenbezogener
          Daten
        </h2>
        <span
          >Für die Organisation Ihrer Buchung, die Kommunikation zwischen den
          Vertragspartnern, die Teilnahme beim Bamberger Ferienabenteuer und die
          reibungslose Durchführung der Veranstaltungen werden personenbezogenen
          Daten benötigt. Die Bereitstellung der personenbezogenen Daten ist
          Voraussetzung für die Teilnahme beim Bamberger Ferienabenteuer und die
          daraus resultierenden Begründung eines Vertragsverhältnisses. Wird das
          Anmeldeverfahren über das Onlinebuchungsportal durchgeführt, werden
          personenbezogene Daten bei der Registrierung erhoben. Demnach ist der
          Zweck der Verarbeitung die Erfüllung des Vertrags bzw. die
          Durchführung vorvertraglichen Maßnahmen, die auf Ihre Anfrage
          erfolgen. Die Rechtsgrundlage für die Verarbeitung Ihrer Daten ergibt
          sich somit aus Art. 4 Abs. 1 des Bayerischen Datenschutzgesetzes
          (BayDSG) in Verbindung mit Art. 6 Abs 1 Buchst. b), c) bzw. e) der
          Datenschutzgrundverordnung (DSGVO) in Verbindung mit Art. 6 Abs. 3
          DSGVO und Art. 57 GO sowie Art. 52 LKrO.
        </span>
        <h2>3. Wer kann auf Ihre personenbezogenen Daten zugreifen?</h2>
        <span
          >Im Rahmen der technischen Verwaltung haben der Administrator der
          Homepage (Netscrapers UG) und der Webhoster (Alfahosting) Datenzugang.
          Die Daten sind hierbei durch Vereinbarungen zur
          Auftragsdatenverarbeitung geschützt.<br /><br />
          Zur Buchungsverwaltung und Durchführung der Abenteuerwoche werden die
          Daten dem jeweiligen Veranstalter über einen geschützten, auf die
          eigenen Teilnehmer beschränkten Datenbankzugang bereitgestellt. Stadt
          und Landkreis unterstützen die Veranstalter des Ferienabenteuers so
          bei der Erfüllung eigener Aufgaben.
        </span>
        <h2>4. Wie werden Ihre Daten durch den Veranstalter geschützt?</h2>
        <span
          >Die Veranstalter sind für den Schutz der Daten ab Abruf von der
          Datenbank selbst verantwortlich. Sie treffen dazu alle laut DSGVO
          notwendigen Maßnahmen, insbesondere
        </span>
        <div class="ml-5">
          <ul class="list-disc">
            <li>Benennung eines Verantwortlichen für Datenverarbeitung</li>
            <li>
              Führen eines Verzeichnisses über Verarbeitungstätigkeiten (v.a.
              über die Speicherung, Löschung und ggf. Weitergabe von Daten)
            </li>
            <li>Datenschutz-Verpflichtung von Beschäftigten</li>
            <li>
              Löschung der Daten gemäß dem gemeinsamen Datenschutzhinweis für
              das Bamberger Ferienabenteuer
            </li>
            <li>
              Grundlegende technische Sicherheitsmaßnahmen zum Schutz der Daten
            </li>
            <li>
              Meldung von Datenschutzverletzungen beim Bayerischen Landesamt für
              Datenschutzaufsicht sowie bei Stadt und Landkreis Bamberg
            </li>
          </ul>
        </div>
        <span class="mt-3"
          >Stadt und Landkreis Bamberg erfüllen die Informationspflicht bei der
          Erhebung der Daten stellvertretend für die Veranstalter. Weitere
          Informationspflichten der Veranstalter bleiben davon unberührt.</span
        >
        <h2>5. Wie lange werden Ihre personenbezogenen Daten gespeichert?</h2>
        <span
          >Wir speichern Ihre Daten solange dies für die Erfüllung der Aufgabe,
          zu Dokumentationspflichten bzw. aufgrund gesetzlicher
          Aufbewahrungsfristen erforderlich ist. Die Aufbewahrungsfristen der
          personenbezogenen Daten, u.a. auch Ermäßigungsnachweise und
          Teilnehmerlisten, richten sich grundsätzlich nach den Vorgaben der
          DSGVO (Art. 17) und des EAPl-Aufbewahrungsfristenverzeichnisses
          (Einheitsaktenplan für die bayerischen Gemeinden und Landratsämter mit
          Verzeichnis der Aufbewahrungsfristen).<br />Die personenbezogenen
          Daten, die bei einer Buchung einer Veranstaltung erfasst werden, sind
          für die Nutzer im persönlichen Profil in der Regel bis zwei Jahre nach
          Abschluss der Veranstaltung einsehbar.<br /><br />Eine Löschung Ihrer
          Daten erfolgt in der Regel nach drei Jahren, sofern alle Anmeldungen
          im Portal abschließend abgewickelt wurden. Bei drei Jahren Inaktivität
          im Portal, wird, nachdem Sie darüber informiert werden, auch das ganze
          Profil einschließlich aller vorhandenen Daten gelöscht.
          Ermäßigungsnachweise und Teilnehmerlisten/Rechnungen der
          Zuschussverwaltung werden nach sechs Jahren gelöscht.
        </span>
        <h2>6. Welche Rechte haben Sie bezüglich Ihrer Daten?</h2>
        <span
          >Soweit wir von Ihnen personenbezogene Daten verarbeiten, stehen Ihnen
          als Betroffener nachfolgende Rechte zu:</span
        >
        <div class="ml-5">
          <ul class="list-disc">
            <li>
              Sie haben das Recht auf Auskunft über die zu Ihrer Person
              gespeicherten Daten (Art. 15 DSGVO).
            </li>
            <li>
              Sollten unrichtige personenbezogene Daten verarbeitet werden,
              steht Ihnen ein Recht auf Berichtigung zu (Art. 16 DSGVO).
            </li>
            <li>
              Liegen die gesetzlichen Voraussetzungen vor, so können Sie die
              Löschung oder Einschränkung der Verarbeitung verlangen (Art. 17
              und 18 DSGVO).
            </li>
            <li>
              Wenn Sie in die Verarbeitung eingewilligt haben oder ein Vertrag
              zur Datenverarbeitung besteht und die Datenverarbeitung mithilfe
              automatisierter Verfahren durchgeführt wird, steht Ihnen
              gegebenenfalls ein Recht auf Datenübertragbarkeit zu (Art. 20
              DSGVO).
            </li>
            <li>
              Falls Sie in die Verarbeitung eingewilligt haben und die
              Verarbeitung auf dieser Einwilligung beruht, können Sie die
              Einwilligung jederzeit für die Zukunft widerrufen.
            </li>
          </ul>
        </div>
        <span class="mt-3"
          >Die Rechtmäßigkeit der aufgrund der Einwilligung bis zum Widerruf
          erfolgten Datenverarbeitung wird durch diesen nicht berührt.<br />
          <br />Sie haben das Recht, aus Gründen, die sich aus Ihrer besonderen
          Situation ergeben, jederzeit gegen die Verarbeitung Ihrer Daten
          Widerspruch einzulegen, wenn die Verarbeitung ausschließlich auf
          Grundlage des Art. 6 Abs. 1 Buchst. e oder f DSGVO erfolgt (Art. 21
          Abs. 1 Satz 1 DSGVO).<br /><br />Sie können sich dazu sowohl an den
          Verantwortlichen von Stadt und Landkreis oder als auch des jeweiligen
          Veranstalters wenden. Sollten Sie Fragen haben, so wenden Sie sich
          vertrauensvoll an uns.<br /><br /><span class="font-bold"
            >Beschwerderecht bei der Aufsichtsbehörde</span
          ><br /><br />Im Beschwerdefall steht als Aufsichtsbehörde das
          Landesamt für Datenschutzaufsicht zur Verfügung. Die für uns
          zuständige Aufsichtsbehörde ist der Bayerischen Landesbeauftragte für
          den Datenschutz. Diesen können Sie unter folgenden Kontaktdaten
          erreichen:
        </span>
        <div class="my-3 flex flex-col">
          <span
            >Der Bayerische Landesbeauftragte für den Datenschutz (BayLfD)</span
          >
          <div class="flex flex-col sm:flex-row">
            <span>Adresse:&nbsp;</span>
            <span class="notranslate">Wagmüllerstraße 18, 80538 München</span>
          </div>
          <div class="flex flex-col sm:flex-row">
            <span>Postanschrift:&nbsp;</span>
            <span class="notranslate">Postfach 22 12 19, 80502 München</span>
          </div>
          <span>
            Telefon:
            <a
              aria-label="+49892126720 anrufen"
              class="link"
              href="tel:+49892126720"
            >
              089 212672-0
            </a>
          </span>
          Fax: 089 212672-50
          <span
            >E-Mail:
            <a
              aria-label="E-Mail an poststelle@datenschutz-bayern.de"
              class="link"
              href="mailto:poststelle@datenschutz-bayern.de"
              >poststelle&#64;datenschutz-bayern.de</a
            >
          </span>
          <span
            >Internet:
            <a
              aria-label="Internetseite www.datenschutz-bayern.de"
              class="link"
              href="https://www.datenschutz-bayern.de"
              target="_blank"
              >datenschutz-bayern.de
            </a>
          </span>
        </div>
        <span
          >Bitte beachten Sie, dass bei jeglichen Informationen zu
          personenbezogenen Daten ein Identifikationsnachweis erforderlich ist.
          Damit scheiden Auskünfte dazu am Telefon oder per E-Mail aus.</span
        >
        <h2>7. Widerrufsrecht</h2>
        <span
          >Mit Abschluss der Buchung stimmen Sie der Datenverarbeitung durch
          Stadt und Landkreis Bamberg und den jeweiligen Veranstalter der
          Abenteuerwoche zu. Sie können die Einwilligung jederzeit für die
          Zukunft widerrufen. Der Widerruf kann an den Verantwortlichen von
          Stadt und Landkreis oder des jeweiligen Veranstalters gerichtet
          werden. Die Rechtmäßigkeit der aufgrund der Einwilligung bis zum
          Widerruf erfolgten Datenverarbeitung wird durch diesen nicht berührt.
          Nach einem Widerruf ist die Teilnahme an einer gebuchten Veranstaltung
          nicht mehr möglich.</span
        >
        <h2>8. Informationen zur Datenverarbeitung</h2>
        <span
          >Ihre bei Nutzung unseres Internetauftritts verarbeiteten Daten werden
          gelöscht oder gesperrt, sobald der Zweck der Speicherung entfällt, der
          Löschung der Daten keine gesetzlichen Aufbewahrungspflichten
          entgegenstehen und nachfolgend keine anderslautenden Angaben zu
          einzelnen Verarbeitungsverfahren gemacht werden.</span
        >
        <h2>9. Protokollierung</h2>
        <span>
          Wenn Sie diese oder andere Internetseiten aufrufen, übermitteln Sie
          über Ihren Internetbrowser Daten an unseren Webserver. Die folgenden
          Daten werden während einer laufenden Verbindung zur Kommunikation
          zwischen Ihrem Internetbrowser und unserem Webserver aufgezeichnet:
        </span>
        <div class="ml-5">
          <ul class="list-disc">
            <li>IP-Adresse des anfragenden Rechners,</li>
            <li>Datum und Uhrzeit des Zugriffs,</li>
            <li>Name und URL der abgerufenen Datei,</li>
            <li>Website, von der aus der Zugriff erfolgt (Referrer-URL),</li>
            <li>
              verwendeter Browser und ggf. das Betriebssystem Ihres Rechners,
            </li>
            <li>Statuscodes und übertragene Datenmenge,</li>
          </ul>
        </div>
        <span class="my-2"
          >Diese Daten werden zu folgenden Zwecken verarbeitet:</span
        >
        <div class="ml-5">
          <ul class="list-disc">
            <li>
              Bereitstellung des Internetangebotes einschließlich sämtlicher
              Funktionen und Inhalte,
            </li>
            <li>
              Gewährleistung eines reibungslosen Verbindungsaufbaus der
              Websites,
            </li>
            <li>Gewährleistung einer komfortablen Nutzung unserer Websites,</li>
            <li>Sicherstellung der Systemsicherheit und -stabilität,</li>
            <li>Anonymisierte statistische Auswertung der Zugriffe,</li>
            <li>Optimierung der Webseite,</li>
            <li>
              Weitergabe an Strafverfolgungsbehörden, sofern ein rechtswidriger
              Eingriff/Angriff auf unsere Systeme erfolgt ist,
            </li>
            <li>weitere administrative Zwecke.</li>
          </ul>
        </div>
        <span class="mt-2"
          >Aus Gründen der technischen Sicherheit, insbesondere zur Abwehr von
          Angriffsversuchen auf unseren Webserver, werden diese Daten 14 Tage
          gespeichert.</span
        >
        <h2>10. SSL-Verschlüsselung</h2>
        <span
          >Diese Seite nutzt aus Sicherheitsgründen und zum Schutz der
          Übertragung vertraulicher Inhalte, wie zum Beispiel Bestellungen oder
          Anfragen, die Sie an uns als Seitenbetreiber senden, eine SSL- bzw.
          TLS-Verschlüsselung. Eine verschlüsselte Verbindung erkennen Sie
          daran, dass die Adresszeile des Browsers von „http://“ auf „https://“
          wechselt und an dem Schloss-Symbol in Ihrer Browserzeile.<br /><br />Wenn
          die SSL- bzw. TLS-Verschlüsselung aktiviert ist, können die Daten, die
          Sie an uns übermitteln, nicht von Dritten mitgelesen werden.
        </span>
        <h2>11. Kontaktmöglichkeit über die Website</h2>
        <span
          >Wenn Sie uns per E-Mail, Kontaktformular, Telefon oder Telefax
          kontaktieren, wird Ihre Anfrage inklusive aller daraus hervorgehenden
          personenbezogenen Daten zum Zwecke der Bearbeitung Ihres Anliegens bei
          uns gespeichert und verarbeitet.
          <span class="font-bold"
            >Diese Daten geben wir nicht ohne Ihre Einwilligung weiter.</span
          ><br /><br />
          Die Verarbeitung dieser Daten erfolgt auf Grundlage von Art. 6 Abs. 1
          lit. b DSGVO, sofern Ihre Anfrage mit der Erfüllung eines Vertrags
          zusammenhängt oder zur Durchführung vorvertraglicher Maßnahmen
          erforderlich ist. In allen übrigen Fällen beruht die Verarbeitung auf
          Ihrer Einwilligung (Art. 6 Abs. 1 lit. a DSGVO) und/oder auf unseren
          berechtigten Interessen (Art. 6 Abs. 1 lit. f DSGVO), da wir ein
          berechtigtes Interesse an der effektiven Bearbeitung der an uns
          gerichteten Anfragen haben.<br /><br />Die von Ihnen an uns per
          Kontaktanfragen übersandten Daten verbleiben bei uns, bis Sie uns zur
          Löschung auffordern, Ihre Einwilligung zur Speicherung widerrufen oder
          der Zweck für die Datenspeicherung entfällt (z. B. nach
          abgeschlossener Bearbeitung Ihres Anliegens). Zwingende gesetzliche
          Bestimmungen – insbesondere gesetzliche Aufbewahrungsfristen – bleiben
          unberührt.
        </span>
        <h2>12. Cookies</h2>
        <span
          >Unsere Website verwendet Cookies. Cookies sind kleine Textdateien,
          die über einen Internetbrowser auf einem Computersystem abgelegt und
          gespeichert werden. Cookies richten auf Ihrem Rechner keinen Schaden
          an und enthalten keine Viren. Cookies dienen dazu, unser Angebot
          <span class="font-bold"
            >nutzerfreundlicher, effektiver und sicherer</span
          >
          zu machen.<br /><br />
          Einige der von uns verwendeten Cookies sind so genannte
          „Session-Cookies“. Sie werden nach Ende Ihres Besuchs automatisch
          gelöscht. Andere Cookies bleiben länger auf Ihrem Endgerät gespeichert
          bzw. bis Sie diese löschen. Diese Cookies ermöglichen es uns,
          <span class="font-bold"
            >Ihren Browser beim nächsten Besuch wiederzuerkennen.</span
          ><br />
          Die Cookies, die unsere Website verwendet, werden unten im Einzelnen
          beschrieben.<br /><br />
          Die meisten Browser sind so eingestellt, dass sie die Verwendung von
          Cookies akzeptieren. Diese Funktion kann aber durch die Einstellung
          des Internetbrowsers von Ihnen für die laufende Sitzung oder dauerhaft
          abgeschaltet werden. Nach Ende Ihres Besuches wird Ihr Browser diese
          Cookies automatisch löschen. Bei der Deaktivierung von Cookies kann
          die Funktionalität dieser Website eingeschränkt sein.<br /><br />
          Cookies, die zur Durchführung des elektronischen
          Kommunikationsvorgangs oder zur Bereitstellung bestimmter, von Ihnen
          erwünschter Funktionen erforderlich sind, werden auf Grundlage von
          Art. 6 Abs. 1 lit. f DSGVO gespeichert. Der Websitebetreiber hat ein
          berechtigtes Interesse an der Speicherung von Cookies zur technisch
          fehlerfreien und optimierten Bereitstellung seiner Dienste.<br /><br />
          Sofern eine entsprechende Einwilligung abgefragt wurde (z. B. eine
          Einwilligung zur Anzeige externen Dienste), erfolgt die Verarbeitung
          ausschließlich auf Grundlage von Art. 6 Abs. 1 lit. a DSGVO; die
          Einwilligung ist jederzeit widerrufbar.
        </span>
        <h2>Weiterer Hinweis zur Datenschutzerklärung</h2>
        <span
          >Wir behalten uns vor, diese Datenschutzerklärung gelegentlich
          anzupassen, damit sie stets den aktuellen rechtlichen Anforderungen
          entspricht.<br /><br />
          Weitere Informationen zum Datenschutz und über Ihre Rechte bei der
          Datenverarbeitung finden Sie auf den Homepages der Stadt Bamberg und
          des Landkreises Bamberg sowie der jeweiligen Veranstalter. Alternativ
          erhalten Sie weitere Informationen auch ausgedruckt in der Infothek
          des Landratsamts oder beim Datenschutzbeauftragten der Stadt Bamberg.
        </span>
      </div>
    </div>
  `,
  styles: [
    `
      :host {
        @apply text-neutralGrayDark;
      }

      h2 {
        @apply font-bold mt-4 mb-2;
      }
    `,
  ],
})
export class DataProtectionComponent {}
