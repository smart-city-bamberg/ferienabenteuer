import { Component } from '@angular/core';

@Component({
  selector: 'app-impressum',
  template: `
    <div class="landing-page-padding mb-12">
      <h1 class="mb-2 pt-10 text-xl font-extrabold md:text-2xl">
        Bamberger Ferienabenteuer – Impressum
      </h1>
      <div class="flex flex-col">
        <h2>Betreiber der Internetseite</h2>
        <span
          >Familienregion Bamberg bestehend aus:<br />Stadt Bamberg, vertreten
          durch Herrn Oberbürgermeister Andreas Starke
          <div class="mt-3 flex flex-col">
            <span class="notranslate"
              >Stadtverwaltung Bamberg<br />
              Maximiliansplatz 3<br />
              96047 Bamberg<br />
            </span>
            <span>
              Telefon:
              <a
                aria-label="+49951870 anrufen"
                class="link"
                href="tel:+49951870"
              >
                +49 (0) 951 87 - 0
              </a>
            </span>
            Fax: +49 (0) 951 87 - 888 1442
            <span>
              E-Mail:
              <a
                aria-label="E-Mail an poststelle@stadt.bamberg.de"
                class="link"
                href="mailto:poststelle@stadt.bamberg.de"
                >poststelle&#64;stadt.bamberg.de</a
              >
            </span>
            <span>
              Internet:
              <a
                aria-label="Internetseite www.stadt.bamberg.de"
                class="link"
                href="https://www.stadt.bamberg.de"
                target="_blank"
                >stadt.bamberg.de
              </a>
            </span>
          </div>
        </span>
        <span class="mb-4 mt-4"
          >Landkreis Bamberg, vertreten durch Herrn Landrat Johann Kalb
          <div class="mt-3 flex flex-col">
            <span class="notranslate"
              >Landratsamt Bamberg<br />
              Ludwigstraße 23<br />
              96047 Bamberg<br />
            </span>
            <span>
              Telefon:
              <a
                aria-label="+49951850 anrufen"
                class="link"
                href="tel:+49951850"
              >
                +49 (0) 951 85 - 0
              </a>
            </span>
            Fax: +49 (0) 951 85 - 125
            <span>
              E-Mail:
              <a
                aria-label="E-Mail an poststelle@lra-ba.bayern.de"
                class="link"
                href="mailto:poststelle@lra-ba.bayern.de"
                >poststelle&#64;lra-ba.bayern.de</a
              >
            </span>
            <span>
              Internet:
              <a
                aria-label="Internetseite www.landkreis-bamberg.de"
                class="link"
                href="https://www.landkreis-bamberg.de"
                target="_blank"
                >landkreis-bamberg.de
              </a>
            </span>
          </div>
        </span>
        <span
          >Im Hinblick auf die Veranstaltungen des Bamberger Ferienabenteuers
          bieten die Familienregion bzw. Stadt und Landkreis Bamberg lediglich
          eine Plattform an, über die Informationen zum Bamberger
          Ferienabenteuer eingeholt werden und Veranstaltungen gebucht werden
          können. Die Personensorgeberechtigten buchen Angebote direkt bei den
          Veranstaltern und nicht bei der Familienregion, Stadt oder Landkreis
          Bamberg. Für Stattfinden, Inhalt, Qualität und Durchführung der
          Veranstaltungen sind ausschließlich die Veranstalter
          verantwortlich.</span
        >
        <h2>Ansprechpartnerin für Rückfragen Landratsamt Bamberg</h2>
        <span
          ><div class="flex flex-col">
            <span class="notranslate"
              >Maarit Stierle<br />
              Ludwigstraße 23<br />
              96052 Bamberg<br />
            </span>
            <span>
              Telefon:
              <a
                aria-label="+4995185510 anrufen"
                class="link"
                href="tel:+4995185510"
              >
                +49 (0) 951 85-510
              </a>
            </span>
            Fax: 0951 85-5810
            <span>
              E-Mail:
              <a
                aria-label="E-Mail an maarit.stierle@lra-ba.bayern.de"
                class="link"
                href="mailto:maarit.stierle@lra-ba.bayern.de"
                >maarit.stierle&#64;lra-ba.bayern.de</a
              >
            </span>
          </div>
        </span>
        <h2>Technische Umsetzung und Betreuung</h2>
        <span class="notranslate"
          >Netscrapers UG (haftungsbeschränkt)<br />
          Dr. Christian Stolcis und Steve Zakrzowsky<br />
          Thomas-Mann-Str. 27<br />
          07743 Jena<br />
          <span>
            Internet:
            <a
              aria-label="Internetseite www.netscrapers.com"
              class="link"
              href="https://netscrapers.com/"
              target="_blank"
              >netscrapers.com
            </a>
          </span>
        </span>
        <h2>Haftung für Hyperlinks</h2>
        <span
          >Diese Internetseite enthält Links auf Webseiten Dritter. Auf die
          Inhalte dieser Seiten haben die Familienregion bzw. Stadt und
          Landkreis Bamberg keinerlei Einfluss. Verantwortlich für den Inhalt
          dieser Seiten ist u.a. der entsprechende Betreiber.<br />Die
          Familienregion bzw. Stadt und Landkreis Bamberg übernehmen daher
          keinerlei Gewähr für die Inhalte der verlinkten Seiten (insbesondere
          inhaltliche Korrektheit, Vollständigkeit und Aktualität der
          Informationen). Links auf andere Internetseiten sind weder Ausdruck
          einer Meinung noch eine Empfehlung der Familienregion, der Stadt oder
          des Landkreises Bamberg, sondern dienen zur Ergänzung der hier
          veröffentlichten Informationen.<br />
          Zum Zeitpunkt der Linksetzung wurden die verlinkten Seiten auf ihre
          Rechtmäßigkeit überprüft und Rechtsverstöße waren nicht erkennbar.
          Eine fortwährende Prüfung der Inhalte ist der Familienregion bzw.
          Stadt und Landkreis Bamberg nicht möglich. Jedoch werden sie, sobald
          Sie von der Möglichkeit eines Rechtverstoßes Kenntnis erlangt, den
          Link unverzüglich inhaltlich überprüfen und bei einem Rechtverstoß den
          Link entfernen.
        </span>
        <h2 class="pt-4">Hinweis zum Datenschutz</h2>
        <span
          >Die Familienregion als Betreiber der Homepage nimmt den Schutz der
          persönlichen Daten sehr ernst und hält sich strikt an die Regeln der
          Datenschutzgesetze. Personenbezogene Daten werden nur im technisch
          notwendigen Umfang erhoben und ausschließlich für die Verwaltung des
          Bamberger Ferienabenteuers verwendet. In keinem Fall werden die
          erhobenen Daten verkauft oder zu Zwecken außerhalb der Verwaltung des
          Ferienabenteuers an Dritte weitergegeben.<br />
          Sie haben jederzeit das Recht auf Auskunft über die bezüglich Ihrer
          Person gespeicherten Daten, deren Herkunft und Empfänger sowie den
          Zweck der Datenverarbeitung. Wenden Sie sich dazu an Frau Maarit
          Stierle unter Tel.:
          <a
            aria-label="+4995185510 anrufen"
            class="link"
            href="tel:+4995185510"
            >0951 85-510</a
          >
          oder
          <a
            aria-label="E-Mail an maarit.stierle@lra-ba.bayern.de"
            class="link"
            href="mailto:maarit.stierle@lra-ba.bayern.de"
            >maarit.stierle&#64;lra-ba.bayern.de</a
          >.<br />
          Als zuständiger örtlicher Datenschutzbeauftragter steht Herr Bauer
          Banzhaf zur Verfügung unter Tel.:
          <a
            aria-label="+49951871130 anrufen"
            class="link"
            href="tel:+49951871130"
            >0951 87-1130</a
          >
          oder
          <a
            aria-label="E-Mail an Bernd.Bauer-Banzhaf@stadt.bamberg.de"
            class="link"
            href="mailto:Bernd.Bauer-Banzhaf@stadt.bamberg.de"
            >Bernd.Bauer-Banzhaf&#64;stadt.bamberg.de</a
          >.
        </span>
        <span class="mt-3"
          >Wir weisen darauf hin, dass die Datenübertragung im Internet (z.B.
          bei der Kommunikation per E-Mail) Sicherheitslücken aufweisen kann.
          Ein lückenloser Schutz der Daten vor dem Zugriff durch Dritte ist
          nicht möglich.</span
        >
      </div>
    </div>
  `,
  styles: [
    `
      :host {
        @apply text-neutralGrayDark;
      }

      h2 {
        @apply font-bold mt-4 mb-2;
      }
    `,
  ],
})
export class LegalNoticeComponent {}
