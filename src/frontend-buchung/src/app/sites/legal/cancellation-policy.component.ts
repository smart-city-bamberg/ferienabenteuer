import { Component } from '@angular/core';

@Component({
  selector: 'app-widerrufsbelehrung',
  template: `
    <div class="landing-page-padding mb-12">
      <h1 class="mb-4 pt-10 text-xl font-extrabold md:text-2xl">
        Bamberger Ferienabenteuer – Widerrufsbelehrung
      </h1>
      <div class="flex flex-col">
        <h2>Widerrufsrecht</h2>
        <span>
          Sie haben das Recht, binnen vierzehn Tagen nach Vertragsabschluss ohne
          Angabe von Gründen Ihren Vertrag zu widerrufen.<br />
          Die Widerrufsfrist beträgt vierzehn Tage ab dem Tag des
          Vertragsabschlusses.<br />
          Um Ihr Widerrufsrecht auszuüben, müssen Sie dem jeweiligen
          Veranstalter des Ferienabenteuers mittels einer eindeutigen Erklärung
          (z. B. ein mit der Post versandter Brief, Telefax oder E-Mail) über
          Ihren Entschluss, den Vertrag zu widerrufen, informieren. Sie können
          dafür das beigefügte Muster-Widerrufsformular verwenden, das jedoch
          nicht vorgeschrieben ist.<br />
          Zur Wahrung der Widerrufsfrist reicht es aus, dass Sie die Mitteilung
          über die Ausübung des Widerrufsrechts vor Ablauf der Widerrufsfrist
          absenden.
        </span>
        <h2>Folgen des Widerrufs</h2>
        <span>
          Wenn Sie Ihren Vertrag widerrufen, hat Ihnen der Veranstalter alle
          Zahlungen, die er von Ihnen erhalten hat, unverzüglich und spätestens
          binnen vierzehn Tagen ab dem Tag zurückzuzahlen, an dem die Mitteilung
          über Ihren Widerruf dieses Vertrags beim Veranstalter eingegangen ist.
          Für diese Rückzahlung verwendet der Veranstalter dasselbe
          Zahlungsmittel, das Sie bei der ursprünglichen Transaktion eingesetzt
          haben, es sei denn, mit Ihnen wurde ausdrücklich etwas anderes
          vereinbart; in keinem Fall werden Ihnen wegen dieser Rückzahlung
          Entgelte berechnet.
        </span>
        <h2>Bei Erbringung von Dienstleistungen</h2>
        <span>
          Hat die gebuchte Veranstaltung bereits während des Laufs der
          Widerrufsfrist begonnen, so haben Sie dem Veranstalter einen
          angemessenen Betrag zu zahlen, der dem Anteil der bis zu dem
          Zeitpunkt, zu dem Sie den Veranstalter von der Ausübung Ihres
          Widerrufsrechts unterrichten, bereits erbrachten Dienstleistungen im
          Vergleich zum Gesamtumfang der im Vertrag vorgesehenen
          Dienstleistungen entspricht.
        </span>
        <h2>Freiwillige Einräumung</h2>
        <span>
          Das Bamberger Ferienabenteuer ist gemäß 312g Abs. 2 Nr. 9 BGB
          (Verträge zur Erbringung von Dienstleistungen im Zusammenhang mit
          Freizeitbetätigungen, wenn der Vertrag für die Erbringung einen
          spezifischen Termin oder Zeitraum vorsieht), vom Widerrufsrecht
          ausgenommen. Das oben genannte Widerrufsrecht wird von den
          Veranstaltern des Bamberger Ferienabenteuers freiwillig gewährt.
        </span>
        <a
          aria-label="Widerrufsformular herunterladen (PDF)"
          class="mt-4 flex"
          href="../assets/files/Ferienabenteuer_Widerruf.pdf"
          download="Ferienabenteuer_Widerruf.pdf"
        >
          <button tabindex="-1" class="flex-grow gap-2 md:flex-grow-0">
            <i class="mi notranslate">download</i>
            Widerrufsformular herunterladen (PDF)
          </button>
        </a>
      </div>
    </div>
  `,
  styles: [
    `
      :host {
        @apply text-neutralGrayDark;
      }
      h2 {
        @apply font-bold my-2;
      }
      span {
        @apply mb-2;
      }
    `,
  ],
})
export class CancellationPolicyComponent {}
