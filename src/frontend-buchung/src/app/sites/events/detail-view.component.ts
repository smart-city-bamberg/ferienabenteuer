import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { filter, map } from 'rxjs';
import { selectUser } from 'src/app/stores/auth/auth.selectors';
import { EventActions } from 'src/app/stores/event/event.actions';
import * as eventSelectors from 'src/app/stores/event/event.selectors';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-detail-view',
  template: `
    <div
      *ngIf="event$ | async; let event; else: doesNotExists"
      class="flex flex-col"
    >
      <div class="flex flex-col md:gap-3 lg:flex-row">
        <div class="mb-4 flex flex-col gap-4 md:mb-0 lg:w-1/2">
          <div
            class="white-card flex h-48 flex-col justify-end bg-cover bg-center"
            [style.background-image]="
              event.picture
                ? 'url(' +
                  (event.picture.url | imageProxy : '600x,jpg,q75') +
                  ')'
                : 'none'
            "
          ></div>
          <div class="white-card flex flex-col items-center justify-center p-2">
            <h2 class="notranslate mb-1 text-center leading-tight">
              {{ event.name }}
            </h2>

            <div
              class="flex flex-row flex-wrap justify-center gap-0 text-xs sm:gap-2 md:text-sm"
            >
              <p class="flex items-center">
                <i aria-label="Zeitraum," class="mi notranslate mr-1"
                  >calendar_month</i
                >
                <app-care-days
                  [careDays]="event.carePeriodCommitted.careDays"
                  [startFormat]="'dd. MMM yyyy'"
                  [endFormat]="'dd. MMM yyyy'"
                />
              </p>
              <p class="notranslate ml-1 flex items-start">
                <i aria-label="Adresse," class="mi mr-1 mt-[0.25rem]"
                  >location_on</i
                >
                <a
                  class="link leading-tight"
                  [href]="
                    'https://www.google.com/maps?q=' +
                    (event.locationCity + ', ' + event.locationStreet)
                  "
                  target="_blank"
                >
                  {{ event.locationCity }}, {{ event.locationStreet }}
                </a>
              </p>
              <p class="ml-1 flex items-center">
                <i aria-label="Alter," class="mi notranslate mr-1">person</i>
                {{ event.ageMinimum }} - {{ event.ageMaximum }} Jahre
              </p>
            </div>
          </div>
          <div class="white-card flex-grow flex-col">
            <div class="border-b-2 border-gray-500 border-opacity-20">
              <h2>Beschreibung</h2>
            </div>
            <div class="content">
              <p>
                {{ event.description }}
              </p>
            </div>
            <div class="flex flex-col md:flex-row md:justify-between md:gap-6">
              <div class="md:w-1/2">
                <div class="mt-4 border-b-2 border-gray-500 border-opacity-20">
                  <h2>Veranstalter</h2>
                </div>
                <div class="notranslate content">
                  <p>
                    {{ event.organizer.name }}
                  </p>
                </div>
              </div>
              <div class="md:w-1/2">
                <div class="mt-4 border-b-2 border-gray-500 border-opacity-20">
                  <h2>Kontakt</h2>
                </div>
                <div class="content">
                  <p>
                    {{
                      event.contactPersonSalutation == 'ms' ? 'Frau' : 'Herr'
                    }}&nbsp;
                    <span class="notranslate"
                      >{{ event.contactPersonFirstname }}
                      {{ event.contactPersonSurname }}</span
                    >
                  </p>
                  <div class="flex flex-row gap-1">
                    <div class="flex flex-col">
                      <p>Telefon:</p>
                      <p>E-Mail:</p>
                    </div>
                    <div class="flex flex-col">
                      <a class="link">{{ event.organizer.phone }}</a>
                      <a class="link">{{ event.organizer.email }}</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="mt-4 border-b-2 border-gray-500 border-opacity-20">
              <h2>Ort/Treffpunkt</h2>
            </div>
            <div class="notranslate content">
              <p>{{ event.locationStreet }}</p>
              <p>{{ event.locationZipcode }} {{ event.locationCity }}</p>
            </div>
          </div>
        </div>
        <div class="flex flex-col gap-4 lg:w-1/2">
          <div class="white-card flex flex-grow flex-col">
            <div class="flex flex-grow flex-col">
              <div class="mt-4 border-b-2 border-gray-500 border-opacity-20">
                <h2>Hinweise</h2>
              </div>
              <div class="content">
                <p>
                  {{ event.hints }}
                </p>
              </div>
              <div class="mt-4 border-b-2 border-gray-500 border-opacity-20">
                <h2>Verpflegung</h2>
              </div>
              <div class="content">
                <p>{{ event.meals }}</p>
              </div>
              <div class="flex flex-col">
                <div class="mt-4 border-b-2 border-gray-500 border-opacity-20">
                  <h2>Anmeldefrist</h2>
                </div>
                <div class="content">
                  <p>{{ event.registrationDeadline | date : 'dd.MM.yyyy' }}</p>
                </div>
              </div>
              <div class="mt-4 flex flex-col">
                <div class="border-b-2 border-gray-500 border-opacity-20">
                  <h2>Betreuung</h2>
                </div>
                <div class="content mb-2">
                  <p class="flex items-center">
                    <i aria-label="Ferien," class="mi notranslate mr-1"
                      >stars</i
                    >
                    {{ event.carePeriodCommitted.period.holiday.name }}
                  </p>

                  <p class="items-center">
                    <i aria-label="Zeitraum," class="mi notranslate mr-1"
                      >calendar_month</i
                    >
                    <app-care-days
                      [careDays]="event.carePeriodCommitted.careDays"
                      [startFormat]="'dd. MMM yyyy'"
                      [endFormat]="'dd. MMM yyyy'"
                    />
                  </p>
                  <p class="notranslate items-center">
                    <i aria-label="Uhrzeit," class="mi mr-1">schedule</i>
                    {{ event.careTimeStart }} - {{ event.careTimeEnd }}
                  </p>
                </div>
                <app-care-days-calendar
                  [careDays]="periodPrimaryCareDays$ | async"
                />
              </div>
            </div>
            <div class="mt-4 flex flex-col md:gap-6 xl:flex-row">
              <div class="xl:w-1/2">
                <div
                  class="flex flex-row flex-wrap items-center justify-between border-b-2 border-gray-500 border-opacity-20"
                >
                  <h2 aria-hidden="true">Teilnehmerliste</h2>
                  <p
                    [attr.aria-label]="
                      'Teilnehmerliste: ' +
                      (event.participantLimit -
                        event.availableTicketCountParticipant) +
                      'von' +
                      event.participantLimit +
                      'Plätzen belegt'
                    "
                    [class.green]="event.availableTicketCountParticipant > 0"
                    [class.red]="event.availableTicketCountParticipant == 0"
                    class="badge notranslate mb-1 w-20"
                  >
                    {{
                      event.participantLimit -
                        event.availableTicketCountParticipant
                    }}
                    |
                    {{ event.participantLimit }}
                  </p>
                </div>
                <div
                  aria-hidden="true"
                  class="mt-3 flex flex-row flex-wrap gap-1"
                >
                  <div
                    *ngFor="
                      let participant of Array.from({
                        length:
                          event.participantLimit -
                          event.availableTicketCountParticipant
                      })
                    "
                  >
                    <div class="lists red">
                      <i class="mi notranslate">person</i>
                    </div>
                  </div>
                  <div
                    *ngFor="
                      let participant of Array.from({
                        length: event.availableTicketCountParticipant
                      })
                    "
                  >
                    <div class="lists green">
                      <i class="mi notranslate text-transparent">person</i>
                    </div>
                  </div>
                </div>
              </div>
              <div class="mt-4 md:mt-0 xl:w-1/2">
                <div
                  class="flex flex-row flex-wrap items-center justify-between border-b-2 border-gray-500 border-opacity-20"
                >
                  <h2 aria-hidden="true">Warteliste</h2>
                  <p
                    [attr.aria-label]="
                      'Warteliste: ' +
                      (event.waitingListLimit -
                        event.availableTicketCountWaitinglist) +
                      'von' +
                      event.waitingListLimit +
                      'Plätzen belegt'
                    "
                    [class.green]="event.availableTicketCountWaitinglist > 0"
                    [class.red]="event.availableTicketCountWaitinglist == 0"
                    class="notranslate badge mb-1 w-20"
                  >
                    {{
                      event.waitingListLimit -
                        event.availableTicketCountWaitinglist
                    }}
                    | {{ event.waitingListLimit }}
                  </p>
                </div>
                <div
                  aria-hidden="true"
                  class="mt-3 flex flex-row flex-wrap gap-1"
                >
                  <div
                    *ngFor="
                      let participant of Array.from({
                        length:
                          event.waitingListLimit -
                          event.availableTicketCountWaitinglist
                      })
                    "
                  >
                    <div class="lists red">
                      <i class="mi notranslate">person</i>
                    </div>
                  </div>
                  <div
                    *ngFor="
                      let participant of Array.from({
                        length: event.availableTicketCountWaitinglist
                      })
                    "
                  >
                    <div class="lists green">
                      <i class="mi notranslate text-transparent">person</i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="mt-6 flex flex-row items-center">
              <div class="lists red ml-[0.1rem]">
                <i aria-label="Wartelistenplatz belegt" class="mi notranslate"
                  >person</i
                >
              </div>
              <p class="ml-1">Belegt</p>
              <div class="lists green ml-6">
                <i
                  aria-label="Teilnehmerplatz frei"
                  class="mi notranslate text-transparent"
                  >person</i
                >
              </div>
              <p class="ml-1">Frei</p>
            </div>
          </div>
        </div>
      </div>

      <h3 class="site-title mt-8">Tickets</h3>

      <div
        *ngFor="let ticket of tickets$ | async; trackBy: identifyById"
        class="white-card mt-3 flex flex-col justify-between rounded-md border-2 border-gray-500 border-opacity-40 p-3 text-textDark md:flex-row md:items-center"
      >
        <div
          class="flex flex-grow flex-col gap-3 md:flex-row md:gap-6 md:text-lg"
        >
          <div
            class="relative flex flex-col-reverse font-semibold leading-5 md:w-48 xl:w-80"
          >
            <p class="relative flex items-start">
              {{
                ticket.discount
                  ? 'Ermäßigung | ' + ticket.discount.name
                  : 'Normal'
              }}
              <button
                *ngIf="ticket.discount"
                class="peer border-none bg-transparent p-1 text-primaryLight"
              >
                <i
                  [attr.aria-label]="'Info:' + ticket.discount.description"
                  title="Info"
                  class="mi notranslate outlined -mt-1"
                  >info</i
                >
              </button>
              <span
                *ngIf="ticket.discount"
                class="absolute bottom-7 right-0 hidden w-auto items-start gap-2 rounded-md border border-gray-500 bg-white p-2 leading-tight text-primaryLight shadow-md peer-hover:flex peer-focus:flex"
                ><i
                  *ngIf="ticket.discount"
                  title="asdfgh"
                  class="mi notranslate outlined mt-1"
                  >info</i
                >
                {{ ticket.discount.description }}</span
              >
            </p>
            <p class="text-[#617794]">1 Teilnehmerplatz</p>
          </div>
          <div
            class="flex flex-grow flex-row items-center justify-between gap-1"
          >
            <div class="flex flex-col">
              <div *ngIf="ticket.discount" class="flex w-20 flex-row text-xs">
                <span class="bg-primaryLight/10 px-1 text-primaryLight"
                  >{{ ticket.discount.discountPercent }}&nbsp;%</span
                >
                <p
                  [attr.aria-label]="
                    ticket.originalCosts + ':euro durchgestrichen'
                  "
                  class="ml-1 line-through"
                >
                  {{ ticket.originalCosts | currency : 'EUR' }}
                </p>
              </div>
              <p class="text-lg">
                {{ ticket.costs | currency : 'EUR' }}
              </p>
            </div>

            <div class="flex flex-grow flex-row justify-end gap-0.5">
              <button
                aria-label="Ticket entfernen"
                [disabled]="!bookingEnabled"
                (click)="removeTicket(ticket.discount?.id ?? '')"
                class="h-8 w-8 md:h-9 md:w-9 md:text-2xl"
              >
                <i class="mi notranslate">remove</i>
              </button>
              <p
                class="notranslate w-6 items-center justify-center font-semibold md:text-lg"
              >
                {{ ticket.amount }}
              </p>
              <button
                aria-label="Ticket hinzufügen"
                [disabled]="!bookingEnabled"
                (click)="addTicket(ticket.discount?.id ?? '')"
                class="h-8 w-8 md:h-9 md:w-9 md:text-2xl"
              >
                <i class="mi notranslate">add</i>
              </button>
            </div>
            <span
              class="notranslate flex w-20 justify-end text-lg font-semibold"
            >
              {{ ticket.sum | currency : 'EUR' }}
            </span>
          </div>
        </div>
      </div>

      <div
        *ngIf="totalTicktes$ | async; let totalTickets"
        class="mt-3 flex flex-col justify-end gap-8 md:flex-row"
      >
        <button
          [attr.aria-label]="
            totalTickets.amount +
            (totalTickets.amount === 1 ? 'Ticket:' : ' Tickets:') +
            totalTickets.sum +
            'Euro,' +
            ' Zum Warenkorb hinzufügen'
          "
          class="p-2"
          (click)="addTicketsToCart()"
          [disabled]="!(canAddToCart$ | async) || !bookingEnabled"
        >
          <span class="notranslate flex flex-row items-center">
            <i class="translate mi mr-2">add_shopping_cart</i>
            <span class="mr-1 w-4">{{ totalTickets.amount }}</span>
            <span class="translate"
              >{{
                totalTickets.amount === 1 ? 'Ticket' : 'Tickets'
              }},&nbsp;</span
            >
            {{ totalTickets.sum | currency : 'EUR' }}</span
          >
        </button>
      </div>
    </div>
    <ng-template #doesNotExists>
      <h1 class="mx-8 flex items-center justify-center gap-2 text-xl">
        <i class="mi text-primaryLight">info</i> Diese Veranstaltung existiert
        nicht.
      </h1>
    </ng-template>
  `,
  styles: [
    `
      :host {
        @apply flex flex-col flex-grow;
      }
      h2 {
        @apply text-lg font-extrabold;
      }

      .lists {
        @apply flex rounded;
        &.green {
          @apply bg-primaryGreen/10 border text-primaryGreen border-[#007565];
        }
        &.red {
          @apply bg-primaryRed/10 border text-primaryRed border-[#E90C54];
        }
      }
      .content {
        @apply mt-2 text-sm leading-tight;
      }
    `,
  ],
})
export class DetailViewComponent {
  Array = Array;
  event$ = this.store.select(eventSelectors.selectEvent);
  tickets$ = this.store.select(eventSelectors.selectTickets);

  totalTicktes$ = this.store.select(eventSelectors.selectTotalTickets);

  periodPrimaryCareDays$ = this.store.select(
    eventSelectors.selectEventPrimaryCareDays
  );

  bookingEnabled = environment.features.booking;

  canAddToCart$ = this.store.select(eventSelectors.selectCanAddToCart);
  loggedIn$ = this.store
    .select(selectUser)
    .pipe(map((user) => user != undefined));

  constructor(private store: Store) {}

  addTicket(discountType: string) {
    this.store.dispatch(
      EventActions.addTicket({
        discountType,
      })
    );
  }
  removeTicket(discountType: string) {
    this.store.dispatch(
      EventActions.removeTicket({
        discountType,
      })
    );
  }

  addTicketsToCart() {
    this.store.dispatch(EventActions.addTicketsToCart());
  }

  identifyById(
    index: number,
    entity: { discount: { id: string } | undefined }
  ) {
    return entity.discount?.id ?? 'no-discount-type';
  }
}
