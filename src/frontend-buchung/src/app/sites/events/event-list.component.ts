import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import {
  selectActiveFilter,
  selectBothFreeSlotsFiltersZero,
  selectCategoryFilterEmpty,
  selectCategoryFiltersWithSelection,
  selectEventCount,
  selectFiltersCount,
  selectFreeSlotsFilter,
  selectHolidayFilterEmpty,
  selectHolidayFiltersWithSelection,
  selectHolidaysWithEvents,
  selectSearchText,
  selectSeasons,
  selectSelectedSeason,
  selectShowFilterMobile,
} from '../../stores/events/events.selectors';
import { EventsActions } from 'src/app/stores/events/events.actions';
import { debounceTime, map } from 'rxjs';

@Component({
  selector: 'app-event-list',
  template: `
    <div [class.hidden]="!(showFilterMobile$ | async)">
      <div class="mb-4 flex flex-col gap-3 md:flex-row">
        <select
          id="saisons"
          aria-label="Auswahl Saison"
          class="text-md !m-0 rounded-lg !border !border-primaryDark/30 bg-white px-3 py-2 font-medium text-textDark"
          [ngModel]="selectedSeason$ | async"
          #seasonSelector
          (change)="selectSeason(seasonSelector.value)"
        >
          <option *ngFor="let season of seasons$ | async" [value]="season.year">
            Saison {{ season.year }}
          </option>
        </select>
        <div
          class="flex flex-grow items-center rounded-lg border border-primaryDark/30 bg-white text-gray-600"
        >
          <button type="submit" class="border-none">
            <i class="mi notranslate text-[#757575]">search</i>
          </button>
          <input
            class="text-md flex-grow rounded-lg px-4 py-2 focus:outline-none"
            type="search"
            name="search"
            placeholder="Suchen nach Veranstaltungstitel, Kategorie, Ort"
            aria-label="Suchfeld"
            [ngModel]="searchText$ | async"
            #searchInput
            (input)="changeSearchText(searchInput.value)"
          />
        </div>
      </div>

      <div class="mb-2 text-lg">Ferien</div>

      <div class="flex flex-row flex-wrap gap-2">
        <button
          class="filter"
          [class.defaultFilter]="holidayFilterEmpty$ | async"
          (click)="resetHolidayFilter()"
        >
          Alle
        </button>
        <button
          *ngFor="let holiday of holidayFilters$ | async; trackBy: identifyById"
          class="filter"
          [class.activeFilter]="holiday.selected"
          (click)="toggleHolidayFilter(holiday.id)"
        >
          {{ holiday.name }}
        </button>
      </div>

      <div class="mb-2 mt-4 text-lg">Kategorien</div>

      <div class="flex flex-row flex-wrap gap-2">
        <button
          class="filter"
          [class.defaultFilter]="categoryFilterEmpty$ | async"
          (click)="resetCategoryFilter()"
        >
          Alle
        </button>
        <button
          class="filter"
          [class.activeFilter]="category.selected"
          *ngFor="
            let category of categoryFilters$ | async;
            trackBy: identifyById
          "
          (click)="toggleCategoryFilter(category.id)"
        >
          {{ category.name }}
        </button>
      </div>

      <div class="flex flex-col md:flex-row md:gap-8">
        <div>
          <div class="mb-2 mt-4 text-lg">Freie Teilnehmerplätze</div>

          <div class="flex flex-row flex-wrap gap-2">
            <button
              [class.activeFilter]="freeSlot.selected && freeSlot.amount !== 0"
              [class.defaultFilter]="freeSlot.selected && freeSlot.amount === 0"
              [class.notranslate]="freeSlot.amount !== 0"
              (click)="selectFreeSlotsFilter(freeSlot.amount, 'participant')"
              class="filter"
              *ngFor="
                let freeSlot of freeSlotsFilterParticipants$ | async;
                trackBy: identifyByAmount
              "
            >
              {{ freeSlot.amount === 0 ? 'Alle' : freeSlot.amount }}
            </button>
          </div>
        </div>
        <div>
          <div class="mb-2 mt-4 text-lg">Freie Warteplätze</div>

          <div class="mb-4 flex flex-row flex-wrap gap-2">
            <button
              [class.activeFilter]="freeSlot.selected && freeSlot.amount !== 0"
              [class.defaultFilter]="freeSlot.selected && freeSlot.amount === 0"
              [class.notranslate]="freeSlot.amount !== 0"
              (click)="selectFreeSlotsFilter(freeSlot.amount, 'waitinglist')"
              class="filter"
              *ngFor="
                let freeSlot of freeSlotsFilterWaitinglist$ | async;
                trackBy: identifyByAmount
              "
            >
              {{ freeSlot.amount === 0 ? 'Alle' : freeSlot.amount }}
            </button>
          </div>
        </div>
      </div>
    </div>

    <button class="text-md p-2" (click)="toggleShowFilter()">
      Filter {{ (showFilterMobile$ | async) ? 'ausblenden' : 'anzeigen' }}
      <span
        class="absolute -mt-3 ml-40 inline-flex h-5 w-5 items-center justify-center rounded-full border-2 border-white bg-primaryRed text-xs font-bold text-white"
        *ngIf="activeFilter$ | async"
        >{{ countFilters$ | async }}</span
      >
    </button>

    <div class="mb-2 mt-4 flex flex-row items-center justify-between">
      <h1 class="text-xl font-bold text-textDark">
        <span *ngIf="(eventCount$ | async) == 1; else multipleEvents"
          >1 Veranstaltung</span
        >
        <ng-template #multipleEvents
          >{{ eventCount$ | async }} Veranstaltungen in Saison
          {{ selectedSeason$ | async }}
        </ng-template>
      </h1>
    </div>

    <div
      class="flex flex-col border-gray-500 border-opacity-20 pb-8 pt-4 md:border-l-4 md:pl-3"
      *ngFor="let holiday of holidays$ | async; trackBy: identifyById"
    >
      <div class="flex flex-row items-center gap-4">
        <i
          aria-hidden="true"
          class="mi notranslate -ml-[1.36rem] hidden text-textDark md:block"
          >circle</i
        >
        <h2 class="text-2xl font-bold text-textDark">{{ holiday.name }}</h2>
      </div>

      <div
        *ngIf="holiday.isEmpty; else holidayFilled"
        class="block px-2 py-6 text-sm text-[#626B79]"
      >
        Keine Veranstaltungen vorhanden
      </div>
      <ng-template #holidayFilled>
        <div
          *ngFor="let period of holiday.periods; trackBy: identifyById"
          class="my-4 flex flex-col gap-4 md:ml-2"
        >
          <div
            class="flex flex-row items-center gap-4 text-primaryDark"
            *ngIf="period.events.length > 0"
          >
            <i
              aria-hidden="true"
              class="mi notranslate -ml-[1.72rem] hidden text-xs md:block"
              >circle</i
            >
            <h3
              *ngIf="period.startDate !== period.endDate; else singleDay"
              class="text-md font-bold md:ml-[0.2rem]"
            >
              {{ period.startDate | date : 'dd. MMMM' }} -
              {{ period.endDate | date : 'dd. MMMM' }}
            </h3>
            <ng-template #singleDay>
              <h3 class="text-md font-bold md:ml-[0.2rem]">
                {{ period.startDate | date : 'dd. MMMM' }}
              </h3>
            </ng-template>
          </div>

          <div class="flex flex-col gap-4 md:flex-row md:flex-wrap md:pb-4">
            <a
              *ngFor="let event of period.events; trackBy: identifyById"
              [routerLink]="['/veranstaltungen', event.id]"
              role="link"
              [class.event-expired]="event.isExpired"
              [attr.aria-label]="
                'Veranstaltung: ' +
                event.name +
                ':' +
                event.availableTicketCountParticipant +
                (event.availableTicketCountParticipant === 1
                  ? ' Freier Teilnehmer Platz'
                  : ' Freie Teilnehmer Plätze') +
                ':' +
                event.availableTicketCountWaitinglist +
                (event.availableTicketCountWaitinglist === 1
                  ? ' Freier Wartelisten Platz'
                  : ' Freie Wartelisten Plätze')
              "
              class="flex cursor-pointer flex-col overflow-hidden rounded-xl border border-primaryLight/40 bg-white shadow-md transition-shadow hover:shadow-lg md:w-[25rem]"
            >
              <div
                class="flex flex-col justify-between bg-cover bg-center"
                [style.background-image]="
                  event.picture
                    ? 'url(' +
                      (event.picture.url | imageProxy : '600x,jpeg,q75') +
                      ')'
                    : 'none'
                "
              >
                <div class="mt-3 flex h-20 flex-row items-start justify-end">
                  <div class="flex flex-col gap-1">
                    <p
                      *ngFor="
                        let categorie of event.categories;
                        trackBy: identifyById
                      "
                      class="items-end rounded-l-md border border-r-0 border-gray-300 bg-white px-[0.35rem] text-sm font-extrabold text-textDark shadow-md"
                    >
                      {{ categorie.name }}
                    </p>
                  </div>
                </div>

                <div
                  class="flex h-20 flex-col justify-end bg-gradient-to-t from-black/90 to-transparent px-2 pt-4 [text-shadow:_0_0_0.3rem_rgb(0_0_0_)]"
                >
                  <p class="notranslate text-xs text-white">
                    {{ event.organizer.name }}
                  </p>
                  <p
                    class="notranslate mb-1 font-extrabold leading-none text-white"
                  >
                    {{ event.name }}
                  </p>
                </div>
              </div>
              <div
                class="mx-3 my-2 flex flex-grow flex-row items-center justify-between text-textDark"
              >
                <div class="flex flex-col gap-1 pr-4 text-xs md:text-sm">
                  <p class="flex items-center">
                    <i class="mi notranslate mr-2">calendar_month</i>
                    <app-care-days
                      [careDays]="event.carePeriodCommitted.careDays"
                      [startFormat]="'dd. MMM'"
                      [endFormat]="'dd. MMM'"
                    />
                  </p>
                  <p class="notranslate flex max-h-10 items-start">
                    <i class="mi notranslate mr-2 mt-[0.2rem]">location_on</i>
                    <span class="flex max-h-10 overflow-hidden leading-tight">
                      {{ event.locationCity }}, {{ event.locationStreet }}
                    </span>
                  </p>
                  <p class="flex items-center">
                    <i class="mi notranslate mr-2">person</i>
                    {{ event.ageMinimum }} - {{ event.ageMaximum }} Jahre
                  </p>
                </div>
                <div class="flex flex-col gap-2">
                  <div
                    class="flex flex-row items-center justify-between gap-2 rounded-md p-1"
                  >
                    <div
                      class="notranslate flex flex-row items-center gap-1"
                      [class.listTextGreen]="
                        event.availableTicketCountParticipant > 0
                      "
                      [class.listTextRed]="
                        event.availableTicketCountParticipant === 0
                      "
                    >
                      <i class="mi">event_seat</i>
                      {{ event.availableTicketCountParticipant }}
                    </div>
                    <div
                      class="notranslate flex flex-row items-center gap-1"
                      [class.listTextGreen]="
                        event.availableTicketCountWaitinglist > 0
                      "
                      [class.listTextRed]="
                        event.availableTicketCountWaitinglist === 0
                      "
                    >
                      <i class="mi outlined">hourglass</i>
                      {{ event.availableTicketCountWaitinglist }}
                    </div>
                  </div>
                  <div class="flex justify-end text-3xl font-black">
                    {{ event.costs }}&nbsp;€
                  </div>
                </div>
              </div>
            </a>
          </div>
        </div>
      </ng-template>
    </div>
  `,
  styles: [
    `
      :host {
        @apply flex flex-col flex-grow;
      }
      p {
        @apply flex flex-row;
      }

      .filter {
        @apply bg-white text-primaryLight rounded-md px-3 h-8 md:h-10 border border-primaryLight/30 md:hover:border-[#008572] shadow-sm md:hover:shadow-md;
      }

      .activeFilter {
        @apply bg-[#008572] text-white;
      }
      .defaultFilter {
        @apply bg-primaryLight text-white;
      }
      .listTextRed {
        @apply text-[#E90C54];
      }

      .listTextGreen {
        @apply text-[#008572];
      }

      .event-expired {
        filter: grayscale(1) contrast(0.4) brightness(1.3);
      }
    `,
  ],
})
export class EventListComponent {
  eventCount$ = this.store.select(selectEventCount);
  holidayFilters$ = this.store.select(selectHolidayFiltersWithSelection);
  holidayFilterEmpty$ = this.store.select(selectHolidayFilterEmpty);
  holidays$ = this.store
    .select(selectHolidaysWithEvents)
    .pipe(debounceTime(200));

  seasons$ = this.store.select(selectSeasons);
  categoryFilters$ = this.store.select(selectCategoryFiltersWithSelection);
  categoryFilterEmpty$ = this.store.select(selectCategoryFilterEmpty);
  selectedSeason$ = this.store.select(selectSelectedSeason);
  searchText$ = this.store.select(selectSearchText);
  bothFreeSlotsFiltersZero$ = this.store.select(selectBothFreeSlotsFiltersZero);
  showFilterMobile$ = this.store.select(selectShowFilterMobile);
  activeFilter$ = this.store.select(selectActiveFilter);
  countFilters$ = this.store.select(selectFiltersCount);

  freeSlotsFilterParticipants$ = this.store
    .select(selectFreeSlotsFilter('participant'))
    .pipe(
      map((filter) =>
        this.freeSlotsFilter.map((freeSlots) => ({
          amount: freeSlots,
          selected: freeSlots === filter,
        }))
      )
    );

  freeSlotsFilterWaitinglist$ = this.store
    .select(selectFreeSlotsFilter('waitinglist'))
    .pipe(
      map((filter) =>
        this.freeSlotsFilter.map((freeSlots) => ({
          amount: freeSlots,
          selected: freeSlots === filter,
        }))
      )
    );

  freeSlotsFilter = Array.from({ length: 8 }, (value, index) => index);

  constructor(private store: Store) {}

  toggleShowFilter() {
    this.store.dispatch(EventsActions.toggleShowFilter());
  }

  selectSeason(year: string) {
    this.store.dispatch(EventsActions.selectSeason({ year: parseInt(year) }));
  }

  changeSearchText(text: string) {
    this.store.dispatch(EventsActions.changeSearchText({ searchText: text }));
  }

  toggleHolidayFilter(holidayId: string) {
    this.store.dispatch(EventsActions.toggleHolidayFilter({ holidayId }));
  }

  resetHolidayFilter() {
    this.store.dispatch(EventsActions.resetHolidayFilter());
  }

  toggleCategoryFilter(categoryId: string) {
    this.store.dispatch(EventsActions.toggleCategoryFilter({ categoryId }));
  }

  resetCategoryFilter() {
    this.store.dispatch(EventsActions.resetCategoryFilter());
  }

  selectFreeSlotsFilter(
    amount: number,
    slotType: 'participant' | 'waitinglist'
  ) {
    this.store.dispatch(
      EventsActions.selectFreeSlotsFilter({ amount, slotType })
    );
  }

  identifyById(index: number, entity: { id: string }) {
    return entity.id;
  }

  identifyByAmount(index: number, entity: { amount: number }) {
    return entity.amount;
  }
}
