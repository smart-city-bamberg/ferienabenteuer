import { Component } from '@angular/core';

@Component({
  selector: 'app-security-delete-account-success',
  template: `
    <h2 class="site-title">Sicherheit</h2>
    <div class="white-card mb-6 mt-4 flex flex-col">
      <h3 class="text-lg font-extrabold">Konto wurde gelöscht</h3>
      <div class="divider mb-4 mt-2"></div>
      <p>
        Mit der Kontolöschung wurden sämtliche personenbezogenen Daten aus dem
        System gelöscht. Sie haben keinen weiteren Zugriff auf Ihre Buchungen
        und können keine weitere Buchungen tätigen.
      </p>
    </div>
  `,
  styles: [
    `
      :host {
        @apply flex flex-col flex-grow md:px-60;
      }

      .label {
        @apply mb-1 font-bold text-sm;
      }
    `,
  ],
})
export class DeleteAccountSuccessComponent {}
