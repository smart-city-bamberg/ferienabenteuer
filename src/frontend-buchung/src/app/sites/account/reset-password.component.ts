import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { PasswordResetActions } from 'src/app/stores/password-reset/password-reset.actions';
import { selectRequestPasswordResetTokenForm } from 'src/app/stores/password-reset/password-reset.selectors';

@Component({
  selector: 'app-reset-password',
  template: `
    <h2 class="site-title">Passwort vergessen</h2>

    <div class="white-card flex flex-col">
      <div>
        <div class="flex flex-col leading-tight">
          <span>
            Wenn Sie Ihr Passwort vergessen haben, können Sie hier ein neues
            anfordern.</span
          ><span class="mt-2"
            >Sie erhalten in wenigen Minuten eine E-Mail mit einem Link bzw.
            Code zum Zurücksetzen Ihres Passworts, welcher
            <b>24 Stunden </b> gültig ist.</span
          >
          <span>
            Falls Sie keine E-Mail erhalten, fordern Sie bitten den Code zum
            Zurücksetzen erneut an. Bitte überprüfen Sie auch Ihren
            <b>Spam-Ordner</b>.
          </span>
        </div>

        <form
          class="flex flex-col"
          [ngrxFormState]="formState"
          *ngIf="requestPasswordResetTokenForm$ | async as formState"
        >
          <div class="mt-6 flex flex-col">
            <p class="label red">E-Mail</p>
            <input
              aria-label="Eingabefeld E-Mail Adresse"
              class="!mt-0 flex flex-grow"
              type="email"
              [ngrxFormControlState]="formState.controls.email"
            />
          </div>

          <button class="mt-8 flex p-2" (click)="requestPasswordResetToken()">
            Passwort zurücksetzen
          </button>
        </form>
      </div>
    </div>
    <div
      class="mt-1 flex flex-wrap items-center justify-center gap-1 leading-tight"
    >
      <i class="mi notranslate text-primaryLight">info</i>
      Sie haben bereits einen Code?
      <a
        [routerLink]="['/neues-passwort']"
        class="link -mt-2 md:mt-0"
        aria-label="Passwort neu vergeben Seite"
        >Hier eingeben und Passwort zurücksetzen</a
      >
    </div>
  `,
  styles: [
    `
      :host {
        @apply flex flex-col flex-grow;
      }
      .label {
        @apply font-bold text-sm;
        &.red {
          @apply after:content-['*'] after:ml-0.5 after:text-red-500;
        }
      }
    `,
  ],
})
export class ResetPasswordComponent {
  requestPasswordResetTokenForm$ = this.store.select(
    selectRequestPasswordResetTokenForm
  );

  constructor(private store: Store) {}

  requestPasswordResetToken() {
    this.store.dispatch(PasswordResetActions.requestPasswordResetToken());
  }
}
