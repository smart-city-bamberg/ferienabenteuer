import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { PasswordResetActions } from 'src/app/stores/password-reset/password-reset.actions';
import { selectResetPasswordForm } from 'src/app/stores/password-reset/password-reset.selectors';

@Component({
  selector: 'app-new-password',
  template: `
    <h2 class="site-title">Neues Passwort vergeben</h2>

    <div class="white-card flex flex-col">
      <div>
        <span class="flex leading-tight">
          Bitte fügen Sie den Code aus der E-Mail in das Feld ein und wählen Sie
          ein neues Passwort.
        </span>
        <div class="mt-4">
          <form
            [ngrxFormState]="formState"
            *ngIf="formState$ | async as formState"
          >
            <div class="flex flex-grow flex-col">
              <div class="flex flex-row">
                <p class="label red">Code</p>
              </div>
              <div class="flex flex-row items-center gap-2">
                <input
                  aria-label="Eingabelfeld Code"
                  class="flex flex-grow"
                  [type]="showToken ? 'text' : 'password'"
                  [ngrxFormControlState]="formState.controls.token"
                />
                <button
                  aria-label="Code in Klartext anzeigen"
                  class="h-10 w-10"
                  (click)="toggleShowToken()"
                >
                  <i class="mi notranslate">{{
                    showToken ? 'visibility_off' : 'visibility'
                  }}</i>
                </button>
              </div>
            </div>
            <div class="mt-6 flex flex-grow flex-col">
              <p class="label red">Neues Passwort</p>
              <p class="text-xs font-semibold leading-none text-[#474747]">
                Mindestens 12 Zeichen (mind. 1 Kleinbuchstabe, mind. 1
                Großbuchstabe und mind. 1 Zahl)
              </p>
              <a
                class="link text-sm leading-none"
                href="https://www.bsi.bund.de/DE/Themen/Verbraucherinnen-und-Verbraucher/Informationen-und-Empfehlungen/Cyber-Sicherheitsempfehlungen/Accountschutz/Sichere-Passwoerter-erstellen/sichere-passwoerter-erstellen_node.html"
                target="_blank"
                >Was ist ein sicheres Passwort?</a
              >
              <div class="flex flex-row items-center gap-2">
                <input
                  aria-label="Eingabelfeld neues Passwort"
                  class="flex-grow"
                  [type]="showNewPassword ? 'text' : 'password'"
                  [ngrxFormControlState]="formState.controls.newPassword"
                />
                <button
                  aria-label="Neues Passwort in Klartext anzeigen"
                  class="h-10 w-10"
                  (click)="toggleShowNewPassword()"
                >
                  <i class="mi notranslate">{{
                    showNewPassword ? 'visibility_off' : 'visibility'
                  }}</i>
                </button>
              </div>
            </div>
            <div class="mt-4 flex flex-grow flex-col">
              <p class="label red">Neues Passwort bestätigen</p>
              <div class="flex flex-row items-center gap-2">
                <input
                  aria-label="Eingabelfeld neues Passwort bestätigen"
                  class="flex-grow"
                  [type]="showNewPasswordRepeat ? 'text' : 'password'"
                  [ngrxFormControlState]="formState.controls.newPasswordRepeat"
                />
                <button
                  aria-label="Neues Passwort in Klartext anzeigen"
                  class="h-10 w-10"
                  (click)="toggleShowNewPasswordRepeat()"
                >
                  <i class="mi notranslate">{{
                    showNewPasswordRepeat ? 'visibility_off' : 'visibility'
                  }}</i>
                </button>
              </div>
            </div>
            <div class="mt-8 flex">
              <button class="flex flex-grow p-2" (click)="changePassword()">
                <span class="text-lg">Passwort ändern</span>
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>

    <div class="mt-1 flex flex-wrap items-center justify-center gap-1">
      <i class="mi notranslate text-primaryLight">info</i>
      E-Mail nicht erhalten?
      <a
        [routerLink]="['/passwort-zuruecksetzen']"
        class="link -mt-3 md:mt-0"
        aria-label="E-Mail mit Coder erneut anfordern"
        >Hier erneut anfordern.</a
      >
    </div>
  `,
  styles: [
    `
      :host {
        @apply flex flex-col flex-grow;
      }
      .label {
        @apply mb-1 font-bold text-sm;
        &.red {
          @apply after:content-['*'] after:ml-0.5 after:text-red-500;
        }
      }
    `,
  ],
})
export class NewPasswordComponent {
  formState$ = this.store.select(selectResetPasswordForm);
  showToken = false;
  showNewPassword = false;
  showNewPasswordRepeat = false;

  constructor(private store: Store) {}

  toggleShowToken() {
    this.showToken = !this.showToken;
  }

  toggleShowNewPassword() {
    this.showNewPassword = !this.showNewPassword;
  }

  toggleShowNewPasswordRepeat() {
    this.showNewPasswordRepeat = !this.showNewPasswordRepeat;
  }

  changePassword() {
    this.store.dispatch(PasswordResetActions.resetPassword());
  }
}
