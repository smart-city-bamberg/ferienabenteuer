import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { ChildrenActions } from 'src/app/stores/children/children.actions';
import * as childrenSelectors from 'src/app/stores/children/children.selectors';

@Component({
  selector: 'app-children',
  template: `
    <div class="flex flex-col md:flex-row md:justify-between">
      <div class="flex justify-center">
        <h2 class="site-title">Meine Kinder</h2>
      </div>
      <div class="flex md:block">
        <button
          aria-label="Kind hinzufügen"
          class="flex flex-grow justify-center gap-2 p-1 text-xl md:w-48 md:flex-grow-0"
          routerLink="/kind-eintragen"
        >
          <i class="mi notranslate">add</i>
          Hinzufügen
        </button>
      </div>
    </div>

    <div
      *ngIf="noChildren$ | async"
      class="flex flex-grow items-center justify-center"
    >
      <p class="text-center text-lg font-semibold text-[#676E75] md:text-xl">
        Es sind noch keine Kinder hinzugefügt!
      </p>
    </div>

    <div class="flex flex-col">
      <div
        class="white-card my-4 flex flex-col gap-2 rounded-md border-[1px] border-[#24524B] px-8 py-4 md:flex-row md:justify-between md:px-4"
        *ngFor="let child of children$ | async"
      >
        <div
          class="flex flex-col gap-2 md:flex-grow md:flex-row md:justify-between md:gap-[7%]"
        >
          <div
            class="flex flex-row gap-10 md:flex-grow md:justify-between md:gap-4"
          >
            <div class="flex flex-grow flex-col gap-2 md:flex-row">
              <div class="flex flex-col md:w-1/2">
                <p class="data-label">Vorname</p>
                <h3 class="data notranslate">{{ child.firstname }}</h3>
              </div>
              <div class="flex flex-col md:w-1/2">
                <p class="data-label">Nachname</p>
                <h3 class="data notranslate">{{ child.surname }}</h3>
              </div>
            </div>
            <div
              class="flex flex-grow flex-col gap-2 md:w-[35%] md:flex-grow-0 md:flex-row md:justify-between"
            >
              <div class="flex flex-col">
                <p class="data-label">Geschlecht</p>
                <h3 class="data">{{ genderLabel(child.gender) }}</h3>
              </div>
              <div class="flex flex-col">
                <p class="data-label">Geburtsdatum</p>
                <h3 class="data notranslate">
                  {{ child.dateOfBirth | date : 'dd.MM.yyyy' }}
                </h3>
              </div>
            </div>
          </div>
          <div
            class="mt-4 flex flex-row items-center justify-between gap-4 md:mt-0 md:justify-end"
          >
            <button
              aria-label="Kind Daten bearbeiten"
              class="flex w-1/2 flex-row items-center justify-center gap-2 px-2 py-[0.3rem] md:h-9 md:w-9 md:p-2"
              [routerLink]="['/kinder', child.id]"
            >
              <i class="mi notranslate">edit</i>
              <span class="md:hidden">Bearbeiten</span>
            </button>
            <button
              aria-label="Kind entfernen"
              class="danger flex w-1/2 flex-row items-center justify-center gap-2 px-2 py-[0.3rem] md:h-9 md:w-9 md:p-2"
              (click)="deleteChild(child.id)"
            >
              <i class="mi notranslate">close</i>
              <span class="md:hidden">Entfernen</span>
            </button>
          </div>
        </div>
      </div>
    </div>
  `,
  styles: [
    `
      :host {
        @apply flex flex-col flex-grow;
      }

      .data {
        @apply font-bold text-lg leading-tight;
      }
      .data-label {
        @apply opacity-60 text-sm;
      }
    `,
  ],
})
export class ChildrenComponent {
  children$ = this.store.select(childrenSelectors.selectChildren);
  noChildren$ = this.store.select(childrenSelectors.selectNoChildren);

  genderLabels: { [index: string]: string } = {
    male: 'männlich',
    female: 'weiblich',
    diverse: 'divers',
  };

  constructor(private store: Store) {}

  genderLabel(gender: string) {
    return this.genderLabels[gender] ?? 'unbekannt';
  }

  deleteChild(childId: string) {
    this.store.dispatch(ChildrenActions.deleteChild({ childId }));
  }
}
