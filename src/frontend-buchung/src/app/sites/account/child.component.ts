import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Store, select } from '@ngrx/store';
import { ChildrenActions } from 'src/app/stores/children/children.actions';
import {
  selectChildForm,
  selectChildFormExists,
} from 'src/app/stores/children/children.selectors';

@Component({
  selector: 'app-add-kids',
  template: `
    <h2 class="site-title">Kind hinzufügen</h2>
    <form [ngrxFormState]="formState" *ngIf="formState$ | async as formState">
      <div class="white-card mb-6 mt-4 flex flex-col">
        <h3 class="text-lg font-extrabold">Allgemeine Informationen</h3>
        <div class="divider mb-2"></div>
        <div class="flex flex-col gap-1 md:flex-row md:gap-8">
          <div class="flex flex-grow flex-col md:w-1/2 md:flex-grow-0">
            <p class="label">Vorname<span class="text-red-600">*</span></p>

            <input
              type="text"
              aria-label="Eingabefeld Vorname"
              placeholder="Max"
              [ngrxFormControlState]="formState.controls.firstname"
            />
          </div>
          <div class="flex flex-grow flex-col md:w-1/2 md:flex-grow-0">
            <p class="label">Nachname<span class="text-red-600">*</span></p>
            <input
              type="text"
              aria-label="Eingabefeld Nachname"
              placeholder="Mustermann"
              [ngrxFormControlState]="formState.controls.surname"
            />
          </div>
        </div>
        <div class="mt-2 flex flex-col gap-1 md:mt-4 md:flex-row md:gap-8">
          <div class="flex flex-grow flex-col md:w-1/2 md:flex-grow-0">
            <p class="label">Geburtsdatum<span class="text-red-600">*</span></p>
            <input
              type="date"
              aria-label="Eingabefeld Geburtsdatum"
              [ngrxFormControlState]="formState.controls.dateOfBirth"
            />
            <div class="h-3">
              <p
                *ngIf="
                  formState.controls.dateOfBirth.isInvalid &&
                  formState.controls.dateOfBirth.isSubmitted
                "
                class="flex flex-grow flex-col text-sm leading-none text-red-600"
              >
                Das Geburtsdatum ist ungültig
              </p>
            </div>
          </div>

          <div class="flex flex-grow flex-col md:w-1/2 md:flex-grow-0">
            <p class="label">Geschlecht<span class="text-red-600">*</span></p>
            <fieldset class="flex flex-grow flex-row items-center gap-12">
              <legend class="hidden">Geschlecht</legend>
              <div class="flex flex-row gap-2">
                <input
                  style="flex-grow: 0;"
                  type="radio"
                  id="male"
                  name="gender"
                  value="male"
                  [ngrxFormControlState]="formState.controls.gender"
                />
                <label for="male">männlich</label>
              </div>
              <div tabindex="0" class="flex flex-row gap-2">
                <input
                  style="flex-grow: 0;"
                  type="radio"
                  id="female"
                  name="gender"
                  value="female"
                  [ngrxFormControlState]="formState.controls.gender"
                />
                <label for="female">weiblich</label>
              </div>
              <div tabindex="0" class="flex flex-row gap-2">
                <input
                  style="flex-grow: 0;"
                  type="radio"
                  id="diverse"
                  name="gender"
                  value="diverse"
                  [ngrxFormControlState]="formState.controls.gender"
                />
                <label for="diverse">divers</label>
              </div>
            </fieldset>
          </div>
        </div>

        <h3 class="mt-6 text-lg font-extrabold">Versicherung</h3>
        <div class="divider mb-2"></div>
        <div class="flex flex-col gap-2 md:flex-row md:gap-8">
          <div class="flex flex-grow flex-col md:w-1/2 md:flex-grow-0">
            <p class="label">
              Krankenversicherung<span class="text-red-600">*</span>
            </p>
            <input
              type="text"
              aria-label="Eingabefeld Krankenkasse"
              placeholder="Name der Krankenkasse"
              [ngrxFormControlState]="formState.controls.healthInsurer"
            />
          </div>
          <div class="flex flex-grow flex-col md:w-1/2 md:flex-grow-0">
            <p class="label">
              Liegt eine Haftpflichtversicherung vor?<span class="text-red-600"
                >*</span
              >
            </p>
            <fieldset class="flex flex-grow flex-row items-center gap-12">
              <legend class="hidden">Haftpflichversicherung</legend>
              <div class="flex flex-row gap-2">
                <input
                  style="flex-grow: 0;"
                  type="radio"
                  id="yes"
                  name="insurance"
                  [value]="true"
                  [ngrxFormControlState]="
                    formState.controls.hasLiabilityInsurance
                  "
                />
                <label for="yes">Ja</label>
              </div>
              <div tabindex="0" class="flex flex-row gap-2">
                <input
                  style="flex-grow: 0;"
                  type="radio"
                  id="no"
                  name="insurance"
                  [value]="false"
                  [ngrxFormControlState]="
                    formState.controls.hasLiabilityInsurance
                  "
                />
                <label for="no">Nein</label>
              </div>
            </fieldset>
          </div>
        </div>

        <h3 class="mt-6 text-lg font-extrabold">Unterstützung</h3>
        <div class="divider mb-2"></div>
        <div class="flex flex-col gap-4">
          <div class="flex flex-grow flex-col">
            <p class="label">
              Benötigt Ihr Kind eine Assistenzkraft?<span class="text-red-600"
                >*</span
              >
            </p>
            <fieldset class="flex flex-grow flex-row items-center gap-12">
              <legend class="hidden">Assistenskraft benötigt</legend>
              <div class="flex flex-row gap-2">
                <input
                  style="flex-grow: 0;"
                  type="radio"
                  name="care"
                  id="yes-care"
                  [value]="true"
                  [ngrxFormControlState]="formState.controls.assistanceRequired"
                />
                <label for="yes-care">Ja</label>
              </div>
              <div tabindex="0" class="flex flex-row gap-2">
                <input
                  style="flex-grow: 0;"
                  type="radio"
                  id="no-care"
                  name="care"
                  [value]="false"
                  [ngrxFormControlState]="formState.controls.assistanceRequired"
                />
                <label for="no-care">Nein</label>
              </div>
            </fieldset>
          </div>
          <span class="text-sm leading-tight opacity-60"
            >Wenn Sie für Ihr Kind eine Assistenzkraft während des
            Ferienabenteuers benötigen, wenden Sie sich bitte an die Offene
            Behindertenarbeit der Lebenshilfe Bamberg (Frau Melanie Bernt, 0951
            / 1897 2104). Pro Kind kann eine Assistenz für 10 Angebotstage
            kostenfrei angeboten werden.
          </span>
          <div class="textbox-label">
            <p class="label">Körperliche Beeinträchtigung</p>
            <textarea
              class="textbox"
              aria-label="Eingabefeld"
              name=""
              id=""
              placeholder="Angaben über körperliche Beeinträchtigungen"
              [ngrxFormControlState]="formState.controls.physicalImpairment"
            ></textarea>
          </div>
        </div>
        <h3 class="mt-6 text-lg font-extrabold">Weitere Angaben</h3>
        <div class="divider mb-2"></div>
        <div class="flex flex-col gap-2">
          <div class="textbox-label">
            <p class="label">Medikamente</p>
            <textarea
              class="textbox"
              aria-label="Eingabefeld"
              name=""
              id=""
              placeholder="Informationen über aktuelle Medikation Ihres Kindes"
              [ngrxFormControlState]="formState.controls.medication"
            ></textarea>
          </div>
          <div class="textbox-label">
            <p class="label">Allergien/Lebensmittelunverträglichkeiten</p>
            <textarea
              class="textbox"
              aria-label="Eingabefeld"
              name=""
              id=""
              placeholder="Angaben über Allergien und Lebensmittelunverträglichkeiten"
              [ngrxFormControlState]="formState.controls.foodIntolerances"
            ></textarea>
          </div>
          <div class="textbox-label">
            <p class="label">Ernährungsvorschriften</p>
            <textarea
              class="textbox"
              aria-label="Eingabefeld"
              name=""
              id=""
              placeholder="Ernährungs- und Lebensweise Ihres Kindes: z. B. vegetarisch, kein Schweinefleisch"
              [ngrxFormControlState]="formState.controls.dietaryRegulations"
            ></textarea>
          </div>
          <div class="textbox-label">
            <p class="label">Sonstiges</p>
            <textarea
              class="textbox"
              aria-label="Eingabefeld"
              name=""
              id=""
              placeholder="Teilen Sie uns andere wichtige Informationen mit"
              [ngrxFormControlState]="formState.controls.miscellaneous"
            ></textarea>
          </div>
        </div>

        <div class="mt-10 flex md:justify-end">
          <button
            aria-label="Speichern"
            class="flex flex-grow justify-center p-2"
            (click)="save()"
          >
            {{ (childExists$ | async) ? 'Speichern' : 'Hinzufügen' }}
          </button>
        </div>
      </div>
    </form>
  `,
  styles: [
    `
      :host {
        @apply flex flex-col flex-grow;
      }

      .label {
        @apply font-bold text-sm;
      }
      label {
        @apply text-base text-textDark;
      }

      .textbox {
        @apply flex flex-grow border border-gray-300 px-2 py-2 h-20 rounded-lg text-sm text-textDark focus:outline-none focus:border-blue-500;
      }
      .textbox-label {
        @apply flex flex-grow flex-col;
      }
    `,
  ],
})
export class ChildComponent {
  formState$ = this.store.select(selectChildForm);
  childExists$ = this.store.select(selectChildFormExists);

  constructor(private store: Store) {}

  save() {
    this.store.dispatch(ChildrenActions.saveChild());
  }
}
