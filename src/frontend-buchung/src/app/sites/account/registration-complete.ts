import { Component } from '@angular/core';

@Component({
  selector: 'app-registration-complete',
  template: `
    <p class="site-title">Erfolgreich registriert</p>

    <div
      class="white-card flex flex-col items-center justify-center leading-tight"
    >
      <div>
        Ihre Registrierung war erfolgreich. Bitte aktivieren Sie nun ihr Konto
        über den Link, den wir Ihnen per E-Mail zugesandt haben.<br />
        Bitte überprüfen Sie auch Ihren Spam-Ordner.<br />
      </div>
    </div>
    <div
      class="mt-1 flex flex-wrap items-center justify-center gap-1 leading-tight"
    >
      <i class="mi notranslate text-primaryLight">info</i>
      E-Mail für Aktivierung nicht erhalten?
      <a
        [routerLink]="['/aktivieren']"
        class="link -mt-2 md:mt-0"
        aria-label="E-Mail mit Coder erneut anfordern"
        >Hier erneut anfordern!</a
      >
    </div>
  `,
  styles: [
    `
      :host {
        @apply flex flex-col flex-grow;
      }
    `,
  ],
})
export class RegistrationCompleteComponent {}
