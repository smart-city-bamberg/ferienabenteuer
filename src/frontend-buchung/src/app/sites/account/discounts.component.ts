import { Component } from '@angular/core';

@Component({
  selector: 'app-account-discounts',
  template: `
    <div class="flex flex-col">
      <h2 class="site-title">Ermäßigungen</h2>
      <span class="ml-2 mt-4 text-sm leading-tight">
        Familien können Ermäßigungen des Ferienabenteuers beantragen. Bevor Sie
        mit dem Antragsprozess beginnen, stellen Sie sicher, dass Sie Ihr Kind
        unter Menüpunkt
        <a
          class="link"
          routerLink="/kinder"
          role="link"
          aria-label="Link zu Meine Kinder"
          >"Meine Kinder"</a
        >
        registriert haben. Sobald Ihr Kind dort angelegt ist, können Sie hier
        einen Antrag auf Ermäßigung stellen. Ein entsprechender Nachweis ist für
        die Gewährung vorzulegen. Einzelheiten sind bei der Beantragung
        beschrieben und in den
        <a
          class="link"
          routerLink="/agb"
          role="link"
          aria-label="Link zu den AGB´s"
          >AGB</a
        >
        geregelt.
      </span>
    </div>
    <app-discounts />
  `,
  styles: [
    `
      :host {
        @apply flex flex-col flex-grow;
      }

      .data-label {
        @apply font-bold;
      }
      .data {
        @apply mt-1 text-sm;
      }
    `,
  ],
})
export class DiscountsComponent {}
