import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { CustodianActions } from 'src/app/stores/custodian/custodian.actions';
import {
  selectCustodianForm,
  selectSeasons,
  selectSelectedSeason,
  selectSelectedEarlyAccessCodeVoucherCode,
  selectCodeIsReadOnly,
  selectCustodianId,
} from 'src/app/stores/custodian/custodian.selectors';

@Component({
  selector: 'app-personal-data',
  template: `
    <h2 class="site-title">Persönliche Daten</h2>
    <form [ngrxFormState]="formState" *ngIf="formState$ | async as formState">
      <div class="white-card flex flex-col">
        <h3 class="text-lg font-extrabold">Allgemeine Informationen</h3>
        <div class="divider mb-4 mt-2"></div>
        <div class="flex flex-col md:w-1/4">
          <p class="label">Anrede</p>
          <select
            id="anrede"
            aria-label="Auswahl Anrede"
            class="px-2 py-2"
            [ngrxFormControlState]="formState.controls.salutation"
          >
            <option value="mr" selected>Herr</option>
            <option value="ms">Frau</option>
          </select>
        </div>
        <div class="mt-4 flex flex-col gap-4 md:flex-row">
          <div class="flex flex-grow flex-col md:w-1/2 md:flex-grow-0">
            <p class="label">Vorname</p>
            <input
              type="text"
              aria-label="Eingabefeld Vorname"
              placeholder="Max"
              [ngrxFormControlState]="formState.controls.firstname"
            />
          </div>
          <div class="flex flex-grow flex-col md:w-1/2 md:flex-grow-0">
            <p class="label">Nachname</p>
            <input
              type="text"
              aria-label="Eingabefeld Nachname"
              placeholder="Mustermann"
              [ngrxFormControlState]="formState.controls.surname"
            />
          </div>
        </div>

        <h3 class="mt-6 text-lg font-extrabold">Adresse</h3>
        <div class="divider mb-4 mt-2"></div>
        <div class="flex flex-col gap-4 md:flex-row">
          <div class="flex flex-grow flex-col md:w-1/2 md:flex-grow-0">
            <p class="label">Straße</p>
            <input
              type="text"
              aria-label="Eingabefeld Straße"
              placeholder="Musterstraße"
              [ngrxFormControlState]="formState.controls.addressStreet"
            />
          </div>
          <div class="flex flex-grow flex-col md:w-1/2 md:flex-grow-0">
            <p class="label">Hausnummer</p>
            <input
              type="text"
              aria-label="Eingabefeld Hausnummer"
              placeholder="12"
              [ngrxFormControlState]="formState.controls.addressHouseNumber"
            />
          </div>
        </div>
        <div class="mt-4 flex flex-col gap-4 md:flex-row">
          <div class="flex flex-grow flex-col">
            <p class="label">Zusatzzeile</p>
            <input
              type="text"
              aria-label="Eingabefeld"
              placeholder="Optional"
              [ngrxFormControlState]="formState.controls.addressAdditional"
            />
          </div>
        </div>
        <div class="mt-4 flex flex-col gap-4 md:flex-row">
          <div class="flex flex-grow flex-col md:w-1/2 md:flex-grow-0">
            <p class="label">Postleitzahl</p>
            <input
              type="text"
              aria-label="Eingabefeld Postleitzahl"
              placeholder="12345"
              [ngrxFormControlState]="formState.controls.addressZip"
            />
          </div>
          <div class="flex flex-grow flex-col md:w-1/2 md:flex-grow-0">
            <p class="label">Stadt</p>
            <input
              type="text"
              aria-label="Eingabefeld Stadt"
              placeholder="Musterstadt"
              [ngrxFormControlState]="formState.controls.addressCity"
            />
          </div>
        </div>
        <h3 class="mt-6 text-lg font-extrabold">Kontaktdaten</h3>
        <div class="divider mb-4 mt-2"></div>
        <div class="flex flex-col gap-4 md:flex-row">
          <div class="flex flex-grow flex-col md:w-1/2 md:flex-grow-0">
            <p class="label">Telefon</p>
            <input
              type="text"
              aria-label="Eingabefeld Telefonummer"
              placeholder="+49 123 12345678"
              [ngrxFormControlState]="formState.controls.phone"
            />
          </div>
          <div class="flex flex-grow flex-col md:w-1/2 md:flex-grow-0">
            <p class="label">Notfallnummer</p>
            <input
              type="text"
              aria-label="Eingabefeld Notfallnummer"
              placeholder="+49 123 12345678"
              [ngrxFormControlState]="formState.controls.emergencyPhone"
            />
            <span class="text-xs font-semibold leading-none text-[#757575]"
              >Bevorzugte Handynummer</span
            >
          </div>
        </div>
        <div class="mt-4 flex md:justify-end">
          <button
            (click)="saveForm()"
            class="flex flex-grow justify-center p-2 md:w-56 md:flex-grow-0"
          >
            Speichern
          </button>
        </div>
      </div>
    </form>

    <div class="white-card my-6 flex flex-col">
      <h3 class="text-lg font-extrabold">Frühbucher-Code</h3>
      <div class="divider mb-4 mt-2"></div>
      <p class="mb-4 leading-tight">
        Geben Sie hier Ihren Frühbucher-Code ein, um Zugang zum Buchungssystem
        vor dem regulären Start zu erhalten. Ein Frühbucher-Code ist nur für
        eine Saison gültig.
      </p>
      <div class="flex flex-col md:flex-row md:justify-between">
        <div class="flex flex-col gap-4 md:w-1/2 md:flex-row">
          <div>
            <select
              id="saisons"
              aria-label="Auswahl Saison"
              class="rounded-lg border-2 border-gray-300 bg-white px-2 py-[0.4rem] text-base font-medium text-textDark"
              [ngModel]="selectedSeason$ | async"
              #seasonSelector
              (change)="selectSeason(seasonSelector.value)"
            >
              <option
                *ngFor="let season of seasons$ | async"
                [value]="season.year"
              >
                Saison {{ season.year }}
              </option>
            </select>
          </div>
          <div class="flex flex-grow flex-col md:w-1/3">
            <input
              type="text"
              aria-label="Eingabefeld Frühbucher-Code"
              placeholder="XYZ#123ABC"
              [value]="earlyAccessCode$ | async"
              #codeInput
              (keyup)="updateCode(codeInput.value)"
              [disabled]="codeIsReadOnly$ | async"
            />
          </div>
        </div>
        <div class="mt-4 flex md:mt-0 md:block">
          <button
            [disabled]="codeIsReadOnly$ | async"
            (click)="redeemCode()"
            class="flex flex-grow justify-center p-2 md:w-56 md:flex-grow-0"
          >
            Einlösen
          </button>
        </div>
      </div>
    </div>

    <div class="white-card mb-6 flex flex-col">
      <h3 class="text-lg font-extrabold">
        Auskunft nach Art. 15 Datenschutz-Grundverordnung (DSGVO)
      </h3>
      <div class="divider mb-4 mt-2"></div>
      <p class="mb-4 leading-tight">
        Hier erhalten Sie eine Kopie Ihrer bei Ferienabenteuer-Bamberg
        gespeicherten personenbezogenen Daten
      </p>
      <div
        class="flex flex-col md:flex-row md:justify-end"
        *ngIf="custodianId$ | async as custodianId"
      >
        <div class="mt-4 flex md:mt-0 md:block">
          <button
            (click)="generateDsgvoInformation(custodianId)"
            class="flex flex-grow justify-center p-2 md:w-56 md:flex-grow-0"
          >
            Download
          </button>
        </div>
      </div>
    </div>
  `,
  styles: [
    `
      :host {
        @apply flex flex-col flex-grow md:px-60;
      }
      .label {
        @apply mb-1 font-bold text-sm;
      }
      input {
        @apply flex flex-grow border border-gray-300 px-4 py-2 max-h-10  rounded-lg text-textDark focus:outline-none focus:border-blue-500;
      }
    `,
  ],
})
export class PersonalDataComponent {
  seasons$ = this.store.select(selectSeasons);
  selectedSeason$ = this.store.select(selectSelectedSeason);
  formState$ = this.store.select(selectCustodianForm);
  custodianId$ = this.store.select(selectCustodianId);

  earlyAccessCode$ = this.store.select(
    selectSelectedEarlyAccessCodeVoucherCode
  );

  codeIsReadOnly$ = this.store.select(selectCodeIsReadOnly);

  constructor(private store: Store) {}

  selectSeason(year: string) {
    this.store.dispatch(
      CustodianActions.selectEarlyAccessCodeYear({ year: parseInt(year) })
    );
  }

  updateCode(code: string) {
    this.store.dispatch(CustodianActions.updateEarlyAccessCode({ code }));
  }

  saveForm() {
    this.store.dispatch(CustodianActions.updateCustodian());
  }

  redeemCode() {
    this.store.dispatch(CustodianActions.redeemEarlyAccessCode());
  }

  generateDsgvoInformation(custodianId: string) {
    this.store.dispatch(
      CustodianActions.generateGdprInformation({
        custodianId,
      })
    );
  }
}
