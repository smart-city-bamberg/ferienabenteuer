import { ChangePassword } from './../../stores/security/security.reducer';
import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { map } from 'rxjs';
import { selectUser } from 'src/app/stores/auth/auth.selectors';
import { SecurityActions } from 'src/app/stores/security/security.actions';
import {
  selectChangePasswordForm,
  selectShowDeleteAccountDialog,
} from 'src/app/stores/security/security.selectors';

@Component({
  selector: 'app-security',
  template: `
    <h2 class="site-title">Sicherheit</h2>
    <div
      class="white-card mb-6 mt-4 flex flex-col"
      *ngIf="formState$ | async; let formState"
    >
      <form [ngrxFormState]="formState">
        <div class="flex flex-col">
          <h3 class="text-lg font-extrabold">Passwort ändern</h3>
          <div class="divider mb-2"></div>
          <div class="flex flex-grow flex-col md:flex-grow-0">
            <p class="label mb-1">E-Mail</p>
            <div
              class="flex flex-row items-center rounded-md border-[1px] border-[#d1d5db] bg-gray-200"
            >
              <input
                type="text"
                aria-label="Eingabefeld E-Mail Adresse"
                [disabled]="true"
                class="grow border-none bg-gray-200"
                [value]="userEmail$ | async"
              />
              <i class="mi notranslate mx-2 text-[#666666]">lock</i>
            </div>
          </div>
          <div class="mt-4 flex flex-grow flex-col md:flex-grow-0">
            <p class="label">Aktuelles Passwort</p>
            <input
              type="password"
              aria-label="Eingabefeld altes Passwort"
              [ngrxFormControlState]="formState.controls.currentPassword"
            />
          </div>
          <div class="mt-4 flex flex-grow flex-col md:flex-grow-0">
            <p class="label">Neues Passwort</p>
            <div class="mb-1 flex flex-col text-xs leading-none">
              <p class="font-semibold text-[#474747]">
                Mindestens 12 Zeichen (mind. 1 Kleinbuchstabe, mind. 1
                Großbuchstabe und mind. 1 Zahl)
              </p>
              <a
                class="link"
                href="https://www.bsi.bund.de/DE/Themen/Verbraucherinnen-und-Verbraucher/Informationen-und-Empfehlungen/Cyber-Sicherheitsempfehlungen/Accountschutz/Sichere-Passwoerter-erstellen/sichere-passwoerter-erstellen_node.html"
                target="_blank"
                >Was ist ein sicheres Passwort?</a
              >
            </div>
            <input
              type="password"
              aria-label="Eingabefeld"
              placeholder="Neues Passwort"
              [ngrxFormControlState]="formState.controls.newPassword"
            />
            <div>
              <p
                *ngIf="
                  formState.controls.newPassword.isInvalid &&
                  formState.controls.newPassword.isSubmitted
                "
                class="error-text flex flex-grow flex-col"
              ></p>
            </div>
          </div>

          <div class="mt-4 flex flex-grow flex-col md:flex-grow-0">
            <input
              type="password"
              aria-label="Eingabefeld"
              placeholder="Neues Passwort bestätigen"
              [ngrxFormControlState]="formState.controls.newPasswordRepeat"
            />
            <div>
              <p
                *ngIf="
                  formState.controls.newPasswordRepeat.isInvalid &&
                  formState.controls.newPasswordRepeat.isSubmitted
                "
                class="error-text flex flex-grow flex-col"
              ></p>
            </div>
          </div>
          <div class="mt-8 flex md:justify-end">
            <button
              aria-label="Speichern"
              class="flex flex-grow justify-center p-2 md:w-56 md:flex-grow-0"
              (click)="changePassword()"
            >
              Speichern
            </button>
          </div>
        </div>
      </form>
    </div>

    <div class="white-card mb-6 mt-4 flex flex-col">
      <h3 class="text-lg font-extrabold">Konto löschen</h3>
      <div class="divider mb-2"></div>
      <p class="leading-tight">
        Mit der Kontolöschung werden sämtliche personenbezogenen Daten aus dem
        System gelöscht. Sie verlieren den Zugriff auf Ihre Buchungen und können
        keine weitere Buchungen tätigen.
      </p>
      <div class="mt-4 flex flex-col justify-between gap-2 md:flex-row-reverse">
        <div class="flex md:w-1/2 md:justify-end">
          <button
            (click)="showDeleteAccountDialog()"
            aria-label="Konto löschen"
            class="danger flex h-fit flex-grow justify-center p-2 md:w-56 md:flex-grow-0"
          >
            Konto löschen
          </button>
        </div>
        <div class="flex md:w-1/2">
          <span class="leading-tight text-[#E90C0C]"
            >Diese Aktion kann nicht rückgängig gemacht werden!</span
          >
        </div>
      </div>
      <div
        *ngIf="showDeleteAccountDialog$ | async"
        class="fixed left-0 top-0 z-[999] h-full w-full bg-[rgba(0,0,0,0.5)]"
      >
        <div
          class="fixed left-1/2 top-1/2 z-[1120] -translate-x-1/2 -translate-y-1/2 rounded-md border-[0.1rem] border-primaryLight bg-white p-7 shadow-lg"
        >
          <p class="text-xl font-bold">
            Wollen Sie Ihr Konto wirklich löschen?
          </p>
          <div class="mt-6 flex flex-row justify-between gap-10">
            <button class="w-1/2 bg-[#E90C0C]" (click)="deleteAccount()">
              Ja
            </button>
            <button class="w-1/2" (click)="deleteAccountRejected()">
              Nein
            </button>
          </div>
        </div>
      </div>
    </div>
  `,
  styles: [
    `
      :host {
        @apply flex flex-col flex-grow md:px-60;
      }

      .label {
        @apply font-bold text-sm;
      }
    `,
  ],
})
export class SecurityComponent {
  userEmail$ = this.store.select(selectUser).pipe(map((user) => user?.email));
  formState$ = this.store.select(selectChangePasswordForm);
  showDeleteAccountDialog$ = this.store.select(selectShowDeleteAccountDialog);

  constructor(private store: Store) {}

  changePassword() {
    this.store.dispatch(SecurityActions.changePassword());
  }

  deleteAccount() {
    this.store.dispatch(SecurityActions.deleteAccount());
  }
  showDeleteAccountDialog() {
    this.store.dispatch(SecurityActions.showDeleteAccountDialog());
  }

  deleteAccountRejected() {
    this.store.dispatch(SecurityActions.deleteAccountRejected());
  }
}
