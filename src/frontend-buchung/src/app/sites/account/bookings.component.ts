import { DatePipe } from '@angular/common';
import { Component } from '@angular/core';
import { Store, select } from '@ngrx/store';
import {
  selectEventsWithBookingsAndTickets,
  selectNoBookingEvents,
} from 'src/app/stores/bookings/bookings.selectors';
import { Booking, Reservation } from 'src/app/stores/bookings/bookings.types';

@Component({
  selector: 'app-bookings',
  template: `
    <h2 class="site-title">Buchungen & Reservierungen</h2>
    <div
      *ngIf="noBookingEvents$ | async"
      class="flex flex-grow items-center justify-center"
    >
      <p class="text-center text-lg font-semibold text-[#676E75] md:text-xl">
        Es sind noch keine Buchungen oder Reservierungen vorhanden!
      </p>
    </div>
    <div class="flex flex-col gap-32">
      <div
        *ngFor="let event of eventBookings$ | async"
        class="flex flex-col gap-4"
      >
        <div class="white-card flex flex-col">
          <app-event-details [event]="event" />
        </div>
        <div *ngFor="let bookingOrReservation of event.bookingsAndReservations">
          <div
            *ngIf="bookingOrReservation.reservation; let reservation"
            class="white-card flex flex-col !p-0"
          >
            <!-- Booking header -->
            <div
              class="flex flex-col rounded-t-[11px] bg-[#444850] px-3 py-2 text-white md:flex-row md:items-center md:justify-between"
            >
              <div
                class="flex flex-col text-sm md:text-base xl:flex-row xl:gap-5"
              >
                <div class="relative hidden xl:flex">
                  <button
                    class="peer border-none bg-transparent p-1 text-white"
                  >
                    <i class="mi notranslate outlined">info</i>
                  </button>
                  <span
                    class="absolute left-7 hidden w-[42rem] items-start gap-2 rounded-md border border-gray-500 bg-white p-2 leading-tight text-primaryLight shadow-md peer-hover:flex peer-focus:flex"
                    ><i class="mi notranslate outlined mt-1">info</i>Eine
                    Reservierung entsteht auf Grund von zwei Möglichen
                    Fällen:<br />
                    1. Es wurde ein ermäßigtes Ticket gebucht aber die
                    Ermäßigung wurde noch nicht gewährt. Der Teilnehmerplatz ist
                    in diesem Fall reserviert. Wird die Ermäßigung gewährt
                    bekommen Sie eine gesonderte E-mail mit der
                    Buchungsbestätigung.<br />
                    2. Es wurde ein Warteplatz gebucht. Sollte ein
                    Teilnehmerplatz freiwerden so werden Sie mit einer
                    gesonderten E-Mail darüber informiert.</span
                  >
                </div>

                <div class="flex flex-row justify-between md:gap-2 xl:gap-3">
                  <span class="text-[#f1f6ff]">Reservierung</span>
                  <p class="font-bold">{{ reservation.code }}</p>
                </div>
                <div class="flex flex-row justify-between md:gap-2 xl:gap-3">
                  <span class="text-[#f1f6ff]">Datum</span>
                  <p class="font-bold">
                    {{ reservation.date | date : 'dd.MM.yyyy HH:mm:ss' }}
                  </p>
                </div>
              </div>

              <div
                class="mb-2 mt-4 flex flex-row items-center justify-between text-base md:m-0 md:gap-4"
              >
                <a
                  href="mailto:{{
                    reservation.event.organizer.email
                  }}?subject={{ cancelEmailSubject(reservation) }}&body={{
                    cancelEmailBody(reservation)
                  }}"
                >
                  <span class="font-bold text-[#FF9EA0] md:text-base"
                    >Stornieren</span
                  >
                </a>
                <div class="relative flex xl:hidden">
                  <button
                    class="peer border-none bg-transparent p-1 text-white"
                  >
                    <i class="mi notranslate outlined">info</i>
                  </button>
                  <span
                    class="absolute -top-1 right-7 hidden w-72 items-start gap-2 rounded-md border border-gray-500 bg-white p-2 leading-tight text-primaryLight shadow-md peer-hover:flex peer-focus:flex md:w-[42rem]"
                    ><i class="mi notranslate outlined mt-1">info</i>Eine
                    Reservierung entsteht auf Grund von zwei Möglichen
                    Fällen:<br />
                    1. Es wurde ein ermäßigtes Ticket gebucht aber die
                    Ermäßigung wurde noch nicht gewährt. Der Teilnehmerplatz ist
                    in diesem Fall reserviert. Wird die Ermäßigung gewährt
                    bekommen Sie eine gesonderte E-mail mit der
                    Buchungsbestätigung.<br />
                    2. Es wurde ein Warteplatz gebucht. Sollte ein
                    Teilnehmerplatz freiwerden so werden Sie mit einer
                    gesonderten E-Mail darüber informiert.</span
                  >
                </div>
              </div>
            </div>

            <!-- Booking tickets -->
            <div class="py-2">
              <div
                class="mb-1 flex flex-row justify-between px-4 text-sm md:text-base"
                *ngFor="let discountGroup of reservation.discountGroups"
              >
                <div class="flex w-3/4 flex-row gap-3 md:gap-12">
                  <div
                    class="flex flex-row gap-0 leading-none md:gap-2 md:text-lg"
                  >
                    <span>{{ discountGroup.tickets.length }}x</span>
                    <span class="flex w-[4.3rem] justify-end"
                      >{{
                        discountGroup.tickets[0].price | number : '1.2-2'
                      }}
                      €</span
                    >
                  </div>
                  <div class="flex flex-col gap-1">
                    <span class="font-bold leading-none md:text-lg"
                      >{{
                        discountGroup.discountType
                          ? 'Ermäßigung | ' + discountGroup.discountType.name
                          : 'Normal'
                      }}
                    </span>
                    <span class="text-sm leading-tight md:font-bold">
                      <p
                        class="flex flex-col md:flex-row md:gap-1"
                        *ngFor="let ticket of discountGroup.tickets"
                        [ngClass]="ticketTypeClass[ticket.eventTicket.type]"
                      >
                        <span>
                          1
                          {{
                            ticketTypeNames[ticket.eventTicket.type] +
                              (ticket.discountVoucher
                                ? ', Nachweis erforderlich'
                                : '')
                          }}
                        </span>
                        <span class="notranslate"
                          >({{ ticket.child?.firstname }}
                          {{ ticket.child?.surname }}){{
                            ticket.eventTicket.type === 'waitinglist'
                              ? ', '
                              : ''
                          }}</span
                        >
                        <span>
                          {{
                            ticket.eventTicket.type === 'waitinglist'
                              ? 'Platz ' + ticket.eventTicket.number
                              : ''
                          }}
                        </span>
                      </p>
                    </span>
                  </div>
                </div>
                <span class="flex flex-row justify-end md:text-lg"
                  >{{ discountGroup.sum | number : '1.2-2' }} €</span
                >
              </div>
            </div>
            <div class="divider mx-4 mb-1 !border-b"></div>

            <!-- Total price -->
            <div
              class="mb-2 flex flex-row justify-between px-4 text-sm font-bold md:justify-end md:gap-8 md:text-lg"
            >
              <span>Gesamtpreis:</span>
              <span>{{ reservation.price | number : '1.2-2' }} €</span>
            </div>
          </div>
          <div
            class="white-card flex flex-col !p-0"
            *ngIf="bookingOrReservation.booking; let booking"
          >
            <!-- Booking header -->
            <div
              class="flex flex-col rounded-t-[11px] bg-[#203C60] px-3 py-2 text-white md:flex-row md:items-center md:justify-between md:py-2"
            >
              <div
                class="flex flex-col text-sm md:text-base xl:flex-row xl:gap-5"
              >
                <div class="relative hidden xl:flex">
                  <button
                    class="peer border-none bg-transparent p-1 text-white"
                  >
                    <i class="mi notranslate outlined">info</i>
                  </button>
                  <span
                    class="absolute left-7 hidden w-[42rem] items-start gap-2 rounded-md border border-gray-500 bg-white p-2 leading-tight text-primaryLight shadow-md peer-hover:flex peer-focus:flex"
                    ><i class="mi notranslate outlined mt-1">info</i
                    >{{ bookingInfoText }}</span
                  >
                </div>

                <div class="flex flex-row justify-between md:gap-2 xl:gap-3">
                  <span class="text-[#C5DDEC]">Buchung</span>
                  <p class="font-bold">{{ booking.code }}</p>
                </div>
                <div class="flex flex-row justify-between md:gap-2 xl:gap-3">
                  <span class="text-[#C5DDEC]">Datum</span>
                  <p class="font-bold">
                    {{ booking.date | date : 'dd.MM.yyyy HH:mm:ss' }}
                  </p>
                </div>
                <div
                  class="flex flex-row items-center justify-between md:gap-6 xl:gap-3"
                >
                  <span class="text-[#C5DDEC]">Zahlungsstatus</span>
                  <p
                    class="badge h-5 items-center justify-center text-sm md:h-[1.6rem] md:w-28 md:text-base"
                    [ngClass]="badgeColors[booking.status]"
                  >
                    {{ statusNames[booking.status] }}
                  </p>
                </div>
              </div>

              <div
                class="mb-2 mt-4 flex flex-row items-center justify-between text-base md:m-0 md:gap-4"
              >
                <a
                  href="mailto:{{ booking.event.organizer.email }}?subject={{
                    cancelEmailSubject(booking)
                  }}&body={{ cancelEmailBody(booking) }}"
                >
                  <span class="font-bold text-[#FF9EA0] md:text-base"
                    >Stornieren</span
                  >
                </a>
                <div class="relative flex xl:hidden">
                  <button
                    class="peer border-none bg-transparent p-1 text-white"
                  >
                    <i class="mi notranslate outlined">info</i>
                  </button>
                  <span
                    class="absolute -top-1 right-7 hidden w-72 items-start gap-2 rounded-md border border-gray-500 bg-white p-2 leading-tight text-primaryLight shadow-md peer-hover:flex peer-focus:flex md:w-[42rem]"
                    ><i class="mi notranslate outlined mt-1">info</i
                    >{{ bookingInfoText }}</span
                  >
                </div>
              </div>
            </div>

            <!-- Booking tickets -->
            <div class="py-2">
              <div
                class="mb-1 flex flex-row justify-between px-4 text-sm md:px-4 md:text-base"
                *ngFor="let discountGroup of booking.discountGroups"
              >
                <div class="flex w-3/4 flex-row gap-3 md:gap-12">
                  <div
                    class="flex flex-row gap-0 leading-none md:gap-2 md:text-lg"
                  >
                    <span>{{ discountGroup.tickets.length }}x</span>
                    <span class="flex w-[4.3rem] justify-end"
                      >{{
                        discountGroup.tickets[0].price | number : '1.2-2'
                      }}
                      €</span
                    >
                  </div>
                  <div class="flex flex-col gap-1">
                    <span class="font-bold leading-none md:text-lg"
                      >{{
                        discountGroup.discountType
                          ? 'Ermäßigung | ' + discountGroup.discountType.name
                          : 'Normal'
                      }}
                    </span>
                    <span class="text-sm leading-tight md:font-bold">
                      <p
                        class="flex flex-col md:flex-row md:gap-1"
                        *ngFor="let ticket of discountGroup.tickets"
                        [ngClass]="ticketTypeClass[ticket.eventTicket.type]"
                      >
                        1
                        {{ ticketTypeNames[ticket.eventTicket.type] }}&nbsp;
                        <span class="notranslate"
                          >({{ ticket.child?.firstname }}
                          {{ ticket.child?.surname }})</span
                        >
                      </p></span
                    >
                  </div>
                </div>
                <span class="flex flex-row justify-end md:text-lg"
                  >{{ discountGroup.sum | number : '1.2-2' }} €</span
                >
              </div>
            </div>
            <div class="divider mx-4 !border-b"></div>

            <!-- Total price -->
            <div
              class="my-1 flex flex-row justify-between px-4 text-sm font-bold md:justify-end md:gap-8 md:text-lg"
            >
              <span>Gesamtpreis:</span>
              <span>{{ booking.price | number : '1.2-2' }} €</span>
            </div>

            <div class="divider mx-4 !border-b"></div>

            <!-- Bank -->
            <div
              class="flex flex-col gap-x-4 gap-y-0 px-4 py-2 text-xs sm:flex-row sm:gap-y-2 lg:gap-8"
            >
              <span class="text-gray-500">Bankverbindung</span>
              <span class="flex flex-row gap-1"
                ><span class="text-[#757575]">IBAN:</span>
                {{ booking.event.organizer.bankIban }}</span
              >
              <span
                class="flex flex-row gap-1"
                *ngIf="booking.event.organizer.bankBic"
                ><span class="text-[#757575]">BIC/Swift:</span>
                {{ booking.event.organizer.bankBic }}</span
              >
              <span
                class="flex flex-row gap-1"
                *ngIf="booking.event.organizer.bankName"
                ><span class="text-[#757575]">Bank:</span>
                {{ booking.event.organizer.bankName }}</span
              >
              <span class="flex flex-row gap-1"
                ><span class="text-[#757575]">Verwendungszweck:</span>
                {{ booking.code }}</span
              >
            </div>
          </div>
        </div>
      </div>
    </div>
  `,
  styles: [
    `
      :host {
        @apply flex flex-col flex-grow gap-4;
      }
    `,
  ],
  providers: [DatePipe],
})
export class BookingsComponent {
  eventBookings$ = this.store.select(selectEventsWithBookingsAndTickets);
  noBookingEvents$ = this.store.select(selectNoBookingEvents);
  bookingInfoText: string =
    'Tickets einer Buchung \n sind für sie reserviert und müssen innerhalb 14 Tage ab Buchungsdatum bezahlt werden. Die Bezahlung erfolgt mittels Überweisung an die untenstehende Kontoverbindung.';
  // reservationInfoText: string =
  //   'Tickets 1: \neiner Buchung sind für sie reserviert und müssen innerhalb 14 Tage ab Buchungsdatum bezahlt werden. Die Bezahlung erfolgt mittels Überweisung an die untenstehende Kontoverbindung.';

  badgeColors: { [index: string]: string } = {
    payed: 'greenInBanner',
    pending: 'orangeInBanner',
    overdue: 'redInBanner',
  };

  statusNames: { [index: string]: string } = {
    overdue: 'überfällig',
    pending: 'ausstehend',
    payed: 'bezahlt',
  };

  ticketTypeNames: { [index: string]: string } = {
    participant: 'Teilnehmerplatz',
    waitinglist: 'Wartelistenplatz',
  };

  ticketTypeClass: { [index: string]: string } = {
    waitinglist: 'text-[#E90C0C]',
    participant: 'text-[#617794]',
  };

  constructor(private store: Store) {}

  cancelEmailSubject = (booking: Booking | Reservation) =>
    `Stornierungsanfrage - ${booking.code}`;
  cancelEmailBody = (booking: Booking | Reservation) => {
    const careDays = booking.event.carePeriodCommitted.careDays;
    return (
      'Ich möchte bitte meine Buchung mit den folgenden Daten stornieren:' +
      '%0D%0A%0D%0A' +
      `Buchungsnummer: ${booking.code}` +
      '%0D%0A' +
      `Veranstaltung: ${booking.event.name}` +
      '%0D%0A' +
      `Ferienwoche: ${this.formatDate(careDays[0].day)}${
        careDays.length > 1
          ? ' - ' + this.formatDate(careDays[careDays.length - 1].day)
          : ''
      }`
    );
  };

  private formatDate(date: string) {
    return new DatePipe('de').transform(date, 'dd. MMM yyyy');
  }
}
