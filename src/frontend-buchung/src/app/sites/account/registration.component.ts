import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { RegistrationActions } from 'src/app/stores/registration/registration.actions';
import { selectRegistrationForm } from 'src/app/stores/registration/registration.selectors';

@Component({
  selector: 'app-registration',
  template: `
    <h2 class="site-title">Neues Konto erstellen</h2>

    <div class="white-card flex flex-col">
      <form [ngrxFormState]="formState" *ngIf="formState$ | async as formState">
        <h3 class="h3">Konto</h3>
        <div class="divider mb-2"></div>
        <p class="label flex-wrap leading-tight">
          Bitte füllen Sie alle mit<span class="text-[#E5153B]"
            >&nbsp;*&nbsp;</span
          >gekennzeichneten Pflichtfelder aus
        </p>
        <div class="mt-4 flex flex-col gap-4 md:flex-row md:items-end">
          <div class="flex flex-grow flex-col md:w-1/2">
            <div class="flex flex-row">
              <p class="label red">E-Mail</p>
            </div>
            <input
              aria-label="Eingabefeld E-Mail Adresse"
              type="email"
              class="mr-8 md:mr-0"
              [ngrxFormControlState]="formState.controls.email"
            />
            <div class="h-3">
              <p
                *ngIf="
                  formState.controls.email.isInvalid &&
                  formState.controls.email.isSubmitted
                "
                class="error-text flex flex-grow flex-col"
              ></p>
            </div>
          </div>
          <div class="flex flex-grow flex-col md:w-1/2">
            <div class="flex flex-row">
              <p class="label red">Passwort</p>
            </div>
            <div class="flex flex-col text-xs leading-none">
              <p class="font-semibold text-[#474747]">
                Mindestens 12 Zeichen (mind. 1 Kleinbuchstabe, mind. 1
                Großbuchstabe und mind. 1 Zahl)
              </p>
              <a
                class="link"
                href="https://www.bsi.bund.de/DE/Themen/Verbraucherinnen-und-Verbraucher/Informationen-und-Empfehlungen/Cyber-Sicherheitsempfehlungen/Accountschutz/Sichere-Passwoerter-erstellen/sichere-passwoerter-erstellen_node.html"
                target="_blank"
                >Was ist ein sicheres Passwort?</a
              >
            </div>
            <div class="flex flex-grow flex-row items-center gap-2">
              <input
                aria-label="Eingabefeld Passwort"
                class="!mt-2 flex flex-grow"
                [type]="showPassword ? 'text' : 'password'"
                [ngrxFormControlState]="formState.controls.password"
              />
              <button
                class="mr-8 h-10 w-10 md:mr-0"
                aria-label="Passwort in Klartext anzeigen"
                (click)="toggleShowPassword()"
              >
                <i class="mi notranslate">visibility</i>
              </button>
            </div>
            <div class="h-3">
              <p
                *ngIf="
                  formState.controls.password.isInvalid &&
                  formState.controls.password.isSubmitted
                "
                class="error-text flex flex-grow flex-col"
              ></p>
            </div>
          </div>
        </div>
        <div class="mt-6">
          <h3 class="h3">Allgemeine Informationen</h3>
        </div>
        <div class="divider mb-2"></div>
        <div class="mt-4">
          <div class="flex flex-row">
            <p class="label red">Anrede</p>
          </div>
          <select
            id="anrede"
            aria-label="Auswahl Anrede"
            class="w-28 px-2 py-2"
            [ngrxFormControlState]="formState.controls.salutation"
          >
            <option value="mr" selected>Herr</option>
            <option value="ms">Frau</option>
          </select>
          <div class="h-3">
            <p
              *ngIf="
                formState.controls.salutation.isInvalid &&
                formState.controls.salutation.isSubmitted
              "
              class="error-text flex flex-grow flex-col"
            ></p>
          </div>
        </div>
        <div class="mt-4 flex flex-col gap-4 md:flex-row">
          <div class="flex flex-grow flex-col">
            <div class="flex flex-row">
              <p class="label red">Vorname</p>
            </div>
            <input
              aria-label="Eingabefeld Vorname"
              type="text"
              [ngrxFormControlState]="formState.controls.firstname"
            />
            <div class="h-3">
              <p
                *ngIf="
                  formState.controls.firstname.isInvalid &&
                  formState.controls.firstname.isSubmitted
                "
                class="error-text flex flex-grow flex-col"
              ></p>
            </div>
          </div>
          <div class="flex flex-grow flex-col">
            <div class="flex flex-row">
              <p class="label red">Nachname</p>
            </div>
            <input
              aria-label="Eingabefeld Nachname"
              type="text"
              [ngrxFormControlState]="formState.controls.surname"
            />
            <div class="h-3">
              <p
                *ngIf="
                  formState.controls.surname.isInvalid &&
                  formState.controls.surname.isSubmitted
                "
                class="error-text flex flex-grow flex-col"
              ></p>
            </div>
          </div>
        </div>
        <div class="mt-6">
          <h3 class="h3">Adresse</h3>
        </div>
        <div class="divider mb-2"></div>
        <div class="mt-4 flex flex-col gap-4 md:flex-row">
          <div class="flex flex-grow flex-col">
            <div class="flex flex-row">
              <p class="label red">Straße</p>
            </div>
            <input
              aria-label="Eingabefeld Straße"
              type="text"
              [ngrxFormControlState]="formState.controls.addressStreet"
            />
            <div class="h-3">
              <p
                *ngIf="
                  formState.controls.addressStreet.isInvalid &&
                  formState.controls.addressStreet.isSubmitted
                "
                class="error-text flex flex-grow flex-col"
              ></p>
            </div>
          </div>
          <div class="flex flex-grow flex-col">
            <div class="flex flex-row">
              <p class="label red">Hausnummer</p>
            </div>
            <input
              aria-label="Eingabefeld Hausnummer"
              type="text"
              [ngrxFormControlState]="formState.controls.addressHouseNumber"
            />
            <div class="h-3">
              <p
                *ngIf="
                  formState.controls.addressHouseNumber.isInvalid &&
                  formState.controls.addressHouseNumber.isSubmitted
                "
                class="error-text flex flex-grow flex-col"
              ></p>
            </div>
          </div>
        </div>
        <div class="mt-4 flex flex-grow flex-col">
          <p class="label">Zusatzzeile</p>
          <input
            type="text"
            aria-label="Eingabefeld"
            placeholder="Optional"
            [ngrxFormControlState]="formState.controls.addressAdditional"
          />
        </div>
        <div class="mt-4 flex flex-col gap-4 md:flex-row">
          <div class="flex flex-grow flex-col">
            <div class="flex flex-row">
              <p class="label red">Postleitzahl</p>
            </div>
            <input
              aria-label="Eingabefeld Postleitzahl"
              type="text"
              [ngrxFormControlState]="formState.controls.addressZip"
            />
            <div class="h-3">
              <p
                *ngIf="
                  formState.controls.addressZip.isInvalid &&
                  formState.controls.addressZip.isSubmitted
                "
                class="error-text flex flex-grow flex-col"
              ></p>
            </div>
          </div>
          <div class="flex flex-grow flex-col">
            <div class="flex flex-row">
              <p class="label red">Stadt</p>
            </div>
            <input
              aria-label="Eingabefeld Stadt"
              type="text"
              [ngrxFormControlState]="formState.controls.addressCity"
            />
            <div class="h-3">
              <p
                *ngIf="
                  formState.controls.addressCity.isInvalid &&
                  formState.controls.addressCity.isSubmitted
                "
                class="error-text flex flex-grow flex-col"
              ></p>
            </div>
          </div>
        </div>
        <div class="mt-6">
          <h3 class="h3">Kontaktdaten</h3>
        </div>
        <div class="divider mb-2"></div>
        <div class="mt-4 flex flex-col gap-4 md:flex-row">
          <div class="flex flex-grow flex-col">
            <div class="flex flex-row">
              <p class="label red">Telefon</p>
            </div>
            <input
              aria-label="Eingabefeld Telefonummer"
              type="text"
              placeholder="+49 123 12345678"
              [ngrxFormControlState]="formState.controls.phone"
            />
            <div
              *ngIf="
                formState.controls.phone.isInvalid &&
                formState.controls.phone.isSubmitted
              "
              class="error-text flex flex-grow flex-col"
            ></div>
          </div>
          <div class="flex flex-grow flex-col">
            <div class="flex flex-row">
              <p class="label red">Notfallnummer</p>
            </div>
            <input
              aria-label="Eingabefeld Notfallnummer"
              type="text"
              placeholder="+49 123 12345678"
              [ngrxFormControlState]="formState.controls.emergencyPhone"
            />

            <p
              *ngIf="
                formState.controls.addressHouseNumber.isInvalid &&
                formState.controls.addressHouseNumber.isSubmitted
              "
              class="error-text flex flex-grow flex-col"
            ></p>

            <span class="text-xs font-semibold leading-none text-[#757575]"
              >Bevorzugte Handynummer</span
            >
          </div>
        </div>
        <div
          class="mt-3 flex flex-col-reverse md:flex-row md:items-center md:justify-between"
        >
          <div class="flex flex-col gap-2">
            <div class="flex flex-row items-start">
              <div class="flex">
                <input
                  type="checkbox"
                  aria-label="Checkbox, Wiederruf- und Datenschutzerklärung"
                  class="checkbox mr-2 mt-[0.2rem]"
                  [ngrxFormControlState]="
                    formState.controls.agreeDataProtection
                  "
                />
              </div>
              <span class="inline text-xs">
                Ich habe die
                <a
                  routerLink="/widerruf"
                  class="link"
                  target="_blank"
                  aria-label="Widerrufsbelehrungs Seite"
                  >Widerrufsbelehrung</a
                >
                und die
                <a
                  routerLink="/datenschutz"
                  class="link"
                  target="_blank"
                  aria-label="Datenschutzerklärungs Seite"
                  >Datenschutzerklärung</a
                >
                gelesen und bin einverstanden
              </span>
            </div>
          </div>
        </div>
        <div class="mt-8 flex">
          <button
            class="flex flex-grow justify-center p-2"
            aria-label="Registrieren"
            (click)="register()"
          >
            <span class="text-xl"> Registrieren</span>
          </button>
        </div>
      </form>
    </div>
  `,
  styles: [
    `
      :host {
        @apply flex flex-col flex-grow md:px-60;
      }

      .label {
        @apply font-bold text-sm;
        &.red {
          @apply after:content-['*'] after:ml-0.5 after:text-[#E5153B];
        }
      }
      .h3 {
        @apply text-lg font-extrabold;
      }
    `,
  ],
})
export class RegistrationComponent {
  showPassword = false;

  formState$ = this.store.select(selectRegistrationForm);

  constructor(private store: Store) {}

  toggleShowPassword() {
    this.showPassword = !this.showPassword;
  }

  register() {
    this.store.dispatch(RegistrationActions.registerCustodian());
  }
}
