import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { VerificationActions } from 'src/app/stores/verification/verification.actions';
import {
  selectResendCodeForm,
  selectVerificationForm,
} from 'src/app/stores/verification/verification.selectors';

@Component({
  selector: 'app-registration',
  template: `
    <h2 class="site-title !mb-2">Konto aktivieren</h2>

    <div class="flex flex-grow flex-col gap-4 lg:flex-col">
      <div class="white-card flex flex-col">
        <form
          [ngrxFormState]="formState"
          *ngIf="validationFormState$ | async as formState"
        >
          <div class="flex flex-col gap-4">
            <div class="flex flex-grow flex-col">
              <div class="flex flex-col">
                <p class="mb-1 text-sm leading-tight">
                  Bitte geben Sie Ihren Aktivierungsschlüssel hier ein, um Ihr
                  Konto zu aktivieren. Falls Sie den Aktivierungslink aus der
                  E-Mail angeklickt haben, wird der Schlüssel automatisch in das
                  Feld eingetragen.
                </p>
                <p class="label red">Aktivierungsschlüssel</p>
              </div>

              <div class="flex flex-row items-center gap-2">
                <input
                  class="flex flex-grow"
                  aria-label="Eingabelfeld Code"
                  [type]="showToken ? 'text' : 'password'"
                  [ngrxFormControlState]="formState.controls.token"
                />
                <button
                  class="h-10 w-10"
                  aria-label="Code in Klartext anzeigen"
                  (click)="toggleShowToken()"
                >
                  <i class="mi notranslate">visibility</i>
                </button>
              </div>
            </div>
          </div>

          <div class="mt-8 flex">
            <button
              class="flex flex-grow justify-center p-2"
              aria-label="Code aktivieren"
              (click)="verify()"
            >
              <span class="text-xl"> Aktivieren</span>
            </button>
          </div>
        </form>
      </div>

      <h2 class="site-title !mb-0">Neuer Aktivierungsschlüssel</h2>
      <div class="white-card flex flex-col">
        <form
          [ngrxFormState]="formState"
          *ngIf="resendCodeFormState$ | async as formState"
        >
          <div class="flex flex-grow flex-col">
            <p class="mb-1 text-sm leading-tight">
              Falls Sie Ihren Aktivierungsschlüssel verloren haben oder
              abgelaufen ist, können Sie hier einen neuen anfordern.
            </p>
            <p class="label red">E-Mail</p>

            <input
              aria-label="Eingabelfeld E-Mail Adresse"
              type="email"
              [ngrxFormControlState]="formState.controls.email"
            />
          </div>
          <div class="mt-8 flex">
            <button
              class="flex flex-grow justify-center p-2"
              (click)="sendCode()"
            >
              <span class="text-xl"> Neuen Aktivierungschlüssel senden</span>
            </button>
          </div>
        </form>
      </div>
    </div>
  `,
  styles: [
    `
      :host {
        @apply flex flex-col flex-grow md:px-20 lg:px-60 xl:px-80;
      }

      .label {
        @apply font-bold text-sm;
        &.red {
          @apply after:content-['*'] after:ml-0.5 after:text-red-500;
        }
      }
    `,
  ],
})
export class VerificationComponent {
  showToken = false;

  validationFormState$ = this.store.select(selectVerificationForm);
  resendCodeFormState$ = this.store.select(selectResendCodeForm);

  constructor(private store: Store) {}

  toggleShowToken() {
    this.showToken = !this.showToken;
  }

  verify() {
    this.store.dispatch(VerificationActions.verifyAccount());
  }

  sendCode() {
    this.store.dispatch(VerificationActions.sendVerifyAccount());
  }
}
