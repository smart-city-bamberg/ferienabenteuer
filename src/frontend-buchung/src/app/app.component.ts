import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <div class="flex flex-grow">
      <router-outlet></router-outlet>
    </div>
    <app-footer style="box-shadow: 1px 14px 20px 0px #333"></app-footer>
  `,
  styles: [
    `
      :host {
        @apply flex flex-col flex-grow;
      }
    `,
  ],
})
export class AppComponent {}
