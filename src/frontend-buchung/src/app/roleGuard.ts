import { inject } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { selectUserRole } from './stores/auth/auth.selectors';
import { filter, map } from 'rxjs';

export const roleGuard = (allowedRoles: string[]) => () => {
  const store = inject(Store);
  const router = inject(Router);
  return store.select(selectUserRole).pipe(
    filter((userRole) => userRole !== undefined),
    map((userRole) =>
      userRole && allowedRoles.includes(userRole ?? '')
        ? true
        : router.parseUrl('keinzugang')
    )
  );
};

export const custodianCanActivate = roleGuard(['custodian']);
