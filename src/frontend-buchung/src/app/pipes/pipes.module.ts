import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImageProxyPipe } from './image-proxy.pipe';

@NgModule({
  imports: [CommonModule],
  declarations: [ImageProxyPipe],
  exports: [ImageProxyPipe],
})
export class PipesModule {}
