import { Pipe, type PipeTransform } from '@angular/core';
import { environment } from 'src/environments/environment';

@Pipe({
  name: 'imageProxy',
})
export class ImageProxyPipe implements PipeTransform {
  transform(value: string | undefined, options: string): string | undefined {
    if (!value) {
      return undefined;
    }

    const imageProxy = environment.imageProxy;

    if (!imageProxy.enabled) {
      return value;
    }

    const resultUrl = value.replace(
      imageProxy.replace,
      imageProxy.url + '/' + options
    );

    return resultUrl;
  }
}
