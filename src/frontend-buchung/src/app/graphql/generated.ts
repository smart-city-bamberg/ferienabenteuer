import { gql } from 'apollo-angular';
import { Injectable } from '@angular/core';
import * as Apollo from 'apollo-angular';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
export type MakeEmpty<T extends { [key: string]: unknown }, K extends keyof T> = { [_ in K]?: never };
export type Incremental<T> = T | { [P in keyof T]?: P extends ' $fragmentName' | '__typename' ? T[P] : never };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: { input: string; output: string; }
  String: { input: string; output: string; }
  Boolean: { input: boolean; output: boolean; }
  Int: { input: number; output: number; }
  Float: { input: number; output: number; }
  CalendarDay: { input: any; output: any; }
  DateTime: { input: any; output: any; }
  /** The `JSON` scalar type represents JSON values as specified by [ECMA-404](http://www.ecma-international.org/publications/files/ECMA-ST/ECMA-404.pdf). */
  JSON: { input: any; output: any; }
  /** The `Upload` scalar type represents a file upload. */
  Upload: { input: any; output: any; }
};

export type AddTicketsToCartAmountsInput = {
  amount: Scalars['Int']['input'];
  discountType?: InputMaybe<Scalars['ID']['input']>;
};

export type AddTicketsToCartFailed = {
  __typename?: 'AddTicketsToCartFailed';
  reason: Scalars['String']['output'];
};

export type AddTicketsToCartResult = AddTicketsToCartFailed | AddTicketsToCartSuccess;

export type AddTicketsToCartSuccess = {
  __typename?: 'AddTicketsToCartSuccess';
  cartTickets: Array<CartTicket>;
};

export type ApplyForDiscountFailed = {
  __typename?: 'ApplyForDiscountFailed';
  reason: Scalars['String']['output'];
};

export type ApplyForDiscountResult = ApplyForDiscountFailed | ApplyForDiscountSuccess;

export type ApplyForDiscountSuccess = {
  __typename?: 'ApplyForDiscountSuccess';
  discount: Discount;
};

export type ApproveDiscountFailed = {
  __typename?: 'ApproveDiscountFailed';
  reason: Scalars['String']['output'];
};

export type ApproveDiscountResult = ApproveDiscountFailed | ApproveDiscountSuccess;

export type ApproveDiscountSuccess = {
  __typename?: 'ApproveDiscountSuccess';
  discount: Discount;
};

export type AssignChildToCartTicketFailed = {
  __typename?: 'AssignChildToCartTicketFailed';
  reason: Scalars['String']['output'];
};

export type AssignChildToCartTicketResult = AssignChildToCartTicketFailed | AssignChildToCartTicketSuccess;

export type AssignChildToCartTicketSuccess = {
  __typename?: 'AssignChildToCartTicketSuccess';
  cartTicket: CartTicket;
};

export type AuthenticatedItem = User;

export type AvailableTicketCountResult = {
  __typename?: 'AvailableTicketCountResult';
  eventId?: Maybe<Scalars['ID']['output']>;
  participant?: Maybe<Scalars['Int']['output']>;
  waitinglist?: Maybe<Scalars['Int']['output']>;
};

export type BookCartTicketsFailed = {
  __typename?: 'BookCartTicketsFailed';
  reason: Scalars['String']['output'];
};

export type BookCartTicketsResult = BookCartTicketsFailed | BookCartTicketsSuccess;

export type BookCartTicketsSuccess = {
  __typename?: 'BookCartTicketsSuccess';
  bookings: Array<Booking>;
  reservations: Array<Reservation>;
};

export type Booking = {
  __typename?: 'Booking';
  code?: Maybe<Scalars['String']['output']>;
  custodian?: Maybe<Custodian>;
  date?: Maybe<Scalars['DateTime']['output']>;
  event?: Maybe<Event>;
  id: Scalars['ID']['output'];
  isDeleted?: Maybe<Scalars['Boolean']['output']>;
  price?: Maybe<Scalars['Float']['output']>;
  reminderDate?: Maybe<Scalars['DateTime']['output']>;
  status?: Maybe<Scalars['String']['output']>;
  tickets?: Maybe<Array<Ticket>>;
  ticketsCount?: Maybe<Scalars['Int']['output']>;
};


export type BookingTicketsArgs = {
  cursor?: InputMaybe<TicketWhereUniqueInput>;
  orderBy?: Array<TicketOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: TicketWhereInput;
};


export type BookingTicketsCountArgs = {
  where?: TicketWhereInput;
};

export type BookingCreateInput = {
  code?: InputMaybe<Scalars['String']['input']>;
  custodian?: InputMaybe<CustodianRelateToOneForCreateInput>;
  date?: InputMaybe<Scalars['DateTime']['input']>;
  event?: InputMaybe<EventRelateToOneForCreateInput>;
  isDeleted?: InputMaybe<Scalars['Boolean']['input']>;
  reminderDate?: InputMaybe<Scalars['DateTime']['input']>;
  status?: InputMaybe<Scalars['String']['input']>;
  tickets?: InputMaybe<TicketRelateToManyForCreateInput>;
};

export type BookingManyRelationFilter = {
  every?: InputMaybe<BookingWhereInput>;
  none?: InputMaybe<BookingWhereInput>;
  some?: InputMaybe<BookingWhereInput>;
};

export type BookingOrderByInput = {
  code?: InputMaybe<OrderDirection>;
  date?: InputMaybe<OrderDirection>;
  id?: InputMaybe<OrderDirection>;
  isDeleted?: InputMaybe<OrderDirection>;
  reminderDate?: InputMaybe<OrderDirection>;
  status?: InputMaybe<OrderDirection>;
};

export type BookingRelateToManyForCreateInput = {
  connect?: InputMaybe<Array<BookingWhereUniqueInput>>;
  create?: InputMaybe<Array<BookingCreateInput>>;
};

export type BookingRelateToManyForUpdateInput = {
  connect?: InputMaybe<Array<BookingWhereUniqueInput>>;
  create?: InputMaybe<Array<BookingCreateInput>>;
  disconnect?: InputMaybe<Array<BookingWhereUniqueInput>>;
  set?: InputMaybe<Array<BookingWhereUniqueInput>>;
};

export type BookingRelateToOneForCreateInput = {
  connect?: InputMaybe<BookingWhereUniqueInput>;
  create?: InputMaybe<BookingCreateInput>;
};

export type BookingRelateToOneForUpdateInput = {
  connect?: InputMaybe<BookingWhereUniqueInput>;
  create?: InputMaybe<BookingCreateInput>;
  disconnect?: InputMaybe<Scalars['Boolean']['input']>;
};

export type BookingUpdateArgs = {
  data: BookingUpdateInput;
  where: BookingWhereUniqueInput;
};

export type BookingUpdateInput = {
  code?: InputMaybe<Scalars['String']['input']>;
  custodian?: InputMaybe<CustodianRelateToOneForUpdateInput>;
  date?: InputMaybe<Scalars['DateTime']['input']>;
  event?: InputMaybe<EventRelateToOneForUpdateInput>;
  isDeleted?: InputMaybe<Scalars['Boolean']['input']>;
  reminderDate?: InputMaybe<Scalars['DateTime']['input']>;
  status?: InputMaybe<Scalars['String']['input']>;
  tickets?: InputMaybe<TicketRelateToManyForUpdateInput>;
};

export type BookingWhereInput = {
  AND?: InputMaybe<Array<BookingWhereInput>>;
  NOT?: InputMaybe<Array<BookingWhereInput>>;
  OR?: InputMaybe<Array<BookingWhereInput>>;
  code?: InputMaybe<StringFilter>;
  custodian?: InputMaybe<CustodianWhereInput>;
  date?: InputMaybe<DateTimeNullableFilter>;
  event?: InputMaybe<EventWhereInput>;
  id?: InputMaybe<IdFilter>;
  isDeleted?: InputMaybe<BooleanFilter>;
  reminderDate?: InputMaybe<DateTimeNullableFilter>;
  status?: InputMaybe<StringNullableFilter>;
  tickets?: InputMaybe<TicketManyRelationFilter>;
};

export type BookingWhereUniqueInput = {
  code?: InputMaybe<Scalars['String']['input']>;
  id?: InputMaybe<Scalars['ID']['input']>;
};

export type BooleanFilter = {
  equals?: InputMaybe<Scalars['Boolean']['input']>;
  not?: InputMaybe<BooleanFilter>;
};

export type CalendarDayFilter = {
  equals?: InputMaybe<Scalars['CalendarDay']['input']>;
  gt?: InputMaybe<Scalars['CalendarDay']['input']>;
  gte?: InputMaybe<Scalars['CalendarDay']['input']>;
  in?: InputMaybe<Array<Scalars['CalendarDay']['input']>>;
  lt?: InputMaybe<Scalars['CalendarDay']['input']>;
  lte?: InputMaybe<Scalars['CalendarDay']['input']>;
  not?: InputMaybe<CalendarDayFilter>;
  notIn?: InputMaybe<Array<Scalars['CalendarDay']['input']>>;
};

export type CalendarDayNullableFilter = {
  equals?: InputMaybe<Scalars['CalendarDay']['input']>;
  gt?: InputMaybe<Scalars['CalendarDay']['input']>;
  gte?: InputMaybe<Scalars['CalendarDay']['input']>;
  in?: InputMaybe<Array<Scalars['CalendarDay']['input']>>;
  lt?: InputMaybe<Scalars['CalendarDay']['input']>;
  lte?: InputMaybe<Scalars['CalendarDay']['input']>;
  not?: InputMaybe<CalendarDayNullableFilter>;
  notIn?: InputMaybe<Array<Scalars['CalendarDay']['input']>>;
};

export type CancelDiscountFailed = {
  __typename?: 'CancelDiscountFailed';
  reason: Scalars['String']['output'];
};

export type CancelDiscountResult = CancelDiscountFailed | CancelDiscountSuccess;

export type CancelDiscountSuccess = {
  __typename?: 'CancelDiscountSuccess';
  discount: Discount;
};

export type CareDay = {
  __typename?: 'CareDay';
  day?: Maybe<Scalars['CalendarDay']['output']>;
  id: Scalars['ID']['output'];
  period?: Maybe<CarePeriod>;
};

export type CareDayCreateInput = {
  day?: InputMaybe<Scalars['CalendarDay']['input']>;
  period?: InputMaybe<CarePeriodRelateToOneForCreateInput>;
};

export type CareDayManyRelationFilter = {
  every?: InputMaybe<CareDayWhereInput>;
  none?: InputMaybe<CareDayWhereInput>;
  some?: InputMaybe<CareDayWhereInput>;
};

export type CareDayOrderByInput = {
  day?: InputMaybe<OrderDirection>;
  id?: InputMaybe<OrderDirection>;
};

export type CareDayRelateToManyForCreateInput = {
  connect?: InputMaybe<Array<CareDayWhereUniqueInput>>;
  create?: InputMaybe<Array<CareDayCreateInput>>;
};

export type CareDayRelateToManyForUpdateInput = {
  connect?: InputMaybe<Array<CareDayWhereUniqueInput>>;
  create?: InputMaybe<Array<CareDayCreateInput>>;
  disconnect?: InputMaybe<Array<CareDayWhereUniqueInput>>;
  set?: InputMaybe<Array<CareDayWhereUniqueInput>>;
};

export type CareDayUpdateArgs = {
  data: CareDayUpdateInput;
  where: CareDayWhereUniqueInput;
};

export type CareDayUpdateInput = {
  day?: InputMaybe<Scalars['CalendarDay']['input']>;
  period?: InputMaybe<CarePeriodRelateToOneForUpdateInput>;
};

export type CareDayWhereInput = {
  AND?: InputMaybe<Array<CareDayWhereInput>>;
  NOT?: InputMaybe<Array<CareDayWhereInput>>;
  OR?: InputMaybe<Array<CareDayWhereInput>>;
  day?: InputMaybe<CalendarDayNullableFilter>;
  id?: InputMaybe<IdFilter>;
  period?: InputMaybe<CarePeriodWhereInput>;
};

export type CareDayWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
};

export type CarePeriod = {
  __typename?: 'CarePeriod';
  careDays?: Maybe<Array<CareDay>>;
  careDaysCount?: Maybe<Scalars['Int']['output']>;
  displayName?: Maybe<Scalars['String']['output']>;
  event?: Maybe<Event>;
  id: Scalars['ID']['output'];
  period?: Maybe<Period>;
};


export type CarePeriodCareDaysArgs = {
  cursor?: InputMaybe<CareDayWhereUniqueInput>;
  orderBy?: Array<CareDayOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: CareDayWhereInput;
};


export type CarePeriodCareDaysCountArgs = {
  where?: CareDayWhereInput;
};

export type CarePeriodCreateInput = {
  careDays?: InputMaybe<CareDayRelateToManyForCreateInput>;
  event?: InputMaybe<EventRelateToOneForCreateInput>;
  period?: InputMaybe<PeriodRelateToOneForCreateInput>;
};

export type CarePeriodManyRelationFilter = {
  every?: InputMaybe<CarePeriodWhereInput>;
  none?: InputMaybe<CarePeriodWhereInput>;
  some?: InputMaybe<CarePeriodWhereInput>;
};

export type CarePeriodOrderByInput = {
  id?: InputMaybe<OrderDirection>;
};

export type CarePeriodRelateToManyForCreateInput = {
  connect?: InputMaybe<Array<CarePeriodWhereUniqueInput>>;
  create?: InputMaybe<Array<CarePeriodCreateInput>>;
};

export type CarePeriodRelateToManyForUpdateInput = {
  connect?: InputMaybe<Array<CarePeriodWhereUniqueInput>>;
  create?: InputMaybe<Array<CarePeriodCreateInput>>;
  disconnect?: InputMaybe<Array<CarePeriodWhereUniqueInput>>;
  set?: InputMaybe<Array<CarePeriodWhereUniqueInput>>;
};

export type CarePeriodRelateToOneForCreateInput = {
  connect?: InputMaybe<CarePeriodWhereUniqueInput>;
  create?: InputMaybe<CarePeriodCreateInput>;
};

export type CarePeriodRelateToOneForUpdateInput = {
  connect?: InputMaybe<CarePeriodWhereUniqueInput>;
  create?: InputMaybe<CarePeriodCreateInput>;
  disconnect?: InputMaybe<Scalars['Boolean']['input']>;
};

export type CarePeriodUpdateArgs = {
  data: CarePeriodUpdateInput;
  where: CarePeriodWhereUniqueInput;
};

export type CarePeriodUpdateInput = {
  careDays?: InputMaybe<CareDayRelateToManyForUpdateInput>;
  event?: InputMaybe<EventRelateToOneForUpdateInput>;
  period?: InputMaybe<PeriodRelateToOneForUpdateInput>;
};

export type CarePeriodWhereInput = {
  AND?: InputMaybe<Array<CarePeriodWhereInput>>;
  NOT?: InputMaybe<Array<CarePeriodWhereInput>>;
  OR?: InputMaybe<Array<CarePeriodWhereInput>>;
  careDays?: InputMaybe<CareDayManyRelationFilter>;
  event?: InputMaybe<EventWhereInput>;
  id?: InputMaybe<IdFilter>;
  period?: InputMaybe<PeriodWhereInput>;
};

export type CarePeriodWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
};

export type Cart = {
  __typename?: 'Cart';
  cartTickets?: Maybe<Array<CartTicket>>;
  cartTicketsCount?: Maybe<Scalars['Int']['output']>;
  createdAt?: Maybe<Scalars['DateTime']['output']>;
  custodian?: Maybe<Custodian>;
  displayName?: Maybe<Scalars['String']['output']>;
  id: Scalars['ID']['output'];
  termExtensionCount?: Maybe<Scalars['Int']['output']>;
  validUntil?: Maybe<Scalars['DateTime']['output']>;
  version?: Maybe<Scalars['Int']['output']>;
};


export type CartCartTicketsArgs = {
  cursor?: InputMaybe<CartTicketWhereUniqueInput>;
  orderBy?: Array<CartTicketOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: CartTicketWhereInput;
};


export type CartCartTicketsCountArgs = {
  where?: CartTicketWhereInput;
};

export type CartCreateInput = {
  cartTickets?: InputMaybe<CartTicketRelateToManyForCreateInput>;
  custodian?: InputMaybe<CustodianRelateToOneForCreateInput>;
};

export type CartOrderByInput = {
  createdAt?: InputMaybe<OrderDirection>;
  id?: InputMaybe<OrderDirection>;
  termExtensionCount?: InputMaybe<OrderDirection>;
  validUntil?: InputMaybe<OrderDirection>;
  version?: InputMaybe<OrderDirection>;
};

export type CartRelateToOneForCreateInput = {
  connect?: InputMaybe<CartWhereUniqueInput>;
  create?: InputMaybe<CartCreateInput>;
};

export type CartRelateToOneForUpdateInput = {
  connect?: InputMaybe<CartWhereUniqueInput>;
  create?: InputMaybe<CartCreateInput>;
  disconnect?: InputMaybe<Scalars['Boolean']['input']>;
};

export type CartTicket = {
  __typename?: 'CartTicket';
  addedToCartAt?: Maybe<Scalars['DateTime']['output']>;
  cart?: Maybe<Cart>;
  child?: Maybe<Child>;
  discountType?: Maybe<DiscountType>;
  displayName?: Maybe<Scalars['String']['output']>;
  eventTicket?: Maybe<EventTicket>;
  id: Scalars['ID']['output'];
};

export type CartTicketCreateInput = {
  cart?: InputMaybe<CartRelateToOneForCreateInput>;
  child?: InputMaybe<ChildRelateToOneForCreateInput>;
  discountType?: InputMaybe<DiscountTypeRelateToOneForCreateInput>;
  eventTicket?: InputMaybe<EventTicketRelateToOneForCreateInput>;
};

export type CartTicketManyRelationFilter = {
  every?: InputMaybe<CartTicketWhereInput>;
  none?: InputMaybe<CartTicketWhereInput>;
  some?: InputMaybe<CartTicketWhereInput>;
};

export type CartTicketOrderByInput = {
  addedToCartAt?: InputMaybe<OrderDirection>;
  id?: InputMaybe<OrderDirection>;
};

export type CartTicketRelateToManyForCreateInput = {
  connect?: InputMaybe<Array<CartTicketWhereUniqueInput>>;
  create?: InputMaybe<Array<CartTicketCreateInput>>;
};

export type CartTicketRelateToManyForUpdateInput = {
  connect?: InputMaybe<Array<CartTicketWhereUniqueInput>>;
  create?: InputMaybe<Array<CartTicketCreateInput>>;
  disconnect?: InputMaybe<Array<CartTicketWhereUniqueInput>>;
  set?: InputMaybe<Array<CartTicketWhereUniqueInput>>;
};

export type CartTicketRelateToOneForCreateInput = {
  connect?: InputMaybe<CartTicketWhereUniqueInput>;
  create?: InputMaybe<CartTicketCreateInput>;
};

export type CartTicketRelateToOneForUpdateInput = {
  connect?: InputMaybe<CartTicketWhereUniqueInput>;
  create?: InputMaybe<CartTicketCreateInput>;
  disconnect?: InputMaybe<Scalars['Boolean']['input']>;
};

export type CartTicketUpdateArgs = {
  data: CartTicketUpdateInput;
  where: CartTicketWhereUniqueInput;
};

export type CartTicketUpdateInput = {
  cart?: InputMaybe<CartRelateToOneForUpdateInput>;
  child?: InputMaybe<ChildRelateToOneForUpdateInput>;
  discountType?: InputMaybe<DiscountTypeRelateToOneForUpdateInput>;
  eventTicket?: InputMaybe<EventTicketRelateToOneForUpdateInput>;
};

export type CartTicketWhereInput = {
  AND?: InputMaybe<Array<CartTicketWhereInput>>;
  NOT?: InputMaybe<Array<CartTicketWhereInput>>;
  OR?: InputMaybe<Array<CartTicketWhereInput>>;
  addedToCartAt?: InputMaybe<DateTimeNullableFilter>;
  cart?: InputMaybe<CartWhereInput>;
  child?: InputMaybe<ChildWhereInput>;
  discountType?: InputMaybe<DiscountTypeWhereInput>;
  eventTicket?: InputMaybe<EventTicketWhereInput>;
  id?: InputMaybe<IdFilter>;
};

export type CartTicketWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
};

export type CartUpdateArgs = {
  data: CartUpdateInput;
  where: CartWhereUniqueInput;
};

export type CartUpdateInput = {
  cartTickets?: InputMaybe<CartTicketRelateToManyForUpdateInput>;
  custodian?: InputMaybe<CustodianRelateToOneForUpdateInput>;
};

export type CartWhereInput = {
  AND?: InputMaybe<Array<CartWhereInput>>;
  NOT?: InputMaybe<Array<CartWhereInput>>;
  OR?: InputMaybe<Array<CartWhereInput>>;
  cartTickets?: InputMaybe<CartTicketManyRelationFilter>;
  createdAt?: InputMaybe<DateTimeNullableFilter>;
  custodian?: InputMaybe<CustodianWhereInput>;
  id?: InputMaybe<IdFilter>;
  termExtensionCount?: InputMaybe<IntFilter>;
  validUntil?: InputMaybe<DateTimeNullableFilter>;
  version?: InputMaybe<IntFilter>;
};

export type CartWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
};

export type ChangePasswordFailed = {
  __typename?: 'ChangePasswordFailed';
  reason: Scalars['String']['output'];
};

export type ChangePasswordResult = ChangePasswordFailed | ChangePasswordSuccess;

export type ChangePasswordSuccess = {
  __typename?: 'ChangePasswordSuccess';
  user: User;
};

export type Child = {
  __typename?: 'Child';
  assistanceRequired?: Maybe<Scalars['Boolean']['output']>;
  custodian?: Maybe<Custodian>;
  dateOfBirth?: Maybe<Scalars['CalendarDay']['output']>;
  dietaryRegulations?: Maybe<Scalars['String']['output']>;
  discounts?: Maybe<Array<Discount>>;
  discountsCount?: Maybe<Scalars['Int']['output']>;
  displayName?: Maybe<Scalars['String']['output']>;
  firstname?: Maybe<Scalars['String']['output']>;
  foodIntolerances?: Maybe<Scalars['String']['output']>;
  gender?: Maybe<Scalars['String']['output']>;
  hasLiabilityInsurance?: Maybe<Scalars['Boolean']['output']>;
  healthInsurer?: Maybe<Scalars['String']['output']>;
  id: Scalars['ID']['output'];
  isDeleted?: Maybe<Scalars['Boolean']['output']>;
  medication?: Maybe<Scalars['String']['output']>;
  miscellaneous?: Maybe<Scalars['String']['output']>;
  physicalImpairment?: Maybe<Scalars['String']['output']>;
  surname?: Maybe<Scalars['String']['output']>;
  tickets?: Maybe<Array<Ticket>>;
  ticketsCount?: Maybe<Scalars['Int']['output']>;
};


export type ChildDiscountsArgs = {
  cursor?: InputMaybe<DiscountWhereUniqueInput>;
  orderBy?: Array<DiscountOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: DiscountWhereInput;
};


export type ChildDiscountsCountArgs = {
  where?: DiscountWhereInput;
};


export type ChildTicketsArgs = {
  cursor?: InputMaybe<TicketWhereUniqueInput>;
  orderBy?: Array<TicketOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: TicketWhereInput;
};


export type ChildTicketsCountArgs = {
  where?: TicketWhereInput;
};

export type ChildCreateInput = {
  assistanceRequired?: InputMaybe<Scalars['Boolean']['input']>;
  custodian?: InputMaybe<CustodianRelateToOneForCreateInput>;
  dateOfBirth?: InputMaybe<Scalars['CalendarDay']['input']>;
  dietaryRegulations?: InputMaybe<Scalars['String']['input']>;
  discounts?: InputMaybe<DiscountRelateToManyForCreateInput>;
  firstname?: InputMaybe<Scalars['String']['input']>;
  foodIntolerances?: InputMaybe<Scalars['String']['input']>;
  gender?: InputMaybe<Scalars['String']['input']>;
  hasLiabilityInsurance?: InputMaybe<Scalars['Boolean']['input']>;
  healthInsurer?: InputMaybe<Scalars['String']['input']>;
  isDeleted?: InputMaybe<Scalars['Boolean']['input']>;
  medication?: InputMaybe<Scalars['String']['input']>;
  miscellaneous?: InputMaybe<Scalars['String']['input']>;
  physicalImpairment?: InputMaybe<Scalars['String']['input']>;
  surname?: InputMaybe<Scalars['String']['input']>;
  tickets?: InputMaybe<TicketRelateToManyForCreateInput>;
};

export type ChildManyRelationFilter = {
  every?: InputMaybe<ChildWhereInput>;
  none?: InputMaybe<ChildWhereInput>;
  some?: InputMaybe<ChildWhereInput>;
};

export type ChildOrderByInput = {
  assistanceRequired?: InputMaybe<OrderDirection>;
  dateOfBirth?: InputMaybe<OrderDirection>;
  dietaryRegulations?: InputMaybe<OrderDirection>;
  firstname?: InputMaybe<OrderDirection>;
  foodIntolerances?: InputMaybe<OrderDirection>;
  gender?: InputMaybe<OrderDirection>;
  hasLiabilityInsurance?: InputMaybe<OrderDirection>;
  healthInsurer?: InputMaybe<OrderDirection>;
  id?: InputMaybe<OrderDirection>;
  isDeleted?: InputMaybe<OrderDirection>;
  medication?: InputMaybe<OrderDirection>;
  miscellaneous?: InputMaybe<OrderDirection>;
  physicalImpairment?: InputMaybe<OrderDirection>;
  surname?: InputMaybe<OrderDirection>;
};

export type ChildRelateToManyForCreateInput = {
  connect?: InputMaybe<Array<ChildWhereUniqueInput>>;
  create?: InputMaybe<Array<ChildCreateInput>>;
};

export type ChildRelateToManyForUpdateInput = {
  connect?: InputMaybe<Array<ChildWhereUniqueInput>>;
  create?: InputMaybe<Array<ChildCreateInput>>;
  disconnect?: InputMaybe<Array<ChildWhereUniqueInput>>;
  set?: InputMaybe<Array<ChildWhereUniqueInput>>;
};

export type ChildRelateToOneForCreateInput = {
  connect?: InputMaybe<ChildWhereUniqueInput>;
  create?: InputMaybe<ChildCreateInput>;
};

export type ChildRelateToOneForUpdateInput = {
  connect?: InputMaybe<ChildWhereUniqueInput>;
  create?: InputMaybe<ChildCreateInput>;
  disconnect?: InputMaybe<Scalars['Boolean']['input']>;
};

export type ChildUpdateArgs = {
  data: ChildUpdateInput;
  where: ChildWhereUniqueInput;
};

export type ChildUpdateInput = {
  assistanceRequired?: InputMaybe<Scalars['Boolean']['input']>;
  custodian?: InputMaybe<CustodianRelateToOneForUpdateInput>;
  dateOfBirth?: InputMaybe<Scalars['CalendarDay']['input']>;
  dietaryRegulations?: InputMaybe<Scalars['String']['input']>;
  discounts?: InputMaybe<DiscountRelateToManyForUpdateInput>;
  firstname?: InputMaybe<Scalars['String']['input']>;
  foodIntolerances?: InputMaybe<Scalars['String']['input']>;
  gender?: InputMaybe<Scalars['String']['input']>;
  hasLiabilityInsurance?: InputMaybe<Scalars['Boolean']['input']>;
  healthInsurer?: InputMaybe<Scalars['String']['input']>;
  isDeleted?: InputMaybe<Scalars['Boolean']['input']>;
  medication?: InputMaybe<Scalars['String']['input']>;
  miscellaneous?: InputMaybe<Scalars['String']['input']>;
  physicalImpairment?: InputMaybe<Scalars['String']['input']>;
  surname?: InputMaybe<Scalars['String']['input']>;
  tickets?: InputMaybe<TicketRelateToManyForUpdateInput>;
};

export type ChildWhereInput = {
  AND?: InputMaybe<Array<ChildWhereInput>>;
  NOT?: InputMaybe<Array<ChildWhereInput>>;
  OR?: InputMaybe<Array<ChildWhereInput>>;
  assistanceRequired?: InputMaybe<BooleanFilter>;
  custodian?: InputMaybe<CustodianWhereInput>;
  dateOfBirth?: InputMaybe<CalendarDayNullableFilter>;
  dietaryRegulations?: InputMaybe<StringFilter>;
  discounts?: InputMaybe<DiscountManyRelationFilter>;
  firstname?: InputMaybe<StringFilter>;
  foodIntolerances?: InputMaybe<StringFilter>;
  gender?: InputMaybe<StringNullableFilter>;
  hasLiabilityInsurance?: InputMaybe<BooleanFilter>;
  healthInsurer?: InputMaybe<StringFilter>;
  id?: InputMaybe<IdFilter>;
  isDeleted?: InputMaybe<BooleanFilter>;
  medication?: InputMaybe<StringFilter>;
  miscellaneous?: InputMaybe<StringFilter>;
  physicalImpairment?: InputMaybe<StringFilter>;
  surname?: InputMaybe<StringFilter>;
  tickets?: InputMaybe<TicketManyRelationFilter>;
};

export type ChildWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
};

export type Contact = {
  __typename?: 'Contact';
  addressCity?: Maybe<Scalars['String']['output']>;
  addressStreet?: Maybe<Scalars['String']['output']>;
  addressZip?: Maybe<Scalars['String']['output']>;
  contactPersonFirstname?: Maybe<Scalars['String']['output']>;
  contactPersonSurname?: Maybe<Scalars['String']['output']>;
  description?: Maybe<Scalars['String']['output']>;
  email?: Maybe<Scalars['String']['output']>;
  hidden?: Maybe<Scalars['Boolean']['output']>;
  id: Scalars['ID']['output'];
  name?: Maybe<Scalars['String']['output']>;
  phone?: Maybe<Scalars['String']['output']>;
};

export type ContactCreateInput = {
  addressCity?: InputMaybe<Scalars['String']['input']>;
  addressStreet?: InputMaybe<Scalars['String']['input']>;
  addressZip?: InputMaybe<Scalars['String']['input']>;
  contactPersonFirstname?: InputMaybe<Scalars['String']['input']>;
  contactPersonSurname?: InputMaybe<Scalars['String']['input']>;
  description?: InputMaybe<Scalars['String']['input']>;
  email?: InputMaybe<Scalars['String']['input']>;
  hidden?: InputMaybe<Scalars['Boolean']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
  phone?: InputMaybe<Scalars['String']['input']>;
};

export type ContactOrderByInput = {
  addressCity?: InputMaybe<OrderDirection>;
  addressStreet?: InputMaybe<OrderDirection>;
  addressZip?: InputMaybe<OrderDirection>;
  contactPersonFirstname?: InputMaybe<OrderDirection>;
  contactPersonSurname?: InputMaybe<OrderDirection>;
  description?: InputMaybe<OrderDirection>;
  email?: InputMaybe<OrderDirection>;
  hidden?: InputMaybe<OrderDirection>;
  id?: InputMaybe<OrderDirection>;
  name?: InputMaybe<OrderDirection>;
  phone?: InputMaybe<OrderDirection>;
};

export type ContactUpdateArgs = {
  data: ContactUpdateInput;
  where: ContactWhereUniqueInput;
};

export type ContactUpdateInput = {
  addressCity?: InputMaybe<Scalars['String']['input']>;
  addressStreet?: InputMaybe<Scalars['String']['input']>;
  addressZip?: InputMaybe<Scalars['String']['input']>;
  contactPersonFirstname?: InputMaybe<Scalars['String']['input']>;
  contactPersonSurname?: InputMaybe<Scalars['String']['input']>;
  description?: InputMaybe<Scalars['String']['input']>;
  email?: InputMaybe<Scalars['String']['input']>;
  hidden?: InputMaybe<Scalars['Boolean']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
  phone?: InputMaybe<Scalars['String']['input']>;
};

export type ContactWhereInput = {
  AND?: InputMaybe<Array<ContactWhereInput>>;
  NOT?: InputMaybe<Array<ContactWhereInput>>;
  OR?: InputMaybe<Array<ContactWhereInput>>;
  addressCity?: InputMaybe<StringFilter>;
  addressStreet?: InputMaybe<StringFilter>;
  addressZip?: InputMaybe<StringFilter>;
  contactPersonFirstname?: InputMaybe<StringFilter>;
  contactPersonSurname?: InputMaybe<StringFilter>;
  description?: InputMaybe<StringFilter>;
  email?: InputMaybe<StringFilter>;
  hidden?: InputMaybe<BooleanFilter>;
  id?: InputMaybe<IdFilter>;
  name?: InputMaybe<StringFilter>;
  phone?: InputMaybe<StringFilter>;
};

export type ContactWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
};

export type CreateInitialUserInput = {
  email?: InputMaybe<Scalars['String']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
  password?: InputMaybe<Scalars['String']['input']>;
};

export type Custodian = {
  __typename?: 'Custodian';
  addressAdditional?: Maybe<Scalars['String']['output']>;
  addressCity?: Maybe<Scalars['String']['output']>;
  addressHouseNumber?: Maybe<Scalars['String']['output']>;
  addressStreet?: Maybe<Scalars['String']['output']>;
  addressZip?: Maybe<Scalars['String']['output']>;
  bookings?: Maybe<Array<Booking>>;
  bookingsCount?: Maybe<Scalars['Int']['output']>;
  children?: Maybe<Array<Child>>;
  childrenCount?: Maybe<Scalars['Int']['output']>;
  discounts?: Maybe<Array<Discount>>;
  discountsCount?: Maybe<Scalars['Int']['output']>;
  displayName?: Maybe<Scalars['String']['output']>;
  emergencyPhone?: Maybe<Scalars['String']['output']>;
  firstname?: Maybe<Scalars['String']['output']>;
  id: Scalars['ID']['output'];
  phone?: Maybe<Scalars['String']['output']>;
  reservations?: Maybe<Array<Reservation>>;
  reservationsCount?: Maybe<Scalars['Int']['output']>;
  salutation?: Maybe<Scalars['String']['output']>;
  surname?: Maybe<Scalars['String']['output']>;
  user?: Maybe<User>;
};


export type CustodianBookingsArgs = {
  cursor?: InputMaybe<BookingWhereUniqueInput>;
  orderBy?: Array<BookingOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: BookingWhereInput;
};


export type CustodianBookingsCountArgs = {
  where?: BookingWhereInput;
};


export type CustodianChildrenArgs = {
  cursor?: InputMaybe<ChildWhereUniqueInput>;
  orderBy?: Array<ChildOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: ChildWhereInput;
};


export type CustodianChildrenCountArgs = {
  where?: ChildWhereInput;
};


export type CustodianDiscountsArgs = {
  cursor?: InputMaybe<DiscountWhereUniqueInput>;
  orderBy?: Array<DiscountOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: DiscountWhereInput;
};


export type CustodianDiscountsCountArgs = {
  where?: DiscountWhereInput;
};


export type CustodianReservationsArgs = {
  cursor?: InputMaybe<ReservationWhereUniqueInput>;
  orderBy?: Array<ReservationOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: ReservationWhereInput;
};


export type CustodianReservationsCountArgs = {
  where?: ReservationWhereInput;
};

export type CustodianCreateInput = {
  addressAdditional?: InputMaybe<Scalars['String']['input']>;
  addressCity?: InputMaybe<Scalars['String']['input']>;
  addressHouseNumber?: InputMaybe<Scalars['String']['input']>;
  addressStreet?: InputMaybe<Scalars['String']['input']>;
  addressZip?: InputMaybe<Scalars['String']['input']>;
  bookings?: InputMaybe<BookingRelateToManyForCreateInput>;
  children?: InputMaybe<ChildRelateToManyForCreateInput>;
  discounts?: InputMaybe<DiscountRelateToManyForCreateInput>;
  emergencyPhone?: InputMaybe<Scalars['String']['input']>;
  firstname?: InputMaybe<Scalars['String']['input']>;
  phone?: InputMaybe<Scalars['String']['input']>;
  reservations?: InputMaybe<ReservationRelateToManyForCreateInput>;
  salutation?: InputMaybe<Scalars['String']['input']>;
  surname?: InputMaybe<Scalars['String']['input']>;
  user?: InputMaybe<UserRelateToOneForCreateInput>;
};

export type CustodianOrderByInput = {
  addressAdditional?: InputMaybe<OrderDirection>;
  addressCity?: InputMaybe<OrderDirection>;
  addressHouseNumber?: InputMaybe<OrderDirection>;
  addressStreet?: InputMaybe<OrderDirection>;
  addressZip?: InputMaybe<OrderDirection>;
  emergencyPhone?: InputMaybe<OrderDirection>;
  firstname?: InputMaybe<OrderDirection>;
  id?: InputMaybe<OrderDirection>;
  phone?: InputMaybe<OrderDirection>;
  salutation?: InputMaybe<OrderDirection>;
  surname?: InputMaybe<OrderDirection>;
};

export type CustodianRelateToOneForCreateInput = {
  connect?: InputMaybe<CustodianWhereUniqueInput>;
  create?: InputMaybe<CustodianCreateInput>;
};

export type CustodianRelateToOneForUpdateInput = {
  connect?: InputMaybe<CustodianWhereUniqueInput>;
  create?: InputMaybe<CustodianCreateInput>;
  disconnect?: InputMaybe<Scalars['Boolean']['input']>;
};

export type CustodianUpdateArgs = {
  data: CustodianUpdateInput;
  where: CustodianWhereUniqueInput;
};

export type CustodianUpdateInput = {
  addressAdditional?: InputMaybe<Scalars['String']['input']>;
  addressCity?: InputMaybe<Scalars['String']['input']>;
  addressHouseNumber?: InputMaybe<Scalars['String']['input']>;
  addressStreet?: InputMaybe<Scalars['String']['input']>;
  addressZip?: InputMaybe<Scalars['String']['input']>;
  bookings?: InputMaybe<BookingRelateToManyForUpdateInput>;
  children?: InputMaybe<ChildRelateToManyForUpdateInput>;
  discounts?: InputMaybe<DiscountRelateToManyForUpdateInput>;
  emergencyPhone?: InputMaybe<Scalars['String']['input']>;
  firstname?: InputMaybe<Scalars['String']['input']>;
  phone?: InputMaybe<Scalars['String']['input']>;
  reservations?: InputMaybe<ReservationRelateToManyForUpdateInput>;
  salutation?: InputMaybe<Scalars['String']['input']>;
  surname?: InputMaybe<Scalars['String']['input']>;
  user?: InputMaybe<UserRelateToOneForUpdateInput>;
};

export type CustodianWhereInput = {
  AND?: InputMaybe<Array<CustodianWhereInput>>;
  NOT?: InputMaybe<Array<CustodianWhereInput>>;
  OR?: InputMaybe<Array<CustodianWhereInput>>;
  addressAdditional?: InputMaybe<StringFilter>;
  addressCity?: InputMaybe<StringFilter>;
  addressHouseNumber?: InputMaybe<StringFilter>;
  addressStreet?: InputMaybe<StringFilter>;
  addressZip?: InputMaybe<StringFilter>;
  bookings?: InputMaybe<BookingManyRelationFilter>;
  children?: InputMaybe<ChildManyRelationFilter>;
  discounts?: InputMaybe<DiscountManyRelationFilter>;
  emergencyPhone?: InputMaybe<StringFilter>;
  firstname?: InputMaybe<StringFilter>;
  id?: InputMaybe<IdFilter>;
  phone?: InputMaybe<StringFilter>;
  reservations?: InputMaybe<ReservationManyRelationFilter>;
  salutation?: InputMaybe<StringNullableFilter>;
  surname?: InputMaybe<StringFilter>;
  user?: InputMaybe<UserWhereInput>;
};

export type CustodianWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
};

export type DateTimeFilter = {
  equals?: InputMaybe<Scalars['DateTime']['input']>;
  gt?: InputMaybe<Scalars['DateTime']['input']>;
  gte?: InputMaybe<Scalars['DateTime']['input']>;
  in?: InputMaybe<Array<Scalars['DateTime']['input']>>;
  lt?: InputMaybe<Scalars['DateTime']['input']>;
  lte?: InputMaybe<Scalars['DateTime']['input']>;
  not?: InputMaybe<DateTimeFilter>;
  notIn?: InputMaybe<Array<Scalars['DateTime']['input']>>;
};

export type DateTimeNullableFilter = {
  equals?: InputMaybe<Scalars['DateTime']['input']>;
  gt?: InputMaybe<Scalars['DateTime']['input']>;
  gte?: InputMaybe<Scalars['DateTime']['input']>;
  in?: InputMaybe<Array<Scalars['DateTime']['input']>>;
  lt?: InputMaybe<Scalars['DateTime']['input']>;
  lte?: InputMaybe<Scalars['DateTime']['input']>;
  not?: InputMaybe<DateTimeNullableFilter>;
  notIn?: InputMaybe<Array<Scalars['DateTime']['input']>>;
};

export type Discount = {
  __typename?: 'Discount';
  child?: Maybe<Child>;
  custodian?: Maybe<Custodian>;
  displayName?: Maybe<Scalars['String']['output']>;
  id: Scalars['ID']['output'];
  rejectionNote?: Maybe<Scalars['String']['output']>;
  rejectionReason?: Maybe<DiscountRejectionReason>;
  reminderDate?: Maybe<Scalars['DateTime']['output']>;
  season?: Maybe<Season>;
  status?: Maybe<Scalars['String']['output']>;
  submissionDate?: Maybe<Scalars['DateTime']['output']>;
  type?: Maybe<DiscountType>;
  validUntil?: Maybe<Scalars['DateTime']['output']>;
  vouchers?: Maybe<Array<DiscountVoucher>>;
  vouchersCount?: Maybe<Scalars['Int']['output']>;
};


export type DiscountVouchersArgs = {
  cursor?: InputMaybe<DiscountVoucherWhereUniqueInput>;
  orderBy?: Array<DiscountVoucherOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: DiscountVoucherWhereInput;
};


export type DiscountVouchersCountArgs = {
  where?: DiscountVoucherWhereInput;
};

export type DiscountCreateInput = {
  child?: InputMaybe<ChildRelateToOneForCreateInput>;
  custodian?: InputMaybe<CustodianRelateToOneForCreateInput>;
  rejectionNote?: InputMaybe<Scalars['String']['input']>;
  rejectionReason?: InputMaybe<DiscountRejectionReasonRelateToOneForCreateInput>;
  reminderDate?: InputMaybe<Scalars['DateTime']['input']>;
  season?: InputMaybe<SeasonRelateToOneForCreateInput>;
  status?: InputMaybe<Scalars['String']['input']>;
  submissionDate?: InputMaybe<Scalars['DateTime']['input']>;
  type?: InputMaybe<DiscountTypeRelateToOneForCreateInput>;
  validUntil?: InputMaybe<Scalars['DateTime']['input']>;
  vouchers?: InputMaybe<DiscountVoucherRelateToManyForCreateInput>;
};

export type DiscountManyRelationFilter = {
  every?: InputMaybe<DiscountWhereInput>;
  none?: InputMaybe<DiscountWhereInput>;
  some?: InputMaybe<DiscountWhereInput>;
};

export type DiscountOrderByInput = {
  id?: InputMaybe<OrderDirection>;
  rejectionNote?: InputMaybe<OrderDirection>;
  reminderDate?: InputMaybe<OrderDirection>;
  status?: InputMaybe<OrderDirection>;
  submissionDate?: InputMaybe<OrderDirection>;
  validUntil?: InputMaybe<OrderDirection>;
};

export type DiscountRejectionReason = {
  __typename?: 'DiscountRejectionReason';
  description?: Maybe<Scalars['String']['output']>;
  id: Scalars['ID']['output'];
  reason?: Maybe<Scalars['String']['output']>;
};

export type DiscountRejectionReasonCreateInput = {
  description?: InputMaybe<Scalars['String']['input']>;
  reason?: InputMaybe<Scalars['String']['input']>;
};

export type DiscountRejectionReasonOrderByInput = {
  description?: InputMaybe<OrderDirection>;
  id?: InputMaybe<OrderDirection>;
  reason?: InputMaybe<OrderDirection>;
};

export type DiscountRejectionReasonRelateToOneForCreateInput = {
  connect?: InputMaybe<DiscountRejectionReasonWhereUniqueInput>;
  create?: InputMaybe<DiscountRejectionReasonCreateInput>;
};

export type DiscountRejectionReasonRelateToOneForUpdateInput = {
  connect?: InputMaybe<DiscountRejectionReasonWhereUniqueInput>;
  create?: InputMaybe<DiscountRejectionReasonCreateInput>;
  disconnect?: InputMaybe<Scalars['Boolean']['input']>;
};

export type DiscountRejectionReasonUpdateArgs = {
  data: DiscountRejectionReasonUpdateInput;
  where: DiscountRejectionReasonWhereUniqueInput;
};

export type DiscountRejectionReasonUpdateInput = {
  description?: InputMaybe<Scalars['String']['input']>;
  reason?: InputMaybe<Scalars['String']['input']>;
};

export type DiscountRejectionReasonWhereInput = {
  AND?: InputMaybe<Array<DiscountRejectionReasonWhereInput>>;
  NOT?: InputMaybe<Array<DiscountRejectionReasonWhereInput>>;
  OR?: InputMaybe<Array<DiscountRejectionReasonWhereInput>>;
  description?: InputMaybe<StringFilter>;
  id?: InputMaybe<IdFilter>;
  reason?: InputMaybe<StringFilter>;
};

export type DiscountRejectionReasonWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
};

export type DiscountRelateToManyForCreateInput = {
  connect?: InputMaybe<Array<DiscountWhereUniqueInput>>;
  create?: InputMaybe<Array<DiscountCreateInput>>;
};

export type DiscountRelateToManyForUpdateInput = {
  connect?: InputMaybe<Array<DiscountWhereUniqueInput>>;
  create?: InputMaybe<Array<DiscountCreateInput>>;
  disconnect?: InputMaybe<Array<DiscountWhereUniqueInput>>;
  set?: InputMaybe<Array<DiscountWhereUniqueInput>>;
};

export type DiscountRelateToOneForCreateInput = {
  connect?: InputMaybe<DiscountWhereUniqueInput>;
  create?: InputMaybe<DiscountCreateInput>;
};

export type DiscountRelateToOneForUpdateInput = {
  connect?: InputMaybe<DiscountWhereUniqueInput>;
  create?: InputMaybe<DiscountCreateInput>;
  disconnect?: InputMaybe<Scalars['Boolean']['input']>;
};

export type DiscountType = {
  __typename?: 'DiscountType';
  amountChildren?: Maybe<Scalars['Int']['output']>;
  amountChildrenDescription?: Maybe<Scalars['String']['output']>;
  amountEvents?: Maybe<Scalars['Int']['output']>;
  amountEventsDescription?: Maybe<Scalars['String']['output']>;
  description?: Maybe<Scalars['String']['output']>;
  discountPercent?: Maybe<Scalars['Float']['output']>;
  id: Scalars['ID']['output'];
  name?: Maybe<Scalars['String']['output']>;
};

export type DiscountTypeCreateInput = {
  amountChildren?: InputMaybe<Scalars['Int']['input']>;
  amountChildrenDescription?: InputMaybe<Scalars['String']['input']>;
  amountEvents?: InputMaybe<Scalars['Int']['input']>;
  amountEventsDescription?: InputMaybe<Scalars['String']['input']>;
  description?: InputMaybe<Scalars['String']['input']>;
  discountPercent?: InputMaybe<Scalars['Float']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
};

export type DiscountTypeOrderByInput = {
  amountChildren?: InputMaybe<OrderDirection>;
  amountChildrenDescription?: InputMaybe<OrderDirection>;
  amountEvents?: InputMaybe<OrderDirection>;
  amountEventsDescription?: InputMaybe<OrderDirection>;
  description?: InputMaybe<OrderDirection>;
  discountPercent?: InputMaybe<OrderDirection>;
  id?: InputMaybe<OrderDirection>;
  name?: InputMaybe<OrderDirection>;
};

export type DiscountTypeRelateToOneForCreateInput = {
  connect?: InputMaybe<DiscountTypeWhereUniqueInput>;
  create?: InputMaybe<DiscountTypeCreateInput>;
};

export type DiscountTypeRelateToOneForUpdateInput = {
  connect?: InputMaybe<DiscountTypeWhereUniqueInput>;
  create?: InputMaybe<DiscountTypeCreateInput>;
  disconnect?: InputMaybe<Scalars['Boolean']['input']>;
};

export type DiscountTypeUpdateArgs = {
  data: DiscountTypeUpdateInput;
  where: DiscountTypeWhereUniqueInput;
};

export type DiscountTypeUpdateInput = {
  amountChildren?: InputMaybe<Scalars['Int']['input']>;
  amountChildrenDescription?: InputMaybe<Scalars['String']['input']>;
  amountEvents?: InputMaybe<Scalars['Int']['input']>;
  amountEventsDescription?: InputMaybe<Scalars['String']['input']>;
  description?: InputMaybe<Scalars['String']['input']>;
  discountPercent?: InputMaybe<Scalars['Float']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
};

export type DiscountTypeWhereInput = {
  AND?: InputMaybe<Array<DiscountTypeWhereInput>>;
  NOT?: InputMaybe<Array<DiscountTypeWhereInput>>;
  OR?: InputMaybe<Array<DiscountTypeWhereInput>>;
  amountChildren?: InputMaybe<IntFilter>;
  amountChildrenDescription?: InputMaybe<StringFilter>;
  amountEvents?: InputMaybe<IntFilter>;
  amountEventsDescription?: InputMaybe<StringFilter>;
  description?: InputMaybe<StringFilter>;
  discountPercent?: InputMaybe<FloatFilter>;
  id?: InputMaybe<IdFilter>;
  name?: InputMaybe<StringFilter>;
};

export type DiscountTypeWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
};

export type DiscountUpdateArgs = {
  data: DiscountUpdateInput;
  where: DiscountWhereUniqueInput;
};

export type DiscountUpdateInput = {
  child?: InputMaybe<ChildRelateToOneForUpdateInput>;
  custodian?: InputMaybe<CustodianRelateToOneForUpdateInput>;
  rejectionNote?: InputMaybe<Scalars['String']['input']>;
  rejectionReason?: InputMaybe<DiscountRejectionReasonRelateToOneForUpdateInput>;
  reminderDate?: InputMaybe<Scalars['DateTime']['input']>;
  season?: InputMaybe<SeasonRelateToOneForUpdateInput>;
  status?: InputMaybe<Scalars['String']['input']>;
  submissionDate?: InputMaybe<Scalars['DateTime']['input']>;
  type?: InputMaybe<DiscountTypeRelateToOneForUpdateInput>;
  validUntil?: InputMaybe<Scalars['DateTime']['input']>;
  vouchers?: InputMaybe<DiscountVoucherRelateToManyForUpdateInput>;
};

export type DiscountVoucher = {
  __typename?: 'DiscountVoucher';
  discount?: Maybe<Discount>;
  displayName?: Maybe<Scalars['String']['output']>;
  id: Scalars['ID']['output'];
  redemptionDate?: Maybe<Scalars['DateTime']['output']>;
  ticket?: Maybe<Ticket>;
};

export type DiscountVoucherCreateInput = {
  discount?: InputMaybe<DiscountRelateToOneForCreateInput>;
  redemptionDate?: InputMaybe<Scalars['DateTime']['input']>;
  ticket?: InputMaybe<TicketRelateToOneForCreateInput>;
};

export type DiscountVoucherManyRelationFilter = {
  every?: InputMaybe<DiscountVoucherWhereInput>;
  none?: InputMaybe<DiscountVoucherWhereInput>;
  some?: InputMaybe<DiscountVoucherWhereInput>;
};

export type DiscountVoucherOrderByInput = {
  id?: InputMaybe<OrderDirection>;
  redemptionDate?: InputMaybe<OrderDirection>;
};

export type DiscountVoucherRelateToManyForCreateInput = {
  connect?: InputMaybe<Array<DiscountVoucherWhereUniqueInput>>;
  create?: InputMaybe<Array<DiscountVoucherCreateInput>>;
};

export type DiscountVoucherRelateToManyForUpdateInput = {
  connect?: InputMaybe<Array<DiscountVoucherWhereUniqueInput>>;
  create?: InputMaybe<Array<DiscountVoucherCreateInput>>;
  disconnect?: InputMaybe<Array<DiscountVoucherWhereUniqueInput>>;
  set?: InputMaybe<Array<DiscountVoucherWhereUniqueInput>>;
};

export type DiscountVoucherRelateToOneForCreateInput = {
  connect?: InputMaybe<DiscountVoucherWhereUniqueInput>;
  create?: InputMaybe<DiscountVoucherCreateInput>;
};

export type DiscountVoucherRelateToOneForUpdateInput = {
  connect?: InputMaybe<DiscountVoucherWhereUniqueInput>;
  create?: InputMaybe<DiscountVoucherCreateInput>;
  disconnect?: InputMaybe<Scalars['Boolean']['input']>;
};

export type DiscountVoucherUpdateArgs = {
  data: DiscountVoucherUpdateInput;
  where: DiscountVoucherWhereUniqueInput;
};

export type DiscountVoucherUpdateInput = {
  discount?: InputMaybe<DiscountRelateToOneForUpdateInput>;
  redemptionDate?: InputMaybe<Scalars['DateTime']['input']>;
  ticket?: InputMaybe<TicketRelateToOneForUpdateInput>;
};

export type DiscountVoucherWhereInput = {
  AND?: InputMaybe<Array<DiscountVoucherWhereInput>>;
  NOT?: InputMaybe<Array<DiscountVoucherWhereInput>>;
  OR?: InputMaybe<Array<DiscountVoucherWhereInput>>;
  discount?: InputMaybe<DiscountWhereInput>;
  id?: InputMaybe<IdFilter>;
  redemptionDate?: InputMaybe<DateTimeNullableFilter>;
  ticket?: InputMaybe<TicketWhereInput>;
};

export type DiscountVoucherWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
};

export type DiscountWhereInput = {
  AND?: InputMaybe<Array<DiscountWhereInput>>;
  NOT?: InputMaybe<Array<DiscountWhereInput>>;
  OR?: InputMaybe<Array<DiscountWhereInput>>;
  child?: InputMaybe<ChildWhereInput>;
  custodian?: InputMaybe<CustodianWhereInput>;
  id?: InputMaybe<IdFilter>;
  rejectionNote?: InputMaybe<StringFilter>;
  rejectionReason?: InputMaybe<DiscountRejectionReasonWhereInput>;
  reminderDate?: InputMaybe<DateTimeNullableFilter>;
  season?: InputMaybe<SeasonWhereInput>;
  status?: InputMaybe<StringNullableFilter>;
  submissionDate?: InputMaybe<DateTimeNullableFilter>;
  type?: InputMaybe<DiscountTypeWhereInput>;
  validUntil?: InputMaybe<DateTimeNullableFilter>;
  vouchers?: InputMaybe<DiscountVoucherManyRelationFilter>;
};

export type DiscountWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
};

export type EarlyAccessCode = {
  __typename?: 'EarlyAccessCode';
  code?: Maybe<Scalars['String']['output']>;
  id: Scalars['ID']['output'];
  notificationDate?: Maybe<Scalars['DateTime']['output']>;
  partner?: Maybe<Partner>;
  season?: Maybe<Season>;
};

export type EarlyAccessCodeCreateInput = {
  code?: InputMaybe<Scalars['String']['input']>;
  notificationDate?: InputMaybe<Scalars['DateTime']['input']>;
  partner?: InputMaybe<PartnerRelateToOneForCreateInput>;
  season?: InputMaybe<SeasonRelateToOneForCreateInput>;
};

export type EarlyAccessCodeManyRelationFilter = {
  every?: InputMaybe<EarlyAccessCodeWhereInput>;
  none?: InputMaybe<EarlyAccessCodeWhereInput>;
  some?: InputMaybe<EarlyAccessCodeWhereInput>;
};

export type EarlyAccessCodeOrderByInput = {
  code?: InputMaybe<OrderDirection>;
  id?: InputMaybe<OrderDirection>;
  notificationDate?: InputMaybe<OrderDirection>;
};

export type EarlyAccessCodeRelateToManyForCreateInput = {
  connect?: InputMaybe<Array<EarlyAccessCodeWhereUniqueInput>>;
  create?: InputMaybe<Array<EarlyAccessCodeCreateInput>>;
};

export type EarlyAccessCodeRelateToManyForUpdateInput = {
  connect?: InputMaybe<Array<EarlyAccessCodeWhereUniqueInput>>;
  create?: InputMaybe<Array<EarlyAccessCodeCreateInput>>;
  disconnect?: InputMaybe<Array<EarlyAccessCodeWhereUniqueInput>>;
  set?: InputMaybe<Array<EarlyAccessCodeWhereUniqueInput>>;
};

export type EarlyAccessCodeUpdateArgs = {
  data: EarlyAccessCodeUpdateInput;
  where: EarlyAccessCodeWhereUniqueInput;
};

export type EarlyAccessCodeUpdateInput = {
  code?: InputMaybe<Scalars['String']['input']>;
  notificationDate?: InputMaybe<Scalars['DateTime']['input']>;
  partner?: InputMaybe<PartnerRelateToOneForUpdateInput>;
  season?: InputMaybe<SeasonRelateToOneForUpdateInput>;
};

export type EarlyAccessCodeVoucher = {
  __typename?: 'EarlyAccessCodeVoucher';
  code?: Maybe<Scalars['String']['output']>;
  custodian?: Maybe<Custodian>;
  id: Scalars['ID']['output'];
  season?: Maybe<Season>;
};

export type EarlyAccessCodeVoucherCreateInput = {
  code?: InputMaybe<Scalars['String']['input']>;
  custodian?: InputMaybe<CustodianRelateToOneForCreateInput>;
  season?: InputMaybe<SeasonRelateToOneForCreateInput>;
};

export type EarlyAccessCodeVoucherOrderByInput = {
  code?: InputMaybe<OrderDirection>;
  id?: InputMaybe<OrderDirection>;
};

export type EarlyAccessCodeVoucherUpdateArgs = {
  data: EarlyAccessCodeVoucherUpdateInput;
  where: EarlyAccessCodeVoucherWhereUniqueInput;
};

export type EarlyAccessCodeVoucherUpdateInput = {
  code?: InputMaybe<Scalars['String']['input']>;
  custodian?: InputMaybe<CustodianRelateToOneForUpdateInput>;
  season?: InputMaybe<SeasonRelateToOneForUpdateInput>;
};

export type EarlyAccessCodeVoucherWhereInput = {
  AND?: InputMaybe<Array<EarlyAccessCodeVoucherWhereInput>>;
  NOT?: InputMaybe<Array<EarlyAccessCodeVoucherWhereInput>>;
  OR?: InputMaybe<Array<EarlyAccessCodeVoucherWhereInput>>;
  code?: InputMaybe<StringFilter>;
  custodian?: InputMaybe<CustodianWhereInput>;
  id?: InputMaybe<IdFilter>;
  season?: InputMaybe<SeasonWhereInput>;
};

export type EarlyAccessCodeVoucherWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
};

export type EarlyAccessCodeWhereInput = {
  AND?: InputMaybe<Array<EarlyAccessCodeWhereInput>>;
  NOT?: InputMaybe<Array<EarlyAccessCodeWhereInput>>;
  OR?: InputMaybe<Array<EarlyAccessCodeWhereInput>>;
  code?: InputMaybe<StringFilter>;
  id?: InputMaybe<IdFilter>;
  notificationDate?: InputMaybe<DateTimeNullableFilter>;
  partner?: InputMaybe<PartnerWhereInput>;
  season?: InputMaybe<SeasonWhereInput>;
};

export type EarlyAccessCodeWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
};

export type EmailTemplate = {
  __typename?: 'EmailTemplate';
  bccRecipients?: Maybe<Array<User>>;
  bccRecipientsCount?: Maybe<Scalars['Int']['output']>;
  content?: Maybe<Scalars['String']['output']>;
  id: Scalars['ID']['output'];
  role?: Maybe<Scalars['String']['output']>;
  subject?: Maybe<Scalars['String']['output']>;
};


export type EmailTemplateBccRecipientsArgs = {
  cursor?: InputMaybe<UserWhereUniqueInput>;
  orderBy?: Array<UserOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: UserWhereInput;
};


export type EmailTemplateBccRecipientsCountArgs = {
  where?: UserWhereInput;
};

export type EmailTemplateCreateInput = {
  bccRecipients?: InputMaybe<UserRelateToManyForCreateInput>;
  content?: InputMaybe<Scalars['String']['input']>;
  role?: InputMaybe<Scalars['String']['input']>;
  subject?: InputMaybe<Scalars['String']['input']>;
};

export type EmailTemplateOrderByInput = {
  content?: InputMaybe<OrderDirection>;
  id?: InputMaybe<OrderDirection>;
  role?: InputMaybe<OrderDirection>;
  subject?: InputMaybe<OrderDirection>;
};

export type EmailTemplateUpdateArgs = {
  data: EmailTemplateUpdateInput;
  where: EmailTemplateWhereUniqueInput;
};

export type EmailTemplateUpdateInput = {
  bccRecipients?: InputMaybe<UserRelateToManyForUpdateInput>;
  content?: InputMaybe<Scalars['String']['input']>;
  role?: InputMaybe<Scalars['String']['input']>;
  subject?: InputMaybe<Scalars['String']['input']>;
};

export type EmailTemplateWhereInput = {
  AND?: InputMaybe<Array<EmailTemplateWhereInput>>;
  NOT?: InputMaybe<Array<EmailTemplateWhereInput>>;
  OR?: InputMaybe<Array<EmailTemplateWhereInput>>;
  bccRecipients?: InputMaybe<UserManyRelationFilter>;
  content?: InputMaybe<StringFilter>;
  id?: InputMaybe<IdFilter>;
  role?: InputMaybe<StringFilter>;
  subject?: InputMaybe<StringFilter>;
};

export type EmailTemplateWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
  role?: InputMaybe<Scalars['String']['input']>;
};

export type Event = {
  __typename?: 'Event';
  ageMaximum?: Maybe<Scalars['Int']['output']>;
  ageMinimum?: Maybe<Scalars['Int']['output']>;
  availableTicketCount?: Maybe<Scalars['Int']['output']>;
  bookedTicketCount?: Maybe<Scalars['Int']['output']>;
  carePeriodAlternative?: Maybe<CarePeriod>;
  carePeriodCommitted?: Maybe<CarePeriod>;
  carePeriodDesired?: Maybe<CarePeriod>;
  careTimeEnd?: Maybe<Scalars['String']['output']>;
  careTimeStart?: Maybe<Scalars['String']['output']>;
  categories?: Maybe<Array<EventCategory>>;
  categoriesCount?: Maybe<Scalars['Int']['output']>;
  contactPersonFirstname?: Maybe<Scalars['String']['output']>;
  contactPersonSalutation?: Maybe<Scalars['String']['output']>;
  contactPersonSurname?: Maybe<Scalars['String']['output']>;
  costs?: Maybe<Scalars['Float']['output']>;
  description?: Maybe<Scalars['String']['output']>;
  emergencyPhone?: Maybe<Scalars['String']['output']>;
  hints?: Maybe<Scalars['String']['output']>;
  id: Scalars['ID']['output'];
  locationCity?: Maybe<Scalars['String']['output']>;
  locationStreet?: Maybe<Scalars['String']['output']>;
  locationZipcode?: Maybe<Scalars['String']['output']>;
  markedAsFinishedAt?: Maybe<Scalars['DateTime']['output']>;
  meals?: Maybe<Scalars['String']['output']>;
  name?: Maybe<Scalars['String']['output']>;
  organizer?: Maybe<Organizer>;
  participantLimit?: Maybe<Scalars['Int']['output']>;
  participantMinimum?: Maybe<Scalars['Int']['output']>;
  picture?: Maybe<ImageFieldOutput>;
  pictureRights?: Maybe<Scalars['String']['output']>;
  registrationDeadline?: Maybe<Scalars['CalendarDay']['output']>;
  season?: Maybe<Season>;
  status?: Maybe<Scalars['String']['output']>;
  waitingListLimit?: Maybe<Scalars['Int']['output']>;
};


export type EventAvailableTicketCountArgs = {
  type?: InputMaybe<TicketType>;
};


export type EventBookedTicketCountArgs = {
  type?: InputMaybe<TicketType>;
};


export type EventCategoriesArgs = {
  cursor?: InputMaybe<EventCategoryWhereUniqueInput>;
  orderBy?: Array<EventCategoryOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: EventCategoryWhereInput;
};


export type EventCategoriesCountArgs = {
  where?: EventCategoryWhereInput;
};

export type EventCategory = {
  __typename?: 'EventCategory';
  events?: Maybe<Array<Event>>;
  eventsCount?: Maybe<Scalars['Int']['output']>;
  id: Scalars['ID']['output'];
  name?: Maybe<Scalars['String']['output']>;
};


export type EventCategoryEventsArgs = {
  cursor?: InputMaybe<EventWhereUniqueInput>;
  orderBy?: Array<EventOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: EventWhereInput;
};


export type EventCategoryEventsCountArgs = {
  where?: EventWhereInput;
};

export type EventCategoryCreateInput = {
  events?: InputMaybe<EventRelateToManyForCreateInput>;
  name?: InputMaybe<Scalars['String']['input']>;
};

export type EventCategoryManyRelationFilter = {
  every?: InputMaybe<EventCategoryWhereInput>;
  none?: InputMaybe<EventCategoryWhereInput>;
  some?: InputMaybe<EventCategoryWhereInput>;
};

export type EventCategoryOrderByInput = {
  id?: InputMaybe<OrderDirection>;
  name?: InputMaybe<OrderDirection>;
};

export type EventCategoryRelateToManyForCreateInput = {
  connect?: InputMaybe<Array<EventCategoryWhereUniqueInput>>;
  create?: InputMaybe<Array<EventCategoryCreateInput>>;
};

export type EventCategoryRelateToManyForUpdateInput = {
  connect?: InputMaybe<Array<EventCategoryWhereUniqueInput>>;
  create?: InputMaybe<Array<EventCategoryCreateInput>>;
  disconnect?: InputMaybe<Array<EventCategoryWhereUniqueInput>>;
  set?: InputMaybe<Array<EventCategoryWhereUniqueInput>>;
};

export type EventCategoryUpdateArgs = {
  data: EventCategoryUpdateInput;
  where: EventCategoryWhereUniqueInput;
};

export type EventCategoryUpdateInput = {
  events?: InputMaybe<EventRelateToManyForUpdateInput>;
  name?: InputMaybe<Scalars['String']['input']>;
};

export type EventCategoryWhereInput = {
  AND?: InputMaybe<Array<EventCategoryWhereInput>>;
  NOT?: InputMaybe<Array<EventCategoryWhereInput>>;
  OR?: InputMaybe<Array<EventCategoryWhereInput>>;
  events?: InputMaybe<EventManyRelationFilter>;
  id?: InputMaybe<IdFilter>;
  name?: InputMaybe<StringFilter>;
};

export type EventCategoryWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
};

export type EventCreateInput = {
  ageMaximum?: InputMaybe<Scalars['Int']['input']>;
  ageMinimum?: InputMaybe<Scalars['Int']['input']>;
  carePeriodAlternative?: InputMaybe<CarePeriodRelateToOneForCreateInput>;
  carePeriodCommitted?: InputMaybe<CarePeriodRelateToOneForCreateInput>;
  carePeriodDesired?: InputMaybe<CarePeriodRelateToOneForCreateInput>;
  careTimeEnd?: InputMaybe<Scalars['String']['input']>;
  careTimeStart?: InputMaybe<Scalars['String']['input']>;
  categories?: InputMaybe<EventCategoryRelateToManyForCreateInput>;
  contactPersonFirstname?: InputMaybe<Scalars['String']['input']>;
  contactPersonSalutation?: InputMaybe<Scalars['String']['input']>;
  contactPersonSurname?: InputMaybe<Scalars['String']['input']>;
  costs?: InputMaybe<Scalars['Float']['input']>;
  description?: InputMaybe<Scalars['String']['input']>;
  emergencyPhone?: InputMaybe<Scalars['String']['input']>;
  hints?: InputMaybe<Scalars['String']['input']>;
  locationCity?: InputMaybe<Scalars['String']['input']>;
  locationStreet?: InputMaybe<Scalars['String']['input']>;
  locationZipcode?: InputMaybe<Scalars['String']['input']>;
  markedAsFinishedAt?: InputMaybe<Scalars['DateTime']['input']>;
  meals?: InputMaybe<Scalars['String']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
  organizer?: InputMaybe<OrganizerRelateToOneForCreateInput>;
  participantLimit?: InputMaybe<Scalars['Int']['input']>;
  participantMinimum?: InputMaybe<Scalars['Int']['input']>;
  picture?: InputMaybe<ImageFieldInput>;
  pictureRights?: InputMaybe<Scalars['String']['input']>;
  registrationDeadline?: InputMaybe<Scalars['CalendarDay']['input']>;
  season?: InputMaybe<SeasonRelateToOneForCreateInput>;
  status?: InputMaybe<Scalars['String']['input']>;
  waitingListLimit?: InputMaybe<Scalars['Int']['input']>;
};

export type EventManyRelationFilter = {
  every?: InputMaybe<EventWhereInput>;
  none?: InputMaybe<EventWhereInput>;
  some?: InputMaybe<EventWhereInput>;
};

export type EventOrderByInput = {
  ageMaximum?: InputMaybe<OrderDirection>;
  ageMinimum?: InputMaybe<OrderDirection>;
  careTimeEnd?: InputMaybe<OrderDirection>;
  careTimeStart?: InputMaybe<OrderDirection>;
  contactPersonFirstname?: InputMaybe<OrderDirection>;
  contactPersonSalutation?: InputMaybe<OrderDirection>;
  contactPersonSurname?: InputMaybe<OrderDirection>;
  costs?: InputMaybe<OrderDirection>;
  description?: InputMaybe<OrderDirection>;
  emergencyPhone?: InputMaybe<OrderDirection>;
  hints?: InputMaybe<OrderDirection>;
  id?: InputMaybe<OrderDirection>;
  locationCity?: InputMaybe<OrderDirection>;
  locationStreet?: InputMaybe<OrderDirection>;
  locationZipcode?: InputMaybe<OrderDirection>;
  markedAsFinishedAt?: InputMaybe<OrderDirection>;
  meals?: InputMaybe<OrderDirection>;
  name?: InputMaybe<OrderDirection>;
  participantLimit?: InputMaybe<OrderDirection>;
  participantMinimum?: InputMaybe<OrderDirection>;
  pictureRights?: InputMaybe<OrderDirection>;
  registrationDeadline?: InputMaybe<OrderDirection>;
  status?: InputMaybe<OrderDirection>;
  waitingListLimit?: InputMaybe<OrderDirection>;
};

export type EventRelateToManyForCreateInput = {
  connect?: InputMaybe<Array<EventWhereUniqueInput>>;
  create?: InputMaybe<Array<EventCreateInput>>;
};

export type EventRelateToManyForUpdateInput = {
  connect?: InputMaybe<Array<EventWhereUniqueInput>>;
  create?: InputMaybe<Array<EventCreateInput>>;
  disconnect?: InputMaybe<Array<EventWhereUniqueInput>>;
  set?: InputMaybe<Array<EventWhereUniqueInput>>;
};

export type EventRelateToOneForCreateInput = {
  connect?: InputMaybe<EventWhereUniqueInput>;
  create?: InputMaybe<EventCreateInput>;
};

export type EventRelateToOneForUpdateInput = {
  connect?: InputMaybe<EventWhereUniqueInput>;
  create?: InputMaybe<EventCreateInput>;
  disconnect?: InputMaybe<Scalars['Boolean']['input']>;
};

export type EventTicket = {
  __typename?: 'EventTicket';
  bookedTicket?: Maybe<Ticket>;
  cartTicket?: Maybe<CartTicket>;
  code?: Maybe<Scalars['String']['output']>;
  displayName?: Maybe<Scalars['String']['output']>;
  event?: Maybe<Event>;
  id: Scalars['ID']['output'];
  isBlocked?: Maybe<Scalars['Boolean']['output']>;
  number?: Maybe<Scalars['Int']['output']>;
  type?: Maybe<Scalars['String']['output']>;
  version?: Maybe<Scalars['Int']['output']>;
};

export type EventTicketCreateInput = {
  bookedTicket?: InputMaybe<TicketRelateToOneForCreateInput>;
  cartTicket?: InputMaybe<CartTicketRelateToOneForCreateInput>;
  code?: InputMaybe<Scalars['String']['input']>;
  event?: InputMaybe<EventRelateToOneForCreateInput>;
  isBlocked?: InputMaybe<Scalars['Boolean']['input']>;
  number?: InputMaybe<Scalars['Int']['input']>;
  type?: InputMaybe<Scalars['String']['input']>;
};

export type EventTicketOrderByInput = {
  code?: InputMaybe<OrderDirection>;
  id?: InputMaybe<OrderDirection>;
  isBlocked?: InputMaybe<OrderDirection>;
  number?: InputMaybe<OrderDirection>;
  type?: InputMaybe<OrderDirection>;
  version?: InputMaybe<OrderDirection>;
};

export type EventTicketRelateToOneForCreateInput = {
  connect?: InputMaybe<EventTicketWhereUniqueInput>;
  create?: InputMaybe<EventTicketCreateInput>;
};

export type EventTicketRelateToOneForUpdateInput = {
  connect?: InputMaybe<EventTicketWhereUniqueInput>;
  create?: InputMaybe<EventTicketCreateInput>;
  disconnect?: InputMaybe<Scalars['Boolean']['input']>;
};

export type EventTicketUpdateArgs = {
  data: EventTicketUpdateInput;
  where: EventTicketWhereUniqueInput;
};

export type EventTicketUpdateInput = {
  bookedTicket?: InputMaybe<TicketRelateToOneForUpdateInput>;
  cartTicket?: InputMaybe<CartTicketRelateToOneForUpdateInput>;
  code?: InputMaybe<Scalars['String']['input']>;
  event?: InputMaybe<EventRelateToOneForUpdateInput>;
  isBlocked?: InputMaybe<Scalars['Boolean']['input']>;
  number?: InputMaybe<Scalars['Int']['input']>;
  type?: InputMaybe<Scalars['String']['input']>;
};

export type EventTicketWhereInput = {
  AND?: InputMaybe<Array<EventTicketWhereInput>>;
  NOT?: InputMaybe<Array<EventTicketWhereInput>>;
  OR?: InputMaybe<Array<EventTicketWhereInput>>;
  bookedTicket?: InputMaybe<TicketWhereInput>;
  cartTicket?: InputMaybe<CartTicketWhereInput>;
  code?: InputMaybe<StringFilter>;
  event?: InputMaybe<EventWhereInput>;
  id?: InputMaybe<IdFilter>;
  isBlocked?: InputMaybe<BooleanFilter>;
  number?: InputMaybe<IntFilter>;
  type?: InputMaybe<StringNullableFilter>;
  version?: InputMaybe<IntFilter>;
};

export type EventTicketWhereUniqueInput = {
  code?: InputMaybe<Scalars['String']['input']>;
  id?: InputMaybe<Scalars['ID']['input']>;
};

export type EventUpdateArgs = {
  data: EventUpdateInput;
  where: EventWhereUniqueInput;
};

export type EventUpdateInput = {
  ageMaximum?: InputMaybe<Scalars['Int']['input']>;
  ageMinimum?: InputMaybe<Scalars['Int']['input']>;
  carePeriodAlternative?: InputMaybe<CarePeriodRelateToOneForUpdateInput>;
  carePeriodCommitted?: InputMaybe<CarePeriodRelateToOneForUpdateInput>;
  carePeriodDesired?: InputMaybe<CarePeriodRelateToOneForUpdateInput>;
  careTimeEnd?: InputMaybe<Scalars['String']['input']>;
  careTimeStart?: InputMaybe<Scalars['String']['input']>;
  categories?: InputMaybe<EventCategoryRelateToManyForUpdateInput>;
  contactPersonFirstname?: InputMaybe<Scalars['String']['input']>;
  contactPersonSalutation?: InputMaybe<Scalars['String']['input']>;
  contactPersonSurname?: InputMaybe<Scalars['String']['input']>;
  costs?: InputMaybe<Scalars['Float']['input']>;
  description?: InputMaybe<Scalars['String']['input']>;
  emergencyPhone?: InputMaybe<Scalars['String']['input']>;
  hints?: InputMaybe<Scalars['String']['input']>;
  locationCity?: InputMaybe<Scalars['String']['input']>;
  locationStreet?: InputMaybe<Scalars['String']['input']>;
  locationZipcode?: InputMaybe<Scalars['String']['input']>;
  markedAsFinishedAt?: InputMaybe<Scalars['DateTime']['input']>;
  meals?: InputMaybe<Scalars['String']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
  organizer?: InputMaybe<OrganizerRelateToOneForUpdateInput>;
  participantLimit?: InputMaybe<Scalars['Int']['input']>;
  participantMinimum?: InputMaybe<Scalars['Int']['input']>;
  picture?: InputMaybe<ImageFieldInput>;
  pictureRights?: InputMaybe<Scalars['String']['input']>;
  registrationDeadline?: InputMaybe<Scalars['CalendarDay']['input']>;
  season?: InputMaybe<SeasonRelateToOneForUpdateInput>;
  status?: InputMaybe<Scalars['String']['input']>;
  waitingListLimit?: InputMaybe<Scalars['Int']['input']>;
};

export type EventWhereInput = {
  AND?: InputMaybe<Array<EventWhereInput>>;
  NOT?: InputMaybe<Array<EventWhereInput>>;
  OR?: InputMaybe<Array<EventWhereInput>>;
  ageMaximum?: InputMaybe<IntNullableFilter>;
  ageMinimum?: InputMaybe<IntNullableFilter>;
  carePeriodAlternative?: InputMaybe<CarePeriodWhereInput>;
  carePeriodCommitted?: InputMaybe<CarePeriodWhereInput>;
  carePeriodDesired?: InputMaybe<CarePeriodWhereInput>;
  careTimeEnd?: InputMaybe<StringFilter>;
  careTimeStart?: InputMaybe<StringFilter>;
  categories?: InputMaybe<EventCategoryManyRelationFilter>;
  contactPersonFirstname?: InputMaybe<StringFilter>;
  contactPersonSalutation?: InputMaybe<StringNullableFilter>;
  contactPersonSurname?: InputMaybe<StringFilter>;
  costs?: InputMaybe<FloatNullableFilter>;
  description?: InputMaybe<StringFilter>;
  emergencyPhone?: InputMaybe<StringFilter>;
  hints?: InputMaybe<StringFilter>;
  id?: InputMaybe<IdFilter>;
  locationCity?: InputMaybe<StringFilter>;
  locationStreet?: InputMaybe<StringFilter>;
  locationZipcode?: InputMaybe<StringFilter>;
  markedAsFinishedAt?: InputMaybe<DateTimeNullableFilter>;
  meals?: InputMaybe<StringFilter>;
  name?: InputMaybe<StringFilter>;
  organizer?: InputMaybe<OrganizerWhereInput>;
  participantLimit?: InputMaybe<IntNullableFilter>;
  participantMinimum?: InputMaybe<IntNullableFilter>;
  pictureRights?: InputMaybe<StringFilter>;
  registrationDeadline?: InputMaybe<CalendarDayNullableFilter>;
  season?: InputMaybe<SeasonWhereInput>;
  status?: InputMaybe<StringNullableFilter>;
  waitingListLimit?: InputMaybe<IntNullableFilter>;
};

export type EventWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
};

export type ExtendCartTermFailed = {
  __typename?: 'ExtendCartTermFailed';
  reason: Scalars['String']['output'];
};

export type ExtendCartTermResult = ExtendCartTermFailed | ExtendCartTermSuccess;

export type ExtendCartTermSuccess = {
  __typename?: 'ExtendCartTermSuccess';
  cart: Cart;
};

export type FileFieldInput = {
  upload: Scalars['Upload']['input'];
};

export type FileFieldOutput = {
  __typename?: 'FileFieldOutput';
  filename: Scalars['String']['output'];
  filesize: Scalars['Int']['output'];
  url: Scalars['String']['output'];
};

export type FloatFilter = {
  equals?: InputMaybe<Scalars['Float']['input']>;
  gt?: InputMaybe<Scalars['Float']['input']>;
  gte?: InputMaybe<Scalars['Float']['input']>;
  in?: InputMaybe<Array<Scalars['Float']['input']>>;
  lt?: InputMaybe<Scalars['Float']['input']>;
  lte?: InputMaybe<Scalars['Float']['input']>;
  not?: InputMaybe<FloatFilter>;
  notIn?: InputMaybe<Array<Scalars['Float']['input']>>;
};

export type FloatNullableFilter = {
  equals?: InputMaybe<Scalars['Float']['input']>;
  gt?: InputMaybe<Scalars['Float']['input']>;
  gte?: InputMaybe<Scalars['Float']['input']>;
  in?: InputMaybe<Array<Scalars['Float']['input']>>;
  lt?: InputMaybe<Scalars['Float']['input']>;
  lte?: InputMaybe<Scalars['Float']['input']>;
  not?: InputMaybe<FloatNullableFilter>;
  notIn?: InputMaybe<Array<Scalars['Float']['input']>>;
};

export type Holiday = {
  __typename?: 'Holiday';
  id: Scalars['ID']['output'];
  name?: Maybe<Scalars['String']['output']>;
  periods?: Maybe<Array<Period>>;
  periodsCount?: Maybe<Scalars['Int']['output']>;
};


export type HolidayPeriodsArgs = {
  cursor?: InputMaybe<PeriodWhereUniqueInput>;
  orderBy?: Array<PeriodOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: PeriodWhereInput;
};


export type HolidayPeriodsCountArgs = {
  where?: PeriodWhereInput;
};

export type HolidayCreateInput = {
  name?: InputMaybe<Scalars['String']['input']>;
  periods?: InputMaybe<PeriodRelateToManyForCreateInput>;
};

export type HolidayOrderByInput = {
  id?: InputMaybe<OrderDirection>;
  name?: InputMaybe<OrderDirection>;
};

export type HolidayRelateToOneForCreateInput = {
  connect?: InputMaybe<HolidayWhereUniqueInput>;
  create?: InputMaybe<HolidayCreateInput>;
};

export type HolidayRelateToOneForUpdateInput = {
  connect?: InputMaybe<HolidayWhereUniqueInput>;
  create?: InputMaybe<HolidayCreateInput>;
  disconnect?: InputMaybe<Scalars['Boolean']['input']>;
};

export type HolidayUpdateArgs = {
  data: HolidayUpdateInput;
  where: HolidayWhereUniqueInput;
};

export type HolidayUpdateInput = {
  name?: InputMaybe<Scalars['String']['input']>;
  periods?: InputMaybe<PeriodRelateToManyForUpdateInput>;
};

export type HolidayWhereInput = {
  AND?: InputMaybe<Array<HolidayWhereInput>>;
  NOT?: InputMaybe<Array<HolidayWhereInput>>;
  OR?: InputMaybe<Array<HolidayWhereInput>>;
  id?: InputMaybe<IdFilter>;
  name?: InputMaybe<StringFilter>;
  periods?: InputMaybe<PeriodManyRelationFilter>;
};

export type HolidayWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
};

export type IdFilter = {
  equals?: InputMaybe<Scalars['ID']['input']>;
  gt?: InputMaybe<Scalars['ID']['input']>;
  gte?: InputMaybe<Scalars['ID']['input']>;
  in?: InputMaybe<Array<Scalars['ID']['input']>>;
  lt?: InputMaybe<Scalars['ID']['input']>;
  lte?: InputMaybe<Scalars['ID']['input']>;
  not?: InputMaybe<IdFilter>;
  notIn?: InputMaybe<Array<Scalars['ID']['input']>>;
};

export enum ImageExtension {
  Gif = 'gif',
  Jpg = 'jpg',
  Png = 'png',
  Webp = 'webp'
}

export type ImageFieldInput = {
  upload: Scalars['Upload']['input'];
};

export type ImageFieldOutput = {
  __typename?: 'ImageFieldOutput';
  extension: ImageExtension;
  filesize: Scalars['Int']['output'];
  height: Scalars['Int']['output'];
  id: Scalars['ID']['output'];
  url: Scalars['String']['output'];
  width: Scalars['Int']['output'];
};

export type IntFilter = {
  equals?: InputMaybe<Scalars['Int']['input']>;
  gt?: InputMaybe<Scalars['Int']['input']>;
  gte?: InputMaybe<Scalars['Int']['input']>;
  in?: InputMaybe<Array<Scalars['Int']['input']>>;
  lt?: InputMaybe<Scalars['Int']['input']>;
  lte?: InputMaybe<Scalars['Int']['input']>;
  not?: InputMaybe<IntFilter>;
  notIn?: InputMaybe<Array<Scalars['Int']['input']>>;
};

export type IntNullableFilter = {
  equals?: InputMaybe<Scalars['Int']['input']>;
  gt?: InputMaybe<Scalars['Int']['input']>;
  gte?: InputMaybe<Scalars['Int']['input']>;
  in?: InputMaybe<Array<Scalars['Int']['input']>>;
  lt?: InputMaybe<Scalars['Int']['input']>;
  lte?: InputMaybe<Scalars['Int']['input']>;
  not?: InputMaybe<IntNullableFilter>;
  notIn?: InputMaybe<Array<Scalars['Int']['input']>>;
};

export type Invoice = {
  __typename?: 'Invoice';
  createdAt?: Maybe<Scalars['DateTime']['output']>;
  document?: Maybe<FileFieldOutput>;
  event?: Maybe<Event>;
  id: Scalars['ID']['output'];
};

export type InvoiceCreateInput = {
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  document?: InputMaybe<FileFieldInput>;
  event?: InputMaybe<EventRelateToOneForCreateInput>;
};

export type InvoiceOrderByInput = {
  createdAt?: InputMaybe<OrderDirection>;
  id?: InputMaybe<OrderDirection>;
};

export type InvoiceUpdateArgs = {
  data: InvoiceUpdateInput;
  where: InvoiceWhereUniqueInput;
};

export type InvoiceUpdateInput = {
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  document?: InputMaybe<FileFieldInput>;
  event?: InputMaybe<EventRelateToOneForUpdateInput>;
};

export type InvoiceWhereInput = {
  AND?: InputMaybe<Array<InvoiceWhereInput>>;
  NOT?: InputMaybe<Array<InvoiceWhereInput>>;
  OR?: InputMaybe<Array<InvoiceWhereInput>>;
  createdAt?: InputMaybe<DateTimeNullableFilter>;
  event?: InputMaybe<EventWhereInput>;
  id?: InputMaybe<IdFilter>;
};

export type InvoiceWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
};

export type KeystoneAdminMeta = {
  __typename?: 'KeystoneAdminMeta';
  list?: Maybe<KeystoneAdminUiListMeta>;
  lists: Array<KeystoneAdminUiListMeta>;
};


export type KeystoneAdminMetaListArgs = {
  key: Scalars['String']['input'];
};

export type KeystoneAdminUiFieldGroupMeta = {
  __typename?: 'KeystoneAdminUIFieldGroupMeta';
  description?: Maybe<Scalars['String']['output']>;
  fields: Array<KeystoneAdminUiFieldMeta>;
  label: Scalars['String']['output'];
};

export type KeystoneAdminUiFieldMeta = {
  __typename?: 'KeystoneAdminUIFieldMeta';
  createView: KeystoneAdminUiFieldMetaCreateView;
  customViewsIndex?: Maybe<Scalars['Int']['output']>;
  description?: Maybe<Scalars['String']['output']>;
  fieldMeta?: Maybe<Scalars['JSON']['output']>;
  isFilterable: Scalars['Boolean']['output'];
  isNonNull?: Maybe<Array<KeystoneAdminUiFieldMetaIsNonNull>>;
  isOrderable: Scalars['Boolean']['output'];
  itemView?: Maybe<KeystoneAdminUiFieldMetaItemView>;
  label: Scalars['String']['output'];
  listView: KeystoneAdminUiFieldMetaListView;
  path: Scalars['String']['output'];
  search?: Maybe<QueryMode>;
  viewsIndex: Scalars['Int']['output'];
};


export type KeystoneAdminUiFieldMetaItemViewArgs = {
  id?: InputMaybe<Scalars['ID']['input']>;
};

export type KeystoneAdminUiFieldMetaCreateView = {
  __typename?: 'KeystoneAdminUIFieldMetaCreateView';
  fieldMode: KeystoneAdminUiFieldMetaCreateViewFieldMode;
};

export enum KeystoneAdminUiFieldMetaCreateViewFieldMode {
  Edit = 'edit',
  Hidden = 'hidden'
}

export enum KeystoneAdminUiFieldMetaIsNonNull {
  Create = 'create',
  Read = 'read',
  Update = 'update'
}

export type KeystoneAdminUiFieldMetaItemView = {
  __typename?: 'KeystoneAdminUIFieldMetaItemView';
  fieldMode?: Maybe<KeystoneAdminUiFieldMetaItemViewFieldMode>;
  fieldPosition?: Maybe<KeystoneAdminUiFieldMetaItemViewFieldPosition>;
};

export enum KeystoneAdminUiFieldMetaItemViewFieldMode {
  Edit = 'edit',
  Hidden = 'hidden',
  Read = 'read'
}

export enum KeystoneAdminUiFieldMetaItemViewFieldPosition {
  Form = 'form',
  Sidebar = 'sidebar'
}

export type KeystoneAdminUiFieldMetaListView = {
  __typename?: 'KeystoneAdminUIFieldMetaListView';
  fieldMode: KeystoneAdminUiFieldMetaListViewFieldMode;
};

export enum KeystoneAdminUiFieldMetaListViewFieldMode {
  Hidden = 'hidden',
  Read = 'read'
}

export type KeystoneAdminUiListMeta = {
  __typename?: 'KeystoneAdminUIListMeta';
  description?: Maybe<Scalars['String']['output']>;
  fields: Array<KeystoneAdminUiFieldMeta>;
  groups: Array<KeystoneAdminUiFieldGroupMeta>;
  hideCreate: Scalars['Boolean']['output'];
  hideDelete: Scalars['Boolean']['output'];
  initialColumns: Array<Scalars['String']['output']>;
  initialSort?: Maybe<KeystoneAdminUiSort>;
  isHidden: Scalars['Boolean']['output'];
  isSingleton: Scalars['Boolean']['output'];
  itemQueryName: Scalars['String']['output'];
  key: Scalars['String']['output'];
  label: Scalars['String']['output'];
  labelField: Scalars['String']['output'];
  listQueryName: Scalars['String']['output'];
  pageSize: Scalars['Int']['output'];
  path: Scalars['String']['output'];
  plural: Scalars['String']['output'];
  singular: Scalars['String']['output'];
};

export type KeystoneAdminUiSort = {
  __typename?: 'KeystoneAdminUISort';
  direction: KeystoneAdminUiSortDirection;
  field: Scalars['String']['output'];
};

export enum KeystoneAdminUiSortDirection {
  Asc = 'ASC',
  Desc = 'DESC'
}

export type KeystoneMeta = {
  __typename?: 'KeystoneMeta';
  adminMeta: KeystoneAdminMeta;
};

export type MoveWaitinglistToParticipantFailed = {
  __typename?: 'MoveWaitinglistToParticipantFailed';
  reason: Scalars['String']['output'];
};

export type MoveWaitinglistToParticipantResult = MoveWaitinglistToParticipantFailed | MoveWaitinglistToParticipantSuccess;

export type MoveWaitinglistToParticipantSuccess = {
  __typename?: 'MoveWaitinglistToParticipantSuccess';
  eventTickets: Array<EventTicket>;
};

export type Mutation = {
  __typename?: 'Mutation';
  addTicketsToCart: AddTicketsToCartResult;
  applyForDiscount: ApplyForDiscountResult;
  approveDiscount: ApproveDiscountResult;
  assignChildToCartTicket: AssignChildToCartTicketResult;
  assignDiscountTypeToCartTicket: CartTicket;
  authenticateUserWithPassword?: Maybe<UserAuthenticationWithPasswordResult>;
  bookCartTickets: BookCartTicketsResult;
  cancelDiscount: CancelDiscountResult;
  cancelTicket: Array<EventTicket>;
  changePassword: ChangePasswordResult;
  createBooking?: Maybe<Booking>;
  createBookings?: Maybe<Array<Maybe<Booking>>>;
  createCareDay?: Maybe<CareDay>;
  createCareDays?: Maybe<Array<Maybe<CareDay>>>;
  createCarePeriod?: Maybe<CarePeriod>;
  createCarePeriods?: Maybe<Array<Maybe<CarePeriod>>>;
  createCart?: Maybe<Cart>;
  createCartTicket?: Maybe<CartTicket>;
  createCartTickets?: Maybe<Array<Maybe<CartTicket>>>;
  createCarts?: Maybe<Array<Maybe<Cart>>>;
  createChild?: Maybe<Child>;
  createChildren?: Maybe<Array<Maybe<Child>>>;
  createContact?: Maybe<Contact>;
  createContacts?: Maybe<Array<Maybe<Contact>>>;
  createCustodian?: Maybe<Custodian>;
  createCustodians?: Maybe<Array<Maybe<Custodian>>>;
  createDiscount?: Maybe<Discount>;
  createDiscountRejectionReason?: Maybe<DiscountRejectionReason>;
  createDiscountRejectionReasons?: Maybe<Array<Maybe<DiscountRejectionReason>>>;
  createDiscountType?: Maybe<DiscountType>;
  createDiscountTypes?: Maybe<Array<Maybe<DiscountType>>>;
  createDiscountVoucher?: Maybe<DiscountVoucher>;
  createDiscountVouchers?: Maybe<Array<Maybe<DiscountVoucher>>>;
  createDiscounts?: Maybe<Array<Maybe<Discount>>>;
  createEarlyAccessCode?: Maybe<EarlyAccessCode>;
  createEarlyAccessCodeVoucher?: Maybe<EarlyAccessCodeVoucher>;
  createEarlyAccessCodeVouchers?: Maybe<Array<Maybe<EarlyAccessCodeVoucher>>>;
  createEarlyAccessCodes?: Maybe<Array<Maybe<EarlyAccessCode>>>;
  createEmailTemplate?: Maybe<EmailTemplate>;
  createEmailTemplates?: Maybe<Array<Maybe<EmailTemplate>>>;
  createEvent?: Maybe<Event>;
  createEventCategories?: Maybe<Array<Maybe<EventCategory>>>;
  createEventCategory?: Maybe<EventCategory>;
  createEventTicket?: Maybe<EventTicket>;
  createEventTickets?: Maybe<Array<Maybe<EventTicket>>>;
  createEvents?: Maybe<Array<Maybe<Event>>>;
  createHoliday?: Maybe<Holiday>;
  createHolidays?: Maybe<Array<Maybe<Holiday>>>;
  createInitialUser: UserAuthenticationWithPasswordSuccess;
  createInvoice?: Maybe<Invoice>;
  createInvoices?: Maybe<Array<Maybe<Invoice>>>;
  createOrganizer?: Maybe<Organizer>;
  createOrganizers?: Maybe<Array<Maybe<Organizer>>>;
  createPartner?: Maybe<Partner>;
  createPartners?: Maybe<Array<Maybe<Partner>>>;
  createPasswordResetToken?: Maybe<PasswordResetToken>;
  createPasswordResetTokens?: Maybe<Array<Maybe<PasswordResetToken>>>;
  createPeriod?: Maybe<Period>;
  createPeriods?: Maybe<Array<Maybe<Period>>>;
  createReservation?: Maybe<Reservation>;
  createReservations?: Maybe<Array<Maybe<Reservation>>>;
  createSeason?: Maybe<Season>;
  createSeasons?: Maybe<Array<Maybe<Season>>>;
  createTicket?: Maybe<Ticket>;
  createTickets?: Maybe<Array<Maybe<Ticket>>>;
  createUser?: Maybe<User>;
  createUserVerificationToken?: Maybe<UserVerificationToken>;
  createUserVerificationTokens?: Maybe<Array<Maybe<UserVerificationToken>>>;
  createUsers?: Maybe<Array<Maybe<User>>>;
  deleteAccount: Scalars['Boolean']['output'];
  deleteBooking?: Maybe<Booking>;
  deleteBookings?: Maybe<Array<Maybe<Booking>>>;
  deleteCareDay?: Maybe<CareDay>;
  deleteCareDays?: Maybe<Array<Maybe<CareDay>>>;
  deleteCarePeriod?: Maybe<CarePeriod>;
  deleteCarePeriods?: Maybe<Array<Maybe<CarePeriod>>>;
  deleteCart?: Maybe<Cart>;
  deleteCartTicket?: Maybe<CartTicket>;
  deleteCartTickets?: Maybe<Array<Maybe<CartTicket>>>;
  deleteCarts?: Maybe<Array<Maybe<Cart>>>;
  deleteChild?: Maybe<Child>;
  deleteChildren?: Maybe<Array<Maybe<Child>>>;
  deleteContact?: Maybe<Contact>;
  deleteContacts?: Maybe<Array<Maybe<Contact>>>;
  deleteCustodian?: Maybe<Custodian>;
  deleteCustodians?: Maybe<Array<Maybe<Custodian>>>;
  deleteDiscount?: Maybe<Discount>;
  deleteDiscountRejectionReason?: Maybe<DiscountRejectionReason>;
  deleteDiscountRejectionReasons?: Maybe<Array<Maybe<DiscountRejectionReason>>>;
  deleteDiscountType?: Maybe<DiscountType>;
  deleteDiscountTypes?: Maybe<Array<Maybe<DiscountType>>>;
  deleteDiscountVoucher?: Maybe<DiscountVoucher>;
  deleteDiscountVouchers?: Maybe<Array<Maybe<DiscountVoucher>>>;
  deleteDiscounts?: Maybe<Array<Maybe<Discount>>>;
  deleteEarlyAccessCode?: Maybe<EarlyAccessCode>;
  deleteEarlyAccessCodeVoucher?: Maybe<EarlyAccessCodeVoucher>;
  deleteEarlyAccessCodeVouchers?: Maybe<Array<Maybe<EarlyAccessCodeVoucher>>>;
  deleteEarlyAccessCodes?: Maybe<Array<Maybe<EarlyAccessCode>>>;
  deleteEmailTemplate?: Maybe<EmailTemplate>;
  deleteEmailTemplates?: Maybe<Array<Maybe<EmailTemplate>>>;
  deleteEvent?: Maybe<Event>;
  deleteEventCategories?: Maybe<Array<Maybe<EventCategory>>>;
  deleteEventCategory?: Maybe<EventCategory>;
  deleteEventTicket?: Maybe<EventTicket>;
  deleteEventTickets?: Maybe<Array<Maybe<EventTicket>>>;
  deleteEvents?: Maybe<Array<Maybe<Event>>>;
  deleteHoliday?: Maybe<Holiday>;
  deleteHolidays?: Maybe<Array<Maybe<Holiday>>>;
  deleteInvoice?: Maybe<Invoice>;
  deleteInvoices?: Maybe<Array<Maybe<Invoice>>>;
  deleteOrganizer?: Maybe<Organizer>;
  deleteOrganizers?: Maybe<Array<Maybe<Organizer>>>;
  deletePartner?: Maybe<Partner>;
  deletePartners?: Maybe<Array<Maybe<Partner>>>;
  deletePasswordResetToken?: Maybe<PasswordResetToken>;
  deletePasswordResetTokens?: Maybe<Array<Maybe<PasswordResetToken>>>;
  deletePeriod?: Maybe<Period>;
  deletePeriods?: Maybe<Array<Maybe<Period>>>;
  deleteReservation?: Maybe<Reservation>;
  deleteReservations?: Maybe<Array<Maybe<Reservation>>>;
  deleteSeason?: Maybe<Season>;
  deleteSeasonData: Scalars['Boolean']['output'];
  deleteSeasons?: Maybe<Array<Maybe<Season>>>;
  deleteTicket?: Maybe<Ticket>;
  deleteTickets?: Maybe<Array<Maybe<Ticket>>>;
  deleteUser?: Maybe<User>;
  deleteUserVerificationToken?: Maybe<UserVerificationToken>;
  deleteUserVerificationTokens?: Maybe<Array<Maybe<UserVerificationToken>>>;
  deleteUsers?: Maybe<Array<Maybe<User>>>;
  duplicateEvent: Array<Event>;
  endSession: Scalars['Boolean']['output'];
  extendCartLifetime: ExtendCartTermResult;
  generateEarlyAccessCodes: Array<EarlyAccessCode>;
  generateInvoice: Invoice;
  markBookingPaymentReminded: Booking;
  markChildDeleted?: Maybe<Child>;
  markEarlyAccessCodeNotified: EarlyAccessCode;
  moveWaitinglistToParticipant: MoveWaitinglistToParticipantResult;
  produceEventTickets: Scalars['Int']['output'];
  redeemEarlyAccessCode: RedeemEarlyAccessCodeResult;
  register: RegisterResult;
  registerPayment: RegisterPaymentResult;
  rejectDiscount: RejectDiscountResult;
  removeTicketsFromCart: Array<Scalars['ID']['output']>;
  requestPasswordResetToken: RequestPasswordResetTokenResult;
  resendBookingConfirmation: Booking;
  resendVerificationCode: ResendVerificationCodeResult;
  resetPassword: ResetPasswordResult;
  sendDiscountApplicationReminder: Discount;
  sendEmailToAllCustodians: Scalars['Boolean']['output'];
  unblockEventTicket: UnblockEventTicketResult;
  updateBooking?: Maybe<Booking>;
  updateBookings?: Maybe<Array<Maybe<Booking>>>;
  updateCareDay?: Maybe<CareDay>;
  updateCareDays?: Maybe<Array<Maybe<CareDay>>>;
  updateCarePeriod?: Maybe<CarePeriod>;
  updateCarePeriods?: Maybe<Array<Maybe<CarePeriod>>>;
  updateCart?: Maybe<Cart>;
  updateCartTicket?: Maybe<CartTicket>;
  updateCartTickets?: Maybe<Array<Maybe<CartTicket>>>;
  updateCarts?: Maybe<Array<Maybe<Cart>>>;
  updateChild?: Maybe<Child>;
  updateChildren?: Maybe<Array<Maybe<Child>>>;
  updateContact?: Maybe<Contact>;
  updateContacts?: Maybe<Array<Maybe<Contact>>>;
  updateCustodian?: Maybe<Custodian>;
  updateCustodians?: Maybe<Array<Maybe<Custodian>>>;
  updateDiscount?: Maybe<Discount>;
  updateDiscountRejectionReason?: Maybe<DiscountRejectionReason>;
  updateDiscountRejectionReasons?: Maybe<Array<Maybe<DiscountRejectionReason>>>;
  updateDiscountType?: Maybe<DiscountType>;
  updateDiscountTypes?: Maybe<Array<Maybe<DiscountType>>>;
  updateDiscountVoucher?: Maybe<DiscountVoucher>;
  updateDiscountVouchers?: Maybe<Array<Maybe<DiscountVoucher>>>;
  updateDiscounts?: Maybe<Array<Maybe<Discount>>>;
  updateEarlyAccessCode?: Maybe<EarlyAccessCode>;
  updateEarlyAccessCodeVoucher?: Maybe<EarlyAccessCodeVoucher>;
  updateEarlyAccessCodeVouchers?: Maybe<Array<Maybe<EarlyAccessCodeVoucher>>>;
  updateEarlyAccessCodes?: Maybe<Array<Maybe<EarlyAccessCode>>>;
  updateEmailTemplate?: Maybe<EmailTemplate>;
  updateEmailTemplates?: Maybe<Array<Maybe<EmailTemplate>>>;
  updateEvent?: Maybe<Event>;
  updateEventCategories?: Maybe<Array<Maybe<EventCategory>>>;
  updateEventCategory?: Maybe<EventCategory>;
  updateEventState?: Maybe<Event>;
  updateEventTicket?: Maybe<EventTicket>;
  updateEventTickets?: Maybe<Array<Maybe<EventTicket>>>;
  updateEvents?: Maybe<Array<Maybe<Event>>>;
  updateHoliday?: Maybe<Holiday>;
  updateHolidays?: Maybe<Array<Maybe<Holiday>>>;
  updateInvoice?: Maybe<Invoice>;
  updateInvoices?: Maybe<Array<Maybe<Invoice>>>;
  updateOrganizer?: Maybe<Organizer>;
  updateOrganizers?: Maybe<Array<Maybe<Organizer>>>;
  updatePartner?: Maybe<Partner>;
  updatePartners?: Maybe<Array<Maybe<Partner>>>;
  updatePasswordResetToken?: Maybe<PasswordResetToken>;
  updatePasswordResetTokens?: Maybe<Array<Maybe<PasswordResetToken>>>;
  updatePeriod?: Maybe<Period>;
  updatePeriods?: Maybe<Array<Maybe<Period>>>;
  updateReservation?: Maybe<Reservation>;
  updateReservations?: Maybe<Array<Maybe<Reservation>>>;
  updateSeason?: Maybe<Season>;
  updateSeasons?: Maybe<Array<Maybe<Season>>>;
  updateTicket?: Maybe<Ticket>;
  updateTickets?: Maybe<Array<Maybe<Ticket>>>;
  updateUser?: Maybe<User>;
  updateUserVerificationToken?: Maybe<UserVerificationToken>;
  updateUserVerificationTokens?: Maybe<Array<Maybe<UserVerificationToken>>>;
  updateUsers?: Maybe<Array<Maybe<User>>>;
  verifyAccount: VerifyAccountResult;
};


export type MutationAddTicketsToCartArgs = {
  amounts: Array<AddTicketsToCartAmountsInput>;
  eventId: Scalars['ID']['input'];
};


export type MutationApplyForDiscountArgs = {
  childId: Scalars['ID']['input'];
  discountTypeId: Scalars['ID']['input'];
  year: Scalars['Int']['input'];
};


export type MutationApproveDiscountArgs = {
  discountId: Scalars['ID']['input'];
};


export type MutationAssignChildToCartTicketArgs = {
  cartTicketId: Scalars['ID']['input'];
  childId?: InputMaybe<Scalars['ID']['input']>;
};


export type MutationAssignDiscountTypeToCartTicketArgs = {
  cartTicketId: Scalars['ID']['input'];
  discountTypeId?: InputMaybe<Scalars['ID']['input']>;
};


export type MutationAuthenticateUserWithPasswordArgs = {
  email: Scalars['String']['input'];
  password: Scalars['String']['input'];
};


export type MutationBookCartTicketsArgs = {
  agreeDataProtection: Scalars['Boolean']['input'];
  agreeTermsAndConditions: Scalars['Boolean']['input'];
};


export type MutationCancelDiscountArgs = {
  discountId: Scalars['ID']['input'];
};


export type MutationCancelTicketArgs = {
  ticketId: Scalars['ID']['input'];
};


export type MutationChangePasswordArgs = {
  currentPassword: Scalars['String']['input'];
  newPassword: Scalars['String']['input'];
};


export type MutationCreateBookingArgs = {
  data: BookingCreateInput;
};


export type MutationCreateBookingsArgs = {
  data: Array<BookingCreateInput>;
};


export type MutationCreateCareDayArgs = {
  data: CareDayCreateInput;
};


export type MutationCreateCareDaysArgs = {
  data: Array<CareDayCreateInput>;
};


export type MutationCreateCarePeriodArgs = {
  data: CarePeriodCreateInput;
};


export type MutationCreateCarePeriodsArgs = {
  data: Array<CarePeriodCreateInput>;
};


export type MutationCreateCartArgs = {
  data: CartCreateInput;
};


export type MutationCreateCartTicketArgs = {
  data: CartTicketCreateInput;
};


export type MutationCreateCartTicketsArgs = {
  data: Array<CartTicketCreateInput>;
};


export type MutationCreateCartsArgs = {
  data: Array<CartCreateInput>;
};


export type MutationCreateChildArgs = {
  data: ChildCreateInput;
};


export type MutationCreateChildrenArgs = {
  data: Array<ChildCreateInput>;
};


export type MutationCreateContactArgs = {
  data: ContactCreateInput;
};


export type MutationCreateContactsArgs = {
  data: Array<ContactCreateInput>;
};


export type MutationCreateCustodianArgs = {
  data: CustodianCreateInput;
};


export type MutationCreateCustodiansArgs = {
  data: Array<CustodianCreateInput>;
};


export type MutationCreateDiscountArgs = {
  data: DiscountCreateInput;
};


export type MutationCreateDiscountRejectionReasonArgs = {
  data: DiscountRejectionReasonCreateInput;
};


export type MutationCreateDiscountRejectionReasonsArgs = {
  data: Array<DiscountRejectionReasonCreateInput>;
};


export type MutationCreateDiscountTypeArgs = {
  data: DiscountTypeCreateInput;
};


export type MutationCreateDiscountTypesArgs = {
  data: Array<DiscountTypeCreateInput>;
};


export type MutationCreateDiscountVoucherArgs = {
  data: DiscountVoucherCreateInput;
};


export type MutationCreateDiscountVouchersArgs = {
  data: Array<DiscountVoucherCreateInput>;
};


export type MutationCreateDiscountsArgs = {
  data: Array<DiscountCreateInput>;
};


export type MutationCreateEarlyAccessCodeArgs = {
  data: EarlyAccessCodeCreateInput;
};


export type MutationCreateEarlyAccessCodeVoucherArgs = {
  data: EarlyAccessCodeVoucherCreateInput;
};


export type MutationCreateEarlyAccessCodeVouchersArgs = {
  data: Array<EarlyAccessCodeVoucherCreateInput>;
};


export type MutationCreateEarlyAccessCodesArgs = {
  data: Array<EarlyAccessCodeCreateInput>;
};


export type MutationCreateEmailTemplateArgs = {
  data: EmailTemplateCreateInput;
};


export type MutationCreateEmailTemplatesArgs = {
  data: Array<EmailTemplateCreateInput>;
};


export type MutationCreateEventArgs = {
  data: EventCreateInput;
};


export type MutationCreateEventCategoriesArgs = {
  data: Array<EventCategoryCreateInput>;
};


export type MutationCreateEventCategoryArgs = {
  data: EventCategoryCreateInput;
};


export type MutationCreateEventTicketArgs = {
  data: EventTicketCreateInput;
};


export type MutationCreateEventTicketsArgs = {
  data: Array<EventTicketCreateInput>;
};


export type MutationCreateEventsArgs = {
  data: Array<EventCreateInput>;
};


export type MutationCreateHolidayArgs = {
  data: HolidayCreateInput;
};


export type MutationCreateHolidaysArgs = {
  data: Array<HolidayCreateInput>;
};


export type MutationCreateInitialUserArgs = {
  data: CreateInitialUserInput;
};


export type MutationCreateInvoiceArgs = {
  data: InvoiceCreateInput;
};


export type MutationCreateInvoicesArgs = {
  data: Array<InvoiceCreateInput>;
};


export type MutationCreateOrganizerArgs = {
  data: OrganizerCreateInput;
};


export type MutationCreateOrganizersArgs = {
  data: Array<OrganizerCreateInput>;
};


export type MutationCreatePartnerArgs = {
  data: PartnerCreateInput;
};


export type MutationCreatePartnersArgs = {
  data: Array<PartnerCreateInput>;
};


export type MutationCreatePasswordResetTokenArgs = {
  data: PasswordResetTokenCreateInput;
};


export type MutationCreatePasswordResetTokensArgs = {
  data: Array<PasswordResetTokenCreateInput>;
};


export type MutationCreatePeriodArgs = {
  data: PeriodCreateInput;
};


export type MutationCreatePeriodsArgs = {
  data: Array<PeriodCreateInput>;
};


export type MutationCreateReservationArgs = {
  data: ReservationCreateInput;
};


export type MutationCreateReservationsArgs = {
  data: Array<ReservationCreateInput>;
};


export type MutationCreateSeasonArgs = {
  data: SeasonCreateInput;
};


export type MutationCreateSeasonsArgs = {
  data: Array<SeasonCreateInput>;
};


export type MutationCreateTicketArgs = {
  data: TicketCreateInput;
};


export type MutationCreateTicketsArgs = {
  data: Array<TicketCreateInput>;
};


export type MutationCreateUserArgs = {
  data: UserCreateInput;
};


export type MutationCreateUserVerificationTokenArgs = {
  data: UserVerificationTokenCreateInput;
};


export type MutationCreateUserVerificationTokensArgs = {
  data: Array<UserVerificationTokenCreateInput>;
};


export type MutationCreateUsersArgs = {
  data: Array<UserCreateInput>;
};


export type MutationDeleteBookingArgs = {
  where: BookingWhereUniqueInput;
};


export type MutationDeleteBookingsArgs = {
  where: Array<BookingWhereUniqueInput>;
};


export type MutationDeleteCareDayArgs = {
  where: CareDayWhereUniqueInput;
};


export type MutationDeleteCareDaysArgs = {
  where: Array<CareDayWhereUniqueInput>;
};


export type MutationDeleteCarePeriodArgs = {
  where: CarePeriodWhereUniqueInput;
};


export type MutationDeleteCarePeriodsArgs = {
  where: Array<CarePeriodWhereUniqueInput>;
};


export type MutationDeleteCartArgs = {
  where: CartWhereUniqueInput;
};


export type MutationDeleteCartTicketArgs = {
  where: CartTicketWhereUniqueInput;
};


export type MutationDeleteCartTicketsArgs = {
  where: Array<CartTicketWhereUniqueInput>;
};


export type MutationDeleteCartsArgs = {
  where: Array<CartWhereUniqueInput>;
};


export type MutationDeleteChildArgs = {
  where: ChildWhereUniqueInput;
};


export type MutationDeleteChildrenArgs = {
  where: Array<ChildWhereUniqueInput>;
};


export type MutationDeleteContactArgs = {
  where: ContactWhereUniqueInput;
};


export type MutationDeleteContactsArgs = {
  where: Array<ContactWhereUniqueInput>;
};


export type MutationDeleteCustodianArgs = {
  where: CustodianWhereUniqueInput;
};


export type MutationDeleteCustodiansArgs = {
  where: Array<CustodianWhereUniqueInput>;
};


export type MutationDeleteDiscountArgs = {
  where: DiscountWhereUniqueInput;
};


export type MutationDeleteDiscountRejectionReasonArgs = {
  where: DiscountRejectionReasonWhereUniqueInput;
};


export type MutationDeleteDiscountRejectionReasonsArgs = {
  where: Array<DiscountRejectionReasonWhereUniqueInput>;
};


export type MutationDeleteDiscountTypeArgs = {
  where: DiscountTypeWhereUniqueInput;
};


export type MutationDeleteDiscountTypesArgs = {
  where: Array<DiscountTypeWhereUniqueInput>;
};


export type MutationDeleteDiscountVoucherArgs = {
  where: DiscountVoucherWhereUniqueInput;
};


export type MutationDeleteDiscountVouchersArgs = {
  where: Array<DiscountVoucherWhereUniqueInput>;
};


export type MutationDeleteDiscountsArgs = {
  where: Array<DiscountWhereUniqueInput>;
};


export type MutationDeleteEarlyAccessCodeArgs = {
  where: EarlyAccessCodeWhereUniqueInput;
};


export type MutationDeleteEarlyAccessCodeVoucherArgs = {
  where: EarlyAccessCodeVoucherWhereUniqueInput;
};


export type MutationDeleteEarlyAccessCodeVouchersArgs = {
  where: Array<EarlyAccessCodeVoucherWhereUniqueInput>;
};


export type MutationDeleteEarlyAccessCodesArgs = {
  where: Array<EarlyAccessCodeWhereUniqueInput>;
};


export type MutationDeleteEmailTemplateArgs = {
  where: EmailTemplateWhereUniqueInput;
};


export type MutationDeleteEmailTemplatesArgs = {
  where: Array<EmailTemplateWhereUniqueInput>;
};


export type MutationDeleteEventArgs = {
  where: EventWhereUniqueInput;
};


export type MutationDeleteEventCategoriesArgs = {
  where: Array<EventCategoryWhereUniqueInput>;
};


export type MutationDeleteEventCategoryArgs = {
  where: EventCategoryWhereUniqueInput;
};


export type MutationDeleteEventTicketArgs = {
  where: EventTicketWhereUniqueInput;
};


export type MutationDeleteEventTicketsArgs = {
  where: Array<EventTicketWhereUniqueInput>;
};


export type MutationDeleteEventsArgs = {
  where: Array<EventWhereUniqueInput>;
};


export type MutationDeleteHolidayArgs = {
  where: HolidayWhereUniqueInput;
};


export type MutationDeleteHolidaysArgs = {
  where: Array<HolidayWhereUniqueInput>;
};


export type MutationDeleteInvoiceArgs = {
  where: InvoiceWhereUniqueInput;
};


export type MutationDeleteInvoicesArgs = {
  where: Array<InvoiceWhereUniqueInput>;
};


export type MutationDeleteOrganizerArgs = {
  where: OrganizerWhereUniqueInput;
};


export type MutationDeleteOrganizersArgs = {
  where: Array<OrganizerWhereUniqueInput>;
};


export type MutationDeletePartnerArgs = {
  where: PartnerWhereUniqueInput;
};


export type MutationDeletePartnersArgs = {
  where: Array<PartnerWhereUniqueInput>;
};


export type MutationDeletePasswordResetTokenArgs = {
  where: PasswordResetTokenWhereUniqueInput;
};


export type MutationDeletePasswordResetTokensArgs = {
  where: Array<PasswordResetTokenWhereUniqueInput>;
};


export type MutationDeletePeriodArgs = {
  where: PeriodWhereUniqueInput;
};


export type MutationDeletePeriodsArgs = {
  where: Array<PeriodWhereUniqueInput>;
};


export type MutationDeleteReservationArgs = {
  where: ReservationWhereUniqueInput;
};


export type MutationDeleteReservationsArgs = {
  where: Array<ReservationWhereUniqueInput>;
};


export type MutationDeleteSeasonArgs = {
  where: SeasonWhereUniqueInput;
};


export type MutationDeleteSeasonDataArgs = {
  seasonId: Scalars['ID']['input'];
};


export type MutationDeleteSeasonsArgs = {
  where: Array<SeasonWhereUniqueInput>;
};


export type MutationDeleteTicketArgs = {
  where: TicketWhereUniqueInput;
};


export type MutationDeleteTicketsArgs = {
  where: Array<TicketWhereUniqueInput>;
};


export type MutationDeleteUserArgs = {
  where: UserWhereUniqueInput;
};


export type MutationDeleteUserVerificationTokenArgs = {
  where: UserVerificationTokenWhereUniqueInput;
};


export type MutationDeleteUserVerificationTokensArgs = {
  where: Array<UserVerificationTokenWhereUniqueInput>;
};


export type MutationDeleteUsersArgs = {
  where: Array<UserWhereUniqueInput>;
};


export type MutationDuplicateEventArgs = {
  amount: Scalars['Int']['input'];
  eventId: Scalars['ID']['input'];
  seasonId: Scalars['ID']['input'];
};


export type MutationGenerateEarlyAccessCodesArgs = {
  seasonId: Scalars['ID']['input'];
};


export type MutationGenerateInvoiceArgs = {
  eventId: Scalars['ID']['input'];
};


export type MutationMarkBookingPaymentRemindedArgs = {
  bookingId: Scalars['ID']['input'];
};


export type MutationMarkChildDeletedArgs = {
  childId: Scalars['ID']['input'];
};


export type MutationMarkEarlyAccessCodeNotifiedArgs = {
  earlyAccessCodeId: Scalars['ID']['input'];
};


export type MutationMoveWaitinglistToParticipantArgs = {
  ticketId: Scalars['ID']['input'];
};


export type MutationRedeemEarlyAccessCodeArgs = {
  code: Scalars['String']['input'];
  year: Scalars['Int']['input'];
};


export type MutationRegisterArgs = {
  data: RegisterInput;
};


export type MutationRegisterPaymentArgs = {
  bookingId: Scalars['ID']['input'];
};


export type MutationRejectDiscountArgs = {
  discountId: Scalars['ID']['input'];
  discountRejectionReasonId: Scalars['ID']['input'];
  rejectionNote?: InputMaybe<Scalars['String']['input']>;
};


export type MutationRemoveTicketsFromCartArgs = {
  ticketIds: Array<Scalars['ID']['input']>;
};


export type MutationRequestPasswordResetTokenArgs = {
  data: RequestPasswordResetTokenInput;
};


export type MutationResendBookingConfirmationArgs = {
  bookingId: Scalars['ID']['input'];
};


export type MutationResendVerificationCodeArgs = {
  data: ResendVerificationCodeInput;
};


export type MutationResetPasswordArgs = {
  newPassword: Scalars['String']['input'];
  token: Scalars['String']['input'];
};


export type MutationSendDiscountApplicationReminderArgs = {
  discountId: Scalars['ID']['input'];
};


export type MutationSendEmailToAllCustodiansArgs = {
  content: Scalars['String']['input'];
  subject: Scalars['String']['input'];
};


export type MutationUnblockEventTicketArgs = {
  eventTicketId: Scalars['ID']['input'];
};


export type MutationUpdateBookingArgs = {
  data: BookingUpdateInput;
  where: BookingWhereUniqueInput;
};


export type MutationUpdateBookingsArgs = {
  data: Array<BookingUpdateArgs>;
};


export type MutationUpdateCareDayArgs = {
  data: CareDayUpdateInput;
  where: CareDayWhereUniqueInput;
};


export type MutationUpdateCareDaysArgs = {
  data: Array<CareDayUpdateArgs>;
};


export type MutationUpdateCarePeriodArgs = {
  data: CarePeriodUpdateInput;
  where: CarePeriodWhereUniqueInput;
};


export type MutationUpdateCarePeriodsArgs = {
  data: Array<CarePeriodUpdateArgs>;
};


export type MutationUpdateCartArgs = {
  data: CartUpdateInput;
  where: CartWhereUniqueInput;
};


export type MutationUpdateCartTicketArgs = {
  data: CartTicketUpdateInput;
  where: CartTicketWhereUniqueInput;
};


export type MutationUpdateCartTicketsArgs = {
  data: Array<CartTicketUpdateArgs>;
};


export type MutationUpdateCartsArgs = {
  data: Array<CartUpdateArgs>;
};


export type MutationUpdateChildArgs = {
  data: ChildUpdateInput;
  where: ChildWhereUniqueInput;
};


export type MutationUpdateChildrenArgs = {
  data: Array<ChildUpdateArgs>;
};


export type MutationUpdateContactArgs = {
  data: ContactUpdateInput;
  where: ContactWhereUniqueInput;
};


export type MutationUpdateContactsArgs = {
  data: Array<ContactUpdateArgs>;
};


export type MutationUpdateCustodianArgs = {
  data: CustodianUpdateInput;
  where: CustodianWhereUniqueInput;
};


export type MutationUpdateCustodiansArgs = {
  data: Array<CustodianUpdateArgs>;
};


export type MutationUpdateDiscountArgs = {
  data: DiscountUpdateInput;
  where: DiscountWhereUniqueInput;
};


export type MutationUpdateDiscountRejectionReasonArgs = {
  data: DiscountRejectionReasonUpdateInput;
  where: DiscountRejectionReasonWhereUniqueInput;
};


export type MutationUpdateDiscountRejectionReasonsArgs = {
  data: Array<DiscountRejectionReasonUpdateArgs>;
};


export type MutationUpdateDiscountTypeArgs = {
  data: DiscountTypeUpdateInput;
  where: DiscountTypeWhereUniqueInput;
};


export type MutationUpdateDiscountTypesArgs = {
  data: Array<DiscountTypeUpdateArgs>;
};


export type MutationUpdateDiscountVoucherArgs = {
  data: DiscountVoucherUpdateInput;
  where: DiscountVoucherWhereUniqueInput;
};


export type MutationUpdateDiscountVouchersArgs = {
  data: Array<DiscountVoucherUpdateArgs>;
};


export type MutationUpdateDiscountsArgs = {
  data: Array<DiscountUpdateArgs>;
};


export type MutationUpdateEarlyAccessCodeArgs = {
  data: EarlyAccessCodeUpdateInput;
  where: EarlyAccessCodeWhereUniqueInput;
};


export type MutationUpdateEarlyAccessCodeVoucherArgs = {
  data: EarlyAccessCodeVoucherUpdateInput;
  where: EarlyAccessCodeVoucherWhereUniqueInput;
};


export type MutationUpdateEarlyAccessCodeVouchersArgs = {
  data: Array<EarlyAccessCodeVoucherUpdateArgs>;
};


export type MutationUpdateEarlyAccessCodesArgs = {
  data: Array<EarlyAccessCodeUpdateArgs>;
};


export type MutationUpdateEmailTemplateArgs = {
  data: EmailTemplateUpdateInput;
  where: EmailTemplateWhereUniqueInput;
};


export type MutationUpdateEmailTemplatesArgs = {
  data: Array<EmailTemplateUpdateArgs>;
};


export type MutationUpdateEventArgs = {
  data: EventUpdateInput;
  where: EventWhereUniqueInput;
};


export type MutationUpdateEventCategoriesArgs = {
  data: Array<EventCategoryUpdateArgs>;
};


export type MutationUpdateEventCategoryArgs = {
  data: EventCategoryUpdateInput;
  where: EventCategoryWhereUniqueInput;
};


export type MutationUpdateEventStateArgs = {
  id: Scalars['ID']['input'];
  status: Scalars['String']['input'];
};


export type MutationUpdateEventTicketArgs = {
  data: EventTicketUpdateInput;
  where: EventTicketWhereUniqueInput;
};


export type MutationUpdateEventTicketsArgs = {
  data: Array<EventTicketUpdateArgs>;
};


export type MutationUpdateEventsArgs = {
  data: Array<EventUpdateArgs>;
};


export type MutationUpdateHolidayArgs = {
  data: HolidayUpdateInput;
  where: HolidayWhereUniqueInput;
};


export type MutationUpdateHolidaysArgs = {
  data: Array<HolidayUpdateArgs>;
};


export type MutationUpdateInvoiceArgs = {
  data: InvoiceUpdateInput;
  where: InvoiceWhereUniqueInput;
};


export type MutationUpdateInvoicesArgs = {
  data: Array<InvoiceUpdateArgs>;
};


export type MutationUpdateOrganizerArgs = {
  data: OrganizerUpdateInput;
  where: OrganizerWhereUniqueInput;
};


export type MutationUpdateOrganizersArgs = {
  data: Array<OrganizerUpdateArgs>;
};


export type MutationUpdatePartnerArgs = {
  data: PartnerUpdateInput;
  where: PartnerWhereUniqueInput;
};


export type MutationUpdatePartnersArgs = {
  data: Array<PartnerUpdateArgs>;
};


export type MutationUpdatePasswordResetTokenArgs = {
  data: PasswordResetTokenUpdateInput;
  where: PasswordResetTokenWhereUniqueInput;
};


export type MutationUpdatePasswordResetTokensArgs = {
  data: Array<PasswordResetTokenUpdateArgs>;
};


export type MutationUpdatePeriodArgs = {
  data: PeriodUpdateInput;
  where: PeriodWhereUniqueInput;
};


export type MutationUpdatePeriodsArgs = {
  data: Array<PeriodUpdateArgs>;
};


export type MutationUpdateReservationArgs = {
  data: ReservationUpdateInput;
  where: ReservationWhereUniqueInput;
};


export type MutationUpdateReservationsArgs = {
  data: Array<ReservationUpdateArgs>;
};


export type MutationUpdateSeasonArgs = {
  data: SeasonUpdateInput;
  where: SeasonWhereUniqueInput;
};


export type MutationUpdateSeasonsArgs = {
  data: Array<SeasonUpdateArgs>;
};


export type MutationUpdateTicketArgs = {
  data: TicketUpdateInput;
  where: TicketWhereUniqueInput;
};


export type MutationUpdateTicketsArgs = {
  data: Array<TicketUpdateArgs>;
};


export type MutationUpdateUserArgs = {
  data: UserUpdateInput;
  where: UserWhereUniqueInput;
};


export type MutationUpdateUserVerificationTokenArgs = {
  data: UserVerificationTokenUpdateInput;
  where: UserVerificationTokenWhereUniqueInput;
};


export type MutationUpdateUserVerificationTokensArgs = {
  data: Array<UserVerificationTokenUpdateArgs>;
};


export type MutationUpdateUsersArgs = {
  data: Array<UserUpdateArgs>;
};


export type MutationVerifyAccountArgs = {
  data: VerifyAccountInput;
};

export type NestedStringFilter = {
  contains?: InputMaybe<Scalars['String']['input']>;
  endsWith?: InputMaybe<Scalars['String']['input']>;
  equals?: InputMaybe<Scalars['String']['input']>;
  gt?: InputMaybe<Scalars['String']['input']>;
  gte?: InputMaybe<Scalars['String']['input']>;
  in?: InputMaybe<Array<Scalars['String']['input']>>;
  lt?: InputMaybe<Scalars['String']['input']>;
  lte?: InputMaybe<Scalars['String']['input']>;
  not?: InputMaybe<NestedStringFilter>;
  notIn?: InputMaybe<Array<Scalars['String']['input']>>;
  startsWith?: InputMaybe<Scalars['String']['input']>;
};

export enum OrderDirection {
  Asc = 'asc',
  Desc = 'desc'
}

export type Organizer = {
  __typename?: 'Organizer';
  addressCity?: Maybe<Scalars['String']['output']>;
  addressStreet?: Maybe<Scalars['String']['output']>;
  addressZip?: Maybe<Scalars['String']['output']>;
  bankBic?: Maybe<Scalars['String']['output']>;
  bankIban?: Maybe<Scalars['String']['output']>;
  bankName?: Maybe<Scalars['String']['output']>;
  bankOwner?: Maybe<Scalars['String']['output']>;
  contactPersonFirstname?: Maybe<Scalars['String']['output']>;
  contactPersonSalutation?: Maybe<Scalars['String']['output']>;
  contactPersonSurname?: Maybe<Scalars['String']['output']>;
  email?: Maybe<Scalars['String']['output']>;
  events?: Maybe<Array<Event>>;
  eventsCount?: Maybe<Scalars['Int']['output']>;
  id: Scalars['ID']['output'];
  name?: Maybe<Scalars['String']['output']>;
  phone?: Maybe<Scalars['String']['output']>;
  users?: Maybe<Array<User>>;
  usersCount?: Maybe<Scalars['Int']['output']>;
};


export type OrganizerEventsArgs = {
  cursor?: InputMaybe<EventWhereUniqueInput>;
  orderBy?: Array<EventOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: EventWhereInput;
};


export type OrganizerEventsCountArgs = {
  where?: EventWhereInput;
};


export type OrganizerUsersArgs = {
  cursor?: InputMaybe<UserWhereUniqueInput>;
  orderBy?: Array<UserOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: UserWhereInput;
};


export type OrganizerUsersCountArgs = {
  where?: UserWhereInput;
};

export type OrganizerCreateInput = {
  addressCity?: InputMaybe<Scalars['String']['input']>;
  addressStreet?: InputMaybe<Scalars['String']['input']>;
  addressZip?: InputMaybe<Scalars['String']['input']>;
  bankBic?: InputMaybe<Scalars['String']['input']>;
  bankIban?: InputMaybe<Scalars['String']['input']>;
  bankName?: InputMaybe<Scalars['String']['input']>;
  bankOwner?: InputMaybe<Scalars['String']['input']>;
  contactPersonFirstname?: InputMaybe<Scalars['String']['input']>;
  contactPersonSalutation?: InputMaybe<Scalars['String']['input']>;
  contactPersonSurname?: InputMaybe<Scalars['String']['input']>;
  email?: InputMaybe<Scalars['String']['input']>;
  events?: InputMaybe<EventRelateToManyForCreateInput>;
  name?: InputMaybe<Scalars['String']['input']>;
  phone?: InputMaybe<Scalars['String']['input']>;
  users?: InputMaybe<UserRelateToManyForCreateInput>;
};

export type OrganizerOrderByInput = {
  addressCity?: InputMaybe<OrderDirection>;
  addressStreet?: InputMaybe<OrderDirection>;
  addressZip?: InputMaybe<OrderDirection>;
  bankBic?: InputMaybe<OrderDirection>;
  bankIban?: InputMaybe<OrderDirection>;
  bankName?: InputMaybe<OrderDirection>;
  bankOwner?: InputMaybe<OrderDirection>;
  contactPersonFirstname?: InputMaybe<OrderDirection>;
  contactPersonSalutation?: InputMaybe<OrderDirection>;
  contactPersonSurname?: InputMaybe<OrderDirection>;
  email?: InputMaybe<OrderDirection>;
  id?: InputMaybe<OrderDirection>;
  name?: InputMaybe<OrderDirection>;
  phone?: InputMaybe<OrderDirection>;
};

export type OrganizerRelateToOneForCreateInput = {
  connect?: InputMaybe<OrganizerWhereUniqueInput>;
  create?: InputMaybe<OrganizerCreateInput>;
};

export type OrganizerRelateToOneForUpdateInput = {
  connect?: InputMaybe<OrganizerWhereUniqueInput>;
  create?: InputMaybe<OrganizerCreateInput>;
  disconnect?: InputMaybe<Scalars['Boolean']['input']>;
};

export type OrganizerUpdateArgs = {
  data: OrganizerUpdateInput;
  where: OrganizerWhereUniqueInput;
};

export type OrganizerUpdateInput = {
  addressCity?: InputMaybe<Scalars['String']['input']>;
  addressStreet?: InputMaybe<Scalars['String']['input']>;
  addressZip?: InputMaybe<Scalars['String']['input']>;
  bankBic?: InputMaybe<Scalars['String']['input']>;
  bankIban?: InputMaybe<Scalars['String']['input']>;
  bankName?: InputMaybe<Scalars['String']['input']>;
  bankOwner?: InputMaybe<Scalars['String']['input']>;
  contactPersonFirstname?: InputMaybe<Scalars['String']['input']>;
  contactPersonSalutation?: InputMaybe<Scalars['String']['input']>;
  contactPersonSurname?: InputMaybe<Scalars['String']['input']>;
  email?: InputMaybe<Scalars['String']['input']>;
  events?: InputMaybe<EventRelateToManyForUpdateInput>;
  name?: InputMaybe<Scalars['String']['input']>;
  phone?: InputMaybe<Scalars['String']['input']>;
  users?: InputMaybe<UserRelateToManyForUpdateInput>;
};

export type OrganizerWhereInput = {
  AND?: InputMaybe<Array<OrganizerWhereInput>>;
  NOT?: InputMaybe<Array<OrganizerWhereInput>>;
  OR?: InputMaybe<Array<OrganizerWhereInput>>;
  addressCity?: InputMaybe<StringFilter>;
  addressStreet?: InputMaybe<StringFilter>;
  addressZip?: InputMaybe<StringFilter>;
  bankBic?: InputMaybe<StringFilter>;
  bankIban?: InputMaybe<StringFilter>;
  bankName?: InputMaybe<StringFilter>;
  bankOwner?: InputMaybe<StringFilter>;
  contactPersonFirstname?: InputMaybe<StringFilter>;
  contactPersonSalutation?: InputMaybe<StringNullableFilter>;
  contactPersonSurname?: InputMaybe<StringFilter>;
  email?: InputMaybe<StringFilter>;
  events?: InputMaybe<EventManyRelationFilter>;
  id?: InputMaybe<IdFilter>;
  name?: InputMaybe<StringFilter>;
  phone?: InputMaybe<StringFilter>;
  users?: InputMaybe<UserManyRelationFilter>;
};

export type OrganizerWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
};

export type Partner = {
  __typename?: 'Partner';
  contactPersonFirstname?: Maybe<Scalars['String']['output']>;
  contactPersonSalutation?: Maybe<Scalars['String']['output']>;
  contactPersonSurname?: Maybe<Scalars['String']['output']>;
  earlyAccessCode?: Maybe<Array<EarlyAccessCode>>;
  earlyAccessCodeCount?: Maybe<Scalars['Int']['output']>;
  email?: Maybe<Scalars['String']['output']>;
  id: Scalars['ID']['output'];
  name?: Maybe<Scalars['String']['output']>;
  phone?: Maybe<Scalars['String']['output']>;
};


export type PartnerEarlyAccessCodeArgs = {
  cursor?: InputMaybe<EarlyAccessCodeWhereUniqueInput>;
  orderBy?: Array<EarlyAccessCodeOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: EarlyAccessCodeWhereInput;
};


export type PartnerEarlyAccessCodeCountArgs = {
  where?: EarlyAccessCodeWhereInput;
};

export type PartnerCreateInput = {
  contactPersonFirstname?: InputMaybe<Scalars['String']['input']>;
  contactPersonSalutation?: InputMaybe<Scalars['String']['input']>;
  contactPersonSurname?: InputMaybe<Scalars['String']['input']>;
  earlyAccessCode?: InputMaybe<EarlyAccessCodeRelateToManyForCreateInput>;
  email?: InputMaybe<Scalars['String']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
  phone?: InputMaybe<Scalars['String']['input']>;
};

export type PartnerOrderByInput = {
  contactPersonFirstname?: InputMaybe<OrderDirection>;
  contactPersonSalutation?: InputMaybe<OrderDirection>;
  contactPersonSurname?: InputMaybe<OrderDirection>;
  email?: InputMaybe<OrderDirection>;
  id?: InputMaybe<OrderDirection>;
  name?: InputMaybe<OrderDirection>;
  phone?: InputMaybe<OrderDirection>;
};

export type PartnerRelateToOneForCreateInput = {
  connect?: InputMaybe<PartnerWhereUniqueInput>;
  create?: InputMaybe<PartnerCreateInput>;
};

export type PartnerRelateToOneForUpdateInput = {
  connect?: InputMaybe<PartnerWhereUniqueInput>;
  create?: InputMaybe<PartnerCreateInput>;
  disconnect?: InputMaybe<Scalars['Boolean']['input']>;
};

export type PartnerUpdateArgs = {
  data: PartnerUpdateInput;
  where: PartnerWhereUniqueInput;
};

export type PartnerUpdateInput = {
  contactPersonFirstname?: InputMaybe<Scalars['String']['input']>;
  contactPersonSalutation?: InputMaybe<Scalars['String']['input']>;
  contactPersonSurname?: InputMaybe<Scalars['String']['input']>;
  earlyAccessCode?: InputMaybe<EarlyAccessCodeRelateToManyForUpdateInput>;
  email?: InputMaybe<Scalars['String']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
  phone?: InputMaybe<Scalars['String']['input']>;
};

export type PartnerWhereInput = {
  AND?: InputMaybe<Array<PartnerWhereInput>>;
  NOT?: InputMaybe<Array<PartnerWhereInput>>;
  OR?: InputMaybe<Array<PartnerWhereInput>>;
  contactPersonFirstname?: InputMaybe<StringFilter>;
  contactPersonSalutation?: InputMaybe<StringNullableFilter>;
  contactPersonSurname?: InputMaybe<StringFilter>;
  earlyAccessCode?: InputMaybe<EarlyAccessCodeManyRelationFilter>;
  email?: InputMaybe<StringFilter>;
  id?: InputMaybe<IdFilter>;
  name?: InputMaybe<StringFilter>;
  phone?: InputMaybe<StringFilter>;
};

export type PartnerWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
};

export type PasswordResetToken = {
  __typename?: 'PasswordResetToken';
  createdAt?: Maybe<Scalars['DateTime']['output']>;
  id: Scalars['ID']['output'];
  user?: Maybe<User>;
  validUntil?: Maybe<Scalars['DateTime']['output']>;
  validationToken?: Maybe<Scalars['String']['output']>;
};

export type PasswordResetTokenCreateInput = {
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  user?: InputMaybe<UserRelateToOneForCreateInput>;
  validUntil?: InputMaybe<Scalars['DateTime']['input']>;
  validationToken?: InputMaybe<Scalars['String']['input']>;
};

export type PasswordResetTokenOrderByInput = {
  createdAt?: InputMaybe<OrderDirection>;
  id?: InputMaybe<OrderDirection>;
  validUntil?: InputMaybe<OrderDirection>;
  validationToken?: InputMaybe<OrderDirection>;
};

export type PasswordResetTokenUpdateArgs = {
  data: PasswordResetTokenUpdateInput;
  where: PasswordResetTokenWhereUniqueInput;
};

export type PasswordResetTokenUpdateInput = {
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  user?: InputMaybe<UserRelateToOneForUpdateInput>;
  validUntil?: InputMaybe<Scalars['DateTime']['input']>;
  validationToken?: InputMaybe<Scalars['String']['input']>;
};

export type PasswordResetTokenWhereInput = {
  AND?: InputMaybe<Array<PasswordResetTokenWhereInput>>;
  NOT?: InputMaybe<Array<PasswordResetTokenWhereInput>>;
  OR?: InputMaybe<Array<PasswordResetTokenWhereInput>>;
  createdAt?: InputMaybe<DateTimeFilter>;
  id?: InputMaybe<IdFilter>;
  user?: InputMaybe<UserWhereInput>;
  validUntil?: InputMaybe<DateTimeFilter>;
  validationToken?: InputMaybe<StringFilter>;
};

export type PasswordResetTokenWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
  validationToken?: InputMaybe<Scalars['String']['input']>;
};

export type PasswordState = {
  __typename?: 'PasswordState';
  isSet: Scalars['Boolean']['output'];
};

export type Period = {
  __typename?: 'Period';
  carePeriods?: Maybe<Array<CarePeriod>>;
  carePeriodsCount?: Maybe<Scalars['Int']['output']>;
  displayName?: Maybe<Scalars['String']['output']>;
  displayNameWithHoliday?: Maybe<Scalars['String']['output']>;
  endDate?: Maybe<Scalars['CalendarDay']['output']>;
  holiday?: Maybe<Holiday>;
  id: Scalars['ID']['output'];
  season?: Maybe<Season>;
  startDate?: Maybe<Scalars['CalendarDay']['output']>;
};


export type PeriodCarePeriodsArgs = {
  cursor?: InputMaybe<CarePeriodWhereUniqueInput>;
  orderBy?: Array<CarePeriodOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: CarePeriodWhereInput;
};


export type PeriodCarePeriodsCountArgs = {
  where?: CarePeriodWhereInput;
};

export type PeriodCreateInput = {
  carePeriods?: InputMaybe<CarePeriodRelateToManyForCreateInput>;
  endDate?: InputMaybe<Scalars['CalendarDay']['input']>;
  holiday?: InputMaybe<HolidayRelateToOneForCreateInput>;
  season?: InputMaybe<SeasonRelateToOneForCreateInput>;
  startDate?: InputMaybe<Scalars['CalendarDay']['input']>;
};

export type PeriodManyRelationFilter = {
  every?: InputMaybe<PeriodWhereInput>;
  none?: InputMaybe<PeriodWhereInput>;
  some?: InputMaybe<PeriodWhereInput>;
};

export type PeriodOrderByInput = {
  endDate?: InputMaybe<OrderDirection>;
  id?: InputMaybe<OrderDirection>;
  startDate?: InputMaybe<OrderDirection>;
};

export type PeriodRelateToManyForCreateInput = {
  connect?: InputMaybe<Array<PeriodWhereUniqueInput>>;
  create?: InputMaybe<Array<PeriodCreateInput>>;
};

export type PeriodRelateToManyForUpdateInput = {
  connect?: InputMaybe<Array<PeriodWhereUniqueInput>>;
  create?: InputMaybe<Array<PeriodCreateInput>>;
  disconnect?: InputMaybe<Array<PeriodWhereUniqueInput>>;
  set?: InputMaybe<Array<PeriodWhereUniqueInput>>;
};

export type PeriodRelateToOneForCreateInput = {
  connect?: InputMaybe<PeriodWhereUniqueInput>;
  create?: InputMaybe<PeriodCreateInput>;
};

export type PeriodRelateToOneForUpdateInput = {
  connect?: InputMaybe<PeriodWhereUniqueInput>;
  create?: InputMaybe<PeriodCreateInput>;
  disconnect?: InputMaybe<Scalars['Boolean']['input']>;
};

export type PeriodUpdateArgs = {
  data: PeriodUpdateInput;
  where: PeriodWhereUniqueInput;
};

export type PeriodUpdateInput = {
  carePeriods?: InputMaybe<CarePeriodRelateToManyForUpdateInput>;
  endDate?: InputMaybe<Scalars['CalendarDay']['input']>;
  holiday?: InputMaybe<HolidayRelateToOneForUpdateInput>;
  season?: InputMaybe<SeasonRelateToOneForUpdateInput>;
  startDate?: InputMaybe<Scalars['CalendarDay']['input']>;
};

export type PeriodWhereInput = {
  AND?: InputMaybe<Array<PeriodWhereInput>>;
  NOT?: InputMaybe<Array<PeriodWhereInput>>;
  OR?: InputMaybe<Array<PeriodWhereInput>>;
  carePeriods?: InputMaybe<CarePeriodManyRelationFilter>;
  endDate?: InputMaybe<CalendarDayFilter>;
  holiday?: InputMaybe<HolidayWhereInput>;
  id?: InputMaybe<IdFilter>;
  season?: InputMaybe<SeasonWhereInput>;
  startDate?: InputMaybe<CalendarDayFilter>;
};

export type PeriodWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
};

export type Query = {
  __typename?: 'Query';
  authenticatedItem?: Maybe<AuthenticatedItem>;
  availableTicketCount: Array<Maybe<AvailableTicketCountResult>>;
  booking?: Maybe<Booking>;
  bookings?: Maybe<Array<Booking>>;
  bookingsCount?: Maybe<Scalars['Int']['output']>;
  careDay?: Maybe<CareDay>;
  careDays?: Maybe<Array<CareDay>>;
  careDaysCount?: Maybe<Scalars['Int']['output']>;
  carePeriod?: Maybe<CarePeriod>;
  carePeriods?: Maybe<Array<CarePeriod>>;
  carePeriodsCount?: Maybe<Scalars['Int']['output']>;
  cart?: Maybe<Cart>;
  cartTicket?: Maybe<CartTicket>;
  cartTickets?: Maybe<Array<CartTicket>>;
  cartTicketsCount?: Maybe<Scalars['Int']['output']>;
  carts?: Maybe<Array<Cart>>;
  cartsCount?: Maybe<Scalars['Int']['output']>;
  child?: Maybe<Child>;
  children?: Maybe<Array<Child>>;
  childrenCount?: Maybe<Scalars['Int']['output']>;
  contact?: Maybe<Contact>;
  contacts?: Maybe<Array<Contact>>;
  contactsCount?: Maybe<Scalars['Int']['output']>;
  custodian?: Maybe<Custodian>;
  custodians?: Maybe<Array<Custodian>>;
  custodiansCount?: Maybe<Scalars['Int']['output']>;
  discount?: Maybe<Discount>;
  discountRejectionReason?: Maybe<DiscountRejectionReason>;
  discountRejectionReasons?: Maybe<Array<DiscountRejectionReason>>;
  discountRejectionReasonsCount?: Maybe<Scalars['Int']['output']>;
  discountType?: Maybe<DiscountType>;
  discountTypes?: Maybe<Array<DiscountType>>;
  discountTypesCount?: Maybe<Scalars['Int']['output']>;
  discountVoucher?: Maybe<DiscountVoucher>;
  discountVouchers?: Maybe<Array<DiscountVoucher>>;
  discountVouchersCount?: Maybe<Scalars['Int']['output']>;
  discounts?: Maybe<Array<Discount>>;
  discountsCount?: Maybe<Scalars['Int']['output']>;
  earlyAccessCode?: Maybe<EarlyAccessCode>;
  earlyAccessCodeVoucher?: Maybe<EarlyAccessCodeVoucher>;
  earlyAccessCodeVouchers?: Maybe<Array<EarlyAccessCodeVoucher>>;
  earlyAccessCodeVouchersCount?: Maybe<Scalars['Int']['output']>;
  earlyAccessCodes?: Maybe<Array<EarlyAccessCode>>;
  earlyAccessCodesCount?: Maybe<Scalars['Int']['output']>;
  emailTemplate?: Maybe<EmailTemplate>;
  emailTemplates?: Maybe<Array<EmailTemplate>>;
  emailTemplatesCount?: Maybe<Scalars['Int']['output']>;
  event?: Maybe<Event>;
  eventCategories?: Maybe<Array<EventCategory>>;
  eventCategoriesCount?: Maybe<Scalars['Int']['output']>;
  eventCategory?: Maybe<EventCategory>;
  eventTicket?: Maybe<EventTicket>;
  eventTickets?: Maybe<Array<EventTicket>>;
  eventTicketsCount?: Maybe<Scalars['Int']['output']>;
  events?: Maybe<Array<Event>>;
  eventsCount?: Maybe<Scalars['Int']['output']>;
  holiday?: Maybe<Holiday>;
  holidays?: Maybe<Array<Holiday>>;
  holidaysCount?: Maybe<Scalars['Int']['output']>;
  invoice?: Maybe<Invoice>;
  invoices?: Maybe<Array<Invoice>>;
  invoicesCount?: Maybe<Scalars['Int']['output']>;
  keystone: KeystoneMeta;
  organizer?: Maybe<Organizer>;
  organizers?: Maybe<Array<Organizer>>;
  organizersCount?: Maybe<Scalars['Int']['output']>;
  partner?: Maybe<Partner>;
  partners?: Maybe<Array<Partner>>;
  partnersCount?: Maybe<Scalars['Int']['output']>;
  passwordResetToken?: Maybe<PasswordResetToken>;
  passwordResetTokens?: Maybe<Array<PasswordResetToken>>;
  passwordResetTokensCount?: Maybe<Scalars['Int']['output']>;
  period?: Maybe<Period>;
  periods?: Maybe<Array<Period>>;
  periodsCount?: Maybe<Scalars['Int']['output']>;
  reservation?: Maybe<Reservation>;
  reservations?: Maybe<Array<Reservation>>;
  reservationsCount?: Maybe<Scalars['Int']['output']>;
  season?: Maybe<Season>;
  seasons?: Maybe<Array<Season>>;
  seasonsCount?: Maybe<Scalars['Int']['output']>;
  ticket?: Maybe<Ticket>;
  tickets?: Maybe<Array<Ticket>>;
  ticketsCount?: Maybe<Scalars['Int']['output']>;
  user?: Maybe<User>;
  userVerificationToken?: Maybe<UserVerificationToken>;
  userVerificationTokens?: Maybe<Array<UserVerificationToken>>;
  userVerificationTokensCount?: Maybe<Scalars['Int']['output']>;
  users?: Maybe<Array<User>>;
  usersCount?: Maybe<Scalars['Int']['output']>;
};


export type QueryAvailableTicketCountArgs = {
  year: Scalars['Int']['input'];
};


export type QueryBookingArgs = {
  where: BookingWhereUniqueInput;
};


export type QueryBookingsArgs = {
  cursor?: InputMaybe<BookingWhereUniqueInput>;
  orderBy?: Array<BookingOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: BookingWhereInput;
};


export type QueryBookingsCountArgs = {
  where?: BookingWhereInput;
};


export type QueryCareDayArgs = {
  where: CareDayWhereUniqueInput;
};


export type QueryCareDaysArgs = {
  cursor?: InputMaybe<CareDayWhereUniqueInput>;
  orderBy?: Array<CareDayOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: CareDayWhereInput;
};


export type QueryCareDaysCountArgs = {
  where?: CareDayWhereInput;
};


export type QueryCarePeriodArgs = {
  where: CarePeriodWhereUniqueInput;
};


export type QueryCarePeriodsArgs = {
  cursor?: InputMaybe<CarePeriodWhereUniqueInput>;
  orderBy?: Array<CarePeriodOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: CarePeriodWhereInput;
};


export type QueryCarePeriodsCountArgs = {
  where?: CarePeriodWhereInput;
};


export type QueryCartArgs = {
  where: CartWhereUniqueInput;
};


export type QueryCartTicketArgs = {
  where: CartTicketWhereUniqueInput;
};


export type QueryCartTicketsArgs = {
  cursor?: InputMaybe<CartTicketWhereUniqueInput>;
  orderBy?: Array<CartTicketOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: CartTicketWhereInput;
};


export type QueryCartTicketsCountArgs = {
  where?: CartTicketWhereInput;
};


export type QueryCartsArgs = {
  cursor?: InputMaybe<CartWhereUniqueInput>;
  orderBy?: Array<CartOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: CartWhereInput;
};


export type QueryCartsCountArgs = {
  where?: CartWhereInput;
};


export type QueryChildArgs = {
  where: ChildWhereUniqueInput;
};


export type QueryChildrenArgs = {
  cursor?: InputMaybe<ChildWhereUniqueInput>;
  orderBy?: Array<ChildOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: ChildWhereInput;
};


export type QueryChildrenCountArgs = {
  where?: ChildWhereInput;
};


export type QueryContactArgs = {
  where: ContactWhereUniqueInput;
};


export type QueryContactsArgs = {
  cursor?: InputMaybe<ContactWhereUniqueInput>;
  orderBy?: Array<ContactOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: ContactWhereInput;
};


export type QueryContactsCountArgs = {
  where?: ContactWhereInput;
};


export type QueryCustodianArgs = {
  where: CustodianWhereUniqueInput;
};


export type QueryCustodiansArgs = {
  cursor?: InputMaybe<CustodianWhereUniqueInput>;
  orderBy?: Array<CustodianOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: CustodianWhereInput;
};


export type QueryCustodiansCountArgs = {
  where?: CustodianWhereInput;
};


export type QueryDiscountArgs = {
  where: DiscountWhereUniqueInput;
};


export type QueryDiscountRejectionReasonArgs = {
  where: DiscountRejectionReasonWhereUniqueInput;
};


export type QueryDiscountRejectionReasonsArgs = {
  cursor?: InputMaybe<DiscountRejectionReasonWhereUniqueInput>;
  orderBy?: Array<DiscountRejectionReasonOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: DiscountRejectionReasonWhereInput;
};


export type QueryDiscountRejectionReasonsCountArgs = {
  where?: DiscountRejectionReasonWhereInput;
};


export type QueryDiscountTypeArgs = {
  where: DiscountTypeWhereUniqueInput;
};


export type QueryDiscountTypesArgs = {
  cursor?: InputMaybe<DiscountTypeWhereUniqueInput>;
  orderBy?: Array<DiscountTypeOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: DiscountTypeWhereInput;
};


export type QueryDiscountTypesCountArgs = {
  where?: DiscountTypeWhereInput;
};


export type QueryDiscountVoucherArgs = {
  where: DiscountVoucherWhereUniqueInput;
};


export type QueryDiscountVouchersArgs = {
  cursor?: InputMaybe<DiscountVoucherWhereUniqueInput>;
  orderBy?: Array<DiscountVoucherOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: DiscountVoucherWhereInput;
};


export type QueryDiscountVouchersCountArgs = {
  where?: DiscountVoucherWhereInput;
};


export type QueryDiscountsArgs = {
  cursor?: InputMaybe<DiscountWhereUniqueInput>;
  orderBy?: Array<DiscountOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: DiscountWhereInput;
};


export type QueryDiscountsCountArgs = {
  where?: DiscountWhereInput;
};


export type QueryEarlyAccessCodeArgs = {
  where: EarlyAccessCodeWhereUniqueInput;
};


export type QueryEarlyAccessCodeVoucherArgs = {
  where: EarlyAccessCodeVoucherWhereUniqueInput;
};


export type QueryEarlyAccessCodeVouchersArgs = {
  cursor?: InputMaybe<EarlyAccessCodeVoucherWhereUniqueInput>;
  orderBy?: Array<EarlyAccessCodeVoucherOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: EarlyAccessCodeVoucherWhereInput;
};


export type QueryEarlyAccessCodeVouchersCountArgs = {
  where?: EarlyAccessCodeVoucherWhereInput;
};


export type QueryEarlyAccessCodesArgs = {
  cursor?: InputMaybe<EarlyAccessCodeWhereUniqueInput>;
  orderBy?: Array<EarlyAccessCodeOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: EarlyAccessCodeWhereInput;
};


export type QueryEarlyAccessCodesCountArgs = {
  where?: EarlyAccessCodeWhereInput;
};


export type QueryEmailTemplateArgs = {
  where: EmailTemplateWhereUniqueInput;
};


export type QueryEmailTemplatesArgs = {
  cursor?: InputMaybe<EmailTemplateWhereUniqueInput>;
  orderBy?: Array<EmailTemplateOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: EmailTemplateWhereInput;
};


export type QueryEmailTemplatesCountArgs = {
  where?: EmailTemplateWhereInput;
};


export type QueryEventArgs = {
  where: EventWhereUniqueInput;
};


export type QueryEventCategoriesArgs = {
  cursor?: InputMaybe<EventCategoryWhereUniqueInput>;
  orderBy?: Array<EventCategoryOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: EventCategoryWhereInput;
};


export type QueryEventCategoriesCountArgs = {
  where?: EventCategoryWhereInput;
};


export type QueryEventCategoryArgs = {
  where: EventCategoryWhereUniqueInput;
};


export type QueryEventTicketArgs = {
  where: EventTicketWhereUniqueInput;
};


export type QueryEventTicketsArgs = {
  cursor?: InputMaybe<EventTicketWhereUniqueInput>;
  orderBy?: Array<EventTicketOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: EventTicketWhereInput;
};


export type QueryEventTicketsCountArgs = {
  where?: EventTicketWhereInput;
};


export type QueryEventsArgs = {
  cursor?: InputMaybe<EventWhereUniqueInput>;
  orderBy?: Array<EventOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: EventWhereInput;
};


export type QueryEventsCountArgs = {
  where?: EventWhereInput;
};


export type QueryHolidayArgs = {
  where: HolidayWhereUniqueInput;
};


export type QueryHolidaysArgs = {
  cursor?: InputMaybe<HolidayWhereUniqueInput>;
  orderBy?: Array<HolidayOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: HolidayWhereInput;
};


export type QueryHolidaysCountArgs = {
  where?: HolidayWhereInput;
};


export type QueryInvoiceArgs = {
  where: InvoiceWhereUniqueInput;
};


export type QueryInvoicesArgs = {
  cursor?: InputMaybe<InvoiceWhereUniqueInput>;
  orderBy?: Array<InvoiceOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: InvoiceWhereInput;
};


export type QueryInvoicesCountArgs = {
  where?: InvoiceWhereInput;
};


export type QueryOrganizerArgs = {
  where: OrganizerWhereUniqueInput;
};


export type QueryOrganizersArgs = {
  cursor?: InputMaybe<OrganizerWhereUniqueInput>;
  orderBy?: Array<OrganizerOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: OrganizerWhereInput;
};


export type QueryOrganizersCountArgs = {
  where?: OrganizerWhereInput;
};


export type QueryPartnerArgs = {
  where: PartnerWhereUniqueInput;
};


export type QueryPartnersArgs = {
  cursor?: InputMaybe<PartnerWhereUniqueInput>;
  orderBy?: Array<PartnerOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: PartnerWhereInput;
};


export type QueryPartnersCountArgs = {
  where?: PartnerWhereInput;
};


export type QueryPasswordResetTokenArgs = {
  where: PasswordResetTokenWhereUniqueInput;
};


export type QueryPasswordResetTokensArgs = {
  cursor?: InputMaybe<PasswordResetTokenWhereUniqueInput>;
  orderBy?: Array<PasswordResetTokenOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: PasswordResetTokenWhereInput;
};


export type QueryPasswordResetTokensCountArgs = {
  where?: PasswordResetTokenWhereInput;
};


export type QueryPeriodArgs = {
  where: PeriodWhereUniqueInput;
};


export type QueryPeriodsArgs = {
  cursor?: InputMaybe<PeriodWhereUniqueInput>;
  orderBy?: Array<PeriodOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: PeriodWhereInput;
};


export type QueryPeriodsCountArgs = {
  where?: PeriodWhereInput;
};


export type QueryReservationArgs = {
  where: ReservationWhereUniqueInput;
};


export type QueryReservationsArgs = {
  cursor?: InputMaybe<ReservationWhereUniqueInput>;
  orderBy?: Array<ReservationOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: ReservationWhereInput;
};


export type QueryReservationsCountArgs = {
  where?: ReservationWhereInput;
};


export type QuerySeasonArgs = {
  where: SeasonWhereUniqueInput;
};


export type QuerySeasonsArgs = {
  cursor?: InputMaybe<SeasonWhereUniqueInput>;
  orderBy?: Array<SeasonOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: SeasonWhereInput;
};


export type QuerySeasonsCountArgs = {
  where?: SeasonWhereInput;
};


export type QueryTicketArgs = {
  where: TicketWhereUniqueInput;
};


export type QueryTicketsArgs = {
  cursor?: InputMaybe<TicketWhereUniqueInput>;
  orderBy?: Array<TicketOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: TicketWhereInput;
};


export type QueryTicketsCountArgs = {
  where?: TicketWhereInput;
};


export type QueryUserArgs = {
  where: UserWhereUniqueInput;
};


export type QueryUserVerificationTokenArgs = {
  where: UserVerificationTokenWhereUniqueInput;
};


export type QueryUserVerificationTokensArgs = {
  cursor?: InputMaybe<UserVerificationTokenWhereUniqueInput>;
  orderBy?: Array<UserVerificationTokenOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: UserVerificationTokenWhereInput;
};


export type QueryUserVerificationTokensCountArgs = {
  where?: UserVerificationTokenWhereInput;
};


export type QueryUsersArgs = {
  cursor?: InputMaybe<UserWhereUniqueInput>;
  orderBy?: Array<UserOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: UserWhereInput;
};


export type QueryUsersCountArgs = {
  where?: UserWhereInput;
};

export enum QueryMode {
  Default = 'default',
  Insensitive = 'insensitive'
}

export type RedeemEarlyAccessCodeFailed = {
  __typename?: 'RedeemEarlyAccessCodeFailed';
  reason: Scalars['String']['output'];
};

export type RedeemEarlyAccessCodeResult = RedeemEarlyAccessCodeFailed | RedeemEarlyAccessCodeSuccess;

export type RedeemEarlyAccessCodeSuccess = {
  __typename?: 'RedeemEarlyAccessCodeSuccess';
  earlyAccessCodeVoucher: EarlyAccessCodeVoucher;
};

export type RegisterFailed = {
  __typename?: 'RegisterFailed';
  reason: Scalars['String']['output'];
};

export type RegisterInput = {
  addressAdditional: Scalars['String']['input'];
  addressCity: Scalars['String']['input'];
  addressHouseNumber: Scalars['String']['input'];
  addressStreet: Scalars['String']['input'];
  addressZip: Scalars['String']['input'];
  agreeDataProtection: Scalars['Boolean']['input'];
  email: Scalars['String']['input'];
  emergencyPhone: Scalars['String']['input'];
  firstname: Scalars['String']['input'];
  password: Scalars['String']['input'];
  phone: Scalars['String']['input'];
  salutation: Scalars['String']['input'];
  surname: Scalars['String']['input'];
};

export type RegisterPaymentFailed = {
  __typename?: 'RegisterPaymentFailed';
  reason: Scalars['String']['output'];
};

export type RegisterPaymentResult = RegisterPaymentFailed | RegisterPaymentSuccess;

export type RegisterPaymentSuccess = {
  __typename?: 'RegisterPaymentSuccess';
  booking: Booking;
};

export type RegisterResult = RegisterFailed | RegisterSuccess;

export type RegisterSuccess = {
  __typename?: 'RegisterSuccess';
  user: User;
};

export type RejectDiscountFailed = {
  __typename?: 'RejectDiscountFailed';
  reason: Scalars['String']['output'];
};

export type RejectDiscountResult = RejectDiscountFailed | RejectDiscountSuccess;

export type RejectDiscountSuccess = {
  __typename?: 'RejectDiscountSuccess';
  discount: Discount;
};

export type RequestPasswordResetTokenFailed = {
  __typename?: 'RequestPasswordResetTokenFailed';
  reason: Scalars['String']['output'];
};

export type RequestPasswordResetTokenInput = {
  email: Scalars['String']['input'];
};

export type RequestPasswordResetTokenResult = RequestPasswordResetTokenFailed | RequestPasswordResetTokenSuccess;

export type RequestPasswordResetTokenSuccess = {
  __typename?: 'RequestPasswordResetTokenSuccess';
  success: Scalars['Boolean']['output'];
};

export type ResendVerificationCodeFailed = {
  __typename?: 'ResendVerificationCodeFailed';
  reason: Scalars['String']['output'];
};

export type ResendVerificationCodeInput = {
  email: Scalars['String']['input'];
};

export type ResendVerificationCodeResult = ResendVerificationCodeFailed | ResendVerificationCodeSuccess;

export type ResendVerificationCodeSuccess = {
  __typename?: 'ResendVerificationCodeSuccess';
  success: Scalars['Boolean']['output'];
};

export type Reservation = {
  __typename?: 'Reservation';
  code?: Maybe<Scalars['String']['output']>;
  custodian?: Maybe<Custodian>;
  date?: Maybe<Scalars['DateTime']['output']>;
  event?: Maybe<Event>;
  id: Scalars['ID']['output'];
  isDeleted?: Maybe<Scalars['Boolean']['output']>;
  price?: Maybe<Scalars['Float']['output']>;
  tickets?: Maybe<Array<Ticket>>;
  ticketsCount?: Maybe<Scalars['Int']['output']>;
};


export type ReservationTicketsArgs = {
  cursor?: InputMaybe<TicketWhereUniqueInput>;
  orderBy?: Array<TicketOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: TicketWhereInput;
};


export type ReservationTicketsCountArgs = {
  where?: TicketWhereInput;
};

export type ReservationCreateInput = {
  code?: InputMaybe<Scalars['String']['input']>;
  custodian?: InputMaybe<CustodianRelateToOneForCreateInput>;
  date?: InputMaybe<Scalars['DateTime']['input']>;
  event?: InputMaybe<EventRelateToOneForCreateInput>;
  isDeleted?: InputMaybe<Scalars['Boolean']['input']>;
  tickets?: InputMaybe<TicketRelateToManyForCreateInput>;
};

export type ReservationManyRelationFilter = {
  every?: InputMaybe<ReservationWhereInput>;
  none?: InputMaybe<ReservationWhereInput>;
  some?: InputMaybe<ReservationWhereInput>;
};

export type ReservationOrderByInput = {
  code?: InputMaybe<OrderDirection>;
  date?: InputMaybe<OrderDirection>;
  id?: InputMaybe<OrderDirection>;
  isDeleted?: InputMaybe<OrderDirection>;
};

export type ReservationRelateToManyForCreateInput = {
  connect?: InputMaybe<Array<ReservationWhereUniqueInput>>;
  create?: InputMaybe<Array<ReservationCreateInput>>;
};

export type ReservationRelateToManyForUpdateInput = {
  connect?: InputMaybe<Array<ReservationWhereUniqueInput>>;
  create?: InputMaybe<Array<ReservationCreateInput>>;
  disconnect?: InputMaybe<Array<ReservationWhereUniqueInput>>;
  set?: InputMaybe<Array<ReservationWhereUniqueInput>>;
};

export type ReservationRelateToOneForCreateInput = {
  connect?: InputMaybe<ReservationWhereUniqueInput>;
  create?: InputMaybe<ReservationCreateInput>;
};

export type ReservationRelateToOneForUpdateInput = {
  connect?: InputMaybe<ReservationWhereUniqueInput>;
  create?: InputMaybe<ReservationCreateInput>;
  disconnect?: InputMaybe<Scalars['Boolean']['input']>;
};

export type ReservationUpdateArgs = {
  data: ReservationUpdateInput;
  where: ReservationWhereUniqueInput;
};

export type ReservationUpdateInput = {
  code?: InputMaybe<Scalars['String']['input']>;
  custodian?: InputMaybe<CustodianRelateToOneForUpdateInput>;
  date?: InputMaybe<Scalars['DateTime']['input']>;
  event?: InputMaybe<EventRelateToOneForUpdateInput>;
  isDeleted?: InputMaybe<Scalars['Boolean']['input']>;
  tickets?: InputMaybe<TicketRelateToManyForUpdateInput>;
};

export type ReservationWhereInput = {
  AND?: InputMaybe<Array<ReservationWhereInput>>;
  NOT?: InputMaybe<Array<ReservationWhereInput>>;
  OR?: InputMaybe<Array<ReservationWhereInput>>;
  code?: InputMaybe<StringFilter>;
  custodian?: InputMaybe<CustodianWhereInput>;
  date?: InputMaybe<DateTimeNullableFilter>;
  event?: InputMaybe<EventWhereInput>;
  id?: InputMaybe<IdFilter>;
  isDeleted?: InputMaybe<BooleanFilter>;
  tickets?: InputMaybe<TicketManyRelationFilter>;
};

export type ReservationWhereUniqueInput = {
  code?: InputMaybe<Scalars['String']['input']>;
  id?: InputMaybe<Scalars['ID']['input']>;
};

export type ResetPasswordFailed = {
  __typename?: 'ResetPasswordFailed';
  reason: Scalars['String']['output'];
};

export type ResetPasswordResult = ResetPasswordFailed | ResetPasswordSuccess;

export type ResetPasswordSuccess = {
  __typename?: 'ResetPasswordSuccess';
  user: User;
};

export type Season = {
  __typename?: 'Season';
  bookingStart?: Maybe<Scalars['DateTime']['output']>;
  bookingStartPartners?: Maybe<Scalars['DateTime']['output']>;
  grantAmount?: Maybe<Scalars['Float']['output']>;
  id: Scalars['ID']['output'];
  period?: Maybe<Array<Period>>;
  periodCount?: Maybe<Scalars['Int']['output']>;
  releaseDate?: Maybe<Scalars['DateTime']['output']>;
  year?: Maybe<Scalars['Int']['output']>;
};


export type SeasonPeriodArgs = {
  cursor?: InputMaybe<PeriodWhereUniqueInput>;
  orderBy?: Array<PeriodOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: PeriodWhereInput;
};


export type SeasonPeriodCountArgs = {
  where?: PeriodWhereInput;
};

export type SeasonCreateInput = {
  bookingStart?: InputMaybe<Scalars['DateTime']['input']>;
  bookingStartPartners?: InputMaybe<Scalars['DateTime']['input']>;
  grantAmount?: InputMaybe<Scalars['Float']['input']>;
  period?: InputMaybe<PeriodRelateToManyForCreateInput>;
  releaseDate?: InputMaybe<Scalars['DateTime']['input']>;
  year?: InputMaybe<Scalars['Int']['input']>;
};

export type SeasonOrderByInput = {
  bookingStart?: InputMaybe<OrderDirection>;
  bookingStartPartners?: InputMaybe<OrderDirection>;
  grantAmount?: InputMaybe<OrderDirection>;
  id?: InputMaybe<OrderDirection>;
  releaseDate?: InputMaybe<OrderDirection>;
  year?: InputMaybe<OrderDirection>;
};

export type SeasonRelateToOneForCreateInput = {
  connect?: InputMaybe<SeasonWhereUniqueInput>;
  create?: InputMaybe<SeasonCreateInput>;
};

export type SeasonRelateToOneForUpdateInput = {
  connect?: InputMaybe<SeasonWhereUniqueInput>;
  create?: InputMaybe<SeasonCreateInput>;
  disconnect?: InputMaybe<Scalars['Boolean']['input']>;
};

export type SeasonUpdateArgs = {
  data: SeasonUpdateInput;
  where: SeasonWhereUniqueInput;
};

export type SeasonUpdateInput = {
  bookingStart?: InputMaybe<Scalars['DateTime']['input']>;
  bookingStartPartners?: InputMaybe<Scalars['DateTime']['input']>;
  grantAmount?: InputMaybe<Scalars['Float']['input']>;
  period?: InputMaybe<PeriodRelateToManyForUpdateInput>;
  releaseDate?: InputMaybe<Scalars['DateTime']['input']>;
  year?: InputMaybe<Scalars['Int']['input']>;
};

export type SeasonWhereInput = {
  AND?: InputMaybe<Array<SeasonWhereInput>>;
  NOT?: InputMaybe<Array<SeasonWhereInput>>;
  OR?: InputMaybe<Array<SeasonWhereInput>>;
  bookingStart?: InputMaybe<DateTimeNullableFilter>;
  bookingStartPartners?: InputMaybe<DateTimeNullableFilter>;
  grantAmount?: InputMaybe<FloatNullableFilter>;
  id?: InputMaybe<IdFilter>;
  period?: InputMaybe<PeriodManyRelationFilter>;
  releaseDate?: InputMaybe<DateTimeNullableFilter>;
  year?: InputMaybe<IntNullableFilter>;
};

export type SeasonWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
  year?: InputMaybe<Scalars['Int']['input']>;
};

export type StringFilter = {
  contains?: InputMaybe<Scalars['String']['input']>;
  endsWith?: InputMaybe<Scalars['String']['input']>;
  equals?: InputMaybe<Scalars['String']['input']>;
  gt?: InputMaybe<Scalars['String']['input']>;
  gte?: InputMaybe<Scalars['String']['input']>;
  in?: InputMaybe<Array<Scalars['String']['input']>>;
  lt?: InputMaybe<Scalars['String']['input']>;
  lte?: InputMaybe<Scalars['String']['input']>;
  mode?: InputMaybe<QueryMode>;
  not?: InputMaybe<NestedStringFilter>;
  notIn?: InputMaybe<Array<Scalars['String']['input']>>;
  startsWith?: InputMaybe<Scalars['String']['input']>;
};

export type StringNullableFilter = {
  contains?: InputMaybe<Scalars['String']['input']>;
  endsWith?: InputMaybe<Scalars['String']['input']>;
  equals?: InputMaybe<Scalars['String']['input']>;
  gt?: InputMaybe<Scalars['String']['input']>;
  gte?: InputMaybe<Scalars['String']['input']>;
  in?: InputMaybe<Array<Scalars['String']['input']>>;
  lt?: InputMaybe<Scalars['String']['input']>;
  lte?: InputMaybe<Scalars['String']['input']>;
  mode?: InputMaybe<QueryMode>;
  not?: InputMaybe<StringNullableFilter>;
  notIn?: InputMaybe<Array<Scalars['String']['input']>>;
  startsWith?: InputMaybe<Scalars['String']['input']>;
};

export type Ticket = {
  __typename?: 'Ticket';
  booking?: Maybe<Booking>;
  child?: Maybe<Child>;
  discountVoucher?: Maybe<DiscountVoucher>;
  eventTicket?: Maybe<EventTicket>;
  id: Scalars['ID']['output'];
  isDeleted?: Maybe<Scalars['Boolean']['output']>;
  price?: Maybe<Scalars['Float']['output']>;
  reservation?: Maybe<Reservation>;
};

export type TicketCreateInput = {
  booking?: InputMaybe<BookingRelateToOneForCreateInput>;
  child?: InputMaybe<ChildRelateToOneForCreateInput>;
  discountVoucher?: InputMaybe<DiscountVoucherRelateToOneForCreateInput>;
  eventTicket?: InputMaybe<EventTicketRelateToOneForCreateInput>;
  isDeleted?: InputMaybe<Scalars['Boolean']['input']>;
  price?: InputMaybe<Scalars['Float']['input']>;
  reservation?: InputMaybe<ReservationRelateToOneForCreateInput>;
};

export type TicketManyRelationFilter = {
  every?: InputMaybe<TicketWhereInput>;
  none?: InputMaybe<TicketWhereInput>;
  some?: InputMaybe<TicketWhereInput>;
};

export type TicketOrderByInput = {
  id?: InputMaybe<OrderDirection>;
  isDeleted?: InputMaybe<OrderDirection>;
  price?: InputMaybe<OrderDirection>;
};

export type TicketRelateToManyForCreateInput = {
  connect?: InputMaybe<Array<TicketWhereUniqueInput>>;
  create?: InputMaybe<Array<TicketCreateInput>>;
};

export type TicketRelateToManyForUpdateInput = {
  connect?: InputMaybe<Array<TicketWhereUniqueInput>>;
  create?: InputMaybe<Array<TicketCreateInput>>;
  disconnect?: InputMaybe<Array<TicketWhereUniqueInput>>;
  set?: InputMaybe<Array<TicketWhereUniqueInput>>;
};

export type TicketRelateToOneForCreateInput = {
  connect?: InputMaybe<TicketWhereUniqueInput>;
  create?: InputMaybe<TicketCreateInput>;
};

export type TicketRelateToOneForUpdateInput = {
  connect?: InputMaybe<TicketWhereUniqueInput>;
  create?: InputMaybe<TicketCreateInput>;
  disconnect?: InputMaybe<Scalars['Boolean']['input']>;
};

export enum TicketType {
  Participant = 'participant',
  Waitinglist = 'waitinglist'
}

export type TicketUpdateArgs = {
  data: TicketUpdateInput;
  where: TicketWhereUniqueInput;
};

export type TicketUpdateInput = {
  booking?: InputMaybe<BookingRelateToOneForUpdateInput>;
  child?: InputMaybe<ChildRelateToOneForUpdateInput>;
  discountVoucher?: InputMaybe<DiscountVoucherRelateToOneForUpdateInput>;
  eventTicket?: InputMaybe<EventTicketRelateToOneForUpdateInput>;
  isDeleted?: InputMaybe<Scalars['Boolean']['input']>;
  price?: InputMaybe<Scalars['Float']['input']>;
  reservation?: InputMaybe<ReservationRelateToOneForUpdateInput>;
};

export type TicketWhereInput = {
  AND?: InputMaybe<Array<TicketWhereInput>>;
  NOT?: InputMaybe<Array<TicketWhereInput>>;
  OR?: InputMaybe<Array<TicketWhereInput>>;
  booking?: InputMaybe<BookingWhereInput>;
  child?: InputMaybe<ChildWhereInput>;
  discountVoucher?: InputMaybe<DiscountVoucherWhereInput>;
  eventTicket?: InputMaybe<EventTicketWhereInput>;
  id?: InputMaybe<IdFilter>;
  isDeleted?: InputMaybe<BooleanFilter>;
  price?: InputMaybe<FloatNullableFilter>;
  reservation?: InputMaybe<ReservationWhereInput>;
};

export type TicketWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
};

export type UnblockEventTicketFailed = {
  __typename?: 'UnblockEventTicketFailed';
  reason: Scalars['String']['output'];
};

export type UnblockEventTicketResult = UnblockEventTicketFailed | UnblockEventTicketSuccess;

export type UnblockEventTicketSuccess = {
  __typename?: 'UnblockEventTicketSuccess';
  eventTicket: EventTicket;
};

export type User = {
  __typename?: 'User';
  createdAt?: Maybe<Scalars['DateTime']['output']>;
  email?: Maybe<Scalars['String']['output']>;
  id: Scalars['ID']['output'];
  isAdministrator?: Maybe<Scalars['Boolean']['output']>;
  isEnabled?: Maybe<Scalars['Boolean']['output']>;
  isTester?: Maybe<Scalars['Boolean']['output']>;
  lastLogin?: Maybe<Scalars['DateTime']['output']>;
  markedForDeletionAt?: Maybe<Scalars['DateTime']['output']>;
  name?: Maybe<Scalars['String']['output']>;
  organizer?: Maybe<Organizer>;
  password?: Maybe<PasswordState>;
  role?: Maybe<Scalars['String']['output']>;
};

export type UserAuthenticationWithPasswordFailure = {
  __typename?: 'UserAuthenticationWithPasswordFailure';
  message: Scalars['String']['output'];
};

export type UserAuthenticationWithPasswordResult = UserAuthenticationWithPasswordFailure | UserAuthenticationWithPasswordSuccess;

export type UserAuthenticationWithPasswordSuccess = {
  __typename?: 'UserAuthenticationWithPasswordSuccess';
  item: User;
  sessionToken: Scalars['String']['output'];
};

export type UserCreateInput = {
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  email?: InputMaybe<Scalars['String']['input']>;
  isAdministrator?: InputMaybe<Scalars['Boolean']['input']>;
  isEnabled?: InputMaybe<Scalars['Boolean']['input']>;
  isTester?: InputMaybe<Scalars['Boolean']['input']>;
  lastLogin?: InputMaybe<Scalars['DateTime']['input']>;
  markedForDeletionAt?: InputMaybe<Scalars['DateTime']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
  organizer?: InputMaybe<OrganizerRelateToOneForCreateInput>;
  password?: InputMaybe<Scalars['String']['input']>;
  role?: InputMaybe<Scalars['String']['input']>;
};

export type UserManyRelationFilter = {
  every?: InputMaybe<UserWhereInput>;
  none?: InputMaybe<UserWhereInput>;
  some?: InputMaybe<UserWhereInput>;
};

export type UserOrderByInput = {
  createdAt?: InputMaybe<OrderDirection>;
  email?: InputMaybe<OrderDirection>;
  id?: InputMaybe<OrderDirection>;
  isAdministrator?: InputMaybe<OrderDirection>;
  isEnabled?: InputMaybe<OrderDirection>;
  isTester?: InputMaybe<OrderDirection>;
  lastLogin?: InputMaybe<OrderDirection>;
  markedForDeletionAt?: InputMaybe<OrderDirection>;
  name?: InputMaybe<OrderDirection>;
  role?: InputMaybe<OrderDirection>;
};

export type UserRelateToManyForCreateInput = {
  connect?: InputMaybe<Array<UserWhereUniqueInput>>;
  create?: InputMaybe<Array<UserCreateInput>>;
};

export type UserRelateToManyForUpdateInput = {
  connect?: InputMaybe<Array<UserWhereUniqueInput>>;
  create?: InputMaybe<Array<UserCreateInput>>;
  disconnect?: InputMaybe<Array<UserWhereUniqueInput>>;
  set?: InputMaybe<Array<UserWhereUniqueInput>>;
};

export type UserRelateToOneForCreateInput = {
  connect?: InputMaybe<UserWhereUniqueInput>;
  create?: InputMaybe<UserCreateInput>;
};

export type UserRelateToOneForUpdateInput = {
  connect?: InputMaybe<UserWhereUniqueInput>;
  create?: InputMaybe<UserCreateInput>;
  disconnect?: InputMaybe<Scalars['Boolean']['input']>;
};

export type UserUpdateArgs = {
  data: UserUpdateInput;
  where: UserWhereUniqueInput;
};

export type UserUpdateInput = {
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  email?: InputMaybe<Scalars['String']['input']>;
  isAdministrator?: InputMaybe<Scalars['Boolean']['input']>;
  isEnabled?: InputMaybe<Scalars['Boolean']['input']>;
  isTester?: InputMaybe<Scalars['Boolean']['input']>;
  lastLogin?: InputMaybe<Scalars['DateTime']['input']>;
  markedForDeletionAt?: InputMaybe<Scalars['DateTime']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
  organizer?: InputMaybe<OrganizerRelateToOneForUpdateInput>;
  password?: InputMaybe<Scalars['String']['input']>;
  role?: InputMaybe<Scalars['String']['input']>;
};

export type UserVerificationToken = {
  __typename?: 'UserVerificationToken';
  createdAt?: Maybe<Scalars['DateTime']['output']>;
  id: Scalars['ID']['output'];
  user?: Maybe<User>;
  validUntil?: Maybe<Scalars['DateTime']['output']>;
  validationToken?: Maybe<Scalars['String']['output']>;
};

export type UserVerificationTokenCreateInput = {
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  user?: InputMaybe<UserRelateToOneForCreateInput>;
  validUntil?: InputMaybe<Scalars['DateTime']['input']>;
  validationToken?: InputMaybe<Scalars['String']['input']>;
};

export type UserVerificationTokenOrderByInput = {
  createdAt?: InputMaybe<OrderDirection>;
  id?: InputMaybe<OrderDirection>;
  validUntil?: InputMaybe<OrderDirection>;
  validationToken?: InputMaybe<OrderDirection>;
};

export type UserVerificationTokenUpdateArgs = {
  data: UserVerificationTokenUpdateInput;
  where: UserVerificationTokenWhereUniqueInput;
};

export type UserVerificationTokenUpdateInput = {
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  user?: InputMaybe<UserRelateToOneForUpdateInput>;
  validUntil?: InputMaybe<Scalars['DateTime']['input']>;
  validationToken?: InputMaybe<Scalars['String']['input']>;
};

export type UserVerificationTokenWhereInput = {
  AND?: InputMaybe<Array<UserVerificationTokenWhereInput>>;
  NOT?: InputMaybe<Array<UserVerificationTokenWhereInput>>;
  OR?: InputMaybe<Array<UserVerificationTokenWhereInput>>;
  createdAt?: InputMaybe<DateTimeFilter>;
  id?: InputMaybe<IdFilter>;
  user?: InputMaybe<UserWhereInput>;
  validUntil?: InputMaybe<DateTimeFilter>;
  validationToken?: InputMaybe<StringFilter>;
};

export type UserVerificationTokenWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
  validationToken?: InputMaybe<Scalars['String']['input']>;
};

export type UserWhereInput = {
  AND?: InputMaybe<Array<UserWhereInput>>;
  NOT?: InputMaybe<Array<UserWhereInput>>;
  OR?: InputMaybe<Array<UserWhereInput>>;
  createdAt?: InputMaybe<DateTimeNullableFilter>;
  email?: InputMaybe<StringFilter>;
  id?: InputMaybe<IdFilter>;
  isAdministrator?: InputMaybe<BooleanFilter>;
  isEnabled?: InputMaybe<BooleanFilter>;
  isTester?: InputMaybe<BooleanFilter>;
  lastLogin?: InputMaybe<DateTimeNullableFilter>;
  markedForDeletionAt?: InputMaybe<DateTimeNullableFilter>;
  name?: InputMaybe<StringFilter>;
  organizer?: InputMaybe<OrganizerWhereInput>;
  role?: InputMaybe<StringNullableFilter>;
};

export type UserWhereUniqueInput = {
  email?: InputMaybe<Scalars['String']['input']>;
  id?: InputMaybe<Scalars['ID']['input']>;
};

export type VerifyAccountFailed = {
  __typename?: 'VerifyAccountFailed';
  reason: Scalars['String']['output'];
};

export type VerifyAccountInput = {
  token: Scalars['String']['input'];
};

export type VerifyAccountResult = VerifyAccountFailed | VerifyAccountSuccess;

export type VerifyAccountSuccess = {
  __typename?: 'VerifyAccountSuccess';
  user: User;
};

export type AddTicketsToCartMutationVariables = Exact<{
  eventId: Scalars['ID']['input'];
  amounts: Array<AddTicketsToCartAmountsInput> | AddTicketsToCartAmountsInput;
}>;


export type AddTicketsToCartMutation = { __typename?: 'Mutation', addTicketsToCart: { __typename?: 'AddTicketsToCartFailed', reason: string } | { __typename?: 'AddTicketsToCartSuccess', cartTickets: Array<{ __typename?: 'CartTicket', id: string, addedToCartAt?: any | null, discountType?: { __typename?: 'DiscountType', id: string, name?: string | null } | null, child?: { __typename?: 'Child', id: string, firstname?: string | null, surname?: string | null, dateOfBirth?: any | null } | null, eventTicket?: { __typename?: 'EventTicket', id: string, version?: number | null, type?: string | null, number?: number | null, event?: { __typename?: 'Event', id: string, name?: string | null, description?: string | null, careTimeStart?: string | null, careTimeEnd?: string | null, ageMinimum?: number | null, ageMaximum?: number | null, locationCity?: string | null, locationZipcode?: string | null, locationStreet?: string | null, costs?: number | null, picture?: { __typename?: 'ImageFieldOutput', url: string } | null, organizer?: { __typename?: 'Organizer', id: string, name?: string | null, phone?: string | null } | null, carePeriodCommitted?: { __typename?: 'CarePeriod', careDays?: Array<{ __typename?: 'CareDay', day?: any | null }> | null, period?: { __typename?: 'Period', id: string } | null } | null } | null } | null }> } };

export type ApplyForDiscountMutationVariables = Exact<{
  childId: Scalars['ID']['input'];
  discountTypeId: Scalars['ID']['input'];
  year: Scalars['Int']['input'];
}>;


export type ApplyForDiscountMutation = { __typename?: 'Mutation', applyForDiscount: { __typename?: 'ApplyForDiscountFailed', reason: string } | { __typename?: 'ApplyForDiscountSuccess', discount: { __typename?: 'Discount', id: string, status?: string | null, submissionDate?: any | null, validUntil?: any | null, rejectionReason?: { __typename?: 'DiscountRejectionReason', reason?: string | null } | null, type?: { __typename?: 'DiscountType', id: string, name?: string | null } | null, child?: { __typename?: 'Child', id: string, firstname?: string | null, surname?: string | null } | null, vouchers?: Array<{ __typename?: 'DiscountVoucher', id: string, redemptionDate?: any | null, ticket?: { __typename?: 'Ticket', id: string } | null }> | null, season?: { __typename?: 'Season', id: string, year?: number | null } | null } } };

export type AssignChildToCartTicketMutationVariables = Exact<{
  cartTicketId: Scalars['ID']['input'];
  childId?: InputMaybe<Scalars['ID']['input']>;
}>;


export type AssignChildToCartTicketMutation = { __typename?: 'Mutation', assignChildToCartTicket: { __typename?: 'AssignChildToCartTicketFailed', reason: string } | { __typename?: 'AssignChildToCartTicketSuccess', cartTicket: { __typename?: 'CartTicket', id: string, addedToCartAt?: any | null, discountType?: { __typename?: 'DiscountType', id: string, name?: string | null } | null, child?: { __typename?: 'Child', id: string, firstname?: string | null, surname?: string | null, dateOfBirth?: any | null } | null, eventTicket?: { __typename?: 'EventTicket', id: string, version?: number | null, type?: string | null, number?: number | null, event?: { __typename?: 'Event', id: string, name?: string | null, description?: string | null, careTimeStart?: string | null, careTimeEnd?: string | null, ageMinimum?: number | null, ageMaximum?: number | null, locationCity?: string | null, locationZipcode?: string | null, locationStreet?: string | null, costs?: number | null, picture?: { __typename?: 'ImageFieldOutput', url: string } | null, organizer?: { __typename?: 'Organizer', id: string, name?: string | null, phone?: string | null } | null, carePeriodCommitted?: { __typename?: 'CarePeriod', careDays?: Array<{ __typename?: 'CareDay', day?: any | null }> | null, period?: { __typename?: 'Period', id: string } | null } | null } | null } | null } } };

export type AssignDiscountTypeToCartTicketMutationVariables = Exact<{
  cartTicketId: Scalars['ID']['input'];
  discountTypeId?: InputMaybe<Scalars['ID']['input']>;
}>;


export type AssignDiscountTypeToCartTicketMutation = { __typename?: 'Mutation', assignDiscountTypeToCartTicket: { __typename?: 'CartTicket', id: string, addedToCartAt?: any | null, discountType?: { __typename?: 'DiscountType', id: string, name?: string | null, discountPercent?: number | null, description?: string | null } | null } };

export type AuthenticateUserWithPasswordMutationVariables = Exact<{
  email: Scalars['String']['input'];
  password: Scalars['String']['input'];
}>;


export type AuthenticateUserWithPasswordMutation = { __typename?: 'Mutation', authenticateUserWithPassword?: { __typename?: 'UserAuthenticationWithPasswordFailure', message: string } | { __typename?: 'UserAuthenticationWithPasswordSuccess', sessionToken: string, item: { __typename?: 'User', id: string, name?: string | null, email?: string | null, createdAt?: any | null, role?: string | null, isEnabled?: boolean | null } } | null };

export type BookCartTicketsMutationVariables = Exact<{
  agreeDataProtection: Scalars['Boolean']['input'];
  agreeTermsAndConditions: Scalars['Boolean']['input'];
}>;


export type BookCartTicketsMutation = { __typename?: 'Mutation', bookCartTickets: { __typename?: 'BookCartTicketsFailed', reason: string } | { __typename?: 'BookCartTicketsSuccess', bookings: Array<{ __typename?: 'Booking', id: string, code?: string | null, price?: number | null, status?: string | null, date?: any | null, tickets?: Array<{ __typename?: 'Ticket', id: string, eventTicket?: { __typename?: 'EventTicket', id: string } | null, child?: { __typename?: 'Child', id: string, firstname?: string | null, surname?: string | null } | null }> | null, event?: { __typename?: 'Event', id: string, name?: string | null } | null }> } };

export type CancelDiscountMutationVariables = Exact<{
  discountId: Scalars['ID']['input'];
}>;


export type CancelDiscountMutation = { __typename?: 'Mutation', cancelDiscount: { __typename?: 'CancelDiscountFailed', reason: string } | { __typename?: 'CancelDiscountSuccess', discount: { __typename?: 'Discount', id: string } } };

export type ChangePasswordMutationVariables = Exact<{
  currentPassword: Scalars['String']['input'];
  newPassword: Scalars['String']['input'];
}>;


export type ChangePasswordMutation = { __typename?: 'Mutation', changePassword: { __typename?: 'ChangePasswordFailed', reason: string } | { __typename?: 'ChangePasswordSuccess', user: { __typename?: 'User', id: string } } };

export type CreateChildMutationVariables = Exact<{
  data: ChildCreateInput;
}>;


export type CreateChildMutation = { __typename?: 'Mutation', createChild?: { __typename?: 'Child', id: string } | null };

export type DeleteAccountMutationVariables = Exact<{ [key: string]: never; }>;


export type DeleteAccountMutation = { __typename?: 'Mutation', deleteAccount: boolean };

export type ExtendCartTermMutationVariables = Exact<{ [key: string]: never; }>;


export type ExtendCartTermMutation = { __typename?: 'Mutation', extendCartLifetime: { __typename?: 'ExtendCartTermFailed', reason: string } | { __typename?: 'ExtendCartTermSuccess', cart: { __typename?: 'Cart', id: string, validUntil?: any | null, termExtensionCount?: number | null, cartTickets?: Array<{ __typename?: 'CartTicket', id: string, addedToCartAt?: any | null, discountType?: { __typename?: 'DiscountType', id: string, name?: string | null } | null, child?: { __typename?: 'Child', id: string, firstname?: string | null, surname?: string | null, dateOfBirth?: any | null } | null, eventTicket?: { __typename?: 'EventTicket', id: string, version?: number | null, type?: string | null, number?: number | null, event?: { __typename?: 'Event', id: string, name?: string | null, description?: string | null, careTimeStart?: string | null, careTimeEnd?: string | null, ageMinimum?: number | null, ageMaximum?: number | null, locationCity?: string | null, locationZipcode?: string | null, locationStreet?: string | null, costs?: number | null, picture?: { __typename?: 'ImageFieldOutput', url: string } | null, organizer?: { __typename?: 'Organizer', id: string, name?: string | null, phone?: string | null } | null, carePeriodCommitted?: { __typename?: 'CarePeriod', careDays?: Array<{ __typename?: 'CareDay', day?: any | null }> | null, period?: { __typename?: 'Period', id: string } | null } | null } | null } | null }> | null } } };

export type LogoutMutationVariables = Exact<{ [key: string]: never; }>;


export type LogoutMutation = { __typename?: 'Mutation', endSession: boolean };

export type MarkChildDeletedMutationVariables = Exact<{
  childId: Scalars['ID']['input'];
}>;


export type MarkChildDeletedMutation = { __typename?: 'Mutation', markChildDeleted?: { __typename?: 'Child', id: string } | null };

export type RedeemEarlyAccessCodeMutationVariables = Exact<{
  year: Scalars['Int']['input'];
  code: Scalars['String']['input'];
}>;


export type RedeemEarlyAccessCodeMutation = { __typename?: 'Mutation', redeemEarlyAccessCode: { __typename?: 'RedeemEarlyAccessCodeFailed', reason: string } | { __typename?: 'RedeemEarlyAccessCodeSuccess', earlyAccessCodeVoucher: { __typename?: 'EarlyAccessCodeVoucher', code?: string | null, id: string, season?: { __typename?: 'Season', id: string, year?: number | null } | null } } };

export type RegisterMutationVariables = Exact<{
  data: RegisterInput;
}>;


export type RegisterMutation = { __typename?: 'Mutation', register: { __typename?: 'RegisterFailed', reason: string } | { __typename?: 'RegisterSuccess', user: { __typename?: 'User', id: string } } };

export type RemoveTicketsFromCartMutationVariables = Exact<{
  ticketIds: Array<Scalars['ID']['input']> | Scalars['ID']['input'];
}>;


export type RemoveTicketsFromCartMutation = { __typename?: 'Mutation', removeTicketsFromCart: Array<string> };

export type RequestPasswordResetTokenMutationVariables = Exact<{
  email: Scalars['String']['input'];
}>;


export type RequestPasswordResetTokenMutation = { __typename?: 'Mutation', requestPasswordResetToken: { __typename?: 'RequestPasswordResetTokenFailed', reason: string } | { __typename?: 'RequestPasswordResetTokenSuccess', success: boolean } };

export type ResendVerificationCodeMutationVariables = Exact<{
  email: Scalars['String']['input'];
}>;


export type ResendVerificationCodeMutation = { __typename?: 'Mutation', resendVerificationCode: { __typename?: 'ResendVerificationCodeFailed', reason: string } | { __typename?: 'ResendVerificationCodeSuccess', success: boolean } };

export type ResetPasswordMutationVariables = Exact<{
  token: Scalars['String']['input'];
  newPassword: Scalars['String']['input'];
}>;


export type ResetPasswordMutation = { __typename?: 'Mutation', resetPassword: { __typename?: 'ResetPasswordFailed', reason: string } | { __typename?: 'ResetPasswordSuccess', user: { __typename?: 'User', id: string } } };

export type UpdateChildMutationVariables = Exact<{
  id: Scalars['ID']['input'];
  data: ChildUpdateInput;
}>;


export type UpdateChildMutation = { __typename?: 'Mutation', updateChild?: { __typename?: 'Child', id: string } | null };

export type UpdateCustodianMutationVariables = Exact<{
  id: Scalars['ID']['input'];
  data: CustodianUpdateInput;
}>;


export type UpdateCustodianMutation = { __typename?: 'Mutation', updateCustodian?: { __typename?: 'Custodian', id: string } | null };

export type VerifyAccountMutationVariables = Exact<{
  token: Scalars['String']['input'];
}>;


export type VerifyAccountMutation = { __typename?: 'Mutation', verifyAccount: { __typename?: 'VerifyAccountFailed', reason: string } | { __typename?: 'VerifyAccountSuccess', user: { __typename?: 'User', id: string } } };

export type AuthenticatedUserQueryVariables = Exact<{ [key: string]: never; }>;


export type AuthenticatedUserQuery = { __typename?: 'Query', authenticatedItem?: { __typename?: 'User', id: string, name?: string | null, email?: string | null, createdAt?: any | null, role?: string | null, isEnabled?: boolean | null } | null };

export type AvailableTicketCountQueryVariables = Exact<{
  year: Scalars['Int']['input'];
}>;


export type AvailableTicketCountQuery = { __typename?: 'Query', availableTicketCount: Array<{ __typename?: 'AvailableTicketCountResult', eventId?: string | null, participant?: number | null, waitinglist?: number | null } | null> };

export type BookingsQueryVariables = Exact<{ [key: string]: never; }>;


export type BookingsQuery = { __typename?: 'Query', bookings?: Array<{ __typename?: 'Booking', id: string, date?: any | null, code?: string | null, status?: string | null, price?: number | null, isDeleted?: boolean | null, event?: { __typename?: 'Event', id: string, name?: string | null, description?: string | null, emergencyPhone?: string | null, careTimeStart?: string | null, careTimeEnd?: string | null, ageMinimum?: number | null, ageMaximum?: number | null, locationCity?: string | null, locationZipcode?: string | null, locationStreet?: string | null, costs?: number | null, picture?: { __typename?: 'ImageFieldOutput', url: string } | null, organizer?: { __typename?: 'Organizer', id: string, email?: string | null, name?: string | null, bankBic?: string | null, bankIban?: string | null, bankName?: string | null, bankOwner?: string | null, phone?: string | null } | null, carePeriodCommitted?: { __typename?: 'CarePeriod', careDays?: Array<{ __typename?: 'CareDay', day?: any | null }> | null, period?: { __typename?: 'Period', id: string } | null } | null } | null, tickets?: Array<{ __typename?: 'Ticket', price?: number | null, child?: { __typename?: 'Child', id: string, surname?: string | null, firstname?: string | null } | null, eventTicket?: { __typename?: 'EventTicket', id: string, type?: string | null, number?: number | null } | null, discountVoucher?: { __typename?: 'DiscountVoucher', id: string, discount?: { __typename?: 'Discount', id: string, type?: { __typename?: 'DiscountType', id: string } | null } | null } | null }> | null }> | null, reservations?: Array<{ __typename?: 'Reservation', id: string, date?: any | null, code?: string | null, price?: number | null, isDeleted?: boolean | null, event?: { __typename?: 'Event', id: string, name?: string | null, description?: string | null, emergencyPhone?: string | null, careTimeStart?: string | null, careTimeEnd?: string | null, ageMinimum?: number | null, ageMaximum?: number | null, locationCity?: string | null, locationZipcode?: string | null, locationStreet?: string | null, costs?: number | null, picture?: { __typename?: 'ImageFieldOutput', url: string } | null, organizer?: { __typename?: 'Organizer', id: string, email?: string | null, name?: string | null, bankBic?: string | null, bankIban?: string | null, bankName?: string | null, bankOwner?: string | null, phone?: string | null } | null, carePeriodCommitted?: { __typename?: 'CarePeriod', careDays?: Array<{ __typename?: 'CareDay', day?: any | null }> | null, period?: { __typename?: 'Period', id: string } | null } | null } | null, tickets?: Array<{ __typename?: 'Ticket', price?: number | null, child?: { __typename?: 'Child', id: string, surname?: string | null, firstname?: string | null } | null, eventTicket?: { __typename?: 'EventTicket', id: string, type?: string | null, number?: number | null } | null, discountVoucher?: { __typename?: 'DiscountVoucher', id: string, discount?: { __typename?: 'Discount', id: string, type?: { __typename?: 'DiscountType', id: string } | null } | null } | null }> | null }> | null, discountTypes?: Array<{ __typename?: 'DiscountType', id: string, name?: string | null, discountPercent?: number | null, description?: string | null }> | null };

export type CartQueryVariables = Exact<{
  userId: Scalars['ID']['input'];
}>;


export type CartQuery = { __typename?: 'Query', carts?: Array<{ __typename?: 'Cart', id: string, validUntil?: any | null, termExtensionCount?: number | null, cartTickets?: Array<{ __typename?: 'CartTicket', id: string, addedToCartAt?: any | null, discountType?: { __typename?: 'DiscountType', id: string, name?: string | null } | null, child?: { __typename?: 'Child', id: string, firstname?: string | null, surname?: string | null, dateOfBirth?: any | null } | null, eventTicket?: { __typename?: 'EventTicket', id: string, version?: number | null, type?: string | null, number?: number | null, event?: { __typename?: 'Event', id: string, name?: string | null, description?: string | null, careTimeStart?: string | null, careTimeEnd?: string | null, ageMinimum?: number | null, ageMaximum?: number | null, locationCity?: string | null, locationZipcode?: string | null, locationStreet?: string | null, costs?: number | null, picture?: { __typename?: 'ImageFieldOutput', url: string } | null, organizer?: { __typename?: 'Organizer', id: string, name?: string | null, phone?: string | null } | null, carePeriodCommitted?: { __typename?: 'CarePeriod', careDays?: Array<{ __typename?: 'CareDay', day?: any | null }> | null, period?: { __typename?: 'Period', id: string } | null } | null } | null } | null }> | null }> | null, children?: Array<{ __typename?: 'Child', id: string, firstname?: string | null, surname?: string | null, discounts?: Array<{ __typename?: 'Discount', id: string, vouchersCount?: number | null, type?: { __typename?: 'DiscountType', id: string, name?: string | null } | null }> | null, tickets?: Array<{ __typename?: 'Ticket', id: string, eventTicket?: { __typename?: 'EventTicket', id: string, number?: number | null, event?: { __typename?: 'Event', id: string, name?: string | null } | null } | null }> | null }> | null, discountTypes?: Array<{ __typename?: 'DiscountType', id: string, name?: string | null, discountPercent?: number | null, description?: string | null }> | null };

export type ChildQueryVariables = Exact<{
  id: Scalars['ID']['input'];
}>;


export type ChildQuery = { __typename?: 'Query', child?: { __typename?: 'Child', id: string, firstname?: string | null, surname?: string | null, displayName?: string | null, dateOfBirth?: any | null, gender?: string | null, healthInsurer?: string | null, hasLiabilityInsurance?: boolean | null, assistanceRequired?: boolean | null, physicalImpairment?: string | null, medication?: string | null, foodIntolerances?: string | null, dietaryRegulations?: string | null, miscellaneous?: string | null } | null };

export type ChildrenQueryVariables = Exact<{ [key: string]: never; }>;


export type ChildrenQuery = { __typename?: 'Query', children?: Array<{ __typename?: 'Child', id: string, firstname?: string | null, surname?: string | null, displayName?: string | null, dateOfBirth?: any | null, gender?: string | null, healthInsurer?: string | null, hasLiabilityInsurance?: boolean | null, assistanceRequired?: boolean | null, physicalImpairment?: string | null, medication?: string | null, foodIntolerances?: string | null, dietaryRegulations?: string | null, miscellaneous?: string | null }> | null };

export type ContactsQueryVariables = Exact<{
  year: Scalars['Int']['input'];
}>;


export type ContactsQuery = { __typename?: 'Query', contacts?: Array<{ __typename?: 'Contact', id: string, name?: string | null, description?: string | null, contactPersonFirstname?: string | null, contactPersonSurname?: string | null, addressStreet?: string | null, addressZip?: string | null, addressCity?: string | null, email?: string | null, phone?: string | null }> | null, organizers?: Array<{ __typename?: 'Organizer', id: string, name?: string | null, email?: string | null, contactPersonSurname?: string | null, contactPersonSalutation?: string | null, contactPersonFirstname?: string | null, bankOwner?: string | null, bankName?: string | null, bankIban?: string | null, bankBic?: string | null, addressZip?: string | null, addressStreet?: string | null, addressCity?: string | null, phone?: string | null }> | null };

export type CustodianQueryVariables = Exact<{ [key: string]: never; }>;


export type CustodianQuery = { __typename?: 'Query', custodians?: Array<{ __typename?: 'Custodian', id: string, salutation?: string | null, firstname?: string | null, surname?: string | null, phone?: string | null, emergencyPhone?: string | null, addressCity?: string | null, addressStreet?: string | null, addressHouseNumber?: string | null, addressZip?: string | null, addressAdditional?: string | null }> | null, seasons?: Array<{ __typename?: 'Season', id: string, year?: number | null }> | null, earlyAccessCodeVouchers?: Array<{ __typename?: 'EarlyAccessCodeVoucher', id: string, code?: string | null, season?: { __typename?: 'Season', id: string, year?: number | null } | null }> | null };

export type DiscountsQueryVariables = Exact<{ [key: string]: never; }>;


export type DiscountsQuery = { __typename?: 'Query', discounts?: Array<{ __typename?: 'Discount', id: string, status?: string | null, submissionDate?: any | null, validUntil?: any | null, rejectionReason?: { __typename?: 'DiscountRejectionReason', reason?: string | null } | null, type?: { __typename?: 'DiscountType', id: string, name?: string | null } | null, child?: { __typename?: 'Child', id: string, firstname?: string | null, surname?: string | null } | null, vouchers?: Array<{ __typename?: 'DiscountVoucher', id: string, redemptionDate?: any | null, ticket?: { __typename?: 'Ticket', id: string } | null }> | null, season?: { __typename?: 'Season', year?: number | null } | null }> | null, seasons?: Array<{ __typename?: 'Season', id: string, year?: number | null }> | null, discountTypes?: Array<{ __typename?: 'DiscountType', id: string, name?: string | null, discountPercent?: number | null, description?: string | null, amountEvents?: number | null, amountChildren?: number | null, amountEventsDescription?: string | null, amountChildrenDescription?: string | null }> | null };

export type EventQueryVariables = Exact<{
  id: Scalars['ID']['input'];
}>;


export type EventQuery = { __typename?: 'Query', event?: { __typename?: 'Event', id: string, name?: string | null, waitingListLimit?: number | null, participantLimit?: number | null, description?: string | null, contactPersonSalutation?: string | null, contactPersonFirstname?: string | null, contactPersonSurname?: string | null, hints?: string | null, meals?: string | null, careTimeStart?: string | null, careTimeEnd?: string | null, ageMinimum?: number | null, ageMaximum?: number | null, locationCity?: string | null, locationZipcode?: string | null, locationStreet?: string | null, costs?: number | null, registrationDeadline?: any | null, availableTicketCountParticipant?: number | null, availableTicketCountWaitinglist?: number | null, categories?: Array<{ __typename?: 'EventCategory', name?: string | null }> | null, picture?: { __typename?: 'ImageFieldOutput', url: string } | null, organizer?: { __typename?: 'Organizer', name?: string | null, phone?: string | null, email?: string | null, addressStreet?: string | null, addressZip?: string | null, addressCity?: string | null } | null, carePeriodCommitted?: { __typename?: 'CarePeriod', careDays?: Array<{ __typename?: 'CareDay', day?: any | null }> | null, period?: { __typename?: 'Period', id: string, startDate?: any | null, endDate?: any | null, holiday?: { __typename?: 'Holiday', id: string, name?: string | null } | null, season?: { __typename?: 'Season', id: string, year?: number | null, bookingStart?: any | null, bookingStartPartners?: any | null } | null } | null } | null } | null, discountTypes?: Array<{ __typename?: 'DiscountType', id: string, name?: string | null, discountPercent?: number | null, description?: string | null }> | null };

export type EventsQueryVariables = Exact<{
  year: Scalars['Int']['input'];
}>;


export type EventsQuery = { __typename?: 'Query', events?: Array<{ __typename?: 'Event', id: string, name?: string | null, waitingListLimit?: number | null, participantLimit?: number | null, description?: string | null, emergencyPhone?: string | null, contactPersonSalutation?: string | null, contactPersonFirstname?: string | null, contactPersonSurname?: string | null, hints?: string | null, meals?: string | null, careTimeStart?: string | null, careTimeEnd?: string | null, ageMinimum?: number | null, ageMaximum?: number | null, locationCity?: string | null, locationZipcode?: string | null, locationStreet?: string | null, costs?: number | null, registrationDeadline?: any | null, availableTicketCountParticipant?: number | null, availableTicketCountWaitinglist?: number | null, categories?: Array<{ __typename?: 'EventCategory', id: string, name?: string | null }> | null, picture?: { __typename?: 'ImageFieldOutput', url: string } | null, organizer?: { __typename?: 'Organizer', name?: string | null } | null, carePeriodCommitted?: { __typename?: 'CarePeriod', careDays?: Array<{ __typename?: 'CareDay', day?: any | null }> | null, period?: { __typename?: 'Period', id: string } | null } | null }> | null, seasons?: Array<{ __typename?: 'Season', id: string, year?: number | null }> | null, holidays?: Array<{ __typename?: 'Holiday', id: string, name?: string | null, periods?: Array<{ __typename?: 'Period', id: string, startDate?: any | null, endDate?: any | null }> | null }> | null };

export type GdprInformationQueryVariables = Exact<{
  custodianId: Scalars['ID']['input'];
}>;


export type GdprInformationQuery = { __typename?: 'Query', custodian?: { __typename?: 'Custodian', id: string, salutation?: string | null, firstname?: string | null, surname?: string | null, addressStreet?: string | null, addressHouseNumber?: string | null, addressZip?: string | null, addressCity?: string | null, addressAdditional?: string | null, emergencyPhone?: string | null, phone?: string | null, user?: { __typename?: 'User', id: string, email?: string | null, createdAt?: any | null, name?: string | null, isEnabled?: boolean | null, role?: string | null, lastLogin?: any | null } | null, children?: Array<{ __typename?: 'Child', id: string, firstname?: string | null, surname?: string | null, healthInsurer?: string | null, hasLiabilityInsurance?: boolean | null, gender?: string | null, foodIntolerances?: string | null, medication?: string | null, miscellaneous?: string | null, physicalImpairment?: string | null, dateOfBirth?: any | null, assistanceRequired?: boolean | null, dietaryRegulations?: string | null }> | null, bookings?: Array<{ __typename?: 'Booking', id: string, code?: string | null, status?: string | null, date?: any | null, price?: number | null, reminderDate?: any | null, event?: { __typename?: 'Event', name?: string | null, season?: { __typename?: 'Season', year?: number | null } | null } | null, tickets?: Array<{ __typename?: 'Ticket', id: string, child?: { __typename?: 'Child', id: string, firstname?: string | null, surname?: string | null } | null, discountVoucher?: { __typename?: 'DiscountVoucher', id: string, redemptionDate?: any | null, discount?: { __typename?: 'Discount', id: string, status?: string | null, type?: { __typename?: 'DiscountType', id: string, name?: string | null } | null } | null } | null }> | null }> | null, reservations?: Array<{ __typename?: 'Reservation', id: string, code?: string | null, date?: any | null, price?: number | null, event?: { __typename?: 'Event', name?: string | null, season?: { __typename?: 'Season', year?: number | null } | null } | null, tickets?: Array<{ __typename?: 'Ticket', id: string, child?: { __typename?: 'Child', id: string, firstname?: string | null, surname?: string | null } | null, discountVoucher?: { __typename?: 'DiscountVoucher', id: string, redemptionDate?: any | null, discount?: { __typename?: 'Discount', id: string, status?: string | null, type?: { __typename?: 'DiscountType', id: string, name?: string | null } | null } | null } | null }> | null }> | null, discounts?: Array<{ __typename?: 'Discount', id: string, type?: { __typename?: 'DiscountType', id: string, name?: string | null } | null, vouchers?: Array<{ __typename?: 'DiscountVoucher', id: string, redemptionDate?: any | null, ticket?: { __typename?: 'Ticket', id: string } | null }> | null }> | null } | null };

export type LandingQueryVariables = Exact<{
  timestamp: Scalars['DateTime']['input'];
}>;


export type LandingQuery = { __typename?: 'Query', seasons?: Array<{ __typename?: 'Season', id: string, year?: number | null, bookingStart?: any | null, bookingStartPartners?: any | null, releaseDate?: any | null }> | null };

export type UsersQueryVariables = Exact<{ [key: string]: never; }>;


export type UsersQuery = { __typename?: 'Query', users?: Array<{ __typename?: 'User', id: string, name?: string | null, email?: string | null, createdAt?: any | null }> | null };

export const AddTicketsToCartDocument = gql`
    mutation addTicketsToCart($eventId: ID!, $amounts: [AddTicketsToCartAmountsInput!]!) {
  addTicketsToCart(eventId: $eventId, amounts: $amounts) {
    ... on AddTicketsToCartSuccess {
      cartTickets {
        id
        addedToCartAt
        discountType {
          id
          name
        }
        child {
          id
          firstname
          surname
          dateOfBirth
        }
        eventTicket {
          id
          version
          type
          number
          event {
            id
            name
            description
            picture {
              url
            }
            organizer {
              id
              name
              phone
            }
            careTimeStart
            careTimeEnd
            ageMinimum
            ageMaximum
            locationCity
            locationZipcode
            locationStreet
            costs
            carePeriodCommitted {
              careDays(orderBy: {day: asc}) {
                day
              }
              period {
                id
              }
            }
          }
        }
      }
    }
    ... on AddTicketsToCartFailed {
      reason
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class AddTicketsToCartGQL extends Apollo.Mutation<AddTicketsToCartMutation, AddTicketsToCartMutationVariables> {
    override document = AddTicketsToCartDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const ApplyForDiscountDocument = gql`
    mutation applyForDiscount($childId: ID!, $discountTypeId: ID!, $year: Int!) {
  applyForDiscount(
    childId: $childId
    discountTypeId: $discountTypeId
    year: $year
  ) {
    ... on ApplyForDiscountSuccess {
      discount {
        id
        status
        submissionDate
        rejectionReason {
          reason
        }
        validUntil
        type {
          id
          name
        }
        child {
          id
          firstname
          surname
        }
        vouchers {
          id
          redemptionDate
          ticket {
            id
          }
        }
        season {
          id
          year
        }
      }
    }
    ... on ApplyForDiscountFailed {
      reason
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class ApplyForDiscountGQL extends Apollo.Mutation<ApplyForDiscountMutation, ApplyForDiscountMutationVariables> {
    override document = ApplyForDiscountDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const AssignChildToCartTicketDocument = gql`
    mutation assignChildToCartTicket($cartTicketId: ID!, $childId: ID) {
  assignChildToCartTicket(cartTicketId: $cartTicketId, childId: $childId) {
    ... on AssignChildToCartTicketSuccess {
      cartTicket {
        id
        addedToCartAt
        discountType {
          id
          name
        }
        child {
          id
          firstname
          surname
          dateOfBirth
        }
        eventTicket {
          id
          version
          type
          number
          event {
            id
            name
            description
            picture {
              url
            }
            organizer {
              id
              name
              phone
            }
            careTimeStart
            careTimeEnd
            ageMinimum
            ageMaximum
            locationCity
            locationZipcode
            locationStreet
            costs
            carePeriodCommitted {
              careDays(orderBy: {day: asc}) {
                day
              }
              period {
                id
              }
            }
          }
        }
      }
    }
    ... on AssignChildToCartTicketFailed {
      reason
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class AssignChildToCartTicketGQL extends Apollo.Mutation<AssignChildToCartTicketMutation, AssignChildToCartTicketMutationVariables> {
    override document = AssignChildToCartTicketDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const AssignDiscountTypeToCartTicketDocument = gql`
    mutation assignDiscountTypeToCartTicket($cartTicketId: ID!, $discountTypeId: ID) {
  assignDiscountTypeToCartTicket(
    cartTicketId: $cartTicketId
    discountTypeId: $discountTypeId
  ) {
    id
    addedToCartAt
    discountType {
      id
      name
      discountPercent
      description
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class AssignDiscountTypeToCartTicketGQL extends Apollo.Mutation<AssignDiscountTypeToCartTicketMutation, AssignDiscountTypeToCartTicketMutationVariables> {
    override document = AssignDiscountTypeToCartTicketDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const AuthenticateUserWithPasswordDocument = gql`
    mutation authenticateUserWithPassword($email: String!, $password: String!) {
  authenticateUserWithPassword(email: $email, password: $password) {
    ... on UserAuthenticationWithPasswordSuccess {
      sessionToken
      item {
        id
        name
        email
        createdAt
        role
        isEnabled
      }
    }
    ... on UserAuthenticationWithPasswordFailure {
      message
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class AuthenticateUserWithPasswordGQL extends Apollo.Mutation<AuthenticateUserWithPasswordMutation, AuthenticateUserWithPasswordMutationVariables> {
    override document = AuthenticateUserWithPasswordDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const BookCartTicketsDocument = gql`
    mutation bookCartTickets($agreeDataProtection: Boolean!, $agreeTermsAndConditions: Boolean!) {
  bookCartTickets(
    agreeDataProtection: $agreeDataProtection
    agreeTermsAndConditions: $agreeTermsAndConditions
  ) {
    ... on BookCartTicketsSuccess {
      bookings {
        id
        code
        price
        status
        date
        tickets {
          id
          eventTicket {
            id
          }
          child {
            id
            firstname
            surname
          }
        }
        event {
          id
          name
        }
      }
    }
    ... on BookCartTicketsFailed {
      reason
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class BookCartTicketsGQL extends Apollo.Mutation<BookCartTicketsMutation, BookCartTicketsMutationVariables> {
    override document = BookCartTicketsDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const CancelDiscountDocument = gql`
    mutation cancelDiscount($discountId: ID!) {
  cancelDiscount(discountId: $discountId) {
    ... on CancelDiscountSuccess {
      discount {
        id
      }
    }
    ... on CancelDiscountFailed {
      reason
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class CancelDiscountGQL extends Apollo.Mutation<CancelDiscountMutation, CancelDiscountMutationVariables> {
    override document = CancelDiscountDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const ChangePasswordDocument = gql`
    mutation changePassword($currentPassword: String!, $newPassword: String!) {
  changePassword(currentPassword: $currentPassword, newPassword: $newPassword) {
    ... on ChangePasswordSuccess {
      user {
        id
      }
    }
    ... on ChangePasswordFailed {
      reason
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class ChangePasswordGQL extends Apollo.Mutation<ChangePasswordMutation, ChangePasswordMutationVariables> {
    override document = ChangePasswordDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const CreateChildDocument = gql`
    mutation createChild($data: ChildCreateInput!) {
  createChild(data: $data) {
    id
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class CreateChildGQL extends Apollo.Mutation<CreateChildMutation, CreateChildMutationVariables> {
    override document = CreateChildDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const DeleteAccountDocument = gql`
    mutation deleteAccount {
  deleteAccount
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class DeleteAccountGQL extends Apollo.Mutation<DeleteAccountMutation, DeleteAccountMutationVariables> {
    override document = DeleteAccountDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const ExtendCartTermDocument = gql`
    mutation extendCartTerm {
  extendCartLifetime {
    ... on ExtendCartTermSuccess {
      cart {
        id
        validUntil
        termExtensionCount
        cartTickets(orderBy: {addedToCartAt: asc}) {
          id
          addedToCartAt
          discountType {
            id
            name
          }
          child {
            id
            firstname
            surname
            dateOfBirth
          }
          eventTicket {
            id
            version
            type
            number
            event {
              id
              name
              description
              picture {
                url
              }
              organizer {
                id
                name
                phone
              }
              careTimeStart
              careTimeEnd
              ageMinimum
              ageMaximum
              locationCity
              locationZipcode
              locationStreet
              costs
              carePeriodCommitted {
                careDays(orderBy: {day: asc}) {
                  day
                }
                period {
                  id
                }
              }
            }
          }
        }
      }
    }
    ... on ExtendCartTermFailed {
      reason
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class ExtendCartTermGQL extends Apollo.Mutation<ExtendCartTermMutation, ExtendCartTermMutationVariables> {
    override document = ExtendCartTermDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const LogoutDocument = gql`
    mutation logout {
  endSession
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class LogoutGQL extends Apollo.Mutation<LogoutMutation, LogoutMutationVariables> {
    override document = LogoutDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const MarkChildDeletedDocument = gql`
    mutation markChildDeleted($childId: ID!) {
  markChildDeleted(childId: $childId) {
    id
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class MarkChildDeletedGQL extends Apollo.Mutation<MarkChildDeletedMutation, MarkChildDeletedMutationVariables> {
    override document = MarkChildDeletedDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const RedeemEarlyAccessCodeDocument = gql`
    mutation redeemEarlyAccessCode($year: Int!, $code: String!) {
  redeemEarlyAccessCode(year: $year, code: $code) {
    ... on RedeemEarlyAccessCodeSuccess {
      earlyAccessCodeVoucher {
        code
        id
        season {
          id
          year
        }
      }
    }
    ... on RedeemEarlyAccessCodeFailed {
      reason
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class RedeemEarlyAccessCodeGQL extends Apollo.Mutation<RedeemEarlyAccessCodeMutation, RedeemEarlyAccessCodeMutationVariables> {
    override document = RedeemEarlyAccessCodeDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const RegisterDocument = gql`
    mutation register($data: RegisterInput!) {
  register(data: $data) {
    ... on RegisterSuccess {
      user {
        id
      }
    }
    ... on RegisterFailed {
      reason
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class RegisterGQL extends Apollo.Mutation<RegisterMutation, RegisterMutationVariables> {
    override document = RegisterDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const RemoveTicketsFromCartDocument = gql`
    mutation removeTicketsFromCart($ticketIds: [ID!]!) {
  removeTicketsFromCart(ticketIds: $ticketIds)
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class RemoveTicketsFromCartGQL extends Apollo.Mutation<RemoveTicketsFromCartMutation, RemoveTicketsFromCartMutationVariables> {
    override document = RemoveTicketsFromCartDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const RequestPasswordResetTokenDocument = gql`
    mutation requestPasswordResetToken($email: String!) {
  requestPasswordResetToken(data: {email: $email}) {
    ... on RequestPasswordResetTokenSuccess {
      success
    }
    ... on RequestPasswordResetTokenFailed {
      reason
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class RequestPasswordResetTokenGQL extends Apollo.Mutation<RequestPasswordResetTokenMutation, RequestPasswordResetTokenMutationVariables> {
    override document = RequestPasswordResetTokenDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const ResendVerificationCodeDocument = gql`
    mutation resendVerificationCode($email: String!) {
  resendVerificationCode(data: {email: $email}) {
    ... on ResendVerificationCodeSuccess {
      success
    }
    ... on ResendVerificationCodeFailed {
      reason
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class ResendVerificationCodeGQL extends Apollo.Mutation<ResendVerificationCodeMutation, ResendVerificationCodeMutationVariables> {
    override document = ResendVerificationCodeDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const ResetPasswordDocument = gql`
    mutation resetPassword($token: String!, $newPassword: String!) {
  resetPassword(token: $token, newPassword: $newPassword) {
    ... on ResetPasswordSuccess {
      user {
        id
      }
    }
    ... on ResetPasswordFailed {
      reason
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class ResetPasswordGQL extends Apollo.Mutation<ResetPasswordMutation, ResetPasswordMutationVariables> {
    override document = ResetPasswordDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const UpdateChildDocument = gql`
    mutation updateChild($id: ID!, $data: ChildUpdateInput!) {
  updateChild(where: {id: $id}, data: $data) {
    id
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class UpdateChildGQL extends Apollo.Mutation<UpdateChildMutation, UpdateChildMutationVariables> {
    override document = UpdateChildDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const UpdateCustodianDocument = gql`
    mutation updateCustodian($id: ID!, $data: CustodianUpdateInput!) {
  updateCustodian(where: {id: $id}, data: $data) {
    id
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class UpdateCustodianGQL extends Apollo.Mutation<UpdateCustodianMutation, UpdateCustodianMutationVariables> {
    override document = UpdateCustodianDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const VerifyAccountDocument = gql`
    mutation verifyAccount($token: String!) {
  verifyAccount(data: {token: $token}) {
    ... on VerifyAccountSuccess {
      user {
        id
      }
    }
    ... on VerifyAccountFailed {
      reason
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class VerifyAccountGQL extends Apollo.Mutation<VerifyAccountMutation, VerifyAccountMutationVariables> {
    override document = VerifyAccountDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const AuthenticatedUserDocument = gql`
    query authenticatedUser {
  authenticatedItem {
    ... on User {
      id
      name
      email
      createdAt
      role
      isEnabled
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class AuthenticatedUserGQL extends Apollo.Query<AuthenticatedUserQuery, AuthenticatedUserQueryVariables> {
    override document = AuthenticatedUserDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const AvailableTicketCountDocument = gql`
    query availableTicketCount($year: Int!) {
  availableTicketCount(year: $year) {
    eventId
    participant
    waitinglist
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class AvailableTicketCountGQL extends Apollo.Query<AvailableTicketCountQuery, AvailableTicketCountQueryVariables> {
    override document = AvailableTicketCountDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const BookingsDocument = gql`
    query bookings {
  bookings(where: {isDeleted: {equals: false}}) {
    id
    date
    code
    status
    price
    isDeleted
    event {
      id
      name
      description
      picture {
        url
      }
      emergencyPhone
      organizer {
        id
        email
        name
        bankBic
        bankIban
        bankName
        bankOwner
        phone
      }
      careTimeStart
      careTimeEnd
      ageMinimum
      ageMaximum
      locationCity
      locationZipcode
      locationStreet
      costs
      carePeriodCommitted {
        careDays(orderBy: {day: asc}) {
          day
        }
        period {
          id
        }
      }
    }
    tickets(where: {isDeleted: {equals: false}}) {
      price
      child {
        id
        surname
        firstname
      }
      eventTicket {
        id
        type
        number
      }
      discountVoucher {
        id
        discount {
          id
          type {
            id
          }
        }
      }
    }
  }
  reservations(where: {isDeleted: {equals: false}}) {
    id
    date
    code
    price
    isDeleted
    event {
      id
      name
      description
      picture {
        url
      }
      emergencyPhone
      organizer {
        id
        email
        name
        bankBic
        bankIban
        bankName
        bankOwner
        phone
      }
      careTimeStart
      careTimeEnd
      ageMinimum
      ageMaximum
      locationCity
      locationZipcode
      locationStreet
      costs
      carePeriodCommitted {
        careDays(orderBy: {day: asc}) {
          day
        }
        period {
          id
        }
      }
    }
    tickets(where: {isDeleted: {equals: false}}) {
      price
      child {
        id
        surname
        firstname
      }
      eventTicket {
        id
        type
        number
      }
      discountVoucher {
        id
        discount {
          id
          type {
            id
          }
        }
      }
    }
  }
  discountTypes {
    id
    name
    discountPercent
    description
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class BookingsGQL extends Apollo.Query<BookingsQuery, BookingsQueryVariables> {
    override document = BookingsDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const CartDocument = gql`
    query cart($userId: ID!) {
  carts(where: {custodian: {user: {id: {equals: $userId}}}}) {
    id
    validUntil
    termExtensionCount
    cartTickets(orderBy: {addedToCartAt: asc}) {
      id
      addedToCartAt
      discountType {
        id
        name
      }
      child {
        id
        firstname
        surname
        dateOfBirth
      }
      eventTicket {
        id
        version
        type
        number
        event {
          id
          name
          description
          picture {
            url
          }
          organizer {
            id
            name
            phone
          }
          careTimeStart
          careTimeEnd
          ageMinimum
          ageMaximum
          locationCity
          locationZipcode
          locationStreet
          costs
          carePeriodCommitted {
            careDays(orderBy: {day: asc}) {
              day
            }
            period {
              id
            }
          }
        }
      }
    }
  }
  children(
    where: {custodian: {user: {id: {equals: $userId}}}, isDeleted: {equals: false}}
  ) {
    id
    firstname
    surname
    discounts(
      where: {OR: [{status: {equals: "approved"}}, {status: {equals: "pending"}}, {status: {equals: "overdue"}}]}
    ) {
      id
      type {
        id
        name
      }
      vouchersCount(where: {ticket: null})
    }
    tickets(
      where: {isDeleted: {equals: false}, eventTicket: {event: {status: {equals: "approved"}}}}
    ) {
      id
      eventTicket {
        id
        number
        event {
          id
          name
        }
      }
    }
  }
  discountTypes {
    id
    name
    discountPercent
    description
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class CartGQL extends Apollo.Query<CartQuery, CartQueryVariables> {
    override document = CartDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const ChildDocument = gql`
    query child($id: ID!) {
  child(where: {id: $id}) {
    id
    firstname
    surname
    displayName
    dateOfBirth
    gender
    healthInsurer
    hasLiabilityInsurance
    assistanceRequired
    physicalImpairment
    medication
    foodIntolerances
    dietaryRegulations
    miscellaneous
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class ChildGQL extends Apollo.Query<ChildQuery, ChildQueryVariables> {
    override document = ChildDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const ChildrenDocument = gql`
    query children {
  children(where: {isDeleted: {equals: false}}) {
    id
    firstname
    surname
    displayName
    dateOfBirth
    gender
    healthInsurer
    hasLiabilityInsurance
    assistanceRequired
    physicalImpairment
    medication
    foodIntolerances
    dietaryRegulations
    miscellaneous
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class ChildrenGQL extends Apollo.Query<ChildrenQuery, ChildrenQueryVariables> {
    override document = ChildrenDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const ContactsDocument = gql`
    query contacts($year: Int!) {
  contacts(where: {hidden: {equals: false}}, orderBy: {name: asc}) {
    id
    name
    description
    contactPersonFirstname
    contactPersonSurname
    addressStreet
    addressZip
    addressCity
    email
    phone
  }
  organizers(
    where: {events: {some: {season: {year: {equals: $year}}}}}
    orderBy: {name: asc}
  ) {
    id
    name
    email
    contactPersonSurname
    contactPersonSalutation
    contactPersonFirstname
    bankOwner
    bankName
    bankIban
    bankBic
    addressZip
    addressStreet
    addressCity
    phone
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class ContactsGQL extends Apollo.Query<ContactsQuery, ContactsQueryVariables> {
    override document = ContactsDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const CustodianDocument = gql`
    query custodian {
  custodians {
    id
    salutation
    firstname
    surname
    phone
    emergencyPhone
    addressCity
    addressStreet
    addressHouseNumber
    addressZip
    addressAdditional
  }
  seasons(orderBy: {year: asc}) {
    id
    year
  }
  earlyAccessCodeVouchers {
    id
    code
    season {
      id
      year
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class CustodianGQL extends Apollo.Query<CustodianQuery, CustodianQueryVariables> {
    override document = CustodianDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const DiscountsDocument = gql`
    query discounts {
  discounts(where: {status: {not: {equals: "cancelled"}}}) {
    id
    status
    submissionDate
    rejectionReason {
      reason
    }
    validUntil
    type {
      id
      name
    }
    child {
      id
      firstname
      surname
    }
    vouchers {
      id
      redemptionDate
      ticket {
        id
      }
    }
    season {
      year
    }
  }
  seasons(orderBy: {year: asc}) {
    id
    year
  }
  discountTypes {
    id
    name
    discountPercent
    description
    amountEvents
    amountChildren
    amountEventsDescription
    amountChildrenDescription
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class DiscountsGQL extends Apollo.Query<DiscountsQuery, DiscountsQueryVariables> {
    override document = DiscountsDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const EventDocument = gql`
    query event($id: ID!) {
  event(where: {id: $id}) {
    id
    name
    waitingListLimit
    participantLimit
    availableTicketCountParticipant: availableTicketCount(type: participant)
    availableTicketCountWaitinglist: availableTicketCount(type: waitinglist)
    categories {
      name
    }
    picture {
      url
    }
    organizer {
      name
      phone
      email
      addressStreet
      addressZip
      addressCity
    }
    description
    contactPersonSalutation
    contactPersonFirstname
    contactPersonSurname
    hints
    meals
    careTimeStart
    careTimeEnd
    ageMinimum
    ageMaximum
    locationCity
    locationZipcode
    locationStreet
    costs
    registrationDeadline
    carePeriodCommitted {
      careDays(orderBy: {day: asc}) {
        day
      }
      period {
        id
        startDate
        endDate
        holiday {
          id
          name
        }
        season {
          id
          year
          bookingStart
          bookingStartPartners
        }
      }
    }
  }
  discountTypes {
    id
    name
    discountPercent
    description
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class EventGQL extends Apollo.Query<EventQuery, EventQueryVariables> {
    override document = EventDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const EventsDocument = gql`
    query events($year: Int!) {
  events(
    where: {carePeriodCommitted: {period: {season: {year: {equals: $year}}}}, status: {equals: "approved"}}
  ) {
    id
    name
    waitingListLimit
    participantLimit
    availableTicketCountParticipant: availableTicketCount(type: participant)
    availableTicketCountWaitinglist: availableTicketCount(type: waitinglist)
    categories {
      id
      name
    }
    picture {
      url
    }
    organizer {
      name
    }
    description
    emergencyPhone
    contactPersonSalutation
    contactPersonFirstname
    contactPersonSurname
    hints
    meals
    careTimeStart
    careTimeEnd
    ageMinimum
    ageMaximum
    locationCity
    locationZipcode
    locationStreet
    costs
    registrationDeadline
    carePeriodCommitted {
      careDays(orderBy: {day: asc}) {
        day
      }
      period {
        id
      }
    }
  }
  seasons {
    id
    year
  }
  holidays(where: {periods: {some: {season: {year: {equals: $year}}}}}) {
    id
    name
    periods {
      id
      startDate
      endDate
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class EventsGQL extends Apollo.Query<EventsQuery, EventsQueryVariables> {
    override document = EventsDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const GdprInformationDocument = gql`
    query gdprInformation($custodianId: ID!) {
  custodian(where: {id: $custodianId}) {
    id
    salutation
    firstname
    surname
    addressStreet
    addressHouseNumber
    addressZip
    addressCity
    addressAdditional
    emergencyPhone
    phone
    user {
      id
      email
      createdAt
      name
      isEnabled
      role
      lastLogin
    }
    children {
      id
      firstname
      surname
      healthInsurer
      hasLiabilityInsurance
      gender
      foodIntolerances
      medication
      miscellaneous
      physicalImpairment
      dateOfBirth
      assistanceRequired
      dietaryRegulations
    }
    bookings(where: {isDeleted: {equals: false}}) {
      id
      event {
        name
        season {
          year
        }
      }
      code
      status
      id
      date
      price
      reminderDate
      tickets {
        id
        child {
          id
          firstname
          surname
        }
        discountVoucher {
          id
          redemptionDate
          discount {
            id
            status
            type {
              id
              name
            }
          }
        }
      }
    }
    reservations(where: {isDeleted: {equals: false}}) {
      id
      code
      date
      event {
        name
        season {
          year
        }
      }
      price
      tickets {
        id
        child {
          id
          firstname
          surname
        }
        discountVoucher {
          id
          redemptionDate
          discount {
            id
            status
            type {
              id
              name
            }
          }
        }
      }
    }
    discounts {
      id
      type {
        id
        name
      }
      vouchers {
        id
        redemptionDate
        ticket {
          id
        }
      }
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class GdprInformationGQL extends Apollo.Query<GdprInformationQuery, GdprInformationQueryVariables> {
    override document = GdprInformationDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const LandingDocument = gql`
    query landing($timestamp: DateTime!) {
  seasons(where: {releaseDate: {lte: $timestamp}}, orderBy: {year: desc}, take: 1) {
    id
    year
    bookingStart
    bookingStartPartners
    releaseDate
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class LandingGQL extends Apollo.Query<LandingQuery, LandingQueryVariables> {
    override document = LandingDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const UsersDocument = gql`
    query users {
  users {
    id
    name
    email
    createdAt
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class UsersGQL extends Apollo.Query<UsersQuery, UsersQueryVariables> {
    override document = UsersDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }