import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-simple-language',
  template: `
    <div class="landing-page-padding mb-12 flex justify-center">
      <div class="">
        <h1 class="!mb-5 pt-10 text-xl font-extrabold md:text-2xl">
          Leichte Sprache
        </h1>

        <div class="flex flex-col gap-2">
          <span>
            Die Familien-Region Bamberg bietet ein Angebot an.<br />
            Das Angebot heißt: Bamberger Ferien-Abenteuer.
          </span>
          <span>Das Angebot ist eine Internet-Seite.</span>
          <span>
            Auf der Internet-Seite kann man Veranstaltungen für Kinder
            buchen.<br />
            Die Familien-Region Bamberg will Eltern helfen.<br />
            Die Eltern sollen in der Zeit arbeiten können.<br />
            Und die Kinder sollen Spaß haben.<br />
            Dafür gibt es die Familien-Region Bamberg.<br />
            Alle Veranstaltungen werden von Fach-Leuten gemacht.<br />
          </span>
        </div>

        <h2>Ferien-Abenteuer - Kurz erklärt</h2>
        <span>
          Das Ferien-Abenteuer ist für Kinder.<br />
          Die Kinder kommen aus der Stadt Bamberg.<br />
          Oder die Kinder kommen aus dem Landkreis Bamberg.<br />
          Die Kinder sind zwischen 6 und 14 Jahre alt.<br /><br />
        </span>

        <span class="mb-2 flex">Die Betreuung ist in den Ferien:</span>
        <ul class="list-disc pl-5">
          <li>Ostern</li>
          <li>Pfingsten</li>
          <li>Sommer</li>
          <li>Herbst</li>
          <li>Buß- und Bettag</li>
        </ul>
        <span>
          <br /><br />Die Angebote sind die ganze Woche.<br />
          Die Kinder können jeden Tag von 8 Uhr morgens bis 15 Uhr nachmittags
          betreut werden.<br /><br />
          Kinder mit Behinderung bekommen eine kostenlose Hilfe.<br />
          Die Hilfe heißt: Assistenzkraft.
        </span>

        <h2>Kosten</h2>
        <h3>Teilnahme-Betrag</h3>
        <p>Das Ferien-Abenteuer kostet 17 Euro pro Tag.</p>
        <p>Manchmal gibt es auch Essen.</p>
        <p>Das Essen kostet 7 Euro pro Tag.</p>
        <p>Dann kostet das Ferien-Abenteuer:</p>
        <p>17 Euro + 7 Euro = 24 Euro.</p>

        <p>Es gibt verschiedene Veranstaltungen.</p>
        <p>Zum Beispiel:</p>
        <ul class="mb-2 list-disc pl-5">
          <li>Veranstaltung 1</li>
          <li>Veranstaltung 2</li>
          <li>Veranstaltung 3</li>
        </ul>
        <p>
          Die Kosten für jede Veranstaltung sind im Buchungsportal aufgelistet.
          <br /><br />
        </p>
        <h3>Ermäßigung</h3>
        <div class="flex flex-col gap-2">
          <span>
            Familien können das Ferien-Abenteuer günstiger bekommen.<br />
            Das heißt:<br />
            Die Familien müssen weniger Geld bezahlen.
          </span>
          <span>
            Das wird Ermäßigung genannt.<br />
            Das geht in 2 Fällen.
          </span>
          <span>
            Der Erste Fall ist: Sie haben 3 oder mehr Kinder?<br />
            Dann bekommen Sie 50 Prozent Ermäßigung.<br />
            Das heißt:<br />
            Sie zahlen nur die Hälfte von den Kosten für ein Kind.<br />
            Nur ein Kind aus der Familie kann mitmachen.
          </span>
          <span>
            Das Kind kann bis zu 4 Abenteuer-Wochen günstiger bezahlen.<br />
            Das Kind kann nicht mit anderen Kindern tauschen.
          </span>
          <span>
            Der zweite Fall ist: Sie verdienen wenig Geld?<br />
            Dann bekommen alle Kinder in der Familie 50 Prozent Ermäßigung.<br />
            Sie können für jedes Kind bis zu 4 Abenteuer-Wochen günstiger
            bezahlen.
          </span>
          <span>
            Sie können eine Ermäßigung beantragen.<br />
            Sie können das im Nutzer-Profil machen.<br />
            Oder Sie machen es bei der Buchung von einer Veranstaltung.<br />
            Sie müssen dafür einen Nachweis haben.<br />
            Dann bekommen Sie die Ermäßigung.<br />
            Wie das geht, steht in der Anmeldung zu der Ermäßigung.<br />
            Und es steht in den AGB.
          </span>
        </div>

        <h2>Inklusion - Ferienabenteuer für alle!</h2>
        <p>Das Bamberger Ferien-Abenteuer ist für Kinder.</p>
        <p>Das Angebot ist für alle Kinder und Jugendlichen.</p>
        <p>Auch für Kinder und Jugendliche mit Behinderung.</p>
        <p>Die Lebens-Hilfe Bamberg macht das Angebot mit.</p>
        <p>Die Lebens-Hilfe Bamberg hat eine Gruppe.</p>
        <p>Die Gruppe hilft Menschen mit Behinderung.</p>
        <p>Die Gruppe heißt: „Region - Bamberg inklusiv“.</p>
        <p>
          Kinder und Jugendliche mit Behinderung können am Angebot teilnehmen.
        </p>
        <p>Dafür gibt es eine Assistenz.</p>
        <p>Die Assistenz hilft den Kindern und Jugendlichen bei allem.</p>
        <p>Die Assistenz ist kostenlos.</p>
        <p>Die Assistenz kann nur 10 Tage lang dabei sein.<br /><br /></p>

        <p>Das macht die Gruppe „Region - Bamberg inklusiv“:</p>
        <p>Die Mitarbeiter helfen bei der Auswahl vom Ferien-Programm.</p>
        <p>Und sie helfen bei der Anmeldung.</p>
        <p>Sie können auch die Anmeldung machen.</p>
        <p>Die Gruppe hat Mitarbeiter für Assistenz.</p>
        <p>Das heißt: Sie kümmern sich um das Kind.</p>
        <p>Sie helfen dem Kind bei Problemen.</p>
        <p>Und sie übersetzen für das Kind.<br /><br /></p>

        <p>Sie haben Fragen zur Inklusion?</p>
        <p>Dann können Sie sich an uns wenden.</p>
        <p>Wir sind die Offene Behinderten-Arbeit.</p>
        <p>Unsere Adresse ist:</p>
        <p>Region-Bamberg inklusiv</p>
        <div class="notranslate">
          <p>Melanie Bernt</p>
          <p>Moosstr. 75</p>
          <p>96050 Bamberg</p>
        </div>
        <p>Sie können uns anrufen.</p>
        <p>
          Unsere Telefon-Nummer ist: &nbsp;
          <a
            class="link"
            href="tel:+4995118972104"
            aria-label="+4995118972104 anrufen"
          >
            +49 0 95 11 89 72 10 4
          </a>
        </p>
        <p>Sie können uns eine Mail schicken.</p>
        <p>
          Unsere E-Mail-Adresse ist:&nbsp;
          <a
            class="link"
            href="mailto:melanie.bernt@lebenshilfe-bamberg.de"
            aria-label="E-Mail an melanie.bernt@lebenshilfe-bamberg.de"
          >
            melanie.bernt&commat;lebenshilfe-bamberg.de
          </a>
        </p>

        <h2>Partnerunternehmen</h2>
        <p>Die Familien-Region Bamberg sagt:</p>
        <p>Vielen Dank an alle Partner-Firmen.</p>
        <p>Die Firmen haben uns Geld gegeben.</p>
        <p>Das Geld ist für das Ferien-Abenteuer.</p>
        <p>Dank dem Geld können wir:</p>
        <ul class="mb-2 list-disc pl-5">
          <li>viele verschiedene Veranstaltungen machen</li>
          <li>die Veranstaltungen gut für die Kinder machen</li>
          <li>die Veranstaltungen für die Eltern günstig machen.</li>
        </ul>
        <p><br /><br />Sie arbeiten in einer Firma?</p>
        <p>Oder Sie arbeiten in einer Behörde?</p>
        <p>
          Oder Sie arbeiten in einem anderen Ort in Bamberg oder im Landkreis
          Bamberg?
        </p>
        <p>Dann können Sie das Ferien-Abenteuer unterstützen.</p>
        <p>
          Die Firma oder Behörde kann Sponsor für das Ferien-Abenteuer werden.
        </p>
        <p>
          Dann bekommen die Mitarbeiter und Mitarbeiterinnen von der Firma oder
          Behörde einen Platz im Ferien-Abenteuer.
        </p>
        <p>
          Die Mitarbeiter und Mitarbeiterinnen können dann schon vor dem 1.
          Februar einen Platz buchen.
        </p>
        <p>
          Sie können dann schon ab dem 1. Werk-Tag nach den Weihnachtsferien
          einen Platz buchen.
        </p>

        <h2>Kontakt</h2>
        <p>Sie wollen mehr über das Konzept vom Ferien-Abenteuer wissen?</p>
        <p>Und Sie wollen mehr Infos über die Internet-Seite?</p>
        <p>Dann können Sie mit uns reden.</p>
        <p>Die Ansprechpartnerin ist:</p>
        <p class="notranslate">Maarit Stierle</p>
        <p>
          Die Telefon-Nummer ist: &nbsp;
          <a
            class="link"
            href="tel:+4995185510"
            aria-label="+4995185510 anrufen"
          >
            +49 0 95 18 55 10
          </a>
        </p>
        <p>
          Die E-Mail-Adresse ist:&nbsp;
          <a
            class="link"
            href="mailto:maarit.stierle@lra-ba.bayern.de"
            aria-label="E-Mail an maarit.stierle@lra-ba.bayern.de"
          >
            maarit.stierle&commat;lra-ba.bayern.de
          </a>
          <br /><br />
        </p>
        <p>Die Ansprechpartnerin für das Sponsoring ist:</p>
        <p class="notranslate">Natalie Lother</p>
        <p>
          Die Telefon-Nummer ist: &nbsp;
          <a
            class="link"
            href="tel:+49951871562"
            aria-label="+49951871562 anrufen"
          >
            +49 0 95 18 71 56 2
          </a>
        </p>
        <p>
          Die E-Mail-Adresse ist:&nbsp;
          <a
            class="link"
            href="mailto:natalie.lother@stadt.bamberg.de"
            aria-label="E-Mail an natalie.lother@stadt.bamberg.de"
          >
            natalie.lother&commat;stadt.bamberg.de
          </a>
          <br /><br />
        </p>
        <p>Sie haben Fragen zu den Veranstaltungen?</p>
        <p>Oder Sie wollen wissen:</p>
        <p>Wie viele Plätze sind noch frei?</p>
        <p>Dann wenden Sie sich an den Veranstalter.</p>
      </div>
    </div>
  `,
  styles: `
  :host {
    @apply text-neutralGrayDark;
  }

  h2 {
    @apply font-bold mt-4 mb-2;
  }
  h3 {
    @apply underline mb-2;
  }
  p {
    @apply mb-2
  }
  .absatz {
    @apply mb-2
  }
  `,
})
export class SimpleLanguageComponent {}
