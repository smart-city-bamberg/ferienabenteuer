import { Component } from '@angular/core';

@Component({
  selector: 'short-explanation',
  template: `
    <div class="landing-page-section landing-page-padding">
      <div class="flex flex-col items-center gap-1">
        <h2 tabindex="0" class="landing-section-title">
          Ferienabenteuer - <span class="text-[#E81111]">Kurz erklärt</span>
        </h2>
        <title-color-bars />
      </div>

      <div class="my-8 flex flex-wrap gap-16 md:my-16 md:justify-around">
        <div class="text-box">
          <i class="mi notranslate bg-blue-800" aria-label="Smiley">mood</i>
          <p>
            Das Ferienabenteuer richtet sich an Kinder aus Stadt und Landkreis
            Bamberg im Alter von <strong>6 bis 14 Jahren</strong>
          </p>
        </div>
        <div class="text-box">
          <i class="mi notranslate bg-red-600" aria-label="Kalender">today</i>
          <p>
            Die Betreuung findet in den
            <strong>Oster-, Pfingst-, Sommer-, Herbstferien</strong> und am
            <strong>Buß- und Bettag</strong> statt
          </p>
        </div>
        <div class="text-box">
          <i class="mi notranslate bg-blue-800" aria-label="Uhr">schedule</i>
          <p>
            Es handelt sich um <strong>ganzwöchige Angebote</strong> mit
            täglicher Betreuungszeit von mindestens
            <strong>8 bis 15 Uhr</strong>
          </p>
        </div>
        <div class="text-box">
          <i class="mi notranslate bg-red-600" aria-label="Behinderung"
            >accessible</i
          >
          <p>
            Für Kinder mit Behinderung werden
            <strong>kostenlose Assistenzkräfte</strong>
            bereitgestellt
          </p>
        </div>
      </div>
    </div>
  `,
  styles: [
    `
      :host {
        @apply hyphens-auto break-words;
      }

      .text-box {
        @apply relative flex pt-8 md:h-60 md:w-56 items-center rounded-xl border-2 border-gray-200 p-4 md:p-6 shadow flex-grow md:flex-grow-0;
        .mi {
          @apply absolute -top-6 md:-top-8 left-1/2 flex w-12 h-12 md:h-16 md:w-16 -translate-x-1/2 items-center justify-center rounded-full text-2xl md:text-5xl text-white;
        }
        p {
          @apply block;
        }
      }
    `,
  ],
})
export class ShortExplanationComponent {}
