import { Component } from '@angular/core';

@Component({
  selector: 'contact',
  template: `
    <div class="landing-page-section landing-page-padding">
      <div class="flex flex-col items-center gap-1">
        <span class="landing-section-title">Kontakt</span>
        <title-color-bars />
      </div>

      <div class="flex flex-col justify-center gap-4">
        <p>
          Ihre Ansprechpartnerin zum Konzept des Ferienabenteuers und zur
          Homepage Landratsamt Bamberg:<br />
          <span class="notranslate font-bold">Maarit Stierle</span><br />
          Telefon:
          <a class="link" href="tel:095185510" aria-label="095185510 anrufen">
            0951 85-510</a
          ><br />
          E-Mail:
          <a
            class="link"
            href="mailto:maarit.stierle@lra-ba.bayern.de"
            aria-label="E-Mail an maarit.stierle@lra-ba.bayern.de"
            >maarit.stierle&commat;lra-ba.bayern.de</a
          >
        </p>
        <p>
          Ihre Ansprechpartnerin für alle Fragen rund um die
          Unternehmenspartnerschaft:<br />
          <span class="notranslate font-bold">Natalie Lother</span><br />
          Telefon:
          <a class="link" href="tel:0951871562" aria-label="0951871562 anrufen">
            0951 87-1562</a
          ><br />
          E-Mail:
          <a
            class="link"
            href="mailto:natalie.lother@stadt.bamberg.de"
            aria-label="E-Mail an natalie.lother@stadt.bamberg.de"
            >natalie.lother&commat;stadt.bamberg.de</a
          >
        </p>
        <p>
          Bei <strong>Fragen zu den einzelnen Veranstaltungen</strong> und zum
          Buchungsstand wenden Sie sich bitte
          <a
            class="link"
            routerLink="/kontakt"
            aria-label="Link zur Kontaktseite"
            >direkt an den jeweiligen Veranstalter</a
          >.
        </p>

        <div
          class="my-16 flex flex-wrap items-center justify-center gap-4 lg:justify-between"
        >
          <div class="img-container border-[#b2c3d9] shadow-lg">
            <img
              loading="lazy"
              class="p-8"
              src="../assets/images/landing/contact/stadt_bamberg.png"
              alt="Logo Stadt Bamberg"
            />
          </div>
          <div class="img-container border-[#f6bbbb] shadow-lg">
            <img
              loading="lazy"
              class="p-2"
              src="../assets/images/landing/contact/ferienregion_bamberg.png"
              alt="Logo Familienregion Bamberg"
            />
          </div>
          <div class="img-container border-[#fdeab5] shadow-lg">
            <img
              loading="lazy"
              class="p-3"
              src="../assets/images/landing/sponsoren/landkreis_bamberg.jpg"
              alt="Logo Landkreis Bamberg"
            />
          </div>
        </div>
      </div>
    </div>
  `,
  styles: [
    `
      p {
        @apply text-lg py-2 block;
      }
      .img-container {
        @apply rounded-full border-[16px] p-4 flex items-center w-48 h-48 md:w-64 md:h-64;
      }
    `,
  ],
})
export class ContactComponent {}
