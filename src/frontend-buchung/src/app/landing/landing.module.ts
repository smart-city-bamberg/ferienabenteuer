import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EffectsModule } from '@ngrx/effects';
import { LandingPageEffects } from '../stores/landing-page.effects';
import { ComponentsModule } from '../components/components.module';
import { LandingPageComponent } from './landing-page.component';
import { LandingContainerComponent } from './landing-container.component';
import { TitleColorBarsComponent } from './components/title-color-bars.component';
import { ContactComponent } from './contact.component';
import { CostsComponent } from './costs.component';
import { EventsComponent } from './events.component';
import { FundingProvidersComponent } from './funding-providers.component';
import { HeroComponent } from './hero.component';
import { InclusionComponent } from './inclusion.component';
import { LandingTopBarComponent } from './landing-top-bar.component';
import { PartnersComponent } from './partners.component';
import { ShortExplanationComponent } from './short-explanation.component';
import { RouterModule } from '@angular/router';
import { SimpleLanguageComponent } from './simple-language.component';

@NgModule({
  declarations: [
    LandingPageComponent,
    LandingContainerComponent,
    HeroComponent,
    ShortExplanationComponent,
    EventsComponent,
    LandingTopBarComponent,
    CostsComponent,
    InclusionComponent,
    PartnersComponent,
    ContactComponent,
    TitleColorBarsComponent,
    FundingProvidersComponent,
    SimpleLanguageComponent,
  ],
  imports: [
    CommonModule,
    EffectsModule.forFeature([LandingPageEffects]),
    ComponentsModule,
    RouterModule,
  ],
  exports: [
    LandingPageComponent,
    LandingContainerComponent,
    HeroComponent,
    ShortExplanationComponent,
    EventsComponent,
    LandingTopBarComponent,
    CostsComponent,
    InclusionComponent,
    PartnersComponent,
    ContactComponent,
    TitleColorBarsComponent,
    FundingProvidersComponent,
    SimpleLanguageComponent,
  ],
})
export class LandingModule {}
