import { Component } from '@angular/core';

@Component({
  selector: 'costs',
  template: `
    <div class="landing-page-section landing-page-padding">
      <div class="flex flex-col items-center gap-1">
        <h2 class="landing-section-title">Kosten</h2>
        <title-color-bars />
      </div>

      <div class="grid w-full grid-cols-1 gap-2 md:grid-cols-2 md:gap-8">
        <div class="flex justify-center px-0 py-8 md:px-4 xl:px-16">
          <div
            class="flex flex-col rounded-2xl border border-gray-300 bg-[#fce8e8] p-8 shadow-md xl:p-16"
          >
            <h3 class="text-xl text-[#D10F0F] md:text-3xl">Teilnahmebetrag</h3>
            <p class="hyphens-auto">
              Die Teilnahme am Ferienabenteuer kostet 17 € pro Kind und Tag.
              Teilweise kommen Verpflegungskosten von bis zu 7 € pro Tag dazu.
              Die Gesamtkosten betragen damit für:
            </p>
            <ul class="list-disc pl-4">
              <li>
                4-Tage-Veranstaltung:
                <strong>68&nbsp;€&nbsp;bis&nbsp;96&nbsp;€</strong>
              </li>
              <li>
                5-Tage-Veranstaltung:
                <strong>85&nbsp;€&nbsp;bis&nbsp;120&nbsp;€</strong>
              </li>
            </ul>
            <p>Die Gesamtkosten sind bei jeder Veranstaltung aufgeführt.</p>
          </div>
        </div>

        <div
          class="flex items-center justify-center px-0 py-8 md:px-4 xl:px-16"
        >
          <div
            class="flex flex-col rounded-2xl border border-gray-300 bg-[#ebf2fa] p-8 shadow-md xl:p-16"
          >
            <h3 class="text-xl text-[#0B62EF] md:text-3xl">Ermäßigung</h3>
            <p>
              Familien können Ermäßigung des Ferienabenteuers in zwei Fällen
              beantragen:
            </p>
            <ol class="list-decimal pl-4">
              <li>
                <strong>Kinderreichtum (3 oder mehr Kinder):</strong><br />
                50 % Ermäßigung von bis zu 4 Veranstaltungen für ein Kind (nicht
                übertragbar, auch nicht innerhalb der Familie).
              </li>
              <li>
                <strong>Geringes Einkommen:</strong><br />
                Alle Kinder der Familie erhalten 50 % Ermäßigung. Es können pro
                Kind bis zu 4 Abenteuerwochen ermäßigt werden.
              </li>
            </ol>
            <p class="!text-xs">
              Eine Ermäßigung kann im Nutzerprofil oder bei der Buchung der
              Veranstaltung beantragt werden. Ein entsprechender Nachweis ist
              für die Gewährung vorzulegen. Einzelheiten sind bei der
              Beantragung beschrieben und in den
              <a
                class="link"
                routerLink="/agb"
                role="link"
                aria-label="Link zu den AGB´s"
                >AGB</a
              >
              geregelt.
            </p>
          </div>
        </div>
      </div>
    </div>
  `,
  styles: [
    `
      :host {
        @apply hyphens-auto;
      }

      p,
      ul,
      ol {
        @apply text-base md:text-lg py-2;
      }
      p {
        @apply block;
      }
    `,
  ],
})
export class CostsComponent {}
