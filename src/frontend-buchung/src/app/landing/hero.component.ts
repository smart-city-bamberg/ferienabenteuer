import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { selectBookingStart } from 'src/app/stores/landing/landing.selectors';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'hero',
  template: `
    <div
      class="landing-page-padding mb-12 mt-0 flex flex-col items-center justify-center gap-10 md:my-32 md:max-w-[110rem] md:gap-20"
    >
      <div class="grid grid-cols-1 gap-6 md:grid-cols-2 lg:gap-16">
        <div class="flex-gow flex flex-col items-center">
          <h1 aria-label="Ferienabenteuer Bamberg">
            <img
              width="821"
              height="580"
              class="flex-grow"
              src="../assets/images/ferienabenteuer_bamberg_logo.svg"
              alt="Logo vom Ferienabentuer Bamberg"
            />
          </h1>

          <div class="flex-gow mt-2 flex-col">
            <p
              class="text-justify text-base sm:text-lg md:text-start md:text-xl"
            >
              Das Bamberger Ferienabenteuer ist ein Angebot der Familienregion
              Bamberg, das berufstätige Eltern bei der Vereinbarkeit von Familie
              und Beruf unterstützen möchte. Stadt und Landkreis Bamberg bieten
              eine Homepage an, über die Veranstaltungen des Ferienabenteuers
              bequem gebucht werden können. Alle Veranstaltungen werden durch
              erfahrene regionale Anbieter durchgeführt.
            </p>
            <p class="mt-4 text-base sm:text-lg md:text-start md:text-xl">
              Buchungsstart: {{ bookingStart$ | async | date : 'dd.MM.yyyy' }}
            </p>
            <div
              class="mt-6 flex flex-col items-center justify-start gap-4 sm:flex-row"
            >
              <a
                *ngIf="registrationEnabled"
                class="button-white"
                routerLink="/registrieren"
                role="link"
                aria-label="Registrieren"
              >
                Registrieren
              </a>
              <a
                class="button px-4 py-1"
                routerLink="/veranstaltungen"
                role="link"
                aria-label="Zum Buchungsportal"
              >
                Zum Buchungsportal
                <i class="mi notranslate ml-2">arrow_forward</i>
              </a>
            </div>
          </div>
        </div>

        <div class="flex flex-grow flex-col items-center justify-center p-8">
          <img
            class="flex aspect-square max-w-[100%] rounded-full border-2 border-white object-cover sm:max-w-sm md:max-w-[100%]"
            src="../assets/images/landing/children.webp"
            alt="Kinder basteln mit Werkzeug"
          />
          <span class="mt-1 text-xs text-gray-600">Bildquelle LRA Bamberg</span>
        </div>
      </div>
    </div>
  `,
  styles: [
    `
      :host {
        @apply flex flex-col flex-grow bg-gradient-to-b from-[#8bb0df] to-white pb-4 items-center hyphens-auto;
      }
    `,
  ],
})
export class HeroComponent {
  registrationEnabled = environment.features.registration;

  bookingStart$ = this.store.select(selectBookingStart);

  constructor(private store: Store) {}
}
