import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'funding-providers',
  template: `
    <div class="landing-page-section landing-page-padding">
      <div class="flex flex-col items-center gap-1">
        <span class="landing-section-title">Durchgeführt von</span>
        <title-color-bars />
      </div>
      <div
        class="flex flex-grow flex-col gap-4 md:justify-center lg:flex-row lg:flex-wrap lg:justify-center"
      >
        <div class="flex flex-col md:items-center lg:flex-row">
          <div class="donor-logo">
            <img
              loading="lazy"
              class="h-48"
              src="../assets/images/landing/funding-providers/scb_logo_transparent_web.png"
              alt="Logo Smart City Bamberg"
            />
          </div>
          <div class="donor-logo">
            <img
              loading="lazy"
              class=""
              src="../assets/images/landing/funding-providers/bmwsb_2021_WebSVG_de.svg"
              alt="Logo Bundesministerium für Wohnen, Stadtentwicklung und Bauwesen"
            />
          </div>
        </div>
        <div class="donor-logo lg:mt-16">
          <img
            loading="lazy"
            class="h-16"
            src="../assets/images/landing/funding-providers/KfW_Bankengruppe_20xx_logo.svg"
            alt="Logo KFW"
          />
        </div>
      </div>
    </div>
  `,
  styles: [
    `
      p {
        @apply text-lg py-2 block;
      }

      .donor-logo {
        @apply flex flex-grow justify-center px-8;

        // img {
        //   @apply h-48;
        // }
      }
    `,
  ],
})
export class FundingProvidersComponent {}
