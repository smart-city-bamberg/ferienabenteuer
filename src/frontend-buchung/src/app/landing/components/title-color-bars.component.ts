import { Component } from '@angular/core';

@Component({
  selector: 'title-color-bars',
  template: `
    <div class="flex items-center gap-2">
      <div class="h-1 w-6 bg-primaryYellow"></div>
      <div class="h-1 w-6 bg-primaryLight"></div>
      <div class="h-1 w-6 bg-primaryRed"></div>
    </div>
  `,
  styles: [],
})
export class TitleColorBarsComponent {}
