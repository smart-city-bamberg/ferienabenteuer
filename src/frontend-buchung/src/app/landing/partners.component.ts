import { Component } from '@angular/core';

@Component({
  selector: 'partners',
  template: `
    <div class="landing-page-section landing-page-padding">
      <div class="flex flex-col items-center gap-1">
        <h2 class="landing-section-title">Partnerunternehmen</h2>
        <title-color-bars />
      </div>

      <div class="flex flex-col justify-center gap-8">
        <p>
          Die Familienregion Bamberg dankt allen Partnerunternehmen für die
          wertvolle Unterstützung des Ferienabenteuers! Die Sponsoring- und
          Spendenbeiträge der Unternehmen machen die besondere Vielfalt und
          pädagogische Qualität der Ferienabenteuerwochen und niedrige
          Kostenbeiträge für alle Eltern möglich.
        </p>

        <p>
          Jedes Unternehmen, aber auch Behörden und andere Arbeitgeber mit Sitz
          in Stadt oder Landkreis Bamberg können Partner des Ferienabenteuers
          werden. Die Beschäftigten der Sponsoring-Partner können benötigte
          Betreuungsplätze bereits vor dem öffentlichen Buchungsstart (jährlich
          am 1. Februar) für die eigenen Kinder buchen: Für sie ist die
          Buchungsfunktion bereits ab dem 1. Werktag nach den Weihnachtsferien
          geöffnet.
        </p>

        <h3 class="text-center text-xl md:text-3xl">Sponsoringpartner</h3>
        <div class="flex flex-wrap justify-center">
          <img
            *ngFor="let sponsor of sponsorImages"
            [src]="sponsorPath + '/' + sponsor.image"
            [alt]="'Logo ' + sponsor.label"
            loading="lazy"
          />
        </div>
        <h3 class="text-center text-xl md:text-3xl">Spendenpartner</h3>
        <div class="flex flex-wrap justify-center">
          <img
            *ngFor="let donor of donorImages"
            [src]="donorsPath + '/' + donor.image"
            [alt]="'Logo ' + donor.label"
            loading="lazy"
          />
        </div>
      </div>
    </div>
  `,
  styles: [
    `
      :host {
        @apply hyphens-auto;
      }

      p {
        @apply text-lg py-2 block;
      }
      img {
        @apply m-1 h-16 rounded-lg border border-gray-300 p-1;

        @screen md {
          @apply m-3 h-24 p-3;
        }
      }
    `,
  ],
})
export class PartnersComponent {
  readonly sponsorPath = '../assets/images/landing/sponsoren';
  readonly donorsPath = '../assets/images/landing/spender';

  readonly sponsorImages = [
    { image: `actimed.webp`, label: 'Actimed' },
    { image: `bosch.webp`, label: 'Bosch' },
    { image: `brose.webp`, label: 'Brose' },
    { image: `erzbistum_bamberg.webp`, label: 'Erzbistum Bamberg' },
    { image: `justizbehoerden_bamberg.webp`, label: 'Justizbehörden Bamberg' },
    { image: `landkreis_bamberg.webp`, label: 'Landkreis Bamberg' },
    { image: `lifbi.webp`, label: 'Leibniz-Institut für Bildungsverläufe' },
    { image: `magellan.webp`, label: 'Magellan' },
    { image: `massiv_moebel_24.webp`, label: 'Massiv Möbel 24.de' },
    { image: `mgo.webp`, label: 'Mediengruppe Oberfranken' },
    { image: `sozialstiftung_bamberg.webp`, label: 'Sozialstiftung Bamberg' },
    { image: `sparkasse_bamberg.webp`, label: 'Sparkasse Bamberg' },
    { image: `sps.webp`, label: 'SPS' },
    { image: `st_georg_apotheke.webp`, label: 'Sankt Georg Apotheke' },
    { image: `stadt_bamberg.webp`, label: 'Stadt Bamberg' },
    { image: `stadtwerke_bamberg.webp`, label: 'Stadtwerke Bamberg' },
    { image: `universitaet_bamberg.webp`, label: 'Universität Bamberg' },
    { image: `vr_bank.webp`, label: 'VR Bank Bamberg-Forchheim eG' },
    { image: `weyermann_malz.webp`, label: 'Weyermann Malz' },
  ];

  readonly donorImages = [
    { image: 'elektro_wittner.webp', label: 'Elektro Wittner' },
    { image: 'lohmann_koester.webp', label: 'Lohmann Koester' },
  ];
}
