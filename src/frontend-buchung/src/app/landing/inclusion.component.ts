import { Component } from '@angular/core';

@Component({
  selector: 'inclusion',
  template: `
    <div class="landing-page-section landing-page-padding">
      <div class="flex flex-col items-center gap-1">
        <h2 class="landing-section-title">
          Inklusion - Ferienabenteuer für alle!
        </h2>
        <title-color-bars />
      </div>

      <div class="grid grid-cols-1 gap-8 md:grid-cols-2">
        <div class="mt-2 flex-col text-lg">
          <p>
            Das Bamberger Ferienabenteuer richtet sich auch an Kinder und
            Jugendliche mit Behinderung. In Kooperation mit „<strong
              class="hyphens-none text-primaryLight"
              >Region - Bamberg inklusiv</strong
            >“ der Offenen Behindertenarbeit der Lebenshilfe Bamberg werden
            inklusive Rahmenbedingungen geschaffen, so dass auch Kinder und
            Jugendliche mit Behinderung am regulären Ferienabenteuer teilnehmen
            können. Pro Kind kann eine Assistenz für 10 Angebotstage kostenfrei
            angeboten werden.

            <br /><br />
            „<strong class="hyphens-none text-primaryLight"
              >Region - Bamberg inklusiv</strong
            >“ der Offenen Behindertenarbeit:
          </p>
          <ul class="list-disc pl-4">
            <li>
              berät bei der Auswahl des Ferienprogramms und klärt den
              notwendigen Unterstützungsbedarf
            </li>
            <li>nimmt bei Bedarf auch die Anmeldung entgegen</li>
            <li>
              stellt dem Kind einen persönlichen Assistenten zur Seite, der das
              Kind begleitet, unterstützt und bei Verständigungsproblemen
              zwischen den Kindern vermittelt
            </li>
          </ul>

          <p>
            Bei Fragen zur Inklusion wenden Sie sich an:
            <br />
            „Region-Bamberg inklusiv“ der Offenen Behindertenarbeit
            <br />
            <span class="notranslate"
              >Melanie Bernt
              <br />
              Moosstr. 75, 96050 Bamberg
              <br
            /></span>
            Telefon:
            <a
              class="link"
              href="tel:+4995118972104"
              aria-label="+4995118972104 anrufen"
            >
              +49 (0) 951 1897 2104
            </a>
            <br />
            E-Mail:
            <a
              class="link"
              href="mailto:melanie.bernt@lebenshilfe-bamberg.de"
              aria-label="E-Mail an melanie.bernt@lebenshilfe-bamberg.de"
            >
              melanie.bernt&commat;lebenshilfe-bamberg.de
            </a>
          </p>
        </div>

        <div class="flex flex-grow flex-col items-center justify-center px-8">
          <img
            width="450"
            height="600"
            class="mx-16 object-cover"
            src="../assets/images/ferienabenteuer_bamberg_inklusion.webp"
            alt="Kinderhände"
          />
          <span class="mt-1 text-xs text-gray-600">Bildquelle LRA Bamberg</span>
        </div>
      </div>
    </div>
  `,
  styles: [
    `
      :host {
        @apply hyphens-auto;
      }

      p {
        @apply py-4 block;
      }
    `,
  ],
})
export class InclusionComponent {}
