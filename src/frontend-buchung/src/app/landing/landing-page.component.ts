import { Component } from '@angular/core';

@Component({
  selector: 'landing-page',
  template: `
    <div class="sections flex flex-col">
      <hero id="start" />
      <div class="flex flex-col items-center">
        <div class="flex flex-col md:max-w-[110rem]">
          <short-explanation id="kurz-erklaert" />
          <!-- <events id="veranstaltungen" /> -->
          <costs id="kosten" />
          <inclusion id="inklusion" />
          <partners id="partner" />
          <contact id="kontakt" />
          <funding-providers id="foerdermittelgeber" />
        </div>
      </div>
    </div>
  `,
  styles: [
    `
      a {
        @apply text-3xl md:text-base text-textDark;
      }
      button {
        @apply text-2xl md:text-base w-3/4 md:w-auto;
      }
      .sections > * {
        @apply scroll-mt-20;
      }
    `,
  ],
})
export class LandingPageComponent {
  public menuClosed = true;

  toggle() {
    this.menuClosed = !this.menuClosed;
  }
}
