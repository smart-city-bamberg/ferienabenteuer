import { Component } from '@angular/core';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'landing-top-bar',
  template: `
    <div
      class="flex h-16 w-full flex-row items-center justify-between gap-8 bg-[#346FBC] px-6 md:h-20 md:justify-start md:pr-12 lg:pr-28 xl:pr-32 2xl:pr-64"
    >
      <ul class="z-10 flex flex-row items-center gap-3 md:z-0">
        <li
          class="menu-item text-white hover:text-white/80"
          aria-label="Seite übersetzen"
        >
          <app-google-translate *ngIf="showTranslateButton" />
        </li>

        <li class="menu-item">
          <a
            class="flex flex-row items-center gap-1 !text-sm"
            routerLink="/leichte-sprache"
            aria-label="leichte Sprache Seite"
            ><i class="mi notranslate">local_library</i>
            <p class="hidden md:flex">Leichte Sprache</p></a
          >
        </li>
      </ul>
      <nav
        [ngClass]="{ hidden: !menuOpen }"
        class="fixed left-0 top-0 hidden h-full w-full flex-col bg-[#346FBC] shadow-lg md:relative md:mt-0 md:block md:w-auto md:flex-grow md:flex-row md:bg-inherit md:bg-none md:shadow-none"
      >
        <ul
          class="flex h-full flex-col items-center justify-start gap-4 pt-16 font-extrabold md:flex-row md:flex-wrap md:justify-center md:pt-0 xl:gap-8"
        >
          <li
            class="menu-item"
            (click)="hideMenu()"
            *ngFor="let menuItem of menuItems"
          >
            <a
              [href]="menuItem.link"
              role="link"
              [ariaLabel]="menuItem.ariaLabel"
              >{{ menuItem.label }}</a
            >
          </li>
          <li>
            <a
              class="button !border !border-white p-2 px-4 py-1"
              routerLink="/veranstaltungen"
              (click)="hideMenu()"
              role="link"
              aria-label="Link zum Buchungsportal"
            >
              Buchungsportal
            </a>
          </li>
        </ul>
      </nav>

      <button
        id="menu-button"
        type="button"
        (click)="toggleMobileMenu()"
        class="md:hidden"
        role="button"
        aria-label="Menü Button"
      >
        <i class="mi notranslate text-3xl">menu</i>
      </button>
    </div>
  `,
  styles: [
    `
      :host {
        @apply flex z-10 shadow-lg;
      }
      a {
        @apply text-xl md:text-base lg:text-xl;

        &:not(.button) {
          @apply text-white hover:text-primaryLight;
        }
      }
      button {
        @apply lg:text-xl;
      }
      li {
        @apply cursor-pointer;
      }
      .menu-item {
        @apply cursor-pointer;
        a {
          @apply my-1 hover:text-white/80 md:my-0;
        }
      }
    `,
  ],
})
export class LandingTopBarComponent {
  public menuOpen = false;
  showTranslateButton = environment.features.translate;

  menuItems = [
    {
      link: '#start',
      label: 'Start',
      ariaLabel: 'Link zur Startseite',
    },
    {
      link: '#kurz-erklaert',
      label: 'Kurz erklärt',
      ariaLabel: 'Link zum Abschnitt Kurz erklärt',
    },
    {
      link: '#kosten',
      label: 'Kosten',
      ariaLabel: 'Link zum Abschnitt Kosten',
    },
    {
      link: '#inklusion',
      label: 'Inklusion',
      ariaLabel: 'Link zum Abschnitt Inklusion',
    },
    {
      link: '#partner',
      label: 'Partner',
      ariaLabel: 'Link zum Abschnitt Partner',
    },
    {
      link: '#kontakt',
      label: 'Kontakt',
      ariaLabel: 'Link zum Abschnitt Kontakt',
    },
  ];

  hideMenu() {
    this.menuOpen = false;
  }
  toggleMobileMenu() {
    this.menuOpen = !this.menuOpen;
  }
}
