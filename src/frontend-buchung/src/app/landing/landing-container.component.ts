import { Component } from '@angular/core';

@Component({
  selector: 'landing-container',
  template: `
    <landing-top-bar class="sticky left-0 right-0 top-0" />
    <div class="">
      <router-outlet />
    </div>
  `,
  styles: [
    `
      :host {
        @apply flex flex-col flex-grow;
      }
    `,
  ],
})
export class LandingContainerComponent {}
