import { Component } from '@angular/core';

@Component({
  selector: 'events',
  template: `
    <div class="landing-page-section landing-page-padding">
      <div class="flex flex-col items-center gap-1">
        <span class="landing-section-title">Veranstaltungen - Saison 2024</span>
        <title-color-bars />
      </div>

      <!-- <div class="grid grid-cols-3 justify-around gap-8">
        <div
          *ngFor="let event of [1, 2, 3, 4, 5, 6]"
          class="flex cursor-pointer flex-col overflow-hidden rounded-xl border border-primaryLight/40 bg-white shadow-md transition-shadow hover:shadow-lg md:w-[22rem]"
        >
          <div
            class="flex h-44 flex-col justify-between bg-cover bg-center"
            [style.background-image]="'url(https://picsum.photos/400/300)'"
          >
            <div class="mt-4 flex flex-row justify-end">
              <div class="flex flex-col gap-1">
                <p
                  class="items-end rounded-l-md bg-white px-2 font-extrabold text-textDark"
                >
                  Kategorie
                </p>
              </div>
            </div>

            <div
              class="bg-gradient-to-t from-black/90 to-transparent px-2 pt-4 text-white [text-shadow:_0_0_0.3rem_rgb(0_0_0_)]"
            >
              <p class="text-xs">Organisator</p>
              <p class="mb-1 font-extrabold">Veranstaltungsname</p>
            </div>
          </div>
          <div
            class="mx-3 my-1 flex flex-row items-center justify-between text-textDark"
          >
            <div class="flex flex-col gap-1 text-xs md:text-sm">
              <p class="flex items-center">
                <i class="mi notranslate mr-2">schedule</i>
                01.12.2023 - 31.12.2023
              </p>
              <p class="flex items-center">
                <i class="mi notranslate mr-2">location_on</i>
                Jena, Abc Strße 123
              </p>
              <p class="flex items-center">
                <i class="mi notranslate mr-2">person</i>
                10 - 14 Jahre
              </p>
            </div>
            <div class="flex flex-col">
              <p class="justify-center text-lg font-black">100 €</p>
              <button>Buchen</button>
            </div>
          </div>
        </div>
      </div> -->

      <button class="px-4 py-1" routerLink="/veranstaltungen">
        Zum Buchungsportal
        <i class="mi notranslate ml-2">arrow_forward</i>
      </button>
    </div>
  `,
  styles: [
    `
      .text-box {
        @apply relative flex h-60 w-56 items-center rounded-xl border-2 border-gray-200 p-6 shadow;
        .icon {
          @apply absolute -top-8 left-1/2 flex h-16 w-16 -translate-x-1/2 items-center justify-center rounded-full;
        }
        p {
          @apply text-center block;
        }
        .event-image {
          background-image: url('https://picsum.photos/400/300');
        }
      }
    `,
  ],
})
export class EventsComponent {}
