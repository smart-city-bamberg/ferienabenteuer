import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TopBarComponent } from './top-bar.component';
import { FooterComponent } from './footer.component';
import { RouterModule } from '@angular/router';
import { CareDaysCalendarComponent } from './care-days-calendar.component';
import { DiscountsComponent } from './discounts.component';
import { CareDaysComponent } from './care-days.component';
import { DirectivesModule } from '../directives/directives.module';
import { CartInfoComponent } from './cart-info.component';
import { FormsModule } from '@angular/forms';
import { GoogleTranslateComponent } from './google-translate.component';

@NgModule({
  declarations: [
    TopBarComponent,
    FooterComponent,
    CareDaysCalendarComponent,
    CareDaysComponent,
    DiscountsComponent,
    CartInfoComponent,
    GoogleTranslateComponent,
  ],
  imports: [CommonModule, RouterModule, DirectivesModule, FormsModule],
  exports: [
    TopBarComponent,
    FooterComponent,
    CareDaysCalendarComponent,
    CareDaysComponent,
    DiscountsComponent,
    GoogleTranslateComponent,
  ],
})
export class ComponentsModule {}
