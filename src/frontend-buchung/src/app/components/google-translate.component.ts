import { Component, Inject, Renderer2, ViewEncapsulation } from '@angular/core';
import { CommonModule, DOCUMENT } from '@angular/common';

const google = { translate: {} as any };

@Component({
  selector: 'app-google-translate',
  template: `
    <button
      class="flex flex-row items-center gap-1 border-none bg-transparent text-sm text-inherit"
      *ngIf="!googleTranslateLoaded"
      (click)="loadGoogleTranslate()"
    >
      <i class="mi"
        ><span class="material-symbols-outlined notranslate" aria-hidden="true">
          g_translate
        </span></i
      >
      <span aria-label="Seite übersetzen">Language</span>
    </button>
    <div *ngIf="googleTranslateLoaded" id="google_translate_element"></div>
  `,
  styles: [``],
})
export class GoogleTranslateComponent {
  constructor(
    private _renderer2: Renderer2,
    @Inject(DOCUMENT) private _document: Document
  ) {}

  googleTranslateLoaded = false;

  loadGoogleTranslate() {
    let script = this._renderer2.createElement('script');
    script.type = 'text/javascript';
    script.src =
      'https://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit';

    this._renderer2.appendChild(this._document.head, script);
    this.googleTranslateLoaded = true;
  }
}
