import { Component } from '@angular/core';
import {
  selectCartEventsWithTicketsCount,
  selectCartValidUntil,
} from '../stores/checkout/checkout.selectors';
import { Store } from '@ngrx/store';
import { switchMap, interval, map } from 'rxjs';
import { CheckoutActions } from '../stores/checkout/checkout.actions';

@Component({
  selector: 'cart-info',
  template: `
    <button
      aria-label="Warenkorb um 30min verlängern"
      class="mi notranslate border-none bg-white text-xl text-primaryDark"
      (click)="extendCartTerm()"
      *ngIf="extendCartTermVisible$ | async"
    >
      refresh
    </button>
    <span class="notranslate text-base">{{ remainingTimeLabel$ | async }}</span>
    <a
      routerLink="/checkout/warenkorb"
      aria-label="Warenkorb"
      class="relative flex items-center"
    >
      <i class="mi notranslate text-3xl">shopping_cart</i>
      <span
        class="absolute -end-2 -top-2 inline-flex h-5 w-5 items-center justify-center rounded-full border-2 border-white bg-primaryRed text-xs font-bold text-white"
        *ngIf="ticketsCount$ | async; let ticketsCount"
      >
        {{ ticketsCount }}
      </span>
    </a>
  `,
  styles: [
    `
      :host {
        @apply flex items-center gap-2;
      }
    `,
  ],
})
export class CartInfoComponent {
  ticketsCount$ = this.store.select(selectCartEventsWithTicketsCount);
  cartValidUntil$ = this.store.select(selectCartValidUntil);

  remainingTime$ = this.cartValidUntil$.pipe(
    switchMap((validUntil) =>
      interval(250).pipe(map(() => this.calculateRemainingTime(validUntil)))
    )
  );

  remainingTimeLabel$ = this.remainingTime$.pipe(
    map((remainingTime) => this.buildRemainingTimeLabel(remainingTime))
  );

  extendCartTermVisible$ = this.remainingTime$.pipe(
    map((remainingTime) => {
      if (!remainingTime) {
        return false;
      }

      const fiveMinutes = 5 * 60;
      return remainingTime > 0 && remainingTime < fiveMinutes;
    })
  );

  constructor(private store: Store) {}

  extendCartTerm() {
    this.store.dispatch(CheckoutActions.extendCartTerm());
  }

  private calculateRemainingTime(
    validUntil: Date | undefined
  ): number | undefined {
    if (
      validUntil === undefined ||
      new Date(validUntil).getTime() < Date.now()
    ) {
      return undefined;
    }

    const timeDifference = Math.floor(
      (new Date(validUntil).getTime() - Date.now()) / 1000
    );

    return timeDifference;
  }

  private buildRemainingTimeLabel(
    timeDifference: number | undefined
  ): string | undefined {
    if (!timeDifference) {
      return undefined;
    }

    const minutes = Math.floor(timeDifference / 60)
      .toString()
      .padStart(2, '0');
    const seconds = (timeDifference % 60).toString().padStart(2, '0');

    return `${minutes}:${seconds}`;
  }
}
