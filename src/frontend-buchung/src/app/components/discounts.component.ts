import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { selectChildren } from 'src/app/stores/children/children.selectors';
import { DiscountsActions } from 'src/app/stores/discounts/discounts.actions';
import {
  selectDiscountTypes,
  selectDiscountsForSeason,
  selectHasDiscounts,
  selectSeasons,
  selectSelectedSeason,
} from 'src/app/stores/discounts/discounts.selectors';

@Component({
  selector: 'app-discounts',
  template: `
    <div class="mt-4 flex flex-col gap-4">
      <div>
        <select
          id="saisons"
          aria-label="Saison auswählen"
          class="rounded-lg !border !border-primaryLight/30 !border-opacity-20 bg-white px-2 py-[0.4rem] text-base font-medium text-textDark"
          [ngModel]="selectedSeason$ | async"
          #seasonSelector
          (change)="selectSeason(seasonSelector.value)"
        >
          <option
            *ngFor="let season of seasons$ | async; trackBy: identifySeason"
            [value]="season.year"
          >
            Saison
            <p class="notranslate">&nbsp;{{ season.year }}</p>
          </option>
        </select>
      </div>
      <div class="flex flex-col gap-4 md:flex-row">
        <div
          class="white-card flex flex-col md:w-[20rem]"
          *ngFor="let discountType of discountTypes$ | async"
        >
          <div class="flex justify-center rounded-lg bg-[#C25200] p-2">
            <span class="flex flex-row font-bold text-white">
              <p class="notranslate">
                {{ discountType.discountPercent }}&nbsp;
              </p>
              % Ermäßigung
            </span>
          </div>
          <div class="flex flex-grow flex-col justify-between">
            <div>
              <h3 class="mt-4 text-lg font-bold">{{ discountType.name }}</h3>
              <p class="text-sm leading-tight">
                {{ discountType.description }}
              </p>
              <p class="text-[#757575 text-xs font-bold">
                Nachweis erforderlich
              </p>
            </div>
            <div class="">
              <div class="mt-4 flex flex-col">
                <div class="flex flex-row items-center gap-3 text-textDark">
                  <i class="mi notranslate">check_circle</i>
                  <p>{{ discountType.amountEventsDescription }}</p>
                </div>
                <div class="flex flex-row items-center gap-3 text-textDark">
                  <i class="mi notranslate">check_circle</i>
                  <p>{{ discountType.amountChildrenDescription }}</p>
                </div>
              </div>
              <div class="mt-4 flex flex-grow flex-col gap-4">
                <select
                  id="child"
                  aria-label="Kind auswählen"
                  class="block w-full rounded-lg border border-gray-300 p-2.5 text-sm font-bold text-textDark focus:border-primaryLight focus:ring-primaryLight"
                  #discountTypeChild
                >
                  <option value="" disabled selected>Auswählen</option>
                  <option
                    *ngFor="let child of children$ | async"
                    [value]="child.id"
                    class="notranslate"
                  >
                    {{ child.firstname }} {{ child.surname }}
                  </option>
                </select>
                <button
                  aria-label="Ermäßigung beantragen"
                  class="flex py-1"
                  (click)="
                    applyForDiscount(discountType.id, discountTypeChild.value)
                  "
                >
                  Beantragen
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="divider my-8"></div>
    <div
      *ngIf="!(hasDiscounts$ | async)"
      class="mb-6 flex items-center justify-center"
    >
      <p class="text-center text-lg font-semibold text-[#676E75] md:text-xl">
        Es sind noch keine Ermäßigungen vorhanden!
      </p>
    </div>
    <div *ngIf="hasDiscounts$ | async" class="flex flex-col space-y-6">
      <div
        class="white-card flex flex-col gap-2 md:flex-row md:justify-between"
        *ngFor="let discount of discounts$ | async"
      >
        <div class="flex flex-col gap-6 md:flex-grow md:flex-row md:gap-8">
          <div
            class="flex flex-row gap-8 md:flex-grow md:justify-between md:gap-[10%]"
          >
            <div
              class="flex flex-grow flex-col gap-2 md:w-[30%] md:flex-row md:justify-between"
            >
              <div class="flex flex-col">
                <p class="data-label">Status</p>
                <p
                  class="data badge w-28 justify-center md:w-24 xl:w-28"
                  [ngClass]="badgeColors[discount.status]"
                >
                  {{ badgeLabels[discount.status] }}
                </p>
              </div>
              <div class="flex flex-col md:w-[30%]">
                <p class="data-label">Kind</p>
                <p class="data notranslate">
                  {{ discount.child.firstname }} {{ discount.child.surname }}
                </p>
              </div>
              <div class="flex flex-col">
                <p class="data-label">Saison</p>
                <p class="data notranslate">
                  {{ discount.season.year }}
                </p>
              </div>
            </div>
            <div
              class="flex flex-grow flex-col gap-2 md:flex-row md:justify-between"
            >
              <div class="flex flex-col">
                <p class="data-label">Beantragt</p>
                <p class="data notranslate">
                  {{ discount.submissionDate | date : 'dd.MM.yyyy' }}
                </p>
              </div>
              <div class="flex flex-col md:w-32">
                <p class="data-label">Art</p>
                <p class="data">{{ discount.type.name }}</p>
              </div>
              <div class="flex flex-col">
                <p class="data-label">Eingelöst</p>
                <p class="data notranslate">
                  {{ discount.redeemCount }} |
                  {{ discount.vouchers.length }}
                </p>
              </div>
            </div>
          </div>
          <div class="flex flex-col items-center justify-center md:w-[12%]">
            <button
              aria-label="Stornieren"
              [disabled]="
                discount.status == 'approved' ||
                discount.status == 'overdue' ||
                discount.status == 'rejected'
              "
              (click)="cancelDiscount(discount.id)"
            >
              Stornieren
            </button>
          </div>
        </div>
      </div>
    </div>
  `,
  styles: [
    `
      :host {
        @apply flex flex-col;
      }

      .data-label {
        @apply font-bold;
      }
      .data {
        @apply text-sm leading-tight;
      }
    `,
  ],
})
export class DiscountsComponent {
  discounts$ = this.store.select(selectDiscountsForSeason);
  discountTypes$ = this.store.select(selectDiscountTypes);
  children$ = this.store.select(selectChildren);
  hasDiscounts$ = this.store.select(selectHasDiscounts);
  selectedSeason$ = this.store.select(selectSelectedSeason);
  seasons$ = this.store.select(selectSeasons);

  badgeColors: { [index: string]: string } = {
    pending: 'orange',
    overdue: 'red',
    approved: 'green',
    rejected: 'gray',
  };

  badgeLabels: { [index: string]: string } = {
    pending: 'ausstehend',
    overdue: 'überfällig',
    approved: 'gewährt',
    rejected: 'abgelehnt',
  };

  constructor(private store: Store) {}

  identifySeason(index: number, season: any) {
    return season.year;
  }

  applyForDiscount(discountType: string, child: string) {
    this.store.dispatch(
      DiscountsActions.applyForDiscount({
        discountType,
        child,
      })
    );
  }

  cancelDiscount(discountId: string) {
    this.store.dispatch(DiscountsActions.cancelDiscount({ discountId }));
  }

  selectSeason(year: string) {
    this.store.dispatch(DiscountsActions.selectYear({ year: parseInt(year) }));
  }
}
