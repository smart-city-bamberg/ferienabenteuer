import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';

type CareDay = {
  date: Date;
  available: boolean;
  enabled: boolean;
};

@Component({
  selector: 'app-care-days-calendar',
  template: `
    <div
      *ngFor="let dayRow of careDays"
      class="flex flex-grow flex-row justify-evenly gap-1 text-sm sm:text-lg"
    >
      <div
        *ngFor="let day of dayRow"
        class="flex flex-grow cursor-default flex-col items-center rounded-lg border p-[3px]"
        [ngClass]="{
          'bg-gray-100 border-gray-100': !day.enabled,
          'bg-primaryLight/10 border-primaryLight/20': day.enabled
        }"
      >
        <p class="font-bold">{{ day.date | date : 'EE' }}</p>
        <p class="notranslate">{{ day.date | date : 'dd' }}.</p>
        <p>{{ day.date | date : 'MMM' }}</p>
        <p>
          <span *ngIf="day.enabled"
            ><i class="mi notranslate text-xl">done</i></span
          >
        </p>
      </div>
    </div>
  `,
  styles: [
    `
      :host {
        @apply flex flex-col mt-2 flex-wrap gap-2 lg:justify-between;
      }
    `,
  ],
})
export class CareDaysCalendarComponent {
  @Input()
  careDays: CareDay[][] | null = [];
}
