import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { selectUser } from '../stores/auth/auth.selectors';
import { map } from 'rxjs';
import { AuthActions } from '../stores/auth/auth.actions';
import { environment } from 'src/environments/environment';
import { selectCartEventsWithTicketsCount } from '../stores/checkout/checkout.selectors';

@Component({
  selector: 'app-top-bar',
  template: `
    <div class="flex flex-col">
      <div class="bg-white shadow-lg">
        <div class="flex h-20 flex-row items-center justify-between gap-4 px-4">
          <a routerLink="/" (click)="hideMenu()">
            <div class="flex flex-row items-center gap-2 hover:cursor-pointer">
              <img
                class="w-12"
                src="../assets/images/ferienabenteuer_bamberg_icon.svg"
                alt="Logo Ferienabenteuer Bamberg"
              />
              <h1
                class="notranslate hidden text-xl font-bold leading-none text-textDark md:block"
              >
                Ferienabenteuer<br />
                Bamberg
              </h1>
            </div>
          </a>

          <app-google-translate
            class="!text-textDark hover:cursor-pointer hover:text-primaryLight"
            *ngIf="showTranslateButton"
          />

          <div
            *ngIf="(loggedIn$ | async) && bookingEnabled"
            class="flex items-center gap-2 text-base text-textDark hover:cursor-pointer hover:text-primaryLight md:mr-2 md:hidden md:items-center md:text-2xl"
            [class.disabled]="(ticketsCount$ | async) === 0"
            routerLink="/checkout/warenkorb"
            (click)="hideMenu()"
          >
            <cart-info />
          </div>

          <nav
            [ngClass]="{ hidden: !menuOpen }"
            class="fixed left-0 top-0 mt-20 hidden h-full w-full flex-grow flex-col bg-white shadow-lg md:relative md:mt-0 md:block md:w-auto md:flex-row md:bg-inherit md:bg-none md:shadow-none"
          >
            <div
              class="border-t-custom-gray flex flex-col items-center border-t bg-white p-4 md:mt-0 md:flex md:h-full md:flex-row md:justify-between md:space-x-6 md:border-none md:bg-inherit md:p-0"
            >
              <div
                class="flex flex-grow flex-col justify-center md:flex md:h-full md:flex-row md:items-stretch md:justify-between md:space-x-6"
              >
                <div></div>
                <div
                  class="flex flex-col font-extrabold md:flex md:flex-row md:flex-wrap md:gap-4 lg:gap-6"
                >
                  <li class="flex cursor-pointer items-center">
                    <a
                      routerLink="/"
                      aria-label="Startseite"
                      (click)="hideMenu()"
                      class="my-1 hover:text-primaryLight md:my-0"
                    >
                      Start
                    </a>
                  </li>
                  <li class="flex cursor-pointer items-center">
                    <a
                      aria-label="Veranstaltungen"
                      class="my-1 hover:text-primaryLight md:my-0"
                      routerLink="/veranstaltungen"
                      (click)="hideMenu()"
                    >
                      Veranstaltungen
                    </a>
                  </li>
                  <li class="flex items-center">
                    <a
                      aria-label="Kontakt & Hilfeseite"
                      routerLink="/kontakt"
                      (click)="hideMenu()"
                      class="my-1 hover:text-primaryLight md:my-0"
                    >
                      Kontakt & Hilfe
                    </a>
                  </li>
                </div>
                <div
                  class="flex flex-col items-start md:flex md:w-72 md:flex-row md:items-center md:justify-end md:gap-2"
                >
                  <li
                    *ngIf="(loggedIn$ | async) && bookingEnabled"
                    class="hidden items-center gap-2 text-base text-textDark hover:text-primaryLight md:mr-2 md:flex md:items-center md:text-2xl"
                    [class.disabled]="(ticketsCount$ | async) === 0"
                  >
                    <div class="hidden sm:block">
                      <cart-info />
                    </div>
                    <a
                      routerLink="/checkout/warenkorb"
                      (click)="hideMenu()"
                      class="my-2 font-extrabold md:my-0 md:-ml-20 md:hidden"
                    >
                      Warenkorb
                    </a>
                  </li>

                  <li
                    *ngIf="registrationEnabled"
                    [ngClass]="{ hidden: (loggedIn$ | async) }"
                    class="flex items-center"
                  >
                    <a
                      routerLink="/registrieren"
                      (click)="hideMenu()"
                      class="my-1 hover:text-primaryLight md:my-0"
                    >
                      Registrieren
                    </a>
                  </li>

                  <div
                    class="md:flex"
                    clickOutside
                    (clickOutside)="hideAccMenu()"
                  >
                    <li
                      [ngClass]="{ hidden: !(loggedIn$ | async) }"
                      class="relative flex flex-col font-normal text-textDark md:items-center"
                    >
                      <button
                        tabindex="0"
                        class="mt-4 hidden !p-0 focus:bg-neutralWhite focus:text-textDark md:my-0 md:flex"
                        (click)="toggleAccMenu()"
                      >
                        <div
                          class="flex flex-row items-center gap-3 md:rounded-md md:border-2 md:border-textDark md:border-opacity-20"
                        >
                          <i
                            aria-hidden="true"
                            class="mi notranslate ml-1 text-2xl"
                            >account_circle</i
                          >
                          <p
                            class="text-base md:text-lg md:leading-tight lg:text-xl"
                          >
                            Mein Konto
                          </p>
                          <i class="mi notranslate hidden text-2xl md:block"
                            >arrow_drop_down</i
                          >
                        </div>
                      </button>
                      <ul
                        [ngClass]="{ 'md:hidden': !accountMenuOpen }"
                        class="bg-white md:absolute md:right-0 md:top-full md:mt-1 md:min-w-full md:rounded-md md:border-2 md:border-textDark md:border-opacity-20 md:p-2 md:shadow-lg"
                      >
                        <div
                          class="border-b-2 border-gray-500 border-opacity-20 py-3"
                        >
                          <li>
                            <a
                              aria-label="Buchungen"
                              class="account-menu-item"
                              routerLink="/buchungen"
                              (click)="hideMenu()"
                            >
                              <i class="mi notranslate">shopping_cart</i>
                              Buchungen
                            </a>
                          </li>
                          <li>
                            <a
                              aria-label="Ermäßigungen"
                              class="account-menu-item"
                              routerLink="/ermaessigungen"
                              (click)="hideMenu()"
                            >
                              <i class="mi notranslate">sell</i>Ermäßigung
                            </a>
                          </li>
                        </div>
                        <div
                          class="border-b-2 border-gray-500 border-opacity-20 py-3"
                        >
                          <li>
                            <a
                              aria-label="Persönliche Daten"
                              class="account-menu-item"
                              routerLink="/daten"
                              (click)="hideMenu()"
                            >
                              <i class="mi notranslate">person</i>Persönliche
                              Daten
                            </a>
                          </li>
                          <li>
                            <a
                              aria-label="Meine Kinder"
                              class="account-menu-item"
                              routerLink="/kinder"
                              (click)="hideMenu()"
                            >
                              <i class="mi notranslate">child_care</i>Meine
                              Kinder
                            </a>
                          </li>
                          <li>
                            <a
                              aria-label="Sicherheit"
                              class="account-menu-item"
                              routerLink="/sicherheit"
                              (click)="hideMenu()"
                            >
                              <i class="mi notranslate">lock</i>Sicherheit
                            </a>
                          </li>
                        </div>
                        <li class="li-hover" (click)="logout()">
                          <a
                            tabindex="0"
                            aria-label="Abmelden"
                            class="account-menu-item mt-3"
                          >
                            <i class="mi notranslate">logout</i>Abmelden
                          </a>
                        </li>
                      </ul>
                    </li>
                  </div>

                  <li
                    [ngClass]="{ hidden: loggedIn$ | async }"
                    class="flex items-center"
                  >
                    <a
                      class="button my-3 text-[1.5rem] md:my-0"
                      (click)="hideMenu()"
                      routerLink="/login"
                    >
                      Anmelden
                    </a>
                  </li>
                </div>
              </div>
            </div>
          </nav>

          <button
            id="menu-button"
            (click)="toggleMobileMenu()"
            class="md:hidden"
          >
            <i class="mi notranslate text-4xl">menu</i>
          </button>
        </div>
      </div>
    </div>
  `,
  styles: [
    `
      :host {
        @apply z-10;
      }
      a {
        &:not(.button) {
          @apply text-xl md:text-sm lg:text-lg text-textDark hover:text-primaryLight;
        }
      }
      /* ul ul a {
        @apply text-xl;
      } */
      button {
        @apply text-2xl md:text-base;
      }
      li {
        @apply cursor-pointer;
      }
      .account-menu-item {
        @apply flex flex-row items-center gap-2;
      }
      .disabled {
        @apply opacity-50 pointer-events-none;
      }
      /* .li-hover:hover img {
        fill: green;
      } */
    `,
  ],
})
export class TopBarComponent {
  public menuOpen = false;
  public accountMenuOpen = false;
  showTranslateButton = environment.features.translate;

  user$ = this.store.select(selectUser);
  loggedIn$ = this.user$.pipe(map((user) => user));
  ticketsCount$ = this.store.select(selectCartEventsWithTicketsCount);

  bookingEnabled = environment.features.booking;
  registrationEnabled = environment.features.registration;

  constructor(private store: Store) {}

  hideMenu() {
    this.menuOpen = false;
    this.accountMenuOpen = false;
  }
  toggleMobileMenu() {
    this.menuOpen = !this.menuOpen;
  }
  hideAccMenu() {
    this.accountMenuOpen = false;
  }
  toggleAccMenu() {
    this.accountMenuOpen = !this.accountMenuOpen;
  }
  logout() {
    this.hideMenu();
    this.store.dispatch(AuthActions.logout());
  }
}
