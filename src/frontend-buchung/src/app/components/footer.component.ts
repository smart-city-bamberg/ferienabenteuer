import { Component } from '@angular/core';

@Component({
  selector: 'app-footer',
  template: `
    <div
      class="m-2 flex flex-col items-center justify-center bg-white text-sm md:flex-row md:gap-1"
    >
      <p href="#ferienabenteuer" class="cursor-default text-textDark">
        © Ferienabenteuer
      </p>
      <span>|</span>
      <a class="link" routerLink="/impressum" aria-label="Impressumsseite"
        >Impressum</a
      >
      <span>|</span>
      <a
        class="link"
        routerLink="/datenschutz"
        aria-label="Datenschutzerklärungsseite"
        >Datenschutzerklärung</a
      >
      <span>|</span>
      <a class="link" routerLink="/agb" aria-label="AGBseite">AGB</a>
      <span>|</span>
      <a
        class="link"
        routerLink="/widerruf"
        aria-label="Widerrufsbelehrungsseite"
        >Widerrufsbelehrung</a
      >
    </div>
  `,
  styles: [
    `
      a {
        @apply text-textDark hover:text-primaryLight;
      }
      span {
        @apply hidden md:flex;
      }
    `,
  ],
})
export class FooterComponent {}
