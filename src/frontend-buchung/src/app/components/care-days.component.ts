import { Component, Input } from '@angular/core';

export interface CarePeriod {
  period: Period;
  careDays: CareDay[];
}

export interface CareDay {
  day: string;
}

export interface Holiday {
  id: string;
  name: string;
}

export interface Period {
  id: string;
  startDate: string;
  endDate: string;
  holiday: Holiday;
}

@Component({
  selector: 'app-care-days',
  template: `
    <p *ngIf="careDays.length > 1; else singleDay">
      {{ careDays[0].day | date : startFormat }}
      -
      {{ careDays[careDays.length - 1].day | date : endFormat }}
    </p>
    <ng-template #singleDay>
      <p>
        {{ careDays[0].day | date : startFormat }}
      </p>
    </ng-template>
  `,
  styles: `

  `,
})
export class CareDaysComponent {
  @Input()
  careDays: CareDay[] = [];

  @Input()
  startFormat = 'dd. MMM yyyy';

  @Input()
  endFormat = 'dd. MMM yyyy';
}
