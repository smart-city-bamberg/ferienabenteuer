import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LandingPageComponent } from './landing/landing-page.component';
import { LandingContainerComponent } from './landing/landing-container.component';
import { ConditionsComponent } from './sites/legal/conditions.component';
import { LegalNoticeComponent } from './sites/legal/legal-notice.component';
import { DataProtectionComponent } from './sites/legal/data-protection.component';
import { CancellationPolicyComponent } from './sites/legal/cancellation-policy.component';
import { SimpleLanguageComponent } from './landing/simple-language.component';

const routes: Routes = [
  {
    path: '',
    component: LandingContainerComponent,
    children: [
      {
        path: '',
        component: LandingPageComponent,
      },
      {
        path: 'impressum',
        component: LegalNoticeComponent,
      },
      {
        path: 'datenschutz',
        component: DataProtectionComponent,
      },
      {
        path: 'agb',
        component: ConditionsComponent,
      },
      {
        path: 'widerruf',
        component: CancellationPolicyComponent,
      },
      {
        path: 'leichte-sprache',
        component: SimpleLanguageComponent,
      },
    ],
  },
  {
    path: '',
    loadChildren: () =>
      import('./sites/sites.module').then((m) => m.SitesModule),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
