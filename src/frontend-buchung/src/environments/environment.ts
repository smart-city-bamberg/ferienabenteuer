export const environment = {
  features: {
    registration: true,
    booking: true,
    simpleLanguage: true,
    translate: true,
  },
  imageProxy: {
    enabled: false,
    url: 'http://localhost:8080',
    replace: 'http://localhost:3000/images',
  },
  apiUrl: 'http://localhost:3000/api/graphql',
};
