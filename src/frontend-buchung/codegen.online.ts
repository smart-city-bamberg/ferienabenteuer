import type { CodegenConfig } from '@graphql-codegen/cli';
import baseConfig from './codegen';

const config: CodegenConfig = {
  ...baseConfig,
  schema: 'http://localhost:3000/api/graphql',
};
export default config;
