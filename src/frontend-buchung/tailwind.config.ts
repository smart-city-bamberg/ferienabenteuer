import { Config } from 'tailwindcss';

export default {
  content: ['./src/**/*.{html,ts}'],
  theme: {
    extend: {
      colors: {
        background: '#b0eaf5',
        primaryLight: '#003981',
        primaryDark: '#002657',
        textDark: '#002657',
        primaryGreen: '#00a991',
        primaryOrange: '#ff9141',
        primaryRed: '#E01E1F',
        primaryYellow: '#F7BB0B',
        secondaryBlue: '#3783ca',
        neutralGrayLight: '#f9f9f9',
        neutralGrayDark: '#505050',
        secondaryGray: '#707070',
        neutralWhite: '#ffffff',
        linkTextColor: '#005BE6',
      },
      fontFamily: {
        calibri: ['Calibri'],
      },
      fontSize: {
        //-------- default werte -----------
        // xs: '0.75rem',
        // sm: '0.8rem',
        // base: '1rem',
        lg: '1.125rem',
        xl: '1.25rem',
        '2xl': '1.5rem',
        '3xl': '1.875rem',
        '4xl': '2.25rem',
        '5xl': '3rem',

        //-------- angepasste werte -----------
        xs: '0.95rem',
        sm: '1rem',
        base: '1.2rem',
        // lg: '1.325rem',
      },
    },
  },
  plugins: [],
} satisfies Config;
