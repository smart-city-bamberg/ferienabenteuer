# Ferienabenteuer - Buchungs Frontend

Dies ist das Buchungs Frontend-Projekt für "Ferienabenteuer - Ferienangebote einfach online buchen" auf Basis von Angular 17. Es lädt Daten über GraphQL aus dem CMS-Projekt.

![Ferienabenteuer Frontend](docs/Veranstaltungen.png)

---

## Lokale Entwicklung

Bevor du das Projekt lokal zu Entwicklungszwecken starten kannst, stelle sicher, dass du folgende Software installiert hast:

- Node.js: [https://nodejs.org](https://nodejs.org)
- Yarn: [https://yarnpkg.com](https://yarnpkg.com)

Folge diesen Schritten, um das Projekt lokal zu starten:

1. Führe den Befehl `yarn install` aus, um die Abhängigkeiten zu installieren.
2. Führe den Befehl `yarn run start` aus.
3. Das Frontend ist nun unter http://localhost:4200 erreichbar. Öffne einen Webbrowser und besuche diese Adresse, um das Projekt anzuzeigen.
---
## Aktualisierung des GraphQL-Schemas

Wenn sich das Schema im CMS-Projekt ändert, musst du das GraphQL-Schema im Frontend-Projekt aktualisieren. Folge diesen Schritten:

1. Führe den Befehl `yarn run generate --config codegen.online.ts` aus.
2. Das GraphQL-Schema wird aktualisiert, basierend auf den Änderungen im CMS-Projekt.

---

## Bereitstellung mit Docker

Folge diesen Schritten, um das Projekt mit Docker Compose und einer Override-Datei zu starten:

⚠️ Für den produktiven Einsatz muss die URL für das CMS in der Datei `environment.ts` angepasst werden.

1. Stelle sicher, dass Docker und Docker Compose auf deinem System installiert sind.
2. Führe den Befehl `docker-compose -f docker-compose.yml -f docker-compose.override.yml up -d` aus, um die Container zu starten.
3. Das Frontend ist nun unter http://localhost:4200 erreichbar. Öffne einen Webbrowser und besuche diese Adresse, um das Projekt anzuzeigen.
