import { NgModule } from '@angular/core';
import { APOLLO_OPTIONS, ApolloModule } from 'apollo-angular';
import { ApolloClientOptions, InMemoryCache } from '@apollo/client/core';
import { HttpLink } from 'apollo-angular/http';
import { DefaultOptions } from '@apollo/client/core/ApolloClient';
import { Store } from '@ngrx/store';
import { environment } from 'src/environments/environment';
import { extractFiles, isExtractableFile } from 'extract-files';
import { HttpHeaders } from '@angular/common/http';

const defaultOptions: DefaultOptions = {
  watchQuery: {
    fetchPolicy: 'no-cache',
    errorPolicy: 'none',
  },
  query: {
    fetchPolicy: 'no-cache',
    errorPolicy: 'none',
  },
  mutate: {
    fetchPolicy: 'no-cache',
    errorPolicy: 'none',
  },
};

const uri = environment.apiUrl;

export const createApollo = (httpLink: HttpLink): ApolloClientOptions<any> => {
  const link = httpLink.create({
    uri,
    extractFiles: (body) => extractFiles(body, '', isExtractableFile),
    withCredentials: true,
    includeExtensions: true,
    headers: new HttpHeaders({
      'apollo-require-preflight': 'true',
    }),
  });

  return {
    link,
    cache: new InMemoryCache(),
    defaultOptions,
  };
};

@NgModule({
  imports: [ApolloModule],
  providers: [
    {
      provide: APOLLO_OPTIONS,
      useFactory: createApollo,
      deps: [HttpLink, Store],
    },
  ],
})
export class GraphQLModule {}
