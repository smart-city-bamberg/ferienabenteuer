import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import {
  selectEventsWithIinvoicesSorted,
  selectSearchText,
  selectSortBy,
} from '../stores/invoices/invoices.selectors';
import { InvoicesActions } from '../stores/invoices/invoices.actions';
import { selectUserRole } from '../stores/auth/auth.selectors';
import { map } from 'rxjs';

@Component({
  selector: 'app-invoice',
  template: `
    <div class="card flex flex-grow flex-col">
      <div
        class="flex items-center rounded-lg border border-primaryDark/30 bg-white text-gray-600"
      >
        <button type="submit" class="border-none">
          <i class="mi text-[#757575]">search</i>
        </button>
        <input
          class="text-md flex-grow rounded-lg px-4 py-2 focus:outline-none"
          type="search"
          name="search"
          placeholder="Suchen"
          aria-label="Suchfeld"
          [value]="searchText$ | async"
          #searchInput
          (input)="changeSearchText(searchInput.value)"
        />
      </div>
      <div class="flex h-[0] flex-grow flex-col overflow-auto pr-1">
        <table class="table-auto border-separate border-spacing-y-2">
          <thead *ngIf="sortBy$ | async; let sortBy">
            <tr>
              <th
                *ngFor="let column of headers"
                class="sortable"
                [ngClass]="column.class"
              >
                <div (click)="setSortBy(column.name)">
                  <p>{{ column.label }}</p>
                  <i class="mi w-4">{{
                    sortBy.by !== column.name
                      ? 'unfold_more'
                      : sortBy.descending
                      ? 'arrow_downwards'
                      : 'arrow_upwards'
                  }}</i>
                </div>
              </th>

              <th class="w-28">
                <div>
                  <p>Aktion</p>
                </div>
              </th>
            </tr>
          </thead>
          <tbody>
            <tr
              *ngFor="let event of events$ | async; trackBy: identifyById"
              class="h-16"
            >
              <td>
                <p
                  class="badge"
                  [class.green]="event.isFinished"
                  [class.orange]="!event.isFinished"
                >
                  {{ event.isFinished ? 'beendet' : 'ausstehend' }}
                </p>
              </td>

              <td>
                <p>
                  {{ event.organizer.name }}
                </p>
              </td>

              <td *ngIf="event.carePeriodCommitted">
                <p class="text-sm text-gray-400">
                  {{ event.carePeriodCommitted.period.holiday.name }}
                </p>
                <p>
                  {{
                    event.carePeriodCommitted.period.startDate
                      | date : 'dd.MM.'
                  }}-{{
                    event.carePeriodCommitted.period.endDate | date : 'dd.MM.'
                  }}
                </p>
              </td>

              <td>
                <p>
                  {{ event.name }}
                </p>
              </td>

              <td>
                <p *ngIf="event.invoice">
                  {{ event.invoice.createdAt | date : 'dd.MM.yyyy' }}
                </p>
                <p *ngIf="event.invoice">
                  {{ event.invoice.createdAt | date : 'HH:mm:ss' }}
                </p>
              </td>

              <td>
                <p class="gap-2">
                  <button
                    class="p-1"
                    (click)="generatedInvoice(event.id)"
                    [disabled]="
                      !event.isFinished && !(roleIsAdministrator$ | async)
                    "
                  >
                    <i class="mi text-xl">rotate_right</i>
                  </button>
                  <button
                    class="p-1"
                    (click)="downloadInvoice(event.invoice)"
                    [disabled]="!event.invoice?.document"
                  >
                    <i class="mi text-xl">download</i>
                  </button>
                </p>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  `,
  styles: [
    `
      :host {
        @apply flex flex-grow flex-col;
      }
    `,
  ],
})
export class InvoicesComponent {
  events$ = this.store.select(selectEventsWithIinvoicesSorted);

  role$ = this.store.select(selectUserRole);

  roleIsAdministrator$ = this.role$.pipe(
    map((role) => role === 'administrator')
  );

  sortBy$ = this.store.select(selectSortBy);
  searchText$ = this.store.select(selectSearchText);

  headers = [
    {
      name: 'isFinished',
      label: 'Status',
      class: 'w-32',
    },
    {
      name: 'organizer',
      label: 'Veranstalter',
    },
    {
      name: 'carePeriodCommitted',
      label: 'Ferien',
      class: 'w-32',
    },
    {
      name: 'name',
      label: 'Name',
    },

    {
      name: 'createdAt',
      label: 'Erzeugt',
      class: 'w-24',
    },
  ];

  constructor(private store: Store) {}

  setSortBy(column: string) {
    this.store.dispatch(
      InvoicesActions.sortBy({
        column,
      })
    );
  }

  changeSearchText(searchText: string) {
    this.store.dispatch(InvoicesActions.changeSearchText({ searchText }));
  }

  generatedInvoice(eventId: string) {
    this.store.dispatch(InvoicesActions.generateInvoice({ eventId }));
  }

  downloadInvoice(invoice: { document: { url: string } | null } | undefined) {
    if (!invoice?.document) {
      return;
    }

    window.open(invoice.document.url, 'blank');
  }

  identifyById(index: number, entity: { id: string }) {
    return entity.id;
  }
}
