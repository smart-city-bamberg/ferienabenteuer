import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { selectUserRole } from '../stores/auth/auth.selectors';
import { map } from 'rxjs';
import {
  selectReservationsSorted,
  selectSearchText,
  selectSortBy,
} from '../stores/reservations/reservations.selectors';
import { ReservationsActions } from '../stores/reservations/reservations.actions';

@Component({
  selector: 'app-reservations',
  template: `
    <div class="card flex flex-grow flex-col">
      <div
        class="flex items-center rounded-lg border border-primaryDark/30 bg-white text-gray-600"
      >
        <button type="submit" class="border-none">
          <i class="mi text-[#757575]">search</i>
        </button>
        <input
          class="text-md flex-grow rounded-lg px-4 py-2 focus:outline-none"
          type="search"
          name="search"
          placeholder="Suchen"
          aria-label="Suchfeld"
          [value]="searchText$ | async"
          #searchInput
          (input)="changeSearchText(searchInput.value)"
        />
      </div>
      <div class="flex h-[0] flex-grow flex-col overflow-auto pr-1">
        <table class="table-auto border-separate border-spacing-y-2">
          <thead *ngIf="sortBy$ | async; let sortBy">
            <tr>
              <th *ngFor="let column of headers" class="sortable">
                <div (click)="setSortBy(column.name)">
                  <p>{{ column.label }}</p>
                  <i class="mi w-4">{{
                    sortBy.by !== column.name
                      ? 'unfold_more'
                      : sortBy.descending
                      ? 'arrow_downwards'
                      : 'arrow_upwards'
                  }}</i>
                </div>
              </th>
            </tr>
          </thead>
          <tbody>
            <tr *ngFor="let reservation of reservations$ | async" class="h-16">
              <td>
                <p>{{ reservation.code }}</p>
              </td>
              <td>
                <p>
                  {{ reservation.date | date : 'dd.MM.yyyy' }}
                </p>
                <p>
                  {{ reservation.date | date : 'HH:mm:ss' }}
                </p>
              </td>
              <td>
                <p>
                  {{ reservation.custodian.surname }},
                  {{ reservation.custodian.firstname }}
                </p>
              </td>
              <td>
                <p>{{ reservation.event.name }}</p>
              </td>
              <td>
                <p>
                  {{ reservation.event.organizer.name }}
                </p>
              </td>
              <td
                *ngIf="
                  reservation.event.carePeriodCommitted;
                  else desiredCarePeriod
                "
                [routerLink]="['/veranstaltungen', reservation.event.id]"
              >
                <p class="text-sm text-gray-400">
                  {{
                    reservation.event.carePeriodCommitted.period.holiday.name
                  }}
                </p>
                <p>
                  {{
                    reservation.event.carePeriodCommitted.period.startDate
                      | date : 'dd.MM.'
                  }}-{{
                    reservation.event.carePeriodCommitted.period.endDate
                      | date : 'dd.MM.'
                  }}
                </p>
              </td>
              <ng-template #desiredCarePeriod>
                <td
                  *ngIf="reservation.event.carePeriodDesired; else noPeriod"
                  [routerLink]="['/veranstaltungen', reservation.event.id]"
                >
                  <p class="text-sm text-gray-400">
                    {{
                      reservation.event.carePeriodDesired.period.holiday.name
                    }}
                  </p>
                  <p>
                    {{
                      reservation.event.carePeriodDesired.period.startDate
                        | date : 'dd.MM.'
                    }}-{{
                      reservation.event.carePeriodDesired.period.endDate
                        | date : 'dd.MM.'
                    }}
                  </p>
                </td>
              </ng-template>
              <ng-template #noPeriod
                ><td [routerLink]="['/veranstaltungen', reservation.event.id]">
                  -
                </td></ng-template
              >
              <td>
                <p class="flex justify-center">
                  {{ reservation.ticketCountParticipant }}
                </p>
              </td>
              <td>
                <p class="flex justify-center">
                  {{ reservation.ticketCountWaitinglist }}
                </p>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  `,
  styles: [
    `
      :host {
        @apply flex flex-grow;
      }
    `,
  ],
})
export class ReservationsComponent {
  reservations$ = this.store.select(selectReservationsSorted);
  role$ = this.store.select(selectUserRole);

  roleIsAdministrator$ = this.role$.pipe(
    map((role) => role === 'administrator')
  );

  sortBy$ = this.store.select(selectSortBy);
  searchText$ = this.store.select(selectSearchText);

  headers = [
    {
      name: 'code',
      label: 'Reservierung',
    },
    {
      name: 'date',
      label: 'Datum',
    },
    {
      name: 'custodian',
      label: 'Sorgeberechtiger',
    },
    {
      name: 'event',
      label: 'Veranstaltung',
    },
    {
      name: 'organizer',
      label: 'Veranstalter',
    },
    {
      name: 'carePeriodCommitted',
      label: 'Ferien',
    },
    {
      name: 'participants',
      label: 'Tickets',
    },
    {
      name: 'waitinglist',
      label: 'Warteplätze',
    },
  ];

  setSortBy(column: string) {
    this.store.dispatch(
      ReservationsActions.sortBy({
        column,
      })
    );
  }

  changeSearchText(text: string) {
    this.store.dispatch(
      ReservationsActions.changeSearchText({ searchText: text })
    );
  }

  constructor(private store: Store) {}
}
