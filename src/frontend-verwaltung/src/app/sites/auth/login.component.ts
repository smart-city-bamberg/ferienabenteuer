import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { AuthActions } from '../../stores/auth/auth.actions';
import {
  selectHasError,
  selectRoleAllowed,
  selectInvalidCredentials,
  selectIsActive,
  selectUser,
} from '../../stores/auth/auth.selectors';
import { combineLatest, delay, map } from 'rxjs';

@Component({
  template: `
    <img
      src="/assets/images/ferienabenteuer_bamberg_logo.svg"
      class="w-72 p-4"
    />
    <div class="card">
      <div class="p-16 text-2xl text-primaryLight">
        <form class="flex w-72 flex-col gap-4">
          <div class="flex flex-col">
            <label for="email">E-Mail</label>
            <input
              type="email"
              name="email"
              #emailInput
              (change)="email = emailInput.value"
            />
          </div>
          <div class="flex flex-col">
            <label>Passwort</label>
            <input
              type="password"
              #passwordInput
              (change)="password = passwordInput.value"
            />
          </div>
          <button
            type="button"
            [disabled]="isActive$ | async"
            (click)="login()"
          >
            Anmelden
          </button>
          <span
            *ngIf="isActiveDelayed$ | async"
            class="flex items-center justify-center text-sm text-primaryDark"
          >
            <svg
              class="-ml-1 mr-3 h-5 w-5 animate-spin text-white"
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
            >
              <circle
                class="opacity-25"
                cx="12"
                cy="12"
                r="10"
                stroke="currentColor"
                stroke-width="4"
              ></circle>
              <path
                class="opacity-75"
                fill="currentColor"
                d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"
              ></path>
            </svg>
            <span>Anmeldung läuft...</span>
          </span>
          <span
            class="text-sm text-secondaryRed"
            *ngIf="invalidCredentials$ | async"
          >
            Email oder Passwort nicht korrekt.
          </span>
          <span class="text-sm text-secondaryRed" *ngIf="hasError$ | async">
            Fehler beim Anmelden. Bitte versuchen Sie es später erneut.
          </span>
          <span
            class="text-sm text-secondaryRed"
            *ngIf="insufficientRole$ | async"
          >
            Sie sind nicht berechtigt sich anzumelden.
          </span>
        </form>
      </div>
    </div>
  `,
  styles: [
    `
      :host {
        @apply flex flex-col items-center gap-4;
      }
    `,
  ],
})
export class LoginComponent {
  email: string = '';
  password: string = '';

  hasError$ = this.store.select(selectHasError);
  isActive$ = this.store.select(selectIsActive);
  isActiveDelayed$ = this.isActive$.pipe(delay(300));
  invalidCredentials$ = this.store.select(selectInvalidCredentials);

  insufficientRole$ = combineLatest([
    this.store.select(selectUser),
    this.store.select(selectRoleAllowed),
    this.store.select(selectIsActive),
  ]).pipe(map(([user, allowed, isActive]) => user && !isActive && !allowed));

  constructor(private store: Store) {}

  login() {
    this.store.dispatch(
      AuthActions.login({ user: this.email, password: this.password })
    );
  }
}
