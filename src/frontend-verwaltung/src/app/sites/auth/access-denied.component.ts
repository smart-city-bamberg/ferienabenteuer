import { Component } from '@angular/core';

@Component({
  selector: 'app-access-denied',
  template: `
    <p>Sie haben keine Berechtigung diese Seite aufzurufen.</p>
    <a class="button" routerLink="/login">Login</a>
  `,
  styles: [],
})
export class AccessDeniedComponent {}
