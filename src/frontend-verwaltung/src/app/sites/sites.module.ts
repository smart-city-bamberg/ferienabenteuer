import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BookingsComponent } from './bookings.component';
import { MainComponent } from './main.component';
import { DiscountsComponent } from './discounts.component';
import { InvoicesComponent } from './invoices.component';
import { OrganizersComponent } from './organizer.component';
import { CustodiansComponent } from './custodians.component';
import { PartnersComponent } from './partners.component';
import { SeasonComponent } from './season.component';
import { EmailTemplatesComponent } from './email-templates.component';
import { ComponentsModule } from '../components/components.module';
import { RouterModule } from '@angular/router';
import { PrimeModule } from '../prime/prime.module';
import { AccessDeniedComponent } from './auth/access-denied.component';
import { ProfileComponent } from './profile.component';
import { NgrxFormsModule } from 'ngrx-forms';
import { EventsModule } from './events/events.module';
import { ReservationsComponent } from './reservations.component';
import { ContextMenuModule } from 'primeng/contextmenu';

@NgModule({
  declarations: [
    BookingsComponent,
    MainComponent,
    DiscountsComponent,
    InvoicesComponent,
    OrganizersComponent,
    CustodiansComponent,
    PartnersComponent,
    SeasonComponent,
    EmailTemplatesComponent,
    AccessDeniedComponent,
    ProfileComponent,
    ReservationsComponent,
  ],
  imports: [
    CommonModule,
    ComponentsModule,
    PrimeModule,
    RouterModule,
    NgrxFormsModule,
    EventsModule,
    ContextMenuModule,
  ],
  exports: [MainComponent],
})
export class SitesModule {}
