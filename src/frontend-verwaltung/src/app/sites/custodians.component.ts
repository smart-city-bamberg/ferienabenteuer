import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import {
  selectCustodiansSorted,
  selectEmailContent,
  selectEmailSubject,
  selectSearchText,
  selectShowEmailForm,
  selectSortBy,
} from '../stores/custodians/custodians.selectors';
import { CustodiansActions } from '../stores/custodians/custodians.actions';
import { selectUserRole } from '../stores/auth/auth.selectors';
import { map } from 'rxjs';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-custodians',
  template: `
    <div class="card flex flex-grow flex-col gap-4 py-8">
      <div class="flex flex-row gap-4">
        <button *ngIf="roleIsAdministrator$ | async" (click)="sendEmail()">
          E-Mail an alle Sorgeberechtigten
        </button>
      </div>
      <div
        class="flex items-center rounded-lg border border-primaryDark/30 bg-white text-gray-600"
      >
        <button type="submit" class="border-none">
          <i class="mi text-[#757575]">search</i>
        </button>
        <input
          class="text-md flex-grow rounded-lg px-4 py-2 focus:outline-none"
          type="search"
          name="search"
          placeholder="Suchen"
          aria-label="Suchfeld"
          [value]="searchText$ | async"
          #searchInput
          (input)="changeSearchText(searchInput.value)"
        />
      </div>
      <div class="flex h-[0] flex-grow flex-col overflow-auto pr-1">
        <table class="table-auto border-separate border-spacing-y-2">
          <thead *ngIf="sortBy$ | async; let sortBy">
            <tr>
              <th
                *ngFor="let column of headers"
                class="sortable"
                [ngClass]="column.class"
              >
                <div (click)="setSortBy(column.name)">
                  <p>{{ column.label }}</p>
                  <i class="mi w-4">{{
                    sortBy.by !== column.name
                      ? 'unfold_more'
                      : sortBy.descending
                      ? 'arrow_downwards'
                      : 'arrow_upwards'
                  }}</i>
                </div>
              </th>

              <th>
                <div class="flex justify-center">
                  <p>Aktion</p>
                </div>
              </th>
            </tr>
          </thead>
          <tbody>
            <tr class="h-16" *ngFor="let custodian of custodians$ | async">
              <td>
                <p>
                  {{ custodian.surname }},
                  {{ custodian.firstname }}
                </p>
              </td>
              <td>
                <p>
                  {{ custodian.addressStreet }}
                  {{ custodian.addressHouseNumber }}
                </p>
                <p>{{ custodian.addressZip }} {{ custodian.addressCity }}</p>
              </td>
              <td>
                <p>
                  {{ custodian.phone }}
                </p>
              </td>
              <td>
                <p>
                  {{ custodian.user?.email }}
                </p>
              </td>
              <td>
                <p *ngFor="let child of custodian.children">
                  {{ child.surname }}, {{ child.firstname }}
                </p>
              </td>
              <td>
                <div class="flex justify-center gap-3">
                  <p class="ml-4 flex gap-1" *ngIf="custodian.user">
                    <a href="mailto:{{ custodian.user.email }}">
                      <button class="p-1">
                        <i class="mi">mail</i>
                      </button>
                    </a>
                    <button
                      #menu
                      class="p-1"
                      style="background-color: transparent; border: none"
                      (click)="setContextMenuCustodianId(custodian.id)"
                    >
                      <i class="mi text-xl text-black">more_vert</i>
                    </button>

                    <p-contextMenu
                      [target]="menu"
                      [model]="contextMenuItems"
                      [triggerEvent]="'click'"
                    ></p-contextMenu>
                  </p>
                </div>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <p-dialog
        header="E-Mail an alle Sorgeberechtigten"
        [visible]="(showEmailDialog$ | async) ?? false"
        [style]="{ width: '50rem' }"
        [styleClass]="'shadow-lg border border-primaryLight '"
        [draggable]="false"
        [modal]="true"
        [resizable]="false"
        [closable]="false"
      >
        <div class="flex flex-col gap-4 text-sm">
          <div class="flex flex-col">
            <div class="flex flex-col">
              <span>Betreff:</span>
              <input
                type="text"
                [value]="emailSubject$ | async"
                #emailSubject
                (keyup)="updateEmailSubject(emailSubject.value)"
              />
            </div>
            <div class="flex flex-col">
              <span>Inhalt:</span>
              <textarea
                [value]="emailContent$ | async"
                #emailContent
                (keyup)="updateEmailContent(emailContent.value)"
                class="min-h-[10rem]"
              ></textarea>
            </div>
          </div>
          <div class="flex flex-row justify-end gap-4">
            <button class="bg-secondaryRed" (click)="processSendEmail()">
              OK
            </button>
            <button (click)="cancelSendEmail()">Abbrechen</button>
          </div>
        </div>
      </p-dialog>
    </div>
  `,
  styles: [
    `
      :host {
        @apply flex flex-grow flex-col;
      }
    `,
  ],
})
export class CustodiansComponent {
  custodians$ = this.store.select(selectCustodiansSorted);
  showEmailDialog$ = this.store.select(selectShowEmailForm);
  emailContent$ = this.store.select(selectEmailContent);
  emailSubject$ = this.store.select(selectEmailSubject);

  sortBy$ = this.store.select(selectSortBy);
  searchText$ = this.store.select(selectSearchText);

  role$ = this.store.select(selectUserRole);

  roleIsAdministrator$ = this.role$.pipe(
    map((role) => role === 'administrator')
  );

  contextMenuItems: MenuItem[] | undefined;
  contextMenuCustodianId?: string;

  headers = [
    {
      name: 'name',
      label: 'Name',
    },
    {
      name: 'address',
      label: 'Adresse',
      class: 'w-34',
    },
    {
      name: 'phone',
      label: 'Telefon',
      class: 'w-20',
    },
    {
      name: 'email',
      label: 'E-Mail',
      class: 'w-20',
    },
    {
      name: 'children',
      label: 'Kinder',
    },
  ];

  setSortBy(column: string) {
    this.store.dispatch(
      CustodiansActions.sortBy({
        column,
      })
    );
  }

  changeSearchText(text: string) {
    this.store.dispatch(
      CustodiansActions.changeSearchText({ searchText: text })
    );
  }

  updateEmailContent(content: string): void {
    this.store.dispatch(
      CustodiansActions.updateEmailContent({
        content,
      })
    );
  }

  updateEmailSubject(subject: string): void {
    this.store.dispatch(
      CustodiansActions.updateEmailSubject({
        subject,
      })
    );
  }

  sendEmail(): void {
    this.store.dispatch(CustodiansActions.sendEmailShow());
  }

  processSendEmail(): void {
    this.store.dispatch(CustodiansActions.sendEmail());
  }

  cancelSendEmail(): void {
    this.store.dispatch(CustodiansActions.sendEmailCancel());
  }

  createDsgvoInformations(): void {}

  setContextMenuCustodianId(custodianId: string) {
    this.contextMenuCustodianId = custodianId;
  }

  generateGdprInformation(custodianId: string): void {
    this.store.dispatch(
      CustodiansActions.generateGdprInformation({
        custodianId: custodianId,
      })
    );
  }

  constructor(private store: Store) {}

  ngOnInit() {
    this.contextMenuItems = [
      {
        label: 'DSGVO Auskunft erzeugen',
        command: (event) =>
          this.generateGdprInformation(this.contextMenuCustodianId as string),
      },
    ];
  }
}
