import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { selectProfileForm } from '../stores/profile/profile.selectors';
import { ProfileActions } from '../stores/profile/profile.actions';

@Component({
  selector: 'app-profile',
  template: `
    <form *ngIf="formState$ | async; let formState" [ngrxFormState]="formState">
      <div class="card flex flex-grow flex-col">
        <div class="flex flex-col gap-10 md:flex-row">
          <div class="flex flex-col md:w-1/2">
            <h2 class="">Ansprechperson</h2>
            <div class="divider mb-3"></div>
            <div class="flex flex-col md:w-1/2">
              <p class="label">Anrede</p>
              <select
                id="anrede"
                aria-label="Auswahl Anrede"
                class="px-2 py-2"
                [ngrxFormControlState]="
                  formState.controls.contactPersonSalutation
                "
              >
                <option value="mr" selected>Herr</option>
                <option value="ms">Frau</option>
              </select>
            </div>
            <div class="mt-4 flex flex-col gap-4 md:flex-row">
              <div class="flex flex-grow flex-col md:w-1/2 md:flex-grow-0">
                <p class="label">Vorname</p>
                <input
                  type="text"
                  aria-label="Eingabefeld Vorname"
                  placeholder="Max"
                  [ngrxFormControlState]="
                    formState.controls.contactPersonFirstname
                  "
                />
              </div>
              <div class="flex flex-grow flex-col md:w-1/2 md:flex-grow-0">
                <p class="label">Nachname</p>
                <input
                  type="text"
                  aria-label="Eingabefeld Nachname"
                  placeholder="Mustermann"
                  [ngrxFormControlState]="
                    formState.controls.contactPersonSurname
                  "
                />
              </div>
            </div>

            <h2 class="mt-6">Bankverbindung</h2>
            <div class="divider mb-3"></div>
            <div class="flex flex-col gap-4 md:flex-row">
              <div class="flex flex-grow flex-col md:w-1/2 md:flex-grow-0">
                <p class="label">Bank</p>
                <input
                  type="text"
                  aria-label="Eingabefeld Bank"
                  placeholder="MusterBank"
                  [ngrxFormControlState]="formState.controls.bankName"
                />
              </div>
              <div class="flex flex-grow flex-col md:w-1/2 md:flex-grow-0">
                <p class="label">Kontoinhaber</p>
                <input
                  type="text"
                  aria-label="Eingabefeld Kontoinhaber"
                  placeholder="Max Mustermann"
                  [ngrxFormControlState]="formState.controls.bankOwner"
                />
              </div>
            </div>
            <div class="mt-4 flex flex-col gap-4 md:flex-row">
              <div class="flex flex-grow flex-col md:w-1/2 md:flex-grow-0">
                <p class="label">IBAN</p>
                <input
                  type="text"
                  aria-label="Eingabefeld IBAN"
                  placeholder="DE12345678901234567800"
                  [ngrxFormControlState]="formState.controls.bankIban"
                />
              </div>
              <div class="flex flex-grow flex-col md:w-1/2 md:flex-grow-0">
                <p class="label">BIC</p>
                <input
                  type="text"
                  aria-label="Eingabefeld BIC"
                  placeholder="ABCDEFGH"
                  [ngrxFormControlState]="formState.controls.bankBic"
                />
              </div>
            </div>
          </div>

          <div class="flex flex-col md:w-1/2">
            <h2>Adresse</h2>
            <div class="divider mb-3"></div>
            <div class="flex flex-col gap-4 md:flex-row">
              <div class="flex flex-grow flex-col md:w-1/2 md:flex-grow-0">
                <p class="label">Straße & Hausnummer</p>
                <input
                  type="text"
                  aria-label="Eingabefeld Straße"
                  placeholder="Musterstraße"
                  [ngrxFormControlState]="formState.controls.addressStreet"
                />
              </div>
            </div>
            <div class="mt-4 flex flex-col gap-4 md:flex-row">
              <div class="flex flex-grow flex-col md:w-1/2 md:flex-grow-0">
                <p class="label">Postleitzahl</p>
                <input
                  type="text"
                  aria-label="Eingabefeld Postleitzahl"
                  placeholder="12345"
                  [ngrxFormControlState]="formState.controls.addressZip"
                />
              </div>
              <div class="flex flex-grow flex-col md:w-1/2 md:flex-grow-0">
                <p class="label">Stadt</p>
                <input
                  type="text"
                  aria-label="Eingabefeld Stadt"
                  placeholder="Musterstadt"
                  [ngrxFormControlState]="formState.controls.addressCity"
                />
              </div>
            </div>
            <h2 class="mt-6">Kontaktdaten</h2>
            <div class="divider mb-3"></div>
            <div class="flex flex-col gap-4 md:flex-row">
              <div class="flex flex-grow flex-col md:w-1/2 md:flex-grow-0">
                <p class="label">Telefon</p>
                <input
                  type="text"
                  aria-label="Eingabefeld Telefonummer"
                  placeholder="+49 123 12345678"
                  [ngrxFormControlState]="formState.controls.phone"
                />
              </div>
              <div class="flex flex-grow flex-col md:w-1/2 md:flex-grow-0">
                <p class="label">E-Mail</p>
                <input
                  type="email"
                  aria-label="Eingabefeld E-Mail"
                  placeholder="max.mustermann@mail.de"
                  [ngrxFormControlState]="formState.controls.email"
                />
              </div>
            </div>
          </div>
        </div>
        <div class="mt-4 flex md:justify-end">
          <button
            (click)="saveForm()"
            class="flex flex-grow justify-center p-2 md:w-56 md:flex-grow-0"
          >
            Speichern
          </button>
        </div>
      </div>
    </form>
  `,
  styles: [
    `
      :host {
        @apply flex flex-col flex-grow;
      }
      .label {
        @apply mb-1 font-bold text-sm;
      }
      input {
        @apply flex flex-grow border border-gray-300 px-4 py-2 max-h-10  rounded-lg focus:outline-none focus:border-blue-500;
      }
      h2 {
        @apply text-lg font-extrabold;
      }
    `,
  ],
})
export class ProfileComponent {
  formState$ = this.store.select(selectProfileForm);

  constructor(private store: Store) {}

  saveForm() {
    this.store.dispatch(ProfileActions.saveProfile());
  }
}
