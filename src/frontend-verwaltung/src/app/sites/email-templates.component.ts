import { Component } from '@angular/core';
import { emailTemplateNames } from '../stores/email-templates/email-templates.types';
import { Store } from '@ngrx/store';
import {
  selectFormState,
  selectSelectedEmailTemplate,
  selectTemplatesWithContent,
} from '../stores/email-templates/email-templates.selectors';
import { EmailTemplatesActions } from '../stores/email-templates/email-templates.actions';
import { map } from 'rxjs';

@Component({
  selector: 'app-setup-templates',
  template: `
    <div class="card flex flex-grow flex-row">
      <div
        class="flex w-[24rem] flex-col border-r-2 border-gray-500 border-opacity-20 py-2 pr-2"
      >
        <li
          class="m-1 flex cursor-pointer flex-row items-center gap-2 rounded-lg border border-gray-200 p-1 px-2 hover:bg-primaryLight/50"
          *ngFor="let template of templates$ | async"
          [class.selected]="template.isSelected"
          (click)="selectTemplate(template.value)"
        >
          <i
            class="mi"
            [ngClass]="{
              'text-green-400': template.hasContent,
              'text-red-400': !template.hasContent
            }"
            >circle</i
          >
          {{ template.label }}
        </li>
      </div>

      <div
        class="flex flex-grow flex-col pl-4"
        *ngIf="formState$ | async; let formState"
      >
        <form class="flex flex-grow flex-col" [ngrxFormState]="formState">
          <h2>Betreff</h2>
          <input
            class="font-mono"
            type="text"
            [ngrxFormControlState]="formState.controls.subject"
          />
          <h2 class="mt-4">Inhalt</h2>
          <textarea
            class="flex flex-grow flex-col font-mono"
            [ngrxFormControlState]="formState.controls.content"
          ></textarea>
          <button
            class="mt-4 flex h-12 w-full justify-center"
            (click)="saveTemplate()"
          >
            <span class="flex flex-row text-lg font-bold">Speichern</span>
          </button>
        </form>
      </div>
    </div>
  `,
  styles: [
    `
      :host {
        @apply flex flex-grow;
      }

      li {
        @apply list-none rounded-md flex px-2 py-1  hover:bg-primaryLight/40 hover:text-neutralWhite bg-gray-50;

        &.selected {
          @apply bg-primaryLight/20 hover:text-neutralWhite hover:bg-primaryLight/30;
        }
      }
    `,
  ],
})
export class EmailTemplatesComponent {
  selectedTemplate$ = this.store.select(selectSelectedEmailTemplate);
  templates$ = this.store.select(selectTemplatesWithContent);
  formState$ = this.store.select(selectFormState);

  constructor(private store: Store) {}

  selectTemplate(template: string) {
    this.store.dispatch(
      EmailTemplatesActions.selectEmailTemplate({ template })
    );
  }

  saveTemplate() {
    this.store.dispatch(EmailTemplatesActions.saveEmailTemplate());
  }
}
