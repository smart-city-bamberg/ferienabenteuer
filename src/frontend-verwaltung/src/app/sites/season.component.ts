import {
  selectSeasonForm,
  selectshowDeleteDialog,
} from './../stores/seasons/seasons.selectors';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Store, select } from '@ngrx/store';
import {
  selectHolidays,
  selectPeriodForm,
  selectPeriods,
} from '../stores/seasons/seasons.selectors';
import { SeasonsActions } from '../stores/seasons/seasons.actions';
import { Holiday } from '../stores/seasons/seasons.types';

@Component({
  selector: 'app-season',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div class="card flex flex-grow flex-row gap-12">
      <div class="flex w-1/2 flex-col gap-4">
        <div class="flex flex-col" *ngIf="periodForm$ | async; let periodForm">
          <p class="text-xl font-bold">Neue Ferien</p>
          <div class="divider my-2"></div>
          <div class="flex flex-col">
            <p class="label">Ferien</p>
            <select [ngrxFormControlState]="periodForm.controls.holidayId">
              <option selected [value]="undefined">-</option>
              <option
                *ngFor="
                  let holiday of holidays$ | async;
                  trackBy: identifyHoliday
                "
                [value]="holiday.id"
              >
                {{ holiday.name }}
              </option>
            </select>
          </div>
          <div class="mt-2 flex gap-6">
            <div class="flex w-1/2 flex-col">
              <p class="label">Beginn</p>
              <input
                type="date"
                [ngrxFormControlState]="periodForm.controls.startDate"
              />
            </div>
            <div class="flex w-1/2 flex-col">
              <p class="label">Ende</p>
              <input
                type="date"
                [ngrxFormControlState]="periodForm.controls.endDate"
              />
            </div>
          </div>
          <div class="divider mb-3 mt-6"></div>
          <button (click)="createPeriod()">Anlegen</button>
        </div>
        <div class="flex flex-grow flex-col">
          <p class="text-xl font-bold">Ferien</p>
          <div class="divider my-2"></div>
          <div class="flex h-[0] flex-grow flex-col overflow-auto pr-1">
            <table>
              <thead>
                <tr>
                  <th>
                    <div>
                      <p>Name</p>
                    </div>
                  </th>
                  <th>
                    <div>
                      <p>Beginn</p>
                    </div>
                  </th>
                  <th>
                    <div>
                      <p>Ende</p>
                    </div>
                  </th>
                  <th>
                    <div>
                      <p>Dauer</p>
                    </div>
                  </th>
                  <th>
                    <div class="flex justify-end">
                      <p>Aktion</p>
                    </div>
                  </th>
                </tr>
              </thead>
              <tbody>
                <tr *ngFor="let period of periods$ | async">
                  <td>
                    <p>{{ period.holiday.name }}</p>
                  </td>
                  <td>
                    <p>{{ period.startDate | date : 'dd.MM.yyyy' }}</p>
                  </td>
                  <td>
                    <p>{{ period.endDate | date : 'dd.MM.yyyy' }}</p>
                  </td>
                  <td>
                    <p>
                      {{ differenceDays(period.startDate, period.endDate) }}
                      {{
                        differenceDays(period.startDate, period.endDate) === 1
                          ? ' Tag'
                          : ' Tage'
                      }}
                    </p>
                  </td>
                  <td>
                    <p class="flex justify-end">
                      <button (click)="deletePeriod(period.id)">
                        <i class="mi">delete</i>
                      </button>
                    </p>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <form
        name="season_form"
        class="flex w-1/2 flex-col"
        *ngIf="seasonForm$ | async; let seasonForm"
        [ngrxFormState]="seasonForm"
      >
        <div class="flex flex-col">
          <p class="text-xl font-bold">Freigabe / Veröffentlichung</p>
          <div class="divider my-2"></div>
          <div class="mt-2 flex gap-6">
            <div class="flex w-1/2 flex-col">
              <p class="label">Buchungsstart: Allgemein</p>
              <input
                type="datetime-local"
                [ngrxFormControlState]="seasonForm.controls.bookingStart"
              />
            </div>
            <div class="flex w-1/2 flex-col">
              <p class="label">Buchungsstart: Partner</p>
              <input
                type="datetime-local"
                [ngrxFormControlState]="
                  seasonForm.controls.bookingStartPartners
                "
              />
            </div>
          </div>
        </div>
        <div class="mt-10 flex gap-6">
          <div class="flex w-1/2 flex-col">
            <p class="text-xl font-bold">Saisonstart</p>
            <div class="divider my-2"></div>
            <p class="label mt-2">Veröffentlichungsdatum</p>
            <input
              type="datetime-local"
              [ngrxFormControlState]="seasonForm.controls.releaseDate"
            />
          </div>
          <div class="flex w-1/2 flex-col">
            <p class="text-xl font-bold">Abrechnung</p>
            <div class="divider my-2"></div>
            <p class="label mt-2">Zuschusshöhe</p>
            <div class="flex items-center">
              <input
                type="number"
                [ngrxFormControlState]="seasonForm.controls.grantAmount"
              />
              <p class="ml-1">€</p>
            </div>
          </div>
        </div>
        <div class="flex flex-grow flex-col justify-between">
          <div class="mt-3 flex flex-col">
            <button (click)="updateSeason()">Speichern</button>
          </div>
          <div class="mt-3 flex flex-col">
            <p class="text-xl font-bold">Saison löschen</p>
            <div class="divider mb-4 mt-2"></div>
            <p class="mb-2 leading-tight">
              Es werden alle Veranstaltungen, Buchungen, Ermäßigungen,
              Reservierungen und Abrechnungen gelöscht. Eine gelöschte Saison
              kann nicht wiederhergestellt werden.
            </p>
            <button class="bg-red-600" (click)="showDeleteSeasonDialog()">
              Löschen
            </button>
          </div>
        </div>
      </form>
      <p-dialog
        [visible]="(showDeleteDialog$ | async) ?? false"
        [style]="{ width: '30rem' }"
        [styleClass]="'shadow-lg border border-primaryLight '"
        [draggable]="false"
        [modal]="true"
        [resizable]="false"
        [closable]="false"
      >
        <ng-template pTemplate="header">
          <div class="flex flex-grow justify-center text-xl font-bold">
            Ausgewählte Saison wirklich löschen?
          </div>
        </ng-template>
        <div class="flex flex-grow flex-row gap-10">
          <button class="flex w-1/2 bg-secondaryRed" (click)="deleteSeason()">
            Ja
          </button>
          <button class="flex w-1/2" (click)="cancelDeleteSeason()">
            Nein
          </button>
        </div>
      </p-dialog>
    </div>
  `,
  styles: [
    `
      :host {
        @apply flex flex-grow flex-col;
      }
      .label {
        @apply font-bold text-sm;
      }
      input {
        @apply border border-gray-300 px-3 py-2 max-h-10  rounded-lg text-neutralGrayDark focus:outline-none focus:border-blue-500;
      }
      table {
        @apply border-separate border-spacing-y-2 table-auto;
      }

      th {
        @apply bg-white py-0;
      }
    `,
  ],
})
export class SeasonComponent {
  periods$ = this.store.select(selectPeriods);
  holidays$ = this.store.select(selectHolidays);
  periodForm$ = this.store.select(selectPeriodForm);
  seasonForm$ = this.store.select(selectSeasonForm);
  showDeleteDialog$ = this.store.select(selectshowDeleteDialog);

  identifyHoliday(index: number, holiday: Holiday) {
    return holiday.id;
  }

  differenceDays(startDate: string, endDate: string): number {
    return (
      1 +
      (new Date(endDate).getTime() - new Date(startDate).getTime()) /
        (1000 * 3600 * 24)
    );
  }

  constructor(private store: Store) {}

  createPeriod() {
    this.store.dispatch(SeasonsActions.createPeriod());
  }

  deletePeriod(periodId: string) {
    if (!window.confirm('Ferien wirklich löschen?')) {
      return;
    }

    this.store.dispatch(SeasonsActions.deletePeriod({ periodId }));
  }

  updateSeason() {
    this.store.dispatch(SeasonsActions.updateSeason());
  }

  showDeleteSeasonDialog() {
    this.store.dispatch(SeasonsActions.showDeleteSeasonDialog());
  }
  deleteSeason() {
    this.store.dispatch(SeasonsActions.deleteSeason());
  }
  cancelDeleteSeason() {
    this.store.dispatch(SeasonsActions.cancelDeleteSeason());
  }
}
