import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import {
  selectDiscounts,
  selectShowRejectionForm,
  selectRejectionReasons,
  selectRejectionReason,
  selectRejectionNote,
  selectRejectionDiscount,
  selectSortBy,
  selectDiscountsSorted,
  selectSearchText,
} from '../stores/discounts/discounts.selectors';
import { DiscountsActions } from '../stores/discounts/discounts.actions';

@Component({
  selector: 'app-discounts',
  template: `
    <div class="card flex flex-grow flex-col">
      <div
        class="flex items-center rounded-lg border border-primaryDark/30 bg-white text-gray-600"
      >
        <button type="submit" class="border-none">
          <i class="mi text-[#757575]">search</i>
        </button>
        <input
          class="text-md flex-grow rounded-lg px-4 py-2 focus:outline-none"
          type="search"
          name="search"
          placeholder="Suchen"
          aria-label="Suchfeld"
          [value]="searchText$ | async"
          #searchInput
          (input)="changeSearchText(searchInput.value)"
        />
      </div>
      <div class="flex h-[0] flex-grow flex-col overflow-auto pr-1">
        <table class="table-auto border-separate border-spacing-y-2">
          <thead *ngIf="sortBy$ | async; let sortBy">
            <tr>
              <th
                *ngFor="let column of headers"
                class="sortable"
                [ngClass]="column.class"
              >
                <div (click)="setSortBy(column.name)">
                  <p>{{ column.label }}</p>
                  <i class="mi w-4">{{
                    sortBy.by !== column.name
                      ? 'unfold_more'
                      : sortBy.descending
                      ? 'arrow_downwards'
                      : 'arrow_upwards'
                  }}</i>
                </div>
              </th>

              <th>
                <div>
                  <p>Aktion</p>
                </div>
              </th>
            </tr>
          </thead>
          <tbody>
            <tr
              [class]="badgeColorRow[discount.status]"
              *ngFor="let discount of discounts$ | async"
              class="h-16"
            >
              <td>
                <p class="badge w-24" [ngClass]="badgeColors[discount.status]">
                  {{ statusDeutsch[discount.status] }}
                </p>
              </td>
              <td>
                <p>
                  {{ discount.custodian.surname }},
                  {{ discount.custodian.firstname }}
                </p>
              </td>
              <td>
                <p>
                  {{ discount.child.surname }}, {{ discount.child.firstname }}
                </p>
              </td>
              <td>
                <p>
                  {{ discount.submissionDate | date : 'dd.MM.yyyy' }}
                </p>
                <p>
                  {{ discount.submissionDate | date : 'HH:mm:ss' }}
                </p>
              </td>

              <td>
                <p>{{ discount.type.name }}</p>
              </td>
              <td>
                <p class="relative justify-start">
                  {{
                    discount.rejectionReason !== null
                      ? discount.rejectionReason.reason
                      : '-'
                  }}

                  <button
                    *ngIf="discount.rejectionNote"
                    class="peer border-none bg-transparent p-1 text-primaryLight"
                  >
                    <i class="mi outlined">description</i>
                  </button>
                  <span
                    class="absolute bottom-7 right-0 hidden w-auto items-center gap-2 rounded-md border border-gray-500 bg-white p-2 text-primaryLight shadow-md peer-hover:flex peer-focus:flex"
                    ><i class="mi outlined">description</i>
                    {{ discount.rejectionNote }}</span
                  >
                </p>
              </td>
              <td>
                <p>
                  {{ discount.reminderDate | date : 'dd.MM.yyyy' }}
                </p>
                <p>
                  {{ discount.reminderDate | date : 'HH:mm:ss' }}
                </p>
              </td>
              <td colspan="3">
                <div class="flex flex-row">
                  <p class="badge orange w-12 !rounded-r-none !border-r-0">
                    {{ discount.vouchersCount }}
                  </p>
                  <p class="badge green w-12 !rounded-none">
                    {{ discount.redeemedVouchersCountParticipant }}
                  </p>
                  <p class="badge blue w-12 !rounded-l-none !border-l-0">
                    {{ discount.redeemedVouchersCountWaitinglist }}
                  </p>
                </div>
              </td>

              <td>
                <p class="gap-2">
                  <button
                    class="p-1"
                    (click)="approveDiscount(discount.id)"
                    [disabled]="
                      discount.status !== 'pending' &&
                      discount.status !== 'overdue'
                    "
                  >
                    <i class="mi">check</i>
                  </button>
                  <button
                    class="bg-secondaryRed p-1"
                    [disabled]="
                      discount.status !== 'pending' &&
                      discount.status !== 'overdue'
                    "
                    (click)="rejectDiscount(discount.id)"
                  >
                    <i class="mi">close</i>
                  </button>
                  <a href="mailto:{{ discount.custodian.user.email }}">
                    <button class="p-1">
                      <i class="mi">mail</i>
                    </button>
                  </a>
                  <button
                    class="bg-primaryOrange p-1"
                    [disabled]="discount.status !== 'overdue'"
                    (click)="sendReminder(discount.id)"
                  >
                    <i class="mi outlined">notifications</i>
                  </button>
                </p>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <p-dialog
        header="Antrag auf Ermäßigung ablehnen"
        [visible]="(showRejectionDialog$ | async) ?? false"
        [style]="{ width: '50rem' }"
        [styleClass]="'shadow-lg border border-primaryLight '"
        [draggable]="false"
        [modal]="true"
        [resizable]="false"
        [closable]="false"
      >
        <div class="flex flex-col gap-4 text-sm">
          <div class="flex flex-col">
            <div
              class="flex flex-col"
              *ngIf="rejectionDiscount$ | async; let rejectionDiscount"
            >
              <div class="flex flex-row gap-2">
                <div class="w-40">Kind:</div>
                <div class="flex flex-row">
                  {{ rejectionDiscount.child.firstname }}
                  {{ rejectionDiscount.child.surname }}
                </div>
              </div>
              <div class="flex flex-row gap-2">
                <div class="w-40">Art:</div>
                <div class="flex flex-row">
                  {{ rejectionDiscount.type.name }}
                </div>
              </div>
              <div class="flex flex-row gap-2">
                <div class="w-40">Eingereicht:</div>
                <div class="flex flex-row">
                  {{
                    rejectionDiscount.submissionDate
                      | date : 'dd.MM.yyyy HH:mm:ss'
                  }}
                </div>
              </div>
            </div>
            <div class="flex flex-row items-center gap-2">
              <span class="w-40">Ablehnungsgrund:</span>
              <select
                [value]="rejectionReason$ | async"
                #reasonSelector
                (change)="updateRejectionReason(reasonSelector.value)"
                class="h-[2rem] p-0"
              >
                <option [value]="undefined"></option>
                <option
                  class="text-sm"
                  *ngFor="let rejectionReason of rejectionReasons$ | async"
                  [value]="rejectionReason.id"
                >
                  {{ rejectionReason.reason }}
                </option>
              </select>
            </div>
            <div class="flex flex-col">
              <span>Erklärung:</span>
              <textarea
                [value]="rejectionNote$ | async"
                #rejectionNode
                (keyup)="updateRejectionNote(rejectionNode.value)"
              ></textarea>
            </div>
          </div>
          <div class="flex flex-row justify-end gap-4">
            <button class="bg-secondaryRed" (click)="processRejectDiscount()">
              OK
            </button>
            <button (click)="cancelRejectDiscount()">Abbrechen</button>
          </div>
        </div>
      </p-dialog>
    </div>
  `,
  styles: [
    `
      :host {
        @apply flex flex-grow flex-col;
      }
    `,
  ],
})
export class DiscountsComponent {
  discounts$ = this.store.select(selectDiscountsSorted);
  rejectionReasons$ = this.store.select(selectRejectionReasons);
  showRejectionDialog$ = this.store.select(selectShowRejectionForm);
  rejectionReason$ = this.store.select(selectRejectionReason);
  rejectionNote$ = this.store.select(selectRejectionNote);
  rejectionDiscount$ = this.store.select(selectRejectionDiscount);

  sortBy$ = this.store.select(selectSortBy);
  searchText$ = this.store.select(selectSearchText);

  headers = [
    {
      name: 'status',
      label: 'Status',
    },
    {
      name: 'custodian',
      label: 'Sorgeberechtigter',
    },
    {
      name: 'child',
      label: 'Kind',
    },
    {
      name: 'submissionDate',
      label: 'Eingereicht',
    },

    {
      name: 'type',
      label: 'Art',
    },
    {
      name: 'rejectionReason',
      label: 'Ablehnungsgrund',
    },
    {
      name: 'reminderDate',
      label: 'Erinnert',
    },
    {
      name: 'vouchersCount',
      label: 'Anzahl',
      class: '!pr-0 w-4',
    },
    {
      name: 'redeemedVouchersCountParticipant',
      label: 'T',
      class: '!pr-0 w-4',
    },
    {
      name: 'redeemedVouchersCountWaitinglist',
      label: 'W',
      class: '!pr-0 w-28',
    },
  ];

  setSortBy(column: string) {
    this.store.dispatch(
      DiscountsActions.sortBy({
        column,
      })
    );
  }

  changeSearchText(text: string) {
    this.store.dispatch(
      DiscountsActions.changeSearchText({ searchText: text })
    );
  }

  statusDeutsch: { [index: string]: string } = {
    overdue: 'überfällig',
    pending: 'ausstehend',
    approved: 'gewährt',
    rejected: 'abgelehnt',
    cancelled: 'storniert',
  };
  badgeColors: { [index: string]: string } = {
    overdue: 'orange',
    pending: 'yellow',
    approved: 'green',
    rejected: 'red',
    cancelled: 'gray',
  };
  badgeColorRow: { [index: string]: string } = {
    overdue: 'red',
  };

  kindDeutsch: { [index: string]: string } = {
    income: 'Einkommen',
    richInChildren: 'Kinderreichtum',
  };
  rejectionReasonDeutsch: { [index: string]: string } = {
    invalidProof: 'Nachweis ungültig',
  };

  constructor(private store: Store) {
    this.store.dispatch(DiscountsActions.loadDiscounts());
  }

  approveDiscount(discountId: string): void {
    if (!window.confirm('Möchten Sie die Ermäßigung genehmigen?')) {
      return;
    }

    this.store.dispatch(
      DiscountsActions.approveDiscount({
        discountId,
      })
    );
  }

  rejectDiscount(discountId: string): void {
    this.store.dispatch(
      DiscountsActions.rejectDiscountFormShow({
        discountId,
      })
    );
  }

  sendReminder(discountId: string): void {
    if (!window.confirm('Möchten Sie die Erinnerung versenden?')) {
      return;
    }

    this.store.dispatch(
      DiscountsActions.sendDiscountApplicationReminder({
        discountId,
      })
    );
  }

  processRejectDiscount(): void {
    this.store.dispatch(DiscountsActions.rejectDiscount());
  }

  cancelRejectDiscount(): void {
    this.store.dispatch(DiscountsActions.rejectDiscountFormCancel());
  }

  updateRejectionNote(note: string): void {
    this.store.dispatch(
      DiscountsActions.rejectDiscountFormUpdateNote({
        note,
      })
    );
  }

  updateRejectionReason(reasonId: string): void {
    this.store.dispatch(
      DiscountsActions.rejectDiscountFormSelectReason({
        reasonId,
      })
    );
  }
}
