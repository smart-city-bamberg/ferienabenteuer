import { Component } from '@angular/core';

@Component({
  selector: 'app-main',
  template: `
    <div class="flex w-[16rem] flex-col gap-4 p-4 pr-0">
      <app-operator-icon class="h-[5rem] w-[16rem] px-4" />
      <app-season-selector class="card" />
      <app-menu-bar class="card flex flex-grow" />
    </div>

    <div class="flex flex-grow flex-col gap-4 p-4">
      <div class="flex h-[5rem] flex-row items-center justify-between">
        <app-title-breadcrumbs />
        <app-user-info />
      </div>
      <div class="flex flex-grow">
        <router-outlet></router-outlet>
      </div>
    </div>
  `,
  styles: [
    `
      :host {
        @apply flex h-screen w-screen flex-row;
      }
    `,
  ],
})
export class MainComponent {}
