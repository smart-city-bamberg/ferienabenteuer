import { helperTexts } from './helper-texts';
import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import * as eventSelectors from '../../../stores/event/event.selectors';
import { mergeMap, Observable, of, combineLatest, map } from 'rxjs';
import { EventActions } from 'src/app/stores/event/event.actions';
import * as authSelectors from 'src/app/stores/auth/auth.selectors';

@Component({
  selector: 'app-event-editor-information',
  template: `
    <div
      *ngIf="formState$ | async; let formState"
      class="flex flex-col gap-3 p-6"
    >
      <div>
        <h1>Informationen</h1>
        <hr class="border-primaryDark/50" />
      </div>
      <div class="flex flex-col">
        <label for="title">Titel</label>
        <input
          pInputText
          class="p-inputtext"
          type="text"
          name="title"
          #titleInput
          [ngrxFormControlState]="formState.controls.name"
        />
      </div>
      <div class="flex flex-col">
        <label for="title">Bild</label>
        <div
          class="flex min-h-[12rem] flex-col rounded-lg border border-dashed border-primaryDark bg-cover bg-center bg-no-repeat"
          [style.background-image]="
            (photoFile$ | async) ? 'url(' + (photoFile$ | async) + ')' : 'none'
          "
        >
          <div
            *ngIf="!(photoFile$ | async)"
            (click)="pictureFileInput.showPicker()"
            class="flex flex-grow cursor-pointer flex-col items-center justify-center py-6 hover:bg-gray-300"
          >
            <div>Datei auswählen</div>
            <div>JPG, PNG oder GIF</div>
          </div>
        </div>
        <input
          class="hidden"
          #pictureFileInput
          type="file"
          (change)="fileSelected($event)"
        />
        <div
          class="mt-4 flex flex-row justify-start gap-4"
          *ngIf="formState.controls.picture.isEnabled"
        >
          <button (click)="pictureFileInput.showPicker()">
            <i class="mi">file_open</i>
            Bild auswählen
          </button>
          <button
            class="danger"
            [disabled]="removePhotoDisabled$ | async"
            (click)="pictureFileInput.value = ''; deletePicture()"
          >
            <i class="mi">delete_forever</i>
            Bild entfernen
          </button>
        </div>
      </div>
      <div class="flex flex-col">
        <label for="title">Bildrechte</label>
        <input
          pInputText
          class="p-inputtext"
          type="text"
          name="title"
          #titleInput
          [ngrxFormControlState]="formState.controls.pictureRights"
        />
      </div>
      <div class="flex flex-col" *ngIf="roleIsAdministrator$ | async">
        <label for="title">Veranstalter</label>
        <select [ngrxFormControlState]="formState.controls.organizer">
          <option [value]="undefined">-</option>
          <option
            *ngFor="let organizer of organizers$ | async"
            [value]="organizer.id"
          >
            {{ organizer.name }}
          </option>
        </select>
      </div>
      <div class="flex flex-col">
        <label for="description">Beschreibung des Angebots</label>
        <textarea
          name="description"
          title="Beschreibung"
          #descriptionInput
          [ngrxFormControlState]="formState.controls.description"
        ></textarea>
      </div>
      <div class="flex flex-col">
        <label class="flex gap-2" for="hint"
          >Hinweis
          <div class="info-icon" [title]="helperTexts.hint">i</div></label
        >
        <textarea
          name="hint"
          title="Hinweis"
          #hintsInput
          [ngrxFormControlState]="formState.controls.hints"
        ></textarea>
      </div>
      <div class="flex flex-col">
        <label class="flex gap-2" for="hint"
          >Verpflegung
          <div class="info-icon" [title]="helperTexts.meals">i</div></label
        >
        <textarea
          name="meals"
          title="Verpflegung"
          #mealsInput
          [ngrxFormControlState]="formState.controls.meals"
        ></textarea>
      </div>
      <div class="flex flex-col">
        <label for="hint"
          >Kategorien
          <span class="font-normal text-gray-400">(höchstens 3)</span></label
        >
        <div class="mt-2 flex flex-row flex-wrap gap-2">
          <button
            class="flex cursor-pointer flex-row items-center rounded-md border p-1 px-2"
            (click)="toggleCategory(item.id)"
            [disabled]="
              formState.controls.categories.isDisabled || item.isDisabled
            "
            [ngClass]="{
              'bg-primaryLight/20 text-primaryDark':
                !item.isSelected && !formState.controls.categories.isDisabled
            }"
            *ngFor="let item of categories$ | async"
          >
            {{ item.name }}
          </button>
        </div>
      </div>
    </div>
  `,
  styles: [],
})
export class InformationComponent {
  helperTexts = helperTexts;
  formState$ = this.store.select(eventSelectors.selectFormState);

  organizers$ = this.store.select(eventSelectors.selectOrganizers);
  organizer$ = this.store.select(eventSelectors.selectEventOrganizer);

  photoFilePreview$ = this.store.select(eventSelectors.selectFile).pipe(
    mergeMap((file) =>
      file
        ? new Observable<string>((subscriber) => {
            const reader = new FileReader();
            reader.onerror = (error) => subscriber.error(error);
            reader.onabort = (error) => subscriber.error(error);
            reader.onload = (event) =>
              subscriber.next(event.target?.result as string);
            reader.onloadend = (event) => subscriber.complete();
            reader.readAsDataURL(file as Blob);
          })
        : of(undefined)
    )
  );

  photoFile$ = combineLatest([
    this.store.select(eventSelectors.selectFormPictureUrl),
    this.store.select(eventSelectors.selectRemovePicture),
    this.photoFilePreview$,
  ]).pipe(
    map(([url, removed, preview]) => preview ?? (!removed ? url : undefined))
  );

  removePhotoDisabled$ = this.photoFile$.pipe(map((photo) => !photo));

  role$ = this.store.select(authSelectors.selectUserRole);

  roleIsAdministrator$ = this.role$.pipe(
    map((role) => role === 'administrator')
  );

  roleIsOrganizer$ = this.role$.pipe(map((role) => role === 'organizer'));

  categories$ = combineLatest([
    this.store.select(eventSelectors.selectEventCategories),
    this.store.select(eventSelectors.selectCategories),
  ]).pipe(
    map(([categories, selected]) =>
      categories.map((category) => ({
        ...category,
        isSelected: selected?.find((selected) => selected.id === category.id),
        isDisabled:
          !selected?.find((selected) => selected.id === category.id) &&
          selected.length === 3,
      }))
    )
  );

  toggleCategory(id: string) {
    this.store.dispatch(EventActions.toggleCategory({ id }));
  }

  fileSelected(file: any) {
    const firstFile = file.target.files[0];

    if (!firstFile) {
      return;
    }

    this.store.dispatch(EventActions.setPhotoFile({ file: firstFile }));
  }

  constructor(private store: Store) {}

  deletePicture() {
    this.store.dispatch(EventActions.removePhoto());
  }
}
