import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { combineLatest, map } from 'rxjs';
import * as authSelectors from 'src/app/stores/auth/auth.selectors';
import { EventActions } from 'src/app/stores/event/event.actions';
import { CarePeriodType } from 'src/app/stores/event/event.reducer';
import * as eventSelectors from 'src/app/stores/event/event.selectors';
import { helperTexts } from './helper-texts';

@Component({
  selector: 'app-event-editor-care-time',
  template: `
    <div
      class="flex flex-col gap-3 p-6"
      *ngIf="formState$ | async; let formState"
    >
      <div>
        <h1>Datum</h1>
        <hr class="border-primaryDark/50" />
      </div>
      <div class="flex flex-col" *ngIf="showCarePeriodCommitted$ | async">
        <label for="title">Festgelegter Termin</label>
        <select
          title="Festgelegter Termin"
          [ngrxFormControlState]="
            formState.controls.carePeriodCommitted.controls.period.controls.id
          "
          #carePeriodCommitted
          (change)="carePeriodCommitted.blur()"
        >
          <option [value]="undefined">-</option>
          <option
            *ngFor="let period of periodsWithSelection$ | async"
            [value]="period.id"
            [ngClass]="{
              'bg-yellow-100': period.isPrimary,
              'bg-blue-200': period.isSecondary
            }"
          >
            {{ period.holidayName }} ({{ period.startDate | date : 'dd.MM.' }} -
            {{ period.endDate | date : 'dd.MM.' }})
            {{
              period.isPrimary
                ? ' / Wunschtermin'
                : period.isSecondary
                ? ' / Ausweichtermin'
                : ''
            }}
          </option>
        </select>
      </div>
      <div
        *ngIf="
          (showCarePeriodCommitted$ | async) &&
          formState.controls.carePeriodCommitted.controls.period.controls.id
            .value
        "
      >
        <label>Betreuungstage</label>
        <app-care-days
          *ngIf="periodCommittedCareDays$ | async; let periodCommittedCareDays"
          (onToggleCareDay)="toggleCareDay($event, 'carePeriodCommitted')"
          [careDays]="periodCommittedCareDays"
          [disabled]="
            formState.controls.carePeriodCommitted.controls.period.controls.id
              .isDisabled
          "
        />
      </div>
      <div
        class="flex flex-col"
        *ngIf="showCarePeriodDesiredOrAlternative$ | async"
      >
        <label for="title">Wunschtermin*</label>

        <select
          title="Wunschtermin"
          [ngrxFormControlState]="
            formState.controls.carePeriodDesired.controls.period.controls.id
          "
          #carePeriodDesired
          (change)="carePeriodDesired.blur()"
        >
          <option [value]="undefined">-</option>
          <option *ngFor="let period of periods$ | async" [value]="period.id">
            {{ period.holidayName }} ({{ period.startDate | date : 'dd.MM.' }} -
            {{ period.endDate | date : 'dd.MM.' }})
          </option>
        </select>
      </div>
      <div
        *ngIf="
          (showCarePeriodDesiredOrAlternative$ | async) &&
          formState.controls.carePeriodDesired.controls.period.controls.id.value
        "
      >
        <label>Betreuungstage</label>
        <app-care-days
          *ngIf="periodPrimaryCareDays$ | async; let periodPrimaryCareDays"
          [careDays]="periodPrimaryCareDays"
          [disabled]="
            formState.controls.carePeriodDesired.controls.period.controls.id
              .isDisabled
          "
          class="my-1 rounded-md border-2 border-white p-2"
          [ngClass]="{
            'form-invalid':
              !formState.controls.carePeriodDesired.controls.careDays.isValid
          }"
          (onToggleCareDay)="toggleCareDay($event, 'carePeriodDesired')"
        />
      </div>
      <div
        class="flex flex-col"
        *ngIf="showCarePeriodDesiredOrAlternative$ | async"
      >
        <label for="title">Ausweichtermin</label>
        <select
          [value]="undefined"
          title="Ausweichtermin"
          [ngrxFormControlState]="
            formState.controls.carePeriodAlternative.controls.period.controls.id
          "
          #carePeriodAlternative
          (change)="carePeriodAlternative.blur()"
        >
          <option [value]="undefined">-</option>
          <option *ngFor="let period of periods$ | async" [value]="period.id">
            {{ period.holidayName }} ({{ period.startDate | date : 'dd.MM.' }} -
            {{ period.endDate | date : 'dd.MM.' }})
          </option>
        </select>
      </div>
      <div
        *ngIf="
          (showCarePeriodDesiredOrAlternative$ | async) &&
          formState.controls.carePeriodAlternative.controls.period.controls.id
            .value
        "
      >
        <label>Betreuungstage</label>
        <app-care-days
          *ngIf="periodSecondaryCareDays$ | async; let periodSecondaryCareDays"
          (onToggleCareDay)="toggleCareDay($event, 'carePeriodAlternative')"
          [careDays]="periodSecondaryCareDays"
          [disabled]="
            formState.controls.carePeriodAlternative.controls.period.controls.id
              .isDisabled
          "
          class="my-1 rounded-md border-2 border-white p-2"
          [ngClass]="{
            'form-invalid':
              !formState.controls.carePeriodAlternative.controls.careDays
                .isValid
          }"
        />
      </div>

      <div class="flex flex-col">
        <label class="flex gap-2" for="title"
          >Betreuungszeit
          <div class="info-icon" [title]="helperTexts.careTime">i</div></label
        >
        <div class="flex flex-row">
          <div class="mr-6 flex w-24 flex-col">
            <input
              pInputText
              class="p-inputtext"
              type="text"
              name="title"
              #careTimeStartInput
              [ngrxFormControlState]="formState.controls.careTimeStart"
            />
            <div>Von</div>
          </div>
          <div class="flex w-24 flex-col">
            <input
              pInputText
              class="p-inputtext"
              type="text"
              name="title"
              #careTimeEndInput
              [ngrxFormControlState]="formState.controls.careTimeEnd"
            />
            <div>Bis</div>
          </div>
        </div>
      </div>
    </div>
  `,
  styles: [],
})
export class CareTimeComponent {
  helperTexts = helperTexts;

  formState$ = this.store.select(eventSelectors.selectFormState);

  organizers$ = this.store.select(eventSelectors.selectOrganizers);
  organizer$ = this.store.select(eventSelectors.selectEventOrganizer);

  role$ = this.store.select(authSelectors.selectUserRole);
  roleIsAdministrator$ = this.role$.pipe(
    map((role) => role === 'administrator')
  );

  roleIsOrganizer$ = this.role$.pipe(map((role) => role === 'organizer'));

  periods$ = this.store.select(eventSelectors.selectPeriods);

  periodsWithSelection$ = combineLatest([
    this.store.select(eventSelectors.selectPeriods),
    this.store.select(eventSelectors.selectEventPeriods),
  ]).pipe(
    map(([periods, selected]) =>
      periods.map((period) => ({
        ...period,
        isPrimary: period.id === selected.desired?.period.id,
        isSecondary: period.id === selected.alternative?.period.id,
      }))
    )
  );

  periodPrimaryCareDays$ = this.store.select(
    eventSelectors.selectEventPrimaryCareDays
  );

  periodSecondaryCareDays$ = this.store.select(
    eventSelectors.selectEventSecondaryCareDays
  );

  periodCommittedCareDays$ = this.store.select(
    eventSelectors.selectEventCommittedCareDays
  );

  showCarePeriodCommitted$ = combineLatest([
    this.roleIsAdministrator$,
    this.formState$,
  ]).pipe(
    map(
      ([isAdministrator, form]) =>
        !['new', ''].includes(form.value.status) ||
        (isAdministrator && ![''].includes(form.value.status))
    )
  );

  showCarePeriodDesiredOrAlternative$ = combineLatest([
    this.roleIsOrganizer$,
    this.formState$,
  ]).pipe(
    map(
      ([isOrganizer, form]) =>
        (isOrganizer && ['', 'new'].includes(form.value.status)) ||
        [''].includes(form.value.status)
    )
  );

  constructor(private store: Store) {}

  toggleCareDay(
    day: { date: Date; available: boolean },
    field: CarePeriodType
  ) {
    if (!day.available) {
      return;
    }

    this.store.dispatch(EventActions.toggleCareDate({ field, date: day.date }));
  }
}
