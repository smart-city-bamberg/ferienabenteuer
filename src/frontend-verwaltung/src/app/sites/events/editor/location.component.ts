import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import * as eventSelectors from 'src/app/stores/event/event.selectors';
import { helperTexts } from './helper-texts';
import { of } from 'rxjs';

@Component({
  selector: 'app-event-editor-location',
  template: `
    <div
      class="flex flex-col gap-3 p-6"
      *ngIf="formState$ | async; let formState"
    >
      <div>
        <h1>Adresse</h1>
        <hr class="border-primaryDark/50" />
      </div>
      <div class="flex flex-row">
        <div class="mr-6 flex w-32 flex-col">
          <label for="title">PLZ</label>
          <input
            pInputText
            class="p-inputtext"
            type="text"
            name="title"
            [ngrxFormControlState]="formState.controls.locationZipcode"
          />
        </div>
        <div class="flex flex-grow flex-col">
          <label for="title">Ort</label>
          <input
            pInputText
            class="p-inputtext"
            type="text"
            name="title"
            [ngrxFormControlState]="formState.controls.locationCity"
          />
        </div>
      </div>
      <div class="flex flex-col">
        <label for="title">Straße</label>
        <input
          pInputText
          class="p-inputtext"
          type="text"
          name="title"
          [ngrxFormControlState]="formState.controls.locationStreet"
        />
        <div>Straße und Hausnummer, sowie weitere Angaben</div>
      </div>
      <div>
        <h1>Teilnehmer</h1>
        <hr class="border-primaryDark/50" />
      </div>
      <div class="flex flex-row justify-between gap-8">
        <div class="flex flex-col">
          <label class="flex gap-2" for="title"
            >Alter
            <div class="info-icon" [title]="helperTexts.age">i</div></label
          >
          <div class="flex flex-row items-center gap-2">
            <input
              pInputText
              min="0"
              type="number"
              name="title"
              class="p-inputtext w-14"
              [ngrxFormControlState]="formState.controls.ageMinimum"
            />
            <span>-</span>
            <input
              pInputText
              min="0"
              type="number"
              name="title"
              class="p-inputtext w-14"
              [ngrxFormControlState]="formState.controls.ageMaximum"
            />
          </div>
        </div>
        <div class="flex flex-col">
          <label class="flex gap-2" for="title"
            >Teilnehmer
            <div class="info-icon" [title]="helperTexts.participant">
              i
            </div></label
          >
          <div class="flex flex-row gap-2">
            <div class="flex flex-col">
              <input
                pInputText
                type="number"
                min="0"
                name="title"
                class="p-inputtext w-14"
                [ngrxFormControlState]="formState.controls.participantMinimum"
              />
              <label for="title">Min.</label>
            </div>
            <div class="flex flex-col">
              <input
                pInputText
                min="0"
                type="number"
                name="title"
                class="p-inputtext w-14"
                [ngrxFormControlState]="formState.controls.participantLimit"
              />
              <label for="title">Max.</label>
            </div>
          </div>
        </div>
        <div class="flex flex-col">
          <label for="title">Warteliste</label>
          <input
            pInputText
            class="p-inputtext"
            type="number"
            name="title"
            class="p-inputtext w-full"
            [ngrxFormControlState]="formState.controls.waitingListLimit"
          />
        </div>
      </div>
      <div>
        <h1>Notfallkontakt</h1>
        <hr class="border-primaryDark/50" />
      </div>
      <div class="flex flex-col gap-2">
        <div class="flex flex-row gap-4">
          <div class="flex flex-grow flex-col">
            <label for="title">Anrede</label>
            <select
              name="title"
              [ngrxFormControlState]="
                formState.controls.contactPersonSalutation
              "
            >
              <option [value]="">-</option>
              <option [value]="'ms'">Frau</option>
              <option [value]="'mr'">Herr</option>
            </select>
          </div>
          <div class="flex flex-grow flex-col">
            <label for="title">Vorname</label>
            <input
              pInputText
              class="p-inputtext"
              type="text"
              name="title"
              [ngrxFormControlState]="formState.controls.contactPersonFirstname"
            />
          </div>
          <div class="flex flex-grow flex-col">
            <label for="title">Nachname</label>
            <input
              pInputText
              class="p-inputtext"
              type="text"
              name="title"
              [ngrxFormControlState]="formState.controls.contactPersonSurname"
            />
          </div>
        </div>
        <div class="flex flex-grow flex-col">
          <label for="title">Notfallnummer</label>
          <input
            pInputText
            class="p-inputtext"
            type="text"
            name="title"
            [ngrxFormControlState]="formState.controls.emergencyPhone"
          />
        </div>
      </div>
      <div *ngIf="isBookingVisible$ | async" class="flex flex-col gap-4">
        <div>
          <h1>Buchung</h1>
          <hr class="border-primaryDark/50" />
        </div>
        <div class="flex flex-row">
          <div class="mr-6 flex flex-grow flex-col">
            <label class="flex gap-2" for="title"
              >Beitrag
              <div class="info-icon" [title]="helperTexts.costs">i</div></label
            >
            <div class="flex items-center gap-2">
              <input
                type="number"
                min="0"
                name="title"
                [ngrxFormControlState]="formState.controls.costs"
                class="text-right"
              />
              <label for="title">€</label>
            </div>
          </div>
          <div class="flex flex-grow flex-col">
            <label for="title">Anmeldefrist</label>
            <input
              pInputText
              class="p-inputtext"
              type="date"
              name="title"
              [min]="registrationDeadlineMinimum$ | async"
              [max]="registrationDeadlineMaximum$ | async"
              [ngrxFormControlState]="formState.controls.registrationDeadline"
            />
          </div>
        </div>
      </div>
    </div>
  `,
  styles: [],
})
export class LocationComponent {
  helperTexts = helperTexts;

  formState$ = this.store.select(eventSelectors.selectFormState);

  isBookingVisible$ = this.store.select(eventSelectors.selectCanBook);

  registrationDeadlineMinimum$ = this.store.select(
    eventSelectors.selectRegistrationDeadlineMinimum
  );

  registrationDeadlineMaximum$ = this.store.select(
    eventSelectors.selectRegistrationDeadlineMaximum
  );

  constructor(private store: Store) {}
}
