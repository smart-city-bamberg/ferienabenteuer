export const helperTexts = {
  hint: 'Geben Sie hier den Eltern weitere Hinweise zur Veranstaltungen wie z.B. Beschreibung des Treffpunktes, Mitzubringendes Schuhwerk, Elternbrief etc.',
  meals:
    'Geben Sie hier den Eltern weitere Hinweise zur Verpflegung wie z.B Selbstverpflegung oder Verpflegung im Teilnehmerpreis inbegriffen und welche Mahlzeiten angeboten werden (Mittagessen, Snacks, Getränke)',
  careTime:
    'Die Betreuungszeit muss mindestens zwischen 08:00  - 15:00 Uhr liegen',
  age: 'Das Alter muss zwischen 6 und 14 Jahren liegen',
  participant:
    'Es müssen mindestens 10 und maximal 25 Teilnehmer an der Veranstaltung teilnehmen können',
  costs:
    'Der Beitrag muss zwischen 68€ und 120€ liegen (abhängig von Betreuungstagen und Verpflegung)',
};
