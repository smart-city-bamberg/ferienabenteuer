import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { combineLatest, map } from 'rxjs';
import * as eventSelectors from '../../../stores/event/event.selectors';
import { MenuItem } from 'primeng/api';
import { EventActions } from 'src/app/stores/event/event.actions';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-waiting-list',
  template: `
    <div class="flex flex-col">
      <div class="flex flex-row justify-between py-2">
        <div class="flex flex-row items-center gap-4">
          <div class="text-lg font-bold">Teilnehmerliste</div>
          <div class="badge green">
            <p class="text-base">
              {{ participantCount$ | async }} |
              {{ participantLimit$ | async }}
            </p>
          </div>
        </div>
        <div class="flex flex-row gap-4">
          <button (click)="exportParticipants()">
            <i class="mi">download</i> Teilnehmerliste als Excel
          </button>
          <a
            [href]="eventTicketsParticipantsPersonsEmails$ | async"
            class="button"
            ><i class="mi">mail</i> an alle
            {{ participantCount$ | async }} Teilnehmer</a
          >
        </div>
      </div>
      <hr />
      <table class="table-auto border-separate border-spacing-y-2">
        <thead *ngIf="sortByParticipants$ | async; let sortBy">
          <tr>
            <th
              *ngFor="let column of headersParticipants"
              class="sortable"
              [ngClass]="column.class"
            >
              <div (click)="setSortByParticipants(column.name)">
                <p>{{ column.label }}</p>
                <i class="mi w-4">{{
                  sortBy.by !== column.name
                    ? 'unfold_more'
                    : sortBy.descending
                    ? 'arrow_downwards'
                    : 'arrow_upwards'
                }}</i>
              </div>
            </th>
            <th class="w-16"></th>
          </tr>
        </thead>
        <tbody>
          <ng-container
            *ngFor="let participantTicket of participantTickets$ | async"
          >
            <tr
              *ngIf="
                participantTicket.bookedTicket &&
                  participantTicket.bookedTicket.booking;
                else reservationTicket
              "
            >
              <td class="text-center">
                <div class="ticket-number">
                  {{ participantTicket.number }}
                </div>
              </td>
              <td colspan="2">
                <div class="flex flex-row">
                  <p
                    class="badge w-28 !rounded-r-none"
                    [ngClass]="
                      paymentStatusStyles[
                        participantTicket.bookedTicket.booking.status
                      ]
                    "
                  >
                    {{
                      paymentStatusLabels[
                        participantTicket.bookedTicket.booking.status
                      ]
                    }}
                  </p>
                  <p
                    class="badge w-24 !rounded-l-none !border-l-0"
                    [ngClass]="
                      discountStatusStyles[
                        participantTicket.bookedTicket.discountVoucher?.discount
                          ?.status ?? 'none'
                      ]
                    "
                  >
                    {{
                      discountStatusLabels[
                        participantTicket.bookedTicket.discountVoucher?.discount
                          ?.status ?? 'none'
                      ]
                    }}
                  </p>
                </div>
              </td>
              <td>
                <p>
                  {{ participantTicket.bookedTicket.child.surname }},
                  {{ participantTicket.bookedTicket.child.firstname }}
                </p>
              </td>
              <td>
                <p>
                  {{ participantTicket.bookedTicket.child.custodian.surname }},
                  {{ participantTicket.bookedTicket.child.custodian.firstname }}
                </p>
              </td>
              <td>
                <p>
                  {{
                    participantTicket.bookedTicket.child.custodian.addressStreet
                  }}
                  {{
                    participantTicket.bookedTicket.child.custodian
                      .addressHouseNumber
                  }}<br />
                  {{
                    participantTicket.bookedTicket.child.custodian.addressZip
                  }}
                  {{
                    participantTicket.bookedTicket.child.custodian.addressCity
                  }}
                </p>
              </td>
              <td>
                <p>
                  {{
                    participantTicket.bookedTicket.child.dateOfBirth
                      | date : 'dd.MM.yyyy'
                  }}
                </p>
              </td>
              <td>
                <p>
                  {{ participantTicket.bookedTicket.booking.code }}
                </p>
              </td>
              <td>
                <p class="text-center">
                  {{
                    participantTicket.bookedTicket.booking.date
                      | date : 'dd.MM.yyyy'
                  }}
                </p>
                <p>
                  {{
                    participantTicket.bookedTicket.booking.date | date : 'HH:mm'
                  }}
                </p>
              </td>
              <td>
                <p>
                  {{
                    participantTicket.bookedTicket.child.custodian
                      .emergencyPhone
                  }}
                </p>
              </td>
              <td>
                <p class="justify-center">
                  {{
                    participantTicket.bookedTicket.child.assistanceRequired
                      ? 'Ja'
                      : 'Nein'
                  }}
                </p>
              </td>
              <td>
                <p class="gap-1">
                  <a
                    href="mailto:{{
                      participantTicket.bookedTicket.child.custodian.user.email
                    }}?subject=Buchung {{
                      participantTicket.bookedTicket.booking.code
                    }} | {{ eventName$ | async }}"
                    class="button"
                    ><i class="mi">mail</i></a
                  >
                  <span *ngIf="cancelTicketsEnabled">
                    <button
                      #menu
                      class="flex p-1"
                      style="background-color: transparent; border: none"
                      (click)="
                        setContextMenuParticipantTicketId(
                          participantTicket.bookedTicket.id
                        )
                      "
                    >
                      <i
                        class="mi flex text-xl text-black"
                        style="font-size:1.3rem"
                        >more_vert</i
                      >
                    </button>

                    <p-contextMenu
                      [target]="menu"
                      [model]="contextMenuParticipantItems"
                      [triggerEvent]="'click'"
                    ></p-contextMenu>
                  </span>
                </p>
              </td>
            </tr>
            <ng-template #reservationTicket>
              <tr
                *ngIf="
                  participantTicket.bookedTicket &&
                    participantTicket.bookedTicket.reservation;
                  else emptyTicket
                "
              >
                <td class="text-center">
                  <div class="ticket-number">
                    {{ participantTicket.number }}
                  </div>
                </td>
                <td colspan="2">
                  <div class="flex flex-row">
                    <p class="badge blue w-28 !rounded-r-none">reserviert</p>
                    <p
                      class="badge w-24 !rounded-l-none !border-l-0"
                      [ngClass]="
                        discountStatusStyles[
                          participantTicket.bookedTicket.discountVoucher
                            ?.discount?.status ?? 'none'
                        ]
                      "
                    >
                      {{
                        discountStatusLabels[
                          participantTicket.bookedTicket.discountVoucher
                            ?.discount?.status ?? 'none'
                        ]
                      }}
                    </p>
                  </div>
                </td>
                <td>
                  <p>
                    {{ participantTicket.bookedTicket.child.surname }},
                    {{ participantTicket.bookedTicket.child.firstname }}
                  </p>
                </td>
                <td>
                  <p>
                    {{
                      participantTicket.bookedTicket.child.custodian.surname
                    }},
                    {{
                      participantTicket.bookedTicket.child.custodian.firstname
                    }}
                  </p>
                </td>
                <td>
                  <p>
                    {{
                      participantTicket.bookedTicket.child.custodian
                        .addressStreet
                    }}
                    {{
                      participantTicket.bookedTicket.child.custodian
                        .addressHouseNumber
                    }}<br />
                    {{
                      participantTicket.bookedTicket.child.custodian.addressZip
                    }}
                    {{
                      participantTicket.bookedTicket.child.custodian.addressCity
                    }}
                  </p>
                </td>
                <td>
                  <p>
                    {{
                      participantTicket.bookedTicket.child.dateOfBirth
                        | date : 'dd.MM.yyyy'
                    }}
                  </p>
                </td>
                <td>
                  <p>
                    {{ participantTicket.bookedTicket.reservation.code }}
                  </p>
                </td>
                <td>
                  <p>
                    {{
                      participantTicket.bookedTicket.reservation.date
                        | date : 'dd.MM.yyyy'
                    }}
                  </p>
                  <p>
                    {{
                      participantTicket.bookedTicket.reservation.date
                        | date : 'HH:mm'
                    }}
                  </p>
                </td>
                <td>
                  <p>
                    {{
                      participantTicket.bookedTicket.child.custodian
                        .emergencyPhone
                    }}
                  </p>
                </td>
                <td>
                  <p class="justify-center">
                    {{
                      participantTicket.bookedTicket.child.assistanceRequired
                        ? 'Ja'
                        : 'Nein'
                    }}
                  </p>
                </td>
                <td>
                  <p class="justify-between">
                    <a
                      href="mailto:{{
                        participantTicket.bookedTicket.child.custodian.user
                          .email
                      }}?subject=Buchung {{
                        participantTicket.bookedTicket.reservation.code
                      }} | {{ eventName$ | async }}"
                      class="button"
                      ><i class="mi">mail</i></a
                    >
                    <span *ngIf="cancelTicketsEnabled">
                      <button
                        #menu
                        class="flex p-1"
                        style="background-color: transparent; border: none"
                        (click)="
                          setContextMenuParticipantTicketId(
                            participantTicket.bookedTicket.id
                          )
                        "
                      >
                        <i
                          class="mi flex text-xl text-black"
                          style="font-size:1.3rem"
                          >more_vert</i
                        >
                      </button>

                      <p-contextMenu
                        [target]="menu"
                        [model]="contextMenuParticipantItems"
                        [triggerEvent]="'click'"
                      ></p-contextMenu>
                    </span>
                  </p>
                </td>
              </tr>
              <ng-template #emptyTicket>
                <tr
                  class="empty"
                  [class.isBlocked]="participantTicket.isBlocked"
                >
                  <td class="text-center">
                    <div class="ticket-number">
                      {{ participantTicket.number }}
                    </div>
                  </td>
                  <td colspan="11">
                    <div class="flex flex-row items-center gap-4">
                      <div
                        class="flex-grow border-b border-neutralGrayDark/20"
                      ></div>
                      <p class="text-neutralGrayDark/30">
                        {{ participantTicket.isBlocked ? 'Blockiert' : 'Frei' }}
                      </p>
                      <div
                        class="flex-grow border-b border-neutralGrayDark/20"
                      ></div>
                      <div *ngIf="participantTicket.isBlocked">
                        <button
                          #menu
                          class="flex p-1"
                          style="background-color: transparent; border: none"
                          (click)="
                            setContextMenuParticipantNumber(
                              participantTicket.id
                            )
                          "
                        >
                          <i
                            class="mi flex text-xl text-black"
                            style="font-size:1.3rem"
                            >more_vert</i
                          >
                        </button>
                        <p-contextMenu
                          [target]="menu"
                          [model]="contextMenuParticipantBlockedItems"
                          [triggerEvent]="'click'"
                        ></p-contextMenu>
                      </div>
                    </div>
                  </td>
                </tr>
              </ng-template>
            </ng-template>
          </ng-container>
        </tbody>
      </table>
    </div>
    <div class="flex flex-col">
      <div class="flex flex-row justify-between py-2">
        <div class="flex flex-row items-center gap-4">
          <div class="text-lg font-bold">Warteliste</div>
          <div class="badge green">
            <p class="text-base">
              {{ waitingListCount$ | async }} |
              {{ waitingListLimit$ | async }}
            </p>
          </div>
        </div>
        <div class="flex flex-row items-center gap-4">
          <a
            [href]="eventTicketsWaitinglistPersonsEmails$ | async"
            class="button"
            ><i class="mi">mail</i> an alle
            {{ waitingListCount$ | async }} Wartenden</a
          >
        </div>
      </div>
      <hr />
      <table class="table-auto border-separate border-spacing-y-2">
        <thead *ngIf="sortByWaitinglist$ | async; let sortBy">
          <tr>
            <th
              *ngFor="let column of headersWaitinglist"
              class="sortable"
              [ngClass]="column.class"
            >
              <div (click)="setSortByWaitinglist(column.name)">
                <p>{{ column.label }}</p>
                <i class="mi w-4">{{
                  sortBy.by !== column.name
                    ? 'unfold_more'
                    : sortBy.descending
                    ? 'arrow_downwards'
                    : 'arrow_upwards'
                }}</i>
              </div>
            </th>
            <th class="w-16"></th>
          </tr>
        </thead>
        <tbody>
          <ng-container
            *ngFor="
              let waitingListTicket of waitingListTickets$ | async;
              trackBy: identifyById
            "
          >
            <tr
              *ngIf="
                waitingListTicket.bookedTicket &&
                  waitingListTicket.bookedTicket.reservation;
                else emptyTicket
              "
            >
              <td class="text-center">
                <div class="ticket-number">
                  {{ waitingListTicket.number }}
                </div>
              </td>
              <td colspan="2">
                <div class="flex flex-row">
                  <p class="badge red w-28 !rounded-r-none">wartet</p>
                  <p
                    class="badge w-24 !rounded-l-none !border-l-0"
                    [ngClass]="
                      discountStatusStyles[
                        waitingListTicket.bookedTicket.discountVoucher?.discount
                          ?.status ?? 'none'
                      ]
                    "
                  >
                    {{
                      discountStatusLabels[
                        waitingListTicket.bookedTicket.discountVoucher?.discount
                          ?.status ?? 'none'
                      ]
                    }}
                  </p>
                </div>
              </td>
              <td>
                <p>
                  {{ waitingListTicket.bookedTicket.child.surname }},
                  {{ waitingListTicket.bookedTicket.child.firstname }}
                </p>
              </td>
              <td>
                <p>
                  {{ waitingListTicket.bookedTicket.child.custodian.surname }},
                  {{ waitingListTicket.bookedTicket.child.custodian.firstname }}
                </p>
              </td>
              <td>
                <p>
                  {{
                    waitingListTicket.bookedTicket.child.custodian.addressStreet
                  }}
                  {{
                    waitingListTicket.bookedTicket.child.custodian
                      .addressHouseNumber
                  }}<br />
                  {{
                    waitingListTicket.bookedTicket.child.custodian.addressZip
                  }}
                  {{
                    waitingListTicket.bookedTicket.child.custodian.addressCity
                  }}
                </p>
              </td>
              <td>
                <p>
                  {{
                    waitingListTicket.bookedTicket.child.dateOfBirth
                      | date : 'dd.MM.yyyy'
                  }}
                </p>
              </td>
              <td>
                <p>
                  {{ waitingListTicket.bookedTicket.reservation.code }}
                </p>
              </td>
              <td>
                <p>
                  {{
                    waitingListTicket.bookedTicket.reservation.date
                      | date : 'dd.MM.yyyy'
                  }}
                </p>
                <p>
                  {{
                    waitingListTicket.bookedTicket.reservation.date
                      | date : 'HH:mm'
                  }}
                </p>
              </td>
              <td>
                <p>
                  {{
                    waitingListTicket.bookedTicket.child.custodian
                      .emergencyPhone
                  }}
                </p>
              </td>
              <td>
                <p class="justify-center">
                  {{
                    waitingListTicket.bookedTicket.child.assistanceRequired
                      ? 'Ja'
                      : 'Nein'
                  }}
                </p>
              </td>
              <td>
                <p class="justify-between">
                  <a
                    href="mailto:{{
                      waitingListTicket.bookedTicket.child.custodian.user.email
                    }}?subject=Reservierung {{
                      waitingListTicket.bookedTicket.reservation.code
                    }} | {{ eventName$ | async }}"
                    class="button"
                    ><i class="mi">mail</i></a
                  >
                  <span *ngIf="advanceWaitinglistToParticipant">
                    <button
                      #menu2
                      class="flex p-1"
                      style="background-color: transparent; border: none"
                      (click)="
                        setContextMenuWaitingListTicketId(
                          waitingListTicket.bookedTicket.id
                        )
                      "
                    >
                      <i
                        class="mi flex text-xl text-black"
                        style="font-size:1.3rem"
                        >more_vert</i
                      >
                    </button>

                    <p-contextMenu
                      [target]="menu2"
                      [model]="contextMenuWaitingListItems"
                      [triggerEvent]="'click'"
                    ></p-contextMenu>
                  </span>
                </p>
              </td>
            </tr>
            <ng-template #emptyTicket>
              <tr class="empty">
                <td class="border-r-0 text-center">
                  <div class="ticket-number">
                    {{ waitingListTicket.number }}
                  </div>
                </td>
                <td colspan="11">
                  <div class="flex flex-row items-center gap-4">
                    <div
                      class="flex-grow border-b border-neutralGrayDark/20"
                    ></div>
                    <p class="text-neutralGrayDark/30">Frei</p>
                    <div
                      class="flex-grow border-b border-neutralGrayDark/20"
                    ></div>
                  </div>
                </td>
              </tr>
            </ng-template>
          </ng-container>
        </tbody>
      </table>
    </div>
    <div class="flex flex-col">
      <div class="flex flex-row justify-between py-2">
        <div class="flex flex-row items-center gap-4">
          <div class="text-lg font-bold">Stornierte Teilnehmerplätze</div>
          <div class="badge green">
            <p class="text-base">{{ cancelledCount$ | async }}</p>
          </div>
        </div>
      </div>
      <hr />
      <table class="table-auto border-separate border-spacing-y-2">
        <thead *ngIf="sortByCancelled$ | async; let sortBy">
          <tr>
            <th
              *ngFor="let column of headersCancelled"
              class="sortable"
              [ngClass]="column.class"
            >
              <div (click)="setSortByCancelled(column.name)">
                <p>{{ column.label }}</p>
                <i class="mi w-4">{{
                  sortBy.by !== column.name
                    ? 'unfold_more'
                    : sortBy.descending
                    ? 'arrow_downwards'
                    : 'arrow_upwards'
                }}</i>
              </div>
            </th>
          </tr>
        </thead>
        <tbody>
          <ng-container
            *ngFor="
              let cancelledTicket of cancelledTickets$ | async;
              trackBy: identifyById
            "
          >
            <tr>
              <td colspan="2">
                <div class="flex flex-row">
                  <p
                    class="badge w-28 !rounded-r-none"
                    [ngClass]="
                      paymentStatusStyles[
                        cancelledTicket.booking?.status ?? 'none'
                      ]
                    "
                  >
                    {{
                      paymentStatusLabels[
                        cancelledTicket.booking?.status ?? 'none'
                      ]
                    }}
                  </p>
                  <p
                    class="badge w-28 !rounded-l-none !border-l-0"
                    [ngClass]="
                      discountStatusStyles[
                        cancelledTicket.discountVoucher?.discount?.status ??
                          'none'
                      ]
                    "
                  >
                    {{
                      discountStatusLabels[
                        cancelledTicket.discountVoucher?.discount?.status ??
                          'none'
                      ]
                    }}
                  </p>
                </div>
              </td>
              <td>
                <p>
                  {{ cancelledTicket.child.surname }},
                  {{ cancelledTicket.child.firstname }}
                </p>
              </td>
              <td>
                <p>
                  {{ cancelledTicket.child.custodian.surname }},
                  {{ cancelledTicket.child.custodian.firstname }}
                </p>
              </td>
              <td>
                <p>
                  {{ cancelledTicket.child.custodian.addressStreet }}
                  {{ cancelledTicket.child.custodian.addressHouseNumber }}<br />
                  {{ cancelledTicket.child.custodian.addressZip }}
                  {{ cancelledTicket.child.custodian.addressCity }}
                </p>
              </td>
              <td>
                <p>
                  {{ cancelledTicket.child.dateOfBirth | date : 'dd.MM.yyyy' }}
                </p>
              </td>
              <td>
                <p>
                  {{ cancelledTicket.booking?.code }}
                </p>
              </td>
              <td>
                <p>
                  {{ cancelledTicket.booking?.date | date : 'dd.MM.yyyy' }}
                </p>
                <p>
                  {{ cancelledTicket.booking?.date | date : 'HH:mm' }}
                </p>
              </td>
              <td>
                <p class="justify-end pr-1">{{ cancelledTicket.price }} €</p>
              </td>
              <td>
                <p>
                  {{ cancelledTicket.child.custodian.emergencyPhone }}
                </p>
              </td>
            </tr>
          </ng-container>
        </tbody>
      </table>
    </div>
  `,
  styles: [
    `
      :host {
        @apply flex flex-col py-4 gap-12;
      }

      table tbody tr td {
        & * {
          @apply text-sm;
        }

        @apply py-2;
      }

      table tbody tr {
        @apply h-16;

        &.empty {
          @apply bg-gray-50 hover:bg-gray-300;

          &.isBlocked {
            @apply bg-red-100/50 hover:bg-red-100;
          }

          &:hover {
            @apply cursor-not-allowed;
          }

          & td {
            @apply border-neutralGrayDark/10;

            &:only-child {
              @apply border;
            }
          }
        }
      }

      .ticket-number {
        @apply text-base flex h-[1.5rem] w-[1.5rem] flex-col items-center justify-center rounded-[10rem] border border-neutralGrayDark/20;
      }
    `,
  ],
})
export class WaitingListComponent {
  contextMenuParticipantItems?: MenuItem[];
  contextMenuParticipantBlockedItems?: MenuItem[];
  contextMenuTicketId?: string;
  contextMenuEventTicketId?: string;
  contextMenuWaitingListItems?: MenuItem[];
  contextMenuWaitingListTicketId!: string;

  cancelTicketsEnabled = environment.features.cancelTickets;
  advanceWaitinglistToParticipant =
    environment.features.advanceWaitinglistToParticipant;

  paymentStatusStyles: { [index: string]: string } = {
    overdue: 'red',
    pending: 'orange',
    payed: 'green',
  };

  paymentStatusLabels: { [index: string]: string } = {
    payed: 'bezahlt',
    pending: 'ausstehend',
    overdue: 'überfällig',
  };

  discountStatusStyles: { [index: string]: string } = {
    none: 'gray',
    overdue: 'orange',
    pending: 'yellow',
    approved: 'green',
    rejected: 'red',
    cancelled: 'gray',
  };

  discountStatusLabels: { [index: string]: string } = {
    none: 'keine',
    approved: 'gewährt',
    pending: 'ausstehend',
    overdue: 'überfällig',
  };

  headersParticipants = [
    {
      name: 'number',
      label: 'Platz',
      class: 'w-4',
    },
    {
      name: 'status',
      label: 'Status',
      class: 'w-28',
    },
    {
      name: 'discountStatus',
      label: 'Ermäßigung',
      class: 'w-28',
    },
    {
      name: 'child',
      label: 'Kind',
      class: '',
    },
    {
      name: 'custodian',
      label: 'Sorgeberechtigter',
      class: '',
    },
    {
      name: 'address',
      label: 'Adresse',
      class: '',
    },
    {
      name: 'dateOfBirth',
      label: 'Geburtstag',
      class: 'w-28',
    },
    {
      name: 'booking',
      label: 'Buchung/Reservierung',
      class: 'w-20',
    },
    {
      name: 'bookingDate',
      label: 'Buchungsdatum',
      class: 'w-28',
    },
    {
      name: 'phone',
      label: 'Telefon',
      class: 'w-32',
    },
    {
      name: 'assistanceRequired',
      label: 'Assistenz',
      class: 'w-20',
    },
  ];

  headersWaitinglist = [
    {
      name: 'number',
      label: 'Platz',
      class: 'w-4',
    },
    {
      name: 'status',
      label: 'Status',
      class: 'w-28',
    },
    {
      name: 'discountStatus',
      label: 'Ermäßigung',
      class: 'w-28',
    },
    {
      name: 'child',
      label: 'Kind',
      class: '',
    },
    {
      name: 'custodian',
      label: 'Sorgeberechtigter',
      class: '',
    },
    {
      name: 'address',
      label: 'Adresse',
      class: '',
    },
    {
      name: 'dateOfBirth',
      label: 'Geburtstag',
      class: 'w-28',
    },
    {
      name: 'reservation',
      label: 'Reservierung',
      class: 'w-20',
    },
    {
      name: 'reservationDate',
      label: 'Reservierungssdatum',
      class: 'w-28',
    },
    {
      name: 'phone',
      label: 'Telefon',
      class: 'w-32',
    },
    {
      name: 'assistanceRequired',
      label: 'Assistenz',
      class: 'w-20',
    },
  ];

  headersCancelled = [
    {
      name: 'status',
      label: 'Status',
      class: 'w-28',
    },
    {
      name: 'discountStatus',
      label: 'Ermäßigung',
      class: 'w-28',
    },
    {
      name: 'child',
      label: 'Kind',
      class: '',
    },
    {
      name: 'custodian',
      label: 'Sorgeberechtigter',
      class: '',
    },
    {
      name: 'address',
      label: 'Adresse',
      class: '',
    },
    {
      name: 'dateOfBirth',
      label: 'Geburtstag',
      class: 'w-28',
    },
    {
      name: 'booking',
      label: 'Buchung',
      class: 'w-20',
    },
    {
      name: 'bookingDate',
      label: 'Buchungssdatum',
      class: 'w-28',
    },
    {
      name: 'price',
      label: 'Preis',
      class: 'w-12',
    },
    {
      name: 'phone',
      label: 'Telefon',
      class: 'w-32',
    },
  ];

  eventName$ = this.store
    .select(eventSelectors.selectFormValue)
    .pipe(map((value) => value.name));

  participantTickets$ = this.store.select(
    eventSelectors.selectEventTicketsParticipantsSorted
  );

  waitingListTickets$ = this.store.select(
    eventSelectors.selectEventTicketsWaitinglistSorted
  );

  cancelledTickets$ = this.store.select(
    eventSelectors.selectCancelledTicketsSorted
  );

  eventTicketsParticipantsPersonsEmails$ = combineLatest([
    this.store.select(eventSelectors.selectEventTicketsParticipantsPersons),
    this.store.select(eventSelectors.selectFormValue),
  ]).pipe(
    map(([custodians, form]) => custodiansToMailLink(custodians, form.name))
  );

  eventTicketsWaitinglistPersonsEmails$ = combineLatest([
    this.store.select(eventSelectors.selectEventTicketsWaitinglistPersons),
    this.store.select(eventSelectors.selectFormValue),
  ]).pipe(
    map(([custodians, form]) => custodiansToMailLink(custodians, form.name))
  );

  participantCount$ = this.store.select(
    eventSelectors.selectEventParticipantCount
  );

  waitingListCount$ = this.store.select(
    eventSelectors.selectEventWaitingListCount
  );

  cancelledCount$ = this.store.select(
    eventSelectors.selectEventCancelledTicketCount
  );

  participantLimit$ = this.store.select(
    eventSelectors.selectEventParticipantLimit
  );

  waitingListLimit$ = this.store.select(
    eventSelectors.selectEventWaitingListLimit
  );

  participantFreeSlotsCount$ = this.store.select(
    eventSelectors.selectEventParticipantFreeSlots
  );

  waitingListFreeSlotsCount$ = this.store.select(
    eventSelectors.selectEventWaitingListFreeSlots
  );

  sortByParticipants$ = this.store.select(
    eventSelectors.selectSortByParticipants
  );
  sortByWaitinglist$ = this.store.select(
    eventSelectors.selectSortByWaitinglist
  );
  sortByCancelled$ = this.store.select(eventSelectors.selectSortByCancelled);

  exportParticipants() {
    this.store.dispatch(EventActions.exportEventParticipants());
  }

  showConfirmationDialog(message: string): boolean {
    return window.confirm(message);
  }
  onCancelTicket(ticketId: string): void {
    const confirmationMessage = 'Möchten Sie das Ticket stornieren?';
    if (this.showConfirmationDialog(confirmationMessage)) {
      this.store.dispatch(
        EventActions.cancelTicket({
          ticketId,
        })
      );
    }
  }

  onReleaseTicket(eventTicketId: string): void {
    const confirmationMessage = 'Möchten Sie den Platz freigeben?';
    if (this.showConfirmationDialog(confirmationMessage)) {
      this.store.dispatch(
        EventActions.unblockEventTicket({
          eventTicketId,
        })
      );
    }
  }

  onMoveUpParticipantList(ticketId: string): void {
    const confirmationMessage = 'Möchten Sie den Teilnehmer nachrücken?';
    if (this.showConfirmationDialog(confirmationMessage)) {
      this.store.dispatch(
        EventActions.moveWaitingListTicketToParticipantList({
          ticketId,
        })
      );
    }
  }

  onCancelWaitingPlace(ticketId: string): void {
    const confirmationMessage = 'Möchten Sie den Warteplatz stornieren?';
    if (this.showConfirmationDialog(confirmationMessage)) {
      this.store.dispatch(
        EventActions.cancelTicket({
          ticketId,
        })
      );
    }
  }

  setSortByParticipants(column: string) {
    this.store.dispatch(
      EventActions.participantsSort({
        column,
      })
    );
  }

  setSortByWaitinglist(column: string) {
    this.store.dispatch(
      EventActions.waitinglistSort({
        column,
      })
    );
  }

  setSortByCancelled(column: string) {
    this.store.dispatch(
      EventActions.cancelledSort({
        column,
      })
    );
  }

  setContextMenuParticipantTicketId(ticketId: string) {
    this.contextMenuTicketId = ticketId;
  }
  setContextMenuParticipantNumber(eventTicketId: string) {
    this.contextMenuEventTicketId = eventTicketId;
  }

  setContextMenuWaitingListTicketId(reservationId: string) {
    this.contextMenuWaitingListTicketId = reservationId;
  }

  identifyById(index: number, entity: { id: string }) {
    return entity.id;
  }

  constructor(private store: Store) {}

  ngOnInit() {
    this.contextMenuParticipantItems = [
      {
        label: 'Ticket stornieren',
        command: (event) =>
          this.onCancelTicket(this.contextMenuTicketId as string),
      },
    ];
    this.contextMenuParticipantBlockedItems = [
      {
        label: 'Platz freigeben',
        command: (event) =>
          this.onReleaseTicket(this.contextMenuEventTicketId as string),
      },
    ];

    this.contextMenuWaitingListItems = [
      {
        label: 'Auf Teilnehmerliste nachrücken',
        command: (event) =>
          this.onMoveUpParticipantList(this.contextMenuWaitingListTicketId),
      },
      {
        label: 'Stornieren',
        command: (event) =>
          this.onCancelWaitingPlace(this.contextMenuWaitingListTicketId),
      },
    ];
  }
}

function custodiansToMailLink(
  custodians: { firstname: string; surname: string; user: { email: string } }[],
  name: string
) {
  const addresses = custodians
    .map(
      (custodian) =>
        `${custodian?.firstname} ${custodian?.surname}<${custodian?.user.email}>`
    )
    .reduce(
      (all, next) => (all.includes(next) ? all : [...all, next]),
      new Array<string>()
    )
    .join(';');

  const subject = `Ferienabenteuer Veranstaltung: ${name}`;

  return `mailto:?bcc=${addresses}&subject=${window.encodeURI(subject)}`;
}
