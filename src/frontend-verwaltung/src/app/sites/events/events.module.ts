import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InformationComponent } from './editor/information.component';
import { EventEditorComponent } from './editor.component';
import { NgrxFormsModule } from 'ngrx-forms';
import { EventListComponent } from './list.component';
import { ComponentsModule } from 'src/app/components/components.module';
import { RouterModule } from '@angular/router';
import { PrimeModule } from 'src/app/prime/prime.module';
import { StoresModule } from 'src/app/stores/stores.module';
import { BrowserModule } from '@angular/platform-browser';
import { CareTimeComponent } from './editor/care-time.component';
import { WaitingListComponent } from './editor/waiting-list.component';
import { LocationComponent } from './editor/location.component';
import { ContextMenuModule } from 'primeng/contextmenu';

@NgModule({
  declarations: [
    InformationComponent,
    EventEditorComponent,
    EventListComponent,
    CareTimeComponent,
    WaitingListComponent,
    LocationComponent,
  ],
  exports: [InformationComponent, EventEditorComponent, EventListComponent],
  imports: [
    BrowserModule,
    CommonModule,
    NgrxFormsModule,
    ComponentsModule,
    RouterModule,
    PrimeModule,
    StoresModule,
    ContextMenuModule,
  ],
})
export class EventsModule {}
