import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import {
  selectDuplication,
  selectEvents,
  selectEventsSorted,
  selectSearchText,
  selectShowDuplicationForm,
  selectSortBy,
} from '../../stores/events/events.selectors';
import { selectUserRole } from 'src/app/stores/auth/auth.selectors';
import { map } from 'rxjs';
import { EventsActions } from 'src/app/stores/events/events.actions';
import { selectSeasons } from 'src/app/stores/seasons/seasons.selectors';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-events',
  template: `
    <div class="card flex flex-grow flex-col gap-4">
      <div class="flex flex-row gap-4">
        <button routerLink="/veranstaltungen/anlegen">
          Veranstaltung anlegen
        </button>
        <button *ngIf="roleIsAdministrator$ | async" (click)="exportEvents()">
          Veranstaltungen exportieren
        </button>
      </div>
      <div
        class="flex items-center rounded-lg border border-primaryDark/30 bg-white text-gray-600"
      >
        <button type="submit" class="border-none">
          <i class="mi text-[#757575]">search</i>
        </button>
        <input
          class="text-md flex-grow rounded-lg px-4 py-2 focus:outline-none"
          type="search"
          name="search"
          placeholder="Suchen"
          aria-label="Suchfeld"
          [value]="searchText$ | async"
          #searchInput
          (input)="changeSearchText(searchInput.value)"
        />
      </div>
      <div class="flex h-[0] flex-grow flex-col overflow-auto pr-1">
        <table>
          <thead *ngIf="sortBy$ | async; let sortBy">
            <tr>
              <th
                *ngFor="let column of headers"
                class="sortable"
                [ngClass]="column.class"
              >
                <div (click)="setSortBy(column.name)">
                  <p>{{ column.label }}</p>
                  <i class="mi w-4">{{
                    sortBy.by !== column.name
                      ? 'unfold_more'
                      : sortBy.descending
                      ? 'arrow_downwards'
                      : 'arrow_upwards'
                  }}</i>
                </div>
              </th>

              <th>
                <div>
                  <p>Aktion</p>
                </div>
              </th>
            </tr>
          </thead>
          <tbody>
            <tr
              class="h-16"
              [class]="badgeColors[event.status]"
              *ngFor="let event of events$ | async"
            >
              <td [routerLink]="['/veranstaltungen', event.id]">
                <p
                  class="badge w-28 justify-center"
                  [ngClass]="badgeColors[event.status]"
                >
                  {{ statusDeutsch[event.status] }}
                </p>
              </td>
              <td [routerLink]="['/veranstaltungen', event.id]">
                <p class="justify-center">
                  {{ (event.progress * 100) / 25 | number : '0.0-0' }}%
                </p>
              </td>
              <td
                *ngIf="event.carePeriodCommitted; else desiredCarePeriod"
                [routerLink]="['/veranstaltungen', event.id]"
              >
                <p class="text-sm text-gray-400">
                  {{ event.carePeriodCommitted.period.holiday.name }}
                </p>
                <p>
                  {{
                    event.carePeriodCommitted.period.startDate
                      | date : 'dd.MM.'
                  }}-{{
                    event.carePeriodCommitted.period.endDate | date : 'dd.MM.'
                  }}
                </p>
              </td>
              <ng-template #desiredCarePeriod>
                <td
                  *ngIf="event.carePeriodDesired; else noPeriod"
                  [routerLink]="['/veranstaltungen', event.id]"
                >
                  <p class="text-sm text-gray-400">
                    {{ event.carePeriodDesired.period.holiday.name }}
                  </p>
                  <p>
                    {{
                      event.carePeriodDesired.period.startDate
                        | date : 'dd.MM.'
                    }}-{{
                      event.carePeriodDesired.period.endDate | date : 'dd.MM.'
                    }}
                  </p>
                </td>
              </ng-template>
              <ng-template #noPeriod
                ><td [routerLink]="['/veranstaltungen', event.id]">
                  -
                </td></ng-template
              >

              <td [routerLink]="['/veranstaltungen', event.id]">
                <p>
                  {{ event.name }}
                </p>
              </td>
              <td [routerLink]="['/veranstaltungen', event.id]">
                <p>{{ event.organizer.name }}</p>
              </td>
              <td [routerLink]="['/veranstaltungen', event.id]">
                <p class="justify-center">
                  {{ event.ageMinimum }} - {{ event.ageMaximum }} J.
                </p>
              </td>
              <td [routerLink]="['/veranstaltungen', event.id]">
                <p class="">{{ event.locationCity }}</p>
              </td>
              <td [routerLink]="['/veranstaltungen', event.id]">
                <p class="justify-end">{{ event.costs }} €</p>
              </td>
              <td
                [routerLink]="['/veranstaltungen', event.id]"
                class="text-center"
                align="center"
                colspan="2"
              >
                <div
                  class="flex flex-row"
                  *ngIf="event.participantLimit !== null"
                >
                  <p class="badge green w-20 !rounded-r-none">
                    {{ event.bookedTicketCountParticipant }}
                    | {{ event.participantLimit }}
                  </p>
                  <p
                    class="badge w-12 !rounded-l-none !border-l-0"
                    [ngClass]="{
                      gray: event.participantPercent === 0.0,
                      yellow:
                        event.participantPercent > 0.0 &&
                        event.participantPercent <= 50,
                      orange:
                        event.participantPercent > 50 &&
                        event.participantPercent <= 90,
                      red: event.participantPercent > 90
                    }"
                  >
                    {{ event.participantPercent | number : '0.0-0' }}%
                  </p>
                </div>
                <p *ngIf="event.participantLimit === null">-</p>
              </td>
              <td
                [routerLink]="['/veranstaltungen', event.id]"
                align="center"
                colspan="2"
              >
                <div
                  class="flex flex-row"
                  *ngIf="
                    event.waitingListLimit !== null &&
                    event.waitingListLimit > 0
                  "
                >
                  <p class="badge blue w-20 !rounded-r-none">
                    {{ event.bookedTicketCountWaitinglist }}
                    | {{ event.waitingListLimit }}
                  </p>
                  <p
                    class="badge w-12 !rounded-l-none !border-l-0"
                    [ngClass]="{
                      gray: event.waitinglistPercent === 0.0,
                      yellow:
                        event.waitinglistPercent > 0.0 &&
                        event.waitinglistPercent <= 50,
                      orange:
                        event.waitinglistPercent > 50 &&
                        event.waitinglistPercent <= 90,
                      red: event.waitinglistPercent > 90
                    }"
                  >
                    {{ event.waitinglistPercent | number : '0.0-0' }}%
                  </p>
                </div>
                <p
                  *ngIf="
                    event.waitingListLimit === null ||
                    event.waitingListLimit === 0
                  "
                ></p>
              </td>
              <td>
                <p class="justify-center">
                  <button
                    #menu
                    class="p-1"
                    style="background-color: transparent; border: none"
                    (click)="setContextMenuEventIdName(event.id, event.name)"
                  >
                    <i class="mi text-xl text-black">more_vert</i>
                  </button>

                  <p-contextMenu
                    [target]="menu"
                    [model]="contextMenuItems"
                    [triggerEvent]="'click'"
                  ></p-contextMenu>
                </p>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <p-dialog
        [visible]="(showDuplicationDialog$ | async) ?? false"
        [style]="{ width: '50rem' }"
        [styleClass]="'shadow-lg border border-primaryLight'"
        [draggable]="false"
        [modal]="true"
        [resizable]="false"
        [closable]="false"
      >
        <ng-template pTemplate="header">
          <div class="flex flex-col">
            <span class="text-lg font-bold">Veranstaltung duplizieren</span>
            <span>{{ contextMenuEventName }}</span>
          </div>
        </ng-template>
        <div
          class="flex flex-col gap-4 text-sm"
          *ngIf="duplication$ | async; let duplication"
        >
          <div class="flex flex-row gap-8">
            <div class="flex flex-row items-center gap-2">
              <span>Saison:</span>
              <select
                [value]="duplication.seasonId"
                #seasonSelector
                (change)="updateDuplicateEventSeason(seasonSelector.value)"
                class="h-[2rem] w-[5rem] p-0 pl-1"
              >
                <option [value]="undefined"></option>
                <option
                  class="text-sm"
                  *ngFor="let seasons of seasons$ | async"
                  [value]="seasons.id"
                >
                  {{ seasons.year }}
                </option>
              </select>
            </div>
            <div class="flex flex-row items-center gap-2">
              <span>Anzahl Kopien:</span>
              <input
                class="h-[2.1rem]"
                type="number"
                min="1"
                max="10"
                [value]="duplication.amount"
                #amountNode
                (change)="updateDuplicateEventAmount(amountNode.value)"
              />
            </div>
          </div>
          <div class="flex flex-row justify-end gap-4">
            <button class="bg-secondaryRed" (click)="processDuplicateEvent()">
              OK
            </button>
            <button (click)="cancelDuplicateEvent()">Abbrechen</button>
          </div>
        </div>
      </p-dialog>
    </div>
  `,
  styles: [
    `
      :host {
        @apply flex flex-grow flex-col gap-4;
      }

      table {
        @apply border-separate border-spacing-y-2 table-auto;
      }

      th {
        // position: sticky;
        // top: 0;
        @apply bg-white py-4;
      }
    `,
  ],
})
export class EventListComponent implements OnInit {
  events$ = this.store.select(selectEventsSorted);
  showDuplicationDialog$ = this.store.select(selectShowDuplicationForm);
  role$ = this.store.select(selectUserRole);
  seasons$ = this.store.select(selectSeasons);
  duplication$ = this.store.select(selectDuplication);

  contextMenuItems: MenuItem[] | undefined;
  contextMenuEventId?: string;
  contextMenuEventName?: string;

  roleIsAdministrator$ = this.role$.pipe(
    map((role) => role === 'administrator')
  );

  sortBy$ = this.store.select(selectSortBy);
  searchText$ = this.store.select(selectSearchText);

  headers = [
    {
      name: 'status',
      label: 'Status',
      class: 'w-28',
    },
    {
      name: 'progress',
      label: 'Bearbeitet',
      class: 'w-8',
    },
    {
      name: 'carePeriodCommitted',
      label: 'Ferien',
      class: '',
    },
    {
      name: 'name',
      label: 'Veranstaltungtitel',
      class: '',
    },
    {
      name: 'organizer',
      label: 'Veranstalter',
      class: '',
    },
    {
      name: 'age',
      label: 'Alter',
      class: 'w-24',
    },
    {
      name: 'location',
      label: 'Ort',
      class: '',
    },
    {
      name: 'costs',
      label: 'Kosten',
      class: 'w-8',
    },
    {
      name: 'participants',
      label: 'Teilnehmer',
      class: '!pr-0 w-28',
    },
    {
      name: 'participantsPercent',
      label: '%',
      class: '!pl-0 w-4',
    },
    {
      name: 'waitinglist',
      label: 'Warteliste',
      class: '!pr-0 w-28',
    },
    {
      name: 'waitinglistPercent',
      label: '%',
      class: '!pl-0 w-4',
    },
  ];

  setSortBy(column: string) {
    this.store.dispatch(
      EventsActions.sortBy({
        column,
      })
    );
  }

  changeSearchText(text: string) {
    this.store.dispatch(EventsActions.changeSearchText({ searchText: text }));
  }

  constructor(private store: Store) {}

  onDuplicateEvent(eventId: string): void {
    this.store.dispatch(
      EventsActions.duplicateEventFormShow({
        eventId,
      })
    );
  }

  setContextMenuEventIdName(eventId: string, eventName: string) {
    this.contextMenuEventId = eventId;
    this.contextMenuEventName = eventName;
  }

  ngOnInit() {
    this.contextMenuItems = [
      {
        label: 'Duplizieren',
        command: (event) =>
          this.onDuplicateEvent(this.contextMenuEventId as string),
      },
    ];
  }

  exportEvents() {
    this.store.dispatch(EventsActions.exportEvents());
  }

  statusDeutsch: { [index: string]: string } = {
    new: 'eingereicht',
    planned: 'geplant',
    check: 'prüfung',
    approved: 'freigegeben',
    cancelled: 'abgesagt',
  };

  badgeColors: { [index: string]: string } = {
    new: 'red',
    planned: 'orange',
    check: 'yellow',
    approved: 'green',
    cancelled: 'gray',
  };

  processDuplicateEvent(): void {
    this.store.dispatch(EventsActions.duplicateEvent());
  }

  cancelDuplicateEvent(): void {
    this.store.dispatch(EventsActions.duplicateEventFormCancel());
  }

  updateDuplicateEventAmount(amount: string): void {
    this.store.dispatch(
      EventsActions.duplicateEventFormUpdateAmount({
        amount: parseInt(amount),
      })
    );
  }

  updateDuplicateEventSeason(seasonId: string): void {
    this.store.dispatch(
      EventsActions.duplicateEventFormSelectSeason({
        seasonId,
      })
    );
  }
}
