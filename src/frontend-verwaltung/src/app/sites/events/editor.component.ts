import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import * as eventSelectors from '../../stores/event/event.selectors';
import { combineLatest, map } from 'rxjs';
import { selectUserRole } from 'src/app/stores/auth/auth.selectors';
import { EventActions } from 'src/app/stores/event/event.actions';

@Component({
  selector: 'app-booking-details',
  template: `
    <div>{{ tab$ | async }}</div>
    <div class="card flex flex-grow flex-col overflow-hidden">
      <div class="mx-[-2rem] my-[-1rem] mb-0 flex flex-row">
        <div
          class="tab"
          [class.active]="(tab$ | async) === 'details'"
          [routerLink]="['.']"
          fragment="details"
        >
          📖 Veranstaltungsdetails
        </div>
        <a
          class="tab"
          [class.active]="(tab$ | async) === 'teilnehmer'"
          [routerLink]="['.']"
          fragment="teilnehmer"
        >
          👥 Teilnehmer-/Warteliste
        </a>
      </div>
      <form
        *ngIf="formState$ | async as formState"
        class="mr-[-1rem] flex flex-grow flex-col pr-[1rem]"
        [ngSwitch]="tab$ | async"
        [ngrxFormState]="formState"
      >
        <div class="flex flex-grow flex-col" *ngSwitchCase="'details'">
          <div class="flex h-[0] flex-grow flex-row overflow-y-auto">
            <app-event-editor-information class="w-1/3" />
            <app-event-editor-care-time class="w-1/3" />
            <app-event-editor-location class="w-1/3" />
          </div>
          <div
            class="m-[-1rem] mt-0 flex flex-row justify-between border-t-2 border-neutralGrayDark/10 bg-secondaryGray/5 p-6"
          >
            <div class="flex flex-row">
              <div
                *ngIf="status$ | async; let status"
                class="badge mr-4 items-center"
                [ngClass]="statusStyles[status]"
              >
                <p class="text-xl font-bold text-inherit">
                  {{ statusLabels[status] }}
                </p>
              </div>
              <div
                *ngIf="status$ | async; let status"
                class="flex flex-col justify-center"
              >
                <div [ngClass]="statusStyle$ | async">
                  {{ statusText$ | async }}
                </div>
                <div>{{ statusDescription$ | async }}</div>
              </div>
            </div>
            <div class="flex flex-row items-center gap-4">
              <button
                [disabled]="!(canDelete$ | async)"
                (click)="deleteEvent()"
                class="danger"
              >
                <i class="mi">delete_forever</i>
                Löschen
              </button>
              <button (click)="saveEvent()">
                <i class="mi">save</i>
                Speichern
              </button>
              <button *ngIf="canApprove$ | async" (click)="approve()">
                <i class="mi">publish</i>
                Freigeben
              </button>
              <button *ngIf="canCancel$ | async" (click)="cancel()">
                <i class="mi">event_busy</i>
                Absagen
              </button>
              <button
                [disabled]="!(canSubmitToCheck$ | async)"
                *ngIf="showSubmitToCheck$ | async"
                (click)="submitToCheck()"
              >
                <i class="mi">mark_email_read</i>
                Zur Prüfung übermitteln
              </button>
              <button (click)="nextEvent()">
                <i class="mi">arrow_right_alt</i>
                Nächste
              </button>
            </div>
          </div>
        </div>
        <div
          class="flex h-[0] flex-grow flex-col overflow-y-auto pr-[1rem]"
          *ngSwitchCase="'teilnehmer'"
        >
          <app-waiting-list />
        </div>
      </form>
    </div>
  `,
  styles: [
    `
      :host {
        @apply flex flex-col flex-grow;
      }

      .tab {
        @apply w-1/2 text-lg flex items-center justify-center p-3 bg-neutralGrayLight mt-[-1rem] pt-[2.1rem];

        &:not(.active) {
          @apply cursor-pointer shadow-[inset_0_0_.3rem_#0000003d];

          &:hover {
            @apply bg-primaryLight/10;
          }
        }

        &:first-child {
        }

        &:last-child {
          @apply border-r-0;
        }

        &.active {
          @apply border-b-0 bg-neutralWhite text-primaryLight;
        }
      }

      .red {
        @apply text-secondaryRed;
      }
      .orange {
        @apply text-primaryOrange;
      }
      .yellow {
        @apply text-secondaryYellow;
      }
      .green {
        @apply text-primaryLight;
      }
      .blue {
        @apply text-secondaryBlue;
      }
      .gray {
        @apply text-secondaryGray;
      }
    `,
  ],
})
export class EventEditorComponent {
  formState$ = this.store.select(eventSelectors.selectFormState);
  role$ = this.store.select(selectUserRole);

  roleIsAdministrator$ = this.role$.pipe(
    map((role) => role === 'administrator')
  );

  roleIsOrganizer$ = this.role$.pipe(map((role) => role === 'organizer'));
  status$ = this.store.select(eventSelectors.selectEventStatus);

  canDelete$ = this.store.select(eventSelectors.selectCanDelete);
  canApprove$ = this.store.select(eventSelectors.selectCanApprove);
  canCancel$ = this.store.select(eventSelectors.selectCanCancel);
  showSubmitToCheck$ = this.store.select(
    eventSelectors.selectShowSubmitToCheck
  );
  canSubmitToCheck$ = this.store.select(eventSelectors.selectCanSubmitToCheck);

  statusNextLabel$ = combineLatest([this.status$, this.role$]).pipe(
    map(
      ([status, role]) =>
        this.statusNextLabels[this.statusNext[role as string]][status as string]
    )
  );

  statusStyleNext$ = this.status$.pipe(
    map((status) => this.statusStyles[this.statusNext[status as string]])
  );

  statusStyle$ = this.status$.pipe(
    map((status) => this.statusStyles[status as string])
  );

  statusText$ = this.status$.pipe(
    map((status) => this.statusTexts[status as string])
  );

  statusDescription$ = combineLatest([this.status$, this.role$]).pipe(
    map(
      ([status, role]) =>
        this.statusDescriptions[role as string][status as string]
    )
  );

  tab$ = this.store.select(eventSelectors.selectEventTab);

  statusStyles: { [index: string]: string } = {
    new: 'red',
    planned: 'orange',
    check: 'yellow',
    approved: 'green',
    cancelled: 'gray',
  };

  statusLabels: { [index: string]: string } = {
    new: 'eingereicht',
    planned: 'geplant',
    check: 'Prüfung',
    approved: 'freigegeben',
    cancelled: 'abgesagt',
  };

  statusNext: { [index: string]: string } = {
    new: 'planned',
    planned: 'approved',
    check: 'approved',
    approved: 'cancelled',
  };

  statusNextLabels: { [index: string]: { [index: string]: string } } = {
    administrator: {
      approved: 'freigeben',
      cancelled: 'absagen',
    },
    organizer: {
      check: 'Zur Prüfung übermitteln',
    },
  };

  statusTexts: { [index: string]: string } = {
    new: 'Neue Veranstaltung wurde eingereicht',
    planned: 'Veranstaltung wurde einem Ferienzeitraum zugewiesen',
    check: 'Die Veranstaltung wurde zur Überprüfung eingereicht',
    approved: 'Veranstaltung hat Freigabe erhalten',
    cancelled: 'Veranstaltung ist online und kann nicht mehr gebucht werden',
  };

  statusDescriptions: { [index: string]: { [index: string]: string } } = {
    administrator: {
      new: 'Nächster Schritt: Weisen Sie der Veranstaltung einen Ferienzeitraum zu',
      planned:
        'Nächster Schritt: Warten Sie bis der Veranstalter die Veranstaltung zur Prüfung eingereicht hat',
      check:
        'Nächster Schritt: Prüfen Sie die Veranstaltung und erteilen Sie die Freigabe',
      approved: 'Nächster Schritt: Veröffentlichen Sie das Angebot online',
      cancelled: '',
    },
    organizer: {
      new: 'Warten Sie auf Rückmeldung durch die Verwaltung',
      planned:
        'Vervollständigen Sie die fehlenden Angaben, um die Veranstaltung zur Prüfung zu übermitteln.',
      check:
        'Sie erhalten eine Benachrichtigung sobald die Freigabe erteilt wurde.',
      approved: '',
      cancelled: '',
    },
  };

  constructor(private store: Store) {}

  saveEvent() {
    this.store.dispatch(EventActions.saveEvent());
  }

  nextEvent() {
    this.store.dispatch(EventActions.nextEvent());
  }

  deleteEvent() {
    this.store.dispatch(EventActions.deleteEvent());
  }

  approve() {
    if (!window.confirm('Veranstaltung wirklich freigeben?')) {
      return;
    }

    this.store.dispatch(EventActions.updateState({ status: 'approved' }));
  }

  cancel() {
    if (!window.confirm('Veranstaltung wirklich absagen?')) {
      return;
    }

    this.store.dispatch(EventActions.updateState({ status: 'cancelled' }));
  }

  submitToCheck() {
    if (!window.confirm('Veranstaltung wirklich zur Prüfung einreichen?')) {
      return;
    }

    this.store.dispatch(EventActions.updateState({ status: 'check' }));
  }
}
