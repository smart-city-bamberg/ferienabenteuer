import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import {
  selectPartnersSorted,
  selectSearchText,
  selectSortBy,
} from '../stores/partners/partners.selectors';
import { PartnersActions } from '../stores/partners/partners.actions';
import { selectUserRole } from '../stores/auth/auth.selectors';
import { map } from 'rxjs';

@Component({
  selector: 'app-partner-company',
  template: `
    <div class="card flex flex-grow flex-col gap-4 py-8">
      <div class="flex flex-row gap-4">
        <button
          *ngIf="roleIsAdministrator$ | async"
          (click)="generateEarlyAccessCodes()"
        >
          Frühbuchercodes erzeugen
        </button>
      </div>
      <div
        class="flex items-center rounded-lg border border-primaryDark/30 bg-white text-gray-600"
      >
        <button type="submit" class="border-none">
          <i class="mi text-[#757575]">search</i>
        </button>
        <input
          class="text-md flex-grow rounded-lg px-4 py-2 focus:outline-none"
          type="search"
          name="search"
          placeholder="Suchen"
          aria-label="Suchfeld"
          [value]="searchText$ | async"
          #searchInput
          (input)="changeSearchText(searchInput.value)"
        />
      </div>
      <div class="flex h-[0] flex-grow flex-col overflow-auto pr-1">
        <table class="table-auto border-separate border-spacing-y-2">
          <thead *ngIf="sortBy$ | async; let sortBy">
            <tr>
              <th
                *ngFor="let column of headers"
                class="sortable"
                [ngClass]="column.class"
              >
                <div (click)="setSortBy(column.name)">
                  <p>{{ column.label }}</p>
                  <i class="mi w-4">{{
                    sortBy.by !== column.name
                      ? 'unfold_more'
                      : sortBy.descending
                      ? 'arrow_downwards'
                      : 'arrow_upwards'
                  }}</i>
                </div>
              </th>

              <th>
                <div>
                  <p>Aktion</p>
                </div>
              </th>
            </tr>
          </thead>
          <tbody>
            <tr class="h-16" *ngFor="let partner of partners$ | async">
              <td>
                <p>
                  {{ partner.name }}
                </p>
              </td>
              <td>
                <p>
                  {{ partner.contactPersonSurname }},
                  {{ partner.contactPersonFirstname }}
                </p>
              </td>
              <td>
                <p>{{ partner.phone }}</p>
              </td>
              <td>
                <p>{{ partner.email }}</p>
              </td>
              <td>
                <p *ngIf="partner.earlyAccessCode.length > 0">
                  {{ partner.earlyAccessCode[0].code }}
                </p>
              </td>
              <td>
                <p *ngIf="partner.earlyAccessCode.length > 0">
                  {{
                    partner.earlyAccessCode[0].notificationDate
                      | date : 'dd.MM.yyyy'
                  }}
                </p>
                <p *ngIf="partner.earlyAccessCode.length > 0">
                  {{
                    partner.earlyAccessCode[0].notificationDate
                      | date : 'HH:mm:ss'
                  }}
                </p>
              </td>
              <td>
                <p class="gap-2">
                  <a href="mailto:{{ partner.email }}">
                    <button class="p-1">
                      <i class="mi">mail</i>
                    </button>
                  </a>
                  <button class="p-1" (click)="sendEarlyAccessCode(partner.id)">
                    <i class="mi">key</i>
                  </button>
                </p>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  `,
  styles: [
    `
      :host {
        @apply flex flex-grow;
      }
    `,
  ],
})
export class PartnersComponent {
  partners$ = this.store.select(selectPartnersSorted);

  sortBy$ = this.store.select(selectSortBy);
  searchText$ = this.store.select(selectSearchText);

  role$ = this.store.select(selectUserRole);

  roleIsAdministrator$ = this.role$.pipe(
    map((role) => role === 'administrator')
  );

  headers = [
    {
      name: 'name',
      label: 'Name',
    },
    {
      name: 'contact',
      label: 'Kontakt',
      class: '',
    },
    {
      name: 'phone',
      label: 'Telefon',
      class: 'w-30',
    },
    {
      name: 'email',
      label: 'E-Mail',
      class: '',
    },
    {
      name: 'code',
      label: 'Frühbuchercode',
      class: '',
    },
    {
      name: 'notificationDate',
      label: 'Verschickt',
      class: '',
    },
  ];

  setSortBy(column: string) {
    this.store.dispatch(
      PartnersActions.sortBy({
        column,
      })
    );
  }

  changeSearchText(text: string) {
    this.store.dispatch(PartnersActions.changeSearchText({ searchText: text }));
  }

  generateEarlyAccessCodes() {
    this.store.dispatch(PartnersActions.generateEarlyAccessCodes());
  }

  sendEarlyAccessCode(partnerId: string) {
    this.store.dispatch(PartnersActions.openSendEarlyAccessCode({ partnerId }));
  }

  constructor(private store: Store) {}
}
