import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import {
  selectBookings,
  selectBookingsSorted,
  selectSearchText,
  selectSortBy,
} from '../stores/bookings/bookings.selectors';
import { BookingsActions } from '../stores/bookings/bookings.actions';
import { selectUserRole } from '../stores/auth/auth.selectors';
import { map } from 'rxjs';
import { MenuItem } from 'primeng/api';
import { Booking, Ticket } from '../stores/bookings/bookings.types';

@Component({
  selector: 'app-bookings',
  template: `
    <div class="card flex flex-grow flex-col py-8">
      <div
        class="flex items-center rounded-lg border border-primaryDark/30 bg-white text-gray-600"
      >
        <button type="submit" class="border-none">
          <i class="mi text-[#757575]">search</i>
        </button>
        <input
          class="text-md flex-grow rounded-lg px-4 py-2 focus:outline-none"
          type="search"
          name="search"
          placeholder="Suchen"
          aria-label="Suchfeld"
          [value]="searchText$ | async"
          #searchInput
          (input)="changeSearchText(searchInput.value)"
        />
      </div>
      <div class="flex h-[0] flex-grow flex-col overflow-auto pr-1">
        <table class="table-auto border-separate border-spacing-y-2">
          <thead *ngIf="sortBy$ | async; let sortBy">
            <tr>
              <th
                *ngFor="let column of headers"
                class="sortable"
                [ngClass]="column.class"
              >
                <div (click)="setSortBy(column.name)">
                  <p>{{ column.label }}</p>
                  <i class="mi w-4">{{
                    sortBy.by !== column.name
                      ? 'unfold_more'
                      : sortBy.descending
                      ? 'arrow_downwards'
                      : 'arrow_upwards'
                  }}</i>
                </div>
              </th>

              <th>
                <div>
                  <p>Aktion</p>
                </div>
              </th>
            </tr>
          </thead>
          <tbody>
            <tr
              class="h-16"
              [class]="badgeColorRow[booking.status]"
              *ngFor="
                let booking of bookings$ | async;
                trackBy: identifyBooking
              "
            >
              <td align="center">
                <p class="badge w-24" [ngClass]="badgeColors[booking.status]">
                  {{ statusDeutsch[booking.status] }}
                </p>
              </td>
              <td>
                <p class="justify-center">{{ booking.code }}</p>
              </td>
              <td>
                <p class="justify-center">{{ booking.tickets.length }}</p>
              </td>
              <td>
                <p class="justify-end">
                  {{ booking.price | number : '1.2-2' }} €
                </p>
              </td>

              <td
                *ngIf="booking.event.carePeriodCommitted"
                [routerLink]="['/veranstaltungen', booking.event.id]"
              >
                <p class="text-sm text-gray-400">
                  {{ booking.event.carePeriodCommitted.period.holiday.name }}
                </p>
                <p>
                  {{
                    booking.event.carePeriodCommitted.period.startDate
                      | date : 'dd.MM.'
                  }}-{{
                    booking.event.carePeriodCommitted.period.endDate
                      | date : 'dd.MM.'
                  }}
                </p>
              </td>

              <td>
                <p>
                  {{ booking.custodian.surname }},
                  {{ booking.custodian.firstname }}
                </p>
              </td>
              <td>
                <p>{{ booking.event.name }}</p>
              </td>
              <td>
                <p>{{ booking.event.organizer.name }}</p>
              </td>
              <td>
                <p>
                  {{ booking.date | date : 'dd.MM.yyyy' }}
                </p>
                <p>{{ booking.date | date : 'HH:mm:ss' }}</p>
              </td>
              <td>
                <p>
                  {{ booking.reminderDate | date : 'dd.MM.yyyy' }}
                </p>
                <p>
                  {{ booking.reminderDate | date : 'HH:mm:ss' }}
                </p>
              </td>
              <td>
                <p class="gap-1">
                  <a
                    href="mailto:{{
                      booking.custodian.user.email
                    }}?subject=Buchung {{ booking.code }} | {{
                      booking.event.name
                    }}"
                  >
                    <button class="p-1">
                      <i class="mi">mail</i>
                    </button>
                  </a>
                  <button
                    #menu
                    class="p-1"
                    style="background-color: transparent; border: none"
                    (click)="setContextMenuBookingId(booking.id)"
                  >
                    <i class="mi text-xl text-black">more_vert</i>
                  </button>

                  <p-contextMenu
                    [target]="menu"
                    [model]="contextMenuItems"
                    [triggerEvent]="'click'"
                  ></p-contextMenu>
                </p>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  `,
  styles: [
    `
      :host {
        @apply flex flex-grow flex-col;
      }
    `,
  ],
})
export class BookingsComponent {
  bookings$ = this.store.select(selectBookingsSorted);
  contextMenuItems: MenuItem[] | undefined;
  contextMenuBookingId?: string;

  sortBy$ = this.store.select(selectSortBy);
  searchText$ = this.store.select(selectSearchText);

  headers = [
    {
      name: 'status',
      label: 'Status',
      class: 'w-28',
    },
    {
      name: 'code',
      label: 'Buchungsnummer',
      class: 'w-20',
    },
    {
      name: 'ticketCount',
      label: 'Tickets',
      class: 'w-16',
    },
    {
      name: 'price',
      label: 'Summe',
      class: 'w-16',
    },
    {
      name: 'carePeriodCommitted',
      label: 'Ferien',
      class: 'w-32',
    },
    {
      name: 'custodian',
      label: 'Sorgeberechtiger',
      class: '',
    },
    {
      name: 'event',
      label: 'Veranstaltung',
      class: '',
    },
    {
      name: 'organizer',
      label: 'Veranstalter',
      class: '',
    },
    {
      name: 'date',
      label: 'Buchung',
      class: 'w-28',
    },
    {
      name: 'reminderDate',
      label: 'Erinnert',
      class: 'w-28',
    },
  ];

  setSortBy(column: string) {
    this.store.dispatch(
      BookingsActions.sortBy({
        column,
      })
    );
  }

  changeSearchText(text: string) {
    this.store.dispatch(BookingsActions.changeSearchText({ searchText: text }));
  }

  statusDeutsch: { [index: string]: string } = {
    overdue: 'überfällig',
    pending: 'ausstehend',
    payed: 'bezahlt',
  };

  badgeColors: { [index: string]: string } = {
    overdue: 'red',
    pending: 'orange',
    payed: 'green',
  };
  badgeColorRow: { [index: string]: string } = {
    overdue: 'red',
    pending: 'green',
    payed: 'green',
  };

  role$ = this.store.select(selectUserRole);

  roleIsAdministrator$ = this.role$.pipe(
    map((role) => role === 'administrator')
  );

  showConfirmationDialog(message: string): boolean {
    return window.confirm(message);
  }

  onRegisterPayment(bookingId: string): void {
    const confirmationMessage = 'Möchten Sie die Zahlung erfassen?';
    if (this.showConfirmationDialog(confirmationMessage)) {
      this.store.dispatch(
        BookingsActions.registerPayment({
          bookingId,
        })
      );
      console.log(bookingId);
    }
  }

  onPaymentReminder(bookingId: string): void {
    this.store.dispatch(
      BookingsActions.openBookingPaymentOverdue({
        bookingId: bookingId,
      })
    );
  }

  onResendBookingConfirmation(bookingId: string): void {
    this.store.dispatch(
      BookingsActions.resendBookingConfirmation({
        bookingId: bookingId,
      })
    );
  }

  setContextMenuBookingId(bookingId: string) {
    this.contextMenuBookingId = bookingId;
  }

  constructor(private store: Store) {}

  identifyBooking(index: number, booking: Booking) {
    return booking.id;
  }

  ngOnInit() {
    this.contextMenuItems = [
      {
        label: 'Zahlung erfassen',
        command: (event) =>
          this.onRegisterPayment(this.contextMenuBookingId as string),
      },
      {
        label: 'Zahlungserinnerung',
        command: (event) =>
          this.onPaymentReminder(this.contextMenuBookingId as string),
      },
      {
        label: 'Buchungsbestätigung erneut versenden',
        command: (event) =>
          this.onResendBookingConfirmation(this.contextMenuBookingId as string),
      },
    ];
  }
}
