import { Component } from '@angular/core';
import { OrganizersActions } from '../stores/organizers/organizers.actions';
import { Store } from '@ngrx/store';
import {
  selectOrganizersSorted,
  selectSearchText,
  selectSortBy,
} from '../stores/organizers/organizers.selectors';

@Component({
  selector: 'app-organizers',
  template: `
    <div class="card flex flex-grow flex-col py-8">
      <div
        class="flex items-center rounded-lg border border-primaryDark/30 bg-white text-gray-600"
      >
        <button type="submit" class="border-none">
          <i class="mi text-[#757575]">search</i>
        </button>
        <input
          class="text-md flex-grow rounded-lg px-4 py-2 focus:outline-none"
          type="search"
          name="search"
          placeholder="Suchen"
          aria-label="Suchfeld"
          [value]="searchText$ | async"
          #searchInput
          (input)="changeSearchText(searchInput.value)"
        />
      </div>
      <div class="flex h-[0] flex-grow flex-col overflow-auto pr-1">
        <table class="table-auto border-separate border-spacing-y-2">
          <thead *ngIf="sortBy$ | async; let sortBy">
            <tr>
              <th
                *ngFor="let column of headers"
                class="sortable"
                [ngClass]="column.class"
              >
                <div (click)="setSortBy(column.name)">
                  <p>{{ column.label }}</p>
                  <i class="mi w-4">{{
                    sortBy.by !== column.name
                      ? 'unfold_more'
                      : sortBy.descending
                      ? 'arrow_downwards'
                      : 'arrow_upwards'
                  }}</i>
                </div>
              </th>

              <th>
                <div>
                  <p>Aktion</p>
                </div>
              </th>
            </tr>
          </thead>
          <tbody>
            <tr class="h-16" *ngFor="let organizer of organizers$ | async">
              <td>
                <p>{{ organizer.name }},</p>
              </td>
              <td>
                <p>
                  {{ organizer.addressStreet }}
                </p>
                <p>{{ organizer.addressZip }} {{ organizer.addressCity }}</p>
              </td>
              <td>
                <p>
                  {{ organizer.contactPersonSurname }},
                  {{ organizer.contactPersonFirstname }}
                </p>
              </td>
              <td>
                <p>
                  {{ organizer.phone }}
                </p>
              </td>
              <td>
                <p>
                  {{ organizer.email }}
                </p>
              </td>
              <td>
                <p class="gap-1">
                  <a href="mailto:{{ organizer.email }}">
                    <button class="p-1">
                      <i class="mi">mail</i>
                    </button>
                  </a>
                </p>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  `,
  styles: [
    `
      :host {
        @apply flex flex-grow flex-col;
      }
    `,
  ],
})
export class OrganizersComponent {
  organizers$ = this.store.select(selectOrganizersSorted);

  sortBy$ = this.store.select(selectSortBy);
  searchText$ = this.store.select(selectSearchText);

  headers = [
    {
      name: 'name',
      label: 'Name',
    },
    {
      name: 'address',
      label: 'Adresse',
      class: 'w-34',
    },
    {
      name: 'contact',
      label: 'Kontakt',
      class: '',
    },
    {
      name: 'phone',
      label: 'Telefon',
      class: 'w-30',
    },
    {
      name: 'email',
      label: 'E-Mail',
      class: '',
    },
  ];

  setSortBy(column: string) {
    this.store.dispatch(
      OrganizersActions.sortBy({
        column,
      })
    );
  }

  changeSearchText(text: string) {
    this.store.dispatch(
      OrganizersActions.changeSearchText({ searchText: text })
    );
  }

  constructor(private store: Store) {}
}
