import { NgModule, inject } from '@angular/core';
import { Router, RouterModule, Routes, UrlTree } from '@angular/router';
import { LoginComponent } from './sites/auth/login.component';
import { MainComponent } from './sites/main.component';
import { EventListComponent } from './sites/events/list.component';
import { BookingsComponent } from './sites/bookings.component';
import { InvoicesComponent } from './sites/invoices.component';
import { OrganizersComponent } from './sites/organizer.component';
import { CustodiansComponent } from './sites/custodians.component';
import { PartnersComponent } from './sites/partners.component';
import { SeasonComponent } from './sites/season.component';
import { EmailTemplatesComponent } from './sites/email-templates.component';
import { DiscountsComponent } from './sites/discounts.component';
import { EventEditorComponent } from './sites/events/editor.component';
import { Store } from '@ngrx/store';
import { selectUserRole } from './stores/auth/auth.selectors';
import { filter, map, tap } from 'rxjs';
import { AccessDeniedComponent } from './sites/auth/access-denied.component';
import { ProfileComponent } from './sites/profile.component';
import { ReservationsComponent } from './sites/reservations.component';

const roleGuard = (allowedRoles: string[]) => () => {
  const store = inject(Store);
  const router = inject(Router);
  return store.select(selectUserRole).pipe(
    filter((userRole) => userRole !== undefined),
    map((userRole) =>
      allowedRoles.includes(userRole ?? '')
        ? true
        : router.parseUrl('keinzugang')
    )
  );
};

const administratorCanActivate = roleGuard(['administrator']);

const organizerCanActivate = roleGuard(['organizer']);

const administratorOrganizerCanActivate = roleGuard([
  'administrator',
  'organizer',
]);

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'keinzugang',
    component: AccessDeniedComponent,
  },
  {
    path: '',
    component: MainComponent,
    canActivate: [administratorOrganizerCanActivate],
    children: [
      {
        path: 'veranstaltungen',
        component: EventListComponent,
        canActivate: [administratorOrganizerCanActivate],
        data: {
          label: 'Veranstaltungen',
        },
      },
      {
        path: 'veranstaltungen/:eventId',
        component: EventEditorComponent,
        canActivate: [administratorOrganizerCanActivate],
        data: {
          label: 'Veranstaltung',
        },
      },
      {
        path: 'veranstaltungen/anlegen',
        component: EventEditorComponent,
        canActivate: [administratorOrganizerCanActivate],
        data: {
          label: 'Veranstaltungen/anlegen',
        },
      },
      {
        path: 'buchungen',
        component: BookingsComponent,
        canActivate: [administratorOrganizerCanActivate],
        data: {
          label: 'Buchungen',
        },
      },
      {
        path: 'reservierungen',
        component: ReservationsComponent,
        canActivate: [administratorOrganizerCanActivate],
        data: {
          label: 'Reservierungen',
        },
      },
      {
        path: 'ermaessigungen',
        component: DiscountsComponent,
        canActivate: [administratorCanActivate],
        data: {
          label: 'Ermäßigungen',
        },
      },
      {
        path: 'abrechnung',
        component: InvoicesComponent,
        canActivate: [administratorOrganizerCanActivate],
        data: {
          label: 'Abrechnung',
        },
      },
      {
        path: 'profil',
        component: ProfileComponent,
        canActivate: [organizerCanActivate],
        data: {
          label: 'Profil',
        },
      },
      {
        path: 'veranstalter',
        component: OrganizersComponent,
        canActivate: [administratorCanActivate],
        data: {
          label: 'Veranstalter',
        },
      },
      {
        path: 'sorgeberechtigte',
        component: CustodiansComponent,
        canActivate: [administratorCanActivate],
        data: {
          label: 'Sorgeberechtigte',
        },
      },
      {
        path: 'partnerunternehmen',
        component: PartnersComponent,
        canActivate: [administratorCanActivate],
        data: {
          label: 'Partnerunternehmen',
        },
      },
      {
        path: 'saison',
        component: SeasonComponent,
        canActivate: [administratorCanActivate],
        data: {
          label: 'Saison',
        },
      },
      {
        path: 'vorlagen',
        component: EmailTemplatesComponent,
        canActivate: [administratorCanActivate],
        data: {
          label: 'Vorlagen',
        },
      },
      {
        path: '**',
        redirectTo: 'veranstaltungen',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
