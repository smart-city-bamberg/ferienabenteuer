import { Component } from '@angular/core';

@Component({
  selector: 'app-operator-icon',
  template: `
    <div>
      <img src="../assets/images/ferienabenteuer_bamberg_logo.svg" alt="Logo" />
    </div>
  `,
  styles: [],
})
export class OperatorIconComponent {}
