import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { selectUser } from '../stores/auth/auth.selectors';
import { AuthActions } from '../stores/auth/auth.actions';
import { map } from 'rxjs';

@Component({
  selector: 'app-user-info',
  template: `
    <nav
      [ngClass]="{ hidden: !menuOpen }"
      class="fixed right-0 top-0 mt-20 hidden h-full w-full flex-col bg-white shadow-lg md:relative md:mt-0 md:block md:flex-row md:bg-inherit md:bg-none md:shadow-none"
    >
      <ul
        class="border-t-custom-gray flex flex-col items-center border-t bg-white p-4 md:mt-0 md:flex md:h-full md:flex-row md:justify-between md:space-x-6 md:border-none md:bg-inherit md:p-0"
      >
        <div
          class="flex flex-grow flex-col justify-center md:flex md:h-full md:flex-row md:items-stretch md:justify-between md:space-x-6"
        >
          <div class="w-8"></div>

          <div
            *ngIf="user$ | async; let user"
            class="flex flex-col items-start md:flex md:flex-row md:items-center md:gap-2"
          >
            <div class="md:flex" clickOutside (clickOutside)="hideAccMenu()">
              <li
                [ngClass]="{ hidden: !(loggedIn$ | async) }"
                class="text-textDark relative flex flex-col font-normal md:items-center"
              >
                <a
                  class="mt-4 focus:bg-red-400 md:my-0"
                  (click)="toggleAccMenu()"
                >
                  <div
                    class="flex flex-row justify-center bg-white/50 md:rounded-md md:border-2 md:border-opacity-20 md:p-1"
                  >
                    <p class="role" [ngClass]="roleColors[user.role]">
                      {{ roleLabels[user.role] }}
                    </p>
                  </div>

                  <div
                    class="md:border-textDark flex flex-col items-start gap-2 bg-white md:rounded-md md:border-2 md:border-opacity-20 md:p-2 md:px-4"
                  >
                    <div
                      class="flex flex-row items-center gap-2 md:text-lg lg:text-xl"
                    >
                      <i class="mi">account_circle</i>
                      <p class="md:leading-tight">
                        {{ user.email }}
                      </p>
                      <i class="mi hidden md:block">arrow_drop_down</i>
                    </div>
                  </div>
                </a>
                <ul
                  [ngClass]="{ 'md:hidden': !accountMenuOpen }"
                  class="md:border-textDark bg-white md:absolute md:right-0 md:top-full md:mt-1 md:min-w-full md:rounded-md md:border-2 md:border-opacity-20 md:p-2 md:shadow-lg"
                >
                  <li class="li-hover" (click)="logout()">
                    <a class="account-menu-item">
                      <i class="mi">logout</i>Abmelden
                    </a>
                  </li>
                </ul>
              </li>
            </div>

            <li
              [ngClass]="{ hidden: loggedIn$ | async }"
              class="flex items-center"
            >
              <a class="my-3 md:my-0">
                <button
                  class="button text-[1.5rem]"
                  (click)="hideMenu()"
                  routerLink="/login"
                >
                  Anmelden
                </button>
              </a>
            </li>
          </div>
        </div>
      </ul>
    </nav>
  `,
  styles: [
    `
      :host {
        @apply flex z-10;
      }
      a {
        @apply text-lg md:text-sm lg:text-lg text-textGreenDark hover:text-primaryLight;
      }
      /* ul ul a {
        @apply text-xl;
      } */
      button {
        @apply text-2xl md:text-base;
      }
      li {
        @apply cursor-pointer;
      }
      .account-menu-item {
        @apply flex flex-row items-center gap-2;
      }

      .role {
        @apply text-sm;

        &.red {
          @apply text-secondaryRed;
        }
        &.blue {
          @apply text-secondaryYellow;
        }
      }
    `,
  ],
})
export class UserInfoComponent {
  user$ = this.store.select(selectUser);
  loggedIn$ = this.user$.pipe(map((user) => user));

  roleLabels: { [index: string]: string } = {
    administrator: 'Administrator',
    organizer: 'Veranstalter',
  };

  roleColors: { [index: string]: string } = {
    administrator: 'red',
    organizer: 'orange',
  };

  constructor(private store: Store) {}

  logout() {
    this.store.dispatch(AuthActions.logout());
  }

  public menuOpen = false;
  public accountMenuOpen = false;

  hideMenu() {
    this.menuOpen = false;
    this.accountMenuOpen = false;
  }

  hideAccMenu() {
    this.accountMenuOpen = false;
  }

  toggle() {
    this.menuOpen = !this.menuOpen;
  }
  toggleAccMenu() {
    this.accountMenuOpen = !this.accountMenuOpen;
  }
  closeAccMenuOnMouseLeave() {
    this.accountMenuOpen = false;
  }
  openAccMenuOnMouseEnter() {
    this.accountMenuOpen = true;
  }
}
