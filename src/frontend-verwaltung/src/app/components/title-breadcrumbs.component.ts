import { Component } from '@angular/core';
import {
  ActivatedRoute,
  ActivationEnd,
  NavigationEnd,
  Router,
} from '@angular/router';
import { Store } from '@ngrx/store';
import { getRouterSelectors } from '@ngrx/router-store';
import { filter, map } from 'rxjs';

const { selectRouteData } = getRouterSelectors();

@Component({
  selector: 'app-title-breadcrumbs',
  template: `
    <div class="flex flex-col pl-4">
      <p class="text-4xl font-bold text-neutralGrayDark">
        {{ page$ | async }}
      </p>
      <!-- <p class="text-sm text-gray-400">
        Menüpunkt > Untermenü > UnterUnterMenü
      </p> -->
    </div>
  `,
  styles: [],
})
export class TitleBreadcrumbsComponent {
  // page$ = this.router.events.pipe(
  //   filter((event) => event instanceof NavigationEnd),
  //   map((event) => this.router.routerState.root.firstChild?.firstChild?.data)
  //   // map((data) => data['label'] as string)
  // );

  page$ = this.store.select(selectRouteData).pipe(map((data) => data['label']));

  constructor(private router: Router, private store: Store) {}
}
