import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import {
  selectCurrentSeason,
  selectHasNextSeason,
  selectHasPreviousSeason,
} from '../stores/seasons/seasons.selectors';
import { filter, map } from 'rxjs';
import { SeasonsActions } from '../stores/seasons/seasons.actions';

@Component({
  selector: 'app-season-selector',
  template: `
    <button
      (click)="previousSeason()"
      [disabled]="hasNoPreviousSeason$ | async"
    >
      <i class="mi">chevron_left</i>
    </button>
    <p class="text-base font-bold text-neutralGrayDark">
      Saison {{ season$ | async }}
    </p>
    <button (click)="nextSeason()" [disabled]="hasNoNextSeason$ | async">
      <i class="mi">chevron_right</i>
    </button>
  `,
  styles: [
    `
      :host {
        @apply flex flex-row items-center justify-between gap-8;
      }

      button {
        @apply p-1 bg-primaryLight/90 border-none;

        &:disabled {
          @apply bg-primaryLight/30;
        }
      }
    `,
  ],
})
export class SeasonSelectorComponent {
  season$ = this.store
    .select(selectCurrentSeason)
    .pipe(map((season) => season?.year ?? ''));

  hasNoNextSeason$ = this.store
    .select(selectHasNextSeason)
    .pipe(map((has) => !has));
  hasNoPreviousSeason$ = this.store
    .select(selectHasPreviousSeason)
    .pipe(map((has) => !has));

  previousSeason() {
    this.store.dispatch(SeasonsActions.previousSeason());
  }

  nextSeason() {
    this.store.dispatch(SeasonsActions.nextSeason());
  }

  constructor(private store: Store) {}
}
