import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OperatorIconComponent } from './operator-icon.component';
import { TitleBreadcrumbsComponent } from './title-breadcrumbs.component';
import { SeasonSelectorComponent } from './season-selector.component';
import { MenuBarComponent } from './menu-bar.component';
import { RouterModule } from '@angular/router';
import { UserInfoComponent } from './user-info.component';
import { CareDaysComponent } from './care-days.component';
import { DirectivesModule } from '../directives/directives.module';

@NgModule({
  declarations: [
    OperatorIconComponent,
    TitleBreadcrumbsComponent,
    SeasonSelectorComponent,
    MenuBarComponent,
    UserInfoComponent,
    CareDaysComponent,
  ],
  imports: [CommonModule, RouterModule, DirectivesModule],
  exports: [
    TitleBreadcrumbsComponent,
    OperatorIconComponent,
    SeasonSelectorComponent,
    MenuBarComponent,
    UserInfoComponent,
    CareDaysComponent,
  ],
})
export class ComponentsModule {}
