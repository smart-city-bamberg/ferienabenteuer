import { Component, EventEmitter, Input, Output } from '@angular/core';

type CareDay = {
  date: Date;
  available: boolean;
  enabled: boolean;
};

@Component({
  selector: 'app-care-days',
  template: `
    <div
      class="flex w-[12%] cursor-pointer flex-col items-center rounded-lg border p-3"
      *ngFor="let day of careDays"
      [ngClass]="{
        'bg-gray-100 border-gray-100 opacity-50 cursor-not-allowed':
          !day.available,
        'hover:border hover:border-primaryLight': day.available,
        'bg-primaryLight/20 border-primaryLight': day.available && day.enabled,
        'bg-gray-200 border-gray-100': day.available && !day.enabled
      }"
      (click)="toggleCareDay(day)"
    >
      <p class="font-bold">{{ day.date | date : 'EE' }}</p>
      <p>{{ day.date | date : 'dd' }}.</p>
      <p>{{ day.date | date : 'MMM' }}</p>
      <p>
        <i *ngIf="day.available && day.enabled" class="mi">check_box</i>
        <i class="mi" *ngIf="day.available && !day.enabled"
          >check_box_outline_blank</i
        >
      </p>
    </div>
  `,
  styles: [
    `
      :host {
        @apply flex flex-row mt-2 flex-wrap gap-2;
      }
    `,
  ],
})
export class CareDaysComponent {
  @Input()
  careDays: CareDay[] = [];

  @Input()
  disabled: boolean = false;

  @Output()
  onToggleCareDay = new EventEmitter<CareDay>();

  toggleCareDay(day: CareDay) {
    if (this.disabled) {
      return;
    }

    this.onToggleCareDay.emit(day);
  }
}
