import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { selectUserRole } from '../stores/auth/auth.selectors';
import { filter, map } from 'rxjs';
import { environment } from 'src/environments/environment';
import { SeasonsActions } from '../stores/seasons/seasons.actions';

@Component({
  selector: 'app-menu-bar',
  template: `
    <div class="flex flex-grow flex-col gap-2">
      <div *ngFor="let item of items$ | async" class="flex flex-col gap-1 pb-4">
        <p class="textLight mx-2">{{ item.label }}</p>
        <li *ngFor="let menuItem of item.children">
          <a
            routerLinkActive="active-site-link"
            [routerLink]="menuItem.link"
            class="flex flex-row items-center gap-2"
            *ngIf="!menuItem.external && !menuItem.action"
          >
            <i class="mi text-lg">{{ menuItem.icon }}</i>
            <p class=" ">{{ menuItem.label }}</p>
          </a>
          <a
            [href]="menuItem.link"
            target="_blank"
            class="flex flex-row items-center gap-2"
            *ngIf="menuItem.external"
          >
            <i class="mi text-lg">{{ menuItem.icon }}</i>
            <p class=" ">{{ menuItem.label }}</p>
          </a>
          <div
            class="flex cursor-pointer flex-row items-center gap-2"
            *ngIf="menuItem.action"
            (click)="menuItem.action()"
          >
            <i class="mi text-lg">{{ menuItem.icon }}</i>
            <p class=" ">{{ menuItem.label }}</p>
          </div>
        </li>
      </div>
    </div>
  `,
  styles: [
    `
      :host {
        @apply flex flex-grow;
      }
      .textLight {
        @apply text-xl font-bold text-gray-400;
      }
      li {
        @apply list-none rounded-lg overflow-hidden mx-[-0.5rem];

        a,
        div {
          @apply flex  px-4 py-2  hover:bg-primaryLight/40 hover:text-neutralWhite;

          &.active-site-link {
            @apply bg-primaryLight/10 hover:text-neutralWhite hover:bg-primaryLight/30;
          }
        }
      }
    `,
  ],
})
export class MenuBarComponent {
  constructor(private store: Store) {}

  private readonly offersMenu = {
    label: 'Angebote',
    children: [
      {
        label: 'Veranstaltungen',
        link: 'veranstaltungen',
        icon: 'calendar_month',
      },
      {
        label: 'Buchungen',
        link: 'buchungen',
        icon: 'shopping_cart',
      },
      {
        label: 'Reservierungen',
        link: 'reservierungen',
        icon: 'add_shopping_cart',
      },
      {
        label: 'Abrechnung',
        link: 'abrechnung',
        icon: 'payments',
      },
    ],
  };

  private readonly offersMenuAdmin = {
    label: 'Angebote',
    children: [
      {
        label: 'Veranstaltungen',
        link: 'veranstaltungen',
        icon: 'calendar_month',
      },
      {
        label: 'Buchungen',
        link: 'buchungen',
        icon: 'shopping_cart',
      },
      {
        label: 'Ermäßigungen',
        link: 'ermaessigungen',
        icon: 'sell',
      },
      {
        label: 'Reservierungen',
        link: 'reservierungen',
        icon: 'add_shopping_cart',
      },
      {
        label: 'Abrechnung',
        link: 'abrechnung',
        icon: 'payments',
      },
    ],
  };

  private readonly userMenu = {
    label: 'Nutzer',
    children: [
      {
        label: 'Veranstalter',
        link: 'veranstalter',
        icon: 'person',
      },
      {
        label: 'Sorgeberechtigte',
        link: 'sorgeberechtigte',
        icon: 'person',
      },
      {
        label: 'Partnerunternehmen',
        link: 'partnerunternehmen',
        icon: 'person',
      },
    ],
  };

  private readonly adminSettingsMenu = {
    label: 'Einstellungen',
    children: [
      {
        label: 'Saison',
        link: 'saison',
        icon: 'date_range',
      },
      {
        label: 'Vorlagen',
        link: 'vorlagen',
        icon: 'mail',
      },
    ],
  };

  private readonly adminStatisticsMenu = {
    label: 'Statistik',
    children: [
      {
        label: 'Dashboard',
        link: environment.grafanaUrl,
        icon: 'query_stats',
        external: true,
      },
      {
        label: 'Export',
        icon: 'save',
        action: () =>
          this.store.dispatch(SeasonsActions.exportSeasonStatistics()),
      },
    ],
  };

  private readonly organizerSettingsMenu = {
    label: 'Einstellungen',
    children: [
      {
        label: 'Profil',
        link: 'profil',
        icon: 'account_circle',
      },
    ],
  };

  roleMenu: { [index: string]: any[] } = {
    administrator: [
      this.offersMenuAdmin,
      this.userMenu,
      this.adminSettingsMenu,
      this.adminStatisticsMenu,
    ],
    organizer: [this.offersMenu, this.organizerSettingsMenu],
  };

  items$ = this.store.select(selectUserRole).pipe(
    filter((role) => role !== undefined),
    map((role) => this.roleMenu[role as string])
  );
}
