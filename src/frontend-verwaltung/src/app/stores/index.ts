import { isDevMode } from '@angular/core';
import {
  ActionReducer,
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
  MetaReducer,
} from '@ngrx/store';
import * as fromRouter from '@ngrx/router-store';

import * as fromAuth from './auth/auth.reducer';
import * as fromEvents from './events/events.reducer';
import * as fromBookings from './bookings/bookings.reducer';
import * as fromDiscounts from './discounts/discounts.reducer';
import * as fromEvent from './event/event.reducer';
import * as fromHolidays from './holidays/holidays.reducer';
import * as fromOrganizers from './organizers/organizers.reducer';
import * as fromEmailTemplates from './email-templates/email-templates.reducer';
import * as fromSeasons from './seasons/seasons.reducer';
import * as fromCustodians from './custodians/custodians.reducer';
import * as fromPartners from './partners/partners.reducer';
import * as fromInvoices from './invoices/invoices.reducer';
import * as fromProfile from './profile/profile.reducer';

export interface State {
  router: fromRouter.RouterReducerState<any>;
  [fromAuth.authFeatureKey]: fromAuth.State;
  [fromEvents.eventsFeatureKey]: fromEvents.State;
  [fromBookings.bookingsFeatureKey]: fromBookings.State;
  [fromDiscounts.discountsFeatureKey]: fromDiscounts.State;  [fromEvent.eventFeatureKey]: fromEvent.State;

[fromHolidays.holidaysFeatureKey]: fromHolidays.State;
[fromOrganizers.organizersFeatureKey]: fromOrganizers.State;
[fromEmailTemplates.emailTemplatesFeatureKey]: fromEmailTemplates.State;
[fromSeasons.seasonsFeatureKey]: fromSeasons.State;
[fromCustodians.custodiansFeatureKey]: fromCustodians.State;
[fromPartners.partnersFeatureKey]: fromPartners.State;
[fromInvoices.invoicesFeatureKey]: fromInvoices.State;
[fromProfile.profileFeatureKey]: fromProfile.State;
}

export const reducers: ActionReducerMap<State> = {
  router: fromRouter.routerReducer,
  [fromAuth.authFeatureKey]: fromAuth.reducer,
  [fromEvents.eventsFeatureKey]: fromEvents.reducer,
  [fromBookings.bookingsFeatureKey]: fromBookings.reducer,
  [fromDiscounts.discountsFeatureKey]: fromDiscounts.reducer,
  [fromEvent.eventFeatureKey]: fromEvent.reducer,
  [fromHolidays.holidaysFeatureKey]: fromHolidays.reducer,
  [fromOrganizers.organizersFeatureKey]: fromOrganizers.reducer,
  [fromEmailTemplates.emailTemplatesFeatureKey]: fromEmailTemplates.reducer,
  [fromSeasons.seasonsFeatureKey]: fromSeasons.reducer,
  [fromCustodians.custodiansFeatureKey]: fromCustodians.reducer,
  [fromPartners.partnersFeatureKey]: fromPartners.reducer,
  [fromInvoices.invoicesFeatureKey]: fromInvoices.reducer,
  [fromProfile.profileFeatureKey]: fromProfile.reducer,
};

export const metaReducers: MetaReducer<State>[] = isDevMode() ? [] : [];
