import { createFeature, createReducer, on } from '@ngrx/store';
import { HolidaysActions } from './holidays.actions';
import { Holiday } from './holidays.types';

export const holidaysFeatureKey = 'holidays';

export interface State {
  holidays: Holiday[];
}

export const initialState: State = {
  holidays: [],
};

export const reducer = createReducer(
  initialState,
  on(HolidaysActions.loadHolidays, (state) => state),
  on(HolidaysActions.loadHolidaysSuccess, (state, action) => ({
    ...state,
    holidays: action.holidays,
  })),
  on(HolidaysActions.loadHolidaysFailure, (state, action) => state)
);

export const holidaysFeature = createFeature({
  name: holidaysFeatureKey,
  reducer,
});
