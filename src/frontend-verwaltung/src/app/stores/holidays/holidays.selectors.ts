import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromHolidays from './holidays.reducer';
import { Period } from './holidays.types';

export const selectHolidaysState = createFeatureSelector<fromHolidays.State>(
  fromHolidays.holidaysFeatureKey
);

export const selectHolidays = createSelector(
  selectHolidaysState,
  (state) => state.holidays
);

export const selectPeriods = createSelector(selectHolidays, (holidays) =>
  holidays
    .filter((holiday) => holiday.periods.length > 0)
    .reduce(
      (periods, holiday) => [
        ...periods,
        ...holiday.periods.map((period) => ({
          ...period,
          holidayName: holiday.name,
        })),
      ],
      new Array<Period & { holidayName: string }>()
    )
    .sort((a, b) => a.startDate.getTime() - b.startDate.getTime())
);
