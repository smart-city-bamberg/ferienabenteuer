import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import * as fromHolidays from './holidays.reducer';
import { EffectsModule } from '@ngrx/effects';
import { HolidaysEffects } from './holidays.effects';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StoreModule.forFeature(fromHolidays.holidaysFeatureKey, fromHolidays.reducer),
    EffectsModule.forFeature([HolidaysEffects])
  ]
})
export class HolidaysModule { }
