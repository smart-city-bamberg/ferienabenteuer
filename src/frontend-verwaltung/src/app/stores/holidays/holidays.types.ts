export interface Period {
  id: string;
  startDate: Date;
  endDate: Date;
}

export interface Holiday {
  id: string;
  name: string;
  periods: Period[];
}
