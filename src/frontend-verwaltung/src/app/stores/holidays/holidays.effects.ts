import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, map, concatMap } from 'rxjs/operators';
import { of } from 'rxjs';
import { HolidaysActions } from './holidays.actions';
import { HolidaysGQL } from 'src/app/graphql/generated';
import { Holiday } from './holidays.types';
import { AuthActions } from '../auth/auth.actions';

@Injectable()
export class HolidaysEffects {
  loadHolidays$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(HolidaysActions.loadHolidays),
      concatMap(() =>
        this.holidaysQuery
          .fetch({
            year: 2024,
          })
          .pipe(
            map((response) =>
              HolidaysActions.loadHolidaysSuccess({
                holidays: response.data.holidays?.map((holiday) => ({
                  ...holiday,
                  periods: holiday.periods?.map((period) => ({
                    id: period.id,
                    startDate: new Date(period.startDate),
                    endDate: new Date(period.endDate),
                  })),
                })) as Holiday[],
              })
            ),
            catchError((error) =>
              of(HolidaysActions.loadHolidaysFailure({ error }))
            )
          )
      )
    );
  });

  loadHolidaysAfterUserLoaded$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AuthActions.loadUserSuccess),
      map((action) => HolidaysActions.loadHolidays())
    );
  });

  constructor(private actions$: Actions, private holidaysQuery: HolidaysGQL) {}
}
