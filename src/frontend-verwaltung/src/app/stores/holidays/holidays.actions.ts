import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { Holiday } from './holidays.types';

export const HolidaysActions = createActionGroup({
  source: 'Holidays',
  events: {
    'Load Holidays': emptyProps(),
    'Load Holidays Success': props<{ holidays: Holiday[] }>(),
    'Load Holidays Failure': props<{ error: unknown }>(),
  },
});
