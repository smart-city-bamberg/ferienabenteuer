export type SortByState = {
  by: string;
  descending: boolean;
};

export const defaultSortByState: SortByState = {
  by: 'date',
  descending: false,
};

export const sortByReducer = (sort: SortByState, column: string): SortByState =>
  sort.by === column
    ? { by: column, descending: !sort.descending }
    : {
        by: column,
        descending: false,
      };

export const stringComparer =
  <T>(selector: (event: T) => string) =>
  (a: T, b: T) =>
    selector(a).localeCompare(selector(b));

export const numberComparer =
  <T>(selector: (event: T) => number) =>
  (a: T, b: T) =>
    selector(a) - selector(b);

export const dateComparer =
  <T>(selector: (event: T) => Date | undefined) =>
  (a: T, b: T) => {
    const selectedA = selector(a);
    const selectedB = selector(b);

    if (selectedA === undefined) {
      if (selectedB === undefined) {
        return 0;
      }

      return 1;
    }

    if (selectedB === undefined) {
      return -1;
    }

    return selectedA.getTime() - selectedB.getTime();
  };

export const booleanComparer =
  <T>(selector: (event: T) => boolean) =>
  (a: T, b: T) =>
    (selector(a) ? 1 : 0) - (selector(b) ? 1 : 0);
