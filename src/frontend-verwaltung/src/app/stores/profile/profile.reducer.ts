import { createFeature, createReducer, on } from '@ngrx/store';
import { ProfileActions } from './profile.actions';
import {
  FormGroupState,
  createFormGroupState,
  onNgrxForms,
  setValue,
  wrapReducerWithFormStateUpdate,
} from 'ngrx-forms';
import { validateForm } from './profile.validation';
import { Organizer } from './profile.types';

export const profileFeatureKey = 'profile';

export interface ProfileForm {
  [index: string]: any;
  contactPersonSalutation: string;
  contactPersonFirstname: string;
  contactPersonSurname: string;
  email: string;
  phone: string;
  addressCity: string;
  addressStreet: string;
  addressZip: string;
  bankName: string;
  bankOwner: string;
  bankIban: string;
  bankBic: string;
}

export interface State {
  form: FormGroupState<ProfileForm>;
}

export const initialState: State = {
  form: createFormGroupState('profile_form', {
    contactPersonSalutation: '',
    contactPersonFirstname: '',
    contactPersonSurname: '',
    email: '',
    phone: '',
    addressCity: '',
    addressStreet: '',
    addressZip: '',
    bankName: '',
    bankOwner: '',
    bankIban: '',
    bankBic: '',
  }),
};

export const reducer = wrapReducerWithFormStateUpdate(
  createReducer(
    initialState,
    on(ProfileActions.loadProfile, (state) => state),
    on(ProfileActions.loadProfileSuccess, (state, action) => ({
      ...state,
      form: setValue({
        ...Object.keys(action.organizer)
          .filter((key) => key !== '__typename')
          .reduce((obj, key) => {
            obj[key] = action.organizer[key];
            return obj;
          }, {} as ProfileForm),
      } as ProfileForm)(state.form),
    })),
    on(ProfileActions.loadProfileFailure, (state, action) => state),
    onNgrxForms()
  ),
  (state) => state.form,
  validateForm
);

export const profileFeature = createFeature({
  name: profileFeatureKey,
  reducer,
});
