export interface Organizer {
  [index: string]: any;
  contactPersonSalutation: string;
  contactPersonFirstname: string;
  contactPersonSurname: string;
  email: string;
  phone: string;
  addressCity: string;
  addressStreet: string;
  addressZip: string;
  bankName: string;
  bankOwner: string;
  bankIban: string;
  bankBic: string;
}
