import { updateGroup, validate, FormGroupState } from 'ngrx-forms';
import { minLength, required } from 'ngrx-forms/validation';
import { ProfileForm } from './profile.reducer';

export const validateForm = (form: FormGroupState<ProfileForm>) => {
  return updateGroup<ProfileForm>({
    contactPersonFirstname: validate(required, minLength(1)),
    contactPersonSurname: validate(required, minLength(1)),
    addressCity: validate(required, minLength(1)),
    addressStreet: validate(required, minLength(1)),
    addressZip: validate(required, minLength(5)),
    phone: validate(required, minLength(1)),
    email: validate(required, minLength(1)),
  })(form);
};
