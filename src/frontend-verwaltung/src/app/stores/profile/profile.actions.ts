import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { Organizer } from './profile.types';

export const ProfileActions = createActionGroup({
  source: 'Profile',
  events: {
    'Load Profile': emptyProps(),
    'Load Profile Success': props<{ organizer: Organizer }>(),
    'Load Profile Failure': props<{ error: unknown }>(),

    'Save Profile': emptyProps(),
    'Save Profile Success': emptyProps(),
    'Save Profile Rejected': emptyProps(),
    'Save Profile Failure': props<{ error: unknown }>(),
  },
});
