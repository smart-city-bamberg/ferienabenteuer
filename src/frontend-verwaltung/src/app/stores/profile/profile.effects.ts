import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  catchError,
  map,
  concatMap,
  filter,
  withLatestFrom,
  tap,
} from 'rxjs/operators';
import { of } from 'rxjs';
import { ProfileActions } from './profile.actions';
import { routerNavigatedAction } from '@ngrx/router-store';
import { ProfileGQL, UpdateProfileGQL } from 'src/app/graphql/generated';
import { Organizer } from './profile.types';
import { Store } from '@ngrx/store';
import { selectUserOrganizer } from '../auth/auth.selectors';
import { selectProfileForm } from './profile.selectors';
import { MessageService } from 'primeng/api';

@Injectable()
export class ProfileEffects {
  loadProfile$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ProfileActions.loadProfile),
      concatMap(() =>
        this.profileQuery.fetch().pipe(
          map((response) =>
            ProfileActions.loadProfileSuccess({
              organizer: response.data.authenticatedItem
                ?.organizer as Organizer,
            })
          ),
          catchError((error) =>
            of(ProfileActions.loadProfileFailure({ error }))
          )
        )
      )
    );
  });

  saveProfile$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ProfileActions.saveProfile),
      withLatestFrom(
        this.store.select(selectUserOrganizer),
        this.store.select(selectProfileForm)
      ),
      concatMap(([action, organizer, form]) =>
        !form.isValid
          ? of(ProfileActions.saveProfileRejected())
          : this.updateProfileMutation
              .mutate({
                organizerId: organizer?.id as string,
                data: {
                  ...form.value,
                },
              })
              .pipe(
                map((response) => ProfileActions.saveProfileSuccess()),
                catchError((error) =>
                  of(ProfileActions.saveProfileFailure({ error }))
                )
              )
      )
    );
  });

  loadProfileAfterNavigate$ = createEffect(() =>
    this.actions$.pipe(
      ofType(routerNavigatedAction),
      filter((action) => action.payload.routerState.url === '/profil'),
      map(() => ProfileActions.loadProfile())
    )
  );

  showToastAfterUpdateProfileSuccess$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(ProfileActions.saveProfileSuccess),
        tap((action) =>
          this.toastService.add({
            summary: 'Profil wurde gespeichert',
            severity: 'success',
          })
        )
      );
    },
    { dispatch: false }
  );

  showToastAfterUpdateProfileRejected$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(ProfileActions.saveProfileRejected),
        tap((action) =>
          this.toastService.add({
            summary: 'Bitte alle Pflichtfelder ausfüllen',
            severity: 'warn',
          })
        )
      );
    },
    { dispatch: false }
  );

  showToastAfterUpdateProfileFailure$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(ProfileActions.saveProfileFailure),
        tap((action) =>
          this.toastService.add({
            summary: 'Fehler beim Speichern des Profils.',
            severity: 'error',
          })
        )
      );
    },
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private store: Store,
    private toastService: MessageService,
    private profileQuery: ProfileGQL,
    private updateProfileMutation: UpdateProfileGQL
  ) {}
}
