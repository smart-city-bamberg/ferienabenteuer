import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromEvents from './events.reducer';
import * as routerStore from '@ngrx/router-store';
import { Event } from './events.types';
import { numberComparer, dateComparer, stringComparer } from '../sorting';

export const { selectUrl, selectCurrentRoute } =
  routerStore.getRouterSelectors();

export const selectEventsState = createFeatureSelector<fromEvents.State>(
  fromEvents.eventsFeatureKey
);

export const selectSortBy = createSelector(
  selectEventsState,
  (state) => state.sort
);

export const selectEvents = createSelector(selectEventsState, (state) =>
  state.events.map((event) => ({
    ...event,
    progress: calculateProgress(event),
    participantPercent: event.participantLimit
      ? (100 / (event.participantLimit ?? 1)) *
        event.bookedTicketCountParticipant
      : 0,
    waitinglistPercent: event.waitingListLimit
      ? (100 / (event.waitingListLimit ?? 1)) *
        event.bookedTicketCountWaitinglist
      : 0,
  }))
);

export const selectSearchText = createSelector(
  selectEventsState,
  (state) => state.searchText
);

type EventWithProgress = Event & {
  progress: number;
  participantPercent: number;
  waitinglistPercent: number;
};

const statusOrder = ['new', 'planned', 'check', 'approved', 'cancelled'];

const sorters: {
  [index: string]: (a: EventWithProgress, b: EventWithProgress) => number;
} = {
  progress: numberComparer((e) => e.progress),
  carePeriodCommitted: dateComparer(
    (e) => new Date(e.carePeriodCommitted.period.startDate)
  ),
  name: (a, b) => {
    const diff = a.name.localeCompare(b.name);

    if (diff !== 0) {
      return diff;
    }

    return (
      new Date(a.carePeriodCommitted.period.startDate).getTime() -
      new Date(b.carePeriodCommitted.period.startDate).getTime()
    );
  },
  organizer: stringComparer((e) => e.organizer.name),
  age: (a, b) => {
    const diff = a.ageMinimum - b.ageMinimum;

    if (diff !== 0) {
      return diff;
    }

    return a.ageMaximum - b.ageMaximum;
  },
  location: stringComparer((e) => e.locationCity),
  costs: numberComparer((e) => e.costs),
  participants: (a, b) => {
    const diff =
      (a.bookedTicketCountParticipant ?? 0) -
      (b.bookedTicketCountParticipant ?? 0);

    if (diff !== 0) {
      return diff;
    }

    return (a.participantLimit ?? 0) - (b.participantLimit ?? 0);
  },
  waitinglist: (a, b) => {
    const diff =
      (a.bookedTicketCountWaitinglist ?? 0) -
      (b.bookedTicketCountWaitinglist ?? 0);

    if (diff !== 0) {
      return diff;
    }

    return (a.waitingListLimit ?? 0) - (b.waitingListLimit ?? 0);
  },
  participantsPercent: numberComparer((e) => e.participantPercent),
  waitinglistPercent: numberComparer((e) => e.waitinglistPercent),
  status: (a, b) => {
    const statusA = statusOrder.indexOf(a.status);
    const statusB = statusOrder.indexOf(b.status);
    return statusA - statusB;
  },
};

export const selectEventsSorted = createSelector(
  selectEvents,
  selectSortBy,
  selectSearchText,
  (events, sort, searchText) => {
    const filteredEvents = events.filter((event) =>
      JSON.stringify({
        name: event.name,
        organizer: event.organizer,
        location: event.locationCity,
        costs: event.costs,
      })
        .toLowerCase()
        .includes(searchText.toLowerCase())
    );

    return sorters[sort.by]
      ? filteredEvents.sort(
          (a, b) => (sort.descending ? -1 : 1) * sorters[sort.by](a, b)
        )
      : filteredEvents;
  }
);

function calculateProgress(event: Event): any {
  var sum = 0;

  if (event.name !== '') sum++;
  if (event.picture) sum++;
  if (event.pictureRights !== '') sum++;
  if (event.description !== '') sum++;
  if (event.hints !== '') sum++;
  if (event.meals !== '') sum++;
  if (event.carePeriodCommitted) sum++;
  if (event.carePeriodDesired || event.status === 'approved') sum++;
  if (event.carePeriodAlternative || event.status === 'approved') sum++;
  if (event.careTimeStart !== '') sum++;
  if (event.careTimeEnd !== '') sum++;
  if (event.locationCity !== '') sum++;
  if (event.locationStreet !== '') sum++;
  if (event.locationZipcode !== '') sum++;
  if (event.ageMaximum !== 0) sum++;
  if (event.ageMinimum !== 0) sum++;
  if (event.participantMinimum !== null) sum++;
  if (event.participantLimit !== null) sum++;
  if (event.waitingListLimit !== null) sum++;
  if (event.contactPersonSalutation !== '') sum++;
  if (event.contactPersonFirstname !== '') sum++;
  if (event.contactPersonSurname !== '') sum++;
  if (event.emergencyPhone !== '') sum++;
  if (event.registrationDeadline) sum++;
  if (event.costs !== 0) sum++;

  return sum;
}

export const selectShowDuplicationForm = createSelector(
  selectEventsState,
  (state) => state.showDuplicationForm
);

export const selectDuplication = createSelector(
  selectEventsState,
  (state) => state.duplication
);
