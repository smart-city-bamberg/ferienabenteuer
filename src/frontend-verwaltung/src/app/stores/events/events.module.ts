import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import * as fromEvents from './events.reducer';
import { EffectsModule } from '@ngrx/effects';
import { EventsEffects } from './events.effects';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StoreModule.forFeature(fromEvents.eventsFeatureKey, fromEvents.reducer),
    EffectsModule.forFeature([EventsEffects]),
  ],
})
export class EventsModule {}
