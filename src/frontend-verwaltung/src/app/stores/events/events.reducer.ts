import { createFeature, createReducer, on } from '@ngrx/store';
import { EventsActions } from './events.actions';
import { Event } from './events.types';
import { SortByState, defaultSortByState, sortByReducer } from '../sorting';

export const eventsFeatureKey = 'events';

export interface State {
  events: Event[];
  sort: SortByState;
  searchText: string;
  duplication: {
    eventId: string | undefined;
    seasonId: string | undefined;
    amount: number;
  };
  showDuplicationForm: boolean;
}

export const initialState: State = {
  events: [],
  sort: defaultSortByState,
  searchText: '',
  duplication: {
    amount: 0,
    eventId: undefined,
    seasonId: undefined,
  },
  showDuplicationForm: false,
};

export const reducer = createReducer(
  initialState,
  on(EventsActions.loadEvents, (state) => state),
  on(EventsActions.loadEventsSuccess, (state, action) => ({
    ...state,
    events: [...action.events],
  })),
  on(EventsActions.loadEventsFailure, (state, action) => state),
  on(EventsActions.sortBy, (state, action) => ({
    ...state,
    sort: sortByReducer(state.sort, action.column),
  })),
  on(EventsActions.changeSearchText, (state, action) => ({
    ...state,
    searchText: action.searchText,
  })),
  on(EventsActions.duplicateEventFormShow, (state, action) => ({
    ...state,
    duplication: {
      eventId: action.eventId,
      seasonId: undefined,
      amount: 1,
    },
    showDuplicationForm: true,
  })),
  on(EventsActions.duplicateEventFormCancel, (state, action) => ({
    ...state,
    duplication: initialState.duplication,
    showDuplicationForm: false,
  })),
  on(EventsActions.duplicateEventFormSelectSeason, (state, action) => ({
    ...state,
    duplication: {
      ...state.duplication,
      seasonId: action.seasonId,
    },
  })),
  on(EventsActions.duplicateEventFormUpdateAmount, (state, action) => ({
    ...state,
    duplication: {
      ...state.duplication,
      amount: action.amount,
    },
  })),
  on(EventsActions.duplicateEventSuccess, (state, action) => ({
    ...state,
    events: [
      ...state.events,
      ...action.events.filter(
        (event) => event.season.id === state.events[0].season.id
      ),
    ],
    duplication: initialState.duplication,
    showDuplicationForm: false,
  }))
);

export const eventsFeature = createFeature({
  name: eventsFeatureKey,
  reducer,
});
