import {
  DuplicateEventGQL,
  ExportEventsQuery,
} from './../../graphql/generated';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  catchError,
  map,
  concatMap,
  withLatestFrom,
  filter,
  switchMap,
  tap,
} from 'rxjs/operators';
import { of } from 'rxjs';
import { EventsActions } from './events.actions';
import { EventsGQL, ExportEventsGQL } from 'src/app/graphql/generated';
import { routerNavigatedAction } from '@ngrx/router-store';
import { Event } from './events.types';
import { Store } from '@ngrx/store';
import * as authSelectors from '../auth/auth.selectors';
import * as eventsSelectors from './events.selectors';
import * as FileSaver from 'file-saver';
import * as ExcelJS from 'exceljs';
import { selectCurrentSeason } from '../seasons/seasons.selectors';
import { SeasonsActions } from '../seasons/seasons.actions';
import { MessageService } from 'primeng/api';

const excelMimeType =
  'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';

const colors = ['ffff00', 'ccffff', 'ff9900', '99cc00', '99ccff'];

const careDayColors = {
  alternative: '00b0f0',
  desired: 'ffff00',
  committed: 'e1126a',
};

const alphabet = Array.from({ length: 26 }, (_, i) =>
  String.fromCharCode(65 + i)
);

@Injectable()
export class EventsEffects {
  loadEvents$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(EventsActions.loadEvents),
      withLatestFrom(
        this.store.select(authSelectors.selectUserRole),
        this.store.select(authSelectors.selectUserOrganizer),
        this.store.select(selectCurrentSeason)
      ),
      concatMap(([action, role, organizer, season]) =>
        this.eventsQuery
          .fetch({
            where:
              role == 'organizer'
                ? {
                    organizer: { id: { equals: organizer?.id } },
                    season: {
                      id: {
                        equals: season?.id,
                      },
                    },
                  }
                : {
                    season: {
                      id: {
                        equals: season?.id,
                      },
                    },
                  },
          })
          .pipe(
            map((response) =>
              EventsActions.loadEventsSuccess({
                events: response.data.events as Event[],
              })
            ),
            catchError((error) =>
              of(EventsActions.loadEventsFailure({ error }))
            )
          )
      )
    );
  });

  loadEventsAfterNavigate$ = createEffect(() =>
    this.actions$.pipe(
      ofType(routerNavigatedAction),
      withLatestFrom(this.store.select(eventsSelectors.selectUrl)),
      filter(([action, url]) => url === '/veranstaltungen'),
      map(() => EventsActions.loadEvents())
    )
  );

  loadEventsAfterSeasonChanged$ = createEffect(() =>
    this.actions$.pipe(
      ofType(
        SeasonsActions.nextSeason,
        SeasonsActions.previousSeason,
        SeasonsActions.selectSeason
      ),
      map(() => EventsActions.loadEvents())
    )
  );

  exportEvents$ = createEffect(() =>
    this.actions$.pipe(
      ofType(EventsActions.exportEvents),
      withLatestFrom(this.store.select(selectCurrentSeason)),
      concatMap(([action, season]) =>
        this.exportEventsQuery
          .fetch({
            season: season?.id as string,
          })
          .pipe(
            switchMap(async (response) => {
              const workbook = this.buildExcelSheet(response.data);
              await this.saveExcelSheet(workbook);
              return EventsActions.exportEventsSuccess();
            }),
            catchError((error) =>
              of(EventsActions.exportEventsFailure({ error }))
            )
          )
      )
    )
  );

  duplicateEvent$ = createEffect(() =>
    this.actions$.pipe(
      ofType(EventsActions.duplicateEvent),
      withLatestFrom(this.store.select(eventsSelectors.selectDuplication)),
      concatMap(([action, duplication]) =>
        duplication.seasonId == undefined
          ? of(EventsActions.duplicateEventFormRejectedSelectReason())
          : this.duplicateEventMutation
              .mutate({
                seasonId: duplication.seasonId as string,
                eventId: duplication.eventId as string,
                amount: duplication.amount,
              })
              .pipe(
                map((response) => {
                  return EventsActions.duplicateEventSuccess({
                    events: response.data?.duplicateEvent as Event[],
                  });
                }),
                catchError((error) =>
                  of(EventsActions.duplicateEventFailure({ error }))
                )
              )
      )
    )
  );

  showToastAfterDuplicateEventsSuccess$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(EventsActions.duplicateEventSuccess),
        tap((action) =>
          this.toastService.add({
            summary: `Die Veranstaltung wurde vervielfältigt`,
            severity: 'success',
          })
        )
      );
    },
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private store: Store,
    private toastService: MessageService,
    private eventsQuery: EventsGQL,
    private exportEventsQuery: ExportEventsGQL,
    private duplicateEventMutation: DuplicateEventGQL
  ) {}

  private async saveExcelSheet(workbook: ExcelJS.Workbook) {
    const buffer = await workbook.xlsx.writeBuffer();

    var blob = new Blob([buffer], {
      type: excelMimeType,
    });

    FileSaver.saveAs(blob, 'Veranstaltungen 2024.xlsx');
  }

  private buildExcelSheet(result: ExportEventsQuery) {
    const workbook = new ExcelJS.Workbook();
    const eventsWorksheet = workbook.addWorksheet('Veranstaltungen', {
      properties: {
        defaultColWidth: 20,
      },
    });

    const sortedHolidays =
      result.holidays?.sort((a, b) => {
        const aPeriods = [...(a.periods ?? [])].sort((x, y) =>
          (x.startDate as string).localeCompare(y.startDate as string)
        );

        const bPeriods = [...(b.periods ?? [])].sort((x, y) =>
          (x.startDate as string).localeCompare(y.startDate as string)
        );

        return (aPeriods[0].startDate as string).localeCompare(
          bPeriods[0].startDate as string
        );
      }) ?? [];

    const sortedEvents =
      [...(result.events ?? [])].sort((a, b) => {
        const organizerComparsion = (a.organizer?.name ?? '')?.localeCompare(
          b.organizer?.name ?? ''
        );

        if (organizerComparsion !== 0) {
          return organizerComparsion;
        }

        return (a.name ?? '')?.localeCompare(b.name ?? '');
      }) ?? [];

    this.buildHolidayColumns(eventsWorksheet, sortedHolidays, sortedEvents);
    this.buildEventColumns(eventsWorksheet);
    this.buildCareDayLegend(eventsWorksheet);

    const startRow = 6;
    const holidyColumnCount =
      sortedHolidays?.reduce(
        (sum, next) => sum + (next.periods?.length ?? 0),
        0
      ) ?? 0;

    const periodIndex = [...sortedHolidays]
      .sort((a, b) => {
        const aPeriods = [...(a.periods ?? [])].sort((x, y) =>
          (x.startDate as string).localeCompare(y.startDate as string)
        );

        const bPeriods = [...(b.periods ?? [])].sort((x, y) =>
          (x.startDate as string).localeCompare(y.startDate as string)
        );

        return (aPeriods[0].startDate as string).localeCompare(
          bPeriods[0].startDate as string
        );
      })
      ?.reduce(
        (sum, holiday) => [
          ...sum,
          ...(holiday.periods?.map((period) => period.id) ?? []),
        ],
        new Array<string>()
      )
      .reduce(
        (periodIds, periodId, periodIndex) => ({
          ...periodIds,
          [periodId]: periodIndex,
        }),
        {} as { [index: string]: number }
      );

    for (let index = 0; index < holidyColumnCount + 4; index++) {
      const cellTop = eventsWorksheet.getCell(5, index + 1);
      cellTop.border = {
        top: {
          style: 'thin',
          color: {
            argb: '000000',
          },
        },
        bottom: {
          style: 'thin',
          color: {
            argb: '000000',
          },
        },
      };
    }

    for (const [eventIndex, event] of sortedEvents.entries()) {
      const row = eventsWorksheet.getRow(startRow + eventIndex);
      row.values = [event.organizer?.name, event.name, event.participantLimit];

      let dailyRatesCell: ExcelJS.Cell | undefined;

      if (event.carePeriodDesired) {
        const countDesired = event.carePeriodDesired?.careDays?.length;
        const columnIndex =
          periodIndex[event.carePeriodDesired?.period?.id as string];

        if (columnIndex !== undefined) {
          const daysCell = eventsWorksheet.getCell(
            startRow + eventIndex,
            5 + columnIndex
          );
          daysCell.value = countDesired;
          daysCell.style.fill = {
            pattern: 'solid',
            type: 'pattern',
            fgColor: {
              argb: careDayColors.desired,
            },
          };

          dailyRatesCell = daysCell;
        }
      }

      if (event.carePeriodAlternative) {
        const countAlternative = event.carePeriodAlternative?.careDays?.length;
        const columnIndex =
          periodIndex[event.carePeriodAlternative?.period?.id as string];

        if (columnIndex !== undefined) {
          const daysCell = eventsWorksheet.getCell(
            startRow + eventIndex,
            5 + columnIndex
          );
          daysCell.value = countAlternative;
          daysCell.style.fill = {
            pattern: 'solid',
            type: 'pattern',
            fgColor: {
              argb: careDayColors.alternative,
            },
          };
        }
      }

      if (event.carePeriodCommitted) {
        const countAlternative = event.carePeriodCommitted?.careDays?.length;
        const columnIndex =
          periodIndex[event.carePeriodCommitted?.period?.id as string];

        if (columnIndex !== undefined) {
          const daysCell = eventsWorksheet.getCell(
            startRow + eventIndex,
            5 + columnIndex
          );
          daysCell.value = countAlternative;
          const border: ExcelJS.Border = {
            color: { argb: careDayColors.committed },
            style: 'medium',
          };
          daysCell.style.border = {
            top: border,
            bottom: border,
            left: border,
            right: border,
          };

          dailyRatesCell = daysCell;
        }
      }

      if (dailyRatesCell) {
        const rowIndex = startRow + eventIndex;
        const daysCell = eventsWorksheet.getCell(rowIndex, 4);
        const formula = `C${rowIndex}*${dailyRatesCell.address}`;
        const result =
          (dailyRatesCell.value as number) * (event.participantLimit ?? 0);

        daysCell.value = {
          formula: formula,
          result,
          date1904: false,
        };
      }
    }

    return workbook;
  }

  buildCareDayLegend(eventsWorksheet: ExcelJS.Worksheet) {
    const cellDesired = eventsWorksheet.getCell(2, 1);
    cellDesired.value = 'Wunschtermin';

    cellDesired.font = {
      bold: true,
    };

    cellDesired.fill = {
      type: 'pattern',
      pattern: 'solid',
      fgColor: {
        argb: careDayColors.desired,
      },
    };

    const cellAlternative = eventsWorksheet.getCell(3, 1);
    cellAlternative.value = 'Ausweichtermin';

    cellAlternative.font = {
      bold: true,
    };

    cellAlternative.fill = {
      type: 'pattern',
      pattern: 'solid',
      fgColor: {
        argb: careDayColors.alternative,
      },
    };

    const cellCommitted = eventsWorksheet.getCell(1, 1);
    cellCommitted.value = 'Termin';

    cellCommitted.font = {
      bold: true,
    };

    cellCommitted.fill = {
      type: 'pattern',
      pattern: 'solid',
      fgColor: {
        argb: careDayColors.committed,
      },
    };
  }

  private buildEventColumns(eventsWorksheet: ExcelJS.Worksheet) {
    const startColumn = 5;
    const startRow = 2;

    const columns = [
      'Veranstalter',
      'Titel der Abenteuerwoche',
      'Teilnehmer max',
      'Tagessätze',
    ];

    columns.forEach((column, columnIndex) => {
      const cell = eventsWorksheet.getCell(5, columnIndex + 1);

      cell.value = column;
      cell.font = {
        bold: true,
      };
      cell.fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: {
          argb: 'ffcc99',
        },
      };
    });
  }

  private buildHolidayColumns(
    eventsWorksheet: ExcelJS.Worksheet,
    holidays: ExportEventsQuery['holidays'],
    events: ExportEventsQuery['events']
  ) {
    let offset = 0;
    const eventCount = events?.length ?? 0;

    holidays?.forEach((holiday, holidayIndex) => {
      const startColumn = 5;
      const startRow = 2;

      const holidayNameCell = eventsWorksheet.getCell(
        startRow,
        startColumn + holidayIndex + offset
      );

      holidayNameCell.value = holiday.name;
      const periodCount = holiday.periods?.length ?? 0;

      eventsWorksheet.mergeCells(
        2,
        startColumn + holidayIndex + offset,
        2,
        startColumn + holidayIndex + offset + periodCount - 1
      );

      holidayNameCell.style.font = {
        bold: true,
      };

      holidayNameCell.style.fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: {
          argb: colors[holidayIndex % colors.length],
        },
      };

      holiday.periods?.forEach((period, periodIndex) => {
        const weekNumberCell = eventsWorksheet.getCell(
          startRow + 1,
          startColumn + holidayIndex + offset + periodIndex
        );

        weekNumberCell.value = `${periodIndex + 1}. Woche`;

        weekNumberCell.style.fill = {
          type: 'pattern',
          pattern: 'solid',
          fgColor: {
            argb: colors[holidayIndex % colors.length],
          },
        };

        const weekDateCell = eventsWorksheet.getCell(
          startRow + 2,
          startColumn + holidayIndex + offset + periodIndex
        );

        const startDate = period.startDate.split('-');
        const endDate = period.endDate.split('-');

        if (period.startDate == period.endDate) {
          weekDateCell.value = `${startDate[2]}.${startDate[1]}`;
        } else if (startDate[1] == endDate[1]) {
          weekDateCell.value = `${startDate[2]}. - ${endDate[2]}.${endDate[1]}`;
        } else {
          weekDateCell.value = `${startDate[2]}.${startDate[1]}. - ${endDate[2]}.${endDate[1]}`;
        }

        weekDateCell.style.fill = {
          type: 'pattern',
          pattern: 'solid',
          fgColor: {
            argb: colors[holidayIndex % colors.length],
          },
        };

        weekDateCell.style.font = {
          bold: true,
        };

        const columnIndex = startColumn + holidayIndex + offset + periodIndex;

        const placeHolderCell = eventsWorksheet.getCell(
          startRow + 3,
          columnIndex
        );

        placeHolderCell.style.fill = {
          type: 'pattern',
          pattern: 'solid',
          fgColor: {
            argb: colors[holidayIndex % colors.length],
          },
        };

        const daysSumCell = eventsWorksheet.getCell(
          startRow + 3 + eventCount + 1,
          columnIndex
        );

        const daysSum = events?.filter(
          (event) =>
            event.carePeriodDesired?.period?.id === period.id ||
            event.carePeriodAlternative?.period?.id === period.id
        ).length;

        const columnName = alphabet[columnIndex - 1];

        const rangeFrom = `${columnName}6`;
        const rangeTo = `${columnName}${startRow + 3 + eventCount}`;
        const range = `${rangeFrom}:${rangeTo}`;
        const formula = `COUNTIF( ${range}, "<>" )`;

        daysSumCell.value = {
          result: daysSum,
          formula: formula,
          date1904: false,
          ref: daysSumCell.address,
          shareType: 'array',
        } as any;

        daysSumCell.style.fill = {
          type: 'pattern',
          pattern: 'solid',
          fgColor: {
            argb: colors[0],
          },
        };
      });

      offset += periodCount - 1;
    });
  }
}
