import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { Event } from './events.types';

export const EventsActions = createActionGroup({
  source: 'Events',
  events: {
    'Load Events': emptyProps(),
    'Load Events Success': props<{ events: Event[] }>(),
    'Load Events Failure': props<{ error: unknown }>(),
    'Sort By': props<{ column: string }>(),
    'Export Events': emptyProps(),
    'Export Events Success': emptyProps(),
    'Export Events Failure': props<{ error: unknown }>(),

    'Change Search Text': props<{ searchText: string }>(),

    'Duplicate Event Form Show': props<{
      eventId: string;
    }>(),

    'Duplicate Event Form Cancel': emptyProps(),
    'Duplicate Event Form Rejected Select Reason': emptyProps(),

    'Duplicate Event Form Select Season': props<{
      seasonId: string;
    }>(),

    'Duplicate Event Form Update Amount': props<{
      amount: number;
    }>(),

    'Duplicate Event': emptyProps(),
    'Duplicate Event Success': props<{
      events: Event[];
    }>(),
    'Duplicate Event Failure': props<{ error: unknown }>(),
  },
});
