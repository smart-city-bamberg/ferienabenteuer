export interface Event {
  id: string;
  season: { id: string; year: number };
  name: string;
  description: string;
  status: string;
  picture: {
    url: string;
  };
  pictureRights: string;
  registrationDeadline: Date;
  contactPersonFirstname: string;
  contactPersonSurname: string;
  contactPersonSalutation: string;
  emergencyPhone: string;
  careTimeStart: string;
  careTimeEnd: string;
  ageMinimum: number;
  ageMaximum: number;
  hints: string;
  meals: string;
  carePeriodDesired: CarePeriod;
  carePeriodAlternative: CarePeriod;
  carePeriodCommitted: CarePeriod;
  costs: number;
  locationCity: string;
  locationZipcode: string;
  locationStreet: string;
  participantMinimum: number | null;
  participantLimit: number | null;
  waitingListLimit: number | null;
  bookedTicketCountParticipant: number;
  bookedTicketCountWaitinglist: number;
  organizer: { name: string };
}

export interface CarePeriod {
  id: string;
  period: Period;
}

export interface Period {
  id: string;
  startDate: Date;
  endDate: Date;
  holiday: {
    id: string;
    name: string;
  };
}

export interface Organizer {
  id: string;
  name: string;
}
