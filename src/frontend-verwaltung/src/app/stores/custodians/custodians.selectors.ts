import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromCustodians from './custodians.reducer';
import { Custodian } from './custodians.types';
import { stringComparer } from '../sorting';

export const selectCustodiansState =
  createFeatureSelector<fromCustodians.State>(
    fromCustodians.custodiansFeatureKey
  );

export const selectCustodians = createSelector(
  selectCustodiansState,
  (state) => state.custodians
);

export const selectSortBy = createSelector(
  selectCustodiansState,
  (state) => state.sort
);

export const selectSearchText = createSelector(
  selectCustodiansState,
  (state) => state.searchText
);

export const selectShowEmailForm = createSelector(
  selectCustodiansState,
  (state) => state.showEmailForm
);

export const selectEmailContent = createSelector(
  selectCustodiansState,
  (state) => state.emailContent
);

export const selectEmailSubject = createSelector(
  selectCustodiansState,
  (state) => state.emailSubject
);

const sorters: {
  [index: string]: (a: Custodian, b: Custodian) => number;
} = {
  name: stringComparer((e) => `${e.surname}, ${e.firstname}`),
  address: stringComparer(
    (e) =>
      `${e.addressStreet} ${e.addressHouseNumber} ${e.addressZip} ${e.addressCity}`
  ),
  phone: stringComparer((e) => e.phone),
  email: stringComparer((e) => e.user?.email ?? ''),
};

export const selectCustodiansSorted = createSelector(
  selectCustodians,
  selectSortBy,
  selectSearchText,
  (custodians, sortBy, searchText) => {
    const filteredCustodians = custodians.filter((custodian) =>
      JSON.stringify(custodian).toLowerCase().includes(searchText.toLowerCase())
    );

    return sorters[sortBy.by]
      ? [...filteredCustodians].sort(
          (a, b) => (sortBy.descending ? -1 : 1) * sorters[sortBy.by](a, b)
        )
      : filteredCustodians;
  }
);
