import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { Custodian } from './custodians.types';

export const CustodiansActions = createActionGroup({
  source: 'Custodians',
  events: {
    'Load Custodians': emptyProps(),
    'Load Custodians Success': props<{ custodians: Custodian[] }>(),
    'Load Custodians Failure': props<{ error: unknown }>(),

    'Sort By': props<{ column: string }>(),
    'Change Search Text': props<{ searchText: string }>(),

    'Update Email Content': props<{ content: string }>(),
    'Update Email Subject': props<{ subject: string }>(),
    'Send Email Show': emptyProps(),
    'Send Email': emptyProps(),
    'Send Email Cancel': emptyProps(),
    'Send Email Success': emptyProps(),
    'Send Email Failure': props<{ error: unknown }>(),

    'Generate Gdpr Information': props<{
      custodianId: string;
    }>(),
    'Generate Gdpr Information Success': emptyProps(),
    'Generate Gdpr Information Failure': props<{ error: unknown }>(),
  },
});
