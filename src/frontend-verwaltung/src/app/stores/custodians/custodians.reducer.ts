import { createFeature, createReducer, on } from '@ngrx/store';
import { CustodiansActions } from './custodians.actions';
import { Custodian } from './custodians.types';
import { SortByState, defaultSortByState, sortByReducer } from '../sorting';

export const custodiansFeatureKey = 'custodians';

export interface State {
  custodians: Custodian[];
  sort: SortByState;
  searchText: string;
  showEmailForm: boolean;
  emailContent: string;
  emailSubject: string;
}

export const initialState: State = {
  custodians: [],
  sort: { ...defaultSortByState, by: 'name' },
  searchText: '',
  showEmailForm: false,
  emailContent: '',
  emailSubject: '',
};

export const reducer = createReducer(
  initialState,
  on(CustodiansActions.loadCustodiansSuccess, (state, action) => ({
    ...state,
    custodians: [...action.custodians],
  })),
  on(CustodiansActions.sortBy, (state, action) => ({
    ...state,
    sort: sortByReducer(state.sort, action.column),
  })),
  on(CustodiansActions.changeSearchText, (state, action) => ({
    ...state,
    searchText: action.searchText,
  })),
  on(CustodiansActions.updateEmailContent, (state, action) => ({
    ...state,
    emailContent: action.content,
  })),
  on(CustodiansActions.updateEmailSubject, (state, action) => ({
    ...state,
    emailSubject: action.subject,
  })),
  on(
    CustodiansActions.sendEmailCancel,
    CustodiansActions.sendEmailSuccess,
    (state, action) => ({
      ...state,
      showEmailForm: false,
      emailContent: '',
      emailSubject: '',
    })
  ),
  on(CustodiansActions.sendEmailShow, (state, action) => ({
    ...state,
    showEmailForm: true,
    emailContent: '',
    emailSubject: '',
  }))
);

export const custodiansFeature = createFeature({
  name: custodiansFeatureKey,
  reducer,
});
