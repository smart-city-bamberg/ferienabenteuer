import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import * as fromCustodians from './custodians.reducer';
import { EffectsModule } from '@ngrx/effects';
import { CustodiansEffects } from './custodians.effects';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StoreModule.forFeature(
      fromCustodians.custodiansFeatureKey,
      fromCustodians.reducer
    ),
    EffectsModule.forFeature([CustodiansEffects]),
  ],
})
export class CustodiansModule {}
