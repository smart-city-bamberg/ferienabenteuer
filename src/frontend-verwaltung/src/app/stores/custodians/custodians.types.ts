export interface Custodian {
  id: string;
  salutation: string;
  firstname: string;
  surname: string;
  phone: string;
  emergencyPhone: string;
  addressCity: string;
  addressStreet: string;
  addressHouseNumber: string;
  addressZip: string;
  addressAdditional: string;
  displayName: string;
  children: Child[];
  user: {
    email: string;
    createdAt: Date;
  } | null;
}

export interface Child {
  id: string;
  firstname: string;
  surname: string;
}
