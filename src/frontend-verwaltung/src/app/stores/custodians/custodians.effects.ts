import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  catchError,
  map,
  concatMap,
  withLatestFrom,
  filter,
  tap,
  switchMap,
} from 'rxjs/operators';
import { of } from 'rxjs';
import { CustodiansActions } from './custodians.actions';
import {
  CustodiansGQL,
  GdprInformationGQL,
  SendEmailToAllCustodiansGQL,
} from 'src/app/graphql/generated';
import { Custodian } from './custodians.types';
import { routerNavigatedAction } from '@ngrx/router-store';
import { Store } from '@ngrx/store';
import { selectEmailContent, selectEmailSubject } from './custodians.selectors';
import { MessageService } from 'primeng/api';
import FileSaver from 'file-saver';

@Injectable()
export class CustodiansEffects {
  loadCustodians$ = createEffect(() =>
    this.actions$.pipe(
      ofType(CustodiansActions.loadCustodians),
      concatMap(() =>
        this.custodiansQuery.fetch().pipe(
          map((response) =>
            CustodiansActions.loadCustodiansSuccess({
              custodians: response.data.custodians as Custodian[],
            })
          ),
          catchError((error) =>
            of(CustodiansActions.loadCustodiansFailure({ error }))
          )
        )
      )
    )
  );

  sendEmailToAllCustodians$ = createEffect(() =>
    this.actions$.pipe(
      ofType(CustodiansActions.sendEmail),
      withLatestFrom(
        this.store.select(selectEmailSubject),
        this.store.select(selectEmailContent)
      ),
      concatMap(([action, subject, content]) =>
        this.sendEmailToAllCustodiansMutation.mutate({ subject, content }).pipe(
          map((response) => CustodiansActions.sendEmailSuccess()),
          catchError((error) =>
            of(CustodiansActions.sendEmailFailure({ error }))
          )
        )
      )
    )
  );

  showToastAfterSendEmailToAllCustodiansSuccess$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(CustodiansActions.sendEmailSuccess),
        tap((action) =>
          this.toastService.add({
            summary: 'Es wurde eine E-Mail an alle Sorgeberechtigte geschickt',
            severity: 'success',
          })
        )
      );
    },
    { dispatch: false }
  );

  loadCustodiansAfterNavigate$ = createEffect(() =>
    this.actions$.pipe(
      ofType(routerNavigatedAction),
      filter((action) =>
        action.payload.routerState.url.startsWith('/sorgeberechtigte')
      ),
      map(() => CustodiansActions.loadCustodians())
    )
  );

  generateGdprInformation$ = createEffect(() =>
    this.actions$.pipe(
      ofType(CustodiansActions.generateGdprInformation),
      switchMap((action) =>
        this.gdprInformationQuery
          .fetch({
            custodianId: action.custodianId,
          })
          .pipe(
            map((response) => {
              const json = JSON.stringify(response.data.custodian, null, 2);
              const blob = new Blob([json], {
                type: 'text/plain;charset=utf-8',
              });

              const fileName = `DSGVO Auskunft (${response.data.custodian?.surname}, ${response.data.custodian?.firstname}).json`;
              FileSaver.saveAs(blob, fileName);

              return CustodiansActions.generateGdprInformationSuccess();
            }),
            catchError((error) => {
              console.error(error);
              return of(
                CustodiansActions.generateGdprInformationFailure({ error })
              );
            })
          )
      )
    )
  );

  constructor(
    private actions$: Actions,
    private store: Store,
    private toastService: MessageService,
    private custodiansQuery: CustodiansGQL,
    private gdprInformationQuery: GdprInformationGQL,
    private sendEmailToAllCustodiansMutation: SendEmailToAllCustodiansGQL
  ) {}
}
