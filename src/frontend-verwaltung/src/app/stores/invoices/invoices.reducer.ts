import { createFeature, createReducer, on } from '@ngrx/store';
import { InvoicesActions } from './invoices.actions';
import { Event, Invoice } from './invoices.types';
import { SortByState, defaultSortByState } from '../sorting';

export const invoicesFeatureKey = 'invoices';

export interface State {
  events: Event[];
  invoices: Invoice[];
  sort: SortByState;
  searchText: string;
}

export const initialState: State = {
  events: [],
  invoices: [],
  sort: {
    ...defaultSortByState,
    by: 'carePeriodCommitted',
  },
  searchText: '',
};

export const reducer = createReducer(
  initialState,
  on(InvoicesActions.loadInvoicesSuccess, (state, action) => ({
    ...state,
    events: [...action.events],
    invoices: [...action.invoices],
  })),
  on(InvoicesActions.generateInvoiceSuccess, (state, action) => ({
    ...state,
    invoices: [
      ...state.invoices.filter(
        (invoice) => invoice.event.id !== action.invoice.event.id
      ),
      action.invoice,
    ],
  })),
  on(InvoicesActions.sortBy, (state, action) => ({
    ...state,
    sort:
      state.sort.by === action.column
        ? { by: action.column, descending: !state.sort.descending }
        : {
            by: action.column,
            descending: false,
          },
  })),
  on(InvoicesActions.changeSearchText, (state, action) => ({
    ...state,
    searchText: action.searchText,
  }))
);

export const invoicesFeature = createFeature({
  name: invoicesFeatureKey,
  reducer,
});
