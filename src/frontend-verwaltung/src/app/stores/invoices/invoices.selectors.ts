import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromInvoices from './invoices.reducer';
import { Event, Invoice } from './invoices.types';
import { booleanComparer, dateComparer, stringComparer } from '../sorting';

export interface EventWithInvoice extends Event {
  invoice: Invoice | undefined;
  isFinished: boolean;
}

export const selectInvoicesState = createFeatureSelector<fromInvoices.State>(
  fromInvoices.invoicesFeatureKey
);

export const selectEvents = createSelector(
  selectInvoicesState,
  (state) => state.events
);
export const selectInvoices = createSelector(
  selectInvoicesState,
  (state) => state.invoices
);

export const selectEventsWithInvoices = createSelector(
  selectEvents,
  selectInvoices,
  (events, invoices) =>
    events.map((event) => ({
      ...event,
      invoice: invoices.find((invoice) => invoice.event.id === event.id),
      isFinished: event.carePeriodCommitted.careDays.every(
        (careDay) => new Date(careDay.day).getTime() < new Date().getTime()
      ),
    }))
);

export const selectSortBy = createSelector(
  selectInvoicesState,
  (state) => state.sort
);

export const selectSearchText = createSelector(
  selectInvoicesState,
  (state) => state.searchText
);

const sorters: {
  [index: string]: (a: EventWithInvoice, b: EventWithInvoice) => number;
} = {
  isFinished: booleanComparer((e) => e.isFinished),
  organizer: stringComparer((e) => e.organizer.name),
  name: stringComparer((e) => e.name),
  createdAt: dateComparer((e) =>
    e.invoice ? new Date(e.invoice.createdAt) : undefined
  ),
  carePeriodCommitted: dateComparer(
    (e) => new Date(e.carePeriodCommitted.careDays[0].day)
  ),
};

export const selectEventsWithIinvoicesSorted = createSelector(
  selectEventsWithInvoices,
  selectSortBy,
  selectSearchText,
  (bookings, sortBy, searchText) => {
    const filteredEvents = bookings.filter((event) =>
      JSON.stringify({
        code: event.name,
      })
        .toLowerCase()
        .includes(searchText.toLowerCase())
    );

    return sorters[sortBy.by]
      ? filteredEvents.sort(
          (a, b) => (sortBy.descending ? -1 : 1) * sorters[sortBy.by](a, b)
        )
      : filteredEvents;
  }
);
