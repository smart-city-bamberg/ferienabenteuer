export interface Event {
  id: string;
  name: string;
  organizer: {
    id: string;
    name: string;
  };
  carePeriodCommitted: CarePeriod;
}

export interface Invoice {
  id: string;
  createdAt: string;
  event: {
    id: string;
  };
  document: {
    filename: string;
    url: string;
  } | null;
}

export interface CarePeriod {
  id: string;
  period: Period;
  careDays: CareDay[];
}

export interface CareDay {
  day: string;
}

export interface Period {
  id: string;
  startDate: Date;
  endDate: Date;
  holiday: {
    id: string;
    name: string;
  };
}
