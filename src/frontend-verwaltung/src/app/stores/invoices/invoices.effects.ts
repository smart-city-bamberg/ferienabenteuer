import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  catchError,
  map,
  concatMap,
  withLatestFrom,
  filter,
  tap,
} from 'rxjs/operators';
import { of } from 'rxjs';
import { InvoicesActions } from './invoices.actions';
import { Store } from '@ngrx/store';
import { GenerateInvoiceGQL, InvoicesGQL } from 'src/app/graphql/generated';
import { selectUserOrganizer } from '../auth/auth.selectors';
import { Event, Invoice } from './invoices.types';
import { SeasonsActions } from '../seasons/seasons.actions';
import { routerNavigatedAction } from '@ngrx/router-store';
import { MessageService } from 'primeng/api';

@Injectable()
export class InvoicesEffects {
  loadInvoices$ = createEffect(() =>
    this.actions$.pipe(
      ofType(InvoicesActions.loadInvoices),
      withLatestFrom(this.store.select(selectUserOrganizer)),
      filter(([action, organizer]) => organizer !== undefined),
      concatMap(([action, organizer]) =>
        this.invoicesQuery
          .fetch({
            organizerId: organizer?.id as string,
          })
          .pipe(
            map((response) =>
              InvoicesActions.loadInvoicesSuccess({
                events: response.data.events as Event[],
                invoices: response.data.invoices as Invoice[],
              })
            ),
            catchError((error) =>
              of(InvoicesActions.loadInvoicesFailure({ error }))
            )
          )
      )
    )
  );

  generateInvoice$ = createEffect(() =>
    this.actions$.pipe(
      ofType(InvoicesActions.generateInvoice),
      concatMap((action) =>
        this.generateInvoiceMutation
          .mutate({
            eventId: action.eventId,
          })
          .pipe(
            map((response) =>
              InvoicesActions.generateInvoiceSuccess({
                invoice: response.data?.generateInvoice as Invoice,
              })
            ),
            catchError((error) =>
              of(InvoicesActions.generateInvoiceFailure({ error }))
            )
          )
      )
    )
  );

  loadInvoicesAfterSeasonChanged$ = createEffect(() =>
    this.actions$.pipe(
      ofType(
        SeasonsActions.nextSeason,
        SeasonsActions.previousSeason,
        SeasonsActions.selectSeason
      ),
      map(() => InvoicesActions.loadInvoices())
    )
  );

  loadInvoicesAfterNavigate$ = createEffect(() =>
    this.actions$.pipe(
      ofType(routerNavigatedAction),
      filter((action) =>
        action.payload.routerState.url.startsWith('/abrechnung')
      ),
      map(() => InvoicesActions.loadInvoices())
    )
  );

  showToastAfterRegisterPaymentSuccess$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(InvoicesActions.generateInvoiceSuccess),
        tap((action) =>
          this.toastService.add({
            summary: 'Abrechnung wurde erzeugt.',
            severity: 'success',
          })
        )
      );
    },
    { dispatch: false }
  );

  showToastAfterRegisterPaymentFailure$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(InvoicesActions.generateInvoiceFailure),
        tap((action) =>
          this.toastService.add({
            summary: 'Abrechnung konnte nicht erzeugt werden.',
            severity: 'error',
          })
        )
      );
    },
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private store: Store,
    private toastService: MessageService,
    private invoicesQuery: InvoicesGQL,
    private generateInvoiceMutation: GenerateInvoiceGQL
  ) {}
}
