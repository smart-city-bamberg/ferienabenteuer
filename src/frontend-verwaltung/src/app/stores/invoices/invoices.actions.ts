import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { Event, Invoice } from './invoices.types';

export const InvoicesActions = createActionGroup({
  source: 'Invoices',
  events: {
    'Load Invoices': emptyProps(),
    'Load Invoices Success': props<{ events: Event[]; invoices: Invoice[] }>(),
    'Load Invoices Failure': props<{ error: unknown }>(),

    'Change Search Text': props<{ searchText: string }>(),
    'Sort By': props<{ column: string }>(),

    'Generate Invoice': props<{ eventId: string }>(),
    'Generate Invoice Success': props<{ invoice: Invoice }>(),
    'Generate Invoice Failure': props<{ error: unknown }>(),
  },
});
