import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import * as fromInvoices from './invoices.reducer';
import { EffectsModule } from '@ngrx/effects';
import { InvoicesEffects } from './invoices.effects';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StoreModule.forFeature(fromInvoices.invoicesFeatureKey, fromInvoices.reducer),
    EffectsModule.forFeature([InvoicesEffects])
  ]
})
export class InvoicesModule { }
