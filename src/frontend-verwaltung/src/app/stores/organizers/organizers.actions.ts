import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { Organizer } from './organizers.types';

export const OrganizersActions = createActionGroup({
  source: 'Organizers',
  events: {
    'Load Organizers': emptyProps(),
    'Load Organizers Success': props<{ organizers: Organizer[] }>(),
    'Load Organizers Failure': props<{ error: unknown }>(),

    'Sort By': props<{ column: string }>(),
    'Change Search Text': props<{ searchText: string }>(),
  },
});
