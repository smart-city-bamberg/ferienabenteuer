import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, map, concatMap, filter } from 'rxjs/operators';
import { Observable, EMPTY, of } from 'rxjs';
import { OrganizersActions } from './organizers.actions';
import { OrganizersGQL } from 'src/app/graphql/generated';
import { Organizer } from './organizers.types';
import { AuthActions } from '../auth/auth.actions';
import { routerNavigatedAction } from '@ngrx/router-store';

@Injectable()
export class OrganizersEffects {
  loadOrganizers$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(OrganizersActions.loadOrganizers),
      concatMap(() =>
        this.organizersQuery.fetch().pipe(
          map((response) =>
            OrganizersActions.loadOrganizersSuccess({
              organizers: response.data.organizers as Organizer[],
            })
          ),
          catchError((error) =>
            of(OrganizersActions.loadOrganizersFailure({ error }))
          )
        )
      )
    );
  });

  loadOrganizersAfterUserLoaded$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AuthActions.loadUserSuccess),
      map((action) => OrganizersActions.loadOrganizers())
    );
  });

  loadOrganizersAfterNavigate$ = createEffect(() =>
    this.actions$.pipe(
      ofType(routerNavigatedAction),
      filter((action) =>
        action.payload.routerState.url.startsWith('/veranstalter')
      ),
      map(() => OrganizersActions.loadOrganizers())
    )
  );

  constructor(
    private actions$: Actions,
    private organizersQuery: OrganizersGQL
  ) {}
}
