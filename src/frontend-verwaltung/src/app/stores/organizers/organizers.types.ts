export interface Organizer {
  id: string;
  name: string;
  contactPersonFirstname: string;
  contactPersonSurname: string;
  email: string;
  phone: string;
  addressCity: string;
  addressStreet: string;
  addressZip: string;
}
