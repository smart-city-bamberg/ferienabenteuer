import { createFeature, createReducer, on } from '@ngrx/store';
import { OrganizersActions } from './organizers.actions';
import { Organizer } from './organizers.types';
import { SortByState, defaultSortByState, sortByReducer } from '../sorting';

export const organizersFeatureKey = 'organizers';

export interface State {
  organizers: Organizer[];
  sort: SortByState;
  searchText: string;
}

export const initialState: State = {
  organizers: [],
  sort: { ...defaultSortByState, by: 'name' },
  searchText: '',
};

export const reducer = createReducer(
  initialState,
  on(OrganizersActions.loadOrganizersSuccess, (state, action) => ({
    ...state,
    organizers: action.organizers,
  })),
  on(OrganizersActions.sortBy, (state, action) => ({
    ...state,
    sort: sortByReducer(state.sort, action.column),
  })),
  on(OrganizersActions.changeSearchText, (state, action) => ({
    ...state,
    searchText: action.searchText,
  }))
);

export const organizersFeature = createFeature({
  name: organizersFeatureKey,
  reducer,
});
