import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromOrganizers from './organizers.reducer';
import { Organizer } from './organizers.types';
import { stringComparer } from '../sorting';

export const selectOrganizersState =
  createFeatureSelector<fromOrganizers.State>(
    fromOrganizers.organizersFeatureKey
  );

export const selectOrganizers = createSelector(
  selectOrganizersState,
  (state) => state.organizers
);

export const selectSortBy = createSelector(
  selectOrganizersState,
  (state) => state.sort
);

export const selectSearchText = createSelector(
  selectOrganizersState,
  (state) => state.searchText
);

const sorters: {
  [index: string]: (a: Organizer, b: Organizer) => number;
} = {
  name: stringComparer((e) => e.name),
  address: stringComparer(
    (e) => `${e.addressStreet} ${e.addressZip} ${e.addressCity}`
  ),
  phone: stringComparer((e) => e.phone),
  email: stringComparer((e) => e.email),
  contact: stringComparer(
    (e) => `${e.contactPersonSurname}, ${e.contactPersonFirstname}`
  ),
};

export const selectOrganizersSorted = createSelector(
  selectOrganizers,
  selectSortBy,
  selectSearchText,
  (organizers, sortBy, searchText) => {
    const filteredDiscounts = organizers.filter((organizer) =>
      JSON.stringify(organizer).toLowerCase().includes(searchText.toLowerCase())
    );

    return sorters[sortBy.by]
      ? [...filteredDiscounts].sort(
          (a, b) => (sortBy.descending ? -1 : 1) * sorters[sortBy.by](a, b)
        )
      : filteredDiscounts;
  }
);
