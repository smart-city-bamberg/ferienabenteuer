import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import * as fromOrganizers from './organizers.reducer';
import { EffectsModule } from '@ngrx/effects';
import { OrganizersEffects } from './organizers.effects';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StoreModule.forFeature(fromOrganizers.organizersFeatureKey, fromOrganizers.reducer),
    EffectsModule.forFeature([OrganizersEffects])
  ]
})
export class OrganizersModule { }
