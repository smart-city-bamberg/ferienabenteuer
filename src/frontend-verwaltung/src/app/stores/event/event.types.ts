export interface Event {
  id: string;
  name: string;
  status: string;

  description: string;

  picture: { url: string } | null;
  pictureRights: string;

  hints: string;
  meals: string;

  categories: { id: string }[];

  carePeriodDesired: CarePeriod;
  carePeriodAlternative: CarePeriod;
  carePeriodCommitted: CarePeriod;

  careTimeStart: string;
  careTimeEnd: string;

  ageMinimum: number;
  ageMaximum: number;

  contactPerson: string;
  emergencyPhone: string;

  costs: number;
  registrationDeadline: string;

  participantMinimum: number;
  participantLimit: number;
  waitingListLimit: number;

  organizer: Organizer;

  locationCity: string;
  locationZipcode: string;
  locationStreet: string;
}

export interface CarePeriod {
  period: Period;
  careDays: {
    day: string;
  }[];
}

export interface Ticket {
  id: string;
  price: number;
  child: Child;
  reservation: Reservation | null;
  booking: Booking | null;
  discountVoucher: {
    discount: {
      status: string;
    };
  } | null;
}

export interface EventTicket {
  id: string;
  event: Event;
  type: string;
  number: number;
  code: string;
  isBlocked: boolean;
  bookedTicket: Ticket | null;
}

export interface Booking {
  id: string;
  code: string;
  status: string;
  date: string;
}

export interface Reservation {
  id: string;
  code: string;
  date: string;
}

export interface Child {
  id: string;
  firstname: string;
  surname: string;
  dateOfBirth: string;
  gender: string;
  healthInsurer: string;
  hasLiabilityInsurance: boolean;
  assistanceRequired: boolean;
  physicalImpairment: string;
  medication: string;
  foodIntolerances: string;
  dietaryRegulations: string;
  miscellaneous: string;
  custodian: {
    firstname: string;
    surname: string;
    emergencyPhone: string;
    addressStreet: string;
    addressHouseNumber: string;
    addressZip: string;
    addressCity: string;
    user: {
      email: string;
    };
  };
}

export interface EventCategory {
  id: string;
  name: string;
}

export interface Organizer {
  id: string;
  name: string;
}

export interface Period {
  id: string;
  startDate: string;
  endDate: string;
}

export interface Holiday {
  id: string;
  name: string;
  periods: Period[];
}
