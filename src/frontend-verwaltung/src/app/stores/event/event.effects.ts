import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  catchError,
  map,
  concatMap,
  withLatestFrom,
  switchMap,
  tap,
  filter,
} from 'rxjs/operators';
import { EventActions } from './event.actions';
import {
  CancelTicketGQL,
  CarePeriodRelateToOneForUpdateInput,
  CreateEventGQL,
  DeleteEventGQL,
  EventGQL,
  EventRelationsGQL,
  EventUpdateInput,
  ImageFieldInput,
  MoveWaitinglistToParticipantGQL,
  UnblockEventTicketGQL,
  UpdateEventGQL,
  UpdateEventStateGQL,
} from 'src/app/graphql/generated';
import { routerNavigatedAction } from '@ngrx/router-store';
import { Store } from '@ngrx/store';
import * as eventSelectors from './event.selectors';
import {
  Child,
  Event,
  EventCategory,
  EventTicket,
  Holiday,
  Organizer,
  Ticket,
} from './event.types';
import { of } from 'rxjs';
import { Router } from '@angular/router';
import * as authSelectors from '../auth/auth.selectors';
import { CarePeriodType, InputForm } from './event.reducer';
import { SetValueAction } from 'ngrx-forms';
import { MessageService } from 'primeng/api';
import * as FileSaver from 'file-saver';
import * as ExcelJS from 'exceljs';
import { selectCurrentSeason } from '../seasons/seasons.selectors';
import { SeasonsActions } from '../seasons/seasons.actions';

const excelMimeType =
  'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';

@Injectable()
export class EventEffects {
  loadEvent$ = createEffect(() =>
    this.actions$.pipe(
      ofType(EventActions.loadEvent),
      switchMap((action) =>
        this.store.select(selectCurrentSeason).pipe(
          filter((season) => season !== undefined),
          switchMap((season) =>
            this.eventQuery
              .fetch({
                eventId: action.eventId,
                seasonId: season?.id as string,
              })
              .pipe(
                map((response) =>
                  EventActions.loadEventSuccess({
                    event: response.data.event as Event,
                    eventTickets: response.data.eventTickets as EventTicket[],
                    cancelledTickets: response.data
                      .cancelledTickets as Ticket[],
                    organizers: response.data.organizers as Organizer[],
                    holidays: response.data.holidays as Holiday[],
                    eventCategories: response.data
                      .eventCategories as EventCategory[],
                  })
                ),
                catchError((error) =>
                  of(EventActions.loadEventFailure({ error }))
                )
              )
          )
        )
      )
    )
  );

  loadEventRelations$ = createEffect(() =>
    this.actions$.pipe(
      ofType(EventActions.loadEventRelations),
      withLatestFrom(this.store.select(selectCurrentSeason)),
      filter(([action, season]) => season !== undefined),
      concatMap(([action, season]) =>
        this.eventRelationsQuery
          .fetch({
            seasonId: season?.id as string,
          })
          .pipe(
            map((response) =>
              EventActions.loadEventRelationsSuccess({
                organizers: response.data.organizers as Organizer[],
                holidays: response.data.holidays as Holiday[],
                eventCategories: response.data
                  .eventCategories as EventCategory[],
              })
            ),
            catchError((error) =>
              of(EventActions.loadEventRelationsFailure({ error }))
            )
          )
      )
    )
  );

  deleteEvent$ = createEffect(() =>
    this.actions$.pipe(
      ofType(EventActions.deleteEvent),
      withLatestFrom(this.store.select(eventSelectors.selectEventId)),
      filter(() => confirm('Wirklich löschen?')),
      concatMap(([action, id]) =>
        this.deleteEvent
          .mutate({
            where: {
              id: id,
            },
          })
          .pipe(
            map((response) =>
              EventActions.deleteEventSuccess({
                id: response.data?.deleteEvent?.id as string,
              })
            ),
            catchError((error) => of(EventActions.loadEventFailure({ error })))
          )
      )
    )
  );

  saveEvent$ = createEffect(() =>
    this.actions$.pipe(
      ofType(EventActions.saveEvent),
      withLatestFrom(
        this.store.select(eventSelectors.selectEventIdFromStore),
        this.store.select(eventSelectors.selectFormState),
        this.store.select(eventSelectors.selectFormValue),
        this.store.select(eventSelectors.selectFile),
        this.store.select(eventSelectors.selectRemovePicture),
        this.store.select(eventSelectors.selectEvent),
        this.store.select(selectCurrentSeason)
      ),
      switchMap(
        ([
          action,
          eventId,
          formState,
          form,
          file,
          removeFile,
          event,
          season,
        ]) => {
          if (!['new', 'planned'].includes(form.status) && !formState.isValid) {
            return of(EventActions.saveEventCancelled());
          }

          return eventId
            ? this.updateEvent
                .mutate(
                  {
                    where: { id: eventId },
                    data: buildInputData(
                      form,
                      file,
                      removeFile,
                      event as Event
                    ),
                  },
                  { context: { useMultipart: true } }
                )
                .pipe(
                  map((response) =>
                    EventActions.updateEventSuccess({
                      id: response.data?.updateEvent?.id as string,
                      status: response.data?.updateEvent?.status as string,
                      picture: response.data?.updateEvent?.picture ?? undefined,
                    })
                  ),
                  catchError((error) =>
                    of(EventActions.updateEventFailure({ error }))
                  )
                )
            : this.createEvent
                .mutate(
                  {
                    data: {
                      ...buildInputData(form, file, removeFile, event as Event),
                      season: { connect: { id: season?.id } },
                    },
                  },
                  { context: { useMultipart: true } }
                )
                .pipe(
                  map((response) =>
                    EventActions.createEventSuccess({
                      id: response.data?.createEvent?.id as string,
                      status: response.data?.createEvent?.status as string,
                    })
                  ),
                  catchError((error) =>
                    of(EventActions.createEventFailure({ error }))
                  )
                );
        }
      )
    )
  );

  loadEventAfterNavigate$ = createEffect(() =>
    this.actions$.pipe(
      ofType(routerNavigatedAction),
      withLatestFrom(
        this.store.select(eventSelectors.selectEventId),
        this.store.select(eventSelectors.selectUrl),
        this.store.select(eventSelectors.selectEvent)
      ),
      filter(
        ([action, eventId, url, event]) =>
          url.startsWith('/veranstaltungen/') &&
          url !== '/veranstaltungen/anlegen' &&
          event?.id !== eventId
      ),
      switchMap(([action, eventId, url]) =>
        this.store.select(authSelectors.selectUserRole).pipe(
          map((role) =>
            EventActions.loadEvent({
              eventId: eventId as string,
              role: role as string,
            })
          )
        )
      )
    )
  );

  loadEventRelationsAfterNavigate$ = createEffect(() =>
    this.actions$.pipe(
      ofType(routerNavigatedAction),
      withLatestFrom(
        this.store.select(eventSelectors.selectUrl),
        this.store.select(authSelectors.selectUserRole)
      ),
      filter(([action, url, role]) => url === '/veranstaltungen/anlegen'),
      map(([action, url, role]) =>
        EventActions.loadEventRelations({
          role: role as string,
        })
      )
    )
  );

  loadEventRelationsAfterSeasonChanged$ = createEffect(() =>
    this.actions$.pipe(
      ofType(
        SeasonsActions.nextSeason,
        SeasonsActions.previousSeason,
        SeasonsActions.selectSeason
      ),
      withLatestFrom(
        this.store.select(eventSelectors.selectUrl),
        this.store.select(authSelectors.selectUserRole)
      ),
      filter(([action, url, role]) => url === '/veranstaltungen/anlegen'),
      map(([action, url, role]) =>
        EventActions.loadEventRelations({
          role: role as string,
        })
      )
    )
  );

  resetEditorAfterNavigate$ = createEffect(() =>
    this.actions$.pipe(
      ofType(routerNavigatedAction),
      withLatestFrom(
        this.store.select(eventSelectors.selectEventId),
        this.store.select(eventSelectors.selectUrl),
        this.store.select(authSelectors.selectUserOrganizer),
        this.store.select(authSelectors.selectUserRole)
      ),
      filter(([action, eventId, url]) => url === '/veranstaltungen/anlegen'),
      concatMap(([action, eventId, url, organizer, role]) => [
        EventActions.resetEditor({ role: role as string }),
        ...(organizer
          ? [
              EventActions.selectOrganizer({
                organizer: organizer?.id as string,
              }),
            ]
          : []),
      ])
    )
  );

  redirectToEventsWhenSaveSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(
          EventActions.createEventSuccess,
          EventActions.deleteEventSuccess
        ),
        tap(() => this.router.navigate(['/veranstaltungen']))
      ),
    {
      dispatch: false,
    }
  );

  nextEvent$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(EventActions.nextEvent),
        withLatestFrom(this.store.select(eventSelectors.selectNextEventId)),
        tap(([action, nextId]) =>
          this.router.navigate(['/veranstaltungen/', nextId])
        )
      ),
    {
      dispatch: false,
    }
  );

  copyCareDays$ = createEffect(() =>
    this.actions$.pipe(
      filter((action) => action.type === SetValueAction.TYPE),
      map((action) => action as SetValueAction<string>),
      filter(
        (action) =>
          action.controlId == 'input_form.carePeriodCommitted.period.id'
      ),
      withLatestFrom(this.store.select(eventSelectors.selectEventPeriods)),
      map(
        ([action, events]) =>
          new SetValueAction(
            'input_form.carePeriodCommitted.careDays',
            action.value == events.desired.period.id
              ? events.desired.careDays
              : action.value == events.alternative.period.id
              ? events.alternative.careDays
              : []
          )
      )
    )
  );

  clearCareDays$ = createEffect(() =>
    this.actions$.pipe(
      filter((action) => action.type === SetValueAction.TYPE),
      map((action) => action as SetValueAction<string>),
      filter(
        (action) =>
          action.controlId == 'input_form.carePeriodDesired.period.id' ||
          action.controlId == 'input_form.carePeriodAlternative.period.id'
      ),
      map(
        (action) =>
          new SetValueAction(
            action.controlId == 'input_form.carePeriodDesired.period.id'
              ? 'input_form.carePeriodDesired.careDays'
              : 'input_form.carePeriodAlternative.careDays',
            []
          )
      )
    )
  );

  updateEventState$ = createEffect(() =>
    this.actions$.pipe(
      ofType(EventActions.updateState),
      withLatestFrom(
        this.store.select(eventSelectors.selectEventId),
        this.store.select(eventSelectors.selectEventStatus)
      ),
      switchMap(([action, eventId, status]) =>
        this.updateEventState
          .mutate({
            id: eventId as string,
            status: action.status,
          })
          .pipe(
            map((response) =>
              EventActions.updateStateSuccess({
                status: response.data?.updateEventState?.status as string,
              })
            ),
            catchError((error) =>
              of(EventActions.updateStateFailure({ error }))
            )
          )
      )
    )
  );

  showToastAfterUpdateEventSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(
          EventActions.updateEventSuccess,
          EventActions.createEventSuccess
        ),
        tap(() =>
          this.toastService.add({
            summary: 'Veranstaltung wurde gespeichert.',
            severity: 'success',
          })
        )
      ),
    { dispatch: false }
  );

  showToastAfterSaveEventCancelled$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(EventActions.saveEventCancelled),
        tap(() =>
          this.toastService.add({
            summary:
              'Bitte füllen Sie die markierten Felder vollständig aus, bevor die Veranstaltung gespeichert werden kann.',
            severity: 'info',
            sticky: true,
          })
        )
      ),
    { dispatch: false }
  );

  cancelTicket$ = createEffect(() =>
    this.actions$.pipe(
      ofType(EventActions.cancelTicket),
      switchMap((action) =>
        this.cancelTicket
          .mutate({
            ticketId: action.ticketId,
          })
          .pipe(
            map((response) =>
              EventActions.cancelTicketSuccess({
                eventTickets: response.data?.cancelTicket as EventTicket[],
              })
            ),
            catchError((error) =>
              of(EventActions.cancelTicketFailure({ error }))
            )
          )
      )
    )
  );

  moveWaitinglistTicketToParticipantList$ = createEffect(() =>
    this.actions$.pipe(
      ofType(EventActions.moveWaitingListTicketToParticipantList),
      switchMap((action) =>
        this.moveWaitinglistToParticipantMutation
          .mutate({
            ticketId: action.ticketId,
          })
          .pipe(
            map((response) => {
              if (
                response.data?.moveWaitinglistToParticipant.__typename ===
                'MoveWaitinglistToParticipantSuccess'
              ) {
                return EventActions.moveWaitingListTicketToParticipantListSuccess(
                  {
                    eventTickets: response.data?.moveWaitinglistToParticipant
                      .eventTickets as EventTicket[],
                  }
                );
              }

              if (
                response.data?.moveWaitinglistToParticipant.__typename ===
                  'MoveWaitinglistToParticipantFailed' &&
                response.data.moveWaitinglistToParticipant.reason ===
                  'NoBlockedTicketAvailable'
              ) {
                return EventActions.moveWaitingListTicketToParticipantListNoBlockedTicketAvailable(
                  {
                    ticketId: action.ticketId,
                  }
                );
              }

              throw new Error('no result');
            }),
            catchError((error) =>
              of(
                EventActions.moveWaitingListTicketToParticipantListFailure({
                  error,
                })
              )
            )
          )
      )
    )
  );

  unblockEventTicket$ = createEffect(() =>
    this.actions$.pipe(
      ofType(EventActions.unblockEventTicket),
      switchMap((action) =>
        this.unblockEventTicketMutation
          .mutate({
            eventTicketId: action.eventTicketId,
          })
          .pipe(
            map((response) => {
              if (
                response.data?.unblockEventTicket.__typename ===
                'UnblockEventTicketSuccess'
              ) {
                return EventActions.unblockEventTicketSuccess({
                  eventTicket: response.data.unblockEventTicket
                    .eventTicket as EventTicket,
                });
              }

              if (
                response.data?.unblockEventTicket.__typename ===
                  'UnblockEventTicketFailed' &&
                response.data.unblockEventTicket.reason ===
                  'EventTicketNotBlocked'
              ) {
                return EventActions.unblockEventTicketNotBlocked({
                  eventTicketId: action.eventTicketId,
                });
              }

              throw new Error('no result');
            }),
            catchError((error) =>
              of(
                EventActions.unblockEventTicketFailure({
                  error,
                })
              )
            )
          )
      )
    )
  );

  exportEvents$ = createEffect(() =>
    this.actions$.pipe(
      ofType(EventActions.exportEventParticipants),
      withLatestFrom(
        this.store.select(eventSelectors.selectEventTicketsParticipants),
        this.store.select(eventSelectors.selectEvent)
      ),
      concatMap(async ([action, participants, event]) => {
        const workbook = this.buildExcelSheet(
          event as Event,
          participants.filter((p) => p.bookedTicket !== null)
        );
        await this.saveExcelSheet(event as Event, workbook);
        return EventActions.exportEventParticipantsSuccess();
      }),
      catchError((error) =>
        of(EventActions.exportEventParticipantsFailure({ error }))
      )
    )
  );

  private async saveExcelSheet(event: Event, workbook: ExcelJS.Workbook) {
    const buffer = await workbook.xlsx.writeBuffer();

    var blob = new Blob([buffer], {
      type: excelMimeType,
    });

    FileSaver.saveAs(blob, `Teilnehmer ${event.name}.xlsx`);
  }

  private buildExcelSheet(event: Event, eventTickets: EventTicket[]) {
    const workbook = new ExcelJS.Workbook();
    const participantsWorksheet = workbook.addWorksheet('Teilnehmer', {
      properties: {
        defaultColWidth: 20,
      },
    });

    const headers = [
      { name: 'Name', width: 40 },
      { name: 'Geburtsdatum', width: 20 },
      { name: 'Geschlecht', width: 20 },
      { name: 'Adresse', width: 40 },
      { name: 'Krankenversicherung', width: 25 },
      { name: 'Haftpflichtversicherung', width: 25 },
      { name: 'Assistenz benötigt', width: 25 },
      { name: 'Körperliche Beeinträchtigung', width: 30 },
      { name: 'Medikamente', width: 30 },
      { name: 'Allergien/Lebensmittelunverträglichkeiten', width: 50 },
      { name: 'Ernährungsvorschriften', width: 30 },
      { name: 'Sonstiges', width: 30 },
      { name: 'Sorgeberechtigter', width: 40 },
      { name: 'Notfallnummer', width: 30 },
      { name: 'E-Mail Sorgeberechtigter', width: 40 },
    ];

    const headerRow = participantsWorksheet.addRow(
      headers.map((header) => header.name)
    );

    headerRow.font = { bold: true };

    participantsWorksheet.autoFilter = {
      from: {
        row: 1,
        column: 1,
      },
      to: {
        row: 1 + headers.length,
        column: headers.length,
      },
    };

    headers.forEach(
      (header, index) =>
        (participantsWorksheet.columns[index].width = header.width)
    );

    participantsWorksheet.addRows(
      eventTickets.map((ticket) => {
        const child = ticket.bookedTicket?.child as Child;
        const custodian = child.custodian;

        return [
          `${child.surname.trim()}, ${child.firstname.trim()}`,
          new Date(child.dateOfBirth).toLocaleDateString(),
          this.genderNames[child.gender],
          `${custodian.addressStreet} ${custodian.addressHouseNumber}, ${custodian.addressZip} ${custodian.addressCity}`,
          child.healthInsurer,
          child.hasLiabilityInsurance ? 'Ja' : 'Nein',
          child.assistanceRequired ? 'Ja' : 'Nein',
          child.physicalImpairment,
          child.medication,
          child.foodIntolerances,
          child.dietaryRegulations,
          child.miscellaneous,
          `${custodian?.surname}, ${custodian?.firstname}`,
          custodian?.emergencyPhone,
          custodian.user.email,
        ];
      })
    );

    return workbook;
  }

  statusNames: { [index: string]: string } = {
    approved: 'freigegeben',
    check: 'zur Prüfung übermittelt',
    cancelled: 'abgesagt',
  };

  genderNames: { [index: string]: string } = {
    male: 'männlich',
    female: 'weiblich',
    diverse: 'divers',
  };

  showToastAfterUpdateStateSuccess$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(EventActions.updateStateSuccess),
        tap((action) =>
          this.toastService.add({
            summary: 'Veranstaltung wurde ' + this.statusNames[action.status],
            severity: 'success',
          })
        )
      );
    },
    { dispatch: false }
  );

  showToastAfterDeleteEventSuccess$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(EventActions.deleteEventSuccess),
        tap((action) =>
          this.toastService.add({
            summary: 'Veranstaltung wurde gelöscht',
            severity: 'success',
          })
        )
      );
    },
    { dispatch: false }
  );

  showToastAfterMoveWaitinglistTicketToParticipantSuccess$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(EventActions.moveWaitingListTicketToParticipantListSuccess),
        tap((action) =>
          this.toastService.add({
            summary: 'Ticket wurde von Warteliste auf Teilnehmerliste gesetzt',
            severity: 'success',
          })
        )
      );
    },
    { dispatch: false }
  );

  showToastAfterMoveWaitinglistTicketToParticipantNoBlockedTicket$ =
    createEffect(
      () => {
        return this.actions$.pipe(
          ofType(
            EventActions.moveWaitingListTicketToParticipantListNoBlockedTicketAvailable
          ),
          tap((action) =>
            this.toastService.add({
              summary: 'Kein freier Platz in der Teilnehmerliste',
              severity: 'warn',
            })
          )
        );
      },
      { dispatch: false }
    );

  showToastAfterUnblockTicketSuccess$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(EventActions.unblockEventTicketSuccess),
        tap((action) =>
          this.toastService.add({
            summary: 'Teilnehmerplatz wurde freigegeben',
            severity: 'success',
          })
        )
      );
    },
    { dispatch: false }
  );

  showToastAfterUnblockTicketNoBlockedTicket$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(EventActions.unblockEventTicketNotBlocked),
        tap((action) =>
          this.toastService.add({
            summary: 'Teilnehmerplatz ist nicht blockiert',
            severity: 'warn',
          })
        )
      );
    },
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private eventQuery: EventGQL,
    private eventRelationsQuery: EventRelationsGQL,
    private createEvent: CreateEventGQL,
    private updateEvent: UpdateEventGQL,
    private updateEventState: UpdateEventStateGQL,
    private deleteEvent: DeleteEventGQL,
    private cancelTicket: CancelTicketGQL,
    private moveWaitinglistToParticipantMutation: MoveWaitinglistToParticipantGQL,
    private unblockEventTicketMutation: UnblockEventTicketGQL,
    private store: Store,
    private router: Router,
    private toastService: MessageService
  ) {}
}
function buildInputData(
  form: InputForm,
  file: File | undefined,
  removeFile: boolean,
  event: Event
): EventUpdateInput {
  return {
    organizer: {
      connect: {
        id: form.organizer,
      },
    },
    categories: {
      connect: form.categories.map((category) => ({ id: category.id })),
      disconnect:
        event?.categories
          .filter(
            (existingCategory) =>
              !form.categories.find(
                (category) => category.id == existingCategory.id
              )
          )
          .map((category) => ({ id: category.id })) ?? undefined,
    },
    picture: buildPicture(file, removeFile),
    pictureRights: form.pictureRights,
    ageMaximum: form.ageMaximum,
    ageMinimum: form.ageMinimum,
    careTimeStart: form.careTimeStart,
    careTimeEnd: form.careTimeEnd,
    contactPersonSalutation:
      form.contactPersonSalutation !== '' ? form.contactPersonSalutation : null,
    contactPersonFirstname: form.contactPersonFirstname,
    contactPersonSurname: form.contactPersonSurname,
    emergencyPhone: form.emergencyPhone,
    costs: form.costs,
    registrationDeadline:
      form.registrationDeadline != '' ? form.registrationDeadline : null,
    description: form.description,
    hints: form.hints,
    meals: form.meals,
    locationCity: form.locationCity,
    locationStreet: form.locationStreet,
    locationZipcode: form.locationZipcode,
    name: form.name,
    participantMinimum: form.participantMinimum,
    participantLimit: form.participantLimit,
    waitingListLimit: form.waitingListLimit,
    carePeriodDesired: buildCarePeriod(form, event, 'carePeriodDesired'),
    carePeriodAlternative: buildCarePeriod(
      form,
      event,
      'carePeriodAlternative'
    ),
    carePeriodCommitted: buildCarePeriod(form, event, 'carePeriodCommitted'),
  };
}

function buildPicture(
  file: File | undefined,
  removeFile: boolean
): ImageFieldInput | null | undefined {
  return file
    ? {
        upload: file,
      }
    : removeFile === true
    ? null
    : undefined;
}

function buildCarePeriod(
  form: InputForm,
  event: Event,
  type: CarePeriodType
): CarePeriodRelateToOneForUpdateInput | undefined {
  return form[type].period.id
    ? {
        create: {
          period: {
            connect: {
              id: form[type].period.id,
            },
          },
          careDays: {
            create: form[type].careDays.map((day) => {
              return buildCareDays(day);
            }),
          },
        },
      }
    : event && event[type]?.period?.id
    ? {
        disconnect: true,
      }
    : undefined;
}

function buildCareDays(day: { day: string }) {
  const date = new Date(day.day);

  return {
    day: `${date.getFullYear()}-${(date.getMonth() + 1)
      .toString()
      .padStart(2, '0')}-${date.getDate().toString().padStart(2, '0')}`,
  };
}

const statusNext: { [index: string]: string } = {
  new: 'planned',
  planned: 'check',
  check: 'approved',
  approved: 'cancelled',
};

function buildNextStatus(status: string | undefined): string {
  return statusNext[status as string];
}
