import { createFeature, createReducer, on } from '@ngrx/store';
import { EventActions } from './event.actions';
import {
  CarePeriod,
  Event,
  EventCategory,
  EventTicket,
  Holiday,
  Organizer,
  Ticket,
} from './event.types';
import {
  FormGroupState,
  createFormGroupState,
  onNgrxForms,
  setValue,
  markAsSubmitted,
  wrapReducerWithFormStateUpdate,
  enable,
  reset,
  disable,
} from 'ngrx-forms';
import { validateForm } from './event.validation';
import { SortByState, defaultSortByState, sortByReducer } from '../sorting';

export const eventFeatureKey = 'event';

export type CarePeriodType =
  | 'carePeriodCommitted'
  | 'carePeriodDesired'
  | 'carePeriodAlternative';

const formularId = 'input_form';

export interface InputForm {
  role: string;

  status: string;
  name: string;

  organizer: string;

  picture: {
    url: string;
  };

  pictureRights: string;

  description: string;

  hints: string;
  meals: string;
  categories: { id: string }[];

  carePeriodDesired: CarePeriod;
  carePeriodAlternative: CarePeriod;
  carePeriodCommitted: CarePeriod;

  careTimeStart: string;
  careTimeEnd: string;

  locationCity: string;
  locationZipcode: string;
  locationStreet: string;

  ageMinimum: number;
  ageMaximum: number;

  participantMinimum: number;
  participantLimit: number;
  waitingListLimit: number | undefined;

  contactPersonSalutation: string;
  contactPersonFirstname: string;
  contactPersonSurname: string;
  emergencyPhone: string;

  costs: number;
  registrationDeadline: string;
}

export interface State {
  event: Event | undefined;
  organizers: Organizer[];
  holidays: Holiday[];
  eventCategories: EventCategory[];
  form: FormGroupState<InputForm>;
  file: File | undefined;
  removePicture: boolean;
  eventTickets: EventTicket[];
  canceledTickets: Ticket[];
  sortByParticipants: SortByState;
  sortByWaitinglist: SortByState;
  sortByCancelled: SortByState;
}

export const initialState: State = {
  event: undefined,
  organizers: [],
  holidays: [],
  eventCategories: [],
  file: undefined,
  removePicture: false,
  eventTickets: [],
  canceledTickets: [],
  sortByParticipants: { ...defaultSortByState, by: 'number' },
  sortByWaitinglist: { ...defaultSortByState, by: 'number' },
  sortByCancelled: { ...defaultSortByState, by: 'bookingDate' },
  form: createFormGroupState(formularId, {
    role: '',
    status: '',
    name: '',
    organizer: '',
    picture: {
      url: '',
    },
    pictureRights: '',

    description: '',
    hints: '',
    meals: '',

    ageMinimum: 0,
    ageMaximum: 0,

    costs: 0,
    registrationDeadline: '',

    careTimeStart: '',
    careTimeEnd: '',
    categories: [],
    contactPersonSalutation: '',
    contactPersonFirstname: '',
    contactPersonSurname: '',
    emergencyPhone: '',
    participantMinimum: 0,
    participantLimit: 0,
    waitingListLimit: undefined,
    locationCity: '',
    locationStreet: '',
    locationZipcode: '',

    carePeriodDesired: {
      id: '',
      period: { id: '', startDate: '', endDate: '' },
      careDays: [],
    },
    carePeriodAlternative: {
      id: '',
      period: { id: '', startDate: '', endDate: '' },
      careDays: [],
    },
    carePeriodCommitted: {
      id: '',
      period: { id: '', startDate: '', endDate: '' },
      careDays: [],
    },
  } as InputForm),
};

export const reducer = wrapReducerWithFormStateUpdate(
  createReducer(
    initialState,
    on(EventActions.loadEvent, (state, action) => ({
      ...state,
      form: setValue({
        ...initialState.form.value,
        role: action.role,
      } as InputForm)(state.form),
    })),
    on(EventActions.loadEventSuccess, (state, action) => ({
      ...initialState,
      event: action.event,
      eventTickets: [...action.eventTickets],
      canceledTickets: [...action.cancelledTickets],
      organizers: [...action.organizers],
      eventCategories: [...action.eventCategories],
      holidays: [...action.holidays],
      form: enable(
        reset(
          setValue({
            ...state.form.value,
            ...action.event,
            organizer: action.event.organizer?.id,
            picture: {
              url: action.event.picture?.url ?? '',
            },
            carePeriodCommitted: action.event.carePeriodCommitted ?? {
              careDays: [],
              period: { id: '', startDate: '', endDate: '' },
            },
            carePeriodDesired: action.event.carePeriodDesired ?? {
              careDays: [],
              period: { id: '', startDate: '', endDate: '' },
            },
            carePeriodAlternative: action.event.carePeriodAlternative ?? {
              careDays: [],
              period: { id: '', startDate: '', endDate: '' },
            },
            categories: action.event.categories,
          } as InputForm)(state.form)
        )
      ),
    })),
    on(EventActions.loadEventRelationsSuccess, (state, action) => ({
      ...state,
      organizers: [...action.organizers],
      eventCategories: [...action.eventCategories],
      holidays: [...action.holidays],
    })),
    on(EventActions.loadEventFailure, (state, action) => state),
    on(EventActions.toggleCareDate, (state, action) => ({
      ...state,
      form: setValue({
        ...state.form.value,
        [action.field]: {
          ...state.form.value[action.field],
          careDays: state.form.value[action.field]?.careDays.find(
            (x) => new Date(x.day).getTime() == action.date.getTime()
          )
            ? state.form.value[action.field]?.careDays.filter(
                (date) => new Date(date.day).getTime() !== action.date.getTime()
              )
            : [
                ...(state.form.value[action.field]?.careDays ?? []),
                {
                  day: `${action.date.getFullYear()}-${(
                    action.date.getMonth() + 1
                  )
                    .toString()
                    .padStart(2, '0')}-${action.date
                    .getDate()
                    .toString()
                    .padStart(2, '0')}`,
                },
              ],
        },
      } as InputForm)(state.form),
    })),
    on(EventActions.resetEditor, (state, action) => ({
      ...initialState,
      organizers: state.organizers,
      holidays: state.holidays,
      eventCategories: state.eventCategories,
      form: enable(
        setValue({
          ...initialState.form.value,
          role: action.role,
        } as InputForm)(initialState.form)
      ),
    })),
    on(EventActions.setPhotoFile, (state, action) => ({
      ...state,
      file: action.file,
    })),
    on(EventActions.createEventSuccess, (state, action) => ({
      ...state,
      event: undefined,
      file: undefined,
    })),
    on(EventActions.updateEventSuccess, (state, action) => {
      const form = setValue({
        ...state.form.value,
        picture: action.picture ?? { url: '' },
        status: action.status,
      } as InputForm)(state.form);

      return {
        ...state,
        event: {
          ...state.event,
          status: action.status,
          picture: action.picture,
        } as Event,
        form: reset(form),
        file: undefined,
        removePicture: false,
      };
    }),
    on(EventActions.removePhoto, (state, action) => ({
      ...state,
      removePicture: true,
      file: undefined,
    })),
    on(EventActions.updateStateSuccess, (state, action) => ({
      ...state,
      event: {
        ...state.event,
        status: action.status,
      } as Event,
      form: setValue({
        ...state.form.value,
        status: action.status,
      } as InputForm)(state.form),
    })),
    on(EventActions.selectOrganizer, (state, action) => ({
      ...state,
      form: setValue({
        ...state.form.value,
        organizer: action.organizer,
      } as InputForm)(state.form),
    })),
    on(EventActions.saveEvent, (state, action) => ({
      ...state,
    })),
    on(EventActions.toggleCategory, (state, action) => ({
      ...state,
      form: setValue({
        ...state.form.value,
        categories: state.form.value.categories.find(
          (category) => category.id == action.id
        )
          ? state.form.value.categories.filter(
              (category) => category.id != action.id
            )
          : [...state.form.value.categories, { id: action.id }],
      } as InputForm)(state.form),
    })),
    on(EventActions.cancelTicketSuccess, (state, action) => ({
      ...state,
      eventTickets: [...action.eventTickets],
    })),
    on(EventActions.unblockEventTicketSuccess, (state, action) => ({
      ...state,
      eventTickets: state.eventTickets.map((eventTicket) =>
        eventTicket.id !== action.eventTicket.id
          ? eventTicket
          : action.eventTicket
      ),
    })),
    on(
      EventActions.moveWaitingListTicketToParticipantListSuccess,
      (state, action) => ({
        ...state,
        eventTickets: [...action.eventTickets],
      })
    ),
    on(EventActions.participantsSort, (state, action) => ({
      ...state,
      sortByParticipants: sortByReducer(
        state.sortByParticipants,
        action.column
      ),
    })),
    on(EventActions.waitinglistSort, (state, action) => ({
      ...state,
      sortByWaitinglist: sortByReducer(state.sortByWaitinglist, action.column),
    })),
    on(EventActions.cancelledSort, (state, action) => ({
      ...state,
      sortByCancelled: sortByReducer(state.sortByCancelled, action.column),
    })),
    onNgrxForms()
  ),
  (state) => state.form,
  validateForm
);

export const eventFeature = createFeature({
  name: eventFeatureKey,
  reducer,
});
