import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromEvent from './event.reducer';
import * as fromRouter from '@ngrx/router-store';
import { selectUserRole } from '../auth/auth.selectors';
import { selectEvents } from '../events/events.selectors';
import {
  stringComparer,
  numberComparer,
  dateComparer,
  booleanComparer,
} from '../sorting';
import { EventTicket, Period, Ticket } from './event.types';

export const selectEventState = createFeatureSelector<fromEvent.State>(
  fromEvent.eventFeatureKey
);

export const selectHolidays = createSelector(
  selectEventState,
  (state) => state.holidays
);

export const selectOrganizers = createSelector(
  selectEventState,

  (state) => state.organizers
);

export const selectEventCategories = createSelector(selectEventState, (state) =>
  [...state.eventCategories].sort((a, b) => a.name.localeCompare(b.name))
);

export const selectPeriods = createSelector(selectHolidays, (holidays) =>
  holidays
    .filter((holiday) => holiday.periods.length > 0)
    .reduce(
      (periods, holiday) => [
        ...periods,
        ...holiday.periods.map((period) => ({
          ...period,
          holidayName: holiday.name,
        })),
      ],
      new Array<Period & { holidayName: string }>()
    )
    .sort(
      (a, b) =>
        new Date(a.startDate).getTime() - new Date(b.startDate).getTime()
    )
);

export const { selectRouteParams, selectFragment, selectUrl, selectRouteData } =
  fromRouter.getRouterSelectors();

export const selectEventTab = createSelector(
  selectFragment,
  (fragment) => fragment ?? 'details'
);

export const selectEventId = createSelector(
  selectRouteParams,
  (params) => params?.['eventId'] as string | undefined
);

export const selectEventIds = createSelector(selectEvents, (params) =>
  params.map((_) => _.id)
);

export const selectNextEventId = createSelector(
  selectEventId,
  selectEventIds,
  (id, ids) => {
    if (!id || ids.length == 0) return undefined;

    const index = ids.indexOf(id);
    if (index == ids.length - 1) {
      return ids[0];
    }

    return ids[index + 1];
  }
);

export const selectEventIdFromStore = createSelector(
  selectEventState,
  (state) => state.event?.id
);

export const selectEvent = createSelector(
  selectEventState,
  (state) => state.event
);

export const selectFile = createSelector(
  selectEventState,
  (state) => state.file
);

export const selectEventForm = createSelector(
  selectEventState,
  (state) => state.form
);

export const selectEventOrganizer = createSelector(
  selectEvent,
  (event) => event?.organizer
);

export const selectEventPeriods = createSelector(selectEventForm, (event) => ({
  desired: event.value.carePeriodDesired,
  alternative: event.value.carePeriodAlternative,
}));

function getDaysBetweenDates(
  startDate: Date,
  endDate: Date
): { date: Date; available: boolean }[] {
  const oneDayInMilliseconds = 24 * 60 * 60 * 1000;
  const daysDiff = Math.floor(
    (endDate.getTime() - startDate.getTime()) / oneDayInMilliseconds
  );

  const weeksArray = Array.from(
    { length: Math.ceil((daysDiff + 1) / 7) },
    (_, index) => {
      const startOfWeek = new Date(startDate);
      const startOfWeekTime =
        startDate.getTime() +
        (index * 7 - ((startDate.getDay() + 6) % 7)) * oneDayInMilliseconds;
      startOfWeek.setTime(startOfWeekTime);

      const endOfWeek = new Date(startOfWeek);
      endOfWeek.setDate(startOfWeek.getDate() + 6);
      const weekDays = Array.from({ length: 7 }, (_, dayIndex) => {
        const currentDate = new Date(startOfWeek);
        currentDate.setDate(startOfWeek.getDate() + dayIndex);
        return {
          date: currentDate,
          available: currentDate >= startDate && currentDate <= endDate,
        };
      });

      return weekDays;
    }
  ).flat();

  return weeksArray;
}

export const selectEventCareDaysPrimary = createSelector(
  selectEventForm,
  (form) => form.value.carePeriodDesired?.careDays
);

export const selectEventPrimaryCareDays = createSelector(
  selectEventForm,
  selectPeriods,
  selectEventCareDaysPrimary,
  (event, periods, careDays) => {
    const period = periods.find(
      (period) => period.id === event.value.carePeriodDesired?.period.id
    );

    if (!period) {
      return [];
    }

    const days = getDaysBetweenDates(
      new Date(period.startDate) as Date,
      new Date(period.endDate) as Date
    );

    return days.map((day) => {
      const careDay = careDays?.find(
        (x) => new Date(x.day).getTime() == day.date.getTime()
      );

      return {
        ...day,
        enabled: careDay !== undefined,
      };
    });
  }
);

export const selectEventCareDaysSecondary = createSelector(
  selectEventForm,
  (form) => form.value.carePeriodAlternative?.careDays
);

export const selectEventSecondaryCareDays = createSelector(
  selectEventForm,
  selectPeriods,
  selectEventCareDaysSecondary,
  (event, periods, careDays) => {
    const period = periods.find(
      (period) => period.id === event.value.carePeriodAlternative?.period.id
    );

    if (!period) {
      return [];
    }

    const days = getDaysBetweenDates(
      new Date(period.startDate) as Date,
      new Date(period.endDate) as Date
    );

    return days.map((day) => {
      const careDay = careDays?.find(
        (x) => new Date(x.day).getTime() == day.date.getTime()
      );

      return {
        ...day,
        enabled: careDay !== undefined,
      };
    });
  }
);

export const selectEventCareDaysCommitted = createSelector(
  selectEventForm,
  (form) => form.value.carePeriodCommitted?.careDays
);

export const selectEventCommittedCareDays = createSelector(
  selectEventForm,
  selectPeriods,
  selectEventCareDaysCommitted,
  (event, periods, careDays) => {
    const period = periods.find(
      (period) => period.id === event.value.carePeriodCommitted?.period.id
    );

    if (!period) {
      return [];
    }

    const days = getDaysBetweenDates(
      new Date(period.startDate) as Date,
      new Date(period.endDate) as Date
    );

    return days.map((day) => {
      const careDay = careDays?.find(
        (x) => new Date(x.day).getTime() == day.date.getTime()
      );

      return {
        ...day,
        enabled: careDay !== undefined,
      };
    });
  }
);

export const selectEventTickets = createSelector(
  selectEventState,
  (state) => state.eventTickets
);

export const selectEventTicketsByType = (type: string) =>
  createSelector(selectEventTickets, (tickets) =>
    tickets
      .filter((ticket) => ticket.type === type)
      .sort((a, b) => a.number - b.number)
  );

export const selectEventTicketsParticipants = createSelector(
  selectEventTicketsByType('participant'),
  (tickets) =>
    tickets
      .sort((a, b) => {
        const dateA =
          a.bookedTicket?.booking?.date ?? a.bookedTicket?.reservation?.date;
        const dateB =
          b.bookedTicket?.booking?.date ?? b.bookedTicket?.reservation?.date;

        if (!dateA) {
          if (!dateB) {
            return 0;
          }

          return 1;
        }

        if (!dateB) {
          return -1;
        }

        return dateA.localeCompare(dateB);
      })
      .map((ticket, index) => ({ ...ticket, number: index + 1 }))
);

export const selectEventTicketsWaitingList = createSelector(
  selectEventTicketsByType('waitinglist'),
  (tickets) => tickets
);

export const selectSortByParticipants = createSelector(
  selectEventState,
  (state) => state.sortByParticipants
);
export const selectSortByWaitinglist = createSelector(
  selectEventState,
  (state) => state.sortByWaitinglist
);

export const selectCancelledTickets = createSelector(
  selectEventState,
  (state) => state.canceledTickets
);

export const selectSortByCancelled = createSelector(
  selectEventState,
  (state) => state.sortByCancelled
);

const paymentStatusOrder = ['overdue', 'pending', 'payed', 'reserved'];
const discountStatusOrder = [
  'overdue',
  'pending',
  'approved',
  'rejected',
  'cancelled',
];

const sortersParticipants: {
  [index: string]: (a: EventTicket, b: EventTicket) => number;
} = {
  number: numberComparer((e) => e.number),
  status: (a, b) => {
    const statusA = paymentStatusOrder.indexOf(
      a.bookedTicket!.booking?.status ??
        (a.bookedTicket!.reservation ? 'reserved' : '')
    );
    const statusB = paymentStatusOrder.indexOf(
      b.bookedTicket!.booking?.status ??
        (b.bookedTicket!.reservation ? 'reserved' : '')
    );

    return statusA - statusB;
  },
  discountStatus: (a, b) => {
    const statusA = discountStatusOrder.indexOf(
      a.bookedTicket!.discountVoucher?.discount?.status ?? ''
    );
    const statusB = discountStatusOrder.indexOf(
      b.bookedTicket!.discountVoucher?.discount?.status ?? ''
    );

    return statusA - statusB;
  },
  child: stringComparer(
    (e) =>
      `${e.bookedTicket!.child.surname}, ${e.bookedTicket!.child.firstname}`
  ),
  custodian: stringComparer(
    (e) =>
      `${e.bookedTicket!.child.custodian.surname}, ${
        e.bookedTicket!.child.custodian.firstname
      }`
  ),
  address: stringComparer(
    (e) => `${e.bookedTicket!.child.custodian.addressStreet}`
  ),
  dateOfBirth: dateComparer((e) => new Date(e.bookedTicket!.child.dateOfBirth)),
  booking: stringComparer(
    (e) => e.bookedTicket!.booking?.code ?? e.bookedTicket!.reservation!.code
  ),
  bookingDate: dateComparer(
    (e) =>
      new Date(
        e.bookedTicket!.booking?.date ?? e.bookedTicket!.reservation!.date
      )
  ),
  phone: stringComparer((e) => e.bookedTicket!.child.custodian.emergencyPhone),
  assistanceRequired: booleanComparer(
    (e) => e.bookedTicket!.child.assistanceRequired
  ),
};

const sortersWaitinglist: {
  [index: string]: (a: EventTicket, b: EventTicket) => number;
} = {
  number: numberComparer((e) => e.number),
  status: numberComparer((e) => e.number),
  discountStatus: (a, b) => {
    const statusA = discountStatusOrder.indexOf(
      a.bookedTicket!.discountVoucher?.discount?.status ?? ''
    );
    const statusB = discountStatusOrder.indexOf(
      b.bookedTicket!.discountVoucher?.discount?.status ?? ''
    );

    return statusA - statusB;
  },
  child: stringComparer(
    (e) =>
      `${e.bookedTicket!.child.surname}, ${e.bookedTicket!.child.firstname}`
  ),
  custodian: stringComparer(
    (e) =>
      `${e.bookedTicket!.child.custodian.surname}, ${
        e.bookedTicket!.child.custodian.firstname
      }`
  ),
  address: stringComparer(
    (e) => `${e.bookedTicket!.child.custodian.addressStreet}`
  ),
  dateOfBirth: dateComparer((e) => new Date(e.bookedTicket!.child.dateOfBirth)),
  reservation: stringComparer((e) => e.bookedTicket!.reservation!.code),
  reservationDate: dateComparer(
    (e) => new Date(e.bookedTicket!.reservation!.date)
  ),
  phone: stringComparer((e) => e.bookedTicket!.child.custodian.emergencyPhone),
  assistanceRequired: booleanComparer(
    (e) => e.bookedTicket!.child.assistanceRequired
  ),
};

const sortersCancelled: {
  [index: string]: (a: Ticket, b: Ticket) => number;
} = {
  status: (a, b) => {
    const statusA = paymentStatusOrder.indexOf(a.booking?.status ?? '');
    const statusB = paymentStatusOrder.indexOf(b.booking?.status ?? '');

    return statusA - statusB;
  },
  discountStatus: (a, b) => {
    const statusA = discountStatusOrder.indexOf(
      a.discountVoucher?.discount?.status ?? ''
    );
    const statusB = discountStatusOrder.indexOf(
      b.discountVoucher?.discount?.status ?? ''
    );

    return statusA - statusB;
  },
  child: stringComparer((e) => `${e.child.surname}, ${e.child.firstname}`),
  custodian: stringComparer(
    (e) => `${e.child.custodian.surname}, ${e.child.custodian.firstname}`
  ),
  address: stringComparer((e) => `${e.child.custodian.addressStreet}`),
  dateOfBirth: dateComparer((e) => new Date(e.child.dateOfBirth)),
  booking: stringComparer((e) => e.booking?.code ?? ''),
  bookingDate: stringComparer((e) => e.booking?.date ?? ''),
  price: numberComparer((e) => e.price),
  phone: stringComparer((e) => e.child.custodian.emergencyPhone),
};

export const selectEventTicketsParticipantsSorted = createSelector(
  selectEventTicketsParticipants,
  selectSortByParticipants,
  (tickets, sortBy) =>
    sortersParticipants[sortBy.by]
      ? tickets.sort((a, b) => {
          if (sortBy.by !== 'number') {
            if (a.isBlocked || !a.bookedTicket) {
              if (b.isBlocked || !b.bookedTicket) {
                return 0;
              }

              return 1;
            }

            if (b.isBlocked || !b.bookedTicket) {
              return -1;
            }
          }

          return (
            (sortBy.descending ? -1 : 1) * sortersParticipants[sortBy.by](a, b)
          );
        })
      : tickets
);

export const selectEventTicketsWaitinglistSorted = createSelector(
  selectEventTicketsWaitingList,
  selectSortByWaitinglist,
  (tickets, sortBy) =>
    sortersWaitinglist[sortBy.by]
      ? tickets.sort((a, b) => {
          if (sortBy.by !== 'number') {
            if (!a.bookedTicket) {
              if (!b.bookedTicket) {
                return 0;
              }

              return 1;
            }

            if (!b.bookedTicket) {
              return -1;
            }
          }

          return (
            (sortBy.descending ? -1 : 1) * sortersWaitinglist[sortBy.by](a, b)
          );
        })
      : tickets
);

export const selectCancelledTicketsSorted = createSelector(
  selectCancelledTickets,
  selectSortByCancelled,
  (tickets, sortBy) =>
    sortersCancelled[sortBy.by]
      ? [...tickets].sort((a, b) => {
          return (
            (sortBy.descending ? -1 : 1) * sortersCancelled[sortBy.by](a, b)
          );
        })
      : tickets
);

export const selectEventTicketsParticipantsPersons = createSelector(
  selectEventTicketsParticipants,
  (tickets) =>
    [...tickets.map((y) => y.bookedTicket?.child?.custodian!)].filter(
      (y) => y !== undefined
    )
);

export const selectEventTicketsWaitinglistPersons = createSelector(
  selectEventTicketsWaitingList,
  (tickets) =>
    tickets
      .map((y) => y.bookedTicket?.child?.custodian!)
      .filter((y) => y !== undefined)
);

export const selectEventParticipantCount = createSelector(
  selectEventTicketsParticipants,
  (tickets) => tickets.filter((ticket) => ticket.bookedTicket !== null).length
);

export const selectEventWaitingListCount = createSelector(
  selectEventTicketsWaitingList,
  (tickets) => tickets.filter((ticket) => ticket.bookedTicket !== null).length
);

export const selectEventCancelledTicketCount = createSelector(
  selectCancelledTickets,
  (tickets) => tickets.length
);

export const selectEventParticipantLimit = createSelector(
  selectEvent,
  (event) => event?.participantLimit
);

export const selectEventWaitingListLimit = createSelector(
  selectEvent,
  (event) => event?.waitingListLimit
);

export const selectEventParticipantFreeSlots = createSelector(
  selectEventParticipantCount,
  selectEventParticipantLimit,
  (count, limit) => (limit ?? 0) - (count ?? 0)
);

export const selectEventWaitingListFreeSlots = createSelector(
  selectEventWaitingListCount,
  selectEventWaitingListLimit,
  (count, limit) => (limit ?? 0) - (count ?? 0)
);

export const selectFormState = createSelector(
  selectEventState,
  (state) => state.form
);

export const selectFormValue = createSelector(
  selectEventState,
  (state) => state.form.value
);

export const selectEventStatus = createSelector(
  selectEvent,
  (event) => event?.status
);

export const selectFormPictureUrl = createSelector(
  selectEventState,
  (state) => state.form.value.picture.url
);

export const selectRemovePicture = createSelector(
  selectEventState,
  (state) => state.removePicture
);

export const selectCanDelete = createSelector(
  selectEventState,
  selectUserRole,
  (state, role) => {
    if (role === 'administrator' && state.event?.status) {
      return true;
    }

    if (state.event?.status === 'new') {
      return true;
    }

    return false;
  }
);

export const selectCanApprove = createSelector(
  selectEventState,
  selectUserRole,
  (state, role) => {
    if (role !== 'administrator') {
      return false;
    }

    if (state.event?.status === 'planned' || state.event?.status === 'check') {
      return true;
    }

    return false;
  }
);

export const selectCanCancel = createSelector(
  selectEventState,
  selectUserRole,
  (state, role) => {
    if (role !== 'administrator') {
      return false;
    }

    if (state.event?.status === 'approved') {
      return true;
    }

    return false;
  }
);

export const selectCanSubmitToCheck = createSelector(
  selectEventState,
  selectUserRole,
  selectEventForm,
  (state, role, form) => {
    if (form.isDirty || !form.isValid) {
      return false;
    }

    if (role !== 'organizer') {
      return false;
    }

    if (state.event?.status === 'planned') {
      return true;
    }

    return false;
  }
);

export const selectShowSubmitToCheck = createSelector(
  selectEventState,
  selectUserRole,
  (state, role) => {
    if (role !== 'organizer') {
      return false;
    }

    if (state.event?.status === 'planned') {
      return true;
    }

    return false;
  }
);

export const selectCanSave = createSelector(
  selectEventState,
  selectUserRole,
  (state, role) => {
    if (!state.form.isValid) {
      return false;
    }

    if (role === 'administrator') {
      return true;
    }

    if (role === 'organizer') {
      if (
        !state.event ||
        state.event?.status === 'new' ||
        state.event?.status === 'planned'
      ) {
        return true;
      }
    }

    return false;
  }
);

export const selectCategories = createSelector(
  selectEventState,
  (state) => state.form.value.categories
);

export const selectCanBook = createSelector(
  selectEventState,
  selectUserRole,
  (event, role) =>
    !['', 'new'].includes(event.form.value.status) || role === 'administrator'
);

export const selectEventCareDaysCommittedFirstDay = createSelector(
  selectEventCareDaysCommitted,
  (days) => {
    const sortedDays = [...days].sort((a, b) => a.day.localeCompare(b.day));

    if (sortedDays.length === 0) {
      return undefined;
    }

    return sortedDays[0];
  }
);

export const selectRegistrationDeadlineMaximum = createSelector(
  selectEventCareDaysCommittedFirstDay,
  (day) => {
    if (!day) {
      return undefined;
    }

    const firstDayTime = new Date(day.day);
    const oneDayBefore = new Date(firstDayTime.getTime() - 60 * 60 * 24 * 1000);

    return `${oneDayBefore.getFullYear()}-${(oneDayBefore.getMonth() + 1)
      .toString()
      .padStart(2, '0')}-${oneDayBefore.getDate().toString().padStart(2, '0')}`;
  }
);

export const selectRegistrationDeadlineMinimum = createSelector(
  selectEventCareDaysCommittedFirstDay,
  (day) => {
    if (!day) {
      return undefined;
    }

    const firstDayTime = new Date(day.day);
    const oneDayBefore = new Date(
      firstDayTime.getTime() - 28 * 60 * 60 * 24 * 1000
    );

    return `${oneDayBefore.getFullYear()}-${(oneDayBefore.getMonth() + 1)
      .toString()
      .padStart(2, '0')}-${oneDayBefore.getDate().toString().padStart(2, '0')}`;
  }
);
