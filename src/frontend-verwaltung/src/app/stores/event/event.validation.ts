import { InputForm } from './event.reducer';
import {
  updateGroup,
  validate,
  FormGroupState,
  StateUpdateFns,
  disable,
  enable,
} from 'ngrx-forms';
import {
  greaterThanOrEqualTo,
  minLength,
  pattern,
  required,
} from 'ngrx-forms/validation';
import { CarePeriod, Period } from './event.types';

export const validateForm = (form: FormGroupState<InputForm>) => {
  if (!form.value.role) {
    return updateGroup<InputForm>({})(form);
  }

  if (form.value.role === 'administrator') {
    return updateGroup<InputForm>({
      organizer: validate(required),
      name: validate(required, minLength(1)),
    })(form);
  }

  if (form.value.status === '') {
    return updateGroup<InputForm>({
      ...requiredFieldsRequired(),
    })(form);
  }

  if (form.value.role === 'organizer') {
    if (form.value.status === 'new') {
      return updateGroup<InputForm>({
        ...allFieldsRequired(),
      })(form);
    }

    if (form.value.status === 'planned') {
      return updateGroup<InputForm>({
        ...allFieldsRequired(),
        name: disable,
        carePeriodCommitted: disable,
        ageMinimum: disable,
        ageMaximum: disable,
        participantMinimum: disable,
        participantLimit: disable,
        waitingListLimit: disable,
        costs: validate(required, greaterThanOrEqualTo(1)),
        registrationDeadline: validate(required, minLength(1)),
      })(form);
    }

    if (['check', 'approved', 'cancelled'].includes(form.value.status)) {
      return updateGroup<InputForm>({
        name: disable,
        description: disable,
        picture: disable,
        pictureRights: disable,
        categories: disable,
        hints: disable,
        meals: disable,
        locationZipcode: disable,
        locationCity: disable,
        locationStreet: disable,
        carePeriodDesired: disable,
        carePeriodAlternative: disable,
        careTimeStart: disable,
        careTimeEnd: disable,
        emergencyPhone: disable,
        contactPersonSalutation: disable,
        contactPersonFirstname: disable,
        contactPersonSurname: disable,
        carePeriodCommitted: disable,
        ageMinimum: disable,
        ageMaximum: disable,
        participantMinimum: disable,
        participantLimit: disable,
        waitingListLimit: disable,
        costs: disable,
        registrationDeadline: disable,
      })(form);
    }
  }

  return updateGroup<InputForm>({ ...requiredFieldsRequired() })(form);
};

function allFieldsRequired(): StateUpdateFns<InputForm> {
  return {
    ...requiredFieldsRequired(),
    hints: validate(required, minLength(1)),
    meals: validate(required, minLength(1)),
    locationZipcode: validate(required, minLength(1)),
    locationCity: validate(required, minLength(1)),
    locationStreet: validate(required, minLength(1)),
    waitingListLimit: validate(required, greaterThanOrEqualTo(0)),
    contactPersonSalutation: validate(required),
    contactPersonFirstname: validate(required, minLength(1)),
    contactPersonSurname: validate(required, minLength(1)),
    emergencyPhone: validate(required, minLength(1)),
  };
}

function requiredFieldsRequired(): StateUpdateFns<InputForm> {
  return {
    name: validate(required, minLength(1)),
    description: validate(required, minLength(1)),
    careTimeStart: validate(
      required,
      minLength(1),
      pattern(/^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/)
    ),
    careTimeEnd: validate(
      required,
      minLength(1),
      pattern(/^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/)
    ),
    ageMinimum: validate(required, greaterThanOrEqualTo(1)),
    ageMaximum: validate(required, greaterThanOrEqualTo(1)),
    participantMinimum: validate(required, greaterThanOrEqualTo(1)),
    participantLimit: validate(required, greaterThanOrEqualTo(1)),
    waitingListLimit: validate(required, greaterThanOrEqualTo(0)),
    organizer: validate(required),
    carePeriodDesired: updateGroup<CarePeriod>({
      period: updateGroup<Period>({
        id: validate(required),
      }),
      careDays: validate(required, minLength(1)),
    }),
    carePeriodAlternative: (x, y) =>
      x.value.period.id
        ? updateGroup<CarePeriod>({
            careDays: validate(required, minLength(1)),
          })(x)
        : updateGroup<CarePeriod>({
            careDays: validate(minLength(0)),
          })(x),
  };
}
