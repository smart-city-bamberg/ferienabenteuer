import { createActionGroup, emptyProps, props } from '@ngrx/store';
import {
  CarePeriod,
  Event,
  EventCategory,
  EventTicket,
  Holiday,
  Organizer,
  Period,
  Ticket,
} from './event.types';
import { CarePeriodType } from './event.reducer';

export const EventActions = createActionGroup({
  source: 'Event',
  events: {
    'Load Event': props<{ eventId: string; role: string }>(),
    'Load Event Success': props<{
      event: Event;
      eventTickets: EventTicket[];
      cancelledTickets: Ticket[];
      organizers: Organizer[];
      holidays: Holiday[];
      eventCategories: EventCategory[];
    }>(),
    'Load Event Failure': props<{ error: unknown }>(),

    'Load Event Relations': props<{ role: string }>(),
    'Load Event Relations Success': props<{
      organizers: Organizer[];
      holidays: Holiday[];
      eventCategories: EventCategory[];
    }>(),
    'Load Event Relations Failure': props<{ error: unknown }>(),

    'Toggle Care Date': props<{
      field: CarePeriodType;
      date: Date;
    }>(),
    'Set Care Days': props<{
      field: CarePeriodType;
      careDays: {
        day: string;
      }[];
    }>(),
    'Toggle Category': props<{
      id: string;
    }>(),
    'Save Event': emptyProps(),
    'Save Event Cancelled': emptyProps(),
    'Next Event': emptyProps(),
    'Create Event Success': props<{ id: string; status: string }>(),
    'Create Event Failure': props<{ error: any }>(),
    'Update Event Success': props<{
      id: string;
      status: string;
      picture: { url: string } | undefined;
    }>(),
    'Update Event Failure': props<{ error: any }>(),
    'Reset Editor': props<{ role: string }>(),
    'Set Photo File': props<{ file: File }>(),
    'Remove Photo': emptyProps(),
    'Select Organizer': props<{ organizer: string }>(),
    'Update State': props<{ status: string }>(),
    'Update State Success': props<{ status: string }>(),
    'Update State Failure': props<{ error: unknown }>(),
    'Delete Event': emptyProps(),
    'Delete Event Success': props<{ id: string }>(),
    'Delete Event Failure': props<{ error: unknown }>(),

    'Cancel Ticket': props<{ ticketId: string }>(),
    'Cancel Ticket Success': props<{ eventTickets: EventTicket[] }>(),
    'Cancel Ticket Failure': props<{ error: unknown }>(),

    'Participants Sort': props<{ column: string }>(),
    'Waitinglist Sort': props<{ column: string }>(),
    'Cancelled Sort': props<{ column: string }>(),

    'Move Waiting List Ticket To Participant List': props<{
      ticketId: string;
    }>(),
    'Move Waiting List Ticket To Participant List Success': props<{
      eventTickets: EventTicket[];
    }>(),
    'Move Waiting List Ticket To Participant List No Blocked Ticket Available':
      props<{
        ticketId: string;
      }>(),
    'Move Waiting List Ticket To Participant List Failure': props<{
      error: unknown;
    }>(),

    'Unblock Event Ticket': props<{ eventTicketId: string }>(),
    'Unblock Event Ticket Success': props<{ eventTicket: EventTicket }>(),
    'Unblock Event Ticket Not Blocked': props<{ eventTicketId: string }>(),
    'Unblock Event Ticket Failure': props<{ error: unknown }>(),

    'Export Event Participants': emptyProps(),
    'Export Event Participants Success': emptyProps(),
    'Export Event Participants Failure': props<{ error: unknown }>(),
  },
});
