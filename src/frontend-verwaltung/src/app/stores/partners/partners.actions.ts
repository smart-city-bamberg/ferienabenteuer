import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { EarlyAccessCode, EmailTemplate, Partner } from './partners.types';

export const PartnersActions = createActionGroup({
  source: 'Partners',
  events: {
    'Load Partners': emptyProps(),
    'Load Partners Success': props<{
      partners: Partner[];
      earlyAccessCodeNotificationEmailTemplate: EmailTemplate;
    }>(),
    'Load Partners Failure': props<{ error: unknown }>(),

    'Sort By': props<{ column: string }>(),
    'Change Search Text': props<{ searchText: string }>(),

    'Generate Early Access Codes': emptyProps(),
    'Generate Early Access Codes Rejected': emptyProps(),
    'Generate Early Access Codes Success': props<{
      codes: EarlyAccessCode[];
    }>(),
    'Generate Early Access Codes Failure': props<{ error: unknown }>(),

    'Open Send Early Access Code': props<{ partnerId: string }>(),

    'Mark Early Access Code Notified Rejected': emptyProps(),
    'Mark Early Access Code Notified Success': props<{
      earlyAccessCode: EarlyAccessCode;
    }>(),
    'Mark Early Access Code Notified Failure': props<{ error: unknown }>(),
  },
});
