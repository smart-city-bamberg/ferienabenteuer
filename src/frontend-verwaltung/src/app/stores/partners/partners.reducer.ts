import { createFeature, createReducer, on } from '@ngrx/store';
import { PartnersActions } from './partners.actions';
import { EarlyAccessCode, EmailTemplate, Partner } from './partners.types';
import { SortByState, defaultSortByState, sortByReducer } from '../sorting';

export const partnersFeatureKey = 'partners';

export interface State {
  partners: Partner[];
  sort: SortByState;
  searchText: string;
  earlyAccessCodeNotificationEmailTemplate: EmailTemplate | undefined;
}

export const initialState: State = {
  partners: [],
  sort: { ...defaultSortByState, by: 'name' },
  searchText: '',
  earlyAccessCodeNotificationEmailTemplate: undefined,
};

export const reducer = createReducer(
  initialState,
  on(PartnersActions.loadPartnersSuccess, (state, action) => ({
    ...state,
    partners: [...action.partners],
    earlyAccessCodeNotificationEmailTemplate:
      action.earlyAccessCodeNotificationEmailTemplate,
  })),
  on(PartnersActions.generateEarlyAccessCodesSuccess, (state, action) => ({
    ...state,
    partners: state.partners.map((partner) => ({
      ...partner,
      earlyAccessCode: [
        action.codes.find(
          (code) => code.partner.id === partner.id
        ) as EarlyAccessCode,
      ],
    })),
  })),
  on(PartnersActions.markEarlyAccessCodeNotifiedSuccess, (state, action) => ({
    ...state,
    partners: state.partners.map((partner) =>
      partner.id === action.earlyAccessCode.partner.id
        ? {
            ...partner,
            earlyAccessCode: [action.earlyAccessCode],
          }
        : partner
    ),
  })),
  on(PartnersActions.sortBy, (state, action) => ({
    ...state,
    sort: sortByReducer(state.sort, action.column),
  })),
  on(PartnersActions.changeSearchText, (state, action) => ({
    ...state,
    searchText: action.searchText,
  }))
);

export const partnersFeature = createFeature({
  name: partnersFeatureKey,
  reducer,
});
