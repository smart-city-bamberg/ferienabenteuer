import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromPartners from './partners.reducer';
import { EmailTemplate, Partner } from './partners.types';
import { stringComparer } from '../sorting';

export const selectPartnersState = createFeatureSelector<fromPartners.State>(
  fromPartners.partnersFeatureKey
);

export const selectPartners = createSelector(
  selectPartnersState,
  (state) => state.partners
);

export const selectPartner = (id: string) =>
  createSelector(
    selectPartners,
    (partners) => partners.find((partner) => partner.id === id) as Partner
  );

export const selectSortBy = createSelector(
  selectPartnersState,
  (state) => state.sort
);

export const selectSearchText = createSelector(
  selectPartnersState,
  (state) => state.searchText
);

const sorters: {
  [index: string]: (a: Partner, b: Partner) => number;
} = {
  name: stringComparer((e) => e.name),
  phone: stringComparer((e) => e.phone),
  email: stringComparer((e) => e.email),
  contact: stringComparer(
    (e) => `${e.contactPersonSurname}, ${e.contactPersonFirstname}`
  ),
  code: stringComparer((e) =>
    e.earlyAccessCode.length > 0 ? e.earlyAccessCode[0].code : ''
  ),
};

export const selectPartnersSorted = createSelector(
  selectPartners,
  selectSortBy,
  selectSearchText,
  (partners, sortBy, searchText) => {
    const filteredPartners = partners.filter((partner) =>
      JSON.stringify(partner).toLowerCase().includes(searchText.toLowerCase())
    );

    return sorters[sortBy.by]
      ? [...filteredPartners].sort(
          (a, b) => (sortBy.descending ? -1 : 1) * sorters[sortBy.by](a, b)
        )
      : filteredPartners;
  }
);

export const selectEarlyAccessCodeNotificationEmailTemplate = createSelector(
  selectPartnersState,
  (state) => state.earlyAccessCodeNotificationEmailTemplate as EmailTemplate
);
