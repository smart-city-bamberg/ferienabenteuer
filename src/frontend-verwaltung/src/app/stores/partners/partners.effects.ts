import { selectCurrentSeason } from './../seasons/seasons.selectors';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  catchError,
  map,
  concatMap,
  filter,
  withLatestFrom,
  switchMap,
  tap,
} from 'rxjs/operators';
import { of } from 'rxjs';
import { PartnersActions } from './partners.actions';
import {
  GenerateEarlyAccessCodesGQL,
  MarkEarlyAccessCodeNotifiedGQL,
  PartnersGQL,
} from 'src/app/graphql/generated';
import { EarlyAccessCode, EmailTemplate, Partner } from './partners.types';
import { routerNavigatedAction } from '@ngrx/router-store';
import { SeasonsActions } from '../seasons/seasons.actions';
import { Store } from '@ngrx/store';
import {
  selectEarlyAccessCodeNotificationEmailTemplate,
  selectPartner,
} from './partners.selectors';

import mustache from 'mustache';
mustache.escape = (input: string) => input;

const monthNames = [
  'Januar',
  'Februar',
  'März',
  'April',
  'Mai',
  'Juni',
  'Juli',
  'August',
  'September',
  'Oktober',
  'November',
  'Dezember',
];

@Injectable()
export class PartnersEffects {
  loadPartners$ = createEffect(() =>
    this.actions$.pipe(
      ofType(PartnersActions.loadPartners),
      withLatestFrom(this.store.select(selectCurrentSeason)),
      filter(([action, season]) => season !== undefined),
      concatMap(([action, season]) =>
        this.partnersQuery
          .fetch({
            seasonId: season?.id as string,
          })
          .pipe(
            map((response) =>
              PartnersActions.loadPartnersSuccess({
                partners: response.data.partners as Partner[],
                earlyAccessCodeNotificationEmailTemplate: response.data
                  .earlyAccessCodeNotificationEmailTemplate as EmailTemplate,
              })
            ),
            catchError((error) =>
              of(PartnersActions.loadPartnersFailure({ error }))
            )
          )
      )
    )
  );

  generateEarlyAccessCodes$ = createEffect(() =>
    this.actions$.pipe(
      ofType(PartnersActions.generateEarlyAccessCodes),
      withLatestFrom(this.store.select(selectCurrentSeason)),
      filter(([action, season]) => season !== undefined),
      concatMap(([action, season]) =>
        !window.confirm(
          `Möchten Sie wirklich Frühbuchercodes für die Saison ${season?.year} erzeugen?`
        )
          ? of(PartnersActions.generateEarlyAccessCodesRejected())
          : this.generateEarlyAccessCodesMutation
              .mutate({
                seasonId: season?.id as string,
              })
              .pipe(
                map((response) =>
                  PartnersActions.generateEarlyAccessCodesSuccess({
                    codes: response.data
                      ?.generateEarlyAccessCodes as EarlyAccessCode[],
                  })
                ),
                catchError((error) =>
                  of(PartnersActions.generateEarlyAccessCodesFailure({ error }))
                )
              )
      )
    )
  );

  loadPartnersAfterNavigate$ = createEffect(() =>
    this.actions$.pipe(
      ofType(routerNavigatedAction),
      filter((action) =>
        action.payload.routerState.url.startsWith('/partnerunternehmen')
      ),
      map(() => PartnersActions.loadPartners())
    )
  );

  loadPartnersAfterSeasonChanged$ = createEffect(() =>
    this.actions$.pipe(
      ofType(
        SeasonsActions.nextSeason,
        SeasonsActions.previousSeason,
        SeasonsActions.selectSeason
      ),
      map(() => PartnersActions.loadPartners())
    )
  );

  openPaymentOverdueEmail$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(PartnersActions.openSendEarlyAccessCode),
        withLatestFrom(
          this.store.select(selectEarlyAccessCodeNotificationEmailTemplate),
          this.store.select(selectCurrentSeason)
        ),
        switchMap(([action, template, season]) =>
          of({ action, template }).pipe(
            withLatestFrom(this.store.select(selectPartner(action.partnerId))),
            tap(([{ action, template }, partner]) => {
              const bookingStartDate = new Date(season!.bookingStart);
              const bookingStart = `${bookingStartDate.getDate()} ${
                monthNames[bookingStartDate.getMonth()]
              }`;

              const bookingStartPartnersDate = new Date(
                season!.bookingStartPartners
              );
              const bookingStartPartners = `${bookingStartPartnersDate.toLocaleDateString()}`;

              const bookingStartPartnersTime = `${bookingStartPartnersDate
                .getHours()
                .toString()
                .padStart(2, '0')}:${bookingStartPartnersDate
                .getMinutes()
                .toString()
                .padStart(2, '0')}`;

              const content = mustache.render(template.content, {
                Code: partner.earlyAccessCode[0].code,
                Anrede:
                  partner.contactPersonSalutation === 'mr'
                    ? 'Sehr geehrter Herr'
                    : partner.contactPersonSalutation == 'ms'
                    ? 'Sehr geehrte Frau'
                    : 'Sehr geehrter Herr/Sehr geehrte Frau',
                Vorname: partner.contactPersonFirstname.trim(),
                Nachname: partner.contactPersonSurname.trim(),
                StartFrühbucherDatum: bookingStartPartners,
                StartFrühbucherUhrzeit: bookingStartPartnersTime,
                StartRegulär: bookingStart,
              });

              const mailtoLink = `mailto:${encodeURIComponent(
                partner.email
              )}?subject=${encodeURIComponent(
                template.subject
              )}&body=${encodeURIComponent(content)}`;

              window.location.href = mailtoLink;
            })
          )
        )
      ),
    {
      dispatch: false,
    }
  );

  markEarlyAccessCodeNotifiedReminded$ = createEffect(() =>
    this.actions$.pipe(
      ofType(PartnersActions.openSendEarlyAccessCode),
      switchMap((action) =>
        of({ action }).pipe(
          withLatestFrom(this.store.select(selectPartner(action.partnerId))),
          concatMap(([action, partner]) =>
            !window.confirm(
              'Soll der Frühbuchercode als verschickt markiert werden?'
            )
              ? of(PartnersActions.markEarlyAccessCodeNotifiedRejected())
              : this.markEarlyAccessCodeNotifiedMutation
                  .mutate({
                    earlyAccessCodeId: partner.earlyAccessCode[0].id,
                  })
                  .pipe(
                    map((response) =>
                      PartnersActions.markEarlyAccessCodeNotifiedSuccess({
                        earlyAccessCode: response.data
                          ?.markEarlyAccessCodeNotified as EarlyAccessCode,
                      })
                    ),
                    catchError((error) =>
                      of(
                        PartnersActions.markEarlyAccessCodeNotifiedFailure({
                          error,
                        })
                      )
                    )
                  )
          )
        )
      )
    )
  );

  constructor(
    private actions$: Actions,
    private store: Store,
    private partnersQuery: PartnersGQL,
    private generateEarlyAccessCodesMutation: GenerateEarlyAccessCodesGQL,
    private markEarlyAccessCodeNotifiedMutation: MarkEarlyAccessCodeNotifiedGQL
  ) {}
}
