export interface Partner {
  id: string;
  name: string;
  contactPersonSalutation: string;
  contactPersonFirstname: string;
  contactPersonSurname: string;
  email: string;
  phone: string;
  earlyAccessCode: EarlyAccessCode[];
}

export interface EarlyAccessCode {
  id: string;
  code: string;
  notificationDate: string;
  partner: {
    id: string;
  };
}

export interface EmailTemplate {
  id: string;
  role: string;
  subject: string;
  content: string;
}
