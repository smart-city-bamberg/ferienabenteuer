import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import * as fromPartners from './partners.reducer';
import { EffectsModule } from '@ngrx/effects';
import { PartnersEffects } from './partners.effects';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StoreModule.forFeature(fromPartners.partnersFeatureKey, fromPartners.reducer),
    EffectsModule.forFeature([PartnersEffects])
  ]
})
export class PartnersModule { }
