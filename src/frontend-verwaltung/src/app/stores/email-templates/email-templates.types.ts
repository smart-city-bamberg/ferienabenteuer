export interface EmailTemplate {
  id: string;
  role: string;
  subject: string;
  content: string;
}

export const emailTemplateNames = [
  {
    label: 'Benutzer registriert',
    value: 'userRegistered',
  },
  {
    label: 'Konto verifiziert',
    value: 'userVerifiedAccount',
  },
  {
    label: 'Teilnehmerplatz reserviert',
    value: 'ticketReserved',
  },
  {
    label: 'Ticket storniert',
    value: 'ticketCancelled',
  },
  {
    label: 'Warteplatz reserviert',
    value: 'waitingListTicketReserved',
  },
  {
    label: 'Warteplatz in Warteliste vorgerückt',
    value: 'waitingListTicketSlotAdvanced',
  },
  {
    label: 'Warteplatz in Teilnehmerliste nachgerückt',
    value: 'waitingListTicketConvertedToBooking',
  },
  {
    label: 'Warteplatz in Teilnehmerliste nachgerückt (Reservierung)',
    value: 'waitingListTicketConvertedToParticipant',
  },
  {
    label: 'Buchung eingegangen',
    value: 'bookingReceived',
  },
  {
    label: 'Buchung bestätigt',
    value: 'bookingConfirmed',
  },
  {
    label: 'Buchung storniert',
    value: 'bookingCancelled',
  },
  {
    label: 'Buchung Assistenz benötigt',
    value: 'bookingAssistanceRequired',
  },
  {
    label: 'Reservierung storniert',
    value: 'reservationCancelled',
  },
  {
    label: 'Veranstaltung abgesagt',
    value: 'eventCancelled',
  },
  {
    label: 'Veranstaltung beendet',
    value: 'eventFinished',
  },
  {
    label: 'Ermäßigung beantragt',
    value: 'discountApplicationReceived',
  },
  {
    label: 'Ermäßigung gewährt',
    value: 'discountApplicationGranted',
  },
  {
    label: 'Ermäßigung abgelehnt',
    value: 'discountApplicationRejected',
  },
  {
    label: 'Ermäßigungsnachweis Erinnerung',
    value: 'discountApplicationReminder',
  },
  {
    label: 'Zahlung erfasst',
    value: 'paymentRegistered',
  },
  {
    label: 'Zahlung überfällig',
    value: 'paymentOverdue',
  },
  {
    label: 'Frühbuchercode senden',
    value: 'earlyAccessCodeNotification',
  },
  {
    label: 'Passwort zurücksetzen',
    value: 'passwordResetRequested',
  },
  {
    label: 'Konto wird bald gelöscht wegen Inaktivität',
    value: 'accountMarkedForDeletedDueToInactivity',
  },
  {
    label: 'Konto gelöscht',
    value: 'accountDeleted',
  },
  {
    label: 'Konto gelöscht wegen Inaktivität',
    value: 'accountDeletedDueToInactivity',
  },
];
