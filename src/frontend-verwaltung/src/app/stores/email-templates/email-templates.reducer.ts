import { createFeature, createReducer, on } from '@ngrx/store';
import { EmailTemplatesActions } from './email-templates.actions';
import { EmailTemplate } from './email-templates.types';
import {
  FormGroupState,
  createFormGroupState,
  onNgrxForms,
  setValue,
  updateGroup,
  validate,
  wrapReducerWithFormStateUpdate,
} from 'ngrx-forms';
import { required } from 'ngrx-forms/validation';

export const emailTemplatesFeatureKey = 'emailTemplates';

export interface State {
  templates: EmailTemplate[];
  selectedTemplate: string | undefined;
  editorForm: FormGroupState<EmailTemplate>;
}

export const initialState: State = {
  templates: [],
  selectedTemplate: undefined,
  editorForm: createFormGroupState<EmailTemplate>('email_template_form', {
    id: '',
    content: '',
    subject: '',
    role: '',
  }),
};

export const reducer = wrapReducerWithFormStateUpdate(
  createReducer(
    initialState,
    on(EmailTemplatesActions.loadEmailTemplatesSuccess, (state, action) => ({
      ...state,
      templates: [...action.templates],
    })),
    on(EmailTemplatesActions.selectEmailTemplate, (state, action) => ({
      ...state,
      selectedTemplate: action.template,
      editorForm: setValue({
        ...(state.templates.find((t) => t.role === action.template) ?? {
          ...initialState.editorForm.value,
          role: action.template,
        }),
      })(state.editorForm),
    })),
    on(EmailTemplatesActions.saveEmailTemplateSuccess, (state, action) => ({
      ...state,
      templates: state.templates.map((template) =>
        template.id === action.template.id ? action.template : template
      ),
    })),
    onNgrxForms()
  ),
  (state) => state.editorForm,
  (state) =>
    updateGroup<EmailTemplate>({
      content: validate(required),
      subject: validate(required),
    })(state)
);

export const emailTemplatesFeature = createFeature({
  name: emailTemplatesFeatureKey,
  reducer,
});
