import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { EmailTemplate } from './email-templates.types';

export const EmailTemplatesActions = createActionGroup({
  source: 'EmailTemplates',
  events: {
    'Load Email Templates': emptyProps(),
    'Load Email Templates Success': props<{ templates: EmailTemplate[] }>(),
    'Load Email Templates Failure': props<{ error: unknown }>(),

    'Select Email Template': props<{ template: string }>(),

    'Save Email Template': emptyProps(),
    'Save Email Template Success': props<{ template: EmailTemplate }>(),
    'Save Email Template Failure': props<{ error: unknown }>(),
  },
});
