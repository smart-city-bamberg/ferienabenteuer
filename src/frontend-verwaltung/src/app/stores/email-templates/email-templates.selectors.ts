import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromEmailTemplates from './email-templates.reducer';
import { emailTemplateNames } from './email-templates.types';

export const selectEmailTemplatesState =
  createFeatureSelector<fromEmailTemplates.State>(
    fromEmailTemplates.emailTemplatesFeatureKey
  );

export const selectSelectedEmailTemplate = createSelector(
  selectEmailTemplatesState,
  (state) => state.selectedTemplate
);

export const selectFormState = createSelector(
  selectEmailTemplatesState,
  (state) => state.editorForm
);

export const selectTemplates = createSelector(
  selectEmailTemplatesState,
  (state) => state.templates
);

export const selectTemplatesWithContent = createSelector(
  selectTemplates,
  selectSelectedEmailTemplate,
  (templates, selected) =>
    emailTemplateNames
      .map((pair) => ({
        ...pair,
        isSelected: pair.value === selected,
        hasContent: templates.some((t) => t.role === pair.value),
      }))
      .sort((a, b) => a.label.localeCompare(b.label))
);
