import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import * as fromEmailTemplates from './email-templates.reducer';
import { EffectsModule } from '@ngrx/effects';
import { EmailTemplatesEffects } from './email-templates.effects';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StoreModule.forFeature(fromEmailTemplates.emailTemplatesFeatureKey, fromEmailTemplates.reducer),
    EffectsModule.forFeature([EmailTemplatesEffects])
  ]
})
export class EmailTemplatesModule { }
