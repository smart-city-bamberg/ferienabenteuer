import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  catchError,
  map,
  concatMap,
  filter,
  withLatestFrom,
  tap,
} from 'rxjs/operators';
import { of } from 'rxjs';
import { EmailTemplatesActions } from './email-templates.actions';
import {
  CreateEmailTemplateGQL,
  EmailTemplatesGQL,
  UpdateEmailTemplateGQL,
} from 'src/app/graphql/generated';
import { EmailTemplate } from './email-templates.types';
import { routerNavigatedAction } from '@ngrx/router-store';
import { Store } from '@ngrx/store';
import { selectFormState } from './email-templates.selectors';
import { MessageService } from 'primeng/api';

@Injectable()
export class EmailTemplatesEffects {
  loadEmailTemplates$ = createEffect(() =>
    this.actions$.pipe(
      ofType(EmailTemplatesActions.loadEmailTemplates),
      concatMap(() =>
        this.emailTemplatesQuery.fetch().pipe(
          map((response) =>
            EmailTemplatesActions.loadEmailTemplatesSuccess({
              templates: response.data.emailTemplates as EmailTemplate[],
            })
          ),
          catchError((error) =>
            of(EmailTemplatesActions.loadEmailTemplatesFailure({ error }))
          )
        )
      )
    )
  );

  saveEmailTemplate$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(EmailTemplatesActions.saveEmailTemplate),
      withLatestFrom(this.store.select(selectFormState)),
      concatMap(([action, form]) =>
        form.value.id === ''
          ? this.createEmailTemplateMutation
              .mutate({
                data: {
                  role: form.value.role,
                  subject: form.value.subject,
                  content: form.value.content,
                },
              })
              .pipe(
                map((response) =>
                  EmailTemplatesActions.saveEmailTemplateSuccess({
                    template: response.data
                      ?.createEmailTemplate as EmailTemplate,
                  })
                ),
                catchError((error) =>
                  of(EmailTemplatesActions.saveEmailTemplateFailure({ error }))
                )
              )
          : this.updateEmailTemplateMutation
              .mutate({
                where: {
                  id: form.value.id,
                },
                data: {
                  role: form.value.role,
                  subject: form.value.subject,
                  content: form.value.content,
                },
              })
              .pipe(
                map((response) =>
                  EmailTemplatesActions.saveEmailTemplateSuccess({
                    template: response.data
                      ?.updateEmailTemplate as EmailTemplate,
                  })
                ),
                catchError((error) =>
                  of(EmailTemplatesActions.saveEmailTemplateFailure({ error }))
                )
              )
      )
    );
  });

  loadTemplatesAfterNavigate$ = createEffect(() =>
    this.actions$.pipe(
      ofType(routerNavigatedAction),
      filter((action) => action.payload.routerState.url === '/vorlagen'),
      map((action) => EmailTemplatesActions.loadEmailTemplates())
    )
  );

  selectFirstTemplateAfterLoad$ = createEffect(() =>
    this.actions$.pipe(
      ofType(EmailTemplatesActions.loadEmailTemplatesSuccess),
      map((action) =>
        EmailTemplatesActions.selectEmailTemplate({
          template: action.templates[0].role,
        })
      )
    )
  );

  showToastAfterRegisterPaymentSuccess$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(EmailTemplatesActions.saveEmailTemplateSuccess),
        tap((action) =>
          this.toastService.add({
            summary: `E-Mail Template wurde gespeichert`,
            severity: 'success',
          })
        )
      );
    },
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private store: Store,
    private toastService: MessageService,
    private emailTemplatesQuery: EmailTemplatesGQL,
    private createEmailTemplateMutation: CreateEmailTemplateGQL,
    private updateEmailTemplateMutation: UpdateEmailTemplateGQL
  ) {}
}
