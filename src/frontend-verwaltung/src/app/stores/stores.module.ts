import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthModule } from './auth/auth.module';
import { GraphQLModule } from '../graphql/graphql.module';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { reducers, metaReducers } from './';
import { isDevMode } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { EventsModule } from './events/events.module';
import { BookingsModule } from './bookings/bookings.module';
import { DiscountsModule } from './discounts/discounts.module';
import { EventModule } from './event/event.module';
import { HolidaysModule } from './holidays/holidays.module';
import { OrganizersModule } from './organizers/organizers.module';
import { PrimeModule } from '../prime/prime.module';
import { EmailTemplatesModule } from './email-templates/email-templates.module';
import { ReservationsModule } from './reservations/reservations.module';
import { SeasonsModule } from './seasons/seasons.module';
import { CustodiansModule } from './custodians/custodians.module';
import { PartnersModule } from './partners/partners.module';
import { InvoicesModule } from './invoices/invoices.module';
import { ProfileModule } from './profile/profile.module';

@NgModule({
  declarations: [],
  imports: [
    AuthModule,
    EventModule,
    EventsModule,
    BookingsModule,
    ReservationsModule,
    DiscountsModule,
    HolidaysModule,
    OrganizersModule,
    EmailTemplatesModule,
    SeasonsModule,
    CustodiansModule,
    PartnersModule,
    InvoicesModule,
    ProfileModule,
    CommonModule,
    HttpClientModule,
    GraphQLModule,
    PrimeModule,
    StoreModule.forRoot(reducers, {
      metaReducers,
    }),
    EffectsModule.forRoot([]),
    StoreRouterConnectingModule.forRoot(),
    StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: !isDevMode() }),
  ],
})
export class StoresModule {}
