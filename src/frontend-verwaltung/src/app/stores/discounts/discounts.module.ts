import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import * as fromDiscounts from './discounts.reducer';
import { EffectsModule } from '@ngrx/effects';
import { DiscountsEffects } from './discounts.effects';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StoreModule.forFeature(fromDiscounts.discountsFeatureKey, fromDiscounts.reducer),
    EffectsModule.forFeature([DiscountsEffects])
  ]
})
export class DiscountsModule { }
