import { defaultSortByState } from './../sorting';
import { createFeature, createReducer, on } from '@ngrx/store';
import { DiscountsActions } from './discounts.actions';
import { Discount, DiscountRejectionReason } from './discounts.types';
import { SortByState, sortByReducer } from '../sorting';

export const discountsFeatureKey = 'discounts';

export interface State {
  discounts: Discount[];
  sort: SortByState;
  searchText: string;
  rejectionReasons: DiscountRejectionReason[];
  rejection: {
    discountId: string | undefined;
    reasonId: string | undefined;
    note: string | undefined;
  };
  showRejectionForm: boolean;
}

export const initialState: State = {
  discounts: [],
  sort: {
    ...defaultSortByState,
    by: 'status',
  },
  searchText: '',
  rejectionReasons: [],
  rejection: {
    discountId: undefined,
    reasonId: undefined,
    note: undefined,
  },
  showRejectionForm: false,
};

export const reducer = createReducer(
  initialState,
  on(DiscountsActions.loadDiscounts, (state) => state),
  on(DiscountsActions.loadDiscountsSuccess, (state, action) => ({
    ...state,
    discounts: [...action.discounts],
    rejectionReasons: [...action.rejectionReasons],
  })),
  on(DiscountsActions.loadDiscountsFailure, (state, action) => state),
  on(DiscountsActions.sortBy, (state, action) => ({
    ...state,
    sort: sortByReducer(state.sort, action.column),
  })),
  on(
    DiscountsActions.approveDiscountSuccess,
    DiscountsActions.rejectDiscountSuccess,
    DiscountsActions.sendDiscountApplicationReminderSuccess,
    (state, action) => ({
      ...state,
      discounts: state.discounts.map((discount) =>
        discount.id === action.discount.id ? action.discount : discount
      ),
      rejection: initialState.rejection,
      showRejectionForm: false,
    })
  ),
  on(DiscountsActions.rejectDiscountFormShow, (state, action) => ({
    ...state,
    rejection: {
      discountId: action.discountId,
      reasonId: undefined,
      note: undefined,
    },
    showRejectionForm: true,
  })),
  on(DiscountsActions.rejectDiscountFormCancel, (state, action) => ({
    ...state,
    rejection: initialState.rejection,
    showRejectionForm: false,
  })),
  on(DiscountsActions.rejectDiscountFormSelectReason, (state, action) => ({
    ...state,
    rejection: {
      ...state.rejection,
      reasonId: action.reasonId,
      note: state.rejectionReasons.find(
        (reason) => reason.id === action.reasonId
      )?.description,
    },
  })),
  on(DiscountsActions.rejectDiscountFormUpdateNote, (state, action) => ({
    ...state,
    rejection: {
      ...state.rejection,
      note: action.note,
    },
  })),
  on(DiscountsActions.changeSearchText, (state, action) => ({
    ...state,
    searchText: action.searchText,
  }))
);

export const discountsFeature = createFeature({
  name: discountsFeatureKey,
  reducer,
});
