import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { Discount, DiscountRejectionReason } from './discounts.types';

export const DiscountsActions = createActionGroup({
  source: 'Discounts',
  events: {
    'Load Discounts': emptyProps(),
    'Load Discounts Success': props<{
      discounts: Discount[];
      rejectionReasons: DiscountRejectionReason[];
    }>(),
    'Load Discounts Failure': props<{ error: Error }>(),
    'Sort By': props<{ column: string }>(),

    'Approve Discount': props<{ discountId: string }>(),
    'Approve Discount Success': props<{
      discount: Discount;
    }>(),
    'Approve Discount Can Only Approve Pending Or Overdue': props<{
      discountId: string;
    }>(),
    'Approve Discount Failure': props<{ error: unknown }>(),

    'Reject Discount Form Show': props<{
      discountId: string;
    }>(),

    'Reject Discount Form Cancel': emptyProps(),
    'Reject Discount Form Rejected Select Reason': emptyProps(),

    'Reject Discount Form Select Reason': props<{
      reasonId: string;
    }>(),

    'Reject Discount Form Update Note': props<{
      note: string;
    }>(),

    'Reject Discount': emptyProps(),
    'Reject Discount Success': props<{
      discount: Discount;
    }>(),
    'Reject Discount Can Only Reject Pending Or Overdue': props<{
      discountId: string;
    }>(),
    'Reject Discount Failure': props<{ error: unknown }>(),

    'Send Discount Application Reminder': props<{ discountId: string }>(),
    'Send Discount Application Reminder Success': props<{
      discount: Discount;
    }>(),
    'Send Discount Application Reminder Failure': props<{ error: unknown }>(),

    'Change Search Text': props<{ searchText: string }>(),
  },
});
