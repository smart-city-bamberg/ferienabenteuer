import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  catchError,
  map,
  concatMap,
  withLatestFrom,
  tap,
  filter,
} from 'rxjs/operators';
import { of } from 'rxjs';
import { DiscountsActions } from './discounts.actions';
import {
  ApproveDiscountGQL,
  DiscountsGQL,
  RejectDiscountGQL,
  SendDiscountApplicationReminderGQL,
} from 'src/app/graphql/generated';
import { Discount, DiscountRejectionReason } from './discounts.types';
import { Store } from '@ngrx/store';
import { selectRejection } from './discounts.selectors';
import { MessageService } from 'primeng/api';
import { selectCurrentSeason } from '../seasons/seasons.selectors';
import { SeasonsActions } from '../seasons/seasons.actions';

@Injectable()
export class DiscountsEffects {
  loadDiscounts$ = createEffect(() =>
    this.actions$.pipe(
      ofType(DiscountsActions.loadDiscounts),
      withLatestFrom(this.store.select(selectCurrentSeason)),
      filter(([action, season]) => season !== undefined),
      concatMap(([action, season]) =>
        this.discountsQuery
          .fetch({
            where: {
              season: {
                id: {
                  equals: season?.id,
                },
              },
            },
          })
          .pipe(
            map((response) =>
              DiscountsActions.loadDiscountsSuccess({
                discounts: response.data.discounts as Discount[],
                rejectionReasons: response.data
                  .discountRejectionReasons as DiscountRejectionReason[],
              })
            ),
            catchError((error) =>
              of(DiscountsActions.loadDiscountsFailure({ error }))
            )
          )
      )
    )
  );

  loadDiscountsAfterSeasonChanged$ = createEffect(() =>
    this.actions$.pipe(
      ofType(
        SeasonsActions.nextSeason,
        SeasonsActions.previousSeason,
        SeasonsActions.selectSeason
      ),
      map(() => DiscountsActions.loadDiscounts())
    )
  );

  approveDiscount$ = createEffect(() =>
    this.actions$.pipe(
      ofType(DiscountsActions.approveDiscount),
      concatMap((action) =>
        this.approveDiscountMutation
          .mutate({
            discountId: action.discountId,
          })
          .pipe(
            map((response) => {
              if (
                response.data?.approveDiscount.__typename ===
                'ApproveDiscountSuccess'
              ) {
                return DiscountsActions.approveDiscountSuccess({
                  discount: response.data?.approveDiscount.discount as Discount,
                });
              }

              if (
                response.data?.approveDiscount.__typename ===
                'ApproveDiscountFailed'
              ) {
                if (
                  response.data.approveDiscount.reason ===
                  'CanOnlyApprovePendingOrOverdueDiscount'
                )
                  return DiscountsActions.approveDiscountCanOnlyApprovePendingOrOverdue(
                    {
                      discountId: action.discountId,
                    }
                  );
              }

              throw new Error('no result');
            }),
            catchError((error) =>
              of(DiscountsActions.approveDiscountFailure({ error }))
            )
          )
      )
    )
  );

  rejectDiscount$ = createEffect(() =>
    this.actions$.pipe(
      ofType(DiscountsActions.rejectDiscount),
      withLatestFrom(this.store.select(selectRejection)),
      concatMap(([action, rejection]) =>
        rejection.reasonId == undefined
          ? of(DiscountsActions.rejectDiscountFormRejectedSelectReason())
          : this.rejectDiscountMutation
              .mutate({
                discountId: rejection.discountId as string,
                reasonId: rejection.reasonId as string,
                note: rejection.note,
              })
              .pipe(
                map((response) => {
                  if (
                    response.data?.rejectDiscount.__typename ===
                    'RejectDiscountSuccess'
                  ) {
                    return DiscountsActions.rejectDiscountSuccess({
                      discount: response.data?.rejectDiscount
                        .discount as Discount,
                    });
                  }

                  if (
                    response.data?.rejectDiscount.__typename ===
                    'RejectDiscountFailed'
                  ) {
                    if (
                      response.data.rejectDiscount.reason ===
                      'CanOnlyRejectPendingOrOverdueDiscount'
                    )
                      return DiscountsActions.rejectDiscountCanOnlyRejectPendingOrOverdue(
                        {
                          discountId: rejection.discountId as string,
                        }
                      );
                  }

                  throw new Error('no result');
                }),
                catchError((error) =>
                  of(DiscountsActions.rejectDiscountFailure({ error }))
                )
              )
      )
    )
  );

  sendDiscountApplicationReminder$ = createEffect(() =>
    this.actions$.pipe(
      ofType(DiscountsActions.sendDiscountApplicationReminder),
      concatMap((action) =>
        this.sendDiscountApplicationReminderMutation
          .mutate({
            discountId: action.discountId,
          })
          .pipe(
            map((response) =>
              DiscountsActions.sendDiscountApplicationReminderSuccess({
                discount: response.data
                  ?.sendDiscountApplicationReminder as Discount,
              })
            ),
            catchError((error) =>
              of(
                DiscountsActions.sendDiscountApplicationReminderFailure({
                  error,
                })
              )
            )
          )
      )
    )
  );

  showToastAfterRejectDiscountSelectReason$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(DiscountsActions.rejectDiscountFormRejectedSelectReason),
        tap((action) =>
          this.toastService.add({
            summary: 'Bitte einen Ablehnungsgrund auswählen',
            severity: 'warning',
          })
        )
      );
    },
    { dispatch: false }
  );

  showToastAfterDeleteEventSuccess$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(DiscountsActions.rejectDiscountSuccess),
        tap((action) =>
          this.toastService.add({
            summary:
              'Ermäßigung abgelehnt. Es wurde eine Email an den Sorgeberechtigten geschickt.',
            severity: 'success',
          })
        )
      );
    },
    { dispatch: false }
  );

  showToastAfterSendDiscountApplicationReminderSuccess$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(DiscountsActions.sendDiscountApplicationReminderSuccess),
        tap((action) =>
          this.toastService.add({
            summary:
              'Erinnerung für Ermäßigungsnachweis wurde per E-Mail an den Sorgeberechtigten geschickt.',
            severity: 'success',
          })
        )
      );
    },
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private store: Store,
    private discountsQuery: DiscountsGQL,
    private approveDiscountMutation: ApproveDiscountGQL,
    private rejectDiscountMutation: RejectDiscountGQL,
    private sendDiscountApplicationReminderMutation: SendDiscountApplicationReminderGQL,
    private toastService: MessageService
  ) {}
}
