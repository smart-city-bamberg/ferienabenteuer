import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromDiscounts from './discounts.reducer';
import { Discount } from './discounts.types';
import { numberComparer, dateComparer, stringComparer } from '../sorting';

export const selectDiscountsState = createFeatureSelector<fromDiscounts.State>(
  fromDiscounts.discountsFeatureKey
);

export const selectSortBy = createSelector(
  selectDiscountsState,
  (state) => state.sort
);

export const selectSearchText = createSelector(
  selectDiscountsState,
  (state) => state.searchText
);

const sorters: {
  [index: string]: (a: Discount, b: Discount) => number;
} = {
  custodian: stringComparer(
    (e) => `${e.custodian.surname}, ${e.custodian.firstname}`
  ),
  child: stringComparer((e) => `${e.child.surname}, ${e.child.firstname}`),
  submissionDate: dateComparer((e) => new Date(e.submissionDate)),
  reminderDate: dateComparer((e) => new Date(e.reminderDate)),
  type: stringComparer((e) => e.type.name),
  rejectionReason: stringComparer((e) => e.rejectionReason?.reason ?? ''),
  vouchersCount: (a, b) => {
    return a.vouchersCount - b.vouchersCount;
  },
  redeemedVouchersCountParticipant: (a, b) => {
    return (
      a.redeemedVouchersCountParticipant - b.redeemedVouchersCountParticipant
    );
  },
  redeemedVouchersCountWaitinglist: (a, b) => {
    return (
      a.redeemedVouchersCountWaitinglist - b.redeemedVouchersCountWaitinglist
    );
  },
};

export const selectDiscounts = createSelector(selectDiscountsState, (state) => [
  ...state.discounts,
]);

export const selectDiscountsSorted = createSelector(
  selectDiscounts,
  selectSortBy,
  selectSearchText,
  (discounts, sortBy, searchText) => {
    const filteredDiscounts = discounts.filter((discount) =>
      JSON.stringify({
        custodian: discount.custodian,
        child: discount.child,
      })
        .toLowerCase()
        .includes(searchText.toLowerCase())
    );

    if (sortBy.by === 'status') {
      const sortByStatus = [
        'overdue',
        'pending',
        'approved',
        'rejected',
        'cancelled',
      ];

      const sortedDiscounts = filteredDiscounts.sort((a, b) => {
        const statusA = sortByStatus.indexOf(a.status);
        const statusB = sortByStatus.indexOf(b.status);

        if (statusA === statusB) {
          const dateA = new Date(b.submissionDate).getTime();
          const dateB = new Date(a.submissionDate).getTime();
          return dateA - dateB;
        }

        return statusA - statusB;
      });

      return sortBy.descending ? sortedDiscounts.reverse() : sortedDiscounts;
    }

    return sorters[sortBy.by]
      ? filteredDiscounts.sort(
          (a, b) => (sortBy.descending ? -1 : 1) * sorters[sortBy.by](a, b)
        )
      : filteredDiscounts;
  }
);

export const selectRejectionReasons = createSelector(
  selectDiscountsState,
  (state) => state.rejectionReasons
);

export const selectRejection = createSelector(
  selectDiscountsState,
  (state) => state.rejection
);

export const selectShowRejectionForm = createSelector(
  selectDiscountsState,
  (state) => state.showRejectionForm
);

export const selectRejectionDiscountId = createSelector(
  selectRejection,
  (rejection) => rejection?.discountId
);

export const selectRejectionDiscount = createSelector(
  selectRejectionDiscountId,
  selectDiscounts,
  (discountId, discounts) =>
    discounts.find((discount) => discount.id == discountId)
);

export const selectRejectionReason = createSelector(
  selectRejection,
  (rejection) => rejection?.reasonId
);

export const selectRejectionNote = createSelector(
  selectRejection,
  (rejection) => rejection?.note
);
