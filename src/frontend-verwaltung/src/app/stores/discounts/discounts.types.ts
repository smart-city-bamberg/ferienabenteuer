export interface Discount {
  id: string;
  status: string;
  child: { id: string; firstname: string; surname: string };
  submissionDate: string;
  kind: string;
  validUntil: string;
  reminderDate: string;
  redeemLimit: number;
  custodian: {
    id: string;
    firstname: string;
    surname: string;
    user: { email: string };
  };
  type: { id: string; name: string };
  vouchersCount: number;
  redeemedVouchersCountWaitinglist: number;
  redeemedVouchersCountParticipant: number;
  rejectionReason: { reason: string };
  rejectionNote: string;
}

export interface DiscountRejectionReason {
  id: string;
  reason: string;
  description: string;
}

export interface Season {
  id: string;
  year: number;
}

export interface DiscountType {
  id: string;
  name: string;
  discountPercent: number;
  description: string;
  amountEvents: number;
  amountChildren: number;
  amountEventsDescription: string;
  amountChildrenDescription: string;
}
