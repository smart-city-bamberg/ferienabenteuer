import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromAuth from './auth.reducer';
import * as routerStore from '@ngrx/router-store';

const allowedRoles = ['administrator', 'organizer'];

export const { selectUrl, selectCurrentRoute } =
  routerStore.getRouterSelectors();

export const selectAuthState = createFeatureSelector<fromAuth.State>(
  fromAuth.authFeatureKey
);

export const selectIsActive = createSelector(
  selectAuthState,
  (state) => state.isActive
);
export const selectInvalidCredentials = createSelector(
  selectAuthState,
  (state) => state.invalidCredentials
);

export const selectRoleAllowed = createSelector(selectAuthState, (state) =>
  allowedRoles.includes(state.user?.role ?? '')
);

export const selectHasError = createSelector(
  selectAuthState,
  (state) => state.hasError
);

export const selectUser = createSelector(
  selectAuthState,
  (state) => state.user
);

export const selectUserRole = createSelector(selectUser, (user) => user?.role);

export const selectUserOrganizer = createSelector(
  selectUser,
  (user) => user?.organizer
);
