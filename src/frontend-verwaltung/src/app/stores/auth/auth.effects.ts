import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  catchError,
  concatMap,
  delay,
  filter,
  map,
  tap,
  withLatestFrom,
} from 'rxjs/operators';
import { of } from 'rxjs';
import { AuthActions } from './auth.actions';
import {
  AuthenticateUserWithPasswordGQL,
  AuthenticatedUserGQL,
  LogoutGQL,
} from 'src/app/graphql/generated';
import { Router } from '@angular/router';
import { Store, select } from '@ngrx/store';
import * as authSelectors from './auth.selectors';
import { User } from './auth.types';

@Injectable()
export class AuthEffects {
  login$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.login),
      concatMap((action) =>
        this.authenticateMutation
          .mutate({ email: action.user, password: action.password })
          .pipe(
            map((response) =>
              response.data?.authenticateUserWithPassword &&
              'message' in response.data?.authenticateUserWithPassword
                ? AuthActions.loginInvalid({
                    message:
                      response.data?.authenticateUserWithPassword.message,
                  })
                : AuthActions.loginSuccess()
            ),
            catchError((error) => of(AuthActions.loginFailure({ error })))
          )
      )
    )
  );

  loadAuthenticatedUser$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.loadUser),
      concatMap(() =>
        this.authenticatedUserQuery.fetch().pipe(
          map((response) =>
            response.data?.authenticatedItem
              ? AuthActions.loadUserSuccess({
                  user: response.data.authenticatedItem as User,
                })
              : AuthActions.loadUserNotAuthenticated()
          ),
          catchError((error) => of(AuthActions.loadUserFailure({ error })))
        )
      )
    )
  );

  loadAuthenticatedUserWhenLoginSuccess$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.loginSuccess),
      map(() => AuthActions.loadUser())
    )
  );

  redirectToHomeWhenLoadAuthenticatedUserSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(AuthActions.loadUserSuccess),
        withLatestFrom(
          this.store.select(authSelectors.selectUrl),
          this.store.select(authSelectors.selectRoleAllowed)
        ),
        filter(
          ([action, route, roleAllowd]) => roleAllowd && route === '/login'
        ),
        tap(() => this.router.navigate(['/']))
      ),
    {
      dispatch: false,
    }
  );

  logout$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.logout),
      concatMap((action) =>
        this.logoutMutation.mutate().pipe(
          map((response) => AuthActions.logoutSuccess()),
          catchError((error) => of(AuthActions.logoutFailure({ error })))
        )
      )
    )
  );

  redirectToLoginWhenLogout$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(AuthActions.logoutSuccess),
        tap(() => this.router.navigate(['/login']))
      ),
    {
      dispatch: false,
    }
  );

  redirectToLoginWhenUserNotAuthenticated$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(AuthActions.loadUserNotAuthenticated),
        tap(() => this.router.navigate(['/login']))
      ),
    {
      dispatch: false,
    }
  );

  constructor(
    private actions$: Actions,
    private authenticateMutation: AuthenticateUserWithPasswordGQL,
    private authenticatedUserQuery: AuthenticatedUserGQL,
    private logoutMutation: LogoutGQL,
    private router: Router,
    private store: Store
  ) {}
}
