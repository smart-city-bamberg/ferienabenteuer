import { createFeature, createReducer, on } from '@ngrx/store';
import { AuthActions } from './auth.actions';
import { User } from './auth.types';

export const authFeatureKey = 'auth';

export interface State {
  isActive: boolean;
  invalidCredentials: boolean;
  hasError: boolean;
  isAuthenticated: boolean;
  user: User | undefined;
}

export const initialState: State = {
  isActive: false,
  invalidCredentials: false,
  hasError: false,
  isAuthenticated: false,
  user: undefined,
};

export const reducer = createReducer(
  initialState,
  on(AuthActions.login, (state) => ({
    ...initialState,
    isActive: true,
  })),
  on(AuthActions.loginFailure, (state, action) => ({
    ...initialState,
    hasError: true,
  })),
  on(AuthActions.loginInvalid, (state, action) => ({
    ...initialState,
    invalidCredentials: true,
  })),
  on(AuthActions.loginSuccess, (state, action) => ({
    ...initialState,
    isAuthenticated: true,
  })),
  on(AuthActions.loadUserSuccess, (state, action) => ({
    ...state,
    user: {
      ...action.user,
    },
  }))
);

export const authFeature = createFeature({
  name: authFeatureKey,
  reducer,
});
