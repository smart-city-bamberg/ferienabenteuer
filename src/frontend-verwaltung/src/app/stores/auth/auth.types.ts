export interface User {
  id: string;
  email: string;
  name: string;
  role: string;
  organizer:
    | {
        id: string;
        name: string;
      }
    | undefined;
}
