import { ExportSeasonQuery } from './../../graphql/generated';
import {
  EarlyAccessCode,
  EarlyAccessCodeVoucher,
  Event,
  Holiday,
  Organizer,
  Partner,
  Period,
  Ticket,
} from './seasons.types';
import FileSaver from 'file-saver';
import * as ExcelJS from 'exceljs';

const excelMimeType =
  'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';

const eventColumns = [
  { name: 'tatsächlich gezahlter Zuschuss', width: 15 },
  { name: 'Zuschuss', width: 15 },
  { name: 'Ermäßigung', width: 15 },
  { name: 'Verfügbare Plätze', width: 15 },
  { name: 'TN', width: 15 },
  { name: 'WL', width: 15 },
  { name: 'Altersgruppe', width: 15 },
  { name: 'Kosten', width: 15 },
  { name: 'Quote Auslastung Plätze', width: 15 },
  { name: 'w', width: 6 },
  { name: 'm', width: 6 },
  { name: '6-9 J.', width: 10 },
  { name: 'ab 10 J.', width: 10 },
  { name: 'k. A.', width: 10 },
  { name: 'Stadt', width: 10 },
  { name: 'LK', width: 10 },
  { name: 'extern', width: 10 },
  { name: 'k. A.', width: 10 },
  { name: "Anzahl U'Kinder", width: 15 },
  { name: "Quote U'-Kinder/ Plätze", width: 15 },
  { name: "Quote U'-Kinder/ TN", width: 15 },
];

const romanNumbers = ['I', 'II', 'III', 'IV', 'V', 'VI'];
const holidayColors = ['b7dee8', 'e6b8b7', 'c4d79b', 'fabf8f', 'b1a0c7'];

const cityZipCodes = ['96047', '96049', '96050', '96052'];
const countyZipCodes = [
  '96185',
  '96103',
  '96149',
  '96169',
  '96191',
  '96117',
  '96154',
  '96173',
  '96196',
  '96129',
  '96158',
  '96179',
  '96138',
  '96135',
  '91332',
  '96170',
  '96120',
  '96155',
  '96175',
  '96132',
  '96164',
  '96163',
  '96148',
  '96167',
  '96114',
  '96194',
  '96123',
  '96157',
  '96178',
  '96199',
  '96161',
  '96182',
  '96146',
  '96187',
  '96110',
];

const calculateAge = (dateOfBirth: string, now: Date): number => {
  const birthDate = new Date(dateOfBirth);

  let age = now.getFullYear() - birthDate.getFullYear();
  const monthDiff = now.getMonth() - birthDate.getMonth();

  if (
    monthDiff < 0 ||
    (monthDiff === 0 && now.getDate() < birthDate.getDate())
  ) {
    age--;
  }

  return age;
};

export async function saveExcelSheet(workbook: ExcelJS.Workbook, year: number) {
  const buffer = await workbook.xlsx.writeBuffer();

  var blob = new Blob([buffer], {
    type: excelMimeType,
  });

  FileSaver.saveAs(blob, `Saison ${year}.xlsx`);
}

export function buildExcelSheet(result: ExportSeasonQuery, year: number) {
  const workbook = new ExcelJS.Workbook();
  const statisticsWorksheet = createWorksheet(workbook, year);

  const sortedHolidays = buildSortedHolidays(result);
  const sortedEvents = buildSortedEvents(result);
  const sortedPartners = buildSortedPartners(result);
  const tickets = result.tickets as Ticket[];
  const earlyAccessCodes = result.earlyAccessCodes as EarlyAccessCode[];
  const earlyAccessCodeVouchers =
    result.earlyAccessCodeVouchers as EarlyAccessCodeVoucher[];

  const now = new Date();
  const headlineCell = statisticsWorksheet.getCell(1, 2);

  headlineCell.value = `Stand ${now.toLocaleDateString()}`;
  headlineCell.fill = {
    type: 'pattern',
    pattern: 'solid',
    fgColor: {
      argb: 'eb3fc6',
    },
  };
  headlineCell.alignment = {
    horizontal: 'center',
    vertical: 'middle',
  };

  headlineCell.font = {
    size: 15,
    bold: true,
  };

  addEventColumnHeaders(statisticsWorksheet, sortedPartners as Partner[]);

  let rowIndex = 0;
  const startRow = 2;

  statisticsWorksheet.getColumn(1).width = 10;
  statisticsWorksheet.getColumn(2).width = 30;
  statisticsWorksheet.getColumn(3).width = 40;

  const sumRowIndexes = [];

  for (const [holidayIndex, holiday] of sortedHolidays.entries()) {
    if (!holiday.periods) continue;

    const holidayEvents = buildHolidayEvents(sortedEvents, holiday);
    const holidayTickets = buildHolidayTickets(tickets, holiday);

    const holidayOrganizers = holidayEvents
      .map((event) => event.organizer as Organizer)
      .reduce(
        (sum, next) =>
          sum.some((x) => x.id === next.id) ? sum : [...sum, next],
        new Array<{ id: string; name: string }>()
      );

    const holidayStartIndex = startRow + holidayIndex + rowIndex;

    for (const [periodIndex, period] of holiday.periods.entries()) {
      const localStartRow = startRow + holidayIndex + rowIndex;
      const weekNameCell = statisticsWorksheet.getCell(localStartRow, 2);
      weekNameCell.value =
        holiday.periods.length > 1
          ? `${holiday.name} ${romanNumbers[periodIndex]}`
          : holiday.name;

      weekNameCell.alignment = {
        horizontal: 'center',
      };

      const weekDateCell = statisticsWorksheet.getCell(localStartRow + 1, 2);
      const startDate = new Date(period.startDate);
      const endDate = new Date(period.endDate);
      weekDateCell.value = `${startDate
        .getDate()
        .toString()
        .padStart(2, '0')}.${(startDate.getMonth() + 1)
        .toString()
        .padStart(2, '0')}. - ${endDate
        .getDate()
        .toString()
        .padStart(2, '0')}.${(endDate.getMonth() + 1)
        .toString()
        .padStart(2, '0')}.`;
      weekDateCell.alignment = {
        horizontal: 'center',
      };

      const periodEvents = buildPeriodEvents(holidayEvents, period as Period);
      const periodTickets = buildPeriodTickets(
        holidayTickets,
        period as Period
      );

      const periodOrganizers = periodEvents
        .map((event) => event.organizer as Organizer)
        .reduce(
          (sum, next) =>
            sum.some((x) => x.id === next.id) ? sum : [...sum, next],
          new Array<{ id: string; name: string }>()
        );

      for (const [organizerIndex, organizer] of periodOrganizers.entries()) {
        processEventRow(
          statisticsWorksheet,
          rowIndex,
          organizer,
          periodEvents,
          periodTickets,
          earlyAccessCodes,
          earlyAccessCodeVouchers,
          sortedPartners,
          localStartRow,
          organizerIndex
        );
      }

      rowIndex += Math.max(2, periodOrganizers.length);
    }

    const localStartRow = startRow + holidayIndex + rowIndex;
    sumRowIndexes.push(localStartRow);

    processSumRow(
      statisticsWorksheet,
      holiday,
      holidayIndex,
      sortedPartners,
      localStartRow,
      holidayStartIndex
    );
  }

  processSuperSumRow(
    statisticsWorksheet,
    sortedEvents,
    sortedPartners,
    sumRowIndexes
  );

  return workbook;
}

function buildHolidayTickets(tickets: Ticket[], holiday: Holiday) {
  return tickets.filter(
    (ticket) =>
      ticket.eventTicket?.event?.carePeriodCommitted?.period?.holiday?.id ===
      holiday.id
  );
}

function buildHolidayEvents(sortedEvents: Event[], holiday: Holiday) {
  return sortedEvents.filter(
    (event) => event.carePeriodCommitted.period.holiday.id === holiday.id
  );
}

function fillColumnSumCell(
  worksheet: ExcelJS.Worksheet,
  row: number,
  column: number,
  topRow: number,
  bottomRow: number
): ExcelJS.Cell {
  const targetCell = worksheet.getCell(row, column);

  const topCell = worksheet.getCell(topRow, column);

  const bottomCell = worksheet.getCell(bottomRow, column);

  targetCell.value = {
    formula: `SUM(${topCell.address}:${bottomCell.address})`,
    date1904: false,
  };

  targetCell.alignment = {
    horizontal: 'center',
  };

  return targetCell;
}

function fillColumnIndexSumCell(
  worksheet: ExcelJS.Worksheet,
  row: number,
  column: number,
  rowIndexes: number[]
): ExcelJS.Cell {
  const targetCell = worksheet.getCell(row, column);

  const rows = [];

  for (const rowIndex of rowIndexes) {
    const sumCell = worksheet.getCell(rowIndex, column);
    rows.push(sumCell);
  }

  targetCell.value = {
    formula: `SUM(${rows.map((row) => row.address).join('+')})`,
    date1904: false,
  };

  targetCell.alignment = {
    horizontal: 'center',
  };

  targetCell.font = {
    bold: true,
  };

  return targetCell;
}

function processSuperSumRow(
  statisticsWorksheet: ExcelJS.Worksheet,
  events: Event[],
  periodPartners: Partner[],
  sumRowIndexes: number[]
) {
  const localStartRow = sumRowIndexes[sumRowIndexes.length - 1] + 2;
  const startColumn = 3;

  const eventCountCell = statisticsWorksheet.getCell(
    localStartRow - 1,
    startColumn
  );
  eventCountCell.value = `${events.length} Abenteuer`;

  const summaryLabelCell = statisticsWorksheet.getCell(
    localStartRow,
    startColumn
  );
  summaryLabelCell.value = 'Gesamt';
  summaryLabelCell.font = {
    bold: true,
  };

  const actuallyPayedGrantCell = fillColumnIndexSumCell(
    statisticsWorksheet,
    localStartRow,
    startColumn + 1,
    sumRowIndexes
  );

  actuallyPayedGrantCell.alignment = {
    horizontal: 'right',
  };

  actuallyPayedGrantCell.numFmt = '0.00 €';

  const payedGrantCell = fillColumnIndexSumCell(
    statisticsWorksheet,
    localStartRow,
    startColumn + 2,
    sumRowIndexes
  );

  payedGrantCell.alignment = {
    horizontal: 'right',
  };

  payedGrantCell.numFmt = '0.00 €';

  const discountGrantCell = fillColumnIndexSumCell(
    statisticsWorksheet,
    localStartRow,
    startColumn + 3,
    sumRowIndexes
  );

  discountGrantCell.alignment = {
    horizontal: 'right',
  };

  discountGrantCell.numFmt = '0.00 €';

  const availableSeatsCell = fillColumnIndexSumCell(
    statisticsWorksheet,
    localStartRow,
    startColumn + 4,
    sumRowIndexes
  );

  const bookedSeatsCell = fillColumnIndexSumCell(
    statisticsWorksheet,
    localStartRow,
    startColumn + 5,
    sumRowIndexes
  );

  const reservedSeatsCell = fillColumnIndexSumCell(
    statisticsWorksheet,
    localStartRow,
    startColumn + 6,
    sumRowIndexes
  );

  const usedSeatsPercentCell = statisticsWorksheet.getCell(
    localStartRow,
    startColumn + 9
  );
  usedSeatsPercentCell.value = {
    formula: `${bookedSeatsCell.address}/${availableSeatsCell.address}`,
    date1904: false,
  };
  usedSeatsPercentCell.numFmt = '0.00%';
  usedSeatsPercentCell.alignment = {
    horizontal: 'center',
  };
  usedSeatsPercentCell.font = {
    bold: true,
  };

  const femaleCountCell = fillColumnIndexSumCell(
    statisticsWorksheet,
    localStartRow,
    startColumn + 10,
    sumRowIndexes
  );

  const maleCountCell = fillColumnIndexSumCell(
    statisticsWorksheet,
    localStartRow,
    startColumn + 11,
    sumRowIndexes
  );

  const ageGroupSixToNineCell = fillColumnIndexSumCell(
    statisticsWorksheet,
    localStartRow,
    startColumn + 12,
    sumRowIndexes
  );

  const ageGroupTenAndAboveCell = fillColumnIndexSumCell(
    statisticsWorksheet,
    localStartRow,
    startColumn + 13,
    sumRowIndexes
  );

  const ageGroupUnknown = fillColumnIndexSumCell(
    statisticsWorksheet,
    localStartRow,
    startColumn + 14,
    sumRowIndexes
  );

  const zipCodeCountCity = fillColumnIndexSumCell(
    statisticsWorksheet,
    localStartRow,
    startColumn + 15,
    sumRowIndexes
  );

  const zipCodeCountCounty = fillColumnIndexSumCell(
    statisticsWorksheet,
    localStartRow,
    startColumn + 16,
    sumRowIndexes
  );

  const zipCodeCountExternal = fillColumnIndexSumCell(
    statisticsWorksheet,
    localStartRow,
    startColumn + 17,
    sumRowIndexes
  );

  const zipCodeCountUnknown = fillColumnIndexSumCell(
    statisticsWorksheet,
    localStartRow,
    startColumn + 18,
    sumRowIndexes
  );

  const partnerChildrenCount = fillColumnIndexSumCell(
    statisticsWorksheet,
    localStartRow,
    startColumn + 19,
    sumRowIndexes
  );

  const partnerChildrenCountPerSeatCell = statisticsWorksheet.getCell(
    localStartRow,
    startColumn + 20
  );
  partnerChildrenCountPerSeatCell.value = {
    formula: `${partnerChildrenCount.address}/${availableSeatsCell.address}`,
    date1904: false,
  };
  partnerChildrenCountPerSeatCell.numFmt = '0%';
  partnerChildrenCountPerSeatCell.alignment = {
    horizontal: 'center',
  };

  const partnerChildrenCountPerBookingCell = statisticsWorksheet.getCell(
    localStartRow,
    startColumn + 21
  );
  partnerChildrenCountPerBookingCell.value = {
    formula: `${partnerChildrenCount.address}/${bookedSeatsCell.address}`,
    date1904: false,
  };
  partnerChildrenCountPerBookingCell.numFmt = '0%';
  partnerChildrenCountPerBookingCell.alignment = {
    horizontal: 'center',
  };

  for (const [partnerIndex, partner] of periodPartners.entries()) {
    const partnerCountCell = fillColumnIndexSumCell(
      statisticsWorksheet,
      localStartRow,
      startColumn + 22 + partnerIndex,
      sumRowIndexes
    );
  }
}

function processSumRow(
  statisticsWorksheet: ExcelJS.Worksheet,
  holiday: Holiday,
  holidayIndex: number,
  periodPartners: Partner[],
  localStartRow: number,
  holidayStartIndex: number
) {
  const startColumn = 3;

  const grantActuallyPayedCell = fillColumnSumCell(
    statisticsWorksheet,
    localStartRow,
    startColumn + 1,
    holidayStartIndex,
    localStartRow - 1
  );

  grantActuallyPayedCell.alignment = {
    horizontal: 'right',
  };

  grantActuallyPayedCell.numFmt = '0.00 €';

  const grantPayedCell = fillColumnSumCell(
    statisticsWorksheet,
    localStartRow,
    startColumn + 2,
    holidayStartIndex,
    localStartRow - 1
  );

  grantPayedCell.alignment = {
    horizontal: 'right',
  };

  grantPayedCell.numFmt = '0.00 €';

  const grantDiscountCell = fillColumnSumCell(
    statisticsWorksheet,
    localStartRow,
    startColumn + 3,
    holidayStartIndex,
    localStartRow - 1
  );

  grantDiscountCell.alignment = {
    horizontal: 'right',
  };

  grantDiscountCell.numFmt = '0.00 €';

  const availableSeatsCell = fillColumnSumCell(
    statisticsWorksheet,
    localStartRow,
    startColumn + 4,
    holidayStartIndex,
    localStartRow - 1
  );

  const bookedSeatsCell = fillColumnSumCell(
    statisticsWorksheet,
    localStartRow,
    startColumn + 5,
    holidayStartIndex,
    localStartRow - 1
  );

  const reservedSeatsCell = fillColumnSumCell(
    statisticsWorksheet,
    localStartRow,
    startColumn + 6,
    holidayStartIndex,
    localStartRow - 1
  );

  const usedSeatsPercentCell = statisticsWorksheet.getCell(
    localStartRow,
    startColumn + 9
  );
  usedSeatsPercentCell.value = {
    formula: `${bookedSeatsCell.address}/${availableSeatsCell.address}`,
    date1904: false,
  };
  usedSeatsPercentCell.numFmt = '0.00%';
  usedSeatsPercentCell.alignment = {
    horizontal: 'center',
  };

  const femaleCountCell = fillColumnSumCell(
    statisticsWorksheet,
    localStartRow,
    startColumn + 10,
    holidayStartIndex,
    localStartRow - 1
  );

  const maleCountCell = fillColumnSumCell(
    statisticsWorksheet,
    localStartRow,
    startColumn + 11,
    holidayStartIndex,
    localStartRow - 1
  );

  const ageGroupSixToNineCell = fillColumnSumCell(
    statisticsWorksheet,
    localStartRow,
    startColumn + 12,
    holidayStartIndex,
    localStartRow - 1
  );

  const ageGroupTenAndAboveCell = fillColumnSumCell(
    statisticsWorksheet,
    localStartRow,
    startColumn + 13,
    holidayStartIndex,
    localStartRow - 1
  );

  const ageGroupUnknown = fillColumnSumCell(
    statisticsWorksheet,
    localStartRow,
    startColumn + 14,
    holidayStartIndex,
    localStartRow - 1
  );

  const zipCodeCountCity = fillColumnSumCell(
    statisticsWorksheet,
    localStartRow,
    startColumn + 15,
    holidayStartIndex,
    localStartRow - 1
  );

  const zipCodeCountCounty = fillColumnSumCell(
    statisticsWorksheet,
    localStartRow,
    startColumn + 16,
    holidayStartIndex,
    localStartRow - 1
  );

  const zipCodeCountExternal = fillColumnSumCell(
    statisticsWorksheet,
    localStartRow,
    startColumn + 17,
    holidayStartIndex,
    localStartRow - 1
  );

  const zipCodeCountUnknown = fillColumnSumCell(
    statisticsWorksheet,
    localStartRow,
    startColumn + 18,
    holidayStartIndex,
    localStartRow - 1
  );

  const partnerChildrenCount = fillColumnSumCell(
    statisticsWorksheet,
    localStartRow,
    startColumn + 19,
    holidayStartIndex,
    localStartRow - 1
  );

  const partnerChildrenCountPerSeatCell = statisticsWorksheet.getCell(
    localStartRow,
    startColumn + 20
  );
  partnerChildrenCountPerSeatCell.value = {
    formula: `${partnerChildrenCount.address}/${availableSeatsCell.address}`,
    date1904: false,
  };
  partnerChildrenCountPerSeatCell.numFmt = '0%';
  partnerChildrenCountPerSeatCell.alignment = {
    horizontal: 'center',
  };

  const partnerChildrenCountPerBookingCell = statisticsWorksheet.getCell(
    localStartRow,
    startColumn + 21
  );
  partnerChildrenCountPerBookingCell.value = {
    formula: `${partnerChildrenCount.address}/${bookedSeatsCell.address}`,
    date1904: false,
  };
  partnerChildrenCountPerBookingCell.numFmt = '0%';
  partnerChildrenCountPerBookingCell.alignment = {
    horizontal: 'center',
  };

  for (const [partnerIndex, partner] of periodPartners.entries()) {
    const partnerCountCell = fillColumnSumCell(
      statisticsWorksheet,
      localStartRow,
      startColumn + 22 + partnerIndex,
      holidayStartIndex,
      localStartRow - 1
    );
  }

  const holidayTotalCell = statisticsWorksheet.getCell(localStartRow, 2);
  holidayTotalCell.value = `${holiday.name} gesamt`;
  holidayTotalCell.alignment = {
    horizontal: 'center',
  };

  for (let index = 0; index < 23 + periodPartners.length; index++) {
    const cell = statisticsWorksheet.getCell(localStartRow, 2 + index);
    cell.fill = {
      pattern: 'solid',
      type: 'pattern',
      fgColor: { argb: holidayColors[holidayIndex] },
    };
    cell.font = {
      bold: true,
    };

    cell.border = {
      top: {
        style: 'medium',
      },
      bottom: {
        style: 'medium',
      },
    };
  }
}

function processEventRow(
  statisticsWorksheet: ExcelJS.Worksheet,
  rowIndex: number,
  organizer: Organizer,
  periodEvents: Event[],
  periodTickets: Ticket[],
  earlyAccessCodes: EarlyAccessCode[],
  earlyAccessCodeVouchers: EarlyAccessCodeVoucher[],
  partners: Partner[],
  localStartRow: number,
  organizerIndex: number
) {
  const eventRowData = buildEventRowData(
    organizer,
    periodEvents,
    periodTickets,
    earlyAccessCodes,
    earlyAccessCodeVouchers,
    partners
  );

  const eventRowIndex = localStartRow + organizerIndex;

  const eventNumberCell = statisticsWorksheet.getCell(eventRowIndex, 1);

  eventNumberCell.value = `${rowIndex + organizerIndex + 1}`;
  eventNumberCell.alignment = {
    horizontal: 'right',
  };

  const eventNameCell = statisticsWorksheet.getCell(eventRowIndex, 3);

  eventNameCell.value = eventRowData.name;

  const startColumn = 3;

  const grantPayedCell = statisticsWorksheet.getCell(
    eventRowIndex,
    startColumn + 2
  );

  grantPayedCell.alignment = {
    horizontal: 'right',
  };

  grantPayedCell.value = {
    formula: eventRowData.grant.payed,
    date1904: false,
  };

  grantPayedCell.numFmt = '0.00 €';

  const grantDiscountCell = statisticsWorksheet.getCell(
    eventRowIndex,
    startColumn + 3
  );

  grantDiscountCell.alignment = {
    horizontal: 'right',
  };

  grantDiscountCell.value = {
    formula: eventRowData.grant.discount,
    date1904: false,
  };

  grantDiscountCell.numFmt = '0.00 €';

  const grandActuallyPayedCell = statisticsWorksheet.getCell(
    eventRowIndex,
    startColumn + 1
  );

  grandActuallyPayedCell.alignment = {
    horizontal: 'right',
  };

  grandActuallyPayedCell.numFmt = '0.00 €';
  grandActuallyPayedCell.value = {
    formula: `SUM(${grantPayedCell.address}+${grantDiscountCell.address})`,
    date1904: false,
  };

  const availableSeatsCell = statisticsWorksheet.getCell(
    eventRowIndex,
    startColumn + 4
  );

  availableSeatsCell.alignment = {
    horizontal: 'center',
  };

  availableSeatsCell.value = eventRowData.availableSeats;

  const bookedSeatsCell = statisticsWorksheet.getCell(
    eventRowIndex,
    startColumn + 5
  );

  bookedSeatsCell.alignment = {
    horizontal: 'center',
  };

  bookedSeatsCell.value = eventRowData.bookedSeats;

  const reservedSeatsCell = statisticsWorksheet.getCell(
    eventRowIndex,
    startColumn + 6
  );

  reservedSeatsCell.alignment = {
    horizontal: 'center',
  };

  reservedSeatsCell.value = eventRowData.reservedSeats;

  const usedSeatsPercentCell = statisticsWorksheet.getCell(
    eventRowIndex,
    startColumn + 9
  );

  usedSeatsPercentCell.value = {
    formula: `${bookedSeatsCell.address}/${availableSeatsCell.address}`,
    date1904: false,
  };

  usedSeatsPercentCell.numFmt = '0%';
  usedSeatsPercentCell.alignment = {
    horizontal: 'center',
  };

  const ageGroupCell = statisticsWorksheet.getCell(
    eventRowIndex,
    startColumn + 7
  );

  ageGroupCell.alignment = {
    horizontal: 'center',
  };

  ageGroupCell.value = eventRowData.ageGroup;

  const costsCell = statisticsWorksheet.getCell(eventRowIndex, startColumn + 8);

  costsCell.value = eventRowData.costs;
  costsCell.numFmt = '0.00 €';

  const femaleCountCell = statisticsWorksheet.getCell(
    eventRowIndex,
    startColumn + 10
  );
  femaleCountCell.value = eventRowData.ticketCounts.female;
  femaleCountCell.alignment = {
    horizontal: 'center',
  };

  const maleCountCell = statisticsWorksheet.getCell(
    eventRowIndex,
    startColumn + 11
  );
  maleCountCell.value = eventRowData.ticketCounts.male;
  maleCountCell.alignment = {
    horizontal: 'center',
  };

  const ageGroupSixToNineCell = statisticsWorksheet.getCell(
    eventRowIndex,
    startColumn + 12
  );

  ageGroupSixToNineCell.alignment = {
    horizontal: 'center',
  };

  ageGroupSixToNineCell.value = eventRowData.ageGroupCounts.sixToNine;

  const ageGroupTenAndAboveCell = statisticsWorksheet.getCell(
    eventRowIndex,
    startColumn + 13
  );

  ageGroupTenAndAboveCell.alignment = {
    horizontal: 'center',
  };

  ageGroupTenAndAboveCell.value = eventRowData.ageGroupCounts.tenAndAbove;

  const zipCodeCountsCityCell = statisticsWorksheet.getCell(
    eventRowIndex,
    startColumn + 15
  );

  zipCodeCountsCityCell.alignment = {
    horizontal: 'center',
  };

  zipCodeCountsCityCell.value = eventRowData.zipCodeCounts.city;

  const zipCodeCountsCountyCell = statisticsWorksheet.getCell(
    eventRowIndex,
    startColumn + 16
  );

  zipCodeCountsCountyCell.alignment = {
    horizontal: 'center',
  };

  zipCodeCountsCountyCell.value = eventRowData.zipCodeCounts.county;

  const zipCodeCountsExternalCell = statisticsWorksheet.getCell(
    eventRowIndex,
    startColumn + 17
  );

  zipCodeCountsExternalCell.alignment = {
    horizontal: 'center',
  };

  zipCodeCountsExternalCell.value = eventRowData.zipCodeCounts.external;

  const partnerChildrenCountCell = statisticsWorksheet.getCell(
    eventRowIndex,
    startColumn + 19
  );

  const partnerChildrenCountCellLeft = statisticsWorksheet.getCell(
    eventRowIndex,
    startColumn + 22
  );

  const partnerChildrenCountCellRight = statisticsWorksheet.getCell(
    eventRowIndex,
    startColumn + 22 + partners.length - 1
  );

  partnerChildrenCountCell.value = {
    formula: `SUM(${partnerChildrenCountCellLeft.address}:${partnerChildrenCountCellRight.address})`,
    date1904: false,
  };

  partnerChildrenCountCell.alignment = {
    horizontal: 'center',
  };

  const partnerChildrenPerSeatsCell = statisticsWorksheet.getCell(
    eventRowIndex,
    startColumn + 20
  );

  partnerChildrenPerSeatsCell.value = {
    formula: `${partnerChildrenCountCell.address}/${availableSeatsCell.address}`,
    date1904: false,
  };

  partnerChildrenPerSeatsCell.numFmt = '0%';
  partnerChildrenPerSeatsCell.alignment = {
    horizontal: 'center',
  };

  const partnerChildrenPerBookingCell = statisticsWorksheet.getCell(
    eventRowIndex,
    startColumn + 21
  );

  partnerChildrenPerBookingCell.value = {
    formula: `${partnerChildrenCountCell.address}/${bookedSeatsCell.address}`,
    date1904: false,
  };

  partnerChildrenPerBookingCell.numFmt = '0%';
  partnerChildrenPerBookingCell.alignment = {
    horizontal: 'center',
  };

  const columns = partners.map((partner) => partner.id);

  for (const [columnIndex, partnerId] of columns.entries()) {
    const cell = statisticsWorksheet.getCell(
      eventRowIndex,
      22 + startColumn + columnIndex
    );

    cell.value =
      eventRowData.partnersCount.find((pair) => pair.partner === partnerId)
        ?.count ?? 0;
    cell.alignment = {
      horizontal: 'center',
    };
  }
}

function buildEventRowData(
  organizer: Organizer,
  periodEvents: Event[],
  periodTickets: Ticket[],
  earlyAccessCodes: EarlyAccessCode[],
  earlyAccessCodeVouchers: EarlyAccessCodeVoucher[],
  partners: Partner[]
) {
  const organizerEvent = periodEvents.find(
    (event) => event.organizer?.id === organizer.id
  ) as Event;

  const organizerTickets = periodTickets.filter(
    (ticket) => ticket.eventTicket?.event?.organizer?.id === organizer.id
  );

  const discountedTickets = organizerTickets.filter(
    (ticket) => ticket.discountVoucher
  );

  const bookedSeats = organizerEvent.bookedTicketCountParticipant;
  const reservedSeats = organizerEvent.bookedTicketCountWaitinglist;
  const availableSeats = organizerEvent.participantLimit;

  const custodianEarlyAccessCodes = earlyAccessCodeVouchers.map((voucher) => {
    const partnerCode = earlyAccessCodes.find(
      (code) => code.code.localeCompare(voucher.code) === 0
    );

    return {
      custodianId: voucher.custodian.id,
      partner: partnerCode?.partner.id as string,
    };
  });

  const eventDate = new Date(
    organizerEvent.carePeriodCommitted.careDays[0].day
  );
  return {
    name: organizer.name,
    grant: {
      payed: `${bookedSeats} * ${organizerEvent.carePeriodCommitted.careDays.length} * ${organizerEvent.season.grantAmount}`,
      discount: `${discountedTickets.length} * (${organizerEvent.costs} / 2)`,
    },
    availableSeats,
    bookedSeats,
    reservedSeats,
    ageGroup: `${organizerEvent.ageMinimum}-${organizerEvent.ageMaximum}`,
    costs: organizerEvent.costs,
    ticketCounts: {
      female: organizerTickets.filter(
        (ticket) => ticket.child.gender === 'female'
      ).length,
      male: organizerTickets.filter((ticket) => ticket.child.gender === 'male')
        .length,
    },
    partnersCount: partners.map((partner) => ({
      partner: partner.id,
      count: organizerTickets.filter((ticket) =>
        custodianEarlyAccessCodes.some(
          (pair) =>
            pair.custodianId === ticket.child.custodian.id &&
            pair.partner === partner.id
        )
      ).length,
    })),
    ageGroupCounts: {
      sixToNine: organizerTickets.filter(
        (ticket) => calculateAge(ticket.child.dateOfBirth, eventDate) <= 9
      ).length,
      tenAndAbove: organizerTickets.filter(
        (ticket) => calculateAge(ticket.child.dateOfBirth, eventDate) >= 10
      ).length,
    },
    zipCodeCounts: {
      city: organizerTickets.filter((ticket) =>
        cityZipCodes.includes(ticket.child.custodian.addressZip)
      ).length,
      county: organizerTickets.filter((ticket) =>
        countyZipCodes.includes(ticket.child.custodian.addressZip)
      ).length,
      external: organizerTickets.filter(
        (ticket) =>
          !countyZipCodes.includes(ticket.child.custodian.addressZip) &&
          !cityZipCodes.includes(ticket.child.custodian.addressZip)
      ).length,
    },
  };
}

function createWorksheet(workbook: ExcelJS.Workbook, year: number) {
  return workbook.addWorksheet(`Statistik ${year}`, {
    properties: {
      defaultColWidth: 20,
    },
  });
}

function buildPeriodEvents(sortedEvents: Event[], period: Period): Event[] {
  return sortedEvents.filter(
    (event) => event.carePeriodCommitted?.period?.id === period.id
  );
}

function buildPeriodTickets(tickets: Ticket[], period: Period): Ticket[] {
  return (tickets?.filter(
    (ticket) =>
      ticket.eventTicket?.event?.carePeriodCommitted?.period?.id === period.id
  ) ?? []) as Ticket[];
}

function buildSortedPartners(result: ExportSeasonQuery): Partner[] {
  return ([...(result.partners ?? [])].sort((a, b) => {
    const organizerComparsion = (a.name ?? '')?.localeCompare(b.name ?? '');

    if (organizerComparsion !== 0) {
      return organizerComparsion;
    }

    return (a.name ?? '')?.localeCompare(b.name ?? '');
  }) ?? []) as Partner[];
}

function buildSortedEvents(result: ExportSeasonQuery): Event[] {
  return ([...(result.events ?? [])].sort((a, b) => {
    const organizerComparsion = (a.organizer?.name ?? '')?.localeCompare(
      b.organizer?.name ?? ''
    );

    if (organizerComparsion !== 0) {
      return organizerComparsion;
    }

    return (a.name ?? '')?.localeCompare(b.name ?? '');
  }) ?? []) as Event[];
}

function buildSortedHolidays(result: ExportSeasonQuery) {
  return (result.holidays?.sort((a, b) => {
    const aPeriods = [...(a.periods ?? [])].sort((x, y) =>
      (x.startDate as string).localeCompare(y.startDate as string)
    );

    const bPeriods = [...(b.periods ?? [])].sort((x, y) =>
      (x.startDate as string).localeCompare(y.startDate as string)
    );

    return (aPeriods[0].startDate as string).localeCompare(
      bPeriods[0].startDate as string
    );
  }) ?? []) as Holiday[];
}

function addEventColumnHeaders(
  eventsWorksheet: ExcelJS.Worksheet,
  partners: Partner[]
) {
  const startColumn = 4;
  const startRow = 1;

  eventsWorksheet.getRow(startRow).height = 50;

  const boldColumns = [
    'Quote Auslastung Plätze',
    "Anzahl U'Kinder",
    "Quote U'-Kinder/ Plätze",
    "Quote U'-Kinder/ TN",
  ];

  const columns: { name: string; width: number; fontSize?: number }[] = [
    ...eventColumns,
    ...partners.map((partner) => ({
      name: partner.name,
      width: 10,
      fontSize: 8,
    })),
  ];

  for (const [columnIndex, column] of columns.entries()) {
    const cell = eventsWorksheet.getCell(startRow, startColumn + columnIndex);
    eventsWorksheet.getColumn(startColumn + columnIndex).width = column.width;

    cell.value = column.name;

    cell.font = {
      bold: boldColumns.includes(column.name),
      size: column.fontSize,
    };
    cell.alignment = {
      horizontal: 'center',
      wrapText: true,
    };
  }
}
