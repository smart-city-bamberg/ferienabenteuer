import { createFeature, createReducer, on } from '@ngrx/store';
import { SeasonsActions } from './seasons.actions';
import { Holiday, Period, Season } from './seasons.types';
import {
  FormGroupState,
  createFormGroupState,
  onNgrxForms,
  setValue,
} from 'ngrx-forms';

export const seasonsFeatureKey = 'seasons';

interface PeriodForm {
  holidayId: string;
  startDate: string;
  endDate: string;
}

interface SeasonForm {
  bookingStart: string;
  bookingStartPartners: string;
  releaseDate: string;
  grantAmount: number;
}

export interface State {
  seasons: Season[];
  seasonIndex: number;
  season: Season | undefined;
  periods: Period[];
  holidays: Holiday[];
  showDeleteDialog: boolean;

  periodForm: FormGroupState<PeriodForm>;
  seasonForm: FormGroupState<SeasonForm>;
}

export const initialState: State = {
  seasons: [],
  seasonIndex: 0,
  season: undefined,
  periods: [],
  holidays: [],
  showDeleteDialog: false,

  periodForm: createFormGroupState<PeriodForm>('period_form', {
    holidayId: '123456',
    startDate: '',
    endDate: '',
  }),
  seasonForm: createFormGroupState<SeasonForm>('season_form', {
    bookingStart: '',
    bookingStartPartners: '',
    releaseDate: '',
    grantAmount: 0,
  }),
};

function setSeasonFormValue(season: SeasonForm) {
  return setValue({
    bookingStart: utcDateToLocal(season.bookingStart),
    bookingStartPartners: utcDateToLocal(season.bookingStartPartners),
    releaseDate: utcDateToLocal(season.releaseDate),
    grantAmount: season.grantAmount ?? 0,
  });
}

export const reducer = createReducer(
  initialState,
  on(SeasonsActions.loadSeasonsSuccess, (state, action) => ({
    ...state,
    seasons: [...action.seasons],
  })),
  on(SeasonsActions.previousSeason, (state, action) => {
    const index = Math.max(0, state.seasonIndex - 1);
    const season = state.seasons[index];

    return {
      ...state,
      seasonIndex: index,
      seasonForm: setSeasonFormValue(season)(state.seasonForm),
    };
  }),
  on(SeasonsActions.nextSeason, (state, action) => {
    const index = Math.min(state.seasons.length - 1, state.seasonIndex + 1);
    const season = state.seasons[index];

    return {
      ...state,
      seasonIndex: index,
      seasonForm: setSeasonFormValue(season)(state.seasonForm),
    };
  }),
  on(SeasonsActions.selectSeason, (state, action) => {
    const index = state.seasons.findIndex(
      (season) => season.id === action.seasonId
    );

    const season = state.seasons[index];

    return {
      ...state,
      seasonIndex: index,
      seasonForm: setSeasonFormValue(season)(state.seasonForm),
    };
  }),
  on(SeasonsActions.loadSeasonSuccess, (state, action) => ({
    ...state,
    season: { ...action.season },
    periods: [...action.periods],
    holidays: [...action.holidays],
    seasonForm: setSeasonFormValue(action.season)(state.seasonForm),
  })),
  on(SeasonsActions.createPeriodSuccess, (state, action) => ({
    ...state,
    periods: [...state.periods, action.period],
  })),
  on(SeasonsActions.deletePeriodSuccess, (state, action) => ({
    ...state,
    periods: state.periods.filter((period) => period.id !== action.periodId),
  })),
  on(SeasonsActions.updateSeasonSuccess, (state, action) => ({
    ...state,
    seasons: state.seasons.map((season) =>
      season.id !== action.season.id ? season : action.season
    ),
    season: { ...action.season },
    seasonForm: setSeasonFormValue(action.season)(state.seasonForm),
  })),
  on(SeasonsActions.showDeleteSeasonDialog, (state, action) => ({
    ...state,
    showDeleteDialog: true,
  })),
  on(
    SeasonsActions.deleteSeason,
    SeasonsActions.cancelDeleteSeason,
    (state, action) => ({
      ...state,
      showDeleteDialog: false,
    })
  ),
  onNgrxForms()
);

export const seasonsFeature = createFeature({
  name: seasonsFeatureKey,
  reducer,
});

function utcDateToLocal(utcDate: string): string {
  const date = new Date(utcDate);
  // Umwandlung in lokale Zeit
  const localDate = new Date(date.getTime() - date.getTimezoneOffset() * 60000);

  const isoString = localDate.toISOString();
  return isoString.substring(0, isoString.length - 1);
}
