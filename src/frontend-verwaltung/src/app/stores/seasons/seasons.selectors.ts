import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromSeasons from './seasons.reducer';

export const selectSeasonsState = createFeatureSelector<fromSeasons.State>(
  fromSeasons.seasonsFeatureKey
);

export const selectSeasons = createSelector(
  selectSeasonsState,
  (state) => state.seasons
);
export const selectSeasonIndex = createSelector(
  selectSeasonsState,
  (state) => state.seasonIndex
);

export const selectCurrentSeason = createSelector(
  selectSeasons,
  selectSeasonIndex,
  (seasons, index) => seasons[index]
);

export const selectHasPreviousSeason = createSelector(
  selectSeasons,
  selectSeasonIndex,
  (seasons, index) => index > 0
);

export const selectHasNextSeason = createSelector(
  selectSeasons,
  selectSeasonIndex,
  (seasons, index) => index < seasons.length - 1
);

export const selectHolidays = createSelector(
  selectSeasonsState,
  (state) => state.holidays
  // .sort((a, b) => a.name.localeCompare(b.name))
);

export const selectPeriods = createSelector(selectSeasonsState, (state) =>
  [...state.periods].sort(
    (a, b) => new Date(a.startDate).getTime() - new Date(b.startDate).getTime()
  )
);

export const selectPeriodForm = createSelector(
  selectSeasonsState,
  (state) => state.periodForm
);

export const selectSeasonForm = createSelector(
  selectSeasonsState,
  (state) => state.seasonForm
);

export const selectshowDeleteDialog = createSelector(
  selectSeasonsState,
  (state) => state.showDeleteDialog
);
