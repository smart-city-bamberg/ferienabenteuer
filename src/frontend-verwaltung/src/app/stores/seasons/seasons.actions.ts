import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { Holiday, Period, Season } from './seasons.types';

export const SeasonsActions = createActionGroup({
  source: 'Seasons',
  events: {
    'Load Seasons': emptyProps(),
    'Load Seasons Success': props<{ seasons: Season[] }>(),
    'Load Seasons Failure': props<{ error: unknown }>(),

    'Next Season': emptyProps(),
    'Previous Season': emptyProps(),
    'Select Season': props<{ seasonId: string }>(),

    'Export Season Statistics': emptyProps(),
    'Export Season Statistics Success': emptyProps(),
    'Export Season Statistics Failure': props<{ error: unknown }>(),

    'Load Season': emptyProps(),
    'Load Season Success': props<{
      season: Season;
      periods: Period[];
      holidays: Holiday[];
    }>(),

    'Load Season Failure': props<{ error: unknown }>(),

    'Create Period': emptyProps(),
    'Create Period Success': props<{ period: Period }>(),
    'Create Period Failure': props<{ error: unknown }>(),

    'Delete Period': props<{ periodId: string }>(),
    'Delete Period Success': props<{ periodId: string }>(),
    'Delete Period Failure': props<{ error: unknown }>(),

    'Update Season': emptyProps(),
    'Update Season Success': props<{ season: Season }>(),
    'Update Season Failure': props<{ error: unknown }>(),

    'Show Delete Season Dialog': emptyProps(),
    'Delete Season': emptyProps(),
    'Delete Season Success': emptyProps(),
    'Delete Season Failure': props<{ error: unknown }>(),
    'Cancel Delete Season': emptyProps(),
  },
});
