export interface Season {
  id: string;
  year: number;
  bookingStart: string;
  bookingStartPartners: string;
  releaseDate: string;
  grantAmount: number;
}

export interface CarePeriod {
  id: string;
  period: {
    id: string;
    holiday: { id: string };
  };
  careDays: {
    id: string;
    day: string;
  }[];
}

export interface Holiday {
  id: string;
  name: string;
  periods: Period[];
}

export interface Period {
  id: string;
  startDate: string;
  endDate: string;
  holiday: Holiday;
}

export interface Organizer {
  id: string;
  name: string;
}

export interface Partner {
  id: string;
  name: string;
}

export interface Event {
  id: string;
  name: string;
  status: string;
  ageMinimum: number;
  ageMaximum: number;
  carePeriodCommitted: CarePeriod;
  costs: number;
  participantLimit: number | null;
  bookedTicketCountParticipant: number;
  bookedTicketCountWaitinglist: number;
  organizer: Organizer;
  season: Season;
}

export interface Ticket {
  id: string;
  discountVoucher: {
    id: string;
  } | null;
  price: number;
  child: {
    gender: string;
    dateOfBirth: string;
    custodian: {
      id: string;
      addressZip: string;
    };
  };
  eventTicket: {
    event: {
      id: string;
      organizer: {
        id: string;
      };
      carePeriodCommitted: CarePeriod;
    };
  };
}

export interface EarlyAccessCode {
  id: string;
  code: string;
  partner: {
    id: string;
  };
}

export interface EarlyAccessCodeVoucher {
  id: string;
  code: string;
  custodian: {
    id: string;
  };
}
