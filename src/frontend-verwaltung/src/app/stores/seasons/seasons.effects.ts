import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  catchError,
  map,
  concatMap,
  filter,
  withLatestFrom,
  switchMap,
} from 'rxjs/operators';
import { of } from 'rxjs';
import { SeasonsActions } from './seasons.actions';
import {
  CreatePeriodGQL,
  DeletePeriodGQL,
  DeleteSeasonDataGQL,
  ExportSeasonGQL,
  SeasonGQL,
  SeasonsGQL,
  UpdateSeasonGQL,
} from 'src/app/graphql/generated';
import { Holiday, Period, Season } from './seasons.types';
import { AuthActions } from '../auth/auth.actions';
import { routerNavigatedAction } from '@ngrx/router-store';
import { Store } from '@ngrx/store';
import {
  selectCurrentSeason,
  selectPeriodForm,
  selectSeasonForm,
} from './seasons.selectors';
import { buildExcelSheet, saveExcelSheet } from './sesons.pdf';

@Injectable()
export class SeasonsEffects {
  loadSeasons$ = createEffect(() =>
    this.actions$.pipe(
      ofType(SeasonsActions.loadSeasons),
      concatMap(() =>
        this.seasonsQuery.fetch().pipe(
          map((response) =>
            SeasonsActions.loadSeasonsSuccess({
              seasons: response.data.seasons as Season[],
            })
          ),
          catchError((error) =>
            of(SeasonsActions.loadSeasonsFailure({ error }))
          )
        )
      )
    )
  );

  loadSeason$ = createEffect(() =>
    this.actions$.pipe(
      ofType(SeasonsActions.loadSeason),
      withLatestFrom(this.store.select(selectCurrentSeason)),
      filter(([action, season]) => season !== undefined),
      concatMap(([action, season]) =>
        this.seasonQuery
          .fetch({
            seasonId: season?.id as string,
          })
          .pipe(
            map((response) =>
              SeasonsActions.loadSeasonSuccess({
                season: response.data.season as Season,
                periods: response.data.periods as Period[],
                holidays: response.data.holidays as Holiday[],
              })
            ),
            catchError((error) =>
              of(SeasonsActions.loadSeasonFailure({ error }))
            )
          )
      )
    )
  );

  createPeriod$ = createEffect(() =>
    this.actions$.pipe(
      ofType(SeasonsActions.createPeriod),
      withLatestFrom(
        this.store.select(selectCurrentSeason),
        this.store.select(selectPeriodForm)
      ),
      concatMap(([action, season, form]) =>
        this.createPeriodMutation
          .mutate({
            seasonId: season?.id as string,
            holidayId: form.value.holidayId as string,
            startDate: form.value.startDate,
            endDate: form.value.endDate,
          })
          .pipe(
            map((response) =>
              SeasonsActions.createPeriodSuccess({
                period: response.data?.createPeriod as Period,
              })
            ),
            catchError((error) =>
              of(SeasonsActions.createPeriodFailure({ error }))
            )
          )
      )
    )
  );

  deletePeriod$ = createEffect(() =>
    this.actions$.pipe(
      ofType(SeasonsActions.deletePeriod),
      concatMap((action) =>
        this.deletePeriodMutation
          .mutate({
            periodId: action.periodId,
          })
          .pipe(
            map((response) =>
              SeasonsActions.deletePeriodSuccess({
                periodId: response.data?.deletePeriod?.id as string,
              })
            ),
            catchError((error) =>
              of(SeasonsActions.deletePeriodFailure({ error }))
            )
          )
      )
    )
  );

  updateSeason$ = createEffect(() =>
    this.actions$.pipe(
      ofType(SeasonsActions.updateSeason),
      withLatestFrom(
        this.store.select(selectCurrentSeason),
        this.store.select(selectSeasonForm)
      ),
      concatMap(([action, season, form]) => {
        return this.updateSeasonMutation
          .mutate({
            seasonId: season?.id as string,
            bookingStart: this.localDateToUtcString(form.value.bookingStart),
            bookingStartPartners: this.localDateToUtcString(
              form.value.bookingStartPartners
            ),
            releaseDate: this.localDateToUtcString(form.value.releaseDate),
            grantAmount: form.value.grantAmount,
          })
          .pipe(
            map((response) =>
              SeasonsActions.updateSeasonSuccess({
                season: response.data?.updateSeason as Season,
              })
            ),
            catchError((error) =>
              of(SeasonsActions.updateSeasonFailure({ error }))
            )
          );
      })
    )
  );

  loadSeasonsAfterNavigation$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.loadUserSuccess),
      map((action) => SeasonsActions.loadSeasons())
    )
  );

  loadSeasonAfterNavigation$ = createEffect(() =>
    this.actions$.pipe(
      ofType(routerNavigatedAction),
      filter((action) => action.payload.routerState.url === '/saison'),
      map((action) => SeasonsActions.loadSeason())
    )
  );

  loadSeasonAfterSeasonChanged$ = createEffect(() =>
    this.actions$.pipe(
      ofType(
        SeasonsActions.previousSeason,
        SeasonsActions.nextSeason,
        SeasonsActions.selectSeason
      ),
      map((action) => SeasonsActions.loadSeason())
    )
  );

  selectSeasonsAfterLoadSeasons$ = createEffect(() =>
    this.actions$.pipe(
      ofType(SeasonsActions.loadSeasonsSuccess),
      map((action) =>
        SeasonsActions.selectSeason({
          seasonId: action.seasons.find(
            (season) => season.year === new Date().getFullYear()
          )?.id as string,
        })
      )
    )
  );

  exportEvents$ = createEffect(() =>
    this.actions$.pipe(
      ofType(SeasonsActions.exportSeasonStatistics),
      withLatestFrom(this.store.select(selectCurrentSeason)),
      concatMap(([action, season]) =>
        this.exportSeasonQuery
          .fetch({
            seasonId: season?.id as string,
          })
          .pipe(
            switchMap(async (response) => {
              const workbook = buildExcelSheet(
                response.data,
                season?.year as number
              );
              await saveExcelSheet(workbook, season?.year as number);
              return SeasonsActions.exportSeasonStatisticsSuccess();
            }),
            catchError((error) =>
              of(
                SeasonsActions.exportSeasonStatisticsFailure({
                  error,
                })
              )
            )
          )
      )
    )
  );

  deleteSeasonData$ = createEffect(() =>
    this.actions$.pipe(
      ofType(SeasonsActions.deleteSeason),
      withLatestFrom(this.store.select(selectCurrentSeason)),
      concatMap(([action, season]) =>
        this.deleteSeasonData
          .mutate({
            seasonId: season!.id,
          })
          .pipe(
            map((response) => SeasonsActions.deleteSeasonSuccess()),
            catchError((error) =>
              of(SeasonsActions.deletePeriodFailure({ error }))
            )
          )
      )
    )
  );

  constructor(
    private actions$: Actions,
    private store: Store,
    private exportSeasonQuery: ExportSeasonGQL,
    private seasonsQuery: SeasonsGQL,
    private seasonQuery: SeasonGQL,
    private createPeriodMutation: CreatePeriodGQL,
    private deletePeriodMutation: DeletePeriodGQL,
    private updateSeasonMutation: UpdateSeasonGQL,
    private deleteSeasonData: DeleteSeasonDataGQL
  ) {}

  private localDateToUtcString(date: string): any {
    const parsedDate = new Date(date);
    return parsedDate.toISOString();
  }
}
