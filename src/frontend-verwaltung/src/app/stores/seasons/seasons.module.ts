import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import * as fromSeasons from './seasons.reducer';
import { EffectsModule } from '@ngrx/effects';
import { SeasonsEffects } from './seasons.effects';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StoreModule.forFeature(fromSeasons.seasonsFeatureKey, fromSeasons.reducer),
    EffectsModule.forFeature([SeasonsEffects])
  ]
})
export class SeasonsModule { }
