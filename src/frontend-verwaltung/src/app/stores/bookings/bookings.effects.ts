import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  catchError,
  map,
  concatMap,
  withLatestFrom,
  filter,
  tap,
  switchMap,
} from 'rxjs/operators';
import { of } from 'rxjs';
import { BookingsActions } from './bookings.actions';
import {
  BookingsGQL,
  MarkBookingPaymentRemindedGQL,
  RegisterPaymentGQL,
  ResendBookingConfirmationGQL,
} from 'src/app/graphql/generated';
import * as routerStore from '@ngrx/router-store';
import { Store } from '@ngrx/store';
import * as authSelectors from '../auth/auth.selectors';
import { Booking, CareDay, EmailTemplate, Event } from './bookings.types';
import { MessageService } from 'primeng/api';
import {
  selectBooking,
  selectPaymentOverdueEmailTemplate,
} from './bookings.selectors';
import mustache from 'mustache';
import { SeasonsActions } from '../seasons/seasons.actions';
import { selectCurrentSeason } from '../seasons/seasons.selectors';

mustache.escape = (input: string) => input;

export const { selectUrl, selectCurrentRoute } =
  routerStore.getRouterSelectors();

const buildCareDaysString = (careDays: CareDay[]) => {
  const firstDay = new Date(careDays[0].day);

  const firstDayString = `${firstDay.getDate().toString().padStart(2, '0')}.${(
    firstDay.getMonth() + 1
  )
    .toString()
    .padStart(2, '0')}.${firstDay.getFullYear()}`;

  if (careDays.length === 1) {
    return firstDayString;
  }

  const lastDay = new Date(careDays[careDays.length - 1].day);

  const lastDayString = `${lastDay.getDate().toString().padStart(2, '0')}.${(
    lastDay.getMonth() + 1
  )
    .toString()
    .padStart(2, '0')}.${lastDay.getFullYear()}`;

  return `${firstDayString} bis ${lastDayString}`;
};

const buildContact = (event: Event) =>
  `${
    event.contactPersonSalutation === 'mr'
      ? 'Herr'
      : event.contactPersonSalutation === 'ms'
      ? 'Frau'
      : ''
  } ${event.contactPersonFirstname.trim()} ${event.contactPersonSurname.trim()}
E-Mail: ${event.organizer.email}
Telefon: ${event.emergencyPhone}
`;

@Injectable()
export class BookingsEffects {
  loadBookings$ = createEffect(() =>
    this.actions$.pipe(
      ofType(BookingsActions.loadBookings),
      withLatestFrom(
        this.store.select(authSelectors.selectUserRole),
        this.store.select(authSelectors.selectUserOrganizer),
        this.store.select(selectCurrentSeason)
      ),
      filter(([action, role, organizer, season]) => season !== undefined),
      concatMap(([action, role, organizer, season]) =>
        this.bookingsQuery
          .fetch({
            where:
              role == 'organizer'
                ? {
                    event: {
                      organizer: { id: { equals: organizer?.id } },
                      season: {
                        id: {
                          equals: season?.id,
                        },
                      },
                    },
                    isDeleted: { equals: false },
                  }
                : {
                    event: {
                      season: {
                        id: {
                          equals: season?.id,
                        },
                      },
                    },
                    isDeleted: { equals: false },
                  },
          })
          .pipe(
            map((response) =>
              BookingsActions.loadBookingsSuccess({
                bookings: response.data.bookings as Booking[],
                paymentOverdueEmailTemplate: response.data
                  .paymentOverdueEmailTemplate as EmailTemplate,
              })
            ),
            catchError((error) =>
              of(BookingsActions.loadBookingsFailure({ error }))
            )
          )
      )
    )
  );

  loadBookingsAfterSeasonChanged$ = createEffect(() =>
    this.actions$.pipe(
      ofType(
        SeasonsActions.nextSeason,
        SeasonsActions.previousSeason,
        SeasonsActions.selectSeason
      ),
      map(() => BookingsActions.loadBookings())
    )
  );

  registerPayment$ = createEffect(() =>
    this.actions$.pipe(
      ofType(BookingsActions.registerPayment),
      concatMap((action) =>
        this.registerPaymentMutation
          .mutate({
            bookingId: action.bookingId,
          })
          .pipe(
            map((response) => {
              if (
                response.data?.registerPayment.__typename ===
                'RegisterPaymentSuccess'
              ) {
                return BookingsActions.registerPaymentSuccess({
                  booking: response.data.registerPayment.booking as Booking,
                });
              }

              if (
                response.data?.registerPayment.__typename ===
                  'RegisterPaymentFailed' &&
                response.data.registerPayment.reason ===
                  'CanOnlyRegisterPaymentForPendingOrOverdueBooking'
              ) {
                return BookingsActions.registerPaymentFailedOnlyPendingAndOverdueCanRegister();
              }

              throw Error('no result');
            }),
            catchError((error) =>
              of(BookingsActions.registerPaymentFailure({ error }))
            )
          )
      )
    )
  );

  resendBookingConfirmation$ = createEffect(() =>
    this.actions$.pipe(
      ofType(BookingsActions.resendBookingConfirmation),
      concatMap((action) =>
        this.resendBookingConfirmationMutation
          .mutate({
            bookingId: action.bookingId,
          })
          .pipe(
            map((response) =>
              BookingsActions.resendBookingConfirmationSuccess({
                booking: response.data?.resendBookingConfirmation as Booking,
              })
            ),
            catchError((error) =>
              of(BookingsActions.resendBookingConfirmationFailure({ error }))
            )
          )
      )
    )
  );

  openPaymentOverdueEmail$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(BookingsActions.openBookingPaymentOverdue),
        withLatestFrom(this.store.select(selectPaymentOverdueEmailTemplate)),
        switchMap(([action, template]) =>
          of({ action, template }).pipe(
            withLatestFrom(this.store.select(selectBooking(action.bookingId))),
            tap(([{ action, template }, booking]) => {
              const now = new Date();

              const subject = mustache.render(template.subject, {
                Buchungsnummer: booking.code,
              });

              const millisecondsPerDay = 24 * 60 * 60 * 1000;
              const custodian = booking.custodian;
              const children = booking.tickets.map((ticket) => ticket.child);
              const event = booking.event;
              const organizer = event.organizer;
              const dateIn5Days = new Date(
                now.getTime() + 5 * millisecondsPerDay
              );
              const careDays = booking.event.carePeriodCommitted.careDays;
              const vonBis = buildCareDaysString(careDays);

              const content = mustache.render(template.content, {
                Anrede:
                  custodian.salutation === 'mr'
                    ? 'Sehr geehrter Herr'
                    : custodian.salutation == 'ms'
                    ? 'Sehr geehrte Frau'
                    : 'Sehr geehrter Herr/Sehr geehrte Frau',
                Vorname: custodian.firstname.trim(),
                Nachname: custodian.surname.trim(),
                'Name Kind': children
                  .map(
                    (child) =>
                      `${child.firstname.trim()} ${child.surname.trim()}`
                  )
                  .join(', '),
                'Nachname Kind': custodian.surname.trim(),
                'Name Veranstaltung': event.name,
                'Datum Veranstaltung von bis': vonBis,
                Buchungsnummer: booking.code,
                'Name Veranstalter': organizer.name,
                'IBAN Veranstalter': organizer.bankIban,
                Betrag: booking.price.toLocaleString(undefined, {
                  minimumFractionDigits: 2,
                  maximumFractionDigits: 2,
                }),
                'Datum in 5 Tagen': dateIn5Days.toLocaleDateString(),
                'Kontaktdaten Veranstalter': buildContact(event),
              });

              const mailtoLink = `mailto:${encodeURIComponent(
                booking.custodian.user.email
              )}?subject=${encodeURIComponent(
                subject
              )}&body=${encodeURIComponent(content)}`;

              window.location.href = mailtoLink;
            })
          )
        )
      ),
    {
      dispatch: false,
    }
  );

  markBookingPaymentOverdueReminded$ = createEffect(() =>
    this.actions$.pipe(
      ofType(BookingsActions.openBookingPaymentOverdue),
      concatMap((action) =>
        !window.confirm(
          'Soll die Buchung markiert werden, dass eine Zahlungserinnerung verschickt wurde?'
        )
          ? of(BookingsActions.markBookingPaymentOverdueRejected())
          : this.markingBookingOverdueReminded
              .mutate({
                bookingId: action.bookingId,
              })
              .pipe(
                map((response) =>
                  BookingsActions.markBookingPaymentOverdueSuccess({
                    booking: response.data
                      ?.markBookingPaymentReminded as Booking,
                  })
                ),
                catchError((error) =>
                  of(
                    BookingsActions.markBookingPaymentOverdueFailure({ error })
                  )
                )
              )
      )
    )
  );

  loadEventsAfterNavigate$ = createEffect(() =>
    this.actions$.pipe(
      ofType(routerStore.routerNavigatedAction),
      withLatestFrom(this.store.select(selectUrl)),
      filter(([action, url]) => url == '/buchungen'),
      map(() => BookingsActions.loadBookings())
    )
  );

  showToastAfterRegisterPaymentSuccess$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(BookingsActions.registerPaymentSuccess),
        tap((action) =>
          this.toastService.add({
            summary:
              'Zahlung wurde erfasst. Es wurde eine E-Mail an den Sorgeberechtigten geschickt.',
            severity: 'success',
          })
        )
      );
    },
    { dispatch: false }
  );

  showToastAfterResendBookingConfirmationSuccess$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(BookingsActions.resendBookingConfirmationSuccess),
        tap((action) =>
          this.toastService.add({
            summary:
              'Es wurde eine E-Mail mit der Buchungsbestätigung an den Sorgeberechtigten geschickt.',
            severity: 'success',
          })
        )
      );
    },
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private store: Store,
    private toastService: MessageService,
    private bookingsQuery: BookingsGQL,
    private registerPaymentMutation: RegisterPaymentGQL,
    private resendBookingConfirmationMutation: ResendBookingConfirmationGQL,
    private markingBookingOverdueReminded: MarkBookingPaymentRemindedGQL
  ) {}
}
