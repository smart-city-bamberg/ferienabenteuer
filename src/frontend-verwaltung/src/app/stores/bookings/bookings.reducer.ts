import { createFeature, createReducer, on } from '@ngrx/store';
import { BookingsActions } from './bookings.actions';
import { Booking, EmailTemplate } from './bookings.types';

export const bookingsFeatureKey = 'bookings';

export interface State {
  bookings: Booking[];
  sort: {
    by: string;
    descending: boolean;
  };
  paymentOverdueEmailTemplate: EmailTemplate | undefined;
  searchText: string;
}

export const initialState: State = {
  bookings: [],
  sort: {
    by: 'status',
    descending: false,
  },
  searchText: '',
  paymentOverdueEmailTemplate: undefined,
};

export const reducer = createReducer(
  initialState,
  on(BookingsActions.loadBookings, (state) => state),
  on(BookingsActions.loadBookingsSuccess, (state, action) => ({
    ...state,
    bookings: [...action.bookings],
    paymentOverdueEmailTemplate: { ...action.paymentOverdueEmailTemplate },
  })),
  on(BookingsActions.loadBookingsFailure, (state, action) => state),
  on(BookingsActions.registerPaymentSuccess, (state, action) => ({
    ...state,
    bookings: state.bookings.map((booking) =>
      booking.id === action.booking.id ? action.booking : booking
    ),
  })),
  on(BookingsActions.markBookingPaymentOverdueSuccess, (state, action) => ({
    ...state,
    bookings: state.bookings.map((booking) =>
      booking.id === action.booking.id ? action.booking : booking
    ),
  })),
  on(BookingsActions.sortBy, (state, action) => ({
    ...state,
    sort:
      state.sort.by === action.column
        ? { by: action.column, descending: !state.sort.descending }
        : {
            by: action.column,
            descending: false,
          },
  })),
  on(BookingsActions.changeSearchText, (state, action) => ({
    ...state,
    searchText: action.searchText,
  }))
);

export const bookingsFeature = createFeature({
  name: bookingsFeatureKey,
  reducer,
});
