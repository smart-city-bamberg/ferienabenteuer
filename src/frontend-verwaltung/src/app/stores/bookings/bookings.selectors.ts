import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromBookings from './bookings.reducer';
import { Booking, EmailTemplate } from './bookings.types';
import { stringComparer, dateComparer, numberComparer } from '../sorting';

export const selectBookingsState = createFeatureSelector<fromBookings.State>(
  fromBookings.bookingsFeatureKey
);

export const selectSortBy = createSelector(
  selectBookingsState,
  (state) => state.sort
);

export const selectBookings = createSelector(selectBookingsState, (state) => [
  ...state.bookings,
]);

export const selectSearchText = createSelector(
  selectBookingsState,
  (state) => state.searchText
);

const sortByStatus = ['overdue', 'pending', 'payed'];

const sorters: {
  [index: string]: (a: Booking, b: Booking) => number;
} = {
  code: stringComparer((e) => e.code),
  ticketCount: numberComparer((e) => e.tickets.length),
  price: numberComparer((e) => e.price),
  carePeriodCommitted: dateComparer(
    (e) => new Date(e.event.carePeriodCommitted.careDays[0].day)
  ),
  custodian: stringComparer(
    (e) => `${e.custodian.surname}, ${e.custodian.firstname}`
  ),
  event: stringComparer((e) => e.event.name),
  organizer: stringComparer((e) => e.event.organizer.name),
  date: dateComparer((e) => new Date(e.date)),
  status: (a, b) => {
    const statusA = sortByStatus.indexOf(a.status);
    const statusB = sortByStatus.indexOf(b.status);

    if (statusA !== statusB) {
      return statusA - statusB;
    } else {
      const dateA = new Date(a.date);
      const dateB = new Date(b.date);
      return dateB.getTime() - dateA.getTime();
    }
  },
};

export const selectBookingsSorted = createSelector(
  selectBookings,
  selectSortBy,
  selectSearchText,
  (bookings, sortBy, searchText) => {
    const filteredBookings = bookings.filter((booking) =>
      JSON.stringify({
        code: booking.code,
        price: booking.price,
        custodian: booking.custodian,
        event: booking.event.name,
        organizer: booking.event.organizer.name,
      })
        .toLowerCase()
        .includes(searchText.toLowerCase())
    );

    return sorters[sortBy.by]
      ? filteredBookings.sort(
          (a, b) => (sortBy.descending ? -1 : 1) * sorters[sortBy.by](a, b)
        )
      : filteredBookings;
  }
);

export const selectPaymentOverdueEmailTemplate = createSelector(
  selectBookingsState,
  (state) => state.paymentOverdueEmailTemplate as EmailTemplate
);

export const selectBooking = (bookingId: string) =>
  createSelector(
    selectBookings,
    (bookings) =>
      bookings.find((booking) => booking.id === bookingId) as Booking
  );
