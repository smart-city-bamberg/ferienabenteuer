import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { Booking, EmailTemplate } from './bookings.types';

export const BookingsActions = createActionGroup({
  source: 'Bookings',
  events: {
    'Load Bookings': emptyProps(),
    'Load Bookings Success': props<{
      bookings: Booking[];
      paymentOverdueEmailTemplate: EmailTemplate;
    }>(),
    'Load Bookings Failure': props<{ error: Error }>(),
    'Sort By': props<{ column: string }>(),

    'Register Payment': props<{ bookingId: string }>(),
    'Register Payment Success': props<{ booking: Booking }>(),
    'Register Payment Failed Only Pending And Overdue Can Register':
      emptyProps(),
    'Register Payment Failure': props<{ error: Error }>(),

    'Open Booking Payment Overdue': props<{ bookingId: string }>(),

    'Mark Booking Payment Overdue Rejected': emptyProps(),
    'Mark Booking Payment Overdue Success': props<{ booking: Booking }>(),
    'Mark Booking Payment Overdue Failure': props<{ error: unknown }>(),

    'Resend Booking Confirmation': props<{ bookingId: string }>(),
    'Resend Booking Confirmation Success': props<{ booking: Booking }>(),
    'Resend Booking Confirmation Failure': props<{ error: Error }>(),

    'Change Search Text': props<{ searchText: string }>(),
  },
});
