export interface Booking {
  id: string;
  code: string;
  price: number;
  status: string;
  custodian: {
    id: string;
    firstname: string;
    surname: string;
    salutation: string;
    user: { email: string };
  };
  date: string;
  reminderDate: string;
  event: Event;
  tickets: Ticket[];
}

export interface Event {
  id: string;
  name: string;
  carePeriodCommitted: CarePeriod;
  contactPersonFirstname: string;
  contactPersonSalutation: string;
  contactPersonSurname: string;
  emergencyPhone: string;
  organizer: {
    name: string;
    email: string;
    bankIban: string;
  };
}

export interface CarePeriod {
  id: string;
  period: Period;
  careDays: CareDay[];
}

export interface CareDay {
  day: string;
}

export interface Period {
  id: string;
  startDate: Date;
  endDate: Date;
  holiday: {
    id: string;
    name: string;
  };
}

export interface Ticket {
  id: string;
  child: {
    id: string;
    surname: string;
    firstname: string;
  };
}

export interface EmailTemplate {
  id: string;
  role: string;
  subject: string;
  content: string;
}
