import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import * as reservationsReducer from './reservations.reducer';
import { ReservationsEffects } from './reservations.effects';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StoreModule.forFeature(
      reservationsReducer.reservationsFeatureKey,
      reservationsReducer.reducer
    ),
    EffectsModule.forFeature([ReservationsEffects]),
  ],
})
export class ReservationsModule {}
