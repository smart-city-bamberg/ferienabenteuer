export interface Reservation {
  id: string;
  code: string;
  custodian: { id: string; firstname: string; surname: string };
  date: any;
  event: Event;
  ticketCountParticipant: number;
  ticketCountWaitinglist: number;
}

export interface Event {
  id: string;
  name: string;
  carePeriodDesired: CarePeriod;
  carePeriodAlternative: CarePeriod;
  carePeriodCommitted: CarePeriod;
  organizer: { name: string };
}

export interface CarePeriod {
  id: string;
  period: Period;
}

export interface Period {
  id: string;
  startDate: Date;
  endDate: Date;
  holiday: {
    id: string;
    name: string;
  };
}
