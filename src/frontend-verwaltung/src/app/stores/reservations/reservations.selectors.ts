import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromReservations from './reservations.reducer';
import { Reservation } from './reservations.types';
import { stringComparer, dateComparer, numberComparer } from '../sorting';

export const selectReservationsState =
  createFeatureSelector<fromReservations.State>(
    fromReservations.reservationsFeatureKey
  );

export const selectReservations = createSelector(
  selectReservationsState,
  (state) => [...state.reservations]
);

export const selectSortBy = createSelector(
  selectReservationsState,
  (state) => state.sort
);

export const selectSearchText = createSelector(
  selectReservationsState,
  (state) => state.searchText
);

const sorters: {
  [index: string]: (a: Reservation, b: Reservation) => number;
} = {
  code: stringComparer((e) => e.code),
  date: dateComparer((e) => new Date(e.date)),
  custodian: stringComparer(
    (e) => `${e.custodian.surname}, ${e.custodian.firstname}`
  ),
  event: stringComparer((e) => e.event.name),
  organizer: stringComparer((e) => e.event.organizer.name),
  carePeriodCommitted: dateComparer((e) => new Date(e.date)),
  participants: numberComparer((e) => e.ticketCountParticipant),
  waitinglist: numberComparer((e) => e.ticketCountWaitinglist),
};

export const selectReservationsSorted = createSelector(
  selectReservations,
  selectSortBy,
  selectSearchText,
  (reservations, sortBy, searchText) => {
    const filteredReservations = reservations.filter((reservation) =>
      JSON.stringify({
        code: reservation.code,
        custodian: reservation.custodian,
        event: reservation.event.name,
        organizer: reservation.event.organizer.name,
      })
        .toLowerCase()
        .includes(searchText.toLowerCase())
    );

    return sorters[sortBy.by]
      ? filteredReservations.sort(
          (a, b) => (sortBy.descending ? -1 : 1) * sorters[sortBy.by](a, b)
        )
      : filteredReservations;
  }
);
