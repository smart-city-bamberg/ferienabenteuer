import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { Reservation } from './reservations.types';

export const ReservationsActions = createActionGroup({
  source: 'Reservations',
  events: {
    'Load Reservations': emptyProps(),
    'Load Reservations Success': props<{ reservations: Reservation[] }>(),
    'Load Reservations Failure': props<{ error: Error }>(),
    'Sort By': props<{ column: string }>(),
    'Change Search Text': props<{ searchText: string }>(),
  },
});
