import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  catchError,
  map,
  concatMap,
  withLatestFrom,
  filter,
} from 'rxjs/operators';
import { of } from 'rxjs';
import { ReservationsActions } from './reservations.actions';
import * as routerStore from '@ngrx/router-store';
import { Store } from '@ngrx/store';
import * as authSelectors from '../auth/auth.selectors';
import { Reservation } from './reservations.types';
import { ReservationsGQL } from 'src/app/graphql/generated';
import { SeasonsActions } from '../seasons/seasons.actions';
import { selectCurrentSeason } from '../seasons/seasons.selectors';

export const { selectUrl, selectCurrentRoute } =
  routerStore.getRouterSelectors();

@Injectable()
export class ReservationsEffects {
  loadReservations$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ReservationsActions.loadReservations),
      withLatestFrom(
        this.store.select(authSelectors.selectUserRole),
        this.store.select(authSelectors.selectUserOrganizer),
        this.store.select(selectCurrentSeason)
      ),
      filter(([action, role, organizer, season]) => season !== undefined),
      concatMap(([action, role, organizer, season]) =>
        this.reservationsQuery
          .fetch({
            where:
              role == 'organizer'
                ? {
                    event: {
                      organizer: { id: { equals: organizer?.id } },
                      season: {
                        id: {
                          equals: season?.id,
                        },
                      },
                    },
                    isDeleted: { equals: false },
                  }
                : {
                    isDeleted: { equals: false },
                    event: {
                      season: {
                        id: {
                          equals: season?.id,
                        },
                      },
                    },
                  },
          })
          .pipe(
            map((response) =>
              ReservationsActions.loadReservationsSuccess({
                reservations: response.data.reservations as Reservation[],
              })
            ),
            catchError((error) =>
              of(ReservationsActions.loadReservationsFailure({ error }))
            )
          )
      )
    )
  );

  loadReservationsAfterSeasonChanged$ = createEffect(() =>
    this.actions$.pipe(
      ofType(
        SeasonsActions.nextSeason,
        SeasonsActions.previousSeason,
        SeasonsActions.selectSeason
      ),
      map(() => ReservationsActions.loadReservations())
    )
  );

  loadEventsAfterNavigate$ = createEffect(() =>
    this.actions$.pipe(
      ofType(routerStore.routerNavigatedAction),
      withLatestFrom(this.store.select(selectUrl)),
      filter(([action, url]) => url == '/reservierungen'),
      map(() => ReservationsActions.loadReservations())
    )
  );

  constructor(
    private actions$: Actions,
    private reservationsQuery: ReservationsGQL,
    private store: Store
  ) {}
}
