import { createFeature, createReducer, on } from '@ngrx/store';
import { ReservationsActions } from './reservations.actions';
import { Reservation } from './reservations.types';
import { SortByState, defaultSortByState, sortByReducer } from '../sorting';

export const reservationsFeatureKey = 'reservations';

export interface State {
  reservations: Reservation[];
  sort: SortByState;
  searchText: string;
}

export const initialState: State = {
  reservations: [],
  sort: defaultSortByState,
  searchText: '',
};

export const reducer = createReducer(
  initialState,
  on(ReservationsActions.loadReservations, (state) => state),
  on(ReservationsActions.loadReservationsSuccess, (state, action) => ({
    ...state,
    reservations: [...action.reservations],
  })),
  on(ReservationsActions.sortBy, (state, action) => ({
    ...state,
    sort: sortByReducer(state.sort, action.column),
  })),
  on(ReservationsActions.loadReservationsFailure, (state, action) => state),
  on(ReservationsActions.changeSearchText, (state, action) => ({
    ...state,
    searchText: action.searchText,
  }))
);

export const reservationsFeature = createFeature({
  name: reservationsFeatureKey,
  reducer,
});
