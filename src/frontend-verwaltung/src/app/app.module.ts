import { LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './sites/auth/login.component';
import { StoresModule } from './stores/stores.module';
import { SitesModule } from './sites/sites.module';
import { PrimeModule } from './prime/prime.module';

import { registerLocaleData } from '@angular/common';
import localeDe from '@angular/common/locales/de';
import { MessageService } from 'primeng/api';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

registerLocaleData(localeDe, 'de');

@NgModule({
  declarations: [AppComponent, LoginComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    PrimeModule,
    StoresModule,
    SitesModule,
  ],
  providers: [{ provide: LOCALE_ID, useValue: 'de' }, MessageService],
  bootstrap: [AppComponent],
})
export class AppModule {}
