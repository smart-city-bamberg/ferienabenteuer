export const environment = {
  features: {
    cancelTickets: true,
    advanceWaitinglistToParticipant: true,
  },
  apiUrl:
    window.location.protocol +
    '//' +
    window.location.hostname +
    ':3000/api/graphql',
  grafanaUrl: 'http://grafana',
};
