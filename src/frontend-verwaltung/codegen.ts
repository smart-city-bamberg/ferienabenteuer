import type { CodegenConfig } from '@graphql-codegen/cli';

const config: CodegenConfig = {
  schema: 'graphql.schema.json',
  documents: './src/app/graphql/+(queries|mutations)/**/*.graphql',
  overwrite: true,
  generates: {
    './src/app/graphql/generated.ts': {
      plugins: [
        'typescript',
        'typescript-operations',
        'typescript-apollo-angular',
      ],
      config: {
        addExplicitOverride: true,
        // skipTypename: true,
        // skipTypeNameForRoot: true,
        // maybeValue: 'T | null',
        scalars: {
          Upload: 'File',
        },
      },
    },
    './graphql.schema.json': {
      plugins: ['introspection'],
    },
  },
};
export default config;
