import { Config } from 'tailwindcss';

export default {
  content: ['./src/**/*.{html,ts}'],
  theme: {
    extend: {
      colors: {
        background: '#e9f2f9',
        primaryLight: '#003981',
        primaryDark: '#002657',
        primaryGreen: '#00a991',
        primaryOrange: '#ff9141',
        secondaryRed: '#E01E1F',
        secondaryYellow: '#ffbe43',
        secondaryBlue: '#3783ca',
        neutralGrayLight: '#f9f9f9',
        neutralGrayDark: '#505050',
        secondaryGray: '#707070',
        neutralWhite: '#ffffff',
        textGreenDark: '#002657',
      },
      fontFamily: {
        calibri: ['Calibri'],
      },
    },
  },
  plugins: [],
} satisfies Config;
