export type Session = {
  data: {
    id: string;
    name: string;
    role: string;
    isAdministrator: boolean;
    isTester: boolean;
    organizer: {
      id: string;
    };
    isEnabled: boolean;
  };
};
