-- CreateTable
CREATE TABLE "DiscountRejectionReason" (
    "id" TEXT NOT NULL,
    "reason" TEXT NOT NULL DEFAULT '',
    "description" TEXT NOT NULL DEFAULT '',

    CONSTRAINT "DiscountRejectionReason_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE INDEX "Discount_rejectionReason_idx" ON "Discount"("rejectionReason");

-- AddForeignKey
ALTER TABLE "Discount" ADD CONSTRAINT "Discount_rejectionReason_fkey" FOREIGN KEY ("rejectionReason") REFERENCES "DiscountRejectionReason"("id") ON DELETE SET NULL ON UPDATE CASCADE;
