/*
  Warnings:

  - You are about to drop the column `custodian` on the `Child` table. All the data in the column will be lost.

*/
-- DropForeignKey
ALTER TABLE "Child" DROP CONSTRAINT "Child_custodian_fkey";

-- DropIndex
DROP INDEX "Child_custodian_idx";

-- AlterTable
ALTER TABLE "Child" DROP COLUMN "custodian";
