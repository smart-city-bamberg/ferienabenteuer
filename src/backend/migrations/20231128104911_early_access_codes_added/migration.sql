-- AlterTable
ALTER TABLE "Partner" ADD COLUMN     "earlyAccessCode" TEXT;

-- CreateTable
CREATE TABLE "EarlyAccessCode" (
    "id" TEXT NOT NULL,
    "code" TEXT NOT NULL DEFAULT '',

    CONSTRAINT "EarlyAccessCode_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE INDEX "Partner_earlyAccessCode_idx" ON "Partner"("earlyAccessCode");

-- AddForeignKey
ALTER TABLE "Partner" ADD CONSTRAINT "Partner_earlyAccessCode_fkey" FOREIGN KEY ("earlyAccessCode") REFERENCES "EarlyAccessCode"("id") ON DELETE SET NULL ON UPDATE CASCADE;
