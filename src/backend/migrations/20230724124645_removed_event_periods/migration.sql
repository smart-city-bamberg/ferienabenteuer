/*
  Warnings:

  - You are about to drop the column `event` on the `CareDay` table. All the data in the column will be lost.
  - You are about to drop the column `periodCommitted` on the `Event` table. All the data in the column will be lost.
  - You are about to drop the column `periodPrimary` on the `Event` table. All the data in the column will be lost.
  - You are about to drop the column `periodSecondary` on the `Event` table. All the data in the column will be lost.

*/
-- DropForeignKey
ALTER TABLE "CareDay" DROP CONSTRAINT "CareDay_event_fkey";

-- DropForeignKey
ALTER TABLE "Event" DROP CONSTRAINT "Event_periodCommitted_fkey";

-- DropForeignKey
ALTER TABLE "Event" DROP CONSTRAINT "Event_periodPrimary_fkey";

-- DropForeignKey
ALTER TABLE "Event" DROP CONSTRAINT "Event_periodSecondary_fkey";

-- DropIndex
DROP INDEX "CareDay_event_idx";

-- DropIndex
DROP INDEX "Event_periodCommitted_idx";

-- DropIndex
DROP INDEX "Event_periodPrimary_idx";

-- DropIndex
DROP INDEX "Event_periodSecondary_idx";

-- AlterTable
ALTER TABLE "CareDay" DROP COLUMN "event",
ADD COLUMN     "period" TEXT;

-- AlterTable
ALTER TABLE "Event" DROP COLUMN "periodCommitted",
DROP COLUMN "periodPrimary",
DROP COLUMN "periodSecondary";

-- CreateTable
CREATE TABLE "CarePeriod" (
    "id" TEXT NOT NULL,
    "period" TEXT,

    CONSTRAINT "CarePeriod_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE INDEX "CarePeriod_period_idx" ON "CarePeriod"("period");

-- CreateIndex
CREATE INDEX "CareDay_period_idx" ON "CareDay"("period");

-- AddForeignKey
ALTER TABLE "CareDay" ADD CONSTRAINT "CareDay_period_fkey" FOREIGN KEY ("period") REFERENCES "CarePeriod"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "CarePeriod" ADD CONSTRAINT "CarePeriod_period_fkey" FOREIGN KEY ("period") REFERENCES "Period"("id") ON DELETE SET NULL ON UPDATE CASCADE;
