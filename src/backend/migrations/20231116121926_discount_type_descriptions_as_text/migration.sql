-- AlterTable
ALTER TABLE "DiscountType" ALTER COLUMN "description" SET DEFAULT '',
ALTER COLUMN "description" SET DATA TYPE TEXT,
ALTER COLUMN "amountEventsDescription" SET DEFAULT '',
ALTER COLUMN "amountEventsDescription" SET DATA TYPE TEXT,
ALTER COLUMN "amountChildrenDescription" SET DEFAULT '',
ALTER COLUMN "amountChildrenDescription" SET DATA TYPE TEXT;
