/*
  Warnings:

  - You are about to drop the column `participants` on the `Event` table. All the data in the column will be lost.
  - You are about to drop the column `waitingList` on the `Event` table. All the data in the column will be lost.

*/
-- DropForeignKey
ALTER TABLE "Event" DROP CONSTRAINT "Event_participants_fkey";

-- DropForeignKey
ALTER TABLE "Event" DROP CONSTRAINT "Event_waitingList_fkey";

-- DropIndex
DROP INDEX "Event_participants_idx";

-- DropIndex
DROP INDEX "Event_waitingList_idx";

-- AlterTable
ALTER TABLE "Event" DROP COLUMN "participants",
DROP COLUMN "waitingList";

-- CreateTable
CREATE TABLE "_Child_participantingEvents" (
    "A" TEXT NOT NULL,
    "B" TEXT NOT NULL
);

-- CreateTable
CREATE TABLE "_Child_waitingListEvents" (
    "A" TEXT NOT NULL,
    "B" TEXT NOT NULL
);

-- CreateIndex
CREATE UNIQUE INDEX "_Child_participantingEvents_AB_unique" ON "_Child_participantingEvents"("A", "B");

-- CreateIndex
CREATE INDEX "_Child_participantingEvents_B_index" ON "_Child_participantingEvents"("B");

-- CreateIndex
CREATE UNIQUE INDEX "_Child_waitingListEvents_AB_unique" ON "_Child_waitingListEvents"("A", "B");

-- CreateIndex
CREATE INDEX "_Child_waitingListEvents_B_index" ON "_Child_waitingListEvents"("B");

-- AddForeignKey
ALTER TABLE "_Child_participantingEvents" ADD CONSTRAINT "_Child_participantingEvents_A_fkey" FOREIGN KEY ("A") REFERENCES "Child"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_Child_participantingEvents" ADD CONSTRAINT "_Child_participantingEvents_B_fkey" FOREIGN KEY ("B") REFERENCES "Event"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_Child_waitingListEvents" ADD CONSTRAINT "_Child_waitingListEvents_A_fkey" FOREIGN KEY ("A") REFERENCES "Child"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_Child_waitingListEvents" ADD CONSTRAINT "_Child_waitingListEvents_B_fkey" FOREIGN KEY ("B") REFERENCES "Event"("id") ON DELETE CASCADE ON UPDATE CASCADE;
