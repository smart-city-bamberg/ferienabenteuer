/*
  Warnings:

  - You are about to drop the column `child` on the `Cart` table. All the data in the column will be lost.
  - You are about to drop the column `discountType` on the `Cart` table. All the data in the column will be lost.

*/
-- DropForeignKey
ALTER TABLE "Cart" DROP CONSTRAINT "Cart_child_fkey";

-- DropForeignKey
ALTER TABLE "Cart" DROP CONSTRAINT "Cart_discountType_fkey";

-- DropIndex
DROP INDEX "Cart_child_idx";

-- DropIndex
DROP INDEX "Cart_discountType_idx";

-- AlterTable
ALTER TABLE "Cart" DROP COLUMN "child",
DROP COLUMN "discountType";
