/*
  Warnings:

  - You are about to drop the column `firstname` on the `Discount` table. All the data in the column will be lost.
  - You are about to drop the column `surname` on the `Discount` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "Discount" DROP COLUMN "firstname",
DROP COLUMN "surname",
ADD COLUMN     "child" TEXT;

-- CreateIndex
CREATE INDEX "Discount_child_idx" ON "Discount"("child");

-- AddForeignKey
ALTER TABLE "Discount" ADD CONSTRAINT "Discount_child_fkey" FOREIGN KEY ("child") REFERENCES "Child"("id") ON DELETE SET NULL ON UPDATE CASCADE;
