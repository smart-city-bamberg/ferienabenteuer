-- CreateTable
CREATE TABLE "Custodian" (
    "id" TEXT NOT NULL,
    "salutation" TEXT,
    "firstname" TEXT NOT NULL DEFAULT '',
    "surname" TEXT NOT NULL DEFAULT '',
    "phone" TEXT NOT NULL DEFAULT '',
    "emergencyPhone" TEXT NOT NULL DEFAULT '',
    "addressCountry" TEXT NOT NULL DEFAULT '',
    "addressCity" TEXT NOT NULL DEFAULT '',
    "addressStreet" TEXT NOT NULL DEFAULT '',
    "addressHouseNumber" TEXT NOT NULL DEFAULT '',
    "addressZip" TEXT NOT NULL DEFAULT '',
    "addressAdditional" TEXT NOT NULL DEFAULT '',

    CONSTRAINT "Custodian_pkey" PRIMARY KEY ("id")
);
