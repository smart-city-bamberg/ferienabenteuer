/*
  Warnings:

  - You are about to drop the column `eventTicket` on the `CartTicket` table. All the data in the column will be lost.
  - A unique constraint covering the columns `[cartTicket]` on the table `EventTicket` will be added. If there are existing duplicate values, this will fail.

*/
-- DropForeignKey
ALTER TABLE "CartTicket" DROP CONSTRAINT "CartTicket_eventTicket_fkey";

-- DropIndex
DROP INDEX "CartTicket_eventTicket_key";

-- AlterTable
ALTER TABLE "CartTicket" DROP COLUMN "eventTicket";

-- AlterTable
ALTER TABLE "EventTicket" ADD COLUMN     "cartTicket" TEXT;

-- CreateIndex
CREATE UNIQUE INDEX "EventTicket_cartTicket_key" ON "EventTicket"("cartTicket");

-- AddForeignKey
ALTER TABLE "EventTicket" ADD CONSTRAINT "EventTicket_cartTicket_fkey" FOREIGN KEY ("cartTicket") REFERENCES "CartTicket"("id") ON DELETE SET NULL ON UPDATE CASCADE;
