/*
  Warnings:

  - You are about to drop the column `careDay0` on the `Event` table. All the data in the column will be lost.
  - You are about to drop the column `careDay1` on the `Event` table. All the data in the column will be lost.
  - You are about to drop the column `careDay2` on the `Event` table. All the data in the column will be lost.
  - You are about to drop the column `careDay3` on the `Event` table. All the data in the column will be lost.
  - You are about to drop the column `careDay4` on the `Event` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "Event" DROP COLUMN "careDay0",
DROP COLUMN "careDay1",
DROP COLUMN "careDay2",
DROP COLUMN "careDay3",
DROP COLUMN "careDay4";

-- CreateTable
CREATE TABLE "CareDay" (
    "id" TEXT NOT NULL,
    "day" DATE,
    "event" TEXT,

    CONSTRAINT "CareDay_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE INDEX "CareDay_event_idx" ON "CareDay"("event");

-- AddForeignKey
ALTER TABLE "CareDay" ADD CONSTRAINT "CareDay_event_fkey" FOREIGN KEY ("event") REFERENCES "Event"("id") ON DELETE SET NULL ON UPDATE CASCADE;
