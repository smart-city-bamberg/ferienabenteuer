-- CreateTable
CREATE TABLE "Invoice" (
    "id" TEXT NOT NULL,
    "event" TEXT,
    "createdAt" TIMESTAMP(3),
    "document_filesize" INTEGER,
    "document_filename" TEXT,

    CONSTRAINT "Invoice_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE INDEX "Invoice_event_idx" ON "Invoice"("event");

-- AddForeignKey
ALTER TABLE "Invoice" ADD CONSTRAINT "Invoice_event_fkey" FOREIGN KEY ("event") REFERENCES "Event"("id") ON DELETE SET NULL ON UPDATE CASCADE;
