-- AlterTable
ALTER TABLE "Event" ADD COLUMN     "meals" JSONB NOT NULL DEFAULT '[{"type":"paragraph","children":[{"text":""}]}]';

-- CreateTable
CREATE TABLE "EventCategory" (
    "id" TEXT NOT NULL,
    "name" TEXT NOT NULL DEFAULT '',

    CONSTRAINT "EventCategory_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "_Event_categories" (
    "A" TEXT NOT NULL,
    "B" TEXT NOT NULL
);

-- CreateIndex
CREATE UNIQUE INDEX "EventCategory_name_key" ON "EventCategory"("name");

-- CreateIndex
CREATE UNIQUE INDEX "_Event_categories_AB_unique" ON "_Event_categories"("A", "B");

-- CreateIndex
CREATE INDEX "_Event_categories_B_index" ON "_Event_categories"("B");

-- AddForeignKey
ALTER TABLE "_Event_categories" ADD CONSTRAINT "_Event_categories_A_fkey" FOREIGN KEY ("A") REFERENCES "Event"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_Event_categories" ADD CONSTRAINT "_Event_categories_B_fkey" FOREIGN KEY ("B") REFERENCES "EventCategory"("id") ON DELETE CASCADE ON UPDATE CASCADE;
