/*
  Warnings:

  - You are about to drop the column `discount` on the `Ticket` table. All the data in the column will be lost.
  - A unique constraint covering the columns `[ticket]` on the table `DiscountVoucher` will be added. If there are existing duplicate values, this will fail.

*/
-- DropForeignKey
ALTER TABLE "Ticket" DROP CONSTRAINT "Ticket_discount_fkey";

-- DropIndex
DROP INDEX "Ticket_discount_idx";

-- AlterTable
ALTER TABLE "DiscountVoucher" ADD COLUMN     "ticket" TEXT;

-- AlterTable
ALTER TABLE "Ticket" DROP COLUMN "discount";

-- CreateIndex
CREATE UNIQUE INDEX "DiscountVoucher_ticket_key" ON "DiscountVoucher"("ticket");

-- AddForeignKey
ALTER TABLE "DiscountVoucher" ADD CONSTRAINT "DiscountVoucher_ticket_fkey" FOREIGN KEY ("ticket") REFERENCES "Ticket"("id") ON DELETE SET NULL ON UPDATE CASCADE;
