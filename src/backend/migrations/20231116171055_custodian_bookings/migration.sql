-- AlterTable
ALTER TABLE "Booking" ADD COLUMN     "custodian" TEXT;

-- CreateIndex
CREATE INDEX "Booking_custodian_idx" ON "Booking"("custodian");

-- AddForeignKey
ALTER TABLE "Booking" ADD CONSTRAINT "Booking_custodian_fkey" FOREIGN KEY ("custodian") REFERENCES "Custodian"("id") ON DELETE SET NULL ON UPDATE CASCADE;
