/*
  Warnings:

  - You are about to drop the column `friday` on the `Event` table. All the data in the column will be lost.
  - You are about to drop the column `monday` on the `Event` table. All the data in the column will be lost.
  - You are about to drop the column `thursday` on the `Event` table. All the data in the column will be lost.
  - You are about to drop the column `tuesday` on the `Event` table. All the data in the column will be lost.
  - You are about to drop the column `wednesday` on the `Event` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "Event" DROP COLUMN "friday",
DROP COLUMN "monday",
DROP COLUMN "thursday",
DROP COLUMN "tuesday",
DROP COLUMN "wednesday",
ADD COLUMN     "careDay0" BOOLEAN NOT NULL DEFAULT false,
ADD COLUMN     "careDay1" BOOLEAN NOT NULL DEFAULT false,
ADD COLUMN     "careDay2" BOOLEAN NOT NULL DEFAULT false,
ADD COLUMN     "careDay3" BOOLEAN NOT NULL DEFAULT false,
ADD COLUMN     "careDay4" BOOLEAN NOT NULL DEFAULT false;
