-- AlterTable
ALTER TABLE "EarlyAccessCode" ADD COLUMN     "season" TEXT;

-- CreateIndex
CREATE INDEX "EarlyAccessCode_season_idx" ON "EarlyAccessCode"("season");

-- AddForeignKey
ALTER TABLE "EarlyAccessCode" ADD CONSTRAINT "EarlyAccessCode_season_fkey" FOREIGN KEY ("season") REFERENCES "Season"("id") ON DELETE SET NULL ON UPDATE CASCADE;
