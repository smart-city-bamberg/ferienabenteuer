-- AlterTable
ALTER TABLE "User" ADD COLUMN     "isEnabled" BOOLEAN NOT NULL DEFAULT false;

-- CreateTable
CREATE TABLE "UserVerificationToken" (
    "id" TEXT NOT NULL,
    "organizer" TEXT,
    "validationToken" TEXT NOT NULL DEFAULT '',
    "createdAt" TIMESTAMP(3),
    "validUntil" TIMESTAMP(3),

    CONSTRAINT "UserVerificationToken_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE INDEX "UserVerificationToken_organizer_idx" ON "UserVerificationToken"("organizer");

-- AddForeignKey
ALTER TABLE "UserVerificationToken" ADD CONSTRAINT "UserVerificationToken_organizer_fkey" FOREIGN KEY ("organizer") REFERENCES "User"("id") ON DELETE SET NULL ON UPDATE CASCADE;
