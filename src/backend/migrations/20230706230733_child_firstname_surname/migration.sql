/*
  Warnings:

  - You are about to drop the column `name` on the `Child` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "Child" DROP COLUMN "name",
ADD COLUMN     "firstname" TEXT NOT NULL DEFAULT '',
ADD COLUMN     "surname" TEXT NOT NULL DEFAULT '';
