/*
  Warnings:

  - You are about to drop the column `custodian` on the `CartTicket` table. All the data in the column will be lost.

*/
-- DropForeignKey
ALTER TABLE "CartTicket" DROP CONSTRAINT "CartTicket_custodian_fkey";

-- DropIndex
DROP INDEX "CartTicket_custodian_idx";

-- AlterTable
ALTER TABLE "CartTicket" DROP COLUMN "custodian";
