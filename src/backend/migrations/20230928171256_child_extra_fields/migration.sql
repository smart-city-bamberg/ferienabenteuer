/*
  Warnings:

  - You are about to drop the column `age` on the `Child` table. All the data in the column will be lost.
  - You are about to drop the column `assistance` on the `Child` table. All the data in the column will be lost.
  - You are about to drop the column `phone` on the `Child` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "Child" DROP COLUMN "age",
DROP COLUMN "assistance",
DROP COLUMN "phone",
ADD COLUMN     "assistanceRequired" BOOLEAN NOT NULL DEFAULT false,
ADD COLUMN     "custodian" TEXT,
ADD COLUMN     "dateOfBirth" DATE,
ADD COLUMN     "dietaryRegulations" TEXT NOT NULL DEFAULT '',
ADD COLUMN     "foodIntolerances" TEXT NOT NULL DEFAULT '',
ADD COLUMN     "gender" TEXT,
ADD COLUMN     "hasLiabilityInsurance" BOOLEAN NOT NULL DEFAULT false,
ADD COLUMN     "healthInsurer" TEXT NOT NULL DEFAULT '',
ADD COLUMN     "medication" TEXT NOT NULL DEFAULT '',
ADD COLUMN     "physicalImpairment" TEXT NOT NULL DEFAULT '';

-- CreateIndex
CREATE INDEX "Child_custodian_idx" ON "Child"("custodian");

-- AddForeignKey
ALTER TABLE "Child" ADD CONSTRAINT "Child_custodian_fkey" FOREIGN KEY ("custodian") REFERENCES "Custodian"("id") ON DELETE SET NULL ON UPDATE CASCADE;
