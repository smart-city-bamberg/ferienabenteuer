/*
  Warnings:

  - You are about to drop the column `lifetimeExtensionCount` on the `Cart` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "Cart" DROP COLUMN "lifetimeExtensionCount",
ADD COLUMN     "termExtensionCount" INTEGER NOT NULL DEFAULT 0;
