-- AlterTable
ALTER TABLE "Event" ADD COLUMN     "season" TEXT;

-- CreateIndex
CREATE INDEX "Event_season_idx" ON "Event"("season");

-- AddForeignKey
ALTER TABLE "Event" ADD CONSTRAINT "Event_season_fkey" FOREIGN KEY ("season") REFERENCES "Season"("id") ON DELETE SET NULL ON UPDATE CASCADE;
