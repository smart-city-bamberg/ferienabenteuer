/*
  Warnings:

  - A unique constraint covering the columns `[code]` on the table `EventTicket` will be added. If there are existing duplicate values, this will fail.

*/
-- AlterTable
ALTER TABLE "EventTicket" ADD COLUMN     "code" TEXT NOT NULL DEFAULT '',
ADD COLUMN     "number" INTEGER NOT NULL DEFAULT 0,
ADD COLUMN     "type" TEXT;

-- CreateIndex
CREATE UNIQUE INDEX "EventTicket_code_key" ON "EventTicket"("code");
