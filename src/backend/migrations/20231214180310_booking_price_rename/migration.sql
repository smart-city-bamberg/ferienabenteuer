/*
  Warnings:

  - You are about to drop the column `sum` on the `Booking` table. All the data in the column will be lost.
  - You are about to drop the column `cost` on the `Ticket` table. All the data in the column will be lost.
  - You are about to drop the `_Child_participantingEvents` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `_Child_waitingListEvents` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropForeignKey
ALTER TABLE "_Child_participantingEvents" DROP CONSTRAINT "_Child_participantingEvents_A_fkey";

-- DropForeignKey
ALTER TABLE "_Child_participantingEvents" DROP CONSTRAINT "_Child_participantingEvents_B_fkey";

-- DropForeignKey
ALTER TABLE "_Child_waitingListEvents" DROP CONSTRAINT "_Child_waitingListEvents_A_fkey";

-- DropForeignKey
ALTER TABLE "_Child_waitingListEvents" DROP CONSTRAINT "_Child_waitingListEvents_B_fkey";

-- AlterTable
ALTER TABLE "Booking" DROP COLUMN "sum",
ADD COLUMN     "price" DOUBLE PRECISION;

-- AlterTable
ALTER TABLE "Ticket" DROP COLUMN "cost",
ADD COLUMN     "price" DOUBLE PRECISION;

-- DropTable
DROP TABLE "_Child_participantingEvents";

-- DropTable
DROP TABLE "_Child_waitingListEvents";
