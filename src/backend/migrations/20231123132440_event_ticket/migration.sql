-- CreateTable
CREATE TABLE "EventTicket" (
    "id" TEXT NOT NULL,
    "event" TEXT,
    "version" INTEGER NOT NULL DEFAULT 1,

    CONSTRAINT "EventTicket_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE INDEX "EventTicket_event_idx" ON "EventTicket"("event");

-- AddForeignKey
ALTER TABLE "EventTicket" ADD CONSTRAINT "EventTicket_event_fkey" FOREIGN KEY ("event") REFERENCES "Event"("id") ON DELETE SET NULL ON UPDATE CASCADE;
