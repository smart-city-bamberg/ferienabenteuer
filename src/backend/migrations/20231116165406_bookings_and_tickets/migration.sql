/*
  Warnings:

  - You are about to drop the column `child` on the `Booking` table. All the data in the column will be lost.
  - A unique constraint covering the columns `[tickets]` on the table `Booking` will be added. If there are existing duplicate values, this will fail.

*/
-- DropForeignKey
ALTER TABLE "Booking" DROP CONSTRAINT "Booking_child_fkey";

-- DropIndex
DROP INDEX "Booking_child_idx";

-- AlterTable
ALTER TABLE "Booking" DROP COLUMN "child",
ADD COLUMN     "tickets" TEXT;

-- CreateTable
CREATE TABLE "Ticket" (
    "id" TEXT NOT NULL,
    "child" TEXT,
    "discount" TEXT,

    CONSTRAINT "Ticket_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE INDEX "Ticket_child_idx" ON "Ticket"("child");

-- CreateIndex
CREATE INDEX "Ticket_discount_idx" ON "Ticket"("discount");

-- CreateIndex
CREATE UNIQUE INDEX "Booking_tickets_key" ON "Booking"("tickets");

-- AddForeignKey
ALTER TABLE "Booking" ADD CONSTRAINT "Booking_tickets_fkey" FOREIGN KEY ("tickets") REFERENCES "Ticket"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Ticket" ADD CONSTRAINT "Ticket_child_fkey" FOREIGN KEY ("child") REFERENCES "Child"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Ticket" ADD CONSTRAINT "Ticket_discount_fkey" FOREIGN KEY ("discount") REFERENCES "Discount"("id") ON DELETE SET NULL ON UPDATE CASCADE;
