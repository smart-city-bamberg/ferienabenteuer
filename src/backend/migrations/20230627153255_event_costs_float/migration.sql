/*
  Warnings:

  - You are about to alter the column `costs` on the `Event` table. The data in that column could be lost. The data in that column will be cast from `Decimal(18,4)` to `DoublePrecision`.

*/
-- AlterTable
ALTER TABLE "Event" ALTER COLUMN "costs" SET DATA TYPE DOUBLE PRECISION;
