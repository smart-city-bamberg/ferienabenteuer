/*
  Warnings:

  - You are about to drop the column `location` on the `Event` table. All the data in the column will be lost.
  - You are about to drop the `Location` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropForeignKey
ALTER TABLE "Event" DROP CONSTRAINT "Event_location_fkey";

-- DropIndex
DROP INDEX "Event_location_idx";

-- AlterTable
ALTER TABLE "Event" DROP COLUMN "location",
ADD COLUMN     "locationCity" TEXT NOT NULL DEFAULT '',
ADD COLUMN     "locationStreet" TEXT NOT NULL DEFAULT '',
ADD COLUMN     "locationZipcode" TEXT NOT NULL DEFAULT '';

-- DropTable
DROP TABLE "Location";
