/*
  Warnings:

  - You are about to drop the column `addressCountry` on the `Custodian` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "Custodian" DROP COLUMN "addressCountry";
