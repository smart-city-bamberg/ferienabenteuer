-- AlterTable
ALTER TABLE "Discount" ADD COLUMN     "season" TEXT;

-- CreateIndex
CREATE INDEX "Discount_season_idx" ON "Discount"("season");

-- AddForeignKey
ALTER TABLE "Discount" ADD CONSTRAINT "Discount_season_fkey" FOREIGN KEY ("season") REFERENCES "Season"("id") ON DELETE SET NULL ON UPDATE CASCADE;
