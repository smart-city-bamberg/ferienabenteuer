/*
  Warnings:

  - You are about to drop the column `cartEvent` on the `CartTicket` table. All the data in the column will be lost.
  - You are about to drop the `CartEvent` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropForeignKey
ALTER TABLE "CartEvent" DROP CONSTRAINT "CartEvent_cart_fkey";

-- DropForeignKey
ALTER TABLE "CartEvent" DROP CONSTRAINT "CartEvent_event_fkey";

-- DropForeignKey
ALTER TABLE "CartTicket" DROP CONSTRAINT "CartTicket_cartEvent_fkey";

-- DropIndex
DROP INDEX "CartTicket_cartEvent_idx";

-- AlterTable
ALTER TABLE "CartTicket" DROP COLUMN "cartEvent",
ADD COLUMN     "cart" TEXT;

-- DropTable
DROP TABLE "CartEvent";

-- CreateIndex
CREATE INDEX "CartTicket_cart_idx" ON "CartTicket"("cart");

-- AddForeignKey
ALTER TABLE "CartTicket" ADD CONSTRAINT "CartTicket_cart_fkey" FOREIGN KEY ("cart") REFERENCES "Cart"("id") ON DELETE SET NULL ON UPDATE CASCADE;
