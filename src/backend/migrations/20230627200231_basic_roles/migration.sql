-- AlterTable
ALTER TABLE "User" ADD COLUMN     "isAdministrator" BOOLEAN NOT NULL DEFAULT false;

-- CreateTable
CREATE TABLE "Role" (
    "id" TEXT NOT NULL,
    "name" TEXT NOT NULL DEFAULT '',
    "eventsCreate" BOOLEAN NOT NULL DEFAULT false,
    "eventsDelete" BOOLEAN NOT NULL DEFAULT false,
    "eventsUpdate" BOOLEAN NOT NULL DEFAULT false,
    "eventsRead" BOOLEAN NOT NULL DEFAULT false,
    "organizerCreate" BOOLEAN NOT NULL DEFAULT false,
    "organizerDelete" BOOLEAN NOT NULL DEFAULT false,
    "organizerUpdate" BOOLEAN NOT NULL DEFAULT false,
    "organizerRead" BOOLEAN NOT NULL DEFAULT false,
    "holidaysCreate" BOOLEAN NOT NULL DEFAULT false,
    "holidaysDelete" BOOLEAN NOT NULL DEFAULT false,
    "holidaysUpdate" BOOLEAN NOT NULL DEFAULT false,
    "holidaysRead" BOOLEAN NOT NULL DEFAULT false,
    "locationCreate" BOOLEAN NOT NULL DEFAULT false,
    "locationDelete" BOOLEAN NOT NULL DEFAULT false,
    "locationUpdate" BOOLEAN NOT NULL DEFAULT false,
    "locationRead" BOOLEAN NOT NULL DEFAULT false,
    "childCreate" BOOLEAN NOT NULL DEFAULT false,
    "childDelete" BOOLEAN NOT NULL DEFAULT false,
    "childUpdate" BOOLEAN NOT NULL DEFAULT false,
    "childRead" BOOLEAN NOT NULL DEFAULT false,

    CONSTRAINT "Role_pkey" PRIMARY KEY ("id")
);
