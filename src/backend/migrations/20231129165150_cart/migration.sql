-- CreateTable
CREATE TABLE "DiscountVoucher" (
    "id" TEXT NOT NULL,
    "discount" TEXT,
    "redemptionDate" TIMESTAMP(3),

    CONSTRAINT "DiscountVoucher_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Cart" (
    "id" TEXT NOT NULL,
    "custodian" TEXT,
    "cartTickets" TEXT,
    "discountType" TEXT,
    "child" TEXT,
    "createdAt" TIMESTAMP(3) DEFAULT CURRENT_TIMESTAMP,
    "validUntil" TIMESTAMP(3),

    CONSTRAINT "Cart_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE INDEX "DiscountVoucher_discount_idx" ON "DiscountVoucher"("discount");

-- CreateIndex
CREATE UNIQUE INDEX "Cart_cartTickets_key" ON "Cart"("cartTickets");

-- CreateIndex
CREATE INDEX "Cart_custodian_idx" ON "Cart"("custodian");

-- CreateIndex
CREATE INDEX "Cart_discountType_idx" ON "Cart"("discountType");

-- CreateIndex
CREATE INDEX "Cart_child_idx" ON "Cart"("child");

-- AddForeignKey
ALTER TABLE "DiscountVoucher" ADD CONSTRAINT "DiscountVoucher_discount_fkey" FOREIGN KEY ("discount") REFERENCES "Discount"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Cart" ADD CONSTRAINT "Cart_custodian_fkey" FOREIGN KEY ("custodian") REFERENCES "Custodian"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Cart" ADD CONSTRAINT "Cart_cartTickets_fkey" FOREIGN KEY ("cartTickets") REFERENCES "CartTicket"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Cart" ADD CONSTRAINT "Cart_discountType_fkey" FOREIGN KEY ("discountType") REFERENCES "DiscountType"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Cart" ADD CONSTRAINT "Cart_child_fkey" FOREIGN KEY ("child") REFERENCES "Child"("id") ON DELETE SET NULL ON UPDATE CASCADE;
