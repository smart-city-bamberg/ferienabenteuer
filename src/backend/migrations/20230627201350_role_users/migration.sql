-- AlterTable
ALTER TABLE "Role" ADD COLUMN     "userCreate" BOOLEAN NOT NULL DEFAULT false,
ADD COLUMN     "userDelete" BOOLEAN NOT NULL DEFAULT false,
ADD COLUMN     "userRead" BOOLEAN NOT NULL DEFAULT false,
ADD COLUMN     "userUpdate" BOOLEAN NOT NULL DEFAULT false;

-- AlterTable
ALTER TABLE "User" ADD COLUMN     "roles" TEXT;

-- CreateIndex
CREATE INDEX "User_roles_idx" ON "User"("roles");

-- AddForeignKey
ALTER TABLE "User" ADD CONSTRAINT "User_roles_fkey" FOREIGN KEY ("roles") REFERENCES "Role"("id") ON DELETE SET NULL ON UPDATE CASCADE;
