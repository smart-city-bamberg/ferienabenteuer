/*
  Warnings:

  - You are about to drop the column `committedPeriod` on the `Event` table. All the data in the column will be lost.
  - You are about to drop the column `primaryPeriod` on the `Event` table. All the data in the column will be lost.
  - You are about to drop the column `secondaryPeriod` on the `Event` table. All the data in the column will be lost.

*/
-- DropForeignKey
ALTER TABLE "Event" DROP CONSTRAINT "Event_committedPeriod_fkey";

-- DropForeignKey
ALTER TABLE "Event" DROP CONSTRAINT "Event_primaryPeriod_fkey";

-- DropForeignKey
ALTER TABLE "Event" DROP CONSTRAINT "Event_secondaryPeriod_fkey";

-- DropIndex
DROP INDEX "Event_committedPeriod_idx";

-- DropIndex
DROP INDEX "Event_primaryPeriod_idx";

-- DropIndex
DROP INDEX "Event_secondaryPeriod_idx";

-- AlterTable
ALTER TABLE "Event" DROP COLUMN "committedPeriod",
DROP COLUMN "primaryPeriod",
DROP COLUMN "secondaryPeriod",
ADD COLUMN     "periodCommitted" TEXT,
ADD COLUMN     "periodPrimary" TEXT,
ADD COLUMN     "periodSecondary" TEXT,
ALTER COLUMN "description" SET DEFAULT '',
ALTER COLUMN "description" SET DATA TYPE TEXT,
ALTER COLUMN "hints" SET DEFAULT '',
ALTER COLUMN "hints" SET DATA TYPE TEXT,
ALTER COLUMN "meals" SET DEFAULT '',
ALTER COLUMN "meals" SET DATA TYPE TEXT;

-- CreateIndex
CREATE INDEX "Event_periodPrimary_idx" ON "Event"("periodPrimary");

-- CreateIndex
CREATE INDEX "Event_periodSecondary_idx" ON "Event"("periodSecondary");

-- CreateIndex
CREATE INDEX "Event_periodCommitted_idx" ON "Event"("periodCommitted");

-- AddForeignKey
ALTER TABLE "Event" ADD CONSTRAINT "Event_periodPrimary_fkey" FOREIGN KEY ("periodPrimary") REFERENCES "Period"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Event" ADD CONSTRAINT "Event_periodSecondary_fkey" FOREIGN KEY ("periodSecondary") REFERENCES "Period"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Event" ADD CONSTRAINT "Event_periodCommitted_fkey" FOREIGN KEY ("periodCommitted") REFERENCES "Period"("id") ON DELETE SET NULL ON UPDATE CASCADE;
