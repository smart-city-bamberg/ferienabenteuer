-- AlterTable
ALTER TABLE "Event" ADD COLUMN     "periodCommitted" TEXT,
ADD COLUMN     "periodPrimary" TEXT,
ADD COLUMN     "periodSecondary" TEXT;

-- CreateIndex
CREATE INDEX "Event_periodPrimary_idx" ON "Event"("periodPrimary");

-- CreateIndex
CREATE INDEX "Event_periodSecondary_idx" ON "Event"("periodSecondary");

-- CreateIndex
CREATE INDEX "Event_periodCommitted_idx" ON "Event"("periodCommitted");

-- AddForeignKey
ALTER TABLE "Event" ADD CONSTRAINT "Event_periodPrimary_fkey" FOREIGN KEY ("periodPrimary") REFERENCES "CarePeriod"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Event" ADD CONSTRAINT "Event_periodSecondary_fkey" FOREIGN KEY ("periodSecondary") REFERENCES "CarePeriod"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Event" ADD CONSTRAINT "Event_periodCommitted_fkey" FOREIGN KEY ("periodCommitted") REFERENCES "CarePeriod"("id") ON DELETE SET NULL ON UPDATE CASCADE;
