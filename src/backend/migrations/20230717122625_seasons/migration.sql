-- AlterTable
ALTER TABLE "Holiday" ADD COLUMN     "season" TEXT;

-- CreateTable
CREATE TABLE "Season" (
    "id" TEXT NOT NULL,
    "year" INTEGER,

    CONSTRAINT "Season_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "Season_year_key" ON "Season"("year");

-- CreateIndex
CREATE INDEX "Holiday_season_idx" ON "Holiday"("season");

-- AddForeignKey
ALTER TABLE "Holiday" ADD CONSTRAINT "Holiday_season_fkey" FOREIGN KEY ("season") REFERENCES "Season"("id") ON DELETE SET NULL ON UPDATE CASCADE;
