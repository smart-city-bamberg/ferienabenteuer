/*
  Warnings:

  - You are about to drop the column `holidays` on the `Event` table. All the data in the column will be lost.

*/
-- DropForeignKey
ALTER TABLE "Event" DROP CONSTRAINT "Event_holidays_fkey";

-- DropIndex
DROP INDEX "Event_holidays_idx";

-- AlterTable
ALTER TABLE "Event" DROP COLUMN "holidays",
ADD COLUMN     "careTimeEnd" TEXT NOT NULL DEFAULT '',
ADD COLUMN     "careTimeStart" TEXT NOT NULL DEFAULT '',
ADD COLUMN     "committedHolidays" TEXT,
ADD COLUMN     "contactPerson" TEXT NOT NULL DEFAULT '',
ADD COLUMN     "description" JSONB NOT NULL DEFAULT '[{"type":"paragraph","children":[{"text":""}]}]',
ADD COLUMN     "emergencyPhone" TEXT NOT NULL DEFAULT '',
ADD COLUMN     "friday" BOOLEAN NOT NULL DEFAULT false,
ADD COLUMN     "hints" JSONB NOT NULL DEFAULT '[{"type":"paragraph","children":[{"text":""}]}]',
ADD COLUMN     "monday" BOOLEAN NOT NULL DEFAULT false,
ADD COLUMN     "pictureRights" TEXT NOT NULL DEFAULT '',
ADD COLUMN     "picture_extension" TEXT,
ADD COLUMN     "picture_filesize" INTEGER,
ADD COLUMN     "picture_height" INTEGER,
ADD COLUMN     "picture_id" TEXT,
ADD COLUMN     "picture_width" INTEGER,
ADD COLUMN     "primaryHolidays" TEXT,
ADD COLUMN     "registrationDeadline" DATE,
ADD COLUMN     "secondaryHolidays" TEXT,
ADD COLUMN     "thursday" BOOLEAN NOT NULL DEFAULT false,
ADD COLUMN     "tuesday" BOOLEAN NOT NULL DEFAULT false,
ADD COLUMN     "wednesday" BOOLEAN NOT NULL DEFAULT false;

-- CreateIndex
CREATE INDEX "Event_primaryHolidays_idx" ON "Event"("primaryHolidays");

-- CreateIndex
CREATE INDEX "Event_secondaryHolidays_idx" ON "Event"("secondaryHolidays");

-- CreateIndex
CREATE INDEX "Event_committedHolidays_idx" ON "Event"("committedHolidays");

-- AddForeignKey
ALTER TABLE "Event" ADD CONSTRAINT "Event_primaryHolidays_fkey" FOREIGN KEY ("primaryHolidays") REFERENCES "Holiday"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Event" ADD CONSTRAINT "Event_secondaryHolidays_fkey" FOREIGN KEY ("secondaryHolidays") REFERENCES "Holiday"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Event" ADD CONSTRAINT "Event_committedHolidays_fkey" FOREIGN KEY ("committedHolidays") REFERENCES "Holiday"("id") ON DELETE SET NULL ON UPDATE CASCADE;
