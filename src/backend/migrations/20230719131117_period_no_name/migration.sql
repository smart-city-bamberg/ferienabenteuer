/*
  Warnings:

  - You are about to drop the column `name` on the `Period` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "Period" DROP COLUMN "name";
