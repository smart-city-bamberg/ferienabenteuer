-- CreateTable
CREATE TABLE "_EmailTemplate_bccRecipients" (
    "A" TEXT NOT NULL,
    "B" TEXT NOT NULL
);

-- CreateIndex
CREATE UNIQUE INDEX "_EmailTemplate_bccRecipients_AB_unique" ON "_EmailTemplate_bccRecipients"("A", "B");

-- CreateIndex
CREATE INDEX "_EmailTemplate_bccRecipients_B_index" ON "_EmailTemplate_bccRecipients"("B");

-- AddForeignKey
ALTER TABLE "_EmailTemplate_bccRecipients" ADD CONSTRAINT "_EmailTemplate_bccRecipients_A_fkey" FOREIGN KEY ("A") REFERENCES "EmailTemplate"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_EmailTemplate_bccRecipients" ADD CONSTRAINT "_EmailTemplate_bccRecipients_B_fkey" FOREIGN KEY ("B") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE CASCADE;
