-- CreateTable
CREATE TABLE "Event" (
    "id" TEXT NOT NULL,
    "name" TEXT NOT NULL DEFAULT '',
    "status" TEXT,
    "holidays" TEXT,
    "organizer" TEXT,
    "ageMinimum" INTEGER,
    "ageMaximum" INTEGER,
    "location" TEXT,
    "costs" DECIMAL(18,4),
    "participantLimit" INTEGER,
    "participants" TEXT,
    "waitingListLimit" INTEGER,
    "waitingList" TEXT,

    CONSTRAINT "Event_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Holiday" (
    "id" TEXT NOT NULL,
    "name" TEXT NOT NULL DEFAULT '',
    "startDate" DATE,
    "endDate" DATE,

    CONSTRAINT "Holiday_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Organizer" (
    "id" TEXT NOT NULL,
    "name" TEXT NOT NULL DEFAULT '',

    CONSTRAINT "Organizer_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Location" (
    "id" TEXT NOT NULL,
    "city" TEXT NOT NULL DEFAULT '',
    "zipcode" TEXT NOT NULL DEFAULT '',
    "street" TEXT NOT NULL DEFAULT '',

    CONSTRAINT "Location_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Child" (
    "id" TEXT NOT NULL,
    "name" TEXT NOT NULL DEFAULT '',

    CONSTRAINT "Child_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE INDEX "Event_holidays_idx" ON "Event"("holidays");

-- CreateIndex
CREATE INDEX "Event_organizer_idx" ON "Event"("organizer");

-- CreateIndex
CREATE INDEX "Event_location_idx" ON "Event"("location");

-- CreateIndex
CREATE INDEX "Event_participants_idx" ON "Event"("participants");

-- CreateIndex
CREATE INDEX "Event_waitingList_idx" ON "Event"("waitingList");

-- AddForeignKey
ALTER TABLE "Event" ADD CONSTRAINT "Event_holidays_fkey" FOREIGN KEY ("holidays") REFERENCES "Holiday"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Event" ADD CONSTRAINT "Event_organizer_fkey" FOREIGN KEY ("organizer") REFERENCES "Organizer"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Event" ADD CONSTRAINT "Event_location_fkey" FOREIGN KEY ("location") REFERENCES "Location"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Event" ADD CONSTRAINT "Event_participants_fkey" FOREIGN KEY ("participants") REFERENCES "Child"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Event" ADD CONSTRAINT "Event_waitingList_fkey" FOREIGN KEY ("waitingList") REFERENCES "Child"("id") ON DELETE SET NULL ON UPDATE CASCADE;
