/*
  Warnings:

  - You are about to drop the column `firstname` on the `Booking` table. All the data in the column will be lost.
  - You are about to drop the column `surname` on the `Booking` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "Booking" DROP COLUMN "firstname",
DROP COLUMN "surname",
ADD COLUMN     "child" TEXT;

-- CreateIndex
CREATE INDEX "Booking_child_idx" ON "Booking"("child");

-- AddForeignKey
ALTER TABLE "Booking" ADD CONSTRAINT "Booking_child_fkey" FOREIGN KEY ("child") REFERENCES "Child"("id") ON DELETE SET NULL ON UPDATE CASCADE;
