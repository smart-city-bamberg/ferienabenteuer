/*
  Warnings:

  - You are about to drop the column `cart` on the `CartTicket` table. All the data in the column will be lost.
  - You are about to drop the `Cart` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropForeignKey
ALTER TABLE "Cart" DROP CONSTRAINT "Cart_custodian_fkey";

-- DropForeignKey
ALTER TABLE "CartTicket" DROP CONSTRAINT "CartTicket_cart_fkey";

-- DropIndex
DROP INDEX "CartTicket_cart_idx";

-- AlterTable
ALTER TABLE "CartTicket" DROP COLUMN "cart",
ADD COLUMN     "custodian" TEXT;

-- DropTable
DROP TABLE "Cart";

-- CreateIndex
CREATE INDEX "CartTicket_custodian_idx" ON "CartTicket"("custodian");

-- AddForeignKey
ALTER TABLE "CartTicket" ADD CONSTRAINT "CartTicket_custodian_fkey" FOREIGN KEY ("custodian") REFERENCES "Custodian"("id") ON DELETE SET NULL ON UPDATE CASCADE;
