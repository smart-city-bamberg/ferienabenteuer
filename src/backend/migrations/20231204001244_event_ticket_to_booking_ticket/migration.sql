/*
  Warnings:

  - A unique constraint covering the columns `[bookedTicket]` on the table `EventTicket` will be added. If there are existing duplicate values, this will fail.

*/
-- AlterTable
ALTER TABLE "EventTicket" ADD COLUMN     "bookedTicket" TEXT;

-- CreateIndex
CREATE UNIQUE INDEX "EventTicket_bookedTicket_key" ON "EventTicket"("bookedTicket");

-- AddForeignKey
ALTER TABLE "EventTicket" ADD CONSTRAINT "EventTicket_bookedTicket_fkey" FOREIGN KEY ("bookedTicket") REFERENCES "Ticket"("id") ON DELETE SET NULL ON UPDATE CASCADE;
