/*
  Warnings:

  - A unique constraint covering the columns `[validationToken]` on the table `UserVerificationToken` will be added. If there are existing duplicate values, this will fail.

*/
-- CreateIndex
CREATE UNIQUE INDEX "UserVerificationToken_validationToken_key" ON "UserVerificationToken"("validationToken");
