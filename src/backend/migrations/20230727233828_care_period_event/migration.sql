/*
  Warnings:

  - You are about to drop the column `periodCommitted` on the `Event` table. All the data in the column will be lost.
  - A unique constraint covering the columns `[event]` on the table `CarePeriod` will be added. If there are existing duplicate values, this will fail.

*/
-- DropForeignKey
ALTER TABLE "Event" DROP CONSTRAINT "Event_periodCommitted_fkey";

-- DropIndex
DROP INDEX "Event_periodCommitted_idx";

-- AlterTable
ALTER TABLE "CarePeriod" ADD COLUMN     "event" TEXT;

-- AlterTable
ALTER TABLE "Event" DROP COLUMN "periodCommitted";

-- CreateIndex
CREATE UNIQUE INDEX "CarePeriod_event_key" ON "CarePeriod"("event");

-- AddForeignKey
ALTER TABLE "CarePeriod" ADD CONSTRAINT "CarePeriod_event_fkey" FOREIGN KEY ("event") REFERENCES "Event"("id") ON DELETE SET NULL ON UPDATE CASCADE;
