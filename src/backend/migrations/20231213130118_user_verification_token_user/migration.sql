/*
  Warnings:

  - You are about to drop the column `organizer` on the `UserVerificationToken` table. All the data in the column will be lost.

*/
-- DropForeignKey
ALTER TABLE "UserVerificationToken" DROP CONSTRAINT "UserVerificationToken_organizer_fkey";

-- DropIndex
DROP INDEX "UserVerificationToken_organizer_idx";

-- AlterTable
ALTER TABLE "UserVerificationToken" DROP COLUMN "organizer",
ADD COLUMN     "user" TEXT;

-- CreateIndex
CREATE INDEX "UserVerificationToken_user_idx" ON "UserVerificationToken"("user");

-- AddForeignKey
ALTER TABLE "UserVerificationToken" ADD CONSTRAINT "UserVerificationToken_user_fkey" FOREIGN KEY ("user") REFERENCES "User"("id") ON DELETE SET NULL ON UPDATE CASCADE;
