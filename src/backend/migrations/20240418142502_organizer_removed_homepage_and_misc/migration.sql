/*
  Warnings:

  - You are about to drop the column `homepage` on the `Organizer` table. All the data in the column will be lost.
  - You are about to drop the column `misc` on the `Organizer` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "Organizer" DROP COLUMN "homepage",
DROP COLUMN "misc";
