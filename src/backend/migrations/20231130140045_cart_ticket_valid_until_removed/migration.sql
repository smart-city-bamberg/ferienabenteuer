/*
  Warnings:

  - You are about to drop the column `validUntil` on the `CartTicket` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "CartTicket" DROP COLUMN "validUntil";
