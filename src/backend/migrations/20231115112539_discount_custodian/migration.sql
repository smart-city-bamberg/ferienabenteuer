-- AlterTable
ALTER TABLE "Discount" ADD COLUMN     "custodian" TEXT;

-- CreateIndex
CREATE INDEX "Discount_custodian_idx" ON "Discount"("custodian");

-- AddForeignKey
ALTER TABLE "Discount" ADD CONSTRAINT "Discount_custodian_fkey" FOREIGN KEY ("custodian") REFERENCES "Custodian"("id") ON DELETE SET NULL ON UPDATE CASCADE;
