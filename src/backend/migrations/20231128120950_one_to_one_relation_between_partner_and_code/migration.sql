/*
  Warnings:

  - You are about to drop the column `earlyAccessCode` on the `Partner` table. All the data in the column will be lost.
  - A unique constraint covering the columns `[partner]` on the table `EarlyAccessCode` will be added. If there are existing duplicate values, this will fail.

*/
-- DropForeignKey
ALTER TABLE "Partner" DROP CONSTRAINT "Partner_earlyAccessCode_fkey";

-- DropIndex
DROP INDEX "Partner_earlyAccessCode_idx";

-- AlterTable
ALTER TABLE "EarlyAccessCode" ADD COLUMN     "partner" TEXT;

-- AlterTable
ALTER TABLE "Partner" DROP COLUMN "earlyAccessCode";

-- CreateIndex
CREATE UNIQUE INDEX "EarlyAccessCode_partner_key" ON "EarlyAccessCode"("partner");

-- AddForeignKey
ALTER TABLE "EarlyAccessCode" ADD CONSTRAINT "EarlyAccessCode_partner_fkey" FOREIGN KEY ("partner") REFERENCES "Partner"("id") ON DELETE SET NULL ON UPDATE CASCADE;
