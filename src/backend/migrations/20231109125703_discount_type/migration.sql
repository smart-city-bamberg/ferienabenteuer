-- AlterTable
ALTER TABLE "Discount" ADD COLUMN     "type" TEXT;

-- CreateTable
CREATE TABLE "DiscountType" (
    "id" TEXT NOT NULL,
    "name" TEXT NOT NULL DEFAULT '',
    "discountPercent" DOUBLE PRECISION,
    "description" JSONB NOT NULL DEFAULT '[{"type":"paragraph","children":[{"text":""}]}]',
    "amountEvents" INTEGER,
    "amountChildren" INTEGER,
    "amountEventsDescription" JSONB NOT NULL DEFAULT '[{"type":"paragraph","children":[{"text":""}]}]',
    "amountChildrenDescription" JSONB NOT NULL DEFAULT '[{"type":"paragraph","children":[{"text":""}]}]',

    CONSTRAINT "DiscountType_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE INDEX "Discount_type_idx" ON "Discount"("type");

-- AddForeignKey
ALTER TABLE "Discount" ADD CONSTRAINT "Discount_type_fkey" FOREIGN KEY ("type") REFERENCES "DiscountType"("id") ON DELETE SET NULL ON UPDATE CASCADE;
