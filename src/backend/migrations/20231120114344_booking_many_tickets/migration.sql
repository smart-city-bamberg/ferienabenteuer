/*
  Warnings:

  - You are about to drop the column `tickets` on the `Booking` table. All the data in the column will be lost.

*/
-- DropForeignKey
ALTER TABLE "Booking" DROP CONSTRAINT "Booking_tickets_fkey";

-- DropIndex
DROP INDEX "Booking_tickets_key";

-- AlterTable
ALTER TABLE "Booking" DROP COLUMN "tickets";

-- AlterTable
ALTER TABLE "Ticket" ADD COLUMN     "booking" TEXT;

-- CreateIndex
CREATE INDEX "Ticket_booking_idx" ON "Ticket"("booking");

-- AddForeignKey
ALTER TABLE "Ticket" ADD CONSTRAINT "Ticket_booking_fkey" FOREIGN KEY ("booking") REFERENCES "Booking"("id") ON DELETE SET NULL ON UPDATE CASCADE;
