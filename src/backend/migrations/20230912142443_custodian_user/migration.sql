-- AlterTable
ALTER TABLE "Custodian" ADD COLUMN     "user" TEXT;

-- CreateIndex
CREATE INDEX "Custodian_user_idx" ON "Custodian"("user");

-- AddForeignKey
ALTER TABLE "Custodian" ADD CONSTRAINT "Custodian_user_fkey" FOREIGN KEY ("user") REFERENCES "User"("id") ON DELETE SET NULL ON UPDATE CASCADE;
