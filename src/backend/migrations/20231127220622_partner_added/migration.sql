-- CreateTable
CREATE TABLE "Partner" (
    "id" TEXT NOT NULL,
    "name" TEXT NOT NULL DEFAULT '',
    "contactPersonSalutation" TEXT,
    "contactPersonFirstname" TEXT NOT NULL DEFAULT '',
    "contactPersonSurname" TEXT NOT NULL DEFAULT '',
    "email" TEXT NOT NULL DEFAULT '',
    "phone" TEXT NOT NULL DEFAULT '',

    CONSTRAINT "Partner_pkey" PRIMARY KEY ("id")
);
