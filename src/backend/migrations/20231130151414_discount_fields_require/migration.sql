/*
  Warnings:

  - Made the column `discountPercent` on table `DiscountType` required. This step will fail if there are existing NULL values in that column.
  - Made the column `amountEvents` on table `DiscountType` required. This step will fail if there are existing NULL values in that column.
  - Made the column `amountChildren` on table `DiscountType` required. This step will fail if there are existing NULL values in that column.

*/
-- AlterTable
ALTER TABLE "DiscountType" ALTER COLUMN "discountPercent" SET NOT NULL,
ALTER COLUMN "amountEvents" SET NOT NULL,
ALTER COLUMN "amountChildren" SET NOT NULL;
