-- CreateTable
CREATE TABLE "Booking" (
    "id" TEXT NOT NULL,
    "paycode" TEXT NOT NULL DEFAULT '',
    "sum" DOUBLE PRECISION,
    "status" TEXT,
    "firstname" TEXT NOT NULL DEFAULT '',
    "surname" TEXT NOT NULL DEFAULT '',
    "event" TEXT,
    "date" DATE,

    CONSTRAINT "Booking_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE INDEX "Booking_event_idx" ON "Booking"("event");

-- AddForeignKey
ALTER TABLE "Booking" ADD CONSTRAINT "Booking_event_fkey" FOREIGN KEY ("event") REFERENCES "Event"("id") ON DELETE SET NULL ON UPDATE CASCADE;
