-- CreateTable
CREATE TABLE "Discount" (
    "id" TEXT NOT NULL,
    "status" TEXT,
    "firstname" TEXT NOT NULL DEFAULT '',
    "surname" TEXT NOT NULL DEFAULT '',
    "submissionDate" DATE,
    "kind" TEXT,
    "rejectionReason" TEXT,
    "validUntil" DATE,
    "redeemLimit" INTEGER,

    CONSTRAINT "Discount_pkey" PRIMARY KEY ("id")
);
