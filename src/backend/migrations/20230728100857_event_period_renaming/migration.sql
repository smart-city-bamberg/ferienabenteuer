/*
  Warnings:

  - You are about to drop the column `periodPrimary` on the `Event` table. All the data in the column will be lost.
  - You are about to drop the column `periodSecondary` on the `Event` table. All the data in the column will be lost.

*/
-- DropForeignKey
ALTER TABLE "Event" DROP CONSTRAINT "Event_periodPrimary_fkey";

-- DropForeignKey
ALTER TABLE "Event" DROP CONSTRAINT "Event_periodSecondary_fkey";

-- DropIndex
DROP INDEX "Event_periodPrimary_idx";

-- DropIndex
DROP INDEX "Event_periodSecondary_idx";

-- AlterTable
ALTER TABLE "Event" DROP COLUMN "periodPrimary",
DROP COLUMN "periodSecondary",
ADD COLUMN     "periodAlternative" TEXT,
ADD COLUMN     "periodDesired" TEXT;

-- CreateIndex
CREATE INDEX "Event_periodDesired_idx" ON "Event"("periodDesired");

-- CreateIndex
CREATE INDEX "Event_periodAlternative_idx" ON "Event"("periodAlternative");

-- AddForeignKey
ALTER TABLE "Event" ADD CONSTRAINT "Event_periodDesired_fkey" FOREIGN KEY ("periodDesired") REFERENCES "CarePeriod"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Event" ADD CONSTRAINT "Event_periodAlternative_fkey" FOREIGN KEY ("periodAlternative") REFERENCES "CarePeriod"("id") ON DELETE SET NULL ON UPDATE CASCADE;
