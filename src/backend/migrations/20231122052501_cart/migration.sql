-- CreateTable
CREATE TABLE "Cart" (
    "id" TEXT NOT NULL,
    "custodian" TEXT,

    CONSTRAINT "Cart_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "CartEvent" (
    "id" TEXT NOT NULL,
    "cart" TEXT,
    "event" TEXT,

    CONSTRAINT "CartEvent_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "CartTicket" (
    "id" TEXT NOT NULL,
    "cartEvent" TEXT,
    "discountType" TEXT,
    "child" TEXT,

    CONSTRAINT "CartTicket_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE INDEX "Cart_custodian_idx" ON "Cart"("custodian");

-- CreateIndex
CREATE INDEX "CartEvent_cart_idx" ON "CartEvent"("cart");

-- CreateIndex
CREATE INDEX "CartEvent_event_idx" ON "CartEvent"("event");

-- CreateIndex
CREATE INDEX "CartTicket_cartEvent_idx" ON "CartTicket"("cartEvent");

-- CreateIndex
CREATE INDEX "CartTicket_discountType_idx" ON "CartTicket"("discountType");

-- CreateIndex
CREATE INDEX "CartTicket_child_idx" ON "CartTicket"("child");

-- AddForeignKey
ALTER TABLE "Cart" ADD CONSTRAINT "Cart_custodian_fkey" FOREIGN KEY ("custodian") REFERENCES "Custodian"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "CartEvent" ADD CONSTRAINT "CartEvent_cart_fkey" FOREIGN KEY ("cart") REFERENCES "Cart"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "CartEvent" ADD CONSTRAINT "CartEvent_event_fkey" FOREIGN KEY ("event") REFERENCES "Event"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "CartTicket" ADD CONSTRAINT "CartTicket_cartEvent_fkey" FOREIGN KEY ("cartEvent") REFERENCES "CartEvent"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "CartTicket" ADD CONSTRAINT "CartTicket_discountType_fkey" FOREIGN KEY ("discountType") REFERENCES "DiscountType"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "CartTicket" ADD CONSTRAINT "CartTicket_child_fkey" FOREIGN KEY ("child") REFERENCES "Child"("id") ON DELETE SET NULL ON UPDATE CASCADE;
