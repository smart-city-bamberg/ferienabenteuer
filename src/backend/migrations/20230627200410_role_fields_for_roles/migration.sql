-- AlterTable
ALTER TABLE "Role" ADD COLUMN     "rolesCreate" BOOLEAN NOT NULL DEFAULT false,
ADD COLUMN     "rolesDelete" BOOLEAN NOT NULL DEFAULT false,
ADD COLUMN     "rolesRead" BOOLEAN NOT NULL DEFAULT false,
ADD COLUMN     "rolesUpdate" BOOLEAN NOT NULL DEFAULT false;
