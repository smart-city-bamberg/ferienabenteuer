-- CreateTable
CREATE TABLE "EarlyAccessCodeVoucher" (
    "id" TEXT NOT NULL,
    "code" TEXT,
    "season" TEXT,
    "custodian" TEXT,

    CONSTRAINT "EarlyAccessCodeVoucher_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE INDEX "EarlyAccessCodeVoucher_code_idx" ON "EarlyAccessCodeVoucher"("code");

-- CreateIndex
CREATE INDEX "EarlyAccessCodeVoucher_season_idx" ON "EarlyAccessCodeVoucher"("season");

-- CreateIndex
CREATE INDEX "EarlyAccessCodeVoucher_custodian_idx" ON "EarlyAccessCodeVoucher"("custodian");

-- AddForeignKey
ALTER TABLE "EarlyAccessCodeVoucher" ADD CONSTRAINT "EarlyAccessCodeVoucher_code_fkey" FOREIGN KEY ("code") REFERENCES "EarlyAccessCode"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "EarlyAccessCodeVoucher" ADD CONSTRAINT "EarlyAccessCodeVoucher_season_fkey" FOREIGN KEY ("season") REFERENCES "Season"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "EarlyAccessCodeVoucher" ADD CONSTRAINT "EarlyAccessCodeVoucher_custodian_fkey" FOREIGN KEY ("custodian") REFERENCES "Custodian"("id") ON DELETE SET NULL ON UPDATE CASCADE;
