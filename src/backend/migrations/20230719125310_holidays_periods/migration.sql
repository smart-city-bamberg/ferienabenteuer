/*
  Warnings:

  - You are about to drop the column `committedHolidays` on the `Event` table. All the data in the column will be lost.
  - You are about to drop the column `primaryHolidays` on the `Event` table. All the data in the column will be lost.
  - You are about to drop the column `secondaryHolidays` on the `Event` table. All the data in the column will be lost.
  - You are about to drop the column `endDate` on the `Holiday` table. All the data in the column will be lost.
  - You are about to drop the column `season` on the `Holiday` table. All the data in the column will be lost.
  - You are about to drop the column `startDate` on the `Holiday` table. All the data in the column will be lost.

*/
-- DropForeignKey
ALTER TABLE "Event" DROP CONSTRAINT "Event_committedHolidays_fkey";

-- DropForeignKey
ALTER TABLE "Event" DROP CONSTRAINT "Event_primaryHolidays_fkey";

-- DropForeignKey
ALTER TABLE "Event" DROP CONSTRAINT "Event_secondaryHolidays_fkey";

-- DropForeignKey
ALTER TABLE "Holiday" DROP CONSTRAINT "Holiday_season_fkey";

-- DropIndex
DROP INDEX "Event_committedHolidays_idx";

-- DropIndex
DROP INDEX "Event_primaryHolidays_idx";

-- DropIndex
DROP INDEX "Event_secondaryHolidays_idx";

-- DropIndex
DROP INDEX "Holiday_season_idx";

-- AlterTable
ALTER TABLE "Event" DROP COLUMN "committedHolidays",
DROP COLUMN "primaryHolidays",
DROP COLUMN "secondaryHolidays",
ADD COLUMN     "committedPeriod" TEXT,
ADD COLUMN     "primaryPeriod" TEXT,
ADD COLUMN     "secondaryPeriod" TEXT;

-- AlterTable
ALTER TABLE "Holiday" DROP COLUMN "endDate",
DROP COLUMN "season",
DROP COLUMN "startDate";

-- CreateTable
CREATE TABLE "Period" (
    "id" TEXT NOT NULL,
    "name" TEXT NOT NULL DEFAULT '',
    "season" TEXT,
    "holiday" TEXT,
    "startDate" DATE,
    "endDate" DATE,

    CONSTRAINT "Period_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE INDEX "Period_season_idx" ON "Period"("season");

-- CreateIndex
CREATE INDEX "Period_holiday_idx" ON "Period"("holiday");

-- CreateIndex
CREATE INDEX "Event_primaryPeriod_idx" ON "Event"("primaryPeriod");

-- CreateIndex
CREATE INDEX "Event_secondaryPeriod_idx" ON "Event"("secondaryPeriod");

-- CreateIndex
CREATE INDEX "Event_committedPeriod_idx" ON "Event"("committedPeriod");

-- AddForeignKey
ALTER TABLE "Period" ADD CONSTRAINT "Period_season_fkey" FOREIGN KEY ("season") REFERENCES "Season"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Period" ADD CONSTRAINT "Period_holiday_fkey" FOREIGN KEY ("holiday") REFERENCES "Holiday"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Event" ADD CONSTRAINT "Event_primaryPeriod_fkey" FOREIGN KEY ("primaryPeriod") REFERENCES "Period"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Event" ADD CONSTRAINT "Event_secondaryPeriod_fkey" FOREIGN KEY ("secondaryPeriod") REFERENCES "Period"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Event" ADD CONSTRAINT "Event_committedPeriod_fkey" FOREIGN KEY ("committedPeriod") REFERENCES "Period"("id") ON DELETE SET NULL ON UPDATE CASCADE;
