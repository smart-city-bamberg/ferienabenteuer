-- AlterTable
ALTER TABLE "Season" ADD COLUMN     "bookingStart" TIMESTAMP(3),
ADD COLUMN     "bookingStartPartners" TIMESTAMP(3),
ADD COLUMN     "grantAmount" DOUBLE PRECISION,
ADD COLUMN     "releaseDate" TIMESTAMP(3);
