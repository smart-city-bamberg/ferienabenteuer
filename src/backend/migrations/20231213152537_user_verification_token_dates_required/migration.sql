/*
  Warnings:

  - Made the column `createdAt` on table `UserVerificationToken` required. This step will fail if there are existing NULL values in that column.
  - Made the column `validUntil` on table `UserVerificationToken` required. This step will fail if there are existing NULL values in that column.

*/
-- AlterTable
ALTER TABLE "UserVerificationToken" ALTER COLUMN "createdAt" SET NOT NULL,
ALTER COLUMN "validUntil" SET NOT NULL;
