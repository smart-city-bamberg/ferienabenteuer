/*
  Warnings:

  - You are about to drop the column `isRequst` on the `Booking` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "Booking" DROP COLUMN "isRequst";

-- AlterTable
ALTER TABLE "Ticket" ADD COLUMN     "reservation" TEXT;

-- CreateTable
CREATE TABLE "Reservation" (
    "id" TEXT NOT NULL,
    "price" DOUBLE PRECISION,
    "event" TEXT,
    "custodian" TEXT,
    "date" TIMESTAMP(3),

    CONSTRAINT "Reservation_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE INDEX "Reservation_event_idx" ON "Reservation"("event");

-- CreateIndex
CREATE INDEX "Reservation_custodian_idx" ON "Reservation"("custodian");

-- CreateIndex
CREATE INDEX "Ticket_reservation_idx" ON "Ticket"("reservation");

-- AddForeignKey
ALTER TABLE "Reservation" ADD CONSTRAINT "Reservation_event_fkey" FOREIGN KEY ("event") REFERENCES "Event"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Reservation" ADD CONSTRAINT "Reservation_custodian_fkey" FOREIGN KEY ("custodian") REFERENCES "Custodian"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Ticket" ADD CONSTRAINT "Ticket_reservation_fkey" FOREIGN KEY ("reservation") REFERENCES "Reservation"("id") ON DELETE SET NULL ON UPDATE CASCADE;
