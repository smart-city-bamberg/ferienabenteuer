/*
  Warnings:

  - You are about to drop the `_Organizer_users` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropForeignKey
ALTER TABLE "_Organizer_users" DROP CONSTRAINT "_Organizer_users_A_fkey";

-- DropForeignKey
ALTER TABLE "_Organizer_users" DROP CONSTRAINT "_Organizer_users_B_fkey";

-- AlterTable
ALTER TABLE "User" ADD COLUMN     "organizer" TEXT;

-- DropTable
DROP TABLE "_Organizer_users";

-- CreateIndex
CREATE INDEX "User_organizer_idx" ON "User"("organizer");

-- AddForeignKey
ALTER TABLE "User" ADD CONSTRAINT "User_organizer_fkey" FOREIGN KEY ("organizer") REFERENCES "Organizer"("id") ON DELETE SET NULL ON UPDATE CASCADE;
