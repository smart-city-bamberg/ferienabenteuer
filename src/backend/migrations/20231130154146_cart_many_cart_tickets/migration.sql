/*
  Warnings:

  - You are about to drop the column `cartTickets` on the `Cart` table. All the data in the column will be lost.

*/
-- DropForeignKey
ALTER TABLE "Cart" DROP CONSTRAINT "Cart_cartTickets_fkey";

-- DropIndex
DROP INDEX "Cart_cartTickets_key";

-- AlterTable
ALTER TABLE "Cart" DROP COLUMN "cartTickets";

-- AlterTable
ALTER TABLE "CartTicket" ADD COLUMN     "cart" TEXT;

-- CreateIndex
CREATE INDEX "CartTicket_cart_idx" ON "CartTicket"("cart");

-- AddForeignKey
ALTER TABLE "CartTicket" ADD CONSTRAINT "CartTicket_cart_fkey" FOREIGN KEY ("cart") REFERENCES "Cart"("id") ON DELETE SET NULL ON UPDATE CASCADE;
