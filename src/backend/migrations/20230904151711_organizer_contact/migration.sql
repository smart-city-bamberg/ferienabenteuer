/*
  Warnings:

  - You are about to drop the column `contactPerson` on the `Organizer` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "Organizer" DROP COLUMN "contactPerson",
ADD COLUMN     "contactPersonFirstname" TEXT NOT NULL DEFAULT '',
ADD COLUMN     "contactPersonSalutation" TEXT,
ADD COLUMN     "contactPersonSurname" TEXT NOT NULL DEFAULT '';
