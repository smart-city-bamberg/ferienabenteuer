-- CreateTable
CREATE TABLE "_Organizer_users" (
    "A" TEXT NOT NULL,
    "B" TEXT NOT NULL
);

-- CreateIndex
CREATE UNIQUE INDEX "_Organizer_users_AB_unique" ON "_Organizer_users"("A", "B");

-- CreateIndex
CREATE INDEX "_Organizer_users_B_index" ON "_Organizer_users"("B");

-- AddForeignKey
ALTER TABLE "_Organizer_users" ADD CONSTRAINT "_Organizer_users_A_fkey" FOREIGN KEY ("A") REFERENCES "Organizer"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_Organizer_users" ADD CONSTRAINT "_Organizer_users_B_fkey" FOREIGN KEY ("B") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE CASCADE;
