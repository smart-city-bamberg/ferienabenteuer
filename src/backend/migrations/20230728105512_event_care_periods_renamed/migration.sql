/*
  Warnings:

  - You are about to drop the column `periodAlternative` on the `Event` table. All the data in the column will be lost.
  - You are about to drop the column `periodDesired` on the `Event` table. All the data in the column will be lost.

*/
-- DropForeignKey
ALTER TABLE "Event" DROP CONSTRAINT "Event_periodAlternative_fkey";

-- DropForeignKey
ALTER TABLE "Event" DROP CONSTRAINT "Event_periodDesired_fkey";

-- DropIndex
DROP INDEX "Event_periodAlternative_idx";

-- DropIndex
DROP INDEX "Event_periodDesired_idx";

-- AlterTable
ALTER TABLE "Event" DROP COLUMN "periodAlternative",
DROP COLUMN "periodDesired",
ADD COLUMN     "carePeriodAlternative" TEXT,
ADD COLUMN     "carePeriodDesired" TEXT;

-- CreateIndex
CREATE INDEX "Event_carePeriodDesired_idx" ON "Event"("carePeriodDesired");

-- CreateIndex
CREATE INDEX "Event_carePeriodAlternative_idx" ON "Event"("carePeriodAlternative");

-- AddForeignKey
ALTER TABLE "Event" ADD CONSTRAINT "Event_carePeriodDesired_fkey" FOREIGN KEY ("carePeriodDesired") REFERENCES "CarePeriod"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Event" ADD CONSTRAINT "Event_carePeriodAlternative_fkey" FOREIGN KEY ("carePeriodAlternative") REFERENCES "CarePeriod"("id") ON DELETE SET NULL ON UPDATE CASCADE;
