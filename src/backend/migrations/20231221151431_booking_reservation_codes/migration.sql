/*
  Warnings:

  - You are about to drop the column `paycode` on the `Booking` table. All the data in the column will be lost.
  - You are about to drop the column `price` on the `Reservation` table. All the data in the column will be lost.
  - A unique constraint covering the columns `[code]` on the table `Booking` will be added. If there are existing duplicate values, this will fail.
  - A unique constraint covering the columns `[code]` on the table `Reservation` will be added. If there are existing duplicate values, this will fail.

*/
-- AlterTable
ALTER TABLE "Booking" DROP COLUMN "paycode",
ADD COLUMN     "code" TEXT NOT NULL DEFAULT '';

-- AlterTable
ALTER TABLE "Reservation" DROP COLUMN "price",
ADD COLUMN     "code" TEXT NOT NULL DEFAULT '';

-- CreateIndex
CREATE UNIQUE INDEX "Booking_code_key" ON "Booking"("code");

-- CreateIndex
CREATE UNIQUE INDEX "Reservation_code_key" ON "Reservation"("code");
