/*
  Warnings:

  - A unique constraint covering the columns `[eventTicket]` on the table `CartTicket` will be added. If there are existing duplicate values, this will fail.

*/
-- AlterTable
ALTER TABLE "CartTicket" ADD COLUMN     "eventTicket" TEXT;

-- CreateIndex
CREATE UNIQUE INDEX "CartTicket_eventTicket_key" ON "CartTicket"("eventTicket");

-- AddForeignKey
ALTER TABLE "CartTicket" ADD CONSTRAINT "CartTicket_eventTicket_fkey" FOREIGN KEY ("eventTicket") REFERENCES "EventTicket"("id") ON DELETE SET NULL ON UPDATE CASCADE;
