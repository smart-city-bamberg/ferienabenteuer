/*
  Warnings:

  - Made the column `code` on table `EarlyAccessCodeVoucher` required. This step will fail if there are existing NULL values in that column.

*/
-- DropForeignKey
ALTER TABLE "EarlyAccessCodeVoucher" DROP CONSTRAINT "EarlyAccessCodeVoucher_code_fkey";

-- DropIndex
DROP INDEX "EarlyAccessCodeVoucher_code_idx";

-- AlterTable
ALTER TABLE "EarlyAccessCodeVoucher" ALTER COLUMN "code" SET NOT NULL,
ALTER COLUMN "code" SET DEFAULT '';
