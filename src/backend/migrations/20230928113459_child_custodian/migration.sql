-- AlterTable
ALTER TABLE "Child" ADD COLUMN     "custodian" TEXT;

-- CreateIndex
CREATE INDEX "Child_custodian_idx" ON "Child"("custodian");

-- AddForeignKey
ALTER TABLE "Child" ADD CONSTRAINT "Child_custodian_fkey" FOREIGN KEY ("custodian") REFERENCES "Custodian"("id") ON DELETE SET NULL ON UPDATE CASCADE;
