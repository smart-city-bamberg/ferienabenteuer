/*
  Warnings:

  - You are about to drop the column `roles` on the `User` table. All the data in the column will be lost.

*/
-- DropForeignKey
ALTER TABLE "User" DROP CONSTRAINT "User_roles_fkey";

-- DropIndex
DROP INDEX "User_roles_idx";

-- AlterTable
ALTER TABLE "User" DROP COLUMN "roles";

-- CreateTable
CREATE TABLE "_User_roles" (
    "A" TEXT NOT NULL,
    "B" TEXT NOT NULL
);

-- CreateIndex
CREATE UNIQUE INDEX "_User_roles_AB_unique" ON "_User_roles"("A", "B");

-- CreateIndex
CREATE INDEX "_User_roles_B_index" ON "_User_roles"("B");

-- AddForeignKey
ALTER TABLE "_User_roles" ADD CONSTRAINT "_User_roles_A_fkey" FOREIGN KEY ("A") REFERENCES "Role"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_User_roles" ADD CONSTRAINT "_User_roles_B_fkey" FOREIGN KEY ("B") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE CASCADE;
