import {
  BaseKeystoneTypeInfo,
  DatabaseConfig,
  GraphQLConfig,
  ServerConfig,
  StorageConfig,
} from "@keystone-6/core/types";

import { readFileSync } from "fs";

const host = process.env.HOST_PUBLIC_NAME ?? "http://localhost:3000";
const port = process.env.HOST_PORT ? parseInt(process.env.HOST_PORT) : 3000;

const hostPath = process.env.HOST_PATH ?? "";
const corsOrigin = process.env.CORS_ORIGIN?.split(",") ?? [
  "http://localhost:4200",
  "http://localhost:4300",
];

const storagePath = process.env.STORAGE_PATH ?? "./storage_production";

let databaseUrl =
  process.env.DATABASE_URL ??
  "postgres://keystone:password@localhost:5432/keystone";

if (process.env.DATABASE_URL_FILE) {
  const fileContent = readFileSync(process.env.DATABASE_URL_FILE);
  databaseUrl = fileContent.toString();
}

const enablePlayground =
  process.env.PLAYGROUND_ENABLED == "true" ||
  process.env.NODE_ENV !== "production";

const enableIntrospection =
  process.env.INTROSPECTION_ENABLED == "true" ||
  process.env.NODE_ENV !== "production";

export const environment = {
  db: <DatabaseConfig<BaseKeystoneTypeInfo>>{
    provider: "postgresql",
    url: databaseUrl,
    useMigrations: true,
    // enableLogging: true,
  },
  server: <ServerConfig<BaseKeystoneTypeInfo>>{
    options: {
      path: hostPath,
      port: port,
    },
    cors: {
      origin: corsOrigin,
      credentials: true,
    },
  },
  graphql: <GraphQLConfig<BaseKeystoneTypeInfo>>{
    playground: enablePlayground,
    apolloConfig: {
      introspection: enableIntrospection,
    },
  },
  storage: <Record<string, StorageConfig>>{
    assets: {
      type: "file",
      kind: "local",
      storagePath: `./assets`,
      generateUrl: (path: string) => `${host}/assets${path}`,
      serverRoute: {
        path: "/assets",
      },
    },
    image_store: {
      type: "image",
      kind: "local",
      storagePath: `${storagePath}/images`,
      generateUrl: (path: string) => `${host}/images${path}`,
      serverRoute: {
        path: "/images",
      },
    },
    invoice_store: {
      type: "file",
      kind: "local",
      storagePath: `${storagePath}/invoices`,
      generateUrl: (path: string) => `${host}/invoice-documents${path}`,
      serverRoute: {
        path: "/invoice-documents",
      },
    },
  },
};
