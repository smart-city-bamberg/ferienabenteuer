import { Context } from ".keystone/types";
import { CronJob } from "cron";
import { oneHour } from "../schemaExtensions";

export const markOverdueBookings = (context: Context) =>
  new CronJob(
    "0 * * * * *", // cronTime
    async function () {
      const timestamp = new Date(Date.now() - 14 * 24 * oneHour);

      const overdueBookings = await context.prisma.booking.findMany({
        where: {
          date: {
            lt: timestamp,
          },
          status: {
            equals: "pending",
          },
        },
      });

      if (overdueBookings.length === 0) {
        return;
      }

      console.log(
        `Found ${overdueBookings.length} overdue bookings older than ${timestamp}`
      );

      for (let booking of overdueBookings) {
        await context.prisma.$transaction(async (transaction) => {
          await transaction.booking.update({
            where: {
              id: booking.id,
            },
            data: {
              status: "overdue",
            },
          });
        });
      }
    }, // onTick
    null, // onComplete
    true, // start
    "Europe/Berlin" // timeZone
  );
