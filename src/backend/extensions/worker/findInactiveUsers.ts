import { Context } from ".keystone/types";
import { CronJob } from "cron";
import { IMessageQueue } from "../eventQueue";

export const findInactiveUsers = (
  context: Context,
  eventQueue: IMessageQueue
) =>
  new CronJob(
    "0 * * * * *", // cronTime
    async function () {
      const now = new Date();
      now.setFullYear(now.getFullYear() - 3);
      const offset = now.getTimezoneOffset();
      const nowWithTimezone = new Date(now.getTime() - offset * 60 * 1000);
      const day = nowWithTimezone.toISOString();

      const inactiveUsers = (await context.query.User.findMany({
        where: {
          OR: [
            {
              lastLogin: {
                lt: day,
              },
            },
            {
              lastLogin: {
                equals: null,
              },
              createdAt: {
                lt: day,
              },
            },
          ],
          markedForDeletionAt: {
            equals: null,
          },
          role: {
            equals: "custodian",
          },
        },
        query: `
          id          
        `,
      })) as { id: string }[];

      if (inactiveUsers.length === 0) {
        return;
      }

      console.log(
        `Found ${inactiveUsers.length} inactive users older than ${day}`
      );

      for (let inactiveUser of inactiveUsers) {
        await context.prisma.$transaction(async (transaction) => {
          await transaction.user.update({
            where: {
              id: inactiveUser.id,
            },
            data: {
              markedForDeletionAt: now,
            },
          });
        });
      }

      await eventQueue.addBulk(
        inactiveUsers.map((user) => ({
          name: "userMarkedForDeletion",
          data: { id: user.id },
        }))
      );
    }, // onTick
    null, // onComplete
    true, // start
    "Europe/Berlin" // timeZone
  );
