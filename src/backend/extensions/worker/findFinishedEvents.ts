import { Context } from ".keystone/types";
import { CronJob } from "cron";
import { IMessageQueue } from "../eventQueue";

export const findFinishedEvents = (
  context: Context,
  eventQueue: IMessageQueue
) =>
  new CronJob(
    "0 * * * * *", // cronTime
    async function () {
      const now = new Date();
      const offset = now.getTimezoneOffset();
      const nowWithTimezone = new Date(now.getTime() - offset * 60 * 1000);
      const day = nowWithTimezone.toISOString().split("T")[0];

      const finishedEvents = (await context.query.Event.findMany({
        where: {
          carePeriodCommitted: {
            careDays: {
              every: {
                day: {
                  lt: day,
                },
              },
            },
          },
          markedAsFinishedAt: {
            equals: null,
          },
        },
        query: `
          id          
        `,
      })) as { id: string }[];

      if (finishedEvents.length === 0) {
        return;
      }

      console.log(
        `Found ${finishedEvents.length} finished events older than ${day}`
      );

      for (let finishedEvent of finishedEvents) {
        await context.prisma.$transaction(async (transaction) => {
          await transaction.event.update({
            where: {
              id: finishedEvent.id,
            },
            data: {
              markedAsFinishedAt: now,
            },
          });
        });
      }

      await eventQueue.addBulk(
        finishedEvents.map((event) => ({
          name: "eventFinished",
          data: { id: event.id },
        }))
      );
    }, // onTick
    null, // onComplete
    true, // start
    "Europe/Berlin" // timeZone
  );
