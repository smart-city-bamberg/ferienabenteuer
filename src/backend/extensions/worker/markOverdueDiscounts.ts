import { Context } from ".keystone/types";
import { CronJob } from "cron";
import { oneHour } from "../schemaExtensions";

export const markOverdueDiscounts = (context: Context) =>
  new CronJob(
    "0 * * * * *", // cronTime
    async function () {
      const timestamp = new Date(Date.now() - 14 * 24 * oneHour);

      const overdueDiscounts = await context.prisma.discount.findMany({
        where: {
          status: {
            equals: "pending",
          },
          vouchers: {
            some: {
              redemptionDate: {
                lt: timestamp,
              },
              ticket: {
                eventTicket: {
                  type: {
                    equals: "participant",
                  },
                },
              },
            },
          },
        },
      });

      if (overdueDiscounts.length === 0) {
        return;
      }

      console.log(
        `Found ${overdueDiscounts.length} overdue discounts older than ${timestamp}`
      );

      for (let discount of overdueDiscounts) {
        await context.prisma.$transaction(async (transaction) => {
          await transaction.discount.update({
            where: {
              id: discount.id,
            },
            data: {
              status: "overdue",
            },
          });
        });
      }
    }, // onTick
    null, // onComplete
    true, // start
    "Europe/Berlin" // timeZone
  );
