import { Context } from ".keystone/types";
import { CronJob } from "cron";
import { IMessageQueue } from "../eventQueue";

const millisecondsPerDay = 60 * 60 * 24 * 1000;

export const deleteInactiveUsers = (
  context: Context,
  eventQueue: IMessageQueue
) =>
  new CronJob(
    "0 * * * * *", // cronTime
    async function () {
      const now = new Date(Date.now() - 7 * millisecondsPerDay);
      const offset = now.getTimezoneOffset();
      const nowWithTimezone = new Date(now.getTime() - offset * 60 * 1000);
      const day = nowWithTimezone.toISOString();

      const inactiveUsers = (await context.query.Custodian.findMany({
        where: {
          user: {
            markedForDeletionAt: {
              lt: day,
            },
            role: {
              equals: "custodian",
            },
          },
        },
        query: `
          id
          firstname
          surname
          salutation      
          user {
            id
            email  
          }
        `,
      })) as {
        id: string;
        firstname: string;
        surname: string;
        salutation: string;
        user: {
          id: string;
          email: string;
        };
      }[];

      if (inactiveUsers.length === 0) {
        return;
      }

      console.log(
        `Found ${inactiveUsers.length} users marked for deletion older than ${day}`
      );

      for (let inactiveUser of inactiveUsers) {
        await context.prisma.$transaction(async (transaction) => {
          await transaction.user.delete({
            where: {
              id: inactiveUser.user.id,
            },
          });
        });
      }

      await eventQueue.addBulk(
        inactiveUsers.map((custodian) => ({
          name: "userDeletedAfterMarkForDeletion",
          data: { custodian: custodian },
        }))
      );
    }, // onTick
    null, // onComplete
    true, // start
    "Europe/Berlin" // timeZone
  );
