import { sendEmailToAllCustodians } from "./mutations/sendEmailToAllCustodians";
import { availableTicketCount } from "./queries/availableTicketCount";
import { cancelTicket } from "./mutations/cancelTicket";
import { rejectDiscount } from "./mutations/rejectDiscount";
import { approveDiscount } from "./mutations/approveDiscount";
import { assignDiscountTypeToCartTicket } from "./mutations/assignDiscountTypeToCartTicket";
import { resendVerificationCode } from "./mutations/resendVerificationCode";
import { changePassword } from "./mutations/changePassword";
import { applyForDiscount } from "./mutations/applyForDiscount";
import { GraphQLError } from "graphql";
import { graphql } from "@keystone-6/core";
import { updateEventState } from "./mutations/updateEventState";
import { addTicketsToCart } from "./mutations/addTicketsToCart";
import { register } from "./mutations/register";
import { removeTicketsFromCart } from "./mutations/removeTicketsFromCart";
import { assignChildToCartTicket } from "./mutations/assignChildToCartTicket";
import { bookCartTickets } from "./mutations/bookCartTickets";
import { redeemEarlyAccessCode } from "./mutations/redeemEarlyAccessCode";
import { verifyAccount } from "./mutations/verifyAccount";
import { markChildDeleted } from "./mutations/deleteChild";
import { cancelDiscount } from "./mutations/cancelDiscount";
import { produceEventTickets } from "./mutations/produceEventTickets";
import { registerPayment } from "./mutations/registerPayment";
import { IMessageQueue } from "./eventQueue";
import { moveWaitinglistToParticipant } from "./mutations/moveWaitinglistToParticipant";
import { requestPasswordResetToken } from "./mutations/requestPasswordResetToken";
import { resetPassword } from "./mutations/resetPassword";
import { unblockEventTicket } from "./mutations/unblockEventTicket";
import { extendCartTerm } from "./mutations/extendCartTerm";
import { resendBookingConfirmation } from "./mutations/resendBookingConfirmation";
import { sendDiscountApplicationReminder } from "./mutations/sendDiscountApplicationReminder";
import { markBookingPaymentReminded } from "./mutations/markBookingPaymentReminded";
import { generateEarlyAccessCodes } from "./mutations/generateEarlyAccessCodes";
import { markEarlyAccessCodeNotified } from "./mutations/markEarlyAccessCodeNotified";
import { generateInvoice } from "./mutations/generateInvoice";
import { StorageConfig } from "@keystone-6/core/types";
import { deleteAccount } from "./mutations/deleteAccount";
import { duplicateEvent } from "./mutations/duplicateEvent";
import { deleteSeasonData } from "./mutations/deleteSeasonData";

export const accessDeniedError = (msg: string) =>
  new GraphQLError(`Access denied: ${msg}`, {
    extensions: { code: "KS_ACCESS_DENIED" },
  });

export const oneSecond = 1000;
export const oneMinute = 60 * oneSecond;
export const oneHour = 60 * oneMinute;

const discountLimitPerChild = parseInt(
  process.env["DISCOUNT_LIMIT_PER_CHILD"] ?? "0"
);

export const extendGraphqlSchema = (
  eventQueue: () => IMessageQueue,
  config: Record<string, StorageConfig>
) =>
  graphql.extend((base) => {
    return {
      query: {
        availableTicketCount,
      },
      mutation: {
        updateEventState: updateEventState(base, eventQueue),
        addTicketsToCart: addTicketsToCart(base, eventQueue),
        removeTicketsFromCart: removeTicketsFromCart(base),
        assignChildToCartTicket: assignChildToCartTicket(base),
        assignDiscountTypeToCartTicket: assignDiscountTypeToCartTicket(base),
        bookCartTickets: bookCartTickets(base, eventQueue),
        register: register(base, eventQueue),
        redeemEarlyAccessCode: redeemEarlyAccessCode(base),
        verifyAccount: verifyAccount(base, eventQueue),
        applyForDiscount: applyForDiscount(
          base,
          eventQueue,
          discountLimitPerChild
        ),
        approveDiscount: approveDiscount(base, eventQueue),
        rejectDiscount: rejectDiscount(base, eventQueue),
        changePassword: changePassword(base),
        requestPasswordResetToken: requestPasswordResetToken(base, eventQueue),
        resetPassword: resetPassword(base),
        resendVerificationCode: resendVerificationCode(base, eventQueue),
        markChildDeleted: markChildDeleted(base),
        cancelDiscount: cancelDiscount(base, eventQueue),
        produceEventTickets: produceEventTickets(base, eventQueue),
        generateEarlyAccessCodes: generateEarlyAccessCodes(base),
        registerPayment: registerPayment(base, eventQueue),
        resendBookingConfirmation: resendBookingConfirmation(base, eventQueue),
        cancelTicket: cancelTicket(base, eventQueue),
        unblockEventTicket: unblockEventTicket(base, eventQueue),
        moveWaitinglistToParticipant: moveWaitinglistToParticipant(
          base,
          eventQueue
        ),
        extendCartLifetime: extendCartTerm(base, eventQueue),
        sendDiscountApplicationReminder: sendDiscountApplicationReminder(
          base,
          eventQueue
        ),
        sendEmailToAllCustodians: sendEmailToAllCustodians(eventQueue),
        markBookingPaymentReminded: markBookingPaymentReminded(base),
        markEarlyAccessCodeNotified: markEarlyAccessCodeNotified(base),
        generateInvoice: generateInvoice(base, config["invoice_store"]),
        deleteAccount: deleteAccount(base, eventQueue),
        duplicateEvent: duplicateEvent(base, config["image_store"]),
        deleteSeasonData: deleteSeasonData(),
      },
    };
  });
