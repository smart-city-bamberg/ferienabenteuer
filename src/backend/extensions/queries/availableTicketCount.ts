import { Context } from ".keystone/types";
import { graphql } from "@keystone-6/core";
import {
  type CacheHint,
  maybeCacheControlFromInfo,
} from "@apollo/cache-control-types";

export const availableTicketCount = graphql.field({
  type: graphql.nonNull(
    graphql.list(
      graphql.object<{
        eventId: string;
        participant: number;
        waitinglist: number;
      }>()({
        name: "AvailableTicketCountResult",
        fields: {
          eventId: graphql.field({
            type: graphql.ID,
          }),
          participant: graphql.field({
            type: graphql.Int,
          }),
          waitinglist: graphql.field({
            type: graphql.Int,
          }),
        },
      })
    )
  ),
  args: {
    year: graphql.arg({
      type: graphql.nonNull(graphql.Int),
    }),
  },
  async resolve(source, { year }, context: Context, info) {
    maybeCacheControlFromInfo(info)?.setCacheHint({
      maxAge: 10,
      scope: "PUBLIC",
    });

    const events = await context.prisma.event.findMany({
      where: {
        status: {
          equals: "approved",
        },
        season: {
          year: {
            equals: year,
          },
        },
      },
    });

    const promises = events.map(async (event) => {
      const participant = await context.prisma.eventTicket.count({
        where: {
          eventId: {
            equals: event.id,
          },
          type: {
            equals: "participant",
          },
          bookedTicketId: {
            equals: null,
          },
          cartTicketId: {
            equals: null,
          },
          isBlocked: {
            equals: false,
          },
        },
      });

      const waitinglist = await context.prisma.eventTicket.count({
        where: {
          eventId: {
            equals: event.id,
          },
          type: {
            equals: "waitinglist",
          },
          bookedTicketId: {
            equals: null,
          },
          cartTicketId: {
            equals: null,
          },
          isBlocked: {
            equals: false,
          },
        },
      });

      return {
        eventId: event.id,
        participant,
        waitinglist,
      };
    });

    const results = await Promise.all(promises);
    return results;
  },
});
