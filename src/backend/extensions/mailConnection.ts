import nodemailer from "nodemailer";

export const buildMailConnection = () =>
  nodemailer.createTransport({
    host: process.env.MAILER_SMTP_HOST ?? "localhost",
    port: parseInt(process.env.MAILER_SMTP_PORT ?? "2525"),
    secure: false,
    auth: {
      user: process.env.MAILER_SMTP_USER ?? "user",
      pass: process.env.MAILER_SMTP_PASSWORD ?? "password",
    },
  });
