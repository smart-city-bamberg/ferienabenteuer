import { GraphQLError } from "graphql";
import { accessDeniedError } from "../schemaExtensions";
import { Context } from ".keystone/types";
import { graphql } from "@keystone-6/core";
import { IMessageQueue } from "../eventQueue";

export const deleteAccount = (
  base: graphql.BaseSchemaMeta,
  eventQueue: () => IMessageQueue
) =>
  graphql.field({
    type: graphql.nonNull(graphql.Boolean),
    async resolve(source, {}, context: Context) {
      const now = new Date();

      if (!context.session?.data.id) {
        throw accessDeniedError("You are not authenticated.");
      }

      const user = await context.prisma.user.findUnique({
        where: {
          id: context.session.data.id,
        },
      });

      if (!user) {
        throw new GraphQLError("The user does not exist.", {
          extensions: { code: "FA_USER_NOT_EXISTING" },
        });
      }

      const custodian = await context.prisma.custodian.findFirst({
        where: {
          userId: { equals: user.id },
        },
      });

      await context.prisma.user.delete({
        where: {
          id: user.id,
        },
      });

      await eventQueue().add("userDeletedAccount", { user, custodian });

      return true;
    },
  });
