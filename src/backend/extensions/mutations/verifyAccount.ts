import { GraphQLError } from "graphql";
import { Context } from ".keystone/types";
import { graphql } from "@keystone-6/core";
import { IMessageQueue } from "../eventQueue";

type VerifyAccountSuccess = {
  user: any;
};

type VerifyAccountFailed = {
  reason: string;
};

export const verifyAccount = (
  base: graphql.BaseSchemaMeta,
  eventQueue: () => IMessageQueue
) =>
  graphql.field({
    type: graphql.nonNull(
      graphql.union({
        name: "VerifyAccountResult",
        types: [
          graphql.object<VerifyAccountSuccess>()({
            name: "VerifyAccountSuccess",
            fields: {
              user: graphql.field({
                type: graphql.nonNull(base.object("User")),
              }),
            },
          }),
          graphql.object<VerifyAccountFailed>()({
            name: "VerifyAccountFailed",
            fields: {
              reason: graphql.field({
                type: graphql.nonNull(graphql.String),
              }),
            },
          }),
        ],
      })
    ),
    args: {
      data: graphql.arg({
        type: graphql.nonNull(
          graphql.inputObject({
            name: "VerifyAccountInput",
            fields: {
              token: graphql.arg({
                type: graphql.nonNull(graphql.String),
              }),
            },
          })
        ),
      }),
    },
    async resolve(source, { data }, context: Context) {
      const now = new Date();

      const userVerificationToken =
        await context.prisma.userVerificationToken.findUnique({
          where: {
            validationToken: data.token,
          },
        });

      if (
        !userVerificationToken ||
        userVerificationToken.validUntil.getTime() < now.getTime()
      ) {
        return {
          __typename: "VerifyAccountFailed",
          reason: "InvalidToken",
        };
      }

      const user = await context.prisma.user.findUnique({
        where: {
          id: userVerificationToken.userId as string,
        },
      });

      if (!user) {
        throw new GraphQLError("The user does not exist.", {
          extensions: { code: "FA_USER_NOT_EXISTING" },
        });
      }

      const transactionResult = await context.prisma.$transaction(
        async (transaction) => {
          await transaction.user.update({
            where: {
              id: user?.id,
            },
            data: {
              isEnabled: true,
            },
          });

          await transaction.userVerificationToken.delete({
            where: {
              id: userVerificationToken.id,
            },
          });

          return {
            __typename: "VerifyAccountSuccess",
            user: user,
          };
        }
      );

      if (transactionResult.__typename === "VerifyAccountSuccess") {
        await eventQueue().add("userVerifiedAccount", transactionResult.user);
      }

      return transactionResult;
    },
  });
