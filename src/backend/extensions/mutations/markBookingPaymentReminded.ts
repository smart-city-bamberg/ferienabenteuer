import { GraphQLError } from "graphql";
import { Context } from ".keystone/types";
import { graphql } from "@keystone-6/core";
import { accessDeniedError } from "../schemaExtensions";
import { isAdminOrRoleAdminOrRoleOrganizer } from "../../auth.filters";

export const markBookingPaymentReminded = (base: graphql.BaseSchemaMeta) =>
  graphql.field({
    type: graphql.nonNull(base.object("Booking")),
    args: {
      bookingId: graphql.arg({ type: graphql.nonNull(graphql.ID) }),
    },
    async resolve(source, { bookingId }, context: Context) {
      if (!isAdminOrRoleAdminOrRoleOrganizer(context)) {
        throw accessDeniedError("You are not authenticated.");
      }

      const booking = await context.db.Booking.findOne({
        where: {
          id: bookingId,
        },
      });

      if (!booking) {
        throw new GraphQLError(`Booking '${bookingId}' does not exist..`, {
          extensions: { code: "FA_BOOKING_NOT_EXISTING" },
        });
      }

      const now = new Date();

      const updatedBooking = await context.db.Booking.updateOne({
        where: {
          id: bookingId,
        },
        data: {
          reminderDate: now,
        },
      });

      return updatedBooking;
    },
  });
