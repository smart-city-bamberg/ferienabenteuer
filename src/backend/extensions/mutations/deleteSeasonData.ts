import { GraphQLError } from "graphql";
import { accessDeniedError } from "../schemaExtensions";
import { Context } from ".keystone/types";
import { graphql } from "@keystone-6/core";
import { isAdminOrRoleAdmin } from "../../auth.filters";

export const deleteSeasonData = () =>
  graphql.field({
    type: graphql.nonNull(graphql.Boolean),
    args: {
      seasonId: graphql.arg({
        type: graphql.nonNull(graphql.ID),
      }),
    },
    async resolve(source, { seasonId }, context: Context) {
      if (!isAdminOrRoleAdmin(context)) {
        throw accessDeniedError("You are not allowed to delete a season.");
      }

      const season = await context.prisma.season.findUnique({
        where: { id: seasonId },
      });

      if (!season) {
        throw new GraphQLError("The season does not exist.", {
          extensions: { code: "FA_SEASON_NOT_EXISTING" },
        });
      }

      await context.prisma.$transaction(async (transaction) => {
        await transaction.discountVoucher.deleteMany({
          where: {
            discount: {
              seasonId: seasonId,
            },
          },
        });

        await transaction.discount.deleteMany({
          where: {
            seasonId: seasonId,
          },
        });

        await transaction.ticket.deleteMany({
          where: {
            OR: [
              {
                booking: {
                  event: {
                    seasonId: seasonId,
                  },
                },
              },
              {
                reservation: {
                  event: {
                    seasonId: seasonId,
                  },
                },
              },
            ],
          },
        });

        await transaction.eventTicket.deleteMany({
          where: {
            event: {
              seasonId: seasonId,
            },
          },
        });

        await transaction.booking.deleteMany({
          where: {
            event: {
              seasonId: seasonId,
            },
          },
        });

        await transaction.reservation.deleteMany({
          where: {
            event: {
              seasonId: seasonId,
            },
          },
        });

        await transaction.careDay.deleteMany({
          where: {
            period: {
              period: {
                seasonId: seasonId,
              },
            },
          },
        });

        await transaction.carePeriod.deleteMany({
          where: {
            period: {
              seasonId: seasonId,
            },
          },
        });

        await transaction.period.deleteMany({
          where: {
            seasonId: seasonId,
          },
        });

        await transaction.invoice.deleteMany({
          where: {
            event: {
              seasonId: seasonId,
            },
          },
        });

        await transaction.earlyAccessCodeVoucher.deleteMany({
          where: {
            seasonId: seasonId,
          },
        });

        await transaction.earlyAccessCode.deleteMany({
          where: {
            seasonId: seasonId,
          },
        });

        await transaction.event.deleteMany({
          where: {
            seasonId: seasonId,
          },
        });
      });

      return true;
    },
  });
