import { GraphQLError } from "graphql";
import { Context } from ".keystone/types";
import { graphql } from "@keystone-6/core";
import { accessDeniedError } from "../schemaExtensions";
import { isAdminOrRoleAdmin } from "../../auth.filters";
import { IMessageQueue } from "../eventQueue";

export const sendDiscountApplicationReminder = (
  base: graphql.BaseSchemaMeta,
  eventQueue: () => IMessageQueue
) =>
  graphql.field({
    type: graphql.nonNull(base.object("Discount")),
    args: {
      discountId: graphql.arg({ type: graphql.nonNull(graphql.ID) }),
    },
    async resolve(source, { discountId }, context: Context) {
      if (!isAdminOrRoleAdmin(context)) {
        throw accessDeniedError("You are not authenticated.");
      }

      const discount = await context.db.Discount.findOne({
        where: {
          id: discountId,
        },
      });

      if (!discount) {
        throw new GraphQLError(`Discount '${discountId}' does not exist..`, {
          extensions: { code: "FA_DISCOUNT_NOT_EXISTING" },
        });
      }

      const now = new Date();

      const updatedDiscount = await context.db.Discount.updateOne({
        where: {
          id: discountId,
        },
        data: {
          reminderDate: now,
        },
      });

      await eventQueue().add(
        "discountApplicationReminderRequested",
        updatedDiscount
      );

      return updatedDiscount;
    },
  });
