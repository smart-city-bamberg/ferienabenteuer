import { Context } from ".keystone/types";
import { graphql } from "@keystone-6/core";
import { accessDeniedError } from "../schemaExtensions";

type AssignChildToCartTicketSuccess = {
  cartTicket: any;
};
type AssignChildToCartTicketFailed = {
  reason: string;
};

export const assignChildToCartTicket = (base: graphql.BaseSchemaMeta) =>
  graphql.field({
    type: graphql.nonNull(
      graphql.union({
        name: "AssignChildToCartTicketResult",
        types: [
          graphql.object<AssignChildToCartTicketSuccess>()({
            name: "AssignChildToCartTicketSuccess",
            fields: {
              cartTicket: graphql.field({
                type: graphql.nonNull(base.object("CartTicket")),
              }),
            },
          }),
          graphql.object<AssignChildToCartTicketFailed>()({
            name: "AssignChildToCartTicketFailed",
            fields: {
              reason: graphql.field({
                type: graphql.nonNull(graphql.String),
              }),
            },
          }),
        ],
      })
    ),
    args: {
      cartTicketId: graphql.arg({ type: graphql.nonNull(graphql.ID) }),
      childId: graphql.arg({ type: graphql.ID }),
    },
    async resolve(source, { cartTicketId, childId }, context: Context) {
      if (!context.session?.data.id) {
        throw accessDeniedError("You are not authenticated.");
      }

      const now = new Date();

      const custodian = await context.prisma.custodian.findFirst({
        where: {
          userId: {
            equals: context.session?.data.id,
          },
        },
      });

      if (!custodian) {
        throw accessDeniedError("You cannot assign a child to ticket.");
      }

      if (childId !== null) {
        const child = await context.prisma.child.findFirst({
          where: {
            id: childId,
            custodianId: { equals: custodian.id },
          },
        });

        if (!child) {
          throw accessDeniedError(`The child ${childId} cannot be assigned.`);
        }
      }

      type CartTicket = {
        id: string;
        cart: {
          id: string;
        };
        eventTicket: {
          id: string;
          type: string;
          event: {
            id: string;
          };
        };
        discountType: {
          id: string;
          discountPercent: number;
        } | null;
        child: {
          id: string;
        };
      };

      const cartTickets = (await context.query.CartTicket.findMany({
        where: {
          id: {
            equals: cartTicketId,
          },
          cart: {
            custodian: { id: { equals: custodian.id } },
          },
        },
        query:
          "id cart { id } eventTicket { id type event { id } } discountType { id discountPercent } child{ id }",
      })) as CartTicket[];

      if (cartTickets.length !== 1) {
        throw accessDeniedError(`The ticket is not in your cart.`);
      }

      const cartTicket = cartTickets[0];

      if (childId !== null) {
        const cartTicketsWithSameEventAndChild =
          await context.query.CartTicket.findMany({
            where: {
              child: {
                id: {
                  equals: childId,
                },
              },
              cart: {
                custodian: { id: { equals: custodian.id } },
              },
              eventTicket: {
                event: {
                  id: {
                    equals: cartTicket.eventTicket.event.id,
                  },
                },
              },
            },
          });

        if (cartTicketsWithSameEventAndChild.length > 0) {
          return {
            __typename: "AssignChildToCartTicketFailed",
            reason: "ChildAlreadyAssignedForEvent",
          };
        }

        const ticketsWithSameEventAndChild =
          await context.query.Ticket.findMany({
            where: {
              child: {
                id: {
                  equals: childId,
                },
                custodian: { id: { equals: custodian.id } },
              },
              eventTicket: {
                event: {
                  id: {
                    equals: cartTicket.eventTicket.event.id,
                  },
                },
              },
            },
          });

        if (ticketsWithSameEventAndChild.length > 0) {
          return {
            __typename: "AssignChildToCartTicketFailed",
            reason: "ChildAlreadyBookedForEvent",
          };
        }
      }

      const updatedCartTicket = await context.prisma.cartTicket.update({
        where: {
          id: cartTicketId,
        },
        data: {
          childId: childId,
        },
      });

      return {
        __typename: "AssignChildToCartTicketSuccess",
        cartTicket: updatedCartTicket,
      };
    },
  });
