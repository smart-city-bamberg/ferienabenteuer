import { GraphQLError } from "graphql";
import { Context } from ".keystone/types";
import { graphql } from "@keystone-6/core";
import { accessDeniedError } from "../schemaExtensions";
import { IMessageQueue } from "../eventQueue";
import {
  isAdminOrRoleAdminOrRoleOrganizer,
  isRoleOrganizer,
} from "../../auth.filters";
import { generateCode } from "../../codeGenerator";

type MoveWaitinglistToParticipantSuccess = {
  eventTickets: any[];
};

type MoveWaitinglistToParticipantFailed = {
  reason: string;
};

export const moveWaitinglistToParticipant = (
  base: graphql.BaseSchemaMeta,
  eventQueue: () => IMessageQueue
) =>
  graphql.field({
    type: graphql.nonNull(
      graphql.union({
        name: "MoveWaitinglistToParticipantResult",
        types: [
          graphql.object<MoveWaitinglistToParticipantSuccess>()({
            name: "MoveWaitinglistToParticipantSuccess",
            fields: {
              eventTickets: graphql.field({
                type: graphql.nonNull(
                  graphql.list(graphql.nonNull(base.object("EventTicket")))
                ),
              }),
            },
          }),
          graphql.object<MoveWaitinglistToParticipantFailed>()({
            name: "MoveWaitinglistToParticipantFailed",
            fields: {
              reason: graphql.field({
                type: graphql.nonNull(graphql.String),
              }),
            },
          }),
        ],
      })
    ),
    args: {
      ticketId: graphql.arg({ type: graphql.nonNull(graphql.ID) }),
    },
    async resolve(source, { ticketId }, context: Context) {
      if (!isAdminOrRoleAdminOrRoleOrganizer(context)) {
        throw accessDeniedError("You are not authenticated.");
      }

      const ticket = (await context.query.Ticket.findOne({
        where: {
          id: ticketId,
        },
        query: `
          id
          eventTicket {
            id
            version
            event {
              id
              organizer {
                id
              }
            }
          }
          child {
            custodian {
              id
            }
          }
          reservation {
            id
          }
          discountVoucher {
            id
            discount {
              status
            }
          }
        `,
      })) as {
        id: string;
        eventTicket: {
          id: string;
          version: number;
          event: {
            id: string;
            organizer: {
              id: string;
            };
          };
        };
        reservation: {
          id: string;
        };
        child: {
          custodian: {
            id: string;
          };
        };
        discountVoucher: {
          id: string;
          discount: {
            status: string;
          };
        };
      } | null;

      if (!ticket) {
        throw new GraphQLError(`Ticket '${ticketId}' does not exist..`, {
          extensions: { code: "FA_TICKET_NOT_EXISTING" },
        });
      }

      const isOrganizerEvent =
        ticket.eventTicket.event.organizer.id ===
        context.session.data.organizer?.id;

      if (isRoleOrganizer(context) && !isOrganizerEvent) {
        throw accessDeniedError("You are not authorized.");
      }

      const availableEventTicket = await context.prisma.eventTicket.findFirst({
        where: {
          eventId: {
            equals: ticket.eventTicket.event.id,
          },
          cartTicket: {
            is: null,
          },
          bookedTicket: {
            is: null,
          },
          type: {
            equals: "participant",
          },
        },
        orderBy: [
          {
            type: "asc",
          },
          {
            isBlocked: "desc",
          },
          { number: "asc" },
        ],
      });

      if (!availableEventTicket) {
        return {
          __typename: "MoveWaitinglistToParticipantFailed",
          reason: "NoBlockedTicketAvailable",
        };
      }

      const now = new Date();

      const result = await context.prisma.$transaction(async (transaction) => {
        const discountVoucher = ticket.discountVoucher;
        const discount = discountVoucher?.discount;
        const discountStatus = discount?.status;

        if (discountVoucher) {
          await transaction.discountVoucher.update({
            where: {
              id: discountVoucher.id,
            },
            data: {
              redemptionDate: now,
            },
          });
        }

        const createBooking = !discountStatus || discountStatus === "approved";

        let reservation: {
          id: string;
          code: string;
          eventId: string | null;
          custodianId: string | null;
          date: Date | null;
          isDeleted: boolean;
        } | null = null;

        let booking: {
          id: string;
          code: string;
          status: string | null;
          eventId: string | null;
          custodianId: string | null;
          date: Date | null;
          reminderDate: Date | null;
          isDeleted: boolean;
        } | null = null;

        if (createBooking) {
          booking = await transaction.booking.create({
            data: {
              custodianId: ticket.child.custodian.id,
              eventId: ticket.eventTicket.event.id,
              status: "pending",
              date: now,
              code: generateCode(),
            },
          });

          await transaction.ticket.update({
            where: {
              id: ticket.id,
            },
            data: {
              bookingId: booking.id,
              reservationId: null,
            },
          });
        } else {
          reservation = await transaction.reservation.create({
            data: {
              custodianId: ticket.child.custodian.id,
              eventId: ticket.eventTicket.event.id,
              date: now,
              code: generateCode(),
            },
          });

          await transaction.ticket.update({
            where: {
              id: ticket.id,
            },
            data: {
              reservationId: reservation.id,
            },
          });
        }

        await transaction.eventTicket.update({
          where: {
            id: ticket.eventTicket.id,
            version: ticket.eventTicket.version,
          },
          data: {
            bookedTicketId: null,
            version: {
              increment: 1,
            },
          },
        });

        await transaction.eventTicket.update({
          where: {
            id: availableEventTicket.id,
            version: availableEventTicket.version,
          },
          data: {
            bookedTicketId: ticket.id,
            isBlocked: false,
            version: {
              increment: 1,
            },
          },
        });

        const reservationTicketsCount = await transaction.ticket.count({
          where: {
            reservationId: ticket.reservation.id,
          },
        });

        if (reservationTicketsCount === 0) {
          await transaction.reservation.delete({
            where: {
              id: ticket.reservation.id,
            },
          });
        }

        const waitinglistEventTickets = await transaction.eventTicket.findMany({
          where: {
            eventId: {
              equals: ticket.eventTicket.event.id,
            },
            type: {
              equals: "waitinglist",
            },
          },
          orderBy: {
            number: "asc",
          },
        });

        const movedEventTickets = [];
        const sortedWaitinglistEventTickets = waitinglistEventTickets.sort(
          (a, b) => {
            if (!a.bookedTicketId) {
              if (!b.bookedTicketId) {
                return 0;
              }

              return 1;
            }

            if (!b.bookedTicketId) {
              return -1;
            }

            return a.number - b.number;
          }
        );

        for (const [
          index,
          waitinglistEventTicket,
        ] of sortedWaitinglistEventTickets.entries()) {
          const movedIndex = index + 1;

          if (waitinglistEventTicket.number === movedIndex) {
            continue;
          }

          const movedEventTicket = await transaction.eventTicket.update({
            where: {
              id: waitinglistEventTicket.id,
              version: waitinglistEventTicket.version,
            },
            data: {
              number: movedIndex,
              version: {
                increment: 1,
              },
            },
          });

          if (movedEventTicket.bookedTicketId) {
            movedEventTickets.push(movedEventTicket);
          }
        }

        return {
          movedEventTickets,
          booking,
          reservation,
        };
      });

      if (result.booking) {
        eventQueue().add("waitinglistTicketConvertedToBooking", result.booking);
      }

      if (result.reservation) {
        eventQueue().add(
          "waitinglistTicketConvertedToParticipant",
          result.reservation
        );
      }

      eventQueue().addBulk(
        result.movedEventTickets.map((eventTicket) => ({
          name: "waitinglistTicketSlotAdvanced",
          data: eventTicket,
        }))
      );

      const eventTickets = await context.prisma.eventTicket.findMany({
        where: {
          eventId: {
            equals: ticket.eventTicket.event.id,
          },
        },
      });

      return {
        __typename: "MoveWaitinglistToParticipantSuccess",
        eventTickets,
      };
    },
  });
