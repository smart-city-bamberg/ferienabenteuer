import { GraphQLError } from "graphql";
import { Context } from ".keystone/types";
import { graphql } from "@keystone-6/core";
import { accessDeniedError } from "../schemaExtensions";

export const assignDiscountTypeToCartTicket = (base: graphql.BaseSchemaMeta) =>
  graphql.field({
    type: graphql.nonNull(base.object("CartTicket")),
    args: {
      cartTicketId: graphql.arg({ type: graphql.nonNull(graphql.ID) }),
      discountTypeId: graphql.arg({ type: graphql.ID }),
    },
    async resolve(source, { cartTicketId, discountTypeId }, context: Context) {
      if (!context.session?.data.id) {
        throw accessDeniedError("You are not authenticated.");
      }

      const now = new Date();

      const custodian = await context.prisma.custodian.findFirst({
        where: {
          userId: {
            equals: context.session?.data.id,
          },
        },
      });

      if (!custodian) {
        throw accessDeniedError("You cannot assign a child to ticket.");
      }

      if (discountTypeId !== null) {
        const discountType = await context.prisma.discountType.findFirst({
          where: {
            id: discountTypeId,
          },
        });

        if (!discountType) {
          throw new GraphQLError(
            `The discount type ${discountTypeId} does not exist.`,
            {
              extensions: { code: "FA_DISCOUNT_TYPE_NOT_EXISTING" },
            }
          );
        }
      }

      const cartTicket = await context.prisma.cartTicket.findFirst({
        where: {
          id: cartTicketId,
          cart: {
            custodianId: { equals: custodian.id },
          },
        },
      });

      if (!cartTicket) {
        throw accessDeniedError(`The ticket is not in your cart.`);
      }

      const updatedCartTicket = await context.prisma.cartTicket.update({
        where: {
          id: cartTicketId,
        },
        data: {
          discountTypeId,
          childId: null,
        },
      });

      return updatedCartTicket;
    },
  });
