import { oneHour, oneMinute } from "./../schemaExtensions";
import bcryptjs from "bcryptjs";
import { Context } from ".keystone/types";
import { graphql } from "@keystone-6/core";
import crypto from "crypto";
import { IMessageQueue } from "../eventQueue";

type RegisterSuccess = {
  user: any;
};

type RegisterFailed = {
  reason: string;
};

export const register = (
  base: graphql.BaseSchemaMeta,
  eventQueue: () => IMessageQueue
) =>
  graphql.field({
    type: graphql.nonNull(
      graphql.union({
        name: "RegisterResult",
        types: [
          graphql.object<RegisterSuccess>()({
            name: "RegisterSuccess",
            fields: {
              user: graphql.field({
                type: graphql.nonNull(base.object("User")),
              }),
            },
          }),
          graphql.object<RegisterFailed>()({
            name: "RegisterFailed",
            fields: {
              reason: graphql.field({
                type: graphql.nonNull(graphql.String),
              }),
            },
          }),
        ],
      })
    ),
    args: {
      data: graphql.arg({
        type: graphql.nonNull(
          graphql.inputObject({
            name: "RegisterInput",
            fields: {
              email: graphql.arg({
                type: graphql.nonNull(graphql.String),
              }),
              password: graphql.arg({
                type: graphql.nonNull(graphql.String),
              }),
              salutation: graphql.arg({
                type: graphql.nonNull(graphql.String),
              }),
              firstname: graphql.arg({
                type: graphql.nonNull(graphql.String),
              }),
              surname: graphql.arg({
                type: graphql.nonNull(graphql.String),
              }),
              addressCity: graphql.arg({
                type: graphql.nonNull(graphql.String),
              }),
              addressZip: graphql.arg({
                type: graphql.nonNull(graphql.String),
              }),
              addressStreet: graphql.arg({
                type: graphql.nonNull(graphql.String),
              }),
              addressHouseNumber: graphql.arg({
                type: graphql.nonNull(graphql.String),
              }),
              addressAdditional: graphql.arg({
                type: graphql.nonNull(graphql.String),
              }),
              phone: graphql.arg({
                type: graphql.nonNull(graphql.String),
              }),
              emergencyPhone: graphql.arg({
                type: graphql.nonNull(graphql.String),
              }),
              agreeDataProtection: graphql.arg({
                type: graphql.nonNull(graphql.Boolean),
              }),
            },
          })
        ),
      }),
    },
    async resolve(source, { data }, context: Context) {
      const now = new Date();

      if (!data.agreeDataProtection) {
        return {
          __typename: "RegisterFailed",
          reason: "MustAgreeDataProtection",
        };
      }

      const existingUser = await context.prisma.user.findUnique({
        where: {
          email: data.email,
        },
      });

      if (existingUser) {
        return {
          __typename: "RegisterFailed",
          reason: "UserEmailExists",
        };
      }

      const transactionResult = await context.prisma.$transaction(
        async (transaction) => {
          const hashedPassword = await bcryptjs.hash(data.password, 10);

          const createdUser = await transaction.user.create({
            data: {
              email: data.email,
              password: hashedPassword,
              name: data.email,
              createdAt: now,
              role: "custodian",
            },
          });

          await transaction.custodian.create({
            data: {
              salutation: data.salutation,
              firstname: data.firstname,
              surname: data.surname,
              addressCity: data.addressCity,
              addressZip: data.addressZip,
              addressStreet: data.addressStreet,
              addressHouseNumber: data.addressHouseNumber,
              addressAdditional: data.addressAdditional,
              phone: data.phone,
              emergencyPhone: data.emergencyPhone,
              userId: createdUser.id,
            },
          });

          await transaction.userVerificationToken.create({
            data: {
              userId: createdUser.id,
              createdAt: now,
              validUntil: new Date(now.getTime() + 24 * oneHour),
              validationToken: crypto
                .randomBytes(20)
                .toString("hex")
                .toUpperCase(),
            },
          });

          return {
            __typename: "RegisterSuccess",
            user: createdUser,
          };
        }
      );

      if (transactionResult.__typename === "RegisterSuccess") {
        await eventQueue().add("userRegistered", transactionResult.user);
      }

      return transactionResult;
    },
  });
