import { GraphQLError } from "graphql";
import { Context } from ".keystone/types";
import { graphql } from "@keystone-6/core";
import { accessDeniedError } from "../schemaExtensions";

type RedeemEarlyAccessCodeSuccess = {
  earlyAccessCodeVoucher: any;
};

type RedeemEarlyAccessCodeFailed = {
  reason: string;
};

export const redeemEarlyAccessCode = (base: graphql.BaseSchemaMeta) =>
  graphql.field({
    type: graphql.nonNull(
      graphql.union({
        name: "RedeemEarlyAccessCodeResult",
        types: [
          graphql.object<RedeemEarlyAccessCodeSuccess>()({
            name: "RedeemEarlyAccessCodeSuccess",
            fields: {
              earlyAccessCodeVoucher: graphql.field({
                type: graphql.nonNull(base.object("EarlyAccessCodeVoucher")),
              }),
            },
          }),
          graphql.object<RedeemEarlyAccessCodeFailed>()({
            name: "RedeemEarlyAccessCodeFailed",
            fields: {
              reason: graphql.field({
                type: graphql.nonNull(graphql.String),
              }),
            },
          }),
        ],
      })
    ),
    args: {
      year: graphql.arg({ type: graphql.nonNull(graphql.Int) }),
      code: graphql.arg({ type: graphql.nonNull(graphql.String) }),
    },
    async resolve(source, { year, code }, context: Context) {
      if (!context.session?.data.id) {
        throw accessDeniedError("You are not authenticated.");
      }

      const now = new Date();

      const custodian = await context.prisma.custodian.findFirst({
        where: {
          userId: {
            equals: context.session?.data.id,
          },
        },
      });

      if (!custodian) {
        throw accessDeniedError("You cannot redeem this early access code.");
      }

      const season = await context.prisma.season.findFirst({
        where: {
          year: {
            equals: year,
          },
        },
      });

      if (!season) {
        throw new GraphQLError(`The season ${year} does not exist.`, {
          extensions: { code: "FA_EAC_INVALID_SEASON" },
        });
      }

      const earlyAccessCode = await context.prisma.earlyAccessCode.findFirst({
        where: {
          code: {
            equals: code,
          },
          seasonId: {
            equals: season.id,
          },
        },
      });

      if (!earlyAccessCode) {
        return {
          __typename: "RedeemEarlyAccessCodeFailed",
          reason: "CodeInvalid",
        };
      }

      const existingEarlyAccessCodeVoucher =
        await context.prisma.earlyAccessCodeVoucher.findFirst({
          where: {
            code: {
              equals: earlyAccessCode.code,
            },
            seasonId: {
              equals: season.id,
            },
            custodianId: {
              equals: custodian.id,
            },
          },
        });

      if (existingEarlyAccessCodeVoucher) {
        return {
          __typename: "RedeemEarlyAccessCodeFailed",
          reason: "CodeAlreadyRedeemed",
        };
      }

      const earlyAccessCodeVoucher =
        await context.prisma.earlyAccessCodeVoucher.create({
          data: {
            code: earlyAccessCode.code,
            custodianId: custodian.id,
            seasonId: season.id,
          },
        });

      return {
        __typename: "RedeemEarlyAccessCodeSuccess",
        earlyAccessCodeVoucher,
      };
    },
  });
