import { GraphQLError } from "graphql";
import { Context } from ".keystone/types";
import { graphql } from "@keystone-6/core";
import { accessDeniedError } from "../schemaExtensions";
import { IMessageQueue } from "../eventQueue";

type CancelDiscountSuccess = {
  discount: any;
};

type CancelDiscountFailed = {
  reason: string;
};

export const cancelDiscount = (
  base: graphql.BaseSchemaMeta,
  eventQueue: () => IMessageQueue
) =>
  graphql.field({
    type: graphql.nonNull(
      graphql.union({
        name: "CancelDiscountResult",
        types: [
          graphql.object<CancelDiscountSuccess>()({
            name: "CancelDiscountSuccess",
            fields: {
              discount: graphql.field({
                type: graphql.nonNull(base.object("Discount")),
              }),
            },
          }),
          graphql.object<CancelDiscountFailed>()({
            name: "CancelDiscountFailed",
            fields: {
              reason: graphql.field({
                type: graphql.nonNull(graphql.String),
              }),
            },
          }),
        ],
      })
    ),
    args: {
      discountId: graphql.arg({ type: graphql.nonNull(graphql.ID) }),
    },
    async resolve(source, { discountId }, context: Context) {
      if (!context.session?.data.id) {
        throw accessDeniedError("You are not authenticated.");
      }

      const custodian = await context.prisma.custodian.findFirst({
        where: {
          userId: {
            equals: context.session?.data.id,
          },
        },
      });

      if (!custodian || !context.session?.data.id) {
        throw accessDeniedError("You cannot cancel a discount.");
      }

      const result = await context.prisma.$transaction(async (transaction) => {
        const discount = await transaction.discount.findFirst({
          where: {
            id: discountId,
          },
        });

        if (!discount) {
          throw new GraphQLError(`Discount '${discountId}' does not exist..`, {
            extensions: { code: "FA_DISCOUNT_NOT_EXISTING" },
          });
        }

        if (discount.status !== "pending") {
          return {
            __typename: "CancelDiscountFailed",
            reason: "CannotCancelApprovedDiscount",
          };
        }

        const updatedDiscount = await transaction.discount.update({
          where: {
            id: discountId,
          },
          data: {
            status: "cancelled",
          },
        });

        await transaction.discountVoucher.deleteMany({
          where: {
            discountId: {
              equals: discountId,
            },
          },
        });

        return {
          __typename: "CancelDiscountSuccess",
          discount: updatedDiscount,
        };
      });

      return result;
    },
  });
