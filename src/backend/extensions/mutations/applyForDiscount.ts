import { GraphQLError } from "graphql";
import { Context } from ".keystone/types";
import { graphql } from "@keystone-6/core";
import { accessDeniedError } from "../schemaExtensions";
import { IMessageQueue } from "../eventQueue";

type ApplyForDiscountSuccess = {
  discount: any;
};

type ApplyForDiscountFailed = {
  reason: string;
};

export const applyForDiscount = (
  base: graphql.BaseSchemaMeta,
  eventQueue: () => IMessageQueue,
  discountLimitPerChild: number
) =>
  graphql.field({
    type: graphql.nonNull(
      graphql.union({
        name: "ApplyForDiscountResult",
        types: [
          graphql.object<ApplyForDiscountSuccess>()({
            name: "ApplyForDiscountSuccess",
            fields: {
              discount: graphql.field({
                type: graphql.nonNull(base.object("Discount")),
              }),
            },
          }),
          graphql.object<ApplyForDiscountFailed>()({
            name: "ApplyForDiscountFailed",
            fields: {
              reason: graphql.field({
                type: graphql.nonNull(graphql.String),
              }),
            },
          }),
        ],
      })
    ),
    args: {
      childId: graphql.arg({ type: graphql.nonNull(graphql.ID) }),
      discountTypeId: graphql.arg({ type: graphql.nonNull(graphql.ID) }),
      year: graphql.arg({ type: graphql.nonNull(graphql.Int) }),
    },
    async resolve(source, { childId, discountTypeId, year }, context: Context) {
      if (!context.session?.data.id) {
        throw accessDeniedError("You are not authenticated.");
      }

      const now = new Date();

      const custodian = await context.prisma.custodian.findFirst({
        where: {
          userId: {
            equals: context.session?.data.id,
          },
        },
      });

      if (!custodian || !context.session?.data.id) {
        throw accessDeniedError("You cannot apply for a discount.");
      }

      const result = await context.prisma.$transaction(async (transaction) => {
        const discountType = await transaction.discountType.findUnique({
          where: {
            id: discountTypeId,
          },
        });

        if (!discountType) {
          throw new GraphQLError(
            `Discount type '${discountTypeId}' does not exist..`,
            {
              extensions: { code: "FA_DISCOUNT_TYPE_INVALID" },
            }
          );
        }

        const season = await transaction.season.findUnique({
          where: {
            year,
          },
        });

        if (!season) {
          throw new GraphQLError(`Season '${year}' does not exist..`, {
            extensions: { code: "FA_SEASON_INVALID" },
          });
        }

        const child = await transaction.child.findUnique({
          where: {
            id: childId,
            custodianId: custodian.id,
          },
        });

        if (!child) {
          throw new GraphQLError(`Child '${childId}' does not exist..`, {
            extensions: { code: "FA_CHILD_NOT_EXISTING" },
          });
        }

        const existingDiscountsForChild =
          await context.prisma.discount.findMany({
            where: {
              custodianId: custodian.id,
              status: {
                not: "cancelled",
              },
              seasonId: season.id,
            },
          });

        const amountPerChildLimited = discountLimitPerChild > 0;

        const amountPerChildExceeded =
          existingDiscountsForChild.length >= discountLimitPerChild;

        if (amountPerChildLimited && amountPerChildExceeded) {
          return {
            __typename: "ApplyForDiscountFailed",
            reason: "AlreadyAppliedMaximumDiscountsForChild",
          };
        }

        const existingDiscounts = await context.prisma.discount.findMany({
          where: {
            custodianId: custodian.id,
            typeId: discountTypeId,
            status: {
              not: "cancelled",
            },
            seasonId: season.id,
          },
        });

        const amountLimited = discountType.amountChildren > 0;

        const amountExceeded =
          existingDiscounts.length >= discountType.amountChildren;

        if (amountLimited && amountExceeded) {
          return {
            __typename: "ApplyForDiscountFailed",
            reason: "AlreadyAppliedForMaximumChildren",
          };
        }

        if (existingDiscounts.some((discount) => discount.childId == childId)) {
          return {
            __typename: "ApplyForDiscountFailed",
            reason: "AlreadyAppliedForChild",
          };
        }

        const discount = await transaction.discount.create({
          data: {
            childId: childId,
            typeId: discountTypeId,
            submissionDate: now,
            custodianId: custodian.id,
            seasonId: season.id,
            vouchers: {
              createMany: {
                data: Array.from({ length: discountType.amountEvents }).map(
                  (index) => ({})
                ),
              },
            },
          },
        });

        return {
          __typename: "ApplyForDiscountSuccess",
          discount,
        };
      });

      if (result.__typename === "ApplyForDiscountSuccess") {
        await eventQueue().add("discountApplicationReceived", result.discount);
      }

      return result;
    },
  });
