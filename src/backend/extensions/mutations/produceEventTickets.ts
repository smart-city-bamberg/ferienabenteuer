import { Context } from ".keystone/types";
import { graphql } from "@keystone-6/core";
import { accessDeniedError } from "../schemaExtensions";
import { isRoleAdmin } from "../../auth.filters";
import { generateCode } from "../../codeGenerator";
import { IMessageQueue } from "../eventQueue";

export const produceEventTickets = (
  base: graphql.BaseSchemaMeta,
  eventQueue: () => IMessageQueue
) =>
  graphql.field({
    type: graphql.nonNull(graphql.Int),
    async resolve(source, {}, context: Context) {
      if (!context.session?.data.id) {
        throw accessDeniedError("You are not authenticated.");
      }

      if (isRoleAdmin(context.session)) {
        throw accessDeniedError("You cannot produce tickets.");
      }

      const now = new Date();

      const approvedEvents = await context.prisma.event.findMany({
        where: {
          status: {
            equals: "approved",
          },
        },
      });

      return await context.prisma.$transaction(async (transaction) => {
        let allResults = 0;

        for (const approvedEvent of approvedEvents) {
          const createdEventTicketsParticipants =
            await transaction.eventTicket.createMany({
              data: Array.from({
                length: approvedEvent.participantLimit as number,
              }).map((_, index) => ({
                number: index + 1,
                eventId: approvedEvent.id,
                type: "participant",
                code: generateCode(),
              })),
            });

          allResults += createdEventTicketsParticipants.count;

          const createdEventTicketsWaitinglist =
            await transaction.eventTicket.createMany({
              data: Array.from({
                length: approvedEvent.waitingListLimit as number,
              }).map((_, index) => ({
                number: index + 1,
                eventId: approvedEvent.id,
                type: "waitinglist",
                code: generateCode(),
              })),
            });

          allResults += createdEventTicketsWaitinglist.count;
        }

        return allResults;
      });
    },
  });
