import { GraphQLError } from "graphql";
import { accessDeniedError } from "../schemaExtensions";
import bcryptjs from "bcryptjs";
import { Context } from ".keystone/types";
import { graphql } from "@keystone-6/core";

type ChangePasswordSuccess = {
  user: any;
};

type ChangePasswordFailed = {
  reason: string;
};

export const changePassword = (base: graphql.BaseSchemaMeta) =>
  graphql.field({
    type: graphql.nonNull(
      graphql.union({
        name: "ChangePasswordResult",
        types: [
          graphql.object<ChangePasswordSuccess>()({
            name: "ChangePasswordSuccess",
            fields: {
              user: graphql.field({
                type: graphql.nonNull(base.object("User")),
              }),
            },
          }),
          graphql.object<ChangePasswordFailed>()({
            name: "ChangePasswordFailed",
            fields: {
              reason: graphql.field({
                type: graphql.nonNull(graphql.String),
              }),
            },
          }),
        ],
      })
    ),
    args: {
      currentPassword: graphql.arg({
        type: graphql.nonNull(graphql.String),
      }),
      newPassword: graphql.arg({
        type: graphql.nonNull(graphql.String),
      }),
    },
    async resolve(source, { currentPassword, newPassword }, context: Context) {
      const now = new Date();

      if (!context.session?.data.id) {
        throw accessDeniedError("You are not authenticated.");
      }

      const existingUser = await context.prisma.user.findUnique({
        where: {
          id: context.session.data.id,
        },
      });

      if (!existingUser) {
        throw new GraphQLError("The user does not exist.", {
          extensions: { code: "FA_USER_NOT_EXISTING" },
        });
      }

      const passwordEquals = await bcryptjs.compare(
        currentPassword,
        existingUser.password
      );

      if (!passwordEquals) {
        return {
          __typename: "ChangePasswordFailed",
          reason: "InvalidPassword",
        };
      }

      const newPasswordEqualsOld = await bcryptjs.compare(
        newPassword,
        existingUser.password
      );

      if (newPasswordEqualsOld) {
        return {
          __typename: "ChangePasswordFailed",
          reason: "PasswordMustNotEqualCurrentPassword",
        };
      }

      const passwordPolicy = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{12,}$/;
      const passwordMatchesPolicy = passwordPolicy.test(newPassword);

      if (!passwordMatchesPolicy) {
        return {
          __typename: "ChangePasswordFailed",
          reason: "PasswordPolicyNotMatched",
        };
      }

      const newHashedPassword = await bcryptjs.hash(newPassword, 10);

      const updatedUser = await context.prisma.user.update({
        where: {
          id: existingUser.id,
        },
        data: {
          password: newHashedPassword,
        },
      });

      return {
        __typename: "ChangePasswordSuccess",
        user: updatedUser,
      };
    },
  });
