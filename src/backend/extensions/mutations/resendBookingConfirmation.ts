import { GraphQLError } from "graphql";
import { Context } from ".keystone/types";
import { graphql } from "@keystone-6/core";
import { accessDeniedError } from "../schemaExtensions";
import {
  isAdminOrRoleAdminOrRoleOrganizer,
  isRoleOrganizer,
} from "../../auth.filters";
import { IMessageQueue } from "../eventQueue";

export const resendBookingConfirmation = (
  base: graphql.BaseSchemaMeta,
  eventQueue: () => IMessageQueue
) =>
  graphql.field({
    type: graphql.nonNull(base.object("Booking")),
    args: {
      bookingId: graphql.arg({ type: graphql.nonNull(graphql.ID) }),
    },
    async resolve(source, { bookingId }, context: Context) {
      if (!isAdminOrRoleAdminOrRoleOrganizer(context)) {
        throw accessDeniedError("You are not authenticated.");
      }

      const bookingWithOrganizer = (await context.query.Booking.findOne({
        where: {
          id: bookingId,
        },
        query: "id status event { organizer { id } }",
      })) as {
        id: string;
        status: string;
        event: { organizer: { id: string } };
      };

      if (!bookingWithOrganizer) {
        throw new GraphQLError(`Booking '${bookingId}' does not exist..`, {
          extensions: { code: "FA_BOOKING_NOT_EXISTING" },
        });
      }

      if (
        isRoleOrganizer(context) &&
        bookingWithOrganizer.event.organizer.id !==
          context.session.data?.organizer?.id
      ) {
        throw accessDeniedError("You are not allowed to access this booking.");
      }

      const booking = await context.db.Booking.findOne({
        where: {
          id: bookingId,
        },
      });

      await eventQueue().add("resendBookingConfirmationRequested", booking);

      return booking;
    },
  });
