import { GraphQLError } from "graphql";
import { accessDeniedError } from "../schemaExtensions";
import bcryptjs from "bcryptjs";
import { Context } from ".keystone/types";
import { graphql } from "@keystone-6/core";

type ResetPasswordSuccess = {
  user: any;
};

type ResetPasswordFailed = {
  reason: string;
};

export const resetPassword = (base: graphql.BaseSchemaMeta) =>
  graphql.field({
    type: graphql.nonNull(
      graphql.union({
        name: "ResetPasswordResult",
        types: [
          graphql.object<ResetPasswordSuccess>()({
            name: "ResetPasswordSuccess",
            fields: {
              user: graphql.field({
                type: graphql.nonNull(base.object("User")),
              }),
            },
          }),
          graphql.object<ResetPasswordFailed>()({
            name: "ResetPasswordFailed",
            fields: {
              reason: graphql.field({
                type: graphql.nonNull(graphql.String),
              }),
            },
          }),
        ],
      })
    ),
    args: {
      token: graphql.arg({
        type: graphql.nonNull(graphql.String),
      }),
      newPassword: graphql.arg({
        type: graphql.nonNull(graphql.String),
      }),
    },
    async resolve(source, { token, newPassword }, context: Context) {
      const now = new Date();

      const passwordResetToken =
        await context.prisma.passwordResetToken.findUnique({
          where: {
            validationToken: token,
          },
        });

      if (
        !passwordResetToken ||
        passwordResetToken.validUntil.getTime() < now.getTime()
      ) {
        return {
          __typename: "ResetPasswordFailed",
          reason: "InvalidToken",
        };
      }

      const user = await context.prisma.user.findUnique({
        where: {
          id: passwordResetToken.userId as string,
        },
      });

      if (!user) {
        throw new GraphQLError("The user does not exist.", {
          extensions: { code: "FA_USER_NOT_EXISTING" },
        });
      }

      const newPasswordEqualsOld = await bcryptjs.compare(
        newPassword,
        user.password
      );

      if (newPasswordEqualsOld) {
        return {
          __typename: "ResetPasswordFailed",
          reason: "PasswordMustNotEqualCurrentPassword",
        };
      }

      const passwordPolicy = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{12,}$/;
      const passwordMatchesPolicy = passwordPolicy.test(newPassword);

      if (!passwordMatchesPolicy) {
        return {
          __typename: "ResetPasswordFailed",
          reason: "PasswordPolicyNotMatched",
        };
      }

      const newHashedPassword = await bcryptjs.hash(newPassword, 10);

      const result = await context.prisma.$transaction(async (transaction) => {
        const updatedUser = await context.prisma.user.update({
          where: {
            id: user.id,
          },
          data: {
            password: newHashedPassword,
          },
        });

        await transaction.passwordResetToken.delete({
          where: {
            id: passwordResetToken.id,
          },
        });

        return updatedUser;
      });

      return {
        __typename: "ResetPasswordSuccess",
        user: result,
      };
    },
  });
