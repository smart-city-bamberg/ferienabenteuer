import { GraphQLError } from "graphql";
import { Context } from ".keystone/types";
import { graphql } from "@keystone-6/core";
import { accessDeniedError } from "../schemaExtensions";
import { isAdminOrRoleAdmin } from "../../auth.filters";
import { IMessageQueue } from "../eventQueue";

type ApproveDiscountSuccess = {
  discount: any;
};

type ApproveDiscountFailed = {
  reason: string;
};

export const approveDiscount = (
  base: graphql.BaseSchemaMeta,
  eventQueue: () => IMessageQueue
) =>
  graphql.field({
    type: graphql.nonNull(
      graphql.union({
        name: "ApproveDiscountResult",
        types: [
          graphql.object<ApproveDiscountSuccess>()({
            name: "ApproveDiscountSuccess",
            fields: {
              discount: graphql.field({
                type: graphql.nonNull(base.object("Discount")),
              }),
            },
          }),
          graphql.object<ApproveDiscountFailed>()({
            name: "ApproveDiscountFailed",
            fields: {
              reason: graphql.field({
                type: graphql.nonNull(graphql.String),
              }),
            },
          }),
        ],
      })
    ),
    args: {
      discountId: graphql.arg({ type: graphql.nonNull(graphql.ID) }),
    },
    async resolve(source, { discountId }, context: Context) {
      if (!isAdminOrRoleAdmin(context)) {
        throw accessDeniedError("You are not authenticated.");
      }

      const result = await context.prisma.$transaction(async (transaction) => {
        const discount = await transaction.discount.findFirst({
          where: {
            id: discountId,
          },
        });

        if (!discount) {
          throw new GraphQLError(`Discount '${discountId}' does not exist..`, {
            extensions: { code: "FA_DISCOUNT_NOT_EXISTING" },
          });
        }

        if (!["pending", "overdue"].includes(discount.status!)) {
          return {
            __typename: "ApproveDiscountFailed",
            reason: "CanOnlyApprovePendingOrOverdueDiscount",
          };
        }

        const approvedDiscount = await transaction.discount.update({
          where: {
            id: discountId,
          },
          data: {
            status: "approved",
          },
        });

        return {
          __typename: "ApproveDiscountSuccess",
          discount: approvedDiscount,
        };
      });

      if (result.__typename === "ApproveDiscountSuccess") {
        await eventQueue().add("discountApplicationGranted", result.discount);
      }

      return result;
    },
  });
