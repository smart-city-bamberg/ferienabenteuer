import { GraphQLError } from "graphql";
import { Context } from ".keystone/types";
import { graphql } from "@keystone-6/core";
import { accessDeniedError } from "../schemaExtensions";
import { isAdminOrRoleAdmin } from "../../auth.filters";
import { IMessageQueue } from "../eventQueue";

export const sendEmailToAllCustodians = (eventQueue: () => IMessageQueue) =>
  graphql.field({
    type: graphql.nonNull(graphql.Boolean),
    args: {
      subject: graphql.arg({ type: graphql.nonNull(graphql.String) }),
      content: graphql.arg({ type: graphql.nonNull(graphql.String) }),
    },
    async resolve(source, { subject, content }, context: Context) {
      if (!isAdminOrRoleAdmin(context)) {
        throw accessDeniedError("You are not authenticated.");
      }

      if (!subject) {
        throw new GraphQLError(`Subject must not be empty.`, {
          extensions: { code: "FA_MAIL_SUBJECT_EMPTY" },
        });
      }

      if (!content) {
        throw new GraphQLError(`Content must not be empty.`, {
          extensions: { code: "FA_MAIL_CONTENT_EMPTY" },
        });
      }

      await eventQueue().add("emailToAllCustodiansRequested", {
        subject,
        content,
      });

      return true;
    },
  });
