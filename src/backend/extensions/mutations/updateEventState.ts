import { IMessageQueue } from "./../eventQueue";
import { Context } from ".keystone/types";
import { graphql } from "@keystone-6/core";
import { isAdminOrRoleAdminOrRoleOrganizer } from "../../auth.filters";
import { accessDeniedError } from "../schemaExtensions";

export const updateEventState = (
  base: graphql.BaseSchemaMeta,
  eventQueue: () => IMessageQueue
) =>
  graphql.field({
    type: base.object("Event"),
    args: {
      id: graphql.arg({ type: graphql.nonNull(graphql.ID) }),
      status: graphql.arg({ type: graphql.nonNull(graphql.String) }),
    },
    async resolve(source, { id, status }, context: Context) {
      if (!isAdminOrRoleAdminOrRoleOrganizer(context)) {
        throw accessDeniedError("You cannot update the event state.");
      }

      const updatedEvent = await context.db.Event.updateOne({
        where: { id },
        data: { status: status },
      });

      eventQueue().add("eventStateChanged", updatedEvent);

      return updatedEvent;
    },
  });
