import { GraphQLError } from "graphql";
import { Context } from ".keystone/types";
import { graphql } from "@keystone-6/core";
import { accessDeniedError, oneMinute } from "../schemaExtensions";
import { isRoleCustodian, isTester } from "../../auth.filters";
import { retry } from "@lifeomic/attempt";
import { IMessageQueue } from "../eventQueue";

type AddTicketsToCartSuccess = {
  cartTickets: any[];
};

type AddTicketsToCartFailed = {
  reason: string;
};

class NotEnoughTickets extends Error {}

export const addTicketsToCart = (
  base: graphql.BaseSchemaMeta,
  eventQueue: () => IMessageQueue
) =>
  graphql.field({
    type: graphql.nonNull(
      graphql.union({
        name: "AddTicketsToCartResult",
        types: [
          graphql.object<AddTicketsToCartSuccess>()({
            name: "AddTicketsToCartSuccess",
            fields: {
              cartTickets: graphql.field({
                type: graphql.nonNull(
                  graphql.list(graphql.nonNull(base.object("CartTicket")))
                ),
              }),
            },
          }),
          graphql.object<AddTicketsToCartFailed>()({
            name: "AddTicketsToCartFailed",
            fields: {
              reason: graphql.field({
                type: graphql.nonNull(graphql.String),
              }),
            },
          }),
        ],
      })
    ),
    args: {
      eventId: graphql.arg({ type: graphql.nonNull(graphql.ID) }),
      amounts: graphql.arg({
        type: graphql.nonNull(
          graphql.list(
            graphql.nonNull(
              graphql.inputObject({
                name: "AddTicketsToCartAmountsInput",
                fields: {
                  discountType: graphql.arg({ type: graphql.ID }),
                  amount: graphql.arg({
                    type: graphql.nonNull(graphql.Int),
                  }),
                },
              })
            )
          )
        ),
      }),
    },
    async resolve(source, { eventId, amounts }, context: Context) {
      if (!isRoleCustodian(context)) {
        throw accessDeniedError("You cannot add tickets to your cart.");
      }

      const now = new Date();

      const custodian = await context.prisma.custodian.findFirst({
        where: {
          userId: {
            equals: context.session?.data.id,
          },
        },
      });

      if (!custodian) {
        throw new GraphQLError(
          `Custodian for user '${context.session.data.id}' does not exist.`,
          {
            extensions: { code: "FA_CUSTODIAN_NOT_EXISTING" },
          }
        );
      }

      const currentSeason = await context.prisma.season.findFirst({
        where: {
          releaseDate: {
            lte: now,
          },
        },
        orderBy: {
          year: "desc",
        },
        take: 1,
      });

      if (!currentSeason) {
        return {
          __typename: "AddTicketsToCartFailed",
          reason: "NoBookingSeasonAvailable",
        };
      }

      const { bookingStart, bookingStartPartners } = currentSeason;

      const beforeBookingStart =
        bookingStart && now.getTime() < bookingStart?.getTime();

      const userIsTester = isTester(context);

      if (beforeBookingStart && !userIsTester) {
        const beforeBookingStartPartners =
          bookingStartPartners &&
          now.getTime() < bookingStartPartners?.getTime();

        const earlyAccessCodeVoucher =
          await context.prisma.earlyAccessCodeVoucher.findFirst({
            where: {
              custodianId: custodian.id,
              seasonId: currentSeason.id,
            },
          });

        if (
          beforeBookingStartPartners ||
          (!beforeBookingStartPartners && !earlyAccessCodeVoucher)
        ) {
          return {
            __typename: "AddTicketsToCartFailed",
            reason: "BookingNotStarted",
          };
        }
      }

      const childrenCount = await context.prisma.child.count({
        where: {
          custodianId: { equals: custodian.id },
          isDeleted: {
            equals: false,
          },
        },
      });

      const cartTicketCount = await context.prisma.cartTicket.count({
        where: {
          cart: {
            custodianId: { equals: custodian.id },
          },
          eventTicket: {
            eventId: { equals: eventId },
          },
        },
      });

      const bookedTicketCount = await context.prisma.ticket.count({
        where: {
          child: {
            custodianId: { equals: custodian.id },
          },
          eventTicket: {
            eventId: { equals: eventId },
          },
        },
      });

      const totalAmountOfTicketsToAdd = amounts.reduce(
        (sum, next) => sum + next.amount,
        0
      );

      const remainingSpaceInCart =
        childrenCount -
        cartTicketCount -
        bookedTicketCount -
        totalAmountOfTicketsToAdd;

      if (remainingSpaceInCart < 0) {
        return {
          __typename: "AddTicketsToCartFailed",
          reason: "MaximumAllowedTicketsPerEvent",
        };
      }

      try {
        return await retry(
          async (attemptContext) => {
            return await context.prisma.$transaction(async (transaction) => {
              let cart = await transaction.cart.findFirst({
                where: {
                  custodianId: { equals: custodian.id },
                },
              });

              if (!cart) {
                const cartLifetime = 30 * oneMinute;

                cart = await transaction.cart.create({
                  data: {
                    custodianId: custodian.id,
                    createdAt: now,
                    validUntil: new Date(now.getTime() + cartLifetime),
                  },
                });

                eventQueue().add(
                  "cartTimeout",
                  { cartId: cart.id, version: cart.version },
                  {
                    delay: cartLifetime,
                  }
                );
              }

              const allResults: any[] = [];

              for (let typeAmount of amounts) {
                if (typeAmount.discountType) {
                  const discountType =
                    await transaction.discountType.findUnique({
                      where: {
                        id: typeAmount.discountType,
                      },
                    });

                  if (!discountType) {
                    throw new GraphQLError(
                      `Discount type '${typeAmount.discountType}' does not exist..`,
                      {
                        extensions: { code: "FA_DISCOUNT_TYPE_INVALID" },
                      }
                    );
                  }
                }

                const availableEventTickets =
                  await transaction.eventTicket.findMany({
                    where: {
                      eventId: {
                        equals: eventId,
                      },
                      cartTicket: {
                        is: null,
                      },
                      bookedTicket: {
                        is: null,
                      },
                      isBlocked: {
                        equals: false,
                      },
                    },
                    take: typeAmount.amount,
                    orderBy: [
                      {
                        type: "asc",
                      },
                      { number: "asc" },
                    ],
                  });

                if (availableEventTickets.length < typeAmount.amount) {
                  throw new NotEnoughTickets();
                }

                const result: any[] = [];

                for (let availableTicket of availableEventTickets) {
                  const createdCartTicket = await transaction.cartTicket.create(
                    {
                      data: {
                        cart: {
                          connect: {
                            id: cart.id,
                          },
                        },
                        discountType: typeAmount.discountType
                          ? {
                              connect: {
                                id: typeAmount.discountType as string,
                              },
                            }
                          : undefined,
                        addedToCartAt: now,
                      },
                    }
                  );

                  await transaction.eventTicket.update({
                    where: {
                      id: availableTicket.id,
                      version: availableTicket.version,
                    },
                    data: {
                      version: { increment: 1 },
                      cartTicketId: createdCartTicket.id,
                    },
                  });

                  result.push(createdCartTicket);
                }

                allResults.push(result);
              }

              return {
                __typename: "AddTicketsToCartSuccess",
                cartTickets: allResults.flat(2),
              };
            });
          },
          {
            delay: 100,
            factor: 2,
            maxAttempts: 5,
            jitter: true,
            minDelay: 100,
            maxDelay: 500,
            handleError: (err, attemptContext) => {
              console.log("error while adding ticket", err, attemptContext);

              if (err instanceof NotEnoughTickets) {
                attemptContext.abort();
              }
            },
          }
        );
      } catch (error) {
        console.log("cought error after adding ticket", error);

        if (error instanceof NotEnoughTickets) {
          return {
            __typename: "AddTicketsToCartFailed",
            reason: "NotEnoughTicketsAvailable",
          };
        }

        throw error;
      }
    },
  });
