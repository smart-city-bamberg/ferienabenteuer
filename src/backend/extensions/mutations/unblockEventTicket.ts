import { GraphQLError } from "graphql";
import { Context } from ".keystone/types";
import { graphql } from "@keystone-6/core";
import { accessDeniedError } from "../schemaExtensions";
import { IMessageQueue } from "../eventQueue";

import {
  isAdminOrRoleAdminOrRoleOrganizer,
  isRoleOrganizer,
} from "../../auth.filters";

type UnblockEventTicketSuccess = {
  eventTicket: any;
};

type UnblockEventTicketFailed = {
  reason: string;
};

export const unblockEventTicket = (
  base: graphql.BaseSchemaMeta,
  eventQueue: () => IMessageQueue
) =>
  graphql.field({
    type: graphql.nonNull(
      graphql.union({
        name: "UnblockEventTicketResult",
        types: [
          graphql.object<UnblockEventTicketSuccess>()({
            name: "UnblockEventTicketSuccess",
            fields: {
              eventTicket: graphql.field({
                type: graphql.nonNull(base.object("EventTicket")),
              }),
            },
          }),
          graphql.object<UnblockEventTicketFailed>()({
            name: "UnblockEventTicketFailed",
            fields: {
              reason: graphql.field({
                type: graphql.nonNull(graphql.String),
              }),
            },
          }),
        ],
      })
    ),
    args: {
      eventTicketId: graphql.arg({ type: graphql.nonNull(graphql.ID) }),
    },
    async resolve(source, { eventTicketId }, context: Context) {
      if (!isAdminOrRoleAdminOrRoleOrganizer(context)) {
        throw accessDeniedError("You are not authenticated.");
      }

      const eventTicket = (await context.query.EventTicket.findOne({
        where: {
          id: eventTicketId,
        },
        query: `
          id
          isBlocked
          version
          event {
            id
            organizer {
              id
            }
          }
        `,
      })) as {
        id: string;
        isBlocked: boolean;
        version: number;
        event: {
          id: string;
          organizer: {
            id: string;
          };
        };
      } | null;

      if (!eventTicket) {
        throw new GraphQLError(
          `EventTicket '${eventTicket}' does not exist..`,
          {
            extensions: { code: "FA_EVENTTICKET_NOT_EXISTING" },
          }
        );
      }

      const isOrganizerEvent =
        eventTicket.event.organizer.id === context.session.data?.organizer?.id;

      if (isRoleOrganizer(context) && !isOrganizerEvent) {
        throw accessDeniedError("You are not authorized.");
      }

      if (!eventTicket.isBlocked) {
        return {
          __typename: "UnblockEventTicketFailed",
          reason: "EventTicketNotBlocked",
        };
      }

      const result = await context.prisma.$transaction(async (transaction) => {
        const updatedEventTicket = await transaction.eventTicket.update({
          where: {
            id: eventTicket.id,
            version: eventTicket.version,
          },
          data: {
            isBlocked: false,
            version: {
              increment: 1,
            },
          },
        });

        return updatedEventTicket;
      });

      return {
        __typename: "UnblockEventTicketSuccess",
        eventTicket: result,
      };
    },
  });
