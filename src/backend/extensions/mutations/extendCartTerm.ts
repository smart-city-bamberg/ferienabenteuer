import { GraphQLError } from "graphql";
import { Context } from ".keystone/types";
import { graphql } from "@keystone-6/core";
import { accessDeniedError, oneMinute } from "../schemaExtensions";
import { isRoleCustodian, isTester } from "../../auth.filters";
import { retry } from "@lifeomic/attempt";
import { IMessageQueue } from "../eventQueue";

type ExtendCartTermSuccess = {
  cart: any;
};

type ExtendCartTermFailed = {
  reason: string;
};

class NotEnoughTickets extends Error {}

export const extendCartTerm = (
  base: graphql.BaseSchemaMeta,
  eventQueue: () => IMessageQueue
) =>
  graphql.field({
    type: graphql.nonNull(
      graphql.union({
        name: "ExtendCartTermResult",
        types: [
          graphql.object<ExtendCartTermSuccess>()({
            name: "ExtendCartTermSuccess",
            fields: {
              cart: graphql.field({
                type: graphql.nonNull(base.object("Cart")),
              }),
            },
          }),
          graphql.object<ExtendCartTermFailed>()({
            name: "ExtendCartTermFailed",
            fields: {
              reason: graphql.field({
                type: graphql.nonNull(graphql.String),
              }),
            },
          }),
        ],
      })
    ),
    async resolve(source, args, context: Context) {
      if (!isRoleCustodian(context)) {
        throw accessDeniedError("You cannot add tickets to your cart.");
      }

      const now = new Date();

      const custodian = await context.prisma.custodian.findFirst({
        where: {
          userId: {
            equals: context.session?.data.id,
          },
        },
      });

      if (!custodian) {
        throw new GraphQLError(
          `Custodian for user '${context.session.data.id}' does not exist.`,
          {
            extensions: { code: "FA_CUSTODIAN_NOT_EXISTING" },
          }
        );
      }

      const cart = await context.prisma.cart.findFirst({
        where: {
          custodianId: { equals: custodian.id },
        },
      });

      if (!cart) {
        throw new GraphQLError(
          `Cart for user '${context.session.data.id}' does not exist.`,
          {
            extensions: { code: "FA_CART_NOT_EXISTING" },
          }
        );
      }

      const maximumTermExtension = 10;

      if (cart.termExtensionCount >= maximumTermExtension) {
        return {
          __typename: "ExtendCartTermFailed",
          reason: "MaximumTermExtensionsReached",
        };
      }

      const cartLifetime = 30 * oneMinute;

      const result = await context.prisma.$transaction(async (transaction) => {
        const updatedCart = await transaction.cart.update({
          where: {
            id: cart.id,
            version: cart.version,
          },
          data: {
            custodianId: custodian.id,
            validUntil: new Date(now.getTime() + cartLifetime),
            termExtensionCount: {
              increment: 1,
            },
            version: {
              increment: 1,
            },
          },
        });

        return updatedCart;
      });

      eventQueue().add(
        "cartTimeout",
        { cartId: cart.id },
        {
          delay: cartLifetime,
        }
      );

      return {
        __typename: "ExtendCartTermSuccess",
        cart: result,
      };
    },
  });
