import { GraphQLError } from "graphql";
import { Context } from ".keystone/types";
import { graphql } from "@keystone-6/core";
import { accessDeniedError } from "../schemaExtensions";
import { isAdminOrRoleAdmin } from "../../auth.filters";
import { IMessageQueue } from "../eventQueue";

type RejectDiscountSuccess = {
  discount: any;
};

type RejectDiscountFailed = {
  reason: string;
};

export const rejectDiscount = (
  base: graphql.BaseSchemaMeta,
  eventQueue: () => IMessageQueue
) =>
  graphql.field({
    type: graphql.nonNull(
      graphql.union({
        name: "RejectDiscountResult",
        types: [
          graphql.object<RejectDiscountSuccess>()({
            name: "RejectDiscountSuccess",
            fields: {
              discount: graphql.field({
                type: graphql.nonNull(base.object("Discount")),
              }),
            },
          }),
          graphql.object<RejectDiscountFailed>()({
            name: "RejectDiscountFailed",
            fields: {
              reason: graphql.field({
                type: graphql.nonNull(graphql.String),
              }),
            },
          }),
        ],
      })
    ),
    args: {
      discountId: graphql.arg({ type: graphql.nonNull(graphql.ID) }),
      discountRejectionReasonId: graphql.arg({
        type: graphql.nonNull(graphql.ID),
      }),
      rejectionNote: graphql.arg({ type: graphql.String }),
    },
    async resolve(
      source,
      { discountId, discountRejectionReasonId, rejectionNote },
      context: Context
    ) {
      if (!isAdminOrRoleAdmin(context)) {
        throw accessDeniedError("You are not authenticated.");
      }

      const result = await context.prisma.$transaction(async (transaction) => {
        const discount = await transaction.discount.findFirst({
          where: {
            id: discountId,
          },
        });

        if (!discount) {
          throw new GraphQLError(`Discount '${discountId}' does not exist..`, {
            extensions: { code: "FA_DISCOUNT_NOT_EXISTING" },
          });
        }

        const discountRejectionReason =
          await transaction.discountRejectionReason.findFirst({
            where: {
              id: discountRejectionReasonId,
            },
          });

        if (!discountRejectionReason) {
          throw new GraphQLError(
            `Discount rejection reason '${discountRejectionReasonId}' does not exist..`,
            {
              extensions: { code: "FA_DISCOUNT_REJECTION_REASON_NOT_EXISTING" },
            }
          );
        }

        if (!["pending", "overdue"].includes(discount.status!)) {
          return {
            __typename: "RejectDiscountFailed",
            reason: "CanOnlyRejectPendingOrOverdueDiscount",
          };
        }

        const rejectedDiscount = await transaction.discount.update({
          where: {
            id: discountId,
          },
          data: {
            status: "rejected",
            rejectionReasonId: discountRejectionReasonId,
            rejectionNote: rejectionNote ?? undefined,
          },
        });

        return {
          __typename: "RejectDiscountSuccess",
          discount: rejectedDiscount,
        };
      });

      if (result.__typename === "RejectDiscountSuccess") {
        await eventQueue().add("discountApplicationRejected", result.discount);
      }

      return result;
    },
  });
