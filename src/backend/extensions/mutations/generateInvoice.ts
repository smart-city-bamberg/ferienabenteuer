import { createWriteStream, writeSync, writeFileSync } from "fs";
import { GraphQLError } from "graphql";
import { Context } from ".keystone/types";
import { graphql } from "@keystone-6/core";
import { accessDeniedError } from "../schemaExtensions";
import {
  isAdminOrRoleAdminOrRoleOrganizer,
  isRoleOrganizer,
} from "../../auth.filters";
import * as ExcelJS from "exceljs";
import { StorageConfig } from "@keystone-6/core/types";
import crypto from "crypto";

const calculateAge = (dateOfBirth: string, now: Date): number => {
  const birthDate = new Date(dateOfBirth);

  let age = now.getFullYear() - birthDate.getFullYear();
  const monthDiff = now.getMonth() - birthDate.getMonth();

  if (
    monthDiff < 0 ||
    (monthDiff === 0 && now.getDate() < birthDate.getDate())
  ) {
    age--;
  }

  return age;
};

const sanitize = (input: string, replacement: string) => {
  const illegalRe = /[\/\?<>\\:\*\|":]/g;
  const controlRe = /[\x00-\x1f\x80-\x9f]/g;
  const reservedRe = /^\.+$/;
  const windowsReservedRe = /^(con|prn|aux|nul|com[0-9]|lpt[0-9])(\..*)?$/i;

  return input
    .replace(illegalRe, replacement)
    .replace(controlRe, replacement)
    .replace(reservedRe, replacement)
    .replace(windowsReservedRe, replacement);
};

export const generateInvoice = (
  base: graphql.BaseSchemaMeta,
  config: StorageConfig
) =>
  graphql.field({
    type: graphql.nonNull(base.object("Invoice")),
    args: {
      eventId: graphql.arg({
        type: graphql.nonNull(graphql.ID),
      }),
    },
    async resolve(source, { eventId }, context: Context) {
      const now = new Date();

      if (!isAdminOrRoleAdminOrRoleOrganizer(context)) {
        throw accessDeniedError("You cannot generate invoices.");
      }

      const event = (await context.query.Event.findOne({
        where: {
          id: eventId,
        },
        query: `
          id
          name
          costs
          season { id year grantAmount }
          organizer {
            id
            name
          }
          carePeriodCommitted {
            careDays( orderBy: { day: asc } ) {
              id
              day
            }
          }
        `,
      })) as {
        id: string;
        name: string;
        costs: number;
        organizer: { id: string; name: string };
        season: { id: string; year: number; grantAmount: number };
        carePeriodCommitted: {
          careDays: {
            id: string;
            day: string;
          }[];
        };
      } | null;

      if (!event) {
        throw new GraphQLError(`Event '${eventId}' does not exist..`, {
          extensions: { code: "FA_EVENT_NOT_EXISTING" },
        });
      }

      if (
        isRoleOrganizer(context) &&
        event.organizer.id !== context.session.data?.organizer?.id
      ) {
        throw accessDeniedError("You cannot generate invoices.");
      }

      const sudoContext = context.sudo();

      await sudoContext.prisma.invoice.deleteMany({
        where: {
          eventId,
        },
      });

      const tickets = (await sudoContext.query.Ticket.findMany({
        where: {
          booking: {
            event: {
              id: {
                equals: eventId,
              },
            },
            isDeleted: {
              equals: false,
            },
          },
        },
        query: `
          id
          child {
            firstname
            surname
            dateOfBirth
            gender
            custodian {
              id
              addressCity
            }
          }          
          booking {
            status
            date
            event {
              season {
                id
              }
            }
          }
          discountVoucher {
            id
            discount {
              status
            }
          }
        `,
      })) as {
        id: string;
        child: {
          firstname: string;
          surname: string;
          dateOfBirth: string;
          gender: string;
          custodian: {
            id: string;
            addressCity: string;
          };
        };
        booking: {
          status: string;
          date: string;
          event: {
            season: {
              id: string;
            };
          };
        };
        discountVoucher: {
          id: string;
          discount: {
            status: string;
          };
        } | null;
      }[];

      const earlyAccessCodes =
        (await sudoContext.query.EarlyAccessCode.findMany({
          where: {
            season: {
              id: {
                equals: event.season.id,
              },
            },
          },
          query: `
            id
            code
            season {
              id
            }
            partner {      
              name
            }
        `,
        })) as {
          id: string;
          code: string;
          season: {
            id: string;
          };
          partner: {
            name: string;
          };
        }[];

      const earlyAccessCodeVouchers =
        (await sudoContext.query.EarlyAccessCodeVoucher.findMany({
          where: {
            season: {
              id: {
                equals: event.season.id,
              },
            },
          },
          query: `
            id
            code
            season {
              id
            }
            custodian {
              id
            }
        `,
        })) as {
          id: string;
          code: string;
          season: {
            id: string;
          };
          custodian: {
            id: string;
          };
        }[];

      const workbook = new ExcelJS.Workbook();
      const statisticsWorksheet = workbook.addWorksheet(`Statistik`, {
        properties: {
          defaultColWidth: 20,
        },
      });

      const oranizerNameCell = statisticsWorksheet.getCell(1, 1);
      oranizerNameCell.value = event.organizer.name;
      oranizerNameCell.font = {
        bold: true,
      };

      const eventNameCell = statisticsWorksheet.getCell(2, 1);

      const careDays = event.carePeriodCommitted.careDays;
      const firstDay = new Date(careDays[0].day);
      const lastDay = new Date(careDays[careDays.length - 1].day);
      const eventDateString = `${firstDay.getDate()}.${
        firstDay.getMonth() + 1
      }. - ${lastDay.getDate()}.${
        lastDay.getMonth() + 1
      }.${lastDay.getFullYear()}`;

      eventNameCell.value = `${event.name} ${eventDateString}`;

      eventNameCell.font = {
        bold: true,
      };

      const columns = [
        "Nr.",
        "Nachname",
        "Vorname",
        "Ort",
        "Geschlecht",
        "Alter",
        "Partner",
        "Status",
        "Ermäßigung erteilt?",
      ];

      for (const [columnIndex, column] of columns.entries()) {
        const columnCell = statisticsWorksheet.getCell(4, columnIndex + 1);
        columnCell.value = column;
        columnCell.font = {
          bold: true,
        };

        const borderStyle: Partial<ExcelJS.Border> = { style: "thin" };

        columnCell.border = {
          top: borderStyle,
          bottom: borderStyle,
          left: borderStyle,
          right: borderStyle,
        };
      }

      const eventDate = new Date(event.carePeriodCommitted.careDays[0].day);

      const sortedTickets = tickets.sort((a, b) => {
        const dateA = a.booking?.date;
        const dateB = b.booking?.date;

        if (!dateA) {
          if (!dateB) {
            return 0;
          }

          return 1;
        }

        if (!dateB) {
          return -1;
        }

        return dateA.localeCompare(dateB);
      });

      for (const [ticketIndex, ticket] of sortedTickets.entries()) {
        const rowIndex = 5 + ticketIndex;

        statisticsWorksheet.getCell(rowIndex, 1).value = ticketIndex + 1;
        statisticsWorksheet.getCell(rowIndex, 2).value = ticket.child.surname;
        statisticsWorksheet.getCell(rowIndex, 3).value = ticket.child.firstname;
        statisticsWorksheet.getCell(rowIndex, 4).value =
          ticket.child.custodian.addressCity;
        statisticsWorksheet.getCell(rowIndex, 5).value =
          ticket.child.gender === "male"
            ? "m"
            : ticket.child.gender === "female"
            ? "w"
            : "d";

        statisticsWorksheet.getCell(rowIndex, 6).value = calculateAge(
          ticket.child.dateOfBirth,
          eventDate
        );

        const earlyAccessCodeVoucher = earlyAccessCodeVouchers.find(
          (voucher) => voucher.custodian.id === ticket.child.custodian.id
        );
        const earlyAccessCode = earlyAccessCodes.find(
          (code) => code.code === earlyAccessCodeVoucher?.code
        );

        statisticsWorksheet.getCell(rowIndex, 7).value =
          earlyAccessCode?.partner.name ?? "-";

        const statusCell = statisticsWorksheet.getCell(rowIndex, 8);

        statusCell.value =
          ticket.booking.status === "payed" ? "bezahlt" : "ausstehend";

        statusCell.font = {
          color: {
            argb: ticket.booking.status === "payed" ? "00aa00" : "cc0000",
          },
        };

        const discountCell = statisticsWorksheet.getCell(rowIndex, 9);

        discountCell.value =
          ticket.discountVoucher?.discount.status === "approved"
            ? "ja"
            : "nein";

        discountCell.font = {
          color: {
            argb:
              ticket.discountVoucher?.discount.status === "approved"
                ? "00aa00"
                : "cc0000",
          },
        };

        for (let columnIndex = 0; columnIndex < 9; columnIndex++) {
          const columnCell = statisticsWorksheet.getCell(
            rowIndex,
            columnIndex + 1
          );

          const borderStyle: Partial<ExcelJS.Border> = { style: "thin" };

          columnCell.border = {
            top: borderStyle,
            bottom: borderStyle,
            left: borderStyle,
            right: borderStyle,
          };
        }
      }

      const grantLabelCell = statisticsWorksheet.getCell(
        sortedTickets.length + 6,
        1
      );

      grantLabelCell.value = "Zuschuss";
      grantLabelCell.font = {
        bold: true,
      };

      statisticsWorksheet.getCell(sortedTickets.length + 7, 1).value = "A)";
      statisticsWorksheet.getCell(sortedTickets.length + 8, 1).value = "B)";

      const grantSumLabelCell = statisticsWorksheet.getCell(
        sortedTickets.length + 9,
        1
      );
      grantSumLabelCell.value = "A+B)";
      grantSumLabelCell.font = {
        bold: true,
      };

      statisticsWorksheet.getCell(sortedTickets.length + 7, 2).value = `${
        sortedTickets.length
      } TN x ${
        event.carePeriodCommitted.careDays.length
      } x ${event.season.grantAmount.toLocaleString(undefined, {
        minimumFractionDigits: 2,
        maximumFractionDigits: 2,
      })}€`;

      statisticsWorksheet.getCell(sortedTickets.length + 8, 2).value = `${
        sortedTickets.filter((ticket) => ticket.discountVoucher).length
      } erm. TN x ${event.costs / 2}€`;

      const grantSumLabel2Cell = statisticsWorksheet.getCell(
        sortedTickets.length + 9,
        2
      );
      grantSumLabel2Cell.value = "gesamt:";
      grantSumLabel2Cell.font = {
        bold: true,
      };

      const grantAValueCell = statisticsWorksheet.getCell(
        sortedTickets.length + 7,
        3
      );

      const grantAValue =
        sortedTickets.length *
        event.carePeriodCommitted.careDays.length *
        event.season.grantAmount;

      grantAValueCell.value = grantAValue;
      grantAValueCell.numFmt = "0.00 €";

      const grantBValueCell = statisticsWorksheet.getCell(
        sortedTickets.length + 8,
        3
      );

      const grantBValue =
        sortedTickets.filter((ticket) => ticket.discountVoucher).length *
        (event.costs / 2);

      grantBValueCell.value = grantBValue;
      grantBValueCell.numFmt = "0.00 €";

      const grantSumValueCell = statisticsWorksheet.getCell(
        sortedTickets.length + 9,
        3
      );

      grantSumValueCell.value = {
        formula: `SUM( ${grantAValueCell.address}+${grantBValueCell.address} )`,
        date1904: false,
        result: grantAValue + grantBValue,
      };

      grantSumValueCell.numFmt = "0.00 €";
      grantSumValueCell.font = {
        bold: true,
      };

      if (config.kind !== "local") {
        return new GraphQLError("Could not save generated document.");
      }

      try {
        const buffer = await workbook.xlsx.writeBuffer();
        const fileName = `${event.season.year} ${event.organizer.name} (${event.name}) ${eventDateString}.xlsx`;
        const sanitizedFileName = sanitize(fileName, "");
        const filePath = `${config.storagePath}/${sanitizedFileName}`;
        const writeStream = createWriteStream(filePath, { flags: "w" });

        writeStream.write(buffer);
        writeStream.close();
        const invoice = await sudoContext.prisma.invoice.create({
          data: {
            createdAt: now,
            event: {
              connect: {
                id: event.id,
              },
            },
            document_filename: sanitizedFileName,
            document_filesize: buffer.byteLength,
          },
        });

        return invoice;
      } catch (error) {
        console.log(error);
        return error;
      }
    },
  });
