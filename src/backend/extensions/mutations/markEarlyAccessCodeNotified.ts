import { GraphQLError } from "graphql";
import { Context } from ".keystone/types";
import { graphql } from "@keystone-6/core";
import { accessDeniedError } from "../schemaExtensions";
import { isAdminOrRoleAdmin } from "../../auth.filters";

export const markEarlyAccessCodeNotified = (base: graphql.BaseSchemaMeta) =>
  graphql.field({
    type: graphql.nonNull(base.object("EarlyAccessCode")),
    args: {
      earlyAccessCodeId: graphql.arg({ type: graphql.nonNull(graphql.ID) }),
    },
    async resolve(source, { earlyAccessCodeId }, context: Context) {
      if (!isAdminOrRoleAdmin(context)) {
        throw accessDeniedError("You are not authenticated.");
      }

      const earlyAccessCode = await context.db.EarlyAccessCode.findOne({
        where: {
          id: earlyAccessCodeId,
        },
      });

      if (!earlyAccessCode) {
        throw new GraphQLError(
          `EarlyAccessCode '${earlyAccessCodeId}' does not exist.`,
          {
            extensions: { code: "FA_EARLYACCESSCODE_NOT_EXISTING" },
          }
        );
      }

      const now = new Date();

      const updatedEarlyAccessCode = await context.db.EarlyAccessCode.updateOne(
        {
          where: {
            id: earlyAccessCodeId,
          },
          data: {
            notificationDate: now,
          },
        }
      );

      return updatedEarlyAccessCode;
    },
  });
