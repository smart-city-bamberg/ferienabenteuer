import { oneHour } from "../schemaExtensions";
import { Context } from ".keystone/types";
import { graphql } from "@keystone-6/core";
import crypto from "crypto";
import { IMessageQueue } from "../eventQueue";

type ResendVerificationCodeSuccess = {
  success: boolean;
};

type ResendVerificationCodeFailed = {
  reason: string;
};

export const resendVerificationCode = (
  base: graphql.BaseSchemaMeta,
  eventQueue: () => IMessageQueue
) =>
  graphql.field({
    type: graphql.nonNull(
      graphql.union({
        name: "ResendVerificationCodeResult",
        types: [
          graphql.object<ResendVerificationCodeSuccess>()({
            name: "ResendVerificationCodeSuccess",
            fields: {
              success: graphql.field({
                type: graphql.nonNull(graphql.Boolean),
              }),
            },
          }),
          graphql.object<ResendVerificationCodeFailed>()({
            name: "ResendVerificationCodeFailed",
            fields: {
              reason: graphql.field({
                type: graphql.nonNull(graphql.String),
              }),
            },
          }),
        ],
      })
    ),
    args: {
      data: graphql.arg({
        type: graphql.nonNull(
          graphql.inputObject({
            name: "ResendVerificationCodeInput",
            fields: {
              email: graphql.arg({
                type: graphql.nonNull(graphql.String),
              }),
            },
          })
        ),
      }),
    },
    async resolve(source, { data }, context: Context) {
      const now = new Date();

      const existingUser = await context.prisma.user.findFirst({
        where: {
          email: data.email,
        },
      });

      if (!existingUser) {
        return {
          __typename: "ResendVerificationCodeSuccess",
          success: true,
        };
      }

      const transactionResult = await context.prisma.$transaction(
        async (transaction) => {
          const existingVerificationToken =
            await transaction.userVerificationToken.findFirst({
              where: {
                userId: existingUser.id,
              },
            });

          if (existingVerificationToken) {
            await transaction.userVerificationToken.delete({
              where: {
                id: existingVerificationToken.id,
              },
            });
          }

          await transaction.userVerificationToken.create({
            data: {
              userId: existingUser.id,
              createdAt: now,
              validUntil: new Date(now.getTime() + 24 * oneHour),
              validationToken: crypto
                .randomBytes(20)
                .toString("hex")
                .toUpperCase(),
            },
          });

          return existingUser;
        }
      );

      await eventQueue().add("userRegistered", transactionResult);

      return {
        __typename: "ResendVerificationCodeSuccess",
        success: true,
      };
    },
  });
