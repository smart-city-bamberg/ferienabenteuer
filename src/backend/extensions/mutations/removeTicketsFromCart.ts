import { Context } from ".keystone/types";
import { graphql } from "@keystone-6/core";
import { accessDeniedError } from "../schemaExtensions";

export const removeTicketsFromCart = (base: graphql.BaseSchemaMeta) =>
  graphql.field({
    type: graphql.nonNull(graphql.list(graphql.nonNull(graphql.ID))),
    args: {
      ticketIds: graphql.arg({
        type: graphql.nonNull(graphql.list(graphql.nonNull(graphql.ID))),
      }),
    },
    async resolve(source, { ticketIds }, context: Context) {
      if (!context.session?.data.id) {
        throw accessDeniedError("You are not authenticated.");
      }

      const custodian = await context.prisma.custodian.findFirst({
        where: {
          userId: {
            equals: context.session?.data.id,
          },
        },
      });

      if (!custodian) {
        throw accessDeniedError("You cannot remove tickets from your cart.");
      }

      const removedTicketIds = new Array<string>();

      await context.prisma.$transaction(async (transaction) => {
        const cart = await context.prisma.cart.findFirst({
          where: {
            custodianId: { equals: custodian.id },
          },
        });

        if (!cart) {
          throw accessDeniedError(`The cart does not exist.`);
        }

        for (let ticketId of ticketIds) {
          const cartTickets = (await context.query.CartTicket.findMany({
            where: {
              cart: {
                id: {
                  equals: cart.id,
                },
              },
              id: {
                equals: ticketId,
              },
            },
            query: "id eventTicket { id }",
          })) as { id: string; eventTicket: { id: string } }[];

          if (cartTickets.length != 1) {
            throw accessDeniedError(
              `The ticket ${ticketId} does not exist in your cart.`
            );
          }

          const cartTicket = cartTickets[0];

          const eventTicket = await transaction.eventTicket.findUnique({
            where: {
              id: cartTicket.eventTicket.id,
            },
          });

          if (eventTicket) {
            await transaction.eventTicket.update({
              where: {
                id: eventTicket.id,
                version: eventTicket.version,
              },
              data: {
                version: { increment: 1 },
                cartTicketId: null,
              },
            });
          }

          await transaction.cartTicket.delete({
            where: {
              id: cartTicket.id,
            },
          });

          removedTicketIds.push(cartTicket.id);
        }

        const remainingCartTicketsCount = await transaction.cartTicket.count({
          where: {
            cart: {
              id: {
                equals: cart.id,
              },
            },
          },
        });

        if (remainingCartTicketsCount === 0) {
          await transaction.cart.delete({
            where: {
              id: cart.id,
            },
          });
        }
      });

      return removedTicketIds;
    },
  });
