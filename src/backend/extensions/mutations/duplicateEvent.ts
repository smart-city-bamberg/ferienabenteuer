import { StorageConfig } from "@keystone-6/core/types";
import { GraphQLError } from "graphql";
import { accessDeniedError } from "../schemaExtensions";
import { Context } from ".keystone/types";
import { graphql } from "@keystone-6/core";
import { isAdminOrRoleAdminOrRoleOrganizer } from "../../auth.filters";
import { copyFileSync } from "fs";
import { randomUUID } from "crypto";

export const duplicateEvent = (
  base: graphql.BaseSchemaMeta,
  config: StorageConfig
) =>
  graphql.field({
    type: graphql.nonNull(graphql.list(graphql.nonNull(base.object("Event")))),
    args: {
      eventId: graphql.arg({ type: graphql.nonNull(graphql.ID) }),
      seasonId: graphql.arg({ type: graphql.nonNull(graphql.ID) }),
      amount: graphql.arg({ type: graphql.nonNull(graphql.Int) }),
    },
    async resolve(source, { eventId, seasonId, amount }, context: Context) {
      if (!isAdminOrRoleAdminOrRoleOrganizer(context)) {
        throw accessDeniedError("You are not allowed to duplicate events.");
      }

      const event = await context.prisma.event.findUnique({
        where: {
          id: eventId,
        },
      });

      if (!event) {
        throw new GraphQLError("The event does not exist.", {
          extensions: { code: "FA_EVENT_NOT_EXISTING" },
        });
      }

      if (amount < 1 || amount > 10) {
        throw new GraphQLError("The amount mut be between 1 and 10.", {
          extensions: { code: "FA_EVENT_NOT_EXISTING" },
        });
      }

      const duplicatedEvents: any[] = [];

      for (let pictureIndex = 0; pictureIndex < amount; pictureIndex++) {
        const picture = {
          picture_id: event.picture_id,
          picture_extension: event.picture_extension,
          picture_width: event.picture_width,
          picture_height: event.picture_height,
          picture_filesize: event.picture_filesize,
        };

        if (event.picture_id) {
          if (config.kind !== "local") {
            throw new GraphQLError("Could not copy event picture.", {
              extensions: { code: "FA_CANNOT_COPY_EVENT_PICTURE" },
            });
          }

          const fileId = randomUUID();

          const sourceFilePath = `${config.storagePath}/${event.picture_id}.${event.picture_extension}`;
          const targetFilePath = `${config.storagePath}/${fileId}.${event.picture_extension}`;
          copyFileSync(sourceFilePath, targetFilePath);

          picture.picture_id = fileId;
        }

        const duplicatedEvent = await context.prisma.event.create({
          data: {
            ...event,
            id: undefined,
            seasonId: seasonId,
            carePeriodAlternativeId: undefined,
            carePeriodDesiredId: undefined,
            carePeriodCommitted: undefined,
            status: "new",
            ...picture,
          },
        });

        duplicatedEvents.push(duplicatedEvent);
      }

      return duplicatedEvents;
    },
  });
