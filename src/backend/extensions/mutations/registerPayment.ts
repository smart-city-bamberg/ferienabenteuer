import { GraphQLError } from "graphql";
import { Context } from ".keystone/types";
import { graphql } from "@keystone-6/core";
import { accessDeniedError } from "../schemaExtensions";
import {
  isAdminOrRoleAdminOrRoleOrganizer,
  isRoleOrganizer,
} from "../../auth.filters";
import { IMessageQueue } from "../eventQueue";

type RegisterPaymentSuccess = {
  booking: any;
};

type RegisterPaymentFailed = {
  reason: string;
};

export const registerPayment = (
  base: graphql.BaseSchemaMeta,
  eventQueue: () => IMessageQueue
) =>
  graphql.field({
    type: graphql.nonNull(
      graphql.union({
        name: "RegisterPaymentResult",
        types: [
          graphql.object<RegisterPaymentSuccess>()({
            name: "RegisterPaymentSuccess",
            fields: {
              booking: graphql.field({
                type: graphql.nonNull(base.object("Booking")),
              }),
            },
          }),
          graphql.object<RegisterPaymentFailed>()({
            name: "RegisterPaymentFailed",
            fields: {
              reason: graphql.field({
                type: graphql.nonNull(graphql.String),
              }),
            },
          }),
        ],
      })
    ),
    args: {
      bookingId: graphql.arg({ type: graphql.nonNull(graphql.ID) }),
    },
    async resolve(source, { bookingId }, context: Context) {
      if (!isAdminOrRoleAdminOrRoleOrganizer(context)) {
        throw accessDeniedError("You are not authenticated.");
      }

      const booking = (await context.query.Booking.findOne({
        where: {
          id: bookingId,
        },
        query: "id status event { organizer { id } }",
      })) as {
        id: string;
        status: string;
        event: { organizer: { id: string } };
      };

      if (!booking) {
        throw new GraphQLError(`Booking '${bookingId}' does not exist..`, {
          extensions: { code: "FA_BOOKING_NOT_EXISTING" },
        });
      }

      if (
        isRoleOrganizer(context) &&
        booking.event.organizer.id !== context.session.data?.organizer?.id
      ) {
        throw accessDeniedError("You are not allowed to access this booking.");
      }

      const result = await context.prisma.$transaction(async (transaction) => {
        if (!["pending", "overdue"].includes(booking.status!)) {
          return {
            __typename: "RegisterPaymentFailed",
            reason: "CanOnlyRegisterPaymentForPendingOrOverdueBooking",
          };
        }

        const payedBooking = await transaction.booking.update({
          where: {
            id: bookingId,
          },
          data: {
            status: "payed",
          },
        });

        return {
          __typename: "RegisterPaymentSuccess",
          booking: payedBooking,
        };
      });

      if (result.__typename === "RegisterPaymentSuccess") {
        await eventQueue().add("paymentRegistered", result.booking);
      }

      return result;
    },
  });
