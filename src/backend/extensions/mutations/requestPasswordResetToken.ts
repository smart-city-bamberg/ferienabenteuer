import { oneHour } from "../schemaExtensions";
import { Context } from ".keystone/types";
import { graphql } from "@keystone-6/core";
import crypto from "crypto";
import { IMessageQueue } from "../eventQueue";

type RequestPasswordResetTokenSuccess = {
  success: boolean;
};

type RequestPasswordResetTokenFailed = {
  reason: string;
};

export const requestPasswordResetToken = (
  base: graphql.BaseSchemaMeta,
  eventQueue: () => IMessageQueue
) =>
  graphql.field({
    type: graphql.nonNull(
      graphql.union({
        name: "RequestPasswordResetTokenResult",
        types: [
          graphql.object<RequestPasswordResetTokenSuccess>()({
            name: "RequestPasswordResetTokenSuccess",
            fields: {
              success: graphql.field({
                type: graphql.nonNull(graphql.Boolean),
              }),
            },
          }),
          graphql.object<RequestPasswordResetTokenFailed>()({
            name: "RequestPasswordResetTokenFailed",
            fields: {
              reason: graphql.field({
                type: graphql.nonNull(graphql.String),
              }),
            },
          }),
        ],
      })
    ),
    args: {
      data: graphql.arg({
        type: graphql.nonNull(
          graphql.inputObject({
            name: "RequestPasswordResetTokenInput",
            fields: {
              email: graphql.arg({
                type: graphql.nonNull(graphql.String),
              }),
            },
          })
        ),
      }),
    },
    async resolve(source, { data }, context: Context) {
      const now = new Date();

      const existingUser = await context.prisma.user.findFirst({
        where: {
          email: data.email,
        },
      });

      if (!existingUser) {
        return {
          __typename: "RequestPasswordResetTokenSuccess",
          success: true,
        };
      }

      const transactionResult = await context.prisma.$transaction(
        async (transaction) => {
          const existingVerificationToken =
            await transaction.passwordResetToken.findFirst({
              where: {
                userId: existingUser.id,
              },
            });

          if (existingVerificationToken) {
            await transaction.passwordResetToken.delete({
              where: {
                id: existingVerificationToken.id,
              },
            });
          }

          await transaction.passwordResetToken.create({
            data: {
              userId: existingUser.id,
              createdAt: now,
              validUntil: new Date(now.getTime() + 24 * oneHour),
              validationToken: crypto
                .randomBytes(20)
                .toString("hex")
                .toUpperCase(),
            },
          });

          return existingUser;
        }
      );

      await eventQueue().add("passwordResetRequested", transactionResult);

      return {
        __typename: "RequestPasswordResetTokenSuccess",
        success: true,
      };
    },
  });
