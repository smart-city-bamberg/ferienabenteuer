import { Context } from ".keystone/types";
import { graphql } from "@keystone-6/core";
import { accessDeniedError } from "../schemaExtensions";
import { isAdminOrRoleAdmin } from "../../auth.filters";
import { generateEarlyAccessCode } from "../../codeGenerator";

export const generateEarlyAccessCodes = (base: graphql.BaseSchemaMeta) =>
  graphql.field({
    type: graphql.nonNull(
      graphql.list(graphql.nonNull(base.object("EarlyAccessCode")))
    ),
    args: {
      seasonId: graphql.arg({
        type: graphql.nonNull(graphql.ID),
      }),
    },
    async resolve(source, { seasonId }, context: Context) {
      if (!isAdminOrRoleAdmin(context)) {
        throw accessDeniedError("You cannot generate early access codes.");
      }

      const partners = await context.prisma.partner.findMany();

      const createdCodes = await context.prisma.$transaction(
        async (transaction) => {
          await transaction.earlyAccessCode.deleteMany({
            where: {
              seasonId,
            },
          });

          const result = [];

          for (const partner of partners) {
            const code = await transaction.earlyAccessCode.create({
              data: {
                code: generateEarlyAccessCode(),
                seasonId,
                partnerId: partner.id,
              },
            });

            result.push(code);
          }

          return result;
        }
      );

      return createdCodes;
    },
  });
