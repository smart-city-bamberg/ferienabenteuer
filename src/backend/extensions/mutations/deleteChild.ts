import { accessDeniedError } from "../schemaExtensions";
import { Context } from ".keystone/types";
import { graphql } from "@keystone-6/core";

export const markChildDeleted = (base: graphql.BaseSchemaMeta) =>
  graphql.field({
    type: base.object("Child"),
    args: {
      childId: graphql.arg({
        type: graphql.nonNull(graphql.ID),
      }),
    },
    async resolve(source, { childId }, context: Context) {
      if (!context.session?.data.id) {
        throw accessDeniedError("You are not authenticated.");
      }

      const custodian = await context.prisma.custodian.findFirst({
        where: {
          userId: {
            equals: context.session?.data.id,
          },
        },
      });

      if (!custodian) {
        throw accessDeniedError("You cannot remove a child.");
      }

      const child = await context.prisma.child.findFirst({
        where: {
          custodianId: {
            equals: custodian.id,
          },
          id: {
            equals: childId,
          },
        },
      });

      if (!child) {
        throw accessDeniedError("This child does not exist.");
      }

      const updatedChild = await context.prisma.child.update({
        where: {
          id: child.id,
        },
        data: {
          isDeleted: true,
        },
      });

      return updatedChild;
    },
  });
