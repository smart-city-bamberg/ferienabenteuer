import { GraphQLError } from "graphql";
import { Context } from ".keystone/types";
import { graphql } from "@keystone-6/core";
import { accessDeniedError } from "../schemaExtensions";
import { IMessageQueue } from "../eventQueue";
import { isAdminOrRoleAdminOrRoleOrganizer } from "../../auth.filters";

export const cancelTicket = (
  base: graphql.BaseSchemaMeta,
  eventQueue: () => IMessageQueue
) =>
  graphql.field({
    type: graphql.nonNull(
      graphql.list(graphql.nonNull(base.object("EventTicket")))
    ),
    args: {
      ticketId: graphql.arg({ type: graphql.nonNull(graphql.ID) }),
    },
    async resolve(source, { ticketId }, context: Context) {
      if (!isAdminOrRoleAdminOrRoleOrganizer(context)) {
        throw accessDeniedError("You are not authenticated.");
      }

      const ticket = (await context.query.Ticket.findOne({
        where: {
          id: ticketId,
        },
        query: `
          id
          eventTicket {
            id
            type
            event {
              id
            }
          }
          booking {
            id
          }
          reservation {
            id
          }
        `,
      })) as {
        id: string;
        eventTicket: {
          id: string;
          version: number;
          type: string;
          event: {
            id: string;
          };
        };
        booking: {
          id: string;
        } | null;
        reservation: {
          id: string;
        } | null;
      } | null;

      if (!ticket) {
        throw new GraphQLError(`Ticket '${ticketId}' does not exist..`, {
          extensions: { code: "FA_TICKET_NOT_EXISTING" },
        });
      }

      const result = await context.prisma.$transaction(async (transaction) => {
        const waitinglistTicketsCount = await transaction.eventTicket.count({
          where: {
            eventId: ticket.eventTicket.event.id,
            type: {
              equals: "waitinglist",
            },
            bookedTicket: {
              isNot: null,
            },
            id: {
              not: ticket.eventTicket.id,
            },
          },
        });

        const isWaitinglistTicket = ticket.eventTicket.type === "waitinglist";
        const hasWaitinglistTickets = waitinglistTicketsCount > 0;
        const shouldBlock = hasWaitinglistTickets && !isWaitinglistTicket;

        const updatedEventTicket = await transaction.eventTicket.update({
          where: {
            bookedTicketId: ticketId,
            version: ticket.eventTicket.version,
          },
          data: {
            isBlocked: shouldBlock,
            bookedTicketId: null,
            version: {
              increment: 1,
            },
          },
        });

        await transaction.discountVoucher.updateMany({
          where: {
            ticketId: ticketId,
          },
          data: {
            ticketId: null,
          },
        });

        const deletedTicket = await transaction.ticket.update({
          where: {
            id: ticketId,
          },
          data: {
            isDeleted: true,
          },
        });

        const movedEventTickets = [];

        if (isWaitinglistTicket) {
          const waitinglistEventTickets =
            await transaction.eventTicket.findMany({
              where: {
                eventId: {
                  equals: ticket.eventTicket.event.id,
                },
                type: {
                  equals: "waitinglist",
                },
              },
              orderBy: {
                number: "asc",
              },
            });

          const sortedWaitinglistEventTickets = waitinglistEventTickets.sort(
            (a, b) => {
              if (!a.bookedTicketId) {
                if (!b.bookedTicketId) {
                  return 0;
                }

                return 1;
              }

              if (!b.bookedTicketId) {
                return -1;
              }

              return a.number - b.number;
            }
          );

          for (const [
            index,
            waitinglistEventTicket,
          ] of sortedWaitinglistEventTickets.entries()) {
            const movedIndex = index + 1;

            if (waitinglistEventTicket.number === movedIndex) {
              continue;
            }

            const movedEventTicket = await transaction.eventTicket.update({
              where: {
                id: waitinglistEventTicket.id,
                version: waitinglistEventTicket.version,
              },
              data: {
                number: movedIndex,
                version: {
                  increment: 1,
                },
              },
            });

            if (movedEventTicket.bookedTicketId) {
              movedEventTickets.push(movedEventTicket);
            }
          }
        }

        return { deletedTicket, updatedEventTicket, movedEventTickets };
      });

      if (result.deletedTicket.bookingId) {
        eventQueue().add("bookingCancelled", result.deletedTicket);
      }

      if (result.deletedTicket.reservationId) {
        eventQueue().add("reservationCancelled", result.deletedTicket);
      }

      if (result.movedEventTickets.length > 0) {
        eventQueue().addBulk(
          result.movedEventTickets.map((eventTicket) => ({
            name: "waitinglistTicketSlotAdvanced",
            data: eventTicket,
          }))
        );
      }

      const eventTickets = await context.prisma.eventTicket.findMany({
        where: {
          eventId: {
            equals: ticket.eventTicket.event.id,
          },
        },
      });

      return eventTickets;
    },
  });
