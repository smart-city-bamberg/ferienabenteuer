import { GraphQLError } from "graphql";
import { Context } from ".keystone/types";
import { graphql } from "@keystone-6/core";
import { accessDeniedError } from "../schemaExtensions";
import { generateCode } from "../../codeGenerator";
import { IMessageQueue } from "../eventQueue";

type BookCartTicketsSuccess = {
  bookings: any[];
  reservations: any[];
};

type BookCartTicketsFailed = {
  reason: string;
};

export const bookCartTickets = (
  base: graphql.BaseSchemaMeta,
  eventQueue: () => IMessageQueue
) =>
  graphql.field({
    type: graphql.nonNull(
      graphql.union({
        name: "BookCartTicketsResult",
        types: [
          graphql.object<BookCartTicketsSuccess>()({
            name: "BookCartTicketsSuccess",
            fields: {
              bookings: graphql.field({
                type: graphql.nonNull(
                  graphql.list(graphql.nonNull(base.object("Booking")))
                ),
              }),
              reservations: graphql.field({
                type: graphql.nonNull(
                  graphql.list(graphql.nonNull(base.object("Reservation")))
                ),
              }),
            },
          }),
          graphql.object<BookCartTicketsFailed>()({
            name: "BookCartTicketsFailed",
            fields: {
              reason: graphql.field({
                type: graphql.nonNull(graphql.String),
              }),
            },
          }),
        ],
      })
    ),
    args: {
      agreeDataProtection: graphql.arg({
        type: graphql.nonNull(graphql.Boolean),
      }),
      agreeTermsAndConditions: graphql.arg({
        type: graphql.nonNull(graphql.Boolean),
      }),
    },
    async resolve(
      source,
      { agreeDataProtection, agreeTermsAndConditions },
      context: Context
    ) {
      if (!context.session?.data.id) {
        throw accessDeniedError("You are not authenticated.");
      }

      const now = new Date();

      const custodian = await context.prisma.custodian.findFirst({
        where: {
          userId: {
            equals: context.session?.data.id,
          },
        },
      });

      if (!custodian) {
        throw accessDeniedError("You cannot book tickets.");
      }

      if (!agreeDataProtection) {
        return {
          __typename: "BookCartTicketsFailed",
          reason: "MustAgreeDataProtection",
        };
      }

      if (!agreeTermsAndConditions) {
        return {
          __typename: "BookCartTicketsFailed",
          reason: "MustAgreeTermsAndConditions",
        };
      }

      const cart = await context.prisma.cart.findFirst({
        where: {
          custodianId: { equals: custodian.id },
        },
      });

      if (!cart) {
        throw new GraphQLError("Your cart does not exist.", {
          extensions: { code: "FA_NO_CART" },
        });
      }

      type CartTicket = {
        id: string;
        cart: {
          id: string;
        };
        eventTicket: {
          id: string;
          type: string;
          event: {
            id: string;
          };
        };
        discountType: {
          id: string;
          discountPercent: number;
        } | null;
        child: {
          id: string;
        };
      };

      const cartTickets = (await context.query.CartTicket.findMany({
        where: {
          cart: {
            id: {
              equals: cart.id,
            },
          },
        },
        query:
          "id cart { id } eventTicket { id type event { id } } discountType { id discountPercent } child{ id }",
      })) as CartTicket[];

      if (cartTickets.some((ticket) => !ticket.child)) {
        return {
          __typename: "BookCartTicketsFailed",
          reason: "AllTicketsMustHaveChildAssigned",
        };
      }

      const groupedTickets = cartTickets.reduce(
        (groups, ticket) => ({
          ...groups,
          [ticket.eventTicket.event.id]: [
            ...(groups[ticket.eventTicket.event.id] ?? []),
            ticket,
          ],
        }),
        {} as { [index: string]: CartTicket[] }
      );

      const result = await context.prisma.$transaction(async (transaction) => {
        const createdBookings: any[] = [];
        const createdReservations: any[] = [];

        for (const group in groupedTickets) {
          const eventCartTickets = groupedTickets[group];
          const event = await transaction.event.findUnique({
            where: {
              id: group,
            },
          });

          if (!event) {
            throw new GraphQLError("The event does not exist.", {
              extensions: { code: "FA_EVENT_NOT_EXISTING" },
            });
          }

          const discounts = await transaction.discount.findMany({
            where: {
              custodianId: custodian.id,
              NOT: {
                status: "cancelled",
              },
            },
          });

          const eventCartTicketsBookable = eventCartTickets.filter(
            (ticket) =>
              ticket.eventTicket.type === "participant" &&
              (ticket.discountType === null ||
                discounts.find(
                  (discount) =>
                    discount.typeId === ticket.discountType?.id &&
                    ticket.child.id === discount.childId
                )?.status === "approved")
          );

          const reservationTickets = eventCartTickets.filter(
            (ticket) =>
              !eventCartTicketsBookable.some(
                (bookedTicket) => bookedTicket.id === ticket.id
              )
          );

          const eventCosts = event.costs;

          if (eventCartTicketsBookable.length > 0) {
            const bookingTicketsWithoutDiscount =
              eventCartTicketsBookable.filter(
                (ticket) => ticket.discountType === null
              );

            const paycode = generateCode();
            const booking = await transaction.booking.create({
              data: {
                custodianId: custodian.id,
                date: now,
                eventId: group,
                code: paycode,
              },
            });

            createdBookings.push(booking);

            for (const cartTicket of bookingTicketsWithoutDiscount) {
              const eventTicket = await transaction.eventTicket.findUnique({
                where: {
                  id: cartTicket.eventTicket.id,
                },
              });

              if (!eventTicket) {
                throw new GraphQLError("The event ticket does not exist.", {
                  extensions: { code: "FA_TICKET_NOT_EXISTING" },
                });
              }

              const bookedTicket = await transaction.ticket.create({
                data: {
                  bookingId: booking.id,
                  childId: cartTicket.child.id,
                  price: eventCosts,
                },
              });

              await transaction.eventTicket.update({
                where: {
                  id: eventTicket.id,
                  version: eventTicket.version,
                },
                data: {
                  bookedTicketId: bookedTicket.id,
                  cartTicketId: null,
                  version: {
                    increment: 1,
                  },
                },
              });

              await transaction.cartTicket.delete({
                where: {
                  id: cartTicket.id,
                },
              });
            }

            const bookingTicketsWithDiscount = eventCartTicketsBookable.filter(
              (ticket) => ticket.discountType !== null
            );

            for (const cartTicket of bookingTicketsWithDiscount) {
              const eventTicket = await transaction.eventTicket.findUnique({
                where: {
                  id: cartTicket.eventTicket.id,
                },
              });

              if (!eventTicket) {
                throw new GraphQLError("The event ticket does not exist.", {
                  extensions: { code: "FA_TICKET_NOT_EXISTING" },
                });
              }

              const discountVoucher =
                await transaction.discountVoucher.findFirst({
                  where: {
                    discount: {
                      childId: cartTicket.child.id,
                      typeId: cartTicket.discountType!.id,
                      status: { equals: "approved" },
                    },
                    ticketId: null,
                  },
                });

              if (!discountVoucher) {
                reservationTickets.push(cartTicket);
                continue;
              }

              const originalPrice = eventCosts ?? 0.0;
              const discountPercent =
                (cartTicket.discountType?.discountPercent ?? 0) / 100;
              const discountedPrice =
                originalPrice - originalPrice * discountPercent;

              const bookedTicket = await transaction.ticket.create({
                data: {
                  bookingId: booking.id,
                  childId: cartTicket.child.id,
                  price: discountedPrice,
                },
              });

              await transaction.discountVoucher.update({
                where: {
                  id: discountVoucher.id,
                },
                data: {
                  redemptionDate: now,
                  ticketId: bookedTicket.id,
                },
              });

              await transaction.eventTicket.update({
                where: {
                  id: eventTicket.id,
                  version: eventTicket.version,
                },
                data: {
                  bookedTicketId: bookedTicket.id,
                  cartTicketId: null,
                  version: {
                    increment: 1,
                  },
                },
              });

              await transaction.cartTicket.delete({
                where: {
                  id: cartTicket.id,
                },
              });
            }
          }

          if (reservationTickets.length > 0) {
            const paycode = generateCode();
            const reservation = await transaction.reservation.create({
              data: {
                custodianId: custodian.id,
                date: now,
                eventId: group,
                code: paycode,
              },
            });

            createdReservations.push(reservation);

            for (const cartTicket of reservationTickets) {
              const eventTicket = await transaction.eventTicket.findUnique({
                where: {
                  id: cartTicket.eventTicket.id,
                },
              });

              if (!eventTicket) {
                throw new GraphQLError("The event ticket does not exist.", {
                  extensions: { code: "FA_TICKET_NOT_EXISTING" },
                });
              }

              let discountVoucher;

              if (cartTicket.discountType) {
                discountVoucher = await transaction.discountVoucher.findFirst({
                  where: {
                    discount: {
                      childId: cartTicket.child.id,
                      typeId: cartTicket.discountType!.id,
                    },
                    ticketId: null,
                  },
                });

                if (!discountVoucher) {
                  return {
                    __typename: "BookCartTicketsFailed",
                    reason: "NoDiscountVoucherAvailable",
                  };
                }
              }

              const originalPrice = eventCosts ?? 0.0;
              const discountPercent =
                (cartTicket.discountType?.discountPercent ?? 0) / 100;
              const discountedPrice =
                originalPrice - originalPrice * discountPercent;

              const reservedTicket = await transaction.ticket.create({
                data: {
                  reservationId: reservation.id,
                  childId: cartTicket.child.id,
                  price: discountedPrice,
                },
              });

              if (discountVoucher) {
                await transaction.discountVoucher.update({
                  where: {
                    id: discountVoucher.id,
                  },
                  data: {
                    redemptionDate: now,
                    ticketId: reservedTicket.id,
                  },
                });
              }

              await transaction.eventTicket.update({
                where: {
                  id: eventTicket.id,
                  version: eventTicket.version,
                },
                data: {
                  bookedTicketId: reservedTicket.id,
                  cartTicketId: null,
                  version: {
                    increment: 1,
                  },
                },
              });

              await transaction.cartTicket.delete({
                where: {
                  id: cartTicket.id,
                },
              });
            }
          }
        }

        await transaction.cart.delete({
          where: {
            id: cart.id,
          },
        });

        return {
          __typename: "BookCartTicketsSuccess",
          bookings: createdBookings,
          reservations: createdReservations,
        };
      });

      if (result.__typename === "BookCartTicketsSuccess") {
        if (result.bookings) {
          eventQueue().addBulk(
            result.bookings.map((booking) => ({
              name: "bookingCreated",
              data: booking,
            }))
          );
        }

        if (result.reservations) {
          eventQueue().addBulk(
            result.reservations.map((reservation) => ({
              name: "reservationCreated",
              data: reservation,
            }))
          );
        }
      }

      return result;
    },
  });
