import { Queue } from "bullmq";
import { Messages } from "../messages";

const queueName = process.env.BULLMQ_QUEUE ?? "events";
const queueHost = process.env.BULLMQ_HOST ?? "localhost";
const queuePort = parseInt(process.env.BULLMQ_PORT ?? "6379");

export interface MessageOptions {
  delay?: number;
}

export interface IMessageQueue {
  add(name: Messages, data: any, opts?: MessageOptions): Promise<void>;

  addBulk(
    messages: {
      name: Messages;
      data: any;
      opts?: MessageOptions;
    }[]
  ): Promise<void>;
}

export class MessageQueue implements IMessageQueue {
  constructor(private queue: Queue) {}

  async add(
    name: Messages,
    data: any,
    opts?: MessageOptions | undefined
  ): Promise<void> {
    await this.queue.add(name, data, opts);
  }

  async addBulk(
    messages: { name: Messages; data: any; opts?: MessageOptions | undefined }[]
  ): Promise<void> {
    await this.queue.addBulk(messages);
  }
}

export const buildEventQueue = () =>
  new Queue(queueName, {
    connection: {
      host: queueHost,
      port: queuePort,
    },
  });
