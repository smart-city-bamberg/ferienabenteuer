# Ferienabenteuer CMS

Dies ist das Backend-CMS für "Ferienabenteuer - Ferienangebote einfach online buchen" auf Basis von Keystone 6 (https://github.com/keystonejs/keystone).

![Alt text](docs/cms-dashboard.jpg)

---

## Systemanforderungen

- PostgreSQL 
- Docker

---

## Lokale Entwicklung

Für Entwicklungszwecke muss die Datenbank selbst bereitgestellt werden.
Folgende Schritte sind erforderlich, um das CMS ohne Docker zu Entwicklungs\-zwecken zu starten:

1. Installiere alle Abhängigkeiten:

```bash
yarn install
```

1. In Postgres die Datenbank `keystone` mit dem Nutzer `keystone` und Passwort `password` anlegen

2. (optional) bei abweichender Konfiguration von Postgres eine neue Datei namens `process.env` anlegen und die angepasste Datenbank-URL als Umgebungsvariable einfügen:

```bash
process.env.DATABASE_URL=postgres://keystone:password@localhost:5432/keystone
```

3. Starte das CMS:

```bash
yarn run dev
```

4. Das CMS ist nun unter `http://localhost:3000` erreichbar.

---

## Docker-Bereitstellung

Das Projekt kann mithilfe von Docker Compose bereitgestellt werden. Dabei werden sowohl das CMS als auch die Datenbank in einem Docker-Container erstellt.

⚠️ Für den produktiven Einsatz sollten die Zugangsdaten für die Datenbank unbedingt geändert werden.

In der Datei `docker-compose.override.yml` können die Zugangsdaten und weitere Einstellungen wie der öffentliche Hostname angepasst werden:

```
version: '3.4'

services:
  keystone:    
    environment:      
      - HOST_PUBLIC_NAME=http://localhost:3000      
      - DATABASE_URL=postgres://keystone:password@db:5432/keystone
      - CORS_ORIGIN=http://localhost:4200      
    volumes:
      - ./public:/storage
    ports:
      - "3000:3000"

  db:    
    environment:
      - POSTGRES_USER=keystone
      - POSTGRES_PASSWORD=password
      - POSTGRES_DB=keystone
    volumes:
      - ./data:/var/lib/postgresql/data:rw
    ports:
      - "5432:5432"

```

Um Docker Compose aufzurufen, führe den folgenden Befehl aus:

```bash
docker compose -f docker-compose.yml -f docker-compose.override.yml up -d
```

Das CMS ist nun unter `http://localhost:3000` erreichbar, Posgres unter `localhost:5432`

---

## Administrator-Nutzer erstellen

Um das CMS verwenden zu können, muss ein Administrator-Nutzer angelegt werden. Dafür sind folgende Schritte erforderlich:

1. Öffne das CMS in einem Webbrowser unter `http://localhost:3000`.
2. Registriere dich als neuer Benutzer.

Nachdem du den Administrator-Nutzer erstellt hast, kannst du dich mit den entsprechenden Zugangsdaten anmelden und das CMS vollständig nutzen.

---

## GraphQL Explorer

Unter http://localhost:3000/api/graphql kann der Apollo GraphQL Explorer geöffnet werden. Hier lässt sich die Schnittstelle erforschen und ausprobieren:

![Alt text](docs/cms-graphql-apollo.jpg)

---

## Entitäten

### Benutzer (User)
- Diese Entität repräsentiert Benutzer im System. Sie enthält Informationen wie Name, E-Mail-Adresse und Passwort. Zusätzlich können Administratoren den Anmeldestatus, die Rolle und weitere relevante Details verwalten.

### Benutzeraktivierungstoken (UserVerificationToken)
- Diese Entität speichert Tokens für die Aktivierung von Benutzerkonten. Jedes Token ist mit einem Benutzer verknüpft und hat ein Erstellungsdatum sowie ein Ablaufdatum.

### Passwort-Zurücksetzungstoken (PasswordResetToken)
- Diese Entität verwaltet Tokens, die für das Zurücksetzen von Benutzerpasswörtern verwendet werden. Sie enthält Informationen über den Benutzer, das Token selbst sowie Erstellungs- und Ablaufdaten.

### Saison (Season)
- Eine Saison repräsentiert einen bestimmten Zeitraum im System, möglicherweise für Veranstaltungen oder andere zeitgebundene Aktivitäten. Sie enthält Informationen wie das Jahr, den Buchungsstart, das Veröffentlichungsdatum und die Zuschusshöhe.

### Ferien (Holiday)
- Diese Entität verwaltet Informationen zu Ferienzeiträumen. Sie enthält einen Namen und eine Liste von Zeiträumen, die mit den Ferien verbunden sind.

### Zeitraum (Period)
- Ein Zeitraum repräsentiert einen bestimmten Zeitraum innerhalb einer Saison oder eines Ferienzeitraums. Sie enthält Start- und Enddaten sowie Informationen zu Betreuungstagen.

### Veranstaltung (Event)
- Diese Entität repräsentiert Veranstaltungen im System. Sie enthält Informationen wie den Saisonbezug, den Status, den Namen, das Bild, den Veranstalter, die Beschreibung, die Verpflegung, die Kategorien, die Betreuungszeit, das Alter der Teilnehmer, die Teilnehmerzahl, die Wartelistenkapazität, den Veranstaltungsort, den Kontakt, die Kosten und die Anmeldefrist.

### Veranstaltungskategorie (EventCategory)
- Diese Entität verwaltet Veranstaltungskategorien. Jede Kategorie hat einen eindeutigen Namen und kann mit mehreren Veranstaltungen verbunden sein.

### Betreuungstage (CareDay)
- Diese Entität repräsentiert die Tage, an denen Betreuung stattfindet. Sie enthält Informationen über den Tag selbst sowie eine Verknüpfung zu Betreuungszeiträumen.

### Betreuungszeiträume (CarePeriod)
- Diese Entität verwaltet die Zeiträume, in denen Betreuung stattfindet. Sie enthält Informationen über den Zeitraum, die Veranstaltung, die Betreuungstage und einen virtuellen Anzeigenamen, der aus dem Zeitraum und den Betreuungstagen generiert wird.

### Veranstalter (Organizer)
- Diese Entität repräsentiert Veranstalter im System. Sie enthält Informationen wie den Namen, die Ansprechperson, die E-Mail, die Telefonnummer, die Adresse, die Bankverbindung sowie Verknüpfungen zu Veranstaltungen und Benutzern.

### Partner
- Diese Entität repräsentiert Partnerorganisationen. Sie enthält Informationen wie den Namen, die Ansprechperson, die E-Mail, die Telefonnummer und Verknüpfungen zu Frühbuchercodes.

### Frühbuchercodes (EarlyAccessCode)
- Diese Entität verwaltet Frühbuchercodes. Sie enthält Informationen wie den Code, den zugehörigen Partner, die Saison und das Datum der Benachrichtigung.

### Eingelöste Frühbuchercodes (EarlyAccessCodeVoucher)
- Diese Entität verwaltet eingelöste Frühbuchercodes. Sie enthält Informationen wie den Code, die zugehörige Saison und den Sorgeberechtigten.

### Sorgeberechtigte (Custodian)
- Diese Entität repräsentiert die Sorgeberechtigten, d.h. die Erziehungsberechtigten der Kinder. Sie enthält Informationen wie die Anrede, den Vornamen, den Nachnamen, die Telefonnummer, die Notfallnummer, die Adresse und Verknüpfungen zu Benutzern, Kindern, Buchungen, Reservierungen und Ermäßigungen.

### Kinder (Child)
- Diese Entität repräsentiert die Kinder, für die Sorgeberechtigte verantwortlich sind. Sie enthält Informationen wie den Vornamen, den Nachnamen, das Geburtsdatum, das Geschlecht, die Krankenversicherung, Angaben zur Haftpflichtversicherung, zur benötigten Assistenz, zu eventuellen körperlichen Beeinträchtigungen, zu Medikamenten, zu Allergien und Unverträglichkeiten sowie zu sonstigen Anmerkungen. Außerdem gibt es Verknüpfungen zu Tickets und Ermäßigungen.

### Reservierungen (Reservation)
- Diese Entität repräsentiert die Reservierungen von Tickets für Veranstaltungen. Sie enthält Informationen wie den Code, den Preis, die Veranstaltung, den Sorgeberechtigten, die reservierten Tickets und das Reservierungsdatum. Außerdem gibt es eine Angabe, ob die Reservierung gelöscht wurde.

### Buchungen (Booking)
- Diese Entität repräsentiert Buchungen für Veranstaltungen. Sie enthält Informationen wie den Code, den Preis, den Status, die Veranstaltung, den Sorgeberechtigten, die gebuchten Tickets, das Buchungsdatum, das Datum der Erinnerung und ob die Buchung gelöscht wurde.

### Tickets (Ticket)
- Diese Entität repräsentiert Tickets für Veranstaltungen. Sie enthält Informationen wie die Buchung, die Reservierung, das Event-Ticket, das Kind, den Ermäßigungsschein, den Preis und ob das Ticket gelöscht wurde.

### Ermäßigungen (Discount)
- Diese Entität repräsentiert Ermäßigungen, die für Kinder angeboten werden. Sie enthält Informationen wie den Status, den Typ, das Kind, die Ermäßigungsscheine, das Beantragungsdatum, den Ablehnungsgrund, die Gültigkeit, das Datum der Erinnerung und den Sorgeberechtigten. Außerdem gibt es eine virtuelle Anzeige mit Details zu Sorgeberechtigten, Kindern, Ermäßigungstyp, und genutzten Ermäßigungsscheinen.

### Ablehnungsgründe für Ermäßigungen (DiscountRejectionReason)
- Diese Entität enthält Gründe für die Ablehnung von Ermäßigungen. Sie enthält Informationen wie den Grund und die Erläuterung.

### Ermäßigungstypen (DiscountType)
- Diese Entität enthält verschiedene Typen von Ermäßigungen. Sie enthält Informationen wie den Namen, den Rabatt in Prozent, die Anzahl der Veranstaltungen, die Anzahl der Kinder, die Beschreibungen zur Anzahl der Veranstaltungen und zur Anzahl der Kinder.

### Ermäßigungsscheine (DiscountVoucher)
- Diese Entität repräsentiert Ermäßigungsscheine, die für bestimmte Ermäßigungen ausgestellt wurden. Sie enthält Informationen wie die Ermäßigung, das Ticket, das Einlösungsdatum und eine virtuelle Anzeige mit Details zur Ermäßigung und zum Einlösungsstatus.

### Veranstaltungstickets (EventTicket)
- Diese Entität repräsentiert Tickets für Veranstaltungen. Sie enthält Informationen wie die Veranstaltung, die Version, die Nummer, den Typ, den Code, das zugehörige Warenkorbticket, das gebuchte Ticket, den Blockierungsstatus und eine virtuelle Anzeige mit Details zur Veranstaltung und zum Buchungsstatus.

### Warenkorbtickets (CartTicket)
- Diese Entität repräsentiert Tickets im Warenkorb. Sie enthält Informationen wie den Warenkorb, das zugehörige Veranstaltungsticket, den Ermäßigungstyp, das Kind, das Hinzufügungsdatum zum Warenkorb und eine virtuelle Anzeige mit Details zur Veranstaltung und zum Warenkorbinhaber.

### Warenkörbe (Cart)
- Diese Entität repräsentiert Warenkörbe von Benutzern. Sie enthält Informationen wie den Warenkorbinhaber, die enthaltenen Warenkorbtickets, das Erstellungsdatum, das Ablaufdatum, die Version, die Anzahl der Laufzeitverlängerungen und eine virtuelle Anzeige mit Details zum Warenkorbinhaber und zum Ablaufdatum.

### Kontakte (Contact)
- Diese Entität repräsentiert Kontakte von Institutionen. Sie enthält Informationen wie den Namen der Institution, eine Beschreibung, den Vor- und Nachnamen der Ansprechperson, die Adresse, die E-Mail-Adresse, die Telefonnummer und ob der Kontakt versteckt ist.

### Abrechnungen (Invoice)
- Diese Entität repräsentiert Abrechnungen für Veranstaltungen. Sie enthält Informationen wie die Veranstaltung, das Erstellungsdatum und das Dokument.

### Email Vorlagen (EmailTemplate)
- Diese Entität repräsentiert E-Mail-Vorlagen für verschiedene Ereignisse. Sie enthält Informationen wie das Ereignis, den Betreff, den Vorlagentext und eine Liste der BCC-Empfänger.

#### Ereignisse
- Benutzer registriert
- Konto verifiziert
- Teilnehmerplatz reserviert
- Ticket storniert
- Warteplatz reserviert
- Warteplatz in Warteliste vorgerückt
- Warteplatz in Teilnehmerliste nachgerückt
- Warteplatz in Teilnehmerliste nachgerückt (Reservierung)
- Buchung eingegangen
- Buchung bestätigt
- Buchung storniert
- Buchung Assistenz benötigt
- Reservierung storniert
- Veranstaltung abgesagt
- Veranstaltung beendet
- Ermäßigung beantragt
- Ermäßigung gewährt
- Ermäßigung abgelehnt
- Ermäßigungsnachweis Erinnerung
- Zahlung erfasst
- Zahlung überfällig
- Frühbuchercode senden
- Passwort zurücksetzen
- Konto wird bald gelöscht wegen Inaktivität
- Konto gelöscht
- Konto gelöscht wegen Inaktivität
