import {
  FieldContainer,
  FieldLabel,
  TextInput,
  DatePicker,
} from "@keystone-ui/fields";
import {
  FieldController,
  FieldControllerConfig,
  FieldProps,
} from "@keystone-6/core/types";
import React from "react";

export const controller = (
  config: FieldControllerConfig
): FieldController<string, string> => {
  return {
    description: "",
    path: config.path,
    label: config.label,
    graphqlSelection: config.path,
    defaultValue: "2023-01-01",
    deserialize: (data) => {
      const date: Date = new Date(data[config.path]) ?? new Date();

      const year = date.getFullYear();
      const month = (date.getMonth() + 1).toString().padStart(2, "0");
      const day = date.getDate().toString().padStart(2, "0");

      return `${day}.${month}.${year}`;
    },
    serialize: (value) => {
      if (value === "") {
        return {
          [config.path]: null,
        };
      }

      const parts = value.split(".");

      if (parts.length <= 1) {
        return {
          [config.path]: null,
        };
      }

      const day = parts[0].toString().padStart(2, "0");
      const month = parts[1].toString().padStart(2, "0");
      const year = parts[2];

      const output = `${year}-${month}-${day}`;

      return {
        [config.path]: output,
      };
    },
  };
};

export const Field = ({
  field,
  value,
  onChange,
  autoFocus,
}: FieldProps<typeof controller>) => (
  <FieldContainer key="0">
    <FieldLabel htmlFor={field.path}>{field.label}</FieldLabel>
    {onChange ? (
      <TextInput
        id={field.path}
        autoFocus={autoFocus}
        type="text"
        value={value}
        key="0"
        onChange={(event) => {
          onChange(event.target.value);
        }}
      />
    ) : (
      value
    )}
  </FieldContainer>
);
