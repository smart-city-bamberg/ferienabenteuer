import crypto from "crypto";

export const characters = "ACDEFGHKLMNPQRSTUVWXYZ123456789";

export const generateCode = (length: number = 10) =>
  Array.from({ length }).reduce<string>(
    (sum) => sum + characters[crypto.randomInt(0, characters.length)],
    ""
  );

export const earlyAccessCharacters =
  "ACDEFGHKLMNPQRSTUVWXYZacdefghkmnpqrstuvwxyz";

export const generateEarlyAccessCode = () => {
  const firstPart = Array.from({ length: 4 }).reduce<string>(
    (sum) =>
      sum +
      earlyAccessCharacters[crypto.randomInt(0, earlyAccessCharacters.length)],
    ""
  );

  const secondPart = Array.from({ length: 4 }).reduce<string>(
    (sum) => `${sum}${crypto.randomInt(0, 9)}`,
    ""
  );

  return `${firstPart}-${secondPart}`;
};
