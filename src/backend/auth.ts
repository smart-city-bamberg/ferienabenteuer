import { Context } from ".keystone/types";
import { createHash } from "crypto";
import { createAuth } from "@keystone-6/auth";
import { statelessSessions } from "@keystone-6/core/session";
import { SessionStrategy } from "@keystone-6/core/types";

let sessionSecret = process.env.SESSION_SECRET;
if (!sessionSecret && process.env.NODE_ENV !== "production") {
  sessionSecret = createHash("sha256").update("devSessionSecret").digest("hex");
}

const { withAuth } = createAuth({
  listKey: "User",
  identityField: "email",
  sessionData:
    "id name role organizer { id } isAdministrator isEnabled isTester",
  secretField: "password",
  initFirstItem: {
    fields: ["name", "email", "password"],
    skipKeystoneWelcome: true,
    itemData: {
      isAdministrator: true,
    },
  },
});

const sessionMaxAge = 60 * 60 * 24 * 30;

const stateless = statelessSessions({
  maxAge: sessionMaxAge,
  secret: sessionSecret!,
});

const sessionStrategy = (
  strategy: SessionStrategy<any, any>
): SessionStrategy<any, any> => {
  return {
    async get({ context }) {
      return strategy.get({ context });
    },
    async end({ context }) {
      return strategy.end({ context });
    },
    async start({ context, data }) {
      const typedContext = (context as Context).sudo();
      await typedContext.db.User.updateOne({
        where: {
          id: data.itemId,
        },
        data: {
          lastLogin: new Date(),
          markedForDeletionAt: null,
        },
      });

      return strategy.start({ context, data });
    },
  };
};

const session = sessionStrategy(stateless);

export { withAuth, session };
