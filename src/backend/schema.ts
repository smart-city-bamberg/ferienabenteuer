import { graphql, group, list } from "@keystone-6/core";
import { allowAll } from "@keystone-6/core/access";

import {
  text,
  password,
  timestamp,
  select,
  relationship,
  calendarDay,
  integer,
  float,
  checkbox,
  virtual,
  image,
  file,
} from "@keystone-6/core/fields";

import type { Lists } from ".keystone/types";
import {
  isAdmin,
  isAuthenticated,
  isAdminOrRoleAdmin,
  isAdminOrRoleAdminOrRoleOrganizer,
  isAdminOrRoleAdminOrRoleCustodian,
  isRoleCustodian,
  isAdminOrRoleAdminOrRoleOrganizerOrRoleCustodian,
  isRoleOrganizer,
} from "./auth.filters";
import { generateCode } from "./codeGenerator";

const ticketType = graphql.enum({
  name: "TicketType",
  values: graphql.enumValues(["participant", "waitinglist"]),
});

export const lists: Lists = {
  User: list({
    access: {
      operation: {
        create: isAdmin,
        delete: isAdmin,
        query: isAuthenticated,
        update: isAdmin,
      },
      filter: {
        query: (args) => {
          if (isAdmin({ session: args.session })) {
            return true;
          }

          if (isRoleOrganizer(args.context)) {
            return true;
          }

          if (!isAuthenticated(args.context)) {
            return false;
          }

          return {
            id: {
              equals: args.session.data.id,
            },
          };
        },
      },
    },
    ui: {
      label: "👥 Benutzer",
    },
    fields: {
      name: text({ label: "🏷️ Name", validation: { isRequired: true } }),
      email: text({
        label: "📧 E-Mail",
        validation: { isRequired: true },
        isIndexed: "unique",
      }),
      password: password({
        label: "🗝️ Passwort",
        validation: { isRequired: true },
        access: {
          read: (args) => {
            if (isAdminOrRoleAdmin(args.context)) {
              return true;
            }

            if (args.item.id === args.context.session?.data?.id) {
              return true;
            }

            return false;
          },
        },
      }),
      createdAt: timestamp({
        label: "📅 Registriert",
        defaultValue: { kind: "now" },
        access: {
          read: (args) => {
            if (isAdminOrRoleAdmin(args.context)) {
              return true;
            }

            if (args.item.id === args.context.session?.data?.id) {
              return true;
            }

            return false;
          },
        },
      }),
      lastLogin: timestamp({
        label: "📅 Letzte Anmeldung",
        access: {
          read: (args) => {
            if (isAdminOrRoleAdmin(args.context)) {
              return true;
            }

            if (args.item.id === args.context.session?.data?.id) {
              return true;
            }

            return false;
          },
        },
      }),
      markedForDeletionAt: timestamp({
        label: "📅 Über Löschung informiert",
        access: {
          read: (args) => {
            if (isAdminOrRoleAdmin(args.context)) {
              return true;
            }

            if (args.item.id === args.context.session?.data?.id) {
              return true;
            }

            return false;
          },
        },
      }),
      isAdministrator: checkbox({
        label: "👑 Administrator",
        access: {
          create: isAdmin,
          update: isAdmin,
          read: (args) => isAdminOrRoleAdmin(args.context),
        },
      }),
      role: select({
        label: "🏆 Rolle",
        access: {
          read: (args) => {
            if (isAdminOrRoleAdmin(args.context)) {
              return true;
            }

            if (args.item.id === args.context.session?.data?.id) {
              return true;
            }

            return false;
          },
        },
        options: [
          {
            label: "Administrator",
            value: "administrator",
          },
          {
            label: "Veranstalter",
            value: "organizer",
          },
          {
            label: "Sorgeberechtigter",
            value: "custodian",
          },
        ],
      }),
      organizer: relationship({
        label: "🗓️ Veranstalter",
        ref: "Organizer.users",
        access: {
          read: (args) => {
            if (isAdminOrRoleAdmin(args.context)) {
              return true;
            }

            if (args.item.id === args.context.session?.data?.id) {
              return true;
            }

            return false;
          },
        },
      }),
      isEnabled: checkbox({
        label: "⭐ Ist aktiviert",
        defaultValue: false,
        access: {
          read: (args) => {
            if (isAdminOrRoleAdmin(args.context)) {
              return true;
            }

            if (args.item.id === args.context.session?.data?.id) {
              return true;
            }

            return false;
          },
        },
      }),
      isTester: checkbox({
        label: "🤖 Ist Tester",
        defaultValue: false,
        access: {
          read: (args) => {
            if (isAdminOrRoleAdmin(args.context)) {
              return true;
            }

            if (args.item.id === args.context.session?.data?.id) {
              return true;
            }

            return false;
          },
        },
      }),
    },
  }),
  UserVerificationToken: list({
    access: {
      operation: {
        create: isAdmin,
        delete: isAdmin,
        query: isAuthenticated,
        update: isAdmin,
      },
      filter: {
        query: (args) => {
          if (isAdmin({ session: args.session })) {
            return true;
          }

          return false;
        },
      },
    },
    ui: {
      label: "🗝️ Benutzer Aktivierung",
    },
    fields: {
      user: relationship({
        label: "👤 Benutzer",
        ref: "User",
      }),
      validationToken: text({
        label: "🗝️ Validierungs Token",
        isIndexed: "unique",
      }),
      createdAt: timestamp({
        label: "📅 Erzeugt am",
        validation: {
          isRequired: true,
        },
      }),
      validUntil: timestamp({
        label: "📅 Gültig bis",
        validation: {
          isRequired: true,
        },
      }),
    },
  }),
  PasswordResetToken: list({
    access: {
      operation: {
        create: isAdmin,
        delete: isAdmin,
        query: isAuthenticated,
        update: isAdmin,
      },
      filter: {
        query: (args) => {
          if (isAdmin({ session: args.session })) {
            return true;
          }

          return false;
        },
      },
    },
    ui: {
      label: "🗝️ Benutzer Passwort Reset",
    },
    fields: {
      user: relationship({
        label: "👤 Benutzer",
        ref: "User",
      }),
      validationToken: text({
        label: "🗝️ Validierungs Token",
        isIndexed: "unique",
      }),
      createdAt: timestamp({
        label: "📅 Erzeugt am",
        validation: {
          isRequired: true,
        },
      }),
      validUntil: timestamp({
        label: "📅 Gültig bis",
        validation: {
          isRequired: true,
        },
      }),
    },
  }),
  Season: list({
    access: {
      operation: {
        create: isAdminOrRoleAdmin,
        delete: isAdminOrRoleAdmin,
        query: allowAll,
        update: isAdminOrRoleAdmin,
      },
    },
    graphql: {
      cacheHint: () => ({
        maxAge: 60,
        scope: "PUBLIC",
      }),
    },
    ui: {
      label: "🏆 Saisons",
      labelField: "year",
    },
    fields: {
      year: integer({
        label: "📅 Jahr",
        isIndexed: "unique",
      }),
      bookingStart: timestamp({
        label: "📅 Buchungsstart Allgemein",
      }),
      bookingStartPartners: timestamp({
        label: "📅 Buchungsstart Partner",
      }),
      releaseDate: timestamp({
        label: "📅 Veröffentlichungsdatum",
      }),
      grantAmount: float({
        label: "🪙 Zuschusshöhe",
        validation: {
          min: 0,
        },
        access: {
          read: isAdminOrRoleAdminOrRoleOrganizer,
        },
      }),
      period: relationship({
        ref: "Period.season",
        many: true,
      }),
    },
  }),
  Holiday: list({
    ui: {
      label: "🗓️ Ferien",
    },
    access: {
      operation: {
        create: isAdminOrRoleAdmin,
        delete: isAdminOrRoleAdmin,
        query: allowAll,
        update: isAdminOrRoleAdmin,
      },
    },
    graphql: {
      cacheHint: () => ({
        maxAge: 60,
        scope: "PUBLIC",
      }),
    },
    fields: {
      name: text({
        label: "🏷️ Name",
      }),
      periods: relationship({
        label: "🗓️ Zeiträume",
        ref: "Period.holiday",
        many: true,
        ui: {
          labelField: "displayName",
        },
        graphql: {
          cacheHint: {
            maxAge: 60,
            scope: "PUBLIC",
          },
        },
      }),
    },
  }),
  Period: list({
    access: {
      operation: {
        create: isAdminOrRoleAdmin,
        delete: isAdminOrRoleAdmin,
        query: allowAll,
        update: isAdminOrRoleAdmin,
      },
    },
    graphql: {
      cacheHint: () => ({
        maxAge: 60,
        scope: "PUBLIC",
      }),
    },
    ui: {
      label: "🗓️ Zeitraum",
      listView: {
        initialColumns: ["season", "holiday", "startDate", "endDate"],
        initialSort: {
          field: "startDate",
          direction: "ASC",
        },
      },
      labelField: "displayNameWithHoliday",
    },
    fields: {
      season: relationship({
        label: "🏆 Saison",
        ref: "Season.period",
        graphql: {
          cacheHint: {
            maxAge: 60,
            scope: "PUBLIC",
          },
        },
      }),
      holiday: relationship({
        label: "📅 Ferien",
        ref: "Holiday.periods",
        graphql: {
          cacheHint: {
            maxAge: 60,
            scope: "PUBLIC",
          },
        },
      }),
      startDate: calendarDay({
        label: "📅 Von",
        validation: {
          isRequired: true,
        },
        ui: {
          views: "./views/date",
        },
      }),
      endDate: calendarDay({
        label: "📅 Bis",
        validation: {
          isRequired: true,
        },
        ui: {
          views: "./views/date",
        },
      }),
      carePeriods: relationship({
        label: "🗓️ Betreuungstage",
        ref: "CarePeriod.period",
        many: true,
        graphql: {
          cacheHint: {
            maxAge: 60,
            scope: "PUBLIC",
          },
        },
      }),
      displayName: virtual({
        ui: {
          createView: {
            fieldMode: "hidden",
          },
          itemView: {
            fieldMode: "hidden",
          },
          listView: {
            fieldMode: "read",
          },
        },
        field: graphql.field({
          type: graphql.String,
          resolve: async (item, args, context) =>
            `${item.startDate.getDate().toString().padStart(2, "0")}.${(
              item.startDate.getMonth() + 1
            )
              .toString()
              .padStart(
                2,
                "0"
              )}.${item.startDate.getFullYear()} - ${item.endDate
              .getDate()
              .toString()
              .padStart(2, "0")}.${(item.endDate.getMonth() + 1)
              .toString()
              .padStart(2, "0")}.${item.endDate.getFullYear()}`,
        }),
      }),
      displayNameWithHoliday: virtual({
        ui: {
          createView: {
            fieldMode: "hidden",
          },
          itemView: {
            fieldMode: "hidden",
          },
          listView: {
            fieldMode: "read",
          },
        },
        field: graphql.field({
          type: graphql.String,
          resolve: async (item, args, context) => {
            const holiday = (await context.db.Holiday.findOne({
              where: { id: item.holidayId },
            })) as Lists.Holiday.Item;

            return `${holiday.name} (${item.startDate.getDate()}.${
              item.startDate.getMonth() + 1
            }.${item.startDate.getFullYear()} - ${item.endDate.getDate()}.${
              item.endDate.getMonth() + 1
            }.${item.endDate.getFullYear()})`;
          },
        }),
      }),
    },
  }),
  Event: list({
    access: {
      operation: {
        create: isAdminOrRoleAdminOrRoleOrganizer,
        delete: isAdminOrRoleAdminOrRoleOrganizer,
        query: allowAll,
        update: isAdminOrRoleAdminOrRoleOrganizer,
      },
      filter: {
        delete: ({ context, session }) => {
          if (isAdminOrRoleAdmin(context)) {
            return true;
          }

          if (isRoleOrganizer(context)) {
            return {
              organizer: {
                id: {
                  equals: session?.data?.organizer?.id,
                },
              },
            };
          }

          return false;
        },
        query: ({ context, session }) => {
          if (isAdminOrRoleAdmin(context)) {
            return true;
          }

          if (isRoleOrganizer(context)) {
            return {
              OR: [
                {
                  organizer: {
                    id: {
                      equals: session?.data?.organizer?.id,
                    },
                  },
                },
                {
                  status: {
                    equals: "approved",
                  },
                },
              ],
            };
          }

          return {
            status: {
              equals: "approved",
            },
          };
        },
      },
    },
    ui: {
      label: "♾️ Veranstaltungen",
      listView: {
        initialColumns: [
          "status",
          "committedPeriod",
          "name",
          "organizer",
          "location",
          "costs",
        ],
      },
    },
    graphql: {
      cacheHint: () => ({
        maxAge: 60,
        scope: "PUBLIC",
      }),
    },
    fields: {
      season: relationship({
        ref: "Season",
        label: "🏆 Saison",
      }),
      status: select({
        label: "🔢 Status",
        options: [
          {
            label: "Neu",
            value: "new",
          },
          {
            label: "Prüfung",
            value: "check",
          },
          {
            label: "Geplant",
            value: "planned",
          },
          {
            label: "Freigegeben",
            value: "approved",
          },
          {
            label: "Abgesagt",
            value: "cancelled",
          },
        ],
        hooks: {
          resolveInput(args) {
            if (args.operation === "update") {
              return args.resolvedData.status;
            }

            return "new";
          },
        },
      }),
      name: text({
        label: "🏷️ Titel",
      }),
      picture: image({
        label: "🖼️ Bild",
        storage: "image_store",
        graphql: {
          cacheHint: {
            maxAge: 60,
            scope: "PUBLIC",
          },
        },
      }),
      pictureRights: text({
        label: "🏷️ Bildrechte",
      }),
      organizer: relationship({
        label: "🧑‍🏭 Veranstalter",
        ref: "Organizer.events",
        ui: {
          labelField: "name",
        },
        graphql: {
          cacheHint: {
            maxAge: 60,
            scope: "PUBLIC",
          },
        },
      }),
      description: text({
        label: "📄 Beschreibung",
      }),
      hints: text({
        label: "📄 Hinweise",
      }),
      meals: text({
        label: "📄 Verpflegung",
      }),

      categories: relationship({
        label: "🏳️‍🌈 Kategorien",
        ref: "EventCategory.events",
        many: true,
        graphql: {
          cacheHint: {
            maxAge: 60,
            scope: "PUBLIC",
          },
        },
      }),

      carePeriodDesired: relationship({
        label: "🗓️ Wunschtermin",
        ref: "CarePeriod",
        access: {
          read: isAdminOrRoleAdminOrRoleOrganizer,
        },
      }),
      carePeriodAlternative: relationship({
        label: "🗓️ Ausweichtermin",
        ref: "CarePeriod",
        access: {
          read: isAdminOrRoleAdminOrRoleOrganizer,
        },
      }),
      carePeriodCommitted: relationship({
        label: "🗓️ Termin",
        ref: "CarePeriod.event",
        graphql: {
          cacheHint: {
            maxAge: 60,
            scope: "PUBLIC",
          },
        },
      }),

      ...group({
        label: "Betreuungszeit",
        fields: {
          careTimeStart: text({
            label: "⏰ Von",
          }),
          careTimeEnd: text({
            label: "⏰ Bis",
          }),
        },
      }),

      ...group({
        label: "Alter",
        fields: {
          ageMinimum: integer({
            label: "➡️ Alter Von",
            validation: {
              min: 0,
            },
          }),
          ageMaximum: integer({
            label: "⬅️ Alter Bis",
            validation: {
              min: 0,
            },
          }),
        },
      }),

      participantMinimum: integer({
        label: "⬇️ Min Anzahl Teilnehmer",
        validation: {
          min: 0,
        },
      }),
      participantLimit: integer({
        label: "⬆️ Max. Anzahl Teilnehmer",
        validation: {
          min: 0,
        },
      }),
      waitingListLimit: integer({
        label: "⬆️ Max. Anzahl auf Warteliste",
        validation: {
          min: 0,
        },
      }),

      availableTicketCount: virtual({
        label: "#️⃣ Verfügbare Tickets",
        graphql: {
          cacheHint: {
            maxAge: 60,
            scope: "PUBLIC",
          },
        },
        field: graphql.field({
          args: {
            type: graphql.arg({
              type: ticketType,
            }),
          },
          type: graphql.Int,
          resolve: async (item, args, context) => {
            const sudoContext = context.sudo();
            return await sudoContext.db.EventTicket.count({
              where: {
                event: {
                  id: {
                    equals: item.id,
                  },
                },
                bookedTicket: null,
                cartTicket: null,
                type: {
                  equals: args.type,
                },
                isBlocked: {
                  equals: false,
                },
              },
            });
          },
        }),
      }),

      bookedTicketCount: virtual({
        label: "#️⃣ Gebuchte Tickets",
        access: {
          read: isAdminOrRoleAdminOrRoleOrganizer,
        },
        graphql: {
          cacheHint: {
            maxAge: 60,
            scope: "PUBLIC",
          },
        },
        field: graphql.field({
          args: {
            type: graphql.arg({
              type: ticketType,
            }),
          },
          type: graphql.Int,
          resolve: async (item, args, context) => {
            const sudoContext = context.sudo();
            return await sudoContext.db.EventTicket.count({
              where: {
                event: {
                  id: {
                    equals: item.id,
                  },
                },
                NOT: {
                  bookedTicket: null,
                  cartTicket: null,
                },
                type: {
                  equals: args.type,
                },
                isBlocked: {
                  equals: false,
                },
              },
            });
          },
        }),
      }),

      ...group({
        label: "📍Veranstaltungsort",
        fields: {
          locationCity: text({
            label: "🗒️ Stadt",
          }),
          locationZipcode: text({
            label: "🔢 PLZ",
          }),
          locationStreet: text({
            label: "🗒️ Straße & Hausnummer",
          }),
        },
      }),

      ...group({
        label: "Kontakt",
        fields: {
          contactPersonSalutation: select({
            label: "🏷️ Anrede",
            options: [
              {
                label: "Frau",
                value: "ms",
              },
              {
                label: "Herr",
                value: "mr",
              },
            ],
          }),
          contactPersonFirstname: text({
            label: "🏷️ Vorname",
          }),
          contactPersonSurname: text({
            label: "🏷️ Nachname",
          }),
          emergencyPhone: text({
            label: "☎️ Notfallnummer",
          }),
        },
      }),

      costs: float({
        label: "🪙 Kosten",
        validation: {
          min: 0.0,
        },
      }),
      registrationDeadline: calendarDay({
        label: "Anmeldefrist",
        ui: {
          views: "./views/date",
        },
      }),
      markedAsFinishedAt: timestamp({
        label: "🗓️ Als beendet markiert",
        ui: {
          createView: {
            fieldMode: "hidden",
          },
        },
      }),
    },
    hooks: {
      resolveInput(args) {
        if (args.operation !== "update") {
          return args.resolvedData;
        }

        if (args.context.session.data.role === "administrator") {
          if (
            args.item.status === "new" &&
            args.resolvedData.carePeriodCommitted?.connect
          ) {
            return {
              ...args.resolvedData,
              status: "planned",
            };
          }
        }

        return args.resolvedData;
      },
      async afterOperation(args) {
        if (args.operation == "create") {
          await args.context.query.EventTicket.createMany({
            data: Array.from({
              length: args.inputData.participantLimit as number,
            }).map((item, index) => ({
              event: { connect: { id: args.item.id } },
              code: generateCode(),
              number: index + 1,
              type: "participant",
            })),
          });

          await args.context.query.EventTicket.createMany({
            data: Array.from({
              length: args.inputData.waitingListLimit as number,
            }).map((item, index) => ({
              event: { connect: { id: args.item.id } },
              code: generateCode(),
              number: index + 1,
              type: "waitinglist",
            })),
          });

          return;
        }

        if (args.operation == "update") {
          const periodIds = [];

          if (
            args.inputData?.carePeriodAlternative?.create &&
            args.originalItem.carePeriodAlternativeId
          ) {
            periodIds.push(args.originalItem.carePeriodAlternativeId);
          }

          if (
            args.inputData?.carePeriodDesired?.create &&
            args.originalItem.carePeriodDesiredId
          ) {
            periodIds.push(args.originalItem.carePeriodDesiredId);
          }

          for (const id of periodIds) {
            const careDays = await args.context.db.CareDay.findMany({
              where: {
                id: {
                  equals: id,
                },
              },
            });

            if (careDays.length > 0) {
              await args.context.db.CareDay.deleteMany({
                where: careDays.map((day) => ({ id: day.id })),
              });
            }
          }

          if (periodIds.length > 0) {
            await args.context.db.CarePeriod.deleteMany({
              where: periodIds.map((period) => ({ id: period })),
            });
          }
        }
      },
    },
  }),
  EventCategory: list({
    access: {
      operation: {
        create: isAdmin,
        delete: isAdmin,
        query: allowAll,
        update: isAdmin,
      },
    },
    graphql: {
      cacheHint: () => ({
        maxAge: 60,
        scope: "PUBLIC",
      }),
    },
    ui: {
      label: "🏳️‍🌈 Veranstaltungskategorien",
    },
    fields: {
      name: text({
        label: "🏷️ Name",
        isIndexed: "unique",
      }),
      events: relationship({
        ref: "Event.categories",
        many: true,
      }),
    },
  }),
  CareDay: list({
    access: {
      operation: {
        create: isAdminOrRoleAdminOrRoleOrganizer,
        delete: isAdminOrRoleAdminOrRoleOrganizer,
        query: allowAll,
        update: isAdminOrRoleAdminOrRoleOrganizer,
      },
    },
    ui: {
      label: "📅 Betreuungstage",
      labelField: "day",
    },
    graphql: {
      cacheHint: () => ({
        maxAge: 60,
        scope: "PUBLIC",
      }),
    },
    fields: {
      day: calendarDay({
        label: "📅 Tag",
        ui: {
          views: "./views/date",
        },
      }),
      period: relationship({
        label: "📅 Betreuungstage",
        ref: "CarePeriod.careDays",
        graphql: {
          cacheHint: {
            maxAge: 60,
            scope: "PUBLIC",
          },
        },
      }),
    },
  }),
  CarePeriod: list({
    access: {
      operation: {
        create: isAdminOrRoleAdminOrRoleOrganizer,
        delete: isAdminOrRoleAdminOrRoleOrganizer,
        query: allowAll,
        update: isAdminOrRoleAdminOrRoleOrganizer,
      },
    },
    graphql: {
      cacheHint: () => ({
        maxAge: 60,
        scope: "PUBLIC",
      }),
    },
    ui: {
      label: "📅 Betreuungszeiträume",
      labelField: "displayName",
      listView: {
        initialColumns: ["period", "careDays"],
      },
    },
    fields: {
      period: relationship({
        label: "📅 Zeitraum",
        ref: "Period.carePeriods",
        ui: {
          labelField: "displayNameWithHoliday",
        },
        graphql: {
          cacheHint: {
            maxAge: 60,
            scope: "PUBLIC",
          },
        },
      }),
      event: relationship({
        label: "♾️ Veranstaltung",
        ref: "Event.carePeriodCommitted",
      }),
      careDays: relationship({
        label: "📅 Betreuungstage",
        ref: "CareDay.period",
        many: true,
        graphql: {
          cacheHint: {
            maxAge: 60,
            scope: "PUBLIC",
          },
        },
      }),
      displayName: virtual({
        ui: {
          createView: {
            fieldMode: "hidden",
          },
          itemView: {
            fieldMode: "hidden",
          },
          listView: {
            fieldMode: "read",
          },
        },
        field: graphql.field({
          type: graphql.String,
          resolve: async (item, args, context) => {
            const period = (await context.query.Period.findOne({
              where: { id: item.periodId },
              query: "displayNameWithHoliday",
            })) as { displayNameWithHoliday: string };

            const days = (await context.query.CareDay.findMany({
              where: {
                period: {
                  id: {
                    equals: item.id,
                  },
                },
              },
              orderBy: {
                day: "asc",
              },
              query: "day",
            })) as { day: Date }[];

            const daysString = days
              .map((careDay) => {
                const date = new Date(careDay.day);

                const year = date.getFullYear();
                const month = (date.getMonth() + 1).toString().padStart(2, "0");
                const day = date.getDate().toString().padStart(2, "0");

                return `${day}.${month}`;
              })
              .join(", ");

            return `${period.displayNameWithHoliday} | ${daysString} ${
              item.eventId ? "🟢" : "🔵"
            }`;
          },
        }),
      }),
    },
  }),
  Organizer: list({
    access: {
      operation: {
        create: isAdminOrRoleAdmin,
        delete: isAdminOrRoleAdmin,
        query: allowAll,
        update: isAdminOrRoleAdmin,
      },
    },
    graphql: {
      cacheHint: () => ({
        maxAge: 60,
        scope: "PUBLIC",
      }),
    },
    ui: {
      label: "🧑‍🏭 Veranstalter",
    },
    fields: {
      name: text({
        label: "🏷️ Name",
      }),
      ...group({
        label: "Ansprechperson",
        fields: {
          contactPersonSalutation: select({
            label: "🏷️ Anrede",
            options: [
              {
                label: "Frau",
                value: "ms",
              },
              {
                label: "Herr",
                value: "mr",
              },
            ],
          }),
          contactPersonFirstname: text({
            label: "🏷️ Vorname",
          }),
          contactPersonSurname: text({
            label: "🏷️ Nachname",
          }),
        },
      }),
      email: text({
        label: "📫 Email",
      }),
      phone: text({
        label: "☎️ Telefonnummer",
      }),
      ...group({
        label: "📍 Adresse",
        fields: {
          addressCity: text({
            label: "Ort",
          }),
          addressStreet: text({
            label: "Straße",
          }),
          addressZip: text({
            label: "PLZ",
          }),
        },
      }),
      ...group({
        label: "🪙 Bankverbindung",
        fields: {
          bankName: text({
            label: "Bank",
            access: {
              read: isAuthenticated,
            },
          }),
          bankOwner: text({
            label: "Kontoinhaber",
            access: {
              read: isAuthenticated,
            },
          }),
          bankIban: text({
            label: "IBAN",
            access: {
              read: isAuthenticated,
            },
          }),
          bankBic: text({
            label: "BIC",
            access: {
              read: isAuthenticated,
            },
          }),
        },
      }),

      events: relationship({
        label: "🗓️ Veranstaltungen",
        ref: "Event.organizer",
        many: true,
      }),
      users: relationship({
        label: "👥 Benutzer",
        ref: "User.organizer",
        many: true,
      }),
    },
  }),
  Partner: list({
    access: {
      operation: {
        create: isAdminOrRoleAdmin,
        delete: isAdminOrRoleAdmin,
        query: isAdminOrRoleAdmin,
        update: isAdminOrRoleAdmin,
      },
    },
    ui: {
      label: "🧑‍💼 Partner",
      listView: {
        initialColumns: [
          "name",
          "contactPersonSalutation",
          "contactPersonFirstname",
          "contactPersonSurname",
          "email",
          "earlyAccessCode",
        ],
      },
    },
    fields: {
      name: text({
        label: "🏢 Institution",
      }),
      ...group({
        label: "Ansprechperson",
        fields: {
          contactPersonSalutation: select({
            label: "🏷️ Anrede",
            options: [
              {
                label: "Frau",
                value: "ms",
              },
              {
                label: "Herr",
                value: "mr",
              },
            ],
          }),
          contactPersonFirstname: text({
            label: "🏷️ Vorname",
          }),
          contactPersonSurname: text({
            label: "🏷️ Nachname",
          }),
        },
      }),
      email: text({
        label: "📫 Email",
      }),
      phone: text({
        label: "☎️ Telefonnummer",
      }),
      earlyAccessCode: relationship({
        label: "🎫 Frühbucher Codes",
        ref: "EarlyAccessCode.partner",
        ui: {
          labelField: "code",
        },
        many: true,
      }),
    },
  }),
  EarlyAccessCode: list({
    access: {
      operation: {
        create: isAdminOrRoleAdmin,
        delete: isAdminOrRoleAdmin,
        query: isAdminOrRoleAdmin,
        update: isAdminOrRoleAdmin,
      },
    },
    ui: {
      label: "🎫 Frühbucher Codes",
      listView: {
        initialColumns: ["code", "partner"],
      },
    },
    fields: {
      code: text({
        label: "🎫 Code",
      }),
      partner: relationship({
        label: "🧑‍💼 Partner",
        ref: "Partner.earlyAccessCode",
      }),
      season: relationship({
        label: "🏆 Saison",
        ref: "Season",
      }),
      notificationDate: timestamp({
        label: "📅 E-Mail verschickt",
      }),
    },
  }),
  EarlyAccessCodeVoucher: list({
    access: {
      operation: {
        create: isAdminOrRoleAdmin,
        delete: isAdminOrRoleAdmin,
        query: isAdminOrRoleAdminOrRoleOrganizerOrRoleCustodian,
        update: isAdminOrRoleAdmin,
      },
      filter: {
        query: ({ context, session }) => {
          if (isAdmin(context)) {
            return true;
          }

          if (isRoleCustodian(context)) {
            return {
              custodian: {
                user: {
                  id: {
                    equals: session.data.id,
                  },
                },
              },
            };
          }

          return false;
        },
      },
    },
    ui: {
      label: "🎫 Frühbucher Codes (eingelöst)",
      listView: {
        initialColumns: ["code", "season", "custodian"],
      },
    },
    fields: {
      code: text({
        label: "🎫 Code",
      }),
      season: relationship({
        label: "🏆 Saison",
        ref: "Season",
      }),
      custodian: relationship({
        label: "🧓 Sorgeberechtigter",
        ref: "Custodian",
      }),
    },
  }),
  Custodian: list({
    access: {
      operation: {
        create: isAdminOrRoleAdmin,
        delete: isAdminOrRoleAdmin,
        query: isAdminOrRoleAdminOrRoleOrganizerOrRoleCustodian,
        update: isAdminOrRoleAdminOrRoleCustodian,
      },
      filter: {
        query: ({ context, session }) => {
          if (isAdminOrRoleAdmin(context)) {
            return true;
          }

          if (isRoleOrganizer(context)) {
            return {
              OR: [
                {
                  bookings: {
                    some: {
                      event: {
                        organizer: {
                          id: {
                            equals: session.data.organizer.id,
                          },
                        },
                      },
                    },
                  },
                },
                {
                  reservations: {
                    some: {
                      event: {
                        organizer: {
                          id: {
                            equals: session.data.organizer.id,
                          },
                        },
                      },
                    },
                  },
                },
              ],
            };
          }

          if (isRoleCustodian(context)) {
            return {
              user: {
                id: {
                  equals: session.data.id,
                },
              },
            };
          }

          return false;
        },
        update: ({ context, session }) => {
          if (isAdmin(context)) {
            return true;
          }

          if (isRoleCustodian(context)) {
            return {
              user: {
                id: {
                  equals: session.data.id,
                },
              },
            };
          }

          return false;
        },
      },
    },
    ui: {
      label: "🧓 Sorgeberechtigte",
      listView: {
        initialColumns: ["salutation", "firstname", "surname", "children"],
      },
      labelField: "displayName",
    },
    fields: {
      salutation: select({
        label: "🏷️ Anrede",
        options: [
          {
            label: "Frau",
            value: "ms",
          },
          {
            label: "Herr",
            value: "mr",
          },
        ],
      }),
      firstname: text({
        label: "🏷️ Vorname",
      }),
      surname: text({
        label: "🏷️ Nachname",
      }),
      phone: text({
        label: "☎️ Telefonnummer",
      }),
      emergencyPhone: text({
        label: "🚨 Notfallnummer",
      }),
      ...group({
        label: "📍 Adresse",
        fields: {
          addressCity: text({
            label: "Ort",
          }),
          addressStreet: text({
            label: "Straße",
          }),
          addressHouseNumber: text({
            label: "Hausnummer",
          }),
          addressZip: text({
            label: "PLZ",
          }),
          addressAdditional: text({
            label: "Zusatz",
          }),
        },
      }),
      user: relationship({
        ref: "User",
        label: "👥 Benutzer",
      }),
      children: relationship({
        ref: "Child.custodian",
        label: "🧒 Kinder",
        many: true,
      }),
      bookings: relationship({
        ref: "Booking.custodian",
        label: "Buchungen",
        many: true,
      }),
      reservations: relationship({
        ref: "Reservation.custodian",
        label: "Reservierungen",
        many: true,
      }),
      discounts: relationship({
        label: "Ermäßigungen",
        ref: "Discount.custodian",
        many: true,
        ui: {
          createView: {
            fieldMode: "hidden",
          },
          itemView: {
            fieldMode: "read",
          },
        },
      }),
      displayName: virtual({
        ui: {
          itemView: {
            fieldMode: "hidden",
          },
          createView: {
            fieldMode: "hidden",
          },
          listView: {
            fieldMode: "hidden",
          },
        },
        field: graphql.field({
          type: graphql.String,
          resolve: async (item, args, context) => {
            const salutation =
              item.salutation == "ms"
                ? "Frau"
                : item.salutation == "mr"
                ? "Herr"
                : "";
            return `${salutation} ${item.firstname} ${item.surname}`.trim();
          },
        }),
      }),
    },
  }),
  Child: list({
    access: {
      operation: {
        create: isAdminOrRoleAdminOrRoleCustodian,
        delete: isAdminOrRoleAdmin,
        query: isAdminOrRoleAdminOrRoleOrganizerOrRoleCustodian,
        update: isAdminOrRoleAdminOrRoleCustodian,
      },
      filter: {
        query: async ({ context, session }) => {
          if (isAdminOrRoleAdmin(context)) {
            return true;
          }

          if (isRoleOrganizer(context)) {
            return {
              tickets: {
                some: {
                  eventTicket: {
                    event: {
                      organizer: {
                        id: {
                          equals: session.data.organizer.id,
                        },
                      },
                    },
                  },
                },
              },
            };
          }

          if (isRoleCustodian(context)) {
            const custodians = await context.db.Custodian.findMany({
              where: {
                user: {
                  id: {
                    equals: session.data.id,
                  },
                },
              },
            });

            if (custodians.length != 1) {
              return false;
            }

            const custodian = custodians[0];

            return {
              custodian: {
                id: {
                  equals: custodian.id,
                },
              },
            };
          }

          return false;
        },
        delete: async ({ context, session }) => {
          if (isAdmin(context)) {
            return true;
          }

          if (isRoleCustodian(context)) {
            const custodians = await context.db.Custodian.findMany({
              where: {
                user: {
                  id: {
                    equals: session.data.id,
                  },
                },
              },
            });

            if (custodians.length != 1) {
              return false;
            }

            const custodian = custodians[0];

            return {
              custodian: {
                id: {
                  equals: custodian.id,
                },
              },
            };
          }

          return false;
        },
        update: async ({ context, session }) => {
          if (isAdmin(context)) {
            return true;
          }

          if (isRoleCustodian(context)) {
            const custodians = await context.db.Custodian.findMany({
              where: {
                user: {
                  id: {
                    equals: session.data.id,
                  },
                },
              },
            });

            if (custodians.length != 1) {
              return false;
            }

            const custodian = custodians[0];

            return {
              custodian: {
                id: {
                  equals: custodian.id,
                },
              },
            };
          }

          return false;
        },
      },
    },
    ui: {
      label: "🧒 Kinder",
      listView: {
        initialColumns: ["surname", "firstname"],
      },
      labelField: "displayName",
    },
    fields: {
      firstname: text({
        label: "🏷️ Vorname",
      }),
      surname: text({
        label: "🏷️ Nachname",
      }),
      displayName: virtual({
        field: graphql.field({
          type: graphql.String,
          resolve: async (item, args, context) =>
            `${item.firstname} ${item.surname}`,
        }),
      }),
      dateOfBirth: calendarDay({
        label: "📅 Geburtsdatum",
      }),
      gender: select({
        label: "Geschlecht",
        options: [
          {
            label: "weiblich",
            value: "female",
          },
          {
            label: "männlich",
            value: "male",
          },
          {
            label: "divers",
            value: "diverse",
          },
        ],
      }),

      healthInsurer: text({
        label: "Krankenversicherung",
      }),
      hasLiabilityInsurance: checkbox({
        label: "Haftpflichtversicherung vorhanden",
      }),

      assistanceRequired: checkbox({
        label: "Assistenz notwendig",
      }),

      physicalImpairment: text({
        label: "Körperliche Beeinträchtigung",
      }),

      medication: text({
        label: "Medikamente",
      }),

      foodIntolerances: text({
        label: "Allergien / Lebensmittelunverträglichkeiten",
      }),

      dietaryRegulations: text({
        label: "Ernährungsvorschriften",
      }),

      miscellaneous: text({
        label: "Sonstiges",
      }),

      tickets: relationship({
        label: "🎫 Tickets",
        ref: "Ticket.child",
        many: true,
      }),
      discounts: relationship({
        label: "🎫 Ermäßigungen",
        ref: "Discount.child",
        many: true,
        ui: {
          createView: {
            fieldMode: "hidden",
          },
          itemView: {
            fieldMode: "read",
          },
        },
      }),

      custodian: relationship({
        ref: "Custodian.children",
        label: "🧓 Sorgeberechtigter",

        hooks: {
          async resolveInput(args) {
            if (args.operation == "update") {
              return args.resolvedData.custodian;
            }

            const custodians = await args.context.db.Custodian.findMany({
              where: {
                user: {
                  id: {
                    equals: args.context.session.data.id,
                  },
                },
              },
            });

            if (custodians.length != 1) {
              return null;
            }

            const custodian = custodians[0];

            return {
              connect: {
                id: custodian.id,
              },
            };
          },
        },
      }),

      isDeleted: checkbox({
        access: {
          create: isAdminOrRoleAdmin,
          update: isAdminOrRoleAdmin,
        },
        label: "🗑️ Ist Gelöscht",
      }),
    },
  }),
  Reservation: list({
    access: {
      operation: {
        create: isAdminOrRoleAdmin,
        delete: isAdminOrRoleAdmin,
        query: isAdminOrRoleAdminOrRoleOrganizerOrRoleCustodian,
        update: isAdminOrRoleAdmin,
      },
      filter: {
        query: async ({ context, session }) => {
          if (isAdminOrRoleAdmin(context)) {
            return true;
          }

          if (isRoleOrganizer(context)) {
            return {
              event: {
                organizer: {
                  id: {
                    equals: session.data.organizer.id,
                  },
                },
              },
            };
          }

          if (isRoleCustodian(context)) {
            const custodians = await context.db.Custodian.findMany({
              where: {
                user: {
                  id: {
                    equals: session.data.id,
                  },
                },
              },
            });

            if (custodians.length != 1) {
              return false;
            }

            const custodian = custodians[0];

            return {
              custodian: {
                id: {
                  equals: custodian.id,
                },
              },
            };
          }

          return false;
        },
      },
    },
    ui: {
      label: "📖 Reservierungen",
      listView: {
        initialColumns: ["event", "status", "paycode", "sum"],
      },
    },
    fields: {
      code: text({
        label: "Code",
        isIndexed: "unique",
      }),
      price: virtual({
        label: "🪙 Preis",
        field: graphql.field({
          type: graphql.Float,
          resolve: async (item, args, context) => {
            const sudoContext = context.sudo();
            const tickets = await sudoContext.db.Ticket.findMany({
              where: {
                reservation: { id: { equals: item.id } },
                isDeleted: {
                  equals: false,
                },
              },
            });

            return tickets.reduce(
              (sum, ticket) => sum + (ticket.price ?? 0),
              0
            );
          },
        }),
      }),
      event: relationship({
        label: "Veranstaltung",
        ref: "Event",
        access: {
          update(args) {
            if (isAdmin(args.context)) {
              return true;
            }

            return false;
          },
        },
      }),
      custodian: relationship({
        label: "Sorgeberechtigter",
        ref: "Custodian.reservations",
        access: {
          update(args) {
            if (isAdmin(args.context)) {
              return true;
            }

            return false;
          },
        },
      }),
      tickets: relationship({
        label: "Tickets",
        ref: "Ticket.reservation",
        many: true,
        access: {
          update(args) {
            if (isAdmin(args.context)) {
              return true;
            }

            return false;
          },
        },
      }),
      date: timestamp({
        label: "Reservierungsdatum",
      }),
      isDeleted: checkbox({
        access: {
          create: isAdminOrRoleAdmin,
          update: isAdminOrRoleAdmin,
        },
        label: "🗑️ Ist Gelöscht",
        defaultValue: false,
      }),
    },
  }),
  Booking: list({
    access: {
      operation: {
        create: isAdminOrRoleAdmin,
        delete: isAdminOrRoleAdmin,
        query: isAdminOrRoleAdminOrRoleOrganizerOrRoleCustodian,
        update: isAdminOrRoleAdmin,
      },
      filter: {
        query: async ({ context, session }) => {
          if (isAdminOrRoleAdmin(context)) {
            return true;
          }

          if (isRoleOrganizer(context)) {
            return {
              event: {
                organizer: {
                  id: {
                    equals: session.data.organizer.id,
                  },
                },
              },
            };
          }

          if (isRoleCustodian(context)) {
            const custodians = await context.db.Custodian.findMany({
              where: {
                user: {
                  id: {
                    equals: session.data.id,
                  },
                },
              },
            });

            if (custodians.length != 1) {
              return false;
            }

            const custodian = custodians[0];

            return {
              custodian: {
                id: {
                  equals: custodian.id,
                },
              },
            };
          }

          return false;
        },
      },
    },
    ui: {
      label: "📖 Buchungen",
      listView: {
        initialColumns: ["code", "status", "event", "sum", "date", "isDeleted"],
        initialSort: {
          field: "date",
          direction: "DESC",
        },
      },
    },
    fields: {
      code: text({
        label: "Code",
        isIndexed: "unique",
      }),
      price: virtual({
        label: "🪙 Preis",
        field: graphql.field({
          type: graphql.Float,
          resolve: async (item, args, context) => {
            const sudoContext = context.sudo();
            const tickets = await sudoContext.db.Ticket.findMany({
              where: {
                booking: { id: { equals: item.id } },
                isDeleted: {
                  equals: false,
                },
              },
            });

            return tickets.reduce(
              (sum, ticket) => sum + (ticket.price ?? 0),
              0
            );
          },
        }),
      }),
      status: select({
        label: "Status",
        options: [
          {
            label: "Überfällig",
            value: "overdue",
          },
          {
            label: "Austehend",
            value: "pending",
          },
          {
            label: "Bezahlt",
            value: "payed",
          },
        ],
        defaultValue: "pending",
        access: {
          create(args) {
            if (isAdmin(args.context)) {
              return true;
            }

            return false;
          },
          update(args) {
            if (isAdmin(args.context)) {
              return true;
            }

            return false;
          },
        },
      }),
      event: relationship({
        label: "Veranstaltung",
        ref: "Event",
        access: {
          update(args) {
            if (isAdmin(args.context)) {
              return true;
            }

            return false;
          },
        },
      }),
      custodian: relationship({
        label: "Sorgeberechtigter",
        ref: "Custodian.bookings",
        access: {
          update(args) {
            if (isAdmin(args.context)) {
              return true;
            }

            return false;
          },
        },
      }),
      tickets: relationship({
        label: "Tickets",
        ref: "Ticket.booking",
        many: true,
        access: {
          update(args) {
            if (isAdmin(args.context)) {
              return true;
            }

            return false;
          },
        },
      }),
      date: timestamp({
        label: "📅 Buchungsdatum",
      }),
      reminderDate: timestamp({
        label: "📅 Erinnerung verschickt",
      }),
      isDeleted: checkbox({
        access: {
          create: isAdminOrRoleAdmin,
          update: isAdminOrRoleAdmin,
        },
        label: "🗑️ Ist Gelöscht",
        defaultValue: false,
      }),
    },
  }),
  Ticket: list({
    access: {
      operation: {
        create: isAdminOrRoleAdmin,
        delete: isAdminOrRoleAdmin,
        query: isAdminOrRoleAdminOrRoleOrganizerOrRoleCustodian,
        update: isAdminOrRoleAdmin,
      },
      filter: {
        query: async ({ context, session }) => {
          if (isAdminOrRoleAdmin(context)) {
            return true;
          }

          if (isRoleOrganizer(context) && session.data.organizer) {
            return {
              eventTicket: {
                event: {
                  organizer: {
                    id: {
                      equals: session.data.organizer.id,
                    },
                  },
                },
              },
            };
          }

          if (isRoleCustodian(context)) {
            const custodians = await context.db.Custodian.findMany({
              where: {
                user: {
                  id: {
                    equals: session.data.id,
                  },
                },
              },
            });

            if (custodians.length != 1) {
              return false;
            }

            const custodian = custodians[0];

            return {
              OR: [
                {
                  booking: {
                    custodian: {
                      id: {
                        equals: custodian.id,
                      },
                    },
                  },
                },
                {
                  reservation: {
                    custodian: {
                      id: {
                        equals: custodian.id,
                      },
                    },
                  },
                },
              ],
            };
          }

          return false;
        },
      },
    },
    ui: {
      label: "📖 Tickets",
      listView: {
        initialColumns: ["booking", "price", "child", "discountVoucher"],
      },
    },
    fields: {
      booking: relationship({
        label: "Buchung",
        ref: "Booking.tickets",
      }),
      reservation: relationship({
        label: "Reservierung",
        ref: "Reservation.tickets",
      }),
      eventTicket: relationship({
        label: "Ticket",
        ref: "EventTicket.bookedTicket",
      }),
      child: relationship({
        label: "Kind",
        ref: "Child.tickets",
      }),
      discountVoucher: relationship({
        label: "Ermäßigungsschein",
        ref: "DiscountVoucher.ticket",
      }),
      price: float({
        label: "🪙 Preis",
      }),
      isDeleted: checkbox({
        access: {
          create: isAdminOrRoleAdmin,
          update: isAdminOrRoleAdmin,
        },
        label: "🗑️ Ist Gelöscht",
        defaultValue: false,
      }),
    },
  }),
  Discount: list({
    access: {
      operation: {
        create: isAdminOrRoleAdmin,
        delete: isAdminOrRoleAdmin,
        query: isAdminOrRoleAdminOrRoleCustodian,
        update: isAdminOrRoleAdmin,
      },
      filter: {
        query: async ({ context, session }) => {
          if (isAdminOrRoleAdmin(context)) {
            return true;
          }

          if (isRoleCustodian(context)) {
            const custodians = await context.db.Custodian.findMany({
              where: {
                user: {
                  id: {
                    equals: session.data.id,
                  },
                },
              },
            });

            if (custodians.length != 1) {
              return false;
            }

            const custodian = custodians[0];

            return {
              custodian: {
                id: {
                  equals: custodian.id,
                },
              },
            };
          }

          return false;
        },
      },
    },
    ui: {
      label: "🏷️ Ermäßigungen",
      labelField: "displayName",
      listView: {
        initialColumns: [
          "child",
          "status",
          "submissionDate",
          "type",
          "validUntil",
          "redeemLimit",
          "rejectionReason",
        ],
      },
    },
    fields: {
      status: select({
        label: "Status",
        options: [
          {
            label: "Ausstehend",
            value: "pending",
          },
          {
            label: "Überfällig",
            value: "overdue",
          },
          {
            label: "Gewährt",
            value: "approved",
          },
          {
            label: "Abgelehnt",
            value: "rejected",
          },
          {
            label: "Storniert",
            value: "cancelled",
          },
        ],
        access: {
          create(args) {
            if (isAdmin(args.context)) {
              return true;
            }

            return false;
          },
          update(args) {
            if (isAdmin(args.context)) {
              return true;
            }

            return false;
          },
        },
        defaultValue: "pending",
      }),
      type: relationship({
        label: "🔢 Typ",
        ref: "DiscountType",
      }),
      child: relationship({
        label: "🧒 Kind",
        ref: "Child.discounts",
      }),
      vouchers: relationship({
        label: "🎫 Ermäßigungsscheine",
        ref: "DiscountVoucher.discount",
        many: true,
      }),
      submissionDate: timestamp({
        label: "📅 Beantragt",
        defaultValue: { kind: "now" },
      }),
      rejectionReason: relationship({
        label: "❌ Ablehnungsgrund",
        ref: "DiscountRejectionReason",
      }),
      rejectionNote: text({
        label: "❌ Ablehnungshinweis",
      }),
      season: relationship({
        label: "🏆 Saison",
        ref: "Season",
      }),
      validUntil: timestamp({
        label: "📅 Gültig bis",
      }),
      reminderDate: timestamp({
        label: "📅 Erinnerung verschickt",
      }),
      custodian: relationship({
        label: "🏷️ Sorgeberechtigter",
        ref: "Custodian.discounts",
      }),
      displayName: virtual({
        field: graphql.field({
          type: graphql.String,
          resolve: async (item, args, context) => {
            const discountType = await context.query.DiscountType.findOne({
              where: {
                id: item.typeId,
              },
              query: "name amountEvents",
            });

            const custodian = await context.query.Custodian.findOne({
              where: {
                id: item.custodianId,
              },
              query: "displayName",
            });

            const child = await context.query.Child.findOne({
              where: {
                id: item.childId,
              },
              query: "firstname surname",
            });

            const vouchers = await context.query.DiscountVoucher.findMany({
              where: {
                discount: {
                  id: {
                    equals: item.id,
                  },
                },
              },
              query: "id ticket { id }",
            });

            const usedVouchersAmount = vouchers.filter(
              (voucher) => voucher.ticket !== null
            ).length;

            return `🧓 ${custodian.displayName} | 🧒 ${child.surname},  ${child.firstname} | ${discountType.name} | ${usedVouchersAmount} / ${vouchers.length}`;
          },
        }),
      }),
    },
    hooks: {
      async afterOperation(args) {
        if (args.operation !== "create") {
          return;
        }

        const discountType = await args.context.db.DiscountType.findOne({
          where: {
            id: args.item.typeId,
          },
        });

        if (!discountType) {
          return;
        }

        await args.context.db.DiscountVoucher.createMany({
          data: Array.from({ length: discountType.amountEvents }).map(() => ({
            discount: {
              connect: {
                id: args.item.id,
              },
            },
          })),
        });
      },
    },
  }),
  DiscountRejectionReason: list({
    access: {
      operation: {
        create: isAdminOrRoleAdmin,
        delete: isAdminOrRoleAdmin,
        query: isAdminOrRoleAdminOrRoleCustodian,
        update: isAdminOrRoleAdmin,
      },
    },
    ui: {
      label: "🏷️ Ablehnungsgründe",
      labelField: "reason",
      listView: {
        initialColumns: ["reason", "description"],
      },
    },
    fields: {
      reason: text({
        label: "Grund",
      }),
      description: text({
        label: "Erläuterung",
      }),
    },
  }),
  DiscountType: list({
    access: {
      operation: {
        create: isAdminOrRoleAdmin,
        delete: isAdminOrRoleAdmin,
        query: allowAll,
        update: isAdminOrRoleAdmin,
      },
    },
    ui: {
      label: "🏷️ Ermäßigungstypen",
      listView: {
        initialColumns: [
          "name",
          "discountPercent",
          "amountEvents",
          "amountChildren",
        ],
      },
    },
    fields: {
      name: text({
        label: "🏷️ Name",
        validation: {
          isRequired: true,
        },
      }),
      discountPercent: float({
        label: "🔢 Rabatt (in %)",
        validation: {
          min: 0,
          max: 100,
          isRequired: true,
        },
      }),
      description: text({
        label: "🏷️ Beschreibung",
      }),
      amountEvents: integer({
        label: "🔢 Anzahl Veranstaltungen",
        validation: {
          isRequired: true,
        },
      }),
      amountChildren: integer({
        label: "🔢 Anzahl Kinder",
        validation: {
          isRequired: true,
        },
      }),
      amountEventsDescription: text({
        label: "📄 Anzahl Veranstaltungen Beschreibung",
      }),
      amountChildrenDescription: text({
        label: "📄 Anzahl Kinder Beschreibung",
      }),
    },
  }),
  DiscountVoucher: list({
    access: {
      operation: {
        create: isAdminOrRoleAdmin,
        delete: isAdminOrRoleAdmin,
        query: isAdminOrRoleAdminOrRoleCustodian,
        update: isAdminOrRoleAdmin,
      },
      filter: {
        query: async ({ context, session }) => {
          if (isAdminOrRoleAdmin(context)) {
            return true;
          }

          if (isRoleCustodian(context)) {
            const custodians = await context.db.Custodian.findMany({
              where: {
                user: {
                  id: {
                    equals: session.data.id,
                  },
                },
              },
            });

            if (custodians.length != 1) {
              return false;
            }

            const custodian = custodians[0];

            return {
              discount: {
                custodian: {
                  id: {
                    equals: custodian.id,
                  },
                },
              },
            };
          }

          return false;
        },
      },
    },
    ui: {
      label: "🏷️ Ermäßigungsscheine",
      labelField: "displayName",
      listView: {
        initialColumns: [
          "discount",
          "ticket",
          "submissionDate",
          "redemptionDate",
        ],
      },
    },
    fields: {
      discount: relationship({
        label: "🎫 Ermäßigung",
        ref: "Discount.vouchers",
      }),

      ticket: relationship({
        label: "Ticket",
        ref: "Ticket.discountVoucher",
      }),
      redemptionDate: timestamp({
        label: "📅 Eingelöst",
      }),
      displayName: virtual({
        field: graphql.field({
          type: graphql.String,
          resolve: async (item, args, context) => {
            const discount = await context.query.Discount.findOne({
              where: {
                id: item.discountId,
              },
              query: "id displayName",
            });

            return `${discount.displayName} | ${
              item.ticketId && item.redemptionDate
                ? "📅 Eingelöst " +
                  new Date(item.redemptionDate).toLocaleString()
                : "🟢 verfügbar"
            }`;
          },
        }),
      }),
    },
  }),
  EventTicket: list({
    access: {
      operation: {
        create: isAdminOrRoleAdmin,
        delete: isAdminOrRoleAdmin,
        query: isAdminOrRoleAdminOrRoleOrganizerOrRoleCustodian,
        update: isAdminOrRoleAdmin,
      },
      filter: {
        // query: ({ context, session }) => {
        //   if (isAdminOrRoleAdmin(context)) {
        //     return true;
        //   }
        //   if (isRoleCustodian(context)) {
        //     return {
        //       cartTicket: {
        //         custodian: {
        //           user: {
        //             id: {
        //               equals: session.data.id,
        //             },
        //           },
        //         },
        //       },
        //     };
        //   }
        //   return false;
        // },
      },
    },
    ui: {
      label: "📖 Veranstaltungstickets",
      labelField: "displayName",
      listView: {
        initialColumns: ["code", "type", "event", "number"],
      },
    },
    fields: {
      event: relationship({
        label: "♾️ Veranstaltung",
        ref: "Event",
      }),
      version: integer({
        label: "🔢 Version",
        validation: {
          isRequired: true,
        },
        defaultValue: 1,
        access: {
          read: isAdminOrRoleAdminOrRoleOrganizer,
        },
        graphql: {
          omit: {
            create: true,
            update: true,
          },
        },
      }),
      number: integer({
        label: "#️⃣ Nummer",
        validation: {
          isRequired: true,
          min: 1,
        },
        defaultValue: 0,
      }),
      type: select({
        label: "🔢 Typ",
        options: [
          {
            label: "⭐ Teilnehmerplatz",
            value: "participant",
          },
          {
            label: "⌛ Wartelistenplatz",
            value: "waitinglist",
          },
        ],
      }),
      code: text({
        label: "🆔 Code",
        isIndexed: "unique",
        validation: {
          isRequired: true,
        },
      }),
      cartTicket: relationship({
        label: "🛒 Warenkorbticket",
        ref: "CartTicket.eventTicket",
        db: {
          foreignKey: true,
        },
      }),
      bookedTicket: relationship({
        label: "🛒 Ticket",
        ref: "Ticket.eventTicket",
        db: {
          foreignKey: true,
        },
      }),
      isBlocked: checkbox({
        label: "🚫 Ist geblockt",
        defaultValue: false,
      }),
      displayName: virtual({
        field: graphql.field({
          type: graphql.String,
          resolve: async (item, args, context) => {
            const event = await context.query.Event.findOne({
              where: {
                id: item.eventId,
              },
              query: "id name",
            });

            const eventTicket = await context.query.EventTicket.findOne({
              where: {
                id: item.id,
              },
              query:
                "id cartTicket { id addedToCartAt } bookedTicket { id booking { date } reservation { date } }",
            });

            return `${event.name} | ${
              eventTicket.cartTicket
                ? "🛒 im Warenkorb bis " +
                  new Date(
                    eventTicket.cartTicket.addedToCartAt
                  ).toLocaleString()
                : eventTicket.bookedTicket?.booking?.date
                ? "📅 gebucht am " +
                  new Date(
                    eventTicket.bookedTicket.booking.date
                  ).toLocaleString()
                : eventTicket.bookedTicket?.reservation?.date
                ? "📅 reserviert am " +
                  new Date(
                    eventTicket.bookedTicket.reservation.date
                  ).toLocaleString()
                : "🟢 verfügbar"
            }`;
          },
        }),
      }),
    },
  }),
  CartTicket: list({
    access: {
      operation: {
        create: isAdminOrRoleAdmin,
        delete: isAdminOrRoleAdmin,
        query: isAdminOrRoleAdminOrRoleCustodian,
        update: isAdminOrRoleAdmin,
      },
      filter: {
        query: ({ session, context }) => {
          if (isAdminOrRoleAdmin(context)) {
            return true;
          }

          if (isRoleCustodian(context)) {
            return {
              cart: {
                custodian: {
                  user: {
                    id: {
                      equals: session.data.id,
                    },
                  },
                },
              },
            };
          }

          return false;
        },
      },
    },
    ui: {
      label: "🎫 Warenkorbtickets",
      labelField: "displayName",
      listView: {},
    },
    fields: {
      cart: relationship({
        label: "🛒 Warenkorb",
        ref: "Cart.cartTickets",
      }),
      eventTicket: relationship({
        label: "📖 Veranstaltungsticket",
        ref: "EventTicket.cartTicket",
      }),
      discountType: relationship({
        label: "🏷️ Ermäßigungstyp",
        ref: "DiscountType",
      }),
      child: relationship({
        label: "🧒 Kind",
        ref: "Child",
      }),
      addedToCartAt: timestamp({
        label: "📅 Im Warenkorb seit",
        graphql: {
          omit: {
            create: true,
            update: true,
          },
        },
        defaultValue: {
          kind: "now",
        },
      }),
      displayName: virtual({
        field: graphql.field({
          type: graphql.String,
          resolve: async (item, args, context) => {
            const cartTicket = await context.query.CartTicket.findOne({
              where: {
                id: item.id,
              },
              query:
                "id eventTicket { event { id name } } cart { custodian { displayName } }",
            });

            return `${cartTicket.eventTicket.event.name} | 🛒 ${
              cartTicket.cart.custodian.displayName
            } | ⏰ ${item.addedToCartAt?.toLocaleString()}`;
          },
        }),
      }),
    },
  }),
  Cart: list({
    access: {
      operation: {
        create: isAdminOrRoleAdmin,
        delete: isAdminOrRoleAdmin,
        query: isAdminOrRoleAdminOrRoleCustodian,
        update: isAdminOrRoleAdmin,
      },
      filter: {
        query: ({ session, context }) => {
          if (isAdminOrRoleAdmin(context)) {
            return true;
          }

          if (isRoleCustodian(context)) {
            return {
              custodian: {
                user: {
                  id: {
                    equals: session.data.id,
                  },
                },
              },
            };
          }

          return false;
        },
      },
    },
    ui: {
      label: "🛒 Warenkörbe",
      labelField: "displayName",
      listView: {},
    },
    fields: {
      custodian: relationship({
        ref: "Custodian",
        label: "🏷️ Sorgeberechtigter",
      }),
      cartTickets: relationship({
        label: "🛒 Warenkorbtickets",
        ref: "CartTicket.cart",
        many: true,
      }),
      createdAt: timestamp({
        label: "📅 Angelegt",
        graphql: {
          omit: {
            create: true,
            update: true,
          },
        },
        defaultValue: {
          kind: "now",
        },
      }),
      validUntil: timestamp({
        label: "📅 Gültig bis",
        graphql: {
          omit: {
            create: true,
            update: true,
          },
        },
        hooks: {
          resolveInput(args) {
            const created = (args.resolvedData.createdAt ??
              args.item?.createdAt) as Date;

            return new Date(created.getTime() + 10 * 60 * 1000);
          },
        },
      }),
      version: integer({
        label: "🔢 Version",
        validation: {
          isRequired: true,
        },
        defaultValue: 1,
        access: {
          read: isAdminOrRoleAdmin,
        },
        graphql: {
          omit: {
            create: true,
            update: true,
          },
        },
      }),
      termExtensionCount: integer({
        label: "🔢 Laufzeitverlängerungen",
        validation: {
          isRequired: true,
        },
        defaultValue: 0,
        graphql: {
          omit: {
            create: true,
            update: true,
          },
        },
      }),
      displayName: virtual({
        field: graphql.field({
          type: graphql.String,
          resolve: async (item, args, context) => {
            const custodian = await context.query.Custodian.findOne({
              where: {
                id: item.custodianId,
              },
              query: "id displayName",
            });

            return `${
              custodian.displayName
            } | ⏰ ${item.validUntil?.toLocaleString()}`;
          },
        }),
      }),
    },
  }),
  Contact: list({
    access: {
      operation: {
        create: isAdminOrRoleAdmin,
        delete: isAdminOrRoleAdmin,
        query: allowAll,
        update: isAdminOrRoleAdmin,
      },
    },
    graphql: {
      cacheHint: () => ({
        maxAge: 60,
        scope: "PUBLIC",
      }),
    },

    ui: {
      label: "🪪 Kontakte",
      listView: {
        initialColumns: [
          "name",
          "role",
          "contactPersonFirstname",
          "contactPersonSurname",
          "email",
          "phone",
          "hidden",
        ],
      },
    },
    fields: {
      name: text({
        label: "🏢 Institution",
        validation: { isRequired: true },
      }),
      description: text({
        label: "📄 Beschreibung",
      }),
      ...group({
        label: "Ansprechperson",
        fields: {
          contactPersonFirstname: text({
            label: "🏷️ Vorname",
          }),
          contactPersonSurname: text({
            label: "🏷️ Nachname",
          }),
        },
      }),
      ...group({
        label: "📍 Adresse",
        fields: {
          addressStreet: text({
            label: "Straße",
          }),
          addressZip: text({
            label: "PLZ",
          }),
          addressCity: text({
            label: "Ort",
          }),
        },
      }),
      email: text({
        label: "📫 E-Mail",
      }),
      phone: text({
        label: "☎️ Telefonnummer",
        ui: {
          description:
            "Bitte verwenden Sie das internationale Telefonnummernformat E.164:\n[+][Länderkennzahl][Nummer inklusive Vorwahl]",
        },
      }),
      hidden: checkbox({
        label: "🕶️ Versteckt",
      }),
    },
  }),
  Invoice: list({
    ui: {
      label: "📄 Abrechnungen",
    },
    access: {
      operation: {
        create: isAdminOrRoleAdmin,
        delete: isAdminOrRoleAdmin,
        query: isAdminOrRoleAdminOrRoleOrganizer,
        update: isAdminOrRoleAdmin,
      },
      filter: {
        query: (args) => {
          if (isAdminOrRoleAdmin(args.context)) {
            return true;
          }

          if (isRoleOrganizer(args.context)) {
            return {
              event: {
                organizer: {
                  id: {
                    equals: args.session.data?.organizer?.id,
                  },
                },
              },
            };
          }

          return false;
        },
      },
    },
    fields: {
      event: relationship({
        ref: "Event",
        label: "♾️ Veranstaltung",
      }),
      createdAt: timestamp({
        label: "📅 Erzeugt",
      }),
      document: file({
        storage: "invoice_store",
        label: "📄 Datei",
      }),
    },
  }),
  EmailTemplate: list({
    access: {
      operation: {
        create: isAdminOrRoleAdmin,
        delete: isAdminOrRoleAdmin,
        query: isAdminOrRoleAdminOrRoleOrganizer,
        update: isAdminOrRoleAdmin,
      },
      filter: {
        query: (args) => {
          if (isAdminOrRoleAdmin(args.context)) {
            return true;
          }

          if (isRoleOrganizer(args.context)) {
            return {
              role: {
                equals: "paymentOverdue",
              },
            };
          }

          return false;
        },
      },
    },
    ui: {
      label: "📧 Email Vorlagen",
      listView: {
        initialColumns: ["role", "subject"],
      },
    },
    fields: {
      role: select({
        label: "⚡ Ereignis",
        validation: { isRequired: true },
        isIndexed: "unique",
        db: {
          isNullable: false,
        },
        options: [
          {
            label: "Benutzer registriert",
            value: "userRegistered",
          },
          {
            label: "Konto verifiziert",
            value: "userVerifiedAccount",
          },
          {
            label: "Teilnehmerplatz reserviert",
            value: "ticketReserved",
          },
          {
            label: "Ticket storniert",
            value: "ticketCancelled",
          },
          {
            label: "Warteplatz reserviert",
            value: "waitingListTicketReserved",
          },
          {
            label: "Warteplatz in Warteliste vorgerückt",
            value: "waitingListTicketSlotAdvanced",
          },
          {
            label: "Warteplatz in Teilnehmerliste nachgerückt",
            value: "waitingListTicketConvertedToBooking",
          },
          {
            label: "Warteplatz in Teilnehmerliste nachgerückt (Reservierung)",
            value: "waitingListTicketConvertedToParticipant",
          },
          {
            label: "Buchung eingegangen",
            value: "bookingReceived",
          },
          {
            label: "Buchung bestätigt",
            value: "bookingConfirmed",
          },
          {
            label: "Buchung storniert",
            value: "bookingCancelled",
          },
          {
            label: "Buchung Assistenz benötigt",
            value: "bookingAssistanceRequired",
          },
          {
            label: "Reservierung storniert",
            value: "reservationCancelled",
          },
          {
            label: "Veranstaltung abgesagt",
            value: "eventCancelled",
          },
          {
            label: "Veranstaltung beendet",
            value: "eventFinished",
          },
          {
            label: "Ermäßigung beantragt",
            value: "discountApplicationReceived",
          },
          {
            label: "Ermäßigung gewährt",
            value: "discountApplicationGranted",
          },
          {
            label: "Ermäßigung abgelehnt",
            value: "discountApplicationRejected",
          },
          {
            label: "Ermäßigungsnachweis Erinnerung",
            value: "discountApplicationReminder",
          },
          {
            label: "Zahlung erfasst",
            value: "paymentRegistered",
          },
          {
            label: "Zahlung überfällig",
            value: "paymentOverdue",
          },
          {
            label: "Frühbuchercode senden",
            value: "earlyAccessCodeNotification",
          },
          {
            label: "Passwort zurücksetzen",
            value: "passwordResetRequested",
          },
          {
            label: "Konto wird bald gelöscht wegen Inaktivität",
            value: "accountMarkedForDeletedDueToInactivity",
          },
          {
            label: "Konto gelöscht",
            value: "accountDeleted",
          },
          {
            label: "Konto gelöscht wegen Inaktivität",
            value: "accountDeletedDueToInactivity",
          },
        ],
      }),
      subject: text({
        label: "Betreff",
      }),
      content: text({
        label: "📄 Vorlage",
        ui: {
          displayMode: "textarea",
        },
      }),
      bccRecipients: relationship({
        ref: "User",
        many: true,
        label: "📧 BCC Empfänger",
      }),
    },
  }),
};
