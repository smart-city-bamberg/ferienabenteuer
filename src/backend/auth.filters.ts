import { Session } from "./auth.session";

export const isAdmin = ({ session }: { session?: Session }) =>
  session?.data?.isAdministrator === true;

export const isRoleAdmin = ({ session }: { session?: Session }) =>
  session?.data?.role === "administrator";

export const isAdminOrRoleAdmin = ({ session }: { session?: Session }) =>
  isAdmin({ session }) || isRoleAdmin({ session });

export const isRoleOrganizer = ({ session }: { session?: Session }) =>
  session?.data?.role === "organizer";

export const isRoleCustodian = ({ session }: { session?: Session }) =>
  session?.data?.role === "custodian" && session.data.isEnabled;

export const isTester = ({ session }: { session?: Session }) =>
  session?.data?.isTester === true;

export const isAdminOrRoleAdminOrRoleOrganizer = ({
  session,
}: {
  session?: Session;
}) =>
  isAdmin({ session }) ||
  isRoleAdmin({ session }) ||
  isRoleOrganizer({ session });

export const isAdminOrRoleAdminOrRoleCustodian = ({
  session,
}: {
  session?: Session;
}) =>
  isAdmin({ session }) ||
  isRoleAdmin({ session }) ||
  isRoleCustodian({ session });

export const isAdminOrRoleAdminOrRoleOrganizerOrRoleCustodian = ({
  session,
}: {
  session?: Session;
}) =>
  isAdmin({ session }) ||
  isRoleAdmin({ session }) ||
  isRoleOrganizer({ session }) ||
  isRoleCustodian({ session });

export const isAuthenticated = ({ session }: { session?: Session }) =>
  session?.data !== undefined;
