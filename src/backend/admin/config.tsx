// admin/config.tsx
/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx } from "@keystone-ui/core";

const CustomLogo = () => (
  <div css={{ display: "flex", flexDirection: "row", alignItems: "center" }}>
    <img
      alt="Logo"
      css={{ maxHeight: "4rem", marginRight: "1rem" }}
      src="/assets/Logo.png"
    ></img>
  </div>
);

export const components = {
  Logo: CustomLogo,
};
