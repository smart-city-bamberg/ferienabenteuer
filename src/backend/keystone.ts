import { config } from "@keystone-6/core";
import { lists } from "./schema";
import { withAuth, session } from "./auth";
import { environment } from "./environment";
import { Context } from ".keystone/types";
import { buildEventWorker } from "./messaging/queueWorker";
import { extendGraphqlSchema } from "./extensions/schemaExtensions";
import { InMemoryLRUCache } from "@apollo/utils.keyvaluecache";

import responseCachePlugin from "@apollo/server-plugin-response-cache";
import { Session } from "./auth.session";
import { markOverdueBookings } from "./extensions/worker/markOverdueBookings";
import { buildMailConnection } from "./extensions/mailConnection";
import {
  IMessageQueue,
  MessageQueue,
  buildEventQueue,
} from "./extensions/eventQueue";
import { markOverdueDiscounts } from "./extensions/worker/markOverdueDiscounts";
import { findFinishedEvents } from "./extensions/worker/findFinishedEvents";
import { isAdminOrRoleAdmin, isRoleOrganizer } from "./auth.filters";
import { findInactiveUsers } from "./extensions/worker/findInactiveUsers";
import { deleteInactiveUsers } from "./extensions/worker/deleteInactiveUsers";

const generator = `
generator client {
provider = "prisma-client-js"
}
`;

const generatorWithExtension = `
generator client {
provider = "prisma-client-js"
previewFeatures = ["extendedWhereUnique"]
}

generator dbml {
provider = "prisma-dbml-generator"
}
`;

let eventQueue: IMessageQueue;

const getEventQueue = () => eventQueue;

export default withAuth(
  config({
    db: {
      ...environment.db,
      extendPrismaSchema(schema) {
        return schema.replace(generator, generatorWithExtension);
      },
    },
    lists,
    session,
    server: {
      ...environment.server,
      extendExpressApp(app, context) {
        const sudoContext = context.sudo() as Context;
        const mail = buildMailConnection();
        eventQueue = new MessageQueue(buildEventQueue());

        const queueName = process.env.BULLMQ_QUEUE ?? "events";
        const queueHost = process.env.BULLMQ_HOST ?? "localhost";
        const queuePort = parseInt(process.env.BULLMQ_PORT ?? "6379");

        const eventWorker = buildEventWorker(
          sudoContext,
          mail,
          {
            host: queueHost,
            port: queuePort,
            name: queueName,
          },
          {
            mailFrom: process.env.MAILER_FROM ?? "Test <test@netscrapers.com>",
            hostUrl:
              process.env.FRONTEND_PUBLIC_NAME ?? "http://localhost:4200",
          },
          getEventQueue
        );

        markOverdueBookings(sudoContext);
        markOverdueDiscounts(sudoContext);
        findFinishedEvents(sudoContext, eventQueue);
        findInactiveUsers(sudoContext, eventQueue);
        deleteInactiveUsers(sudoContext, eventQueue);

        app.use(async (request, response, next) => {
          const requestContext = await context.withRequest(request);
          const session = requestContext.session as Session;

          if (session) {
            const data = session.data;
            request.headers.sessionId = data?.id;
            request.headers.role = data?.role;
            request.headers.isAdministator = data?.isAdministrator
              ? "true"
              : "false";
          }

          next();
        });

        if (environment.storage.invoice_store.kind !== "local") {
          throw new Error("No storage for invoices configured.");
        }

        const path = environment.storage.invoice_store.serverRoute!.path;

        app.use(async (request, response, next) => {
          if (!request.path.startsWith(path)) {
            next();
            return;
          }

          const requestContext = (await context.withRequest(
            request
          )) as Context;
          const session = requestContext.session as Session;

          if (!session) {
            response.sendStatus(401);
            return;
          }

          if (isAdminOrRoleAdmin(requestContext)) {
            next();
            return;
          }

          if (isRoleOrganizer(requestContext)) {
            const fileName = request.path.replace(`${path}/`, "");
            const decodedFileName = decodeURIComponent(fileName);

            const invoice = await requestContext.prisma.invoice.findFirst({
              where: {
                document_filename: {
                  equals: decodedFileName,
                },
              },
            });

            if (!invoice) {
              response.sendStatus(404);
              return;
            }

            const event = (await requestContext.query.Event.findOne({
              where: {
                id: invoice.eventId,
              },
              query: `id organizer { id }`,
            })) as { id: string; organizer: { id: string } };

            if (event.organizer.id !== session.data.organizer?.id) {
              response.sendStatus(403);
              return;
            }

            next();
            return;
          }

          response.sendStatus(404);
        });
      },
    },
    graphql: {
      ...environment.graphql,
      apolloConfig: {
        plugins: [
          responseCachePlugin({
            cache: new InMemoryLRUCache(),
            shouldReadFromCache: (requestContext: any) => {
              const sessionId =
                requestContext.request.http?.headers.get("sessionId");

              if (!sessionId) {
                return Promise.resolve(true);
              }

              const isAdministrator =
                requestContext.request.http?.headers.get("isAdministrator") ===
                "true";

              const role = requestContext.request.http?.headers.get("role");

              if (
                !isAdministrator &&
                !["administrator", "organizer"].includes(role)
              ) {
                return Promise.resolve(true);
              }

              return Promise.resolve(false);
            },
          }),
        ],
      },
    },
    storage: {
      ...environment.storage,
    },
    ui: {
      isAccessAllowed(context) {
        return context.session?.data?.isAdministrator === true;
      },
    },
    extendGraphqlSchema: extendGraphqlSchema(
      getEventQueue,
      environment.storage
    ),
  })
);
