import { Job } from "bullmq";
import { JobHandler } from "../queueWorker";
import { generateCode } from "../../codeGenerator";

export const createBookingWhenDiscountApplicationGrantedOrRejected: JobHandler =
  async (job: Job, { mailConnection, context, mailConfig, eventQueue }) => {
    const discount = job.data;
    const isRejected = discount.status === "rejected";

    const tickets = (await context.query.Ticket.findMany({
      where: {
        discountVoucher: {
          discount: { id: { equals: discount.id } },
        },
        eventTicket: { type: { equals: "participant" } },
        NOT: { reservation: null },
      },
      query: `      
      id
      discountVoucher {
        id
      }
      reservation {
        id
        custodian {
          id
        }
        event {
          id
          costs
        }
      }
      eventTicket {
        id
        type
      }      
    `,
    })) as {
      id: string;
      discountVoucher: {
        id: string;
      };
      reservation: {
        id: string;
        custodian: {
          id: string;
        };
        event: {
          id: string;
          costs: number;
        };
      };
      eventTicket: {
        id: string;
        type: string;
      };
    }[];

    if (tickets.length === 0) {
      console.warn(`no tickets for discount ${job.data.id} not found`);
      return;
    }

    const eventIds = tickets.reduce(
      (groups, ticket) =>
        groups.includes(ticket.reservation.event.id)
          ? groups
          : [...groups, ticket.reservation.event.id],
      new Array<string>()
    );

    const now = new Date();

    const result = await context.prisma.$transaction(async (transaction) => {
      const touchedReservations: string[] = [];
      const bookings = [];

      for (const eventId of eventIds) {
        const ticketsForEvent = tickets.filter(
          (ticket) => ticket.reservation.event.id === eventId
        );

        const booking = await transaction.booking.create({
          data: {
            custodianId: discount.custodianId,
            eventId: eventId,
            status: "pending",
            date: now,
            code: generateCode(),
          },
        });

        bookings.push(booking);

        for (const ticket of ticketsForEvent) {
          if (!touchedReservations.includes(ticket.reservation.id)) {
            touchedReservations.push(ticket.reservation.id);
          }

          await transaction.ticket.update({
            where: {
              id: ticket.id,
            },
            data: isRejected
              ? {
                  bookingId: booking.id,
                  reservationId: null,
                  price: ticket.reservation.event.costs,
                  discountVoucher: {
                    disconnect: {
                      id: ticket.discountVoucher.id,
                    },
                  },
                }
              : {
                  bookingId: booking.id,
                  reservationId: null,
                },
          });
        }
      }

      for (const reservationId of touchedReservations) {
        const reservationTickets = await transaction.ticket.findMany({
          where: {
            reservationId: reservationId,
          },
        });

        if (reservationTickets.length === 0) {
          await transaction.reservation.delete({
            where: {
              id: reservationId,
            },
          });
        }
      }

      return bookings;
    });

    eventQueue.addBulk(
      result.map((booking) => ({
        name: "bookingCreated",
        data: booking,
      }))
    );
  };
