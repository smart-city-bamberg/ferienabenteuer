import { JobHandler } from "../queueWorker";

export const cartTimeout: JobHandler = async (job, { context }) => {
  const cart = await context.prisma.cart.findFirst({
    where: {
      id: job.data.cartId,
      version: job.data.version,
    },
  });

  if (!cart) {
    console.log(
      `Cart ${job.data.cartId} with version ${job.data.version} does not exist any more.`
    );
    return;
  }

  await context.prisma.$transaction(async (transaction) => {
    await transaction.cartTicket.deleteMany({
      where: {
        cartId: cart.id,
      },
    });

    await transaction.cart.delete({
      where: {
        id: cart.id,
      },
    });
  });
};
