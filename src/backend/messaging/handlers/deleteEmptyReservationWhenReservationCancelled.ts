import { Job } from "bullmq";
import { JobHandler } from "../queueWorker";

export const deleteEmptyReservationWhenReservationCancelled: JobHandler =
  async (job: Job, { mailConnection, context, mailConfig, eventQueue }) => {
    const cancelledTicket = job.data;

    await context.prisma.$transaction(async (transaction) => {
      const reservationTickets = await transaction.ticket.findMany({
        where: {
          reservationId: cancelledTicket.reservationId,
          isDeleted: {
            equals: false,
          },
        },
      });

      if (reservationTickets.length === 0) {
        await transaction.reservation.update({
          where: {
            id: cancelledTicket.reservationId,
          },
          data: {
            isDeleted: true,
          },
        });
      }
    });
  };
