import { Job } from "bullmq";
import mustache from "mustache";
import { JobHandler } from "../../queueWorker";

export const sendEmailWhenUserDeletedAfterMarkedForDeletion: JobHandler =
  async (job: Job, { mailConnection, context, mailConfig }) => {
    const emailTemplate = (await context.query.EmailTemplate.findOne({
      where: {
        role: "accountDeletedDueToInactivity",
      },
      query: "role subject content",
    })) as {
      role: string;
      subject: string;
      content: string;
    } | null;

    if (!emailTemplate) {
      console.warn(`email template accountDeletedDueToInactivity not found`);
      return;
    }

    const renderedTemplate = mustache.render(emailTemplate.content, {
      Anrede:
        job.data.custodian.salutation === "mr"
          ? "Sehr geehrter Herr"
          : job.data.custodian.salutation == "ms"
          ? "Sehr geehrte Frau"
          : "Sehr geehrter Herr/Sehr geehrte Frau",
      Vorname: job.data.custodian.firstname,
      Nachname: job.data.custodian.surname,
    });

    const emailResult = await mailConnection.sendMail({
      from: mailConfig.mailFrom,
      to: job.data.custodian.user.email,
      subject: emailTemplate.subject,
      html: renderedTemplate,
    });

    console.log(
      `sent account deleted due to inactivity email to ${job.data.custodian.user.email}`,
      emailResult.response
    );
  };
