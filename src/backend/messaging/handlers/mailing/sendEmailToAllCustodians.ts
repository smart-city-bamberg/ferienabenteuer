import { Job } from "bullmq";
import { JobHandler } from "../../queueWorker";
import { toChunks } from "./helpers";

export const sendEmailToAllCustodians: JobHandler = async (
  job: Job,
  { mailConnection, context, mailConfig }
) => {
  const mail = job.data;

  const custodians = (await context.query.Custodian.findMany({
    query: `
        id    
        firstname
        surname    
        user {
          email
        }
      `,
  })) as {
    id: string;
    firstname: string;
    surname: string;
    user: {
      email: string;
    };
  }[];

  const chunks = toChunks(custodians, 100);

  for (const chunk of chunks) {
    const emailResult = await mailConnection.sendMail({
      from: mailConfig.mailFrom,
      bcc: chunk.map((custodian) => custodian.user.email),
      subject: mail.subject,
      html: mail.content,
    });

    console.log(
      `sent custom email to ${chunk.length} custodians`,
      emailResult.response
    );
  }
};
