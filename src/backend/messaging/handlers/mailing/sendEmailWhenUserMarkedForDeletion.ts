import { Job } from "bullmq";
import mustache from "mustache";
import { JobHandler } from "../../queueWorker";

export const sendEmailWhenUserMarkedForDeletion: JobHandler = async (
  job: Job,
  { mailConnection, context, mailConfig }
) => {
  const custodians = (await context.query.Custodian.findMany({
    where: {
      user: {
        id: {
          equals: job.data.id,
        },
      },
    },
    query: `
      id
      firstname
      surname
      salutation      
      user {
        email  
      }
    `,
  })) as {
    id: string;
    firstname: string;
    surname: string;
    salutation: string;
    user: {
      email: string;
    };
  }[];

  if (custodians.length === 0) {
    console.warn(`custodians for user ${job.data.id} not found`);
    return;
  }

  const custodian = custodians[0];

  const emailTemplate = (await context.query.EmailTemplate.findOne({
    where: {
      role: "accountMarkedForDeletedDueToInactivity",
    },
    query: "role subject content",
  })) as {
    role: string;
    subject: string;
    content: string;
  } | null;

  if (!emailTemplate) {
    console.warn(`email template sendEmailWhenUserMarkedForDeletion not found`);
    return;
  }

  const renderedTemplate = mustache.render(emailTemplate.content, {
    Anrede:
      custodian.salutation === "mr"
        ? "Sehr geehrter Herr"
        : custodian.salutation == "ms"
        ? "Sehr geehrte Frau"
        : "Sehr geehrter Herr/Sehr geehrte Frau",
    Vorname: custodian.firstname,
    Nachname: custodian.surname,
  });

  const emailResult = await mailConnection.sendMail({
    from: mailConfig.mailFrom,
    to: custodian.user.email,
    subject: emailTemplate.subject,
    html: renderedTemplate,
  });

  console.log(
    `sent account will be deleted email to ${custodian.user.email}`,
    emailResult.response
  );
};
