import { Job } from "bullmq";
import mustache from "mustache";
import { JobHandler } from "../../queueWorker";
import { buildCareDaysString, buildOrganizerContact } from "./helpers";

export const sendEmailWhenBookingCancelled: JobHandler = async (
  job: Job,
  { mailConnection, context, mailConfig }
) => {
  const cancelledTicket = job.data;

  const booking = (await context.query.Booking.findOne({
    where: {
      id: cancelledTicket.bookingId,
    },
    query: `
      code      
      date
      custodian {
        firstname
        surname
        salutation
        user {
          email
        }
      }
      event {
        name        
        contactPersonSalutation
        contactPersonFirstname
        contactPersonSurname
        emergencyPhone
        carePeriodCommitted {
          careDays( orderBy: { day: asc } ) {
            day
          }
        }
        organizer {
          name
          email
          phone
          users {
            email
          }
        }        
      }
      tickets {
        id
        child {
          firstname
          surname
        }
      }
      `,
  })) as {
    code: string;
    date: string;
    price: number;
    event: {
      name: string;
      contactPersonSalutation: string;
      contactPersonFirstname: string;
      contactPersonSurname: string;
      emergencyPhone: string;
      carePeriodCommitted: {
        careDays: {
          day: string;
        }[];
      };
      organizer: {
        name: string;
        email: string;
        phone: string;
        users: {
          email: string;
        }[];
      };
    };
    tickets: {
      id: string;
      child: {
        firstname: string;
        surname: string;
      };
    }[];
    custodian: {
      firstname: string;
      surname: string;
      salutation: string;
      user: {
        email: string;
      };
    };
  } | null;

  if (!booking) {
    console.warn(`booking ${cancelledTicket.bookingId} not found`);
    return;
  }

  const event = booking.event;
  const custodian = booking.custodian;
  const ticket = booking.tickets.find(
    (ticket) => ticket.id === cancelledTicket.id
  );

  if (!ticket) {
    console.warn(`ticket ${cancelledTicket.id} not found`);
    return;
  }

  const child = ticket.child;

  const emailTemplate = (await context.query.EmailTemplate.findOne({
    where: {
      role: "bookingCancelled",
    },
    query: "role subject content",
  })) as {
    role: string;
    subject: string;
    content: string;
  } | null;

  if (!emailTemplate) {
    console.warn(`email template ${job.name} not found`);
    return;
  }

  const careDays = booking.event.carePeriodCommitted.careDays;
  const vonBis = buildCareDaysString(careDays);

  const renderedTemplate = mustache.render(emailTemplate.content, {
    Anrede:
      custodian.salutation === "mr"
        ? "Sehr geehrter Herr"
        : custodian.salutation == "ms"
        ? "Sehr geehrte Frau"
        : "Sehr geehrter Herr/Sehr geehrte Frau",
    Vorname: custodian.firstname,
    Nachname: custodian.surname,
    "Name Kind": `${child.firstname} ${child.surname}`,
    "Name Veranstaltung": event.name,
    "Datum Veranstaltung von bis": vonBis,
    Buchungsnummer: booking.code,
    "Kontaktdaten Veranstalter": buildOrganizerContact(event),
  });

  const emailResult = await mailConnection.sendMail({
    from: mailConfig.mailFrom,
    to: custodian.user.email,
    bcc: booking.event.organizer.users.map((user) => user.email),
    subject: emailTemplate.subject,
    html: renderedTemplate,
  });

  console.log(
    `sent booking cancelled email to ${custodian.user.email}`,
    emailResult.response
  );
};
