import { Job } from "bullmq";
import mustache from "mustache";
import { JobHandler } from "../../queueWorker";
import { buildCareDaysString, buildOrganizerContact } from "./helpers";

export const sendEmailWhenWaitinglistTicketConvertedToBooking: JobHandler =
  async (job: Job, { mailConnection, context, mailConfig }) => {
    const booking = (await context.query.Booking.findOne({
      where: {
        id: job.data.id,
      },
      query: `
      code
      price
      date
      custodian {
        firstname
        surname
        salutation
        user {
          email
        }
      }
      event {
        name
        description
        careTimeStart
        careTimeEnd
        locationCity
        locationZipcode
        locationStreet
        contactPersonSalutation
        contactPersonFirstname
        contactPersonSurname
        emergencyPhone
        organizer {
          name
          email
          phone
          bankIban
        }
        carePeriodCommitted {
          careDays( orderBy: { day: asc } ) {
            day
          }
        }
      }
      tickets {
        child {
          firstname
          surname
        }
      }
      `,
    })) as {
      code: string;
      date: string;
      price: number;
      event: {
        name: string;
        description: string;
        careTimeStart: string;
        careTimeEnd: string;
        locationCity: string;
        locationZipcode: string;
        locationStreet: string;
        contactPersonSalutation: string;
        contactPersonFirstname: string;
        contactPersonSurname: string;
        emergencyPhone: string;
        carePeriodCommitted: {
          careDays: {
            day: string;
          }[];
        };
        organizer: {
          name: string;
          email: string;
          phone: string;
          bankIban: string;
        };
      };
      tickets: {
        child: {
          firstname: string;
          surname: string;
        };
      }[];
      custodian: {
        firstname: string;
        surname: string;
        salutation: string;
        user: {
          email: string;
        };
      };
    } | null;

    if (!booking) {
      console.warn(`booking ${job.data.id} not found`);
      return;
    }

    const emailTemplate = (await context.query.EmailTemplate.findOne({
      where: {
        role: "waitingListTicketConvertedToBooking",
      },
      query: "role subject content",
    })) as {
      role: string;
      subject: string;
      content: string;
    } | null;

    if (!emailTemplate) {
      console.warn(`email template ${job.name} not found`);
      return;
    }

    const ticksIn14Days = 14 * 24 * 60 * 60 * 1000;

    const dateIn14Days = new Date(
      new Date(booking.date).getTime() + ticksIn14Days
    );

    const careDays = booking.event.carePeriodCommitted.careDays;
    const vonBis = buildCareDaysString(careDays);

    const custodian = booking.custodian;
    const event = booking.event;
    const organizer = event.organizer;
    const children = booking.tickets.map((ticket) => ticket.child);
    const renderedTemplate = mustache.render(emailTemplate.content, {
      Anrede:
        custodian.salutation === "mr"
          ? "Sehr geehrter Herr"
          : custodian.salutation == "ms"
          ? "Sehr geehrte Frau"
          : "Sehr geehrter Herr/Sehr geehrte Frau",
      Vorname: custodian.firstname,
      Nachname: custodian.surname,
      "Name Kind": children
        .map((child) => `${child.firstname} ${child.surname}`)
        .join(", "),
      "Nachname Kind": custodian.surname,
      "Name Veranstaltung": event.name,
      Buchungsnummer: booking.code,
      "Name Veranstalter": organizer.name,
      "IBAN Veranstalter": organizer.bankIban,
      Betrag: booking.price.toLocaleString(undefined, {
        minimumFractionDigits: 2,
        maximumFractionDigits: 2,
      }),
      "Beschreibung der Veranstaltung": event.description,
      "von bis": `${event.careTimeStart} bis ${event.careTimeEnd} Uhr`,
      "Ort/Treffpunkt": `${event.locationZipcode} ${event.locationCity}, ${event.locationStreet}`,
      "Kontaktdaten Veranstalter": buildOrganizerContact(event),
      "Datum Veranstaltung von bis": vonBis,
      "Datum in 14 Tagen": `${dateIn14Days
        .getDate()
        .toString()
        .padStart(2, "0")}.${(dateIn14Days.getMonth() + 1)
        .toString()
        .padStart(2, "0")}.${dateIn14Days.getFullYear()}`,
    });

    const emailResult = await mailConnection.sendMail({
      from: mailConfig.mailFrom,
      to: booking.custodian.user.email,
      subject: emailTemplate.subject,
      html: renderedTemplate,
    });

    console.log(
      `sent waitingListTicketConvertedToBooking email to ${booking.custodian.user.email}`,
      emailResult.response
    );
  };
