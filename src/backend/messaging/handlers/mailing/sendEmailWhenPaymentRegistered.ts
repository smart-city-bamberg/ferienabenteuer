import { Job } from "bullmq";
import mustache from "mustache";
import { JobHandler } from "../../queueWorker";
import { buildCareDaysString, buildOrganizerContact } from "./helpers";

export const sendEmailWhenPaymentRegistered: JobHandler = async (
  job: Job,
  { mailConnection, context, mailConfig }
) => {
  const booking = (await context.query.Booking.findOne({
    where: {
      id: job.data.id,
    },
    query: `
      code      
      date
      custodian {
        firstname
        surname
        salutation
        user {
          email
        }
      }
      event {
        name        
        contactPersonSalutation
        contactPersonFirstname
        contactPersonSurname
        emergencyPhone        
        carePeriodCommitted {
          careDays( orderBy: { day: asc } ) {
            day
          }
        }
        organizer {          
          name
          email
          phone
          users {
            email
          }
        }
      }
      tickets {
        child {
          firstname
          surname
        }
      }
      `,
  })) as {
    code: string;
    date: string;
    event: {
      name: string;
      contactPersonSalutation: string;
      contactPersonFirstname: string;
      contactPersonSurname: string;
      emergencyPhone: string;
      carePeriodCommitted: {
        careDays: {
          day: string;
        }[];
      };
      organizer: {
        name: string;
        email: string;
        phone: string;
        users: {
          email: string;
        }[];
      };
    };
    tickets: {
      child: {
        firstname: string;
        surname: string;
      };
    }[];
    custodian: {
      firstname: string;
      surname: string;
      salutation: string;
      user: {
        email: string;
      };
    };
  } | null;

  if (!booking) {
    console.warn(`booking ${job.data.id} not found`);
    return;
  }

  const emailTemplate = (await context.query.EmailTemplate.findOne({
    where: {
      role: "paymentRegistered",
    },
    query: `
      role
      subject
      content
      bccRecipients {
        id
        email
      }
    `,
  })) as {
    role: string;
    subject: string;
    content: string;
    bccRecipients: {
      id: string;
      email: string;
    }[];
  } | null;

  if (!emailTemplate) {
    console.warn(`email template ${job.name} not found`);
    return;
  }

  const careDays = booking.event.carePeriodCommitted.careDays;
  const vonBis = buildCareDaysString(careDays);

  const custodian = booking.custodian;
  const event = booking.event;
  const children = booking.tickets.map((ticket) => ticket.child);

  const renderedTemplate = mustache.render(emailTemplate.content, {
    Anrede:
      custodian.salutation === "mr"
        ? "Sehr geehrter Herr"
        : custodian.salutation == "ms"
        ? "Sehr geehrte Frau"
        : "Sehr geehrter Herr/Sehr geehrte Frau",
    Vorname: custodian.firstname,
    Nachname: custodian.surname,
    "Name Kind": children
      .map((child) => `${child.firstname} ${child.surname}`)
      .join(", "),
    "Name Veranstaltung": event.name,
    Buchungsnummer: booking.code,
    "Kontaktdaten Veranstalter": buildOrganizerContact(event),
    "Datum Veranstaltung von bis": vonBis,
  });

  const emailResult = await mailConnection.sendMail({
    from: mailConfig.mailFrom,
    to: booking.custodian.user.email,
    bcc: booking.event.organizer.users.map((user) => user.email),
    subject: emailTemplate.subject,
    html: renderedTemplate,
  });

  console.log(
    `sent payment registered email to ${booking.custodian.user.email}`,
    emailResult.response
  );
};
