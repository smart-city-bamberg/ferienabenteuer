import { Job } from "bullmq";
import mustache from "mustache";
import { JobHandler } from "../../queueWorker";
import { buildCareDaysString } from "./helpers";

export const sendEmailWhenBookingCreatedAndAssistanceRequired: JobHandler =
  async (job: Job, { mailConnection, context, mailConfig }) => {
    const booking = (await context.query.Booking.findOne({
      where: {
        id: job.data.id,
      },
      query: `
      code            
      custodian {
        firstname
        surname
        salutation
        user {
          email
        }
      }
      event {
        name                
        carePeriodCommitted {
          careDays( orderBy: { day: asc } ) {
            day
          }
        }
      }
      tickets {
        child {
          firstname
          surname
          assistanceRequired
        }
      }
      `,
    })) as {
      code: string;
      event: {
        name: string;
        carePeriodCommitted: {
          careDays: {
            day: string;
          }[];
        };
      };
      tickets: {
        child: {
          firstname: string;
          surname: string;
          assistanceRequired: boolean;
        };
      }[];
      custodian: {
        firstname: string;
        surname: string;
        salutation: string;
        user: {
          email: string;
        };
      };
    } | null;

    if (!booking) {
      console.warn(`booking ${job.data.id} not found`);
      return;
    }

    const assistanceRequiredTickets = booking.tickets.filter(
      (ticket) => ticket.child.assistanceRequired
    );

    if (assistanceRequiredTickets.length === 0) {
      return;
    }

    const emailTemplate = (await context.query.EmailTemplate.findOne({
      where: {
        role: "bookingAssistanceRequired",
      },
      query: "role subject content",
    })) as {
      role: string;
      subject: string;
      content: string;
    } | null;

    if (!emailTemplate) {
      console.warn(`email template bookingAssistanceRequired not found`);
      return;
    }

    const custodian = booking.custodian;
    const event = booking.event;
    const careDays = booking.event.carePeriodCommitted.careDays;
    const vonBis = buildCareDaysString(careDays);

    for (const ticket of assistanceRequiredTickets) {
      const child = ticket.child;

      const renderedTemplate = mustache.render(emailTemplate.content, {
        Anrede:
          custodian.salutation === "mr"
            ? "Sehr geehrter Herr"
            : custodian.salutation == "ms"
            ? "Sehr geehrte Frau"
            : "Sehr geehrter Herr/Sehr geehrte Frau",
        Vorname: custodian.firstname,
        Nachname: custodian.surname,
        "Name Kind": `${child.firstname} ${child.surname}`,
        "Name Veranstaltung": event.name,
        Buchungsnummer: booking.code,
        "Datum Veranstaltung von bis": vonBis,
      });

      const emailResult = await mailConnection.sendMail({
        from: mailConfig.mailFrom,
        to: booking.custodian.user.email,
        subject: emailTemplate.subject,
        html: renderedTemplate,
      });

      console.log(
        `sent booking email to ${booking.custodian.user.email}`,
        emailResult.response
      );
    }
  };
