import mustache from "mustache";
import { JobHandler } from "../../queueWorker";

export const sendEmailWhenPasswortResetRequested: JobHandler = async (
  job,
  { context, mailConnection, mailConfig }
) => {
  const custodian = (await context.prisma.custodian.findFirst({
    where: {
      userId: job.data.id,
    },
  })) as {
    firstname: string;
    surname: string;
    salutation: string;
  } | null;

  if (!custodian) {
    console.warn(
      `custodian for user ${job.data.id} with mail ${job.data.email} not found`
    );
    return;
  }

  const passwordResetToken = (await context.prisma.passwordResetToken.findFirst(
    {
      where: {
        userId: job.data.id,
      },
    }
  )) as {
    validationToken: string;
  } | null;

  if (!passwordResetToken) {
    console.warn(
      `passwordResetToken for new user ${job.data.id} with mail ${job.data.email} not found`
    );
    return;
  }

  const emailTemplate = (await context.query.EmailTemplate.findOne({
    where: {
      role: "passwordResetRequested",
    },
    query: "role subject content",
  })) as {
    role: string;
    subject: string;
    content: string;
  } | null;

  if (!emailTemplate) {
    console.warn(`email template ${job.name} not found`);
    return;
  }

  const renderedTemplate = mustache.render(emailTemplate.content, {
    Anrede:
      custodian.salutation === "mr"
        ? "Sehr geehrter Herr"
        : custodian.salutation == "ms"
        ? "Sehr geehrte Frau"
        : "Sehr geehrter Herr/Sehr geehrte Frau",
    Vorname: custodian.firstname,
    Nachname: custodian.surname,
    Code: passwordResetToken.validationToken,
    Bestätigungslink: `${mailConfig.hostUrl}/neues-passwort?token=${passwordResetToken.validationToken}`,
  });

  const emailResult = await mailConnection.sendMail({
    from: mailConfig.mailFrom,
    to: job.data.email,
    subject: emailTemplate.subject,
    html: renderedTemplate,
  });

  console.log(
    `sent password reset email to ${job.data.email}`,
    emailResult.response
  );
};
