import { Job } from "bullmq";
import mustache from "mustache";
import { JobHandler } from "../../queueWorker";
import { buildCareDaysString, buildOrganizerContact } from "./helpers";

export const sendEmailWhenTicketCancelled: JobHandler = async (
  job: Job,
  { mailConnection, context, mailConfig }
) => {
  const cancelledTicket = job.data;

  const ticket = (await context.query.Ticket.findOne({
    where: {
      id: cancelledTicket.id,
    },
    query: `
      id
      child {
        firstname
        surname
        custodian {
          firstname
          surname
          salutation
          user {
            email
          }
        }
      }
      booking {
        code
        id
        event {
          name        
          contactPersonSalutation
          contactPersonFirstname
          contactPersonSurname
          emergencyPhone
          carePeriodCommitted {
            careDays( orderBy: { day: asc } ) {
              day
            }
          }
          organizer {
            name            
            email
            phone
            users {
              email
            }
          }        
        }  
      }
      reservation {
        code
        id
        event {
          name        
          contactPersonSalutation
          contactPersonFirstname
          contactPersonSurname
          emergencyPhone
          carePeriodCommitted {
            careDays( orderBy: { day: asc } ) {
              day
            }
          }
          organizer {
            name            
            email
            phone
            users {
              email
            }
          }        
        }  
      }      
      `,
  })) as {
    id: string;
    child: {
      firstname: string;
      surname: string;
      custodian: {
        firstname: string;
        surname: string;
        salutation: string;
        user: {
          email: string;
        };
      };
    };
    booking: {
      code: string;
      id: string;
      event: {
        name: string;
        contactPersonSalutation: string;
        contactPersonFirstname: string;
        contactPersonSurname: string;
        emergencyPhone: string;
        carePeriodCommitted: {
          careDays: {
            day: string;
          }[];
        };
        organizer: {
          name: string;
          email: string;
          phone: string;
          users: {
            email: string;
          }[];
        };
      };
    } | null;
    reservation: {
      code: string;
      id: string;
      event: {
        name: string;
        contactPersonSalutation: string;
        contactPersonFirstname: string;
        contactPersonSurname: string;
        emergencyPhone: string;
        carePeriodCommitted: {
          careDays: {
            day: string;
          }[];
        };
        organizer: {
          name: string;
          email: string;
          phone: string;
          users: {
            email: string;
          }[];
        };
      };
    } | null;
  } | null;

  if (!ticket) {
    console.warn(`ticket ${cancelledTicket.id} not found`);
    return;
  }

  const event = ticket.booking?.event ?? ticket.reservation?.event;

  if (!event) {
    console.warn(`event for cancelled ticket ${cancelledTicket.id} not found`);
    return;
  }

  const child = ticket.child;
  const custodian = ticket.child.custodian;
  const booking = ticket.booking;
  const reservation = ticket.reservation;

  const emailTemplate = (await context.query.EmailTemplate.findOne({
    where: {
      role: "ticketCancelled",
    },
    query: "role subject content",
  })) as {
    role: string;
    subject: string;
    content: string;
  } | null;

  if (!emailTemplate) {
    console.warn(`email template ${job.name} not found`);
    return;
  }

  const careDays = event.carePeriodCommitted.careDays;
  const vonBis = buildCareDaysString(careDays);

  const renderedTemplate = mustache.render(emailTemplate.content, {
    Anrede:
      custodian.salutation === "mr"
        ? "Sehr geehrter Herr"
        : custodian.salutation == "ms"
        ? "Sehr geehrte Frau"
        : "Sehr geehrter Herr/Sehr geehrte Frau",
    Vorname: custodian.firstname,
    Nachname: custodian.surname,
    "Name Kind": `${child.firstname} ${child.surname}`,
    "Name Veranstaltung": event.name,
    "Datum Veranstaltung von bis": vonBis,
    Buchungsnummer: booking?.code ?? reservation?.code,
    "Kontaktdaten Veranstalter": buildOrganizerContact(event),
  });

  const emailResult = await mailConnection.sendMail({
    from: mailConfig.mailFrom,
    to: custodian.user.email,
    subject: emailTemplate.subject,
    html: renderedTemplate,
  });

  console.log(
    `sent ticket cancelled email to ${custodian.user.email}`,
    emailResult.response
  );
};
