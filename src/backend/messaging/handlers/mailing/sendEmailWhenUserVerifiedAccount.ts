import { Job } from "bullmq";
import mustache from "mustache";
import { JobHandler } from "../../queueWorker";

export const sendEmailWhenUserVerifiedAccount: JobHandler = async (
  job: Job,
  { mailConnection, context, mailConfig }
) => {
  const custodian = (await context.prisma.custodian.findFirst({
    where: {
      userId: job.data.id,
    },
  })) as {
    firstname: string;
    surname: string;
    salutation: string;
  } | null;

  if (!custodian) {
    console.warn(
      `custodian for new user ${job.data.id} with mail ${job.data.email} not found`
    );
    return;
  }

  const emailTemplate = (await context.query.EmailTemplate.findOne({
    where: {
      role: job.name,
    },
    query: "role subject content",
  })) as {
    role: string;
    subject: string;
    content: string;
  } | null;

  if (!emailTemplate) {
    console.warn(`email template ${job.name} not found`);
    return;
  }

  const renderedTemplate = mustache.render(emailTemplate.content, {
    Anrede:
      custodian.salutation === "mr"
        ? "Sehr geehrter Herr"
        : custodian.salutation == "ms"
        ? "Sehr geehrte Frau"
        : "Sehr geehrter Herr/Sehr geehrte Frau",
    Vorname: custodian.firstname,
    Nachname: custodian.surname,
  });

  const emailResult = await mailConnection.sendMail({
    from: mailConfig.mailFrom,
    to: job.data.email,
    subject: emailTemplate.subject,
    html: renderedTemplate,
  });

  console.log(
    `sent verifcation success email to ${job.data.email}`,
    emailResult.response
  );
};
