import { Job } from "bullmq";
import mustache from "mustache";
import { JobHandler } from "../../queueWorker";

export const sendEmailWhenDiscountApplicationReceived: JobHandler = async (
  job: Job,
  { mailConnection, context, mailConfig }
) => {
  const custodian = (await context.query.Custodian.findOne({
    where: {
      id: job.data.custodianId,
    },
    query: "firstname surname salutation user { email }",
  })) as {
    firstname: string;
    surname: string;
    salutation: string;
    user: {
      email: string;
    };
  } | null;

  if (!custodian) {
    console.warn(`custodian for discount ${job.data.id} not found`);
    return;
  }

  const emailTemplate = (await context.query.EmailTemplate.findOne({
    where: {
      role: job.name,
    },
    query: `
      role
      subject
      content
      bccRecipients {
        id
        email
      }
    `,
  })) as {
    role: string;
    subject: string;
    content: string;
    bccRecipients: {
      id: string;
      email: string;
    }[];
  } | null;

  if (!emailTemplate) {
    console.warn(`email template ${job.name} not found`);
    return;
  }

  const ticksIn14Days = 14 * 24 * 60 * 60 * 1000;

  const dateIn14Days = new Date(
    new Date(job.data.submissionDate).getTime() + ticksIn14Days
  );

  const renderedTemplate = mustache.render(emailTemplate.content, {
    Anrede:
      custodian.salutation === "mr"
        ? "Sehr geehrter Herr"
        : custodian.salutation == "ms"
        ? "Sehr geehrte Frau"
        : "Sehr geehrter Herr/Sehr geehrte Frau",
    Vorname: custodian.firstname,
    Nachname: custodian.surname,
    "Datum in 14 Tagen": `${dateIn14Days
      .getDate()
      .toString()
      .padStart(2, "0")}.${(dateIn14Days.getMonth() + 1)
      .toString()
      .padStart(2, "0")}.${dateIn14Days.getFullYear()}`,
  });

  const emailResult = await mailConnection.sendMail({
    from: mailConfig.mailFrom,
    to: custodian.user.email,
    bcc: emailTemplate.bccRecipients.map((user) => user.email),
    subject: emailTemplate.subject,
    html: renderedTemplate,
  });

  console.log(
    `sent discount application received email to ${job.data.email}`,
    emailResult.response
  );
};
