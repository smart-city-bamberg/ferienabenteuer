import { Job } from "bullmq";
import mustache from "mustache";
import { JobHandler } from "../../queueWorker";
import { buildCareDaysString, buildOrganizerContact } from "./helpers";

export const sendEmailWhenWaitinglistTicketSlotAdvanced: JobHandler = async (
  job: Job,
  { mailConnection, context, mailConfig }
) => {
  const eventTicket = (await context.query.EventTicket.findOne({
    where: {
      id: job.data.id,
    },
    query: `          
      number
      bookedTicket {
        child {
          firstname
          surname
        }
        reservation {
          custodian {
            salutation
            firstname
            surname
            user {
              email
            }
          }
          date
          event {
            name
            description
            careTimeStart
            careTimeEnd
            locationCity
            locationZipcode
            locationStreet
            contactPersonSalutation
            contactPersonFirstname
            contactPersonSurname
            emergencyPhone
            organizer {
              name
              email
              phone
              users {
                email
              }
            }
            carePeriodCommitted {
              careDays( orderBy: { day: asc } ) {
                day
              }
            }
          }    
        }
      }            
      `,
  })) as {
    number: string;
    bookedTicket: {
      child: {
        firstname: string;
        surname: string;
      };
      reservation: {
        date: string;
        custodian: {
          firstname: string;
          surname: string;
          salutation: string;
          user: {
            email: string;
          };
        };
        event: {
          name: string;
          description: string;
          careTimeStart: string;
          careTimeEnd: string;
          locationCity: string;
          locationZipcode: string;
          locationStreet: string;
          contactPersonSalutation: string;
          contactPersonFirstname: string;
          contactPersonSurname: string;
          emergencyPhone: string;
          carePeriodCommitted: {
            careDays: {
              day: string;
            }[];
          };
          organizer: {
            name: string;
            email: string;
            phone: string;
            users: {
              email: string;
            }[];
          };
        };
      };
    };
  } | null;

  if (!eventTicket) {
    console.warn(`eventTicket ${job.data.id} not found`);
    return;
  }

  const emailTemplate = (await context.query.EmailTemplate.findOne({
    where: {
      role: "waitingListTicketSlotAdvanced",
    },
    query: `
      role
      subject
      content     
    `,
  })) as {
    role: string;
    subject: string;
    content: string;
  } | null;

  if (!emailTemplate) {
    console.warn(`email template waitingListTicketSlotAdvanced not found`);
    return;
  }

  const bookedTicket = eventTicket.bookedTicket;
  const child = bookedTicket.child;
  const reservation = bookedTicket.reservation;
  const ticksIn14Days = 14 * 24 * 60 * 60 * 1000;

  const dateIn14Days = new Date(
    new Date(reservation.date).getTime() + ticksIn14Days
  );

  const custodian = reservation.custodian;
  const event = reservation.event;
  const careDays = event.carePeriodCommitted.careDays;
  const vonBis = buildCareDaysString(careDays);

  const renderedTemplate = mustache.render(emailTemplate.content, {
    Anrede:
      custodian.salutation === "mr"
        ? "Sehr geehrter Herr"
        : custodian.salutation == "ms"
        ? "Sehr geehrte Frau"
        : "Sehr geehrter Herr/Sehr geehrte Frau",
    Vorname: custodian.firstname,
    Nachname: custodian.surname,
    "Name Veranstaltung": event.name,
    "Name Veranstalter": event.organizer.name,
    "Name Kind": `${child.firstname} ${child.surname}`,
    "Beschreibung der Veranstaltung": event.description,
    "von bis": `${event.careTimeStart} bis ${event.careTimeEnd} Uhr`,
    "Ort/Treffpunkt": `${event.locationZipcode} ${event.locationCity}, ${event.locationStreet}`,
    "Kontaktdaten Veranstalter": buildOrganizerContact(event),
    Wartelistenplatz: eventTicket.number,
    "Datum Veranstaltung von bis": vonBis,
  });

  const emailResult = await mailConnection.sendMail({
    from: mailConfig.mailFrom,
    to: custodian.user.email,
    subject: emailTemplate.subject,
    html: renderedTemplate,
  });

  console.log(
    `sent waitingListTicketSlotAdvanced email to ${custodian.user.email}`,
    emailResult.response
  );
};
