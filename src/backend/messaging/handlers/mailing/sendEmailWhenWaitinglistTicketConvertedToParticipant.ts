import { Job } from "bullmq";
import mustache from "mustache";
import { JobHandler } from "../../queueWorker";
import { buildCareDaysString, buildOrganizerContact } from "./helpers";

export const sendEmailWhenWaitinglistTicketConvertedToParticipant: JobHandler =
  async (job: Job, { mailConnection, context, mailConfig }) => {
    const reservation = (await context.query.Reservation.findOne({
      where: {
        id: job.data.id,
      },
      query: `
      code
      price
      date
      custodian {
        firstname
        surname
        salutation
        user {
          email
        }
      }
      event {
        name
        description
        careTimeStart
        careTimeEnd
        locationCity
        locationZipcode
        locationStreet
        contactPersonSalutation
        contactPersonFirstname
        contactPersonSurname
        emergencyPhone
        organizer {
          name
          email
          phone
          bankIban
        }
        carePeriodCommitted {
          careDays( orderBy: { day: asc } ) {
            day
          }
        }
      }
      tickets {
        child {
          firstname
          surname
        }
      }
      `,
    })) as {
      code: string;
      date: string;
      price: number;
      event: {
        name: string;
        description: string;
        careTimeStart: string;
        careTimeEnd: string;
        locationCity: string;
        locationZipcode: string;
        locationStreet: string;
        contactPersonSalutation: string;
        contactPersonFirstname: string;
        contactPersonSurname: string;
        emergencyPhone: string;
        carePeriodCommitted: {
          careDays: {
            day: string;
          }[];
        };
        organizer: {
          name: string;
          email: string;
          phone: string;
          bankIban: string;
        };
      };
      tickets: {
        child: {
          firstname: string;
          surname: string;
        };
      }[];
      custodian: {
        firstname: string;
        surname: string;
        salutation: string;
        user: {
          email: string;
        };
      };
    } | null;

    if (!reservation) {
      console.warn(`reservation ${job.data.id} not found`);
      return;
    }

    const emailTemplate = (await context.query.EmailTemplate.findOne({
      where: {
        role: "waitingListTicketConvertedToParticipant",
      },
      query: "role subject content",
    })) as {
      role: string;
      subject: string;
      content: string;
    } | null;

    if (!emailTemplate) {
      console.warn(
        `email template 'waitingListTicketConvertedToParticipant' not found`
      );
      return;
    }

    const ticksIn14Days = 14 * 24 * 60 * 60 * 1000;

    const now = new Date();
    const dateIn14Days = new Date(now.getTime() + ticksIn14Days);

    const careDays = reservation.event.carePeriodCommitted.careDays;
    const vonBis = buildCareDaysString(careDays);

    const custodian = reservation.custodian;
    const event = reservation.event;
    const organizer = event.organizer;
    const child = reservation.tickets[0].child;
    const renderedTemplate = mustache.render(emailTemplate.content, {
      Anrede:
        custodian.salutation === "mr"
          ? "Sehr geehrter Herr"
          : custodian.salutation == "ms"
          ? "Sehr geehrte Frau"
          : "Sehr geehrter Herr/Sehr geehrte Frau",
      Vorname: custodian.firstname,
      Nachname: custodian.surname,
      "Name Kind": `${child.firstname} ${child.surname}`,
      "Nachname Kind": custodian.surname,
      "Name Veranstaltung": event.name,
      Buchungsnummer: reservation.code,
      "Name Veranstalter": organizer.name,
      "IBAN Veranstalter": organizer.bankIban,
      "Beschreibung der Veranstaltung": event.description,
      "von bis": `${event.careTimeStart} bis ${event.careTimeEnd} Uhr`,
      "Ort/Treffpunkt": `${event.locationZipcode} ${event.locationCity}, ${event.locationStreet}`,
      "Kontaktdaten Veranstalter": buildOrganizerContact(event),
      "Datum Veranstaltung von bis": vonBis,
      "Datum in 14 Tagen": `${dateIn14Days
        .getDate()
        .toString()
        .padStart(2, "0")}.${(dateIn14Days.getMonth() + 1)
        .toString()
        .padStart(2, "0")}.${dateIn14Days.getFullYear()}`,
    });

    const emailResult = await mailConnection.sendMail({
      from: mailConfig.mailFrom,
      to: custodian.user.email,
      subject: emailTemplate.subject,
      html: renderedTemplate,
    });

    console.log(
      `sent waitingListTicketConvertedToParticipant email to ${custodian.user.email}`,
      emailResult.response
    );
  };
