import { Job } from "bullmq";
import mustache from "mustache";
import { JobHandler } from "../../queueWorker";
import { toChunks } from "./helpers";

export const sendEmailToCustodiansWhenEventFinished: JobHandler = async (
  job: Job,
  { mailConnection, context, mailConfig }
) => {
  const finishedEvent = job.data;

  const event = (await context.query.Event.findOne({
    where: {
      id: finishedEvent.id,
    },
    query: `            
      id
      name              
      `,
  })) as {
    id: string;
    name: string;
  } | null;

  if (!event) {
    console.warn(`event ${finishedEvent.id} not found`);
    return;
  }

  const custodians = (await context.query.Custodian.findMany({
    where: {
      bookings: {
        some: {
          isDeleted: {
            equals: false,
          },
          event: {
            id: {
              equals: event.id,
            },
          },
        },
      },
    },
    query: `
        id    
        firstname
        surname    
        user {
          email
        }
      `,
  })) as {
    id: string;
    firstname: string;
    surname: string;
    user: {
      email: string;
    };
  }[];

  const emailTemplate = (await context.query.EmailTemplate.findOne({
    where: {
      role: "eventFinished",
    },
    query: "role subject content",
  })) as {
    role: string;
    subject: string;
    content: string;
  } | null;

  if (!emailTemplate) {
    console.warn(`email template eventFinished not found`);
    return;
  }

  const renderedTemplate = mustache.render(emailTemplate.content, {
    Name: event.name,
  });

  const chunks = toChunks(custodians, 100);

  for (const chunk of chunks) {
    const emailResult = await mailConnection.sendMail({
      from: mailConfig.mailFrom,
      bcc: chunk.map((custodian) => custodian.user.email),
      subject: emailTemplate.subject,
      html: renderedTemplate,
    });

    console.log(
      `sent event finished email to ${chunk.length} custodians`,
      emailResult.response
    );
  }
};
