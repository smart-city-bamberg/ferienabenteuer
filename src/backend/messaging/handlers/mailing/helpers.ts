export const buildCareDaysString = (careDays: { day: string }[]) => {
  const firstDay = new Date(careDays[0].day);

  const firstDayString = `${firstDay.getDate().toString().padStart(2, "0")}.${(
    firstDay.getMonth() + 1
  )
    .toString()
    .padStart(2, "0")}.${firstDay.getFullYear()}`;

  if (careDays.length === 1) {
    return firstDayString;
  }

  const lastDay = new Date(careDays[careDays.length - 1].day);

  const lastDayString = `${lastDay.getDate().toString().padStart(2, "0")}.${(
    lastDay.getMonth() + 1
  )
    .toString()
    .padStart(2, "0")}.${lastDay.getFullYear()}`;

  return `${firstDayString} bis ${lastDayString}`;
};

export const buildOrganizerContact = (event: {
  contactPersonSalutation: string;
  contactPersonFirstname: string;
  contactPersonSurname: string;
  emergencyPhone: string;
  organizer: {
    email: string;
    phone: string;
  };
}) => {
  return `${
    event.contactPersonSalutation === "mr"
      ? "Herr"
      : event.contactPersonSalutation === "ms"
      ? "Frau"
      : ""
  }
  ${event.contactPersonFirstname} ${event.contactPersonSurname}<br />
  ${event.organizer.phone}<br />
  ${event.organizer.email}<br />
  Notfallnummer während der Betreuung: ${event.emergencyPhone}`;
};

export const toChunks = <T>(array: T[], size: number) =>
  Array.from({ length: Math.ceil(array.length / size) }, (value, index) =>
    array.slice(index * size, index * size + size)
  );
