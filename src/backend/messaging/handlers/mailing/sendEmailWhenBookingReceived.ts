import { Job } from "bullmq";
import mustache from "mustache";
import { JobHandler } from "../../queueWorker";
import { buildCareDaysString } from "./helpers";

export const sendEmailWhenBookingReceived: JobHandler = async (
  job: Job,
  { mailConnection, context, mailConfig }
) => {
  const booking = (await context.query.Booking.findOne({
    where: {
      id: job.data.id,
    },
    query: `
      code
      price
      date
      custodian {
        firstname
        surname
      }
      event {
        name
        description
        locationCity
        locationZipcode
        locationStreet
        organizer {
          name
          users {
            email
          }
        }
        carePeriodCommitted {
          careDays( orderBy: { day: asc } ) {
            day
          }
        }
      }
      tickets {
        child {
          firstname
          surname
        }
      }
      `,
  })) as {
    code: string;
    date: string;
    price: number;
    event: {
      name: string;
      description: string;
      locationCity: string;
      locationZipcode: string;
      locationStreet: string;
      carePeriodCommitted: {
        careDays: {
          day: string;
        }[];
      };
      organizer: {
        name: string;
        users: {
          email: string;
        }[];
      };
    };
    tickets: {
      child: {
        firstname: string;
        surname: string;
      };
    }[];
    custodian: {
      firstname: string;
      surname: string;
    };
  } | null;

  if (!booking) {
    console.warn(`booking ${job.data.id} not found`);
    return;
  }

  const emailTemplate = (await context.query.EmailTemplate.findOne({
    where: {
      role: "bookingReceived",
    },
    query: "role subject content",
  })) as {
    role: string;
    subject: string;
    content: string;
  } | null;

  if (!emailTemplate) {
    console.warn(`email template ${job.name} not found`);
    return;
  }

  const bookingDate = new Date(booking.date);
  const careDays = booking.event.carePeriodCommitted.careDays;
  const vonBis = buildCareDaysString(careDays);
  const custodian = booking.custodian;
  const event = booking.event;
  const organizer = event.organizer;
  const children = booking.tickets.map((ticket) => ticket.child);

  const renderedTemplate = mustache.render(emailTemplate.content, {
    "Name Veranstaltung": event.name,
    "Ort/Treffpunkt": `${event.locationZipcode} ${event.locationCity}, ${event.locationStreet}`,
    "Datum Veranstaltung von bis": vonBis,
    Buchungsnummer: booking.code,
    "Name Veranstalter": organizer.name,
    Buchungsdatum: `${bookingDate.getDate().toString().padStart(2, "0")}.${(
      bookingDate.getMonth() + 1
    )
      .toString()
      .padStart(2, "0")}.${bookingDate.getFullYear()}, ${bookingDate
      .getHours()
      .toString()
      .padStart(2, "0")}:${bookingDate
      .getMinutes()
      .toString()
      .padStart(2, "0")} Uhr`,
    "Name Sorgeberechtigter": `${custodian.surname}, ${custodian.firstname}`,
    "Name Kind": children
      .map((child) => `${child.firstname} ${child.surname}`)
      .join(", "),
    Betrag: booking.price.toLocaleString(undefined, {
      minimumFractionDigits: 2,
      maximumFractionDigits: 2,
    }),
  });

  for (var user of organizer.users) {
    const emailResult = await mailConnection.sendMail({
      from: mailConfig.mailFrom,
      to: user.email,
      subject: emailTemplate.subject,
      html: renderedTemplate,
    });

    console.log(`sent booking email to ${user.email}`, emailResult.response);
  }
};
