import { Job } from "bullmq";
import mustache from "mustache";
import { JobHandler } from "../../queueWorker";

export const sendEmailWhenDiscountApplicationGranted: JobHandler = async (
  job: Job,
  { mailConnection, context, mailConfig }
) => {
  const custodian = (await context.query.Custodian.findOne({
    where: {
      id: job.data.custodianId,
    },
    query: "firstname surname salutation user { email }",
  })) as {
    firstname: string;
    surname: string;
    salutation: string;
    user: {
      email: string;
    };
  } | null;

  if (!custodian) {
    console.warn(`custodian for discount ${job.data.id} not found`);
    return;
  }

  const emailTemplate = (await context.query.EmailTemplate.findOne({
    where: {
      role: job.name,
    },
    query: `
      role
      subject
      content
      bccRecipients {
        id
        email
      }
    `,
  })) as {
    role: string;
    subject: string;
    content: string;
    bccRecipients: {
      id: string;
      email: string;
    }[];
  } | null;

  if (!emailTemplate) {
    console.warn(`email template ${job.name} not found`);
    return;
  }

  const renderedTemplate = mustache.render(emailTemplate.content, {
    Anrede:
      custodian.salutation === "mr"
        ? "Sehr geehrter Herr"
        : custodian.salutation == "ms"
        ? "Sehr geehrte Frau"
        : "Sehr geehrter Herr/Sehr geehrte Frau",
    Vorname: custodian.firstname,
    Nachname: custodian.surname,
  });

  const emailResult = await mailConnection.sendMail({
    from: mailConfig.mailFrom,
    to: custodian.user.email,
    bcc: emailTemplate.bccRecipients.map((user) => user.email),
    subject: emailTemplate.subject,
    html: renderedTemplate,
  });

  console.log(
    `sent discount application granted email to ${job.data.email}`,
    emailResult.response
  );
};
