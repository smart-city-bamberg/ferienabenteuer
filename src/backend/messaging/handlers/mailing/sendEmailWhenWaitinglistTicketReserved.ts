import { Job } from "bullmq";
import mustache from "mustache";
import { JobHandler } from "../../queueWorker";
import { buildCareDaysString, buildOrganizerContact } from "./helpers";

export const sendEmailWhenWaitinglistTicketReserved: JobHandler = async (
  job: Job,
  { mailConnection, context, mailConfig }
) => {
  const ticket = (await context.query.Ticket.findOne({
    where: {
      id: job.data.ticketId,
    },
    query: `    
      eventTicket {
        number
      }
      child {
        firstname
        surname
      }
      reservation {
        custodian {
          salutation
          firstname
          surname
          user {
            email
          }
        }
        date
        event {
          name
          description
          careTimeStart
          careTimeEnd
          locationCity
          locationZipcode
          locationStreet
          contactPersonSalutation
          contactPersonFirstname
          contactPersonSurname
          emergencyPhone
          organizer {
            name
            email
            phone
            users {
              email
            }
          }
          carePeriodCommitted {
            careDays( orderBy: { day: asc } ) {
              day
            }
          }
        }    
      }      
      `,
  })) as {
    eventTicket: {
      number: string;
    };
    child: {
      firstname: string;
      surname: string;
    };
    reservation: {
      date: string;
      custodian: {
        firstname: string;
        surname: string;
        salutation: string;
        user: {
          email: string;
        };
      };
      event: {
        name: string;
        description: string;
        careTimeStart: string;
        careTimeEnd: string;
        locationCity: string;
        locationZipcode: string;
        locationStreet: string;
        contactPersonSalutation: string;
        contactPersonFirstname: string;
        contactPersonSurname: string;
        emergencyPhone: string;
        carePeriodCommitted: {
          careDays: {
            day: string;
          }[];
        };
        organizer: {
          name: string;
          email: string;
          phone: string;
          users: {
            email: string;
          }[];
        };
      };
    };
  } | null;

  if (!ticket) {
    console.warn(`ticket ${job.data.ticketId} not found`);
    return;
  }

  const emailTemplate = (await context.query.EmailTemplate.findOne({
    where: {
      role: "waitingListTicketReserved",
    },
    query: `
      role
      subject
      content     
    `,
  })) as {
    role: string;
    subject: string;
    content: string;
  } | null;

  if (!emailTemplate) {
    console.warn(`email template waitingListTicketReserved not found`);
    return;
  }

  const reservation = ticket.reservation;
  const ticksIn14Days = 14 * 24 * 60 * 60 * 1000;

  const dateIn14Days = new Date(
    new Date(reservation.date).getTime() + ticksIn14Days
  );

  const custodian = reservation.custodian;
  const event = reservation.event;
  const careDays = event.carePeriodCommitted.careDays;
  const vonBis = buildCareDaysString(careDays);

  const renderedTemplate = mustache.render(emailTemplate.content, {
    Anrede:
      custodian.salutation === "mr"
        ? "Sehr geehrter Herr"
        : custodian.salutation == "ms"
        ? "Sehr geehrte Frau"
        : "Sehr geehrter Herr/Sehr geehrte Frau",
    Vorname: custodian.firstname,
    Nachname: custodian.surname,
    "Name Veranstaltung": ticket.reservation.event.name,
    "Name Veranstalter": ticket.reservation.event.organizer.name,
    "Name Kind": `${ticket.child.firstname} ${ticket.child.surname}`,
    "Beschreibung der Veranstaltung": event.description,
    "von bis": `${event.careTimeStart} bis ${event.careTimeEnd} Uhr`,
    "Ort/Treffpunkt": `${event.locationZipcode} ${event.locationCity}, ${event.locationStreet}`,
    "Kontaktdaten Veranstalter": buildOrganizerContact(event),
    Wartelistenplatz: ticket.eventTicket.number,
    "Datum Veranstaltung von bis": vonBis,
    "Datum in 14 Tagen": `${dateIn14Days
      .getDate()
      .toString()
      .padStart(2, "0")}.${(dateIn14Days.getMonth() + 1)
      .toString()
      .padStart(2, "0")}.${dateIn14Days.getFullYear()}`,
  });

  const emailResult = await mailConnection.sendMail({
    from: mailConfig.mailFrom,
    to: custodian.user.email,
    bcc: event.organizer.users.map((user) => user.email),
    subject: emailTemplate.subject,
    html: renderedTemplate,
  });

  console.log(
    `sent waitinglist ticket reserved email to ${custodian.user.email}`,
    emailResult.response
  );
};
