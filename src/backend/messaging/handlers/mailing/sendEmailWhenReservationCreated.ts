import { Job } from "bullmq";
import mustache from "mustache";
import { JobHandler } from "../../queueWorker";

export const sendEmailWhenReservationCreated: JobHandler = async (
  job: Job,
  { mailConnection, context, mailConfig, eventQueue }
) => {
  const reservation = (await context.query.Reservation.findOne({
    where: {
      id: job.data.id,
    },
    query: `      
      tickets {
        id
        child {
          firstname
          surname
        }
        discountVoucher {
          id
          discount {
            status
          }
        }
        eventTicket {
          type
        }
      }
      `,
  })) as {
    tickets: {
      id: string;
      firstname: string;
      surname: string;
      discountVoucher: {
        id: string;
        discount: {
          status: string;
        };
      } | null;
      eventTicket: {
        type: string;
      };
    }[];
  } | null;

  if (!reservation) {
    console.warn(`reservation ${job.data.id} not found`);
    return;
  }

  const participantTickets = reservation.tickets.filter(
    (ticket) => ticket.eventTicket.type === "participant"
  );

  const waitinglistTickets = reservation.tickets.filter(
    (ticket) => ticket.eventTicket.type === "waitinglist"
  );

  eventQueue.addBulk(
    participantTickets.map((ticket) => ({
      name: "participantTicketReserved",
      data: {
        ticketId: ticket.id,
      },
    }))
  );

  eventQueue.addBulk(
    waitinglistTickets.map((ticket) => ({
      name: "waitinglistTicketReserved",
      data: {
        ticketId: ticket.id,
      },
    }))
  );
};
