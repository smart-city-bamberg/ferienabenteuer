import { Job } from "bullmq";
import mustache from "mustache";
import { JobHandler } from "../../queueWorker";
import { buildCareDaysString } from "./helpers";

export const sendEmailToOrganizerWhenEventCancelled: JobHandler = async (
  job: Job,
  { mailConnection, context, mailConfig }
) => {
  const cancelledEvent = job.data;

  if (cancelledEvent.status !== "cancelled") {
    return;
  }

  const event = (await context.query.Event.findOne({
    where: {
      id: cancelledEvent.id,
    },
    query: `            
      name        
      contactPersonSalutation
      contactPersonFirstname
      contactPersonSurname
      emergencyPhone
      carePeriodCommitted {
        careDays( orderBy: { day: asc } ) {
          day
        }
      }
      organizer {
        name
        email
        users {
          email
        }
      }                    
      `,
  })) as {
    name: string;
    contactPersonSalutation: string;
    contactPersonFirstname: string;
    contactPersonSurname: string;
    emergencyPhone: string;
    carePeriodCommitted: {
      careDays: {
        day: string;
      }[];
    };
    organizer: {
      name: string;
      email: string;
      users: {
        email: string;
      }[];
    };
  } | null;

  if (!event) {
    console.warn(`event ${cancelledEvent.id} not found`);
    return;
  }

  const emailTemplate = (await context.query.EmailTemplate.findOne({
    where: {
      role: "eventCancelled",
    },
    query: "role subject content",
  })) as {
    role: string;
    subject: string;
    content: string;
  } | null;

  if (!emailTemplate) {
    console.warn(`email template eventCancelled not found`);
    return;
  }

  const careDays = event.carePeriodCommitted.careDays;
  const vonBis = buildCareDaysString(careDays);

  const renderedTemplate = mustache.render(emailTemplate.content, {
    Anrede:
      event.contactPersonSalutation === "mr"
        ? "Sehr geehrter Herr"
        : event.contactPersonSalutation == "ms"
        ? "Sehr geehrte Frau"
        : "Sehr geehrter Herr/Sehr geehrte Frau",
    Vorname: event.contactPersonFirstname,
    Nachname: event.contactPersonSurname,
    "Name Veranstaltung": event.name,
    "Datum Veranstaltung von bis": vonBis,
  });

  const emailResult = await mailConnection.sendMail({
    from: mailConfig.mailFrom,
    to: event.organizer.users.map((user) => user.email),
    subject: emailTemplate.subject,
    html: renderedTemplate,
  });

  console.log(
    `sent event cancelled email to ${event.organizer.users
      .map((user) => user.email)
      .join(", ")}`,
    emailResult.response
  );
};
