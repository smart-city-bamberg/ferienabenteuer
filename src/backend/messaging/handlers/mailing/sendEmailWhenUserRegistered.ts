import mustache from "mustache";
import { JobHandler } from "../../queueWorker";

export const sendEmailWhenUserRegistered: JobHandler = async (
  job,
  { context, mailConnection, mailConfig }
) => {
  const custodian = (await context.prisma.custodian.findFirst({
    where: {
      userId: job.data.id,
    },
  })) as {
    firstname: string;
    surname: string;
    salutation: string;
  } | null;

  if (!custodian) {
    console.warn(
      `custodian for new user ${job.data.id} with mail ${job.data.email} not found`
    );
    return;
  }

  const verificationToken =
    (await context.prisma.userVerificationToken.findFirst({
      where: {
        userId: job.data.id,
      },
    })) as {
      validationToken: string;
    } | null;

  if (!verificationToken) {
    console.warn(
      `verificationToken for new user ${job.data.id} with mail ${job.data.email} not found`
    );
    return;
  }

  const emailTemplate = (await context.query.EmailTemplate.findOne({
    where: {
      role: job.name,
    },
    query: "role subject content",
  })) as {
    role: string;
    subject: string;
    content: string;
  } | null;

  if (!emailTemplate) {
    console.warn(`email template ${job.name} not found`);
    return;
  }

  const renderedTemplate = mustache.render(emailTemplate.content, {
    Anrede:
      custodian.salutation === "mr"
        ? "Sehr geehrter Herr"
        : custodian.salutation == "ms"
        ? "Sehr geehrte Frau"
        : "Sehr geehrter Herr/Sehr geehrte Frau",
    Vorname: custodian.firstname,
    Nachname: custodian.surname,
    Code: verificationToken.validationToken,
    Bestätigungslink: `${mailConfig.hostUrl}/aktivieren?token=${verificationToken.validationToken}`,
  });

  const emailResult = await mailConnection.sendMail({
    from: mailConfig.mailFrom,
    to: job.data.email,
    subject: emailTemplate.subject,
    html: renderedTemplate,
  });

  console.log(
    `sent registration email to ${job.data.email}`,
    emailResult.response
  );
};
