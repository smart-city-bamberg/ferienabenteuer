import { Job } from "bullmq";
import { JobHandler } from "../queueWorker";

export const deleteDiscountVouchersWhenDiscountRejected: JobHandler = async (
  job: Job,
  { mailConnection, context, mailConfig, eventQueue }
) => {
  const discount = job.data;

  await context.prisma.$transaction(async (transaction) => {
    await transaction.discountVoucher.deleteMany({
      where: {
        discountId: discount.id,
      },
    });
  });
};
