import { Job } from "bullmq";
import { JobHandler } from "../queueWorker";

export const deleteEmptyBookingWhenBookingCancelled: JobHandler = async (
  job: Job,
  { mailConnection, context, mailConfig, eventQueue }
) => {
  const cancelledTicket = job.data;

  await context.prisma.$transaction(async (transaction) => {
    const bookingTickets = await transaction.ticket.findMany({
      where: {
        bookingId: cancelledTicket.bookingId,
        isDeleted: {
          equals: false,
        },
      },
    });

    if (bookingTickets.length === 0) {
      await transaction.booking.update({
        where: {
          id: cancelledTicket.bookingId,
        },
        data: {
          isDeleted: true,
        },
      });
    }
  });
};
