import { Job } from "bullmq";
import { JobHandler } from "../queueWorker";

export const cancelBookingsAndReservationsWhenEventCancelled: JobHandler =
  async (job: Job, { mailConnection, context, mailConfig, eventQueue }) => {
    const cancelledEvent = job.data;

    if (cancelledEvent.status !== "cancelled") {
      return;
    }

    const result = await context.prisma.$transaction(async (transaction) => {
      const deletedTickets = [];

      const bookedEventTickets = await transaction.eventTicket.findMany({
        where: {
          eventId: { equals: cancelledEvent.id },
          bookedTicket: {
            isNot: null,
          },
        },
      });

      for (const eventTicket of bookedEventTickets) {
        const ticket = await transaction.ticket.findFirst({
          where: {
            id: {
              equals: eventTicket.bookedTicketId as string,
            },
          },
        });

        if (!ticket) {
          console.log(`Ticket ${eventTicket.bookedTicketId} does not exist.`);
          continue;
        }

        const deletedTicket = await transaction.ticket.update({
          where: {
            id: ticket.id,
          },
          data: {
            isDeleted: true,
          },
        });

        deletedTickets.push(deletedTicket);

        await transaction.discountVoucher.updateMany({
          where: {
            ticketId: ticket.id,
          },
          data: {
            ticketId: null,
          },
        });

        if (ticket.bookingId) {
          await transaction.booking.update({
            where: {
              id: ticket.bookingId,
            },
            data: {
              isDeleted: true,
            },
          });
        }

        if (ticket.reservationId) {
          await transaction.reservation.update({
            where: {
              id: ticket.reservationId,
            },
            data: {
              isDeleted: true,
            },
          });
        }

        await transaction.eventTicket.delete({
          where: {
            id: eventTicket.id,
          },
        });
      }

      return {
        deletedTickets,
      };
    });

    await eventQueue.addBulk(
      result.deletedTickets.map((ticket) => ({
        name: "ticketCancelled",
        data: ticket,
      }))
    );
  };
