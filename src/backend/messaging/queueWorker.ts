import mustache from "mustache";
import SMTPTransport from "nodemailer/lib/smtp-transport";
import { Context } from ".keystone/types";
import { Job, Worker } from "bullmq";
import { Transporter } from "nodemailer";
import { cartTimeout } from "./handlers/cartTimeout";
import { IMessageQueue } from "../extensions/eventQueue";
import { Messages } from "../messages";
import { createBookingWhenDiscountApplicationGrantedOrRejected } from "./handlers/createBookingWhenDiscountApplicationGrantedOrRejected";
import { cancelBookingsAndReservationsWhenEventCancelled } from "./handlers/cancelBookingsAndReservationsWhenEventCancelled";
import { deleteDiscountVouchersWhenDiscountRejected } from "./handlers/deleteDiscountVouchersWhenDiscountRejected";
import { deleteEmptyBookingWhenBookingCancelled } from "./handlers/deleteEmptyBookingWhenBookingCancelled";
import { deleteEmptyReservationWhenReservationCancelled } from "./handlers/deleteEmptyReservationWhenReservationCancelled";
import { sendEmailWhenPaymentRegistered } from "./handlers/mailing/sendEmailWhenPaymentRegistered";
import { sendEmailWhenBookingCreated } from "./handlers/mailing/sendEmailWhenBookingCreated";
import { sendEmailWhenDiscountApplicationRejected } from "./handlers/mailing/sendEmailWhenDiscountApplicationRejected";
import { sendEmailWhenDiscountApplicationReceived } from "./handlers/mailing/sendEmailWhenDiscountApplicationReceived";
import { sendEmailWhenUserRegistered } from "./handlers/mailing/sendEmailWhenUserRegistered";
import { sendEmailWhenUserVerifiedAccount } from "./handlers/mailing/sendEmailWhenUserVerifiedAccount";
import { sendEmailWhenDiscountApplicationGranted } from "./handlers/mailing/sendEmailWhenDiscountApplicationGranted";
import { sendEmailWhenReservationCreated } from "./handlers/mailing/sendEmailWhenReservationCreated";
import { sendEmailWhenParticipantTicketReserved } from "./handlers/mailing/sendEmailWhenParticipantTicketReserved";
import { sendEmailWhenWaitinglistTicketReserved } from "./handlers/mailing/sendEmailWhenWaitinglistTicketReserved";
import { sendEmailWhenBookingCancelled } from "./handlers/mailing/sendEmailWhenBookingCancelled";
import { sendEmailWhenReservationCancelled } from "./handlers/mailing/sendEmailWhenReservationCancelled";
import { sendEmailWhenBookingReceived } from "./handlers/mailing/sendEmailWhenBookingReceived";
import { sendEmailWhenPasswortResetRequested } from "./handlers/mailing/sendEmailWhenPasswortResetRequested";
import { sendEmailWhenWaitinglistTicketConvertedToBooking } from "./handlers/mailing/sendEmailWhenWaitinglistTicketConvertedToBooking";
import { sendEmailWhenTicketCancelled } from "./handlers/mailing/sendEmailWhenTicketCancelled";
import { sendEmailToOrganizerWhenEventCancelled } from "./handlers/mailing/sendEmailToOrganizerWhenEventCancelled";
import { sendEmailWhenDiscountApplicationReminderRequested } from "./handlers/mailing/sendEmailWhenDiscountApplicationReminderRequested";
import { sendEmailToCustodiansWhenEventFinished } from "./handlers/mailing/sendEmailToCustodiansWhenEventFinished";
import { sendEmailToAllCustodians } from "./handlers/mailing/sendEmailToAllCustodians";
import { sendEmailWhenBookingCreatedAndAssistanceRequired } from "./handlers/mailing/sendEmailWhenBookingCreatedAndAssistanceRequired";
import { sendEmailWhenUserDeletedAccount } from "./handlers/mailing/sendEmailWhenUserDeletedAccount";
import { sendEmailWhenUserMarkedForDeletion } from "./handlers/mailing/sendEmailWhenUserMarkedForDeletion";
import { sendEmailWhenUserDeletedAfterMarkedForDeletion } from "./handlers/mailing/sendEmailWhenUserDeletedAfterMarkedForDeletion";
import { sendEmailWhenWaitinglistTicketConvertedToParticipant } from "./handlers/mailing/sendEmailWhenWaitinglistTicketConvertedToParticipant";

mustache.escape = (input) => input;

export interface QueueConfig {
  host: string;
  port: number;
  name: string;
}

export interface MailConfig {
  hostUrl: string;
  mailFrom: string;
}

export type JobHandlerContext = {
  context: Context;
  mailConnection: Transporter<SMTPTransport.SentMessageInfo>;
  mailConfig: MailConfig;
  eventQueue: IMessageQueue;
};

export type JobHandler = (
  job: Job,
  handlerContext: JobHandlerContext
) => Promise<void>;

export const buildEventWorker = (
  context: Context,
  mailConnection: Transporter<SMTPTransport.SentMessageInfo>,
  queue: QueueConfig,
  config: MailConfig,
  eventQueue: () => IMessageQueue
) =>
  new Worker(
    queue.name,
    handleJobs(context, mailConnection, config, eventQueue),
    {
      connection: {
        host: queue.host,
        port: queue.port,
      },
    }
  );

const jobHandlers: Record<Messages, JobHandler[]> = {
  cartTimeout: [cartTimeout],
  userRegistered: [sendEmailWhenUserRegistered],
  userVerifiedAccount: [sendEmailWhenUserVerifiedAccount],
  passwordResetRequested: [sendEmailWhenPasswortResetRequested],
  discountApplicationReceived: [sendEmailWhenDiscountApplicationReceived],
  discountApplicationGranted: [
    sendEmailWhenDiscountApplicationGranted,
    createBookingWhenDiscountApplicationGrantedOrRejected,
  ],
  discountApplicationRejected: [
    sendEmailWhenDiscountApplicationRejected,
    createBookingWhenDiscountApplicationGrantedOrRejected,
    deleteDiscountVouchersWhenDiscountRejected,
  ],
  bookingCreated: [
    sendEmailWhenBookingCreated,
    sendEmailWhenBookingCreatedAndAssistanceRequired,
    sendEmailWhenBookingReceived,
  ],
  bookingCancelled: [
    sendEmailWhenBookingCancelled,
    deleteEmptyBookingWhenBookingCancelled,
  ],
  reservationCreated: [sendEmailWhenReservationCreated],
  reservationCancelled: [
    sendEmailWhenReservationCancelled,
    deleteEmptyReservationWhenReservationCancelled,
  ],
  participantTicketReserved: [sendEmailWhenParticipantTicketReserved],
  waitinglistTicketReserved: [sendEmailWhenWaitinglistTicketReserved],
  paymentRegistered: [sendEmailWhenPaymentRegistered],
  waitinglistTicketSlotAdvanced: [],
  waitinglistTicketConvertedToBooking: [
    sendEmailWhenWaitinglistTicketConvertedToBooking,
    sendEmailWhenBookingReceived,
  ],
  waitinglistTicketConvertedToParticipant: [
    sendEmailWhenWaitinglistTicketConvertedToParticipant,
  ],
  eventStateChanged: [
    sendEmailToOrganizerWhenEventCancelled,
    cancelBookingsAndReservationsWhenEventCancelled,
  ],
  ticketCancelled: [sendEmailWhenTicketCancelled],
  resendBookingConfirmationRequested: [sendEmailWhenBookingCreated],
  discountApplicationReminderRequested: [
    sendEmailWhenDiscountApplicationReminderRequested,
  ],
  eventFinished: [sendEmailToCustodiansWhenEventFinished],
  emailToAllCustodiansRequested: [sendEmailToAllCustodians],
  userDeletedAccount: [sendEmailWhenUserDeletedAccount],
  userMarkedForDeletion: [sendEmailWhenUserMarkedForDeletion],
  userDeletedAfterMarkForDeletion: [
    sendEmailWhenUserDeletedAfterMarkedForDeletion,
  ],
};

const handleJobs =
  (
    context: Context,
    mailConnection: Transporter<SMTPTransport.SentMessageInfo>,
    mailConfig: MailConfig,
    eventQueue: () => IMessageQueue
  ) =>
  async (job: Job) => {
    console.log("Handling worker job", job.name);

    const handlers = jobHandlers[job.name as Messages];

    if (handlers.length === 0) {
      console.warn(`No JobHandler registered for ${job.name}`);
      return;
    }

    for (const handler of handlers) {
      console.log(`executing job handler for job ${job.name}`);

      try {
        await handler(job, {
          context,
          mailConfig,
          mailConnection,
          eventQueue: eventQueue(),
        });
      } catch (error) {
        console.log(`error in handler for ${job.name}`, error);
      }
    }
  };
