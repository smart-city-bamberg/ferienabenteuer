# Ferienabenteuer - Ferienangebote einfach online buchen

Das vorliegende Projekt ist ein Angebot der Familienregion Bamberg, das berufstätige Eltern bei der Vereinbarkeit von Familie und Beruf unterstützen möchte. Stadt und Landkreis Bamberg bieten eine Homepage an (https://ferienabenteuer-bamberg.de/), über die Veranstaltungen des Ferienabenteuers bequem eingesehen und gebucht werden können. Dabei unsterstützt das System sowohl die Eltern beim gesamten Registrierungs- und Buchungsprozess sowie die regionalen Anbieter bei der Verwaltung ihrer Angebote.


Hierfür verfügt das System zwei Frontends. Zum einen das sog. "Frontend Buchung" welches für die Öffentlichkeit zugänglich ist und für die Familien welche für ihre Kinder eine Ferienbetreuung suchen gemacht ist. Hierüber können Angebote gebucht, storniert und verwaltet werden.


Für die Veranstalter wurde das sog. "Frontend Verwaltungs" umgesetzt, welches Funktionalitäten für die Verwaltung von Veranstaltungen bietet. Dieses wird auch von den Administratoren bzw. Betreibern verwendet, welche hierüber seinen Verwaltungsaufgaben nachgehen kann wie z.B. Verwalten von Anbietern und Verwalten von Ermäßigungen.

### Frontend Buchung
<img src="docs/Frontend-Buchung.png" alt="Bildschirmbeispiel" width="950"/>

### Frontend Verwaltung
<img src="docs/Frontend-Verwaltung.png" alt="Bildschirmbeispiel" width="950"/>

## Architektur

<img src="docs/Architektur.png" alt="Bildschirmbeispiel" width="510"/>

Das Projekt besteht aus folgenden 3 Unterprojekten
* [CMS](src/backend/README.md): Keystone 6 CMS
* [Verwaltungs Frontend](src/frontend-verwaltung/README.md): Angular Web-App 
* [Buchungs Frontend](src/frontend-buchung/README.md): Angular Web-App 

Dokumentation ist in den einzelnen Unterprojekten im entsprechenden Verzeichnis hinterlegt.

## Förderung

Dieses Projekt wurde gefördert durch das  Bundesministerium des Innern, für Bau und Heimat and die Kreditanstalt für Wiederaufbau.

<img src="docs/Logo_BMI.jpg" alt="BMI" width="250"/>
<img src="docs/KFW.png" alt="KFW" width="200"/>
